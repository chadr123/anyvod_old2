﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "PlayListItemUpdater.h"
#include "models/PlayListModel.h"
#include "utils/RemoteFileUtils.h"
#include "pickers/URLPicker.h"

#include <QGuiApplication>

PlayListItemUpdater::PlayListItemUpdater(PlayListModel *parent) :
    m_parent(parent),
    m_stop(false)
{

}

void PlayListItemUpdater::setStop(bool stop)
{
    this->m_stop = stop;
}

void PlayListItemUpdater::run()
{
    QVector<PlayItem> list;

    this->m_parent->getPlayList(&list);

    for (const PlayItem &item : qAsConst(list))
    {
        if (this->m_stop)
            break;

        if (item.itemUpdated)
            continue;

        bool itemUpdated = false;
        QString title;

        if (RemoteFileUtils::determinRemoteProtocol(item.path) && !RemoteFileUtils::determinRemoteFile(item.path))
        {
            URLPicker picker;
            QVector<URLPickerInterface::Item> urls;

            urls = picker.pickURL(item.path);

            if (!urls.isEmpty())
            {
                title = urls.first().title;
                itemUpdated = true;
            }
        }
        else
        {
            title = item.title;
            itemUpdated = true;
        }

        if (itemUpdated)
            QGuiApplication::postEvent(this->m_parent, new PlayListModel::UpdateItemEvent(title, item.unique));
        else
            QGuiApplication::postEvent(this->m_parent, new PlayListModel::UpdateIncCountEvent(item.unique));
    }
}
