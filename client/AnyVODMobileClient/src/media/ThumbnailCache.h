﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "media/FrameExtractor.h"

#include <QThread>
#include <QModelIndex>
#include <QQueue>
#include <QMutex>
#include <QWaitCondition>
#include <QUuid>

class ThumbnailCache : public FrameExtractor, QThread
{
public:
    struct Info
    {
        Info()
        {
            duration = 0.0;
            width = 0;
            height = 0;
            fileSize = 0;
            isVideo = false;
        }

        QString path;
        QString imgPath;
        double duration;
        int width;
        int height;
        unsigned long long fileSize;
        bool isVideo;
    };

private:
    struct RequestInfo
    {
        RequestInfo()
        {
            role = -1;
        }

        QString path;
        QModelIndex index;
        int role;
        QUuid signature;
    };

public:
    ThumbnailCache();
    ~ThumbnailCache();

    void setReciever(QObject *reciever);
    void setOnlyDirectory(bool only);
    void setOnlySubtitle(bool only);
    void setOnlyETC(bool only);
    void stop();
    void clear();
    bool exists(const QString &moviePath);
    bool getThumbnailInfo(const QString &moviePath, Info *ret);

    void requestToUpdate(const QString &moviePath, const QModelIndex &index, int role, const QUuid &signature);

public:
    static const QString CONFIG_EXTENSION;

private:
    QString getConfigRoot() const;
    QString getConfigFile(const QString &moviePath, bool exists) const;
    double getDuration(const QString &moviePath) const;
    int getEntryCount(const QString &path) const;

private:
    QQueue<RequestInfo> m_reqList;
    QMutex m_mutex;
    QWaitCondition m_cond;
    QObject *m_reciever;
    bool m_onlyDirectory;
    bool m_onlySubtitle;
    bool m_onlyETC;

private:
    volatile bool m_stop;

protected:
    virtual void run();
};
