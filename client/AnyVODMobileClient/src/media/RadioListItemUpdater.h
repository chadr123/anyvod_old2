﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "device/RadioReaderInterface.h"

#include <QThread>

class RadioReader;
class RadioListModel;

class RadioListItemUpdater : public QThread
{
    Q_OBJECT
public:
    explicit RadioListItemUpdater(RadioListModel *parent);

    void setData(long adapter, RadioReaderInterface::DemodulatorType demodulatorType, QLocale::Country country,
                 int start, int end);
    void setReader(RadioReader *reader);
    void setStop(bool stop);

protected:
    virtual void run();

private:
    RadioListModel *m_parent;
    RadioReader *m_reader;
    long m_adapter;
    RadioReaderInterface::DemodulatorType m_type;
    QLocale::Country m_country;
    int m_start;
    int m_end;

private:
    volatile bool m_stop;
};
