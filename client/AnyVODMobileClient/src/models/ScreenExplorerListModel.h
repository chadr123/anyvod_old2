﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "media/ScreenExplorerListItemUpdater.h"

#include <QAbstractListModel>
#include <QEvent>
#include <QImage>

class ScreenExplorerProvider;

class ScreenExplorerListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    static const QEvent::Type ADD_ITEM_EVENT;
    static const QEvent::Type UPDATE_PARENT_EVENT;

public:
    enum RoleName
    {
        ScreenRole = Qt::UserRole,
        TimeRole,
        TimeStringRole,
    };

    class AddItemEvent : public QEvent
    {
    public:
        AddItemEvent(double time, double rotation, const QImage &screen) :
            QEvent(ADD_ITEM_EVENT),
            m_time(time),
            m_rotation(rotation),
            m_screen(screen)
        {

        }

        double getTime() const { return this->m_time; }
        double getRotation() const { return this->m_rotation; }
        QImage getScreen() const { return this->m_screen; }

    private:
        double m_time;
        double m_rotation;
        QImage m_screen;
    };

    class UpdateParentEvent : public QEvent
    {
    public:
        UpdateParentEvent() :
            QEvent(UPDATE_PARENT_EVENT)
        {

        }
    };

private:
    struct Item
    {
        Item()
        {
            time = 0.0;
            rotation = 0.0;
        }

        QImage screen;
        double time;
        double rotation;
    };

public:
    explicit ScreenExplorerListModel(QObject *parent = nullptr);
    ~ScreenExplorerListModel();

    Q_INVOKABLE double getDuration(const QString &path) const;
    Q_INVOKABLE void updateScreen(const QString &path, int second);
    Q_INVOKABLE void clear();
    Q_INVOKABLE bool isMovie(const QString &path) const;

public:
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

protected:
    virtual QHash<int, QByteArray> roleNames() const;

private:
    virtual void customEvent(QEvent *event);

public:
    static const QString SCREEN_ID;

private:
    ScreenExplorerListItemUpdater m_updater;
    QHash<int, QByteArray> m_roleNames;
    QVector<Item> m_items;
    bool m_stopAddingItem;
    ScreenExplorerProvider *m_provider;
};
