﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QAbstractListModel>

class SearchMediaModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum RoleName
    {
        FileNameRole = Qt::UserRole,
        PathRole,
        ThumbnailRole,
    };

public:
    explicit SearchMediaModel(QObject *parent = nullptr);
    ~SearchMediaModel();

    Q_INVOKABLE bool search(const QString &startPath, const QString &filter);
    Q_INVOKABLE QStringList getResult() const;

public:
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

protected:
    virtual QHash<int, QByteArray> roleNames() const;

private:
    QStringList searchMedia(const QString &startPath, const QString &func, const QString &filter) const;
    QStringList searchMediaOnFileSystem(const QString &startPath, const QStringList &filter) const;

private:
    QHash<int, QByteArray> m_roleNames;
    QStringList m_paths;
};
