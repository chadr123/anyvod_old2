﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ManagePlayListModel.h"
#include "setting/Settings.h"
#include "setting/SettingsFactory.h"

#include <QSettings>
#include <QDebug>

ManagePlayListModel::ManagePlayListModel(QObject *parent) :
    QAbstractListModel(parent),
    m_settings(SettingsFactory::getInstance(SettingsFactory::ST_GLOBAL))
{
    QVariantHash hash = this->m_settings.value(SETTING_PLAYLIST, QVariantHash()).toHash();

    this->reload(hash);

    this->m_roleNames[NameRole] = "name";
}

ManagePlayListModel::~ManagePlayListModel()
{

}

void ManagePlayListModel::deleteItem(const QString &name)
{
    int i;

    for (i = 0; i < this->m_names.count(); i++)
    {
        if (this->m_names[i] == name)
            break;
    }

    if (i >= this->m_names.count())
        return;

    QVariantHash hash = this->m_settings.value(SETTING_PLAYLIST, QVariantHash()).toHash();

    hash.remove(name);
    this->m_settings.setValue(SETTING_PLAYLIST, hash);

    this->removeRow(i);
}

void ManagePlayListModel::reload(const QVariantHash &hash)
{
    this->beginResetModel();

    this->m_names = hash.keys();
    this->m_names.sort();

    this->endResetModel();
}

int ManagePlayListModel::rowCount(const QModelIndex &parent) const
{
    (void)parent;

    return this->m_names.count();
}

QVariant ManagePlayListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row();

    if (row < 0 || row >= this->m_names.count())
        return QVariant();

    QVariant ret;
    QString item = this->m_names[row];

    switch (role)
    {
        case NameRole:
        {
            ret = item;
            break;
        }
        default:
        {
            ret = QVariant();
            break;
        }
    }

    return ret;
}

bool ManagePlayListModel::removeRows(int row, int count, const QModelIndex &parent)
{
    int last = row + count - 1;

    if (row >= this->m_names.count() || last >= this->m_names.count())
        return false;

    this->beginRemoveRows(parent, row, last);

    while (count --> 0)
        this->m_names.removeAt(row);

    this->endRemoveRows();

    return true;
}

QHash<int, QByteArray> ManagePlayListModel::roleNames() const
{
    return this->m_roleNames;
}
