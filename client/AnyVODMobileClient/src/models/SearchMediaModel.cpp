﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SearchMediaModel.h"
#include "FileListModel.h"
#include "core/FileExtensions.h"
#include "utils/MediaTypeUtils.h"

#include <QFileInfo>
#include <QDir>

#ifdef Q_OS_ANDROID
# include <QAndroidJniEnvironment>
# include <QAndroidJniObject>
# include <QtAndroid>
#endif

SearchMediaModel::SearchMediaModel(QObject *parent) :
    QAbstractListModel(parent)
{
    this->m_roleNames[PathRole] = "path";
    this->m_roleNames[FileNameRole] = "fileName";
    this->m_roleNames[ThumbnailRole] = "thumbnail";
}

SearchMediaModel::~SearchMediaModel()
{

}

bool SearchMediaModel::search(const QString &startPath, const QString &filter)
{
    this->beginResetModel();

    this->m_paths.clear();

    this->m_paths += this->searchMedia(startPath, "getVideoURLs", filter);
    this->m_paths += this->searchMedia(startPath, "getAudioURLs", filter);

    this->endResetModel();

    return !this->m_paths.isEmpty();
}

QStringList SearchMediaModel::getResult() const
{
    return this->m_paths;
}

int SearchMediaModel::rowCount(const QModelIndex &parent) const
{
    (void)parent;

    return this->m_paths.count();
}

QVariant SearchMediaModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row();

    if (row < 0 || row >= this->m_paths.count())
        return QVariant();

    QVariant ret;
    QString item = this->m_paths[row];

    switch (role)
    {
        case FileNameRole:
        {
            ret = QFileInfo(item).fileName();
            break;
        }
        case PathRole:
        {
            ret = item;
            break;
        }
        case ThumbnailRole:
        {
            QFileInfo info(item);
            QString suffix = info.suffix();

            if (MediaTypeUtils::isExtension(suffix, MT_VIDEO))
                ret = FileListModel::DEFAULT_MOVIE_ICON;
            else if (MediaTypeUtils::isExtension(suffix, MT_AUDIO))
                ret = FileListModel::DEFAULT_AUDIO_ICON;
            else
                ret = FileListModel::DEFAULT_ETC_ICON;

            break;
        }
        default:
        {
            ret = QVariant();
            break;
        }
    }

    return ret;
}

QHash<int, QByteArray> SearchMediaModel::roleNames() const
{
    return this->m_roleNames;
}

QStringList SearchMediaModel::searchMedia(const QString &startPath, const QString &func, const QString &filter) const
{
    QStringList ret;

#if defined Q_OS_ANDROID
    (void)startPath;
    QAndroidJniEnvironment env;
    QAndroidJniObject activity = QtAndroid::androidActivity();
    QAndroidJniObject paths = activity.callObjectMethod(func.toLatin1(),
                                                        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;",
                                                        QAndroidJniObject::fromString("name").object<jstring>(),
                                                        QAndroidJniObject::fromString("").object<jstring>(),
                                                        QAndroidJniObject::fromString(filter).object<jstring>());

    if (paths.isValid())
    {
        int pathCount = env->GetArrayLength(paths.object<jarray>());
        jobjectArray pathsJava = paths.object<jobjectArray>();

        for (int i = 0; i < pathCount; i++)
        {
            jobject pathJava = env->GetObjectArrayElement(pathsJava, i);
            QAndroidJniObject path = pathJava;

            ret.append(path.toString());
            env->DeleteLocalRef(pathJava);
        }
    }
#else
    QStringList filterList;
    QStringList extList;

    if (func == "getVideoURLs")
        extList = FileExtensions::MOVIE_EXTS_LIST;
    else if (func == "getAudioURLs")
        extList = FileExtensions::AUDIO_EXTS_LIST;

    if (!extList.isEmpty())
    {
        for (const QString &ext : qAsConst(extList))
            filterList << "*" + filter + "*." + ext;

        ret = this->searchMediaOnFileSystem(startPath, filterList);
    }
#endif

    return ret;
}

QStringList SearchMediaModel::searchMediaOnFileSystem(const QString &startPath, const QStringList &filter) const
{
    QDir dir(startPath);
    QStringList list;
    const QFileInfoList files = dir.entryInfoList(filter, QDir::Files, QDir::Name);
    const QFileInfoList dirs = dir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name);

    for (const QFileInfo &f : files)
        list.append(f.absoluteFilePath());

    for (const QFileInfo &d : dirs)
        list.append(this->searchMediaOnFileSystem(d.absoluteFilePath(), filter));

    return list;
}
