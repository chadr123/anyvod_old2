﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "media/ThumbnailCache.h"
#include "ui/RenderScreen.h"

#include <QAbstractListModel>
#include <QEvent>
#include <QUuid>
#include <QStack>

class FileListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    static const QEvent::Type UPDATE_ITEM_EVENT;

public:
    class UpdateItemEvent : public QEvent
    {
    public:
        UpdateItemEvent(QVariant var, QModelIndex &index, int role, QUuid &signature) :
            QEvent(UPDATE_ITEM_EVENT),
            m_var(var),
            m_index(index),
            m_role(role),
            m_signature(signature)
        {

        }

        QVariant getVar() const { return this->m_var; }
        QModelIndex getIndex() const { return this->m_index; }
        int getRole() const { return this->m_role; }
        QUuid getSignature() const { return this->m_signature; }

    private:
        QVariant m_var;
        QModelIndex m_index;
        int m_role;
        QUuid m_signature;
    };

    enum Type
    {
        Directory,
        File,
    };
    Q_ENUM(Type)

    enum MediaType
    {
        Video,
        Audio,
    };
    Q_ENUM(MediaType)

    enum QuickBarType
    {
        InternalType = 0,
        ExternalType,
        VideoType,
        AudioType,
        QuickBarTypeCount
    };
    Q_ENUM(QuickBarType)

    enum SortType
    {
        NameAscending,
        TimeAscending,
        SizeAscending,
        TypeAscending,
        NameDescending,
        TimeDescending,
        SizeDescending,
        TypeDescending,
    };
    Q_ENUM(SortType)

    enum RoleName
    {
        ThumbnailRole = Qt::UserRole,
        NameRole,
        TypeRole,
        PathRole,
        FileInfoRole,
        FileNameRole,
        DirEntryCountRole,
        DurationRole,
        CurPositionRole,
    };

private:
    struct Item
    {
        Item()
        {
            dirEntryCount = -1;
            duration = -1.0;
            curPosition = -1.0;
            isLoaded = false;
        }

        QString path;
        QString imgPath;
        QString fileInfo;
        int dirEntryCount;
        double duration;
        double curPosition;
        bool isLoaded;
    };

public:
    explicit FileListModel(QObject *parent = nullptr);
    ~FileListModel();

public:
    virtual int rowCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role);
    virtual bool removeRows(int row, int count, const QModelIndex &parent);

public:
    Q_INVOKABLE void init();

    Q_INVOKABLE double cdup();
    Q_INVOKABLE void cd(const QString &path, double scrollPos);
    Q_INVOKABLE void clearScrollPos();

    Q_INVOKABLE void update();
    Q_INVOKABLE void update(const QString &path);
    Q_INVOKABLE void updateFromDatabase(int type);
    Q_INVOKABLE void updateCurrentPos(const QString &filePath);

    Q_INVOKABLE QString getFirstFilePath(const QString &path) const;
    Q_INVOKABLE bool deletePath(const QString &path);
    Q_INVOKABLE bool isSecondaryPath(const QString &path) const;
    Q_INVOKABLE QString appendEntry(const QString &base, const QString &entry) const;

    Q_INVOKABLE void setSortType(int type);
    Q_INVOKABLE int getSortType() const;

    Q_INVOKABLE void selectSecondaryPath(int index);
    Q_INVOKABLE int getSecondaryPathCount() const;
    Q_INVOKABLE bool existSecondaryPath() const;
    Q_INVOKABLE int getSelectedSecondaryPath() const;
    Q_INVOKABLE QStringList getSecondaryPathNames() const;

    Q_INVOKABLE QStringList getItems() const;

public:
    static const QString DEFAULT_MOVIE_ICON;
    static const QString DEFAULT_AUDIO_ICON;
    static const QString DEFAULT_SUBTITLE_ICON;
    static const QString DEFAULT_ETC_ICON;
    static const QString DEFAULT_DIRECTORY_ICON;
    static const QString OPEN_DIRECTORY_ICON;

public:
    Q_PROPERTY(QString curDirName READ curDirName CONSTANT)
    Q_PROPERTY(QString curDirPath READ curDirPath CONSTANT)
    Q_PROPERTY(QString topSignature READ topSignature CONSTANT)
    Q_PROPERTY(QString primaryPath READ primaryPath CONSTANT)
    Q_PROPERTY(QString secondaryPath READ secondaryPath CONSTANT)

    Q_PROPERTY(QString startDirectory MEMBER m_startDirectory NOTIFY startDirectoryChanged)
    Q_PROPERTY(bool onlyDirectory MEMBER m_onlyDirectory NOTIFY onlyDirectoryChanged)
    Q_PROPERTY(bool onlySubtitle MEMBER m_onlySubtitle NOTIFY onlySubtitleChanged)
    Q_PROPERTY(bool onlyETC MEMBER m_onlyETC NOTIFY onlyETCChanged)

    Q_PROPERTY(QString defaultMovieIcon MEMBER DEFAULT_MOVIE_ICON CONSTANT)
    Q_PROPERTY(QString defaultAudioIcon MEMBER DEFAULT_AUDIO_ICON CONSTANT)
    Q_PROPERTY(QString defaultSubtitleIcon MEMBER DEFAULT_SUBTITLE_ICON CONSTANT)
    Q_PROPERTY(QString defaultEtcIcon MEMBER DEFAULT_ETC_ICON CONSTANT)
    Q_PROPERTY(QString defaultDirectoryIcon MEMBER DEFAULT_DIRECTORY_ICON CONSTANT)
    Q_PROPERTY(QString openDirectoryIcon MEMBER OPEN_DIRECTORY_ICON CONSTANT)

    Q_PROPERTY(int quickBarType MEMBER m_quickBarType CONSTANT)

signals:
    void startDirectoryChanged();
    void onlyDirectoryChanged();
    void onlySubtitleChanged();
    void onlyETCChanged();

protected:
    QString curDirName() const;
    QString curDirPath() const;
    QString topSignature() const;

    QString primaryPath() const;
    QString secondaryPath() const;

    bool existsMediaFiles(const QString &path) const;

private:
    QStringList findSecondaryPaths() const;
    QStringList findRemovablePaths() const;
    void saveQuickBarType(QuickBarType type);
    bool isSubFilelistMode() const;

private:
    virtual void customEvent(QEvent *event);

protected:
    virtual QHash<int, QByteArray> roleNames() const;

protected:
    QVector<Item> m_curList;

private:
    QHash<int, QByteArray> m_roleNames;
    QString m_curDir;
    QString m_rootDirPrimary;
    QString m_rootDirSecondary;
    ThumbnailCache m_cache;
    QUuid m_signature;
    QString m_startDirectory;
    bool m_onlyDirectory;
    bool m_onlySubtitle;
    bool m_onlyETC;
    QStack<double> m_scrollPos;
    QuickBarType m_quickBarType;
    SortType m_sortType[QuickBarTypeCount];
    QStringList m_rootDirSecondaries;
};
