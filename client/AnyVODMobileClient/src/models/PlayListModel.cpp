﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "PlayListModel.h"
#include "utils/RemoteFileUtils.h"
#include "utils/PlayItemUtils.h"
#include "utils/SeparatingUtils.h"
#include "utils/MediaTypeUtils.h"
#include "setting/Settings.h"
#include "parsers/playlist/PlayListParserInterface.h"
#include "parsers/playlist/PlayListParserGenerator.h"
#include "models/FileListModel.h"
#include "setting/SettingsFactory.h"

#include <QFileInfo>
#include <QGuiApplication>
#include <QClipboard>
#include <QDir>
#include <QSettings>

const QEvent::Type PlayListModel::UPDATE_ITEM_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type PlayListModel::UPDATE_INC_COUNT = (QEvent::Type)QEvent::registerEventType();
const int PlayListModel::UPDATE_PLAYLIST_TIME = 5000;
const int PlayListModel::MAX_RETRY_COUNT = 5;

QDataStream& operator << (QDataStream &out, const PlayItem &item)
{
    out << item.path;
    out << item.extraData.duration;
    out << item.extraData.totalFrame;
    out << item.extraData.valid;
    out << item.unique;
    out << item.title;
    out << item.totalTime;
    out << item.itemUpdated;
    out << item.extraData.userData;
    out << item.retry;

    return out;
}

QDataStream& operator >> (QDataStream &in, PlayItem &item)
{
    in >> item.path;
    in >> item.extraData.duration;
    in >> item.extraData.totalFrame;
    in >> item.extraData.valid;
    in >> item.unique;
    in >> item.title;
    in >> item.totalTime;
    in >> item.itemUpdated;
    in >> item.extraData.userData;
    in >> item.retry;

    if (item.unique.isNull())
        item.unique = QUuid::createUuid();

    return in;
}

PlayListModel::PlayListModel(QObject *parent) :
    QAbstractListModel(parent),
    m_updater(this),
    m_updateTimerID(0),
    m_currentIndex(-1),
    m_prevIndex(-1),
    m_isAutoSave(true),
    m_settings(SettingsFactory::getInstance(SettingsFactory::ST_GLOBAL))
{
    this->m_roleNames[NameRole] = "name";
    this->m_roleNames[PathRole] = "path";
    this->m_roleNames[TitleRole] = "title";
    this->m_roleNames[URLPickerRole] = "isURLPicker";
    this->m_roleNames[FileNameRole] = "fileName";
    this->m_roleNames[ThumbnailRole] = "thumbnail";
    this->m_roleNames[ColorRole] = "color";

    this->m_updateTimerID = this->startTimer(UPDATE_PLAYLIST_TIME);
}

PlayListModel::~PlayListModel()
{

}

int PlayListModel::rowCount(const QModelIndex &parent) const
{
    (void)parent;

    return this->m_playItems.count();
}

QVariant PlayListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row();

    if (row < 0 || row >= this->m_playItems.count())
        return QVariant();

    QVariant ret;
    const PlayItem &item = this->m_playItems[row];

    switch (role)
    {
        case FileNameRole:
        {
            ret = QFileInfo(item.path).fileName();
            break;
        }
        case NameRole:
        {
            ret = QFileInfo(item.path).completeBaseName();
            break;
        }
        case TitleRole:
        {
            ret = item.title;
            break;
        }
        case URLPickerRole:
        {
            ret = !item.extraData.userData.isEmpty();
            break;
        }
        case PathRole:
        {
            ret = item.path;
            break;
        }
        case ThumbnailRole:
        {
            QFileInfo info(item.path);
            QString suffix = info.suffix();

            if (MediaTypeUtils::isExtension(suffix, MT_VIDEO))
                ret = FileListModel::DEFAULT_MOVIE_ICON;
            else if (MediaTypeUtils::isExtension(suffix, MT_AUDIO))
                ret = FileListModel::DEFAULT_AUDIO_ICON;
            else if (MediaTypeUtils::isExtension(suffix, MT_SUBTITLE))
                ret = FileListModel::DEFAULT_SUBTITLE_ICON;
            else
                ret = FileListModel::DEFAULT_ETC_ICON;

            break;
        }
        case ColorRole:
        {
            ret = this->m_currentIndex == row ? "red" : "black";
            break;
        }
        default:
        {
            ret = QVariant();
            break;
        }
    }

    return ret;
}

bool PlayListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;

    int row = index.row();

    if (row < 0 || row >= this->m_playItems.count())
        return false;

    PlayItem &item = this->m_playItems[row];
    QVector<int> roles;

    roles.append(role);

    switch (role)
    {
        case TitleRole:
        {
            item.title = value.toString();
            break;
        }
        case ColorRole:
        {
            break;
        }
        default:
        {
            return false;
        }
    }

    emit dataChanged(index, index, roles);

    return true;
}

QHash<int, QByteArray> PlayListModel::roleNames() const
{
    return this->m_roleNames;
}

void PlayListModel::customEvent(QEvent *event)
{
    if (event->type() == UPDATE_ITEM_EVENT)
    {
        UpdateItemEvent *e = (UpdateItemEvent*)event;
        int index = this->findIndex(e->getUnique());

        if (index >= 0)
        {
            PlayItem &data = this->m_playItems[index];
            QString title = e->getTitle();

            data.itemUpdated = true;

            this->setData(this->index(index), title, TitleRole);
        }
    }
    else if (event->type() == UPDATE_INC_COUNT)
    {
        UpdateIncCountEvent *e = (UpdateIncCountEvent*)event;
        int index = this->findIndex(e->getUnique());

        if (index >= 0)
        {
            PlayItem &data = this->m_playItems[index];

            data.retry++;

            if (data.retry > MAX_RETRY_COUNT)
                data.itemUpdated = true;
        }
    }
    else
    {
        QAbstractListModel::customEvent(event);
    }
}

void PlayListModel::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == this->m_updateTimerID)
        this->startUpdateThread();
}

void PlayListModel::startUpdateThread()
{
    if (!this->m_updater.isRunning())
    {
        this->m_updater.setStop(false);
        this->m_updater.start();
    }
}

void PlayListModel::addItem(QFileInfo &info, PlayItem &item)
{
    item.unique = QUuid::createUuid();
    item.path = SeparatingUtils::adjustNetworkPath(info.filePath());

    this->m_playItems.append(item);
}

int PlayListModel::findURLPickerIndex(const QString &quality, const QString &mime, const QVector<URLPickerInterface::Item> &items) const
{
    for (int i = 0; i < items.count(); i++)
    {
        const URLPickerInterface::Item &item = items[i];

        if (item.quality == quality && item.mimeType == mime)
            return i;
    }

    return 0;
}

void PlayListModel::setPlayList(const QVector<PlayItem> &list)
{
    this->clearPlayList();
    this->addPlayList(list);
}

bool PlayListModel::addPlayList(const QVector<PlayItem> &list)
{
    bool added = false;

    this->beginResetModel();

    for (int i = 0; i < list.count(); i++)
    {
        PlayItem playItem = list[i];
        QFileInfo info(playItem.path);
        QString suffix = info.suffix();
        PlayListParserGenerator gen;
        PlayListParserInterface *parser = gen.getParser(suffix);

        if (parser)
        {
            QVector<PlayListParserInterface::PlayListItem> fileList;

            parser->setRoot(info.absolutePath() + QDir::separator());

            if (parser->open(playItem.path))
            {
                parser->getFileList(&fileList);
                parser->close();

                for (const PlayListParserInterface::PlayListItem &item : qAsConst(fileList))
                {
                    QFileInfo fileInfo(item.path);
                    PlayItem fileItem;

                    fileItem.title = item.title;

                    this->addItem(fileInfo, fileItem);
                    added = true;
                }
            }
            else if (!parser->isSuccess())
            {
                this->addItem(info, playItem);
                added = true;
            }

            delete parser;
        }
        else
        {
            this->addItem(info, playItem);
            added = true;
        }
    }

    this->endResetModel();
    this->startUpdateThread();

    return added;
}

void PlayListModel::getPlayList(QVector<PlayItem> *ret)
{
    QMutexLocker locker(&this->m_getPlayListMutex);

    for (int i = 0; i < this->getCount(); i++)
        ret->append(this->getPlayItem(i));
}

int PlayListModel::getCount() const
{
    return this->m_playItems.count();
}

bool PlayListModel::exist() const
{
    return this->getCount() > 0;
}

bool PlayListModel::canSave() const
{
    return this->m_isAutoSave && !this->m_name.isEmpty();
}

QString PlayListModel::getFileName(int index) const
{
    if (index >= 0 && index < this->getCount())
        return QFileInfo(this->m_playItems[index].path).fileName();
    else
        return QString();
}

PlayItem PlayListModel::getPlayItem(int index) const
{
    return this->m_playItems[index];
}

PlayItem PlayListModel::getCurrentPlayItem() const
{
    return this->getPlayItem(this->m_currentIndex);
}

int PlayListModel::findIndex(const QUuid &unique) const
{
    for (int i = 0; i < this->getCount(); i++)
    {
        PlayItem item = this->getPlayItem(i);

        if (item.unique == unique)
            return i;
    }

    return -1;
}

bool PlayListModel::setCurrentIndexByUnique(const QUuid &unique)
{
    if (unique == QUuid())
    {
        this->resetCurrentIndex();
        return false;
    }
    else
    {
        this->setCurrentIndex(this->findIndex(unique));
        return true;
    }
}

void PlayListModel::setRandomCurrentIndex()
{
    this->setCurrentIndex(rand() % this->getCount());
}

void PlayListModel::resetCurrentIndex()
{
    this->setCurrentIndex(-1);
}

void PlayListModel::decreaseCurrentIndex()
{
    this->setCurrentIndex(this->m_currentIndex - 1);
}

void PlayListModel::increaseCurrentIndex()
{
    this->setCurrentIndex(this->m_currentIndex + 1);
}

void PlayListModel::adjustCurrentIndex()
{
    if (this->m_currentIndex >= this->getCount())
        this->setCurrentIndex(this->getCount() - 1);
    else if (this->m_currentIndex < 0)
        this->setCurrentIndex(0);
    else if (this->getCount() <= 0)
        this->resetCurrentIndex();
}

bool PlayListModel::isVaildCurrentIndex() const
{
    return this->m_currentIndex >= 0 && this->m_currentIndex < this->getCount();
}

bool PlayListModel::selectOtherQuality(int quality)
{
    return this->selectOtherQuality(this->m_currentIndex, quality);
}

bool PlayListModel::selectOtherQuality(int index, int quality)
{
    PlayItem &playItem = this->m_playItems[index];

    if (playItem.urlPickerData.isEmpty() || playItem.urlPickerIndex == quality)
        return false;

    playItem.urlPickerIndex = quality;

    this->m_lastURLPickerMime = playItem.urlPickerData[quality].mimeType;
    this->m_lastURLPickerQuality = playItem.urlPickerData[quality].quality;

    return true;
}

bool PlayListModel::hasQualities(int index) const
{
    if (index >= 0 && index < this->getCount())
        return this->m_playItems[index].urlPickerData.count() > 0;
    else
        return false;
}

QStringList PlayListModel::getQualityDescriptions(int index) const
{
    QStringList list;
    const PlayItem &playItem = this->m_playItems[index];

    for (const URLPickerInterface::Item &item : playItem.urlPickerData)
        list.append(item.shortDesc);

    return list;
}

QString PlayListModel::getPath(int index, bool realPath) const
{
    const PlayItem &item = this->m_playItems[index];
    QString path;

    if (realPath)
    {
        if (item.urlPickerData.isEmpty())
            path = item.path;
        else
            path = item.urlPickerData[item.urlPickerIndex].url;

    }
    else
    {
        path = item.path;
    }

    return path;
}

void PlayListModel::copyPath(const QString &text)
{
    QClipboard *clipboard = QGuiApplication::clipboard();
    QString clip;

    if (RemoteFileUtils::determinRemoteProtocol(text))
        clip = text;
    else
        clip = QDir::toNativeSeparators(text);

    clipboard->setText(clip);
}

void PlayListModel::setCurrentIndex(int index)
{
    this->m_currentIndex = index;

    if (index >= 0)
        this->setData(this->index(index), QVariant(), ColorRole);

    if (this->m_prevIndex >= 0)
        this->setData(this->index(this->m_prevIndex), QVariant(), ColorRole);

    this->m_prevIndex = index;
}

int PlayListModel::getCurrentIndex() const
{
    return this->m_currentIndex;
}

void PlayListModel::setPlayListItems(const QStringList &list, int index)
{
    QVector<PlayItem> playList = PlayItemUtils::getPlayItemFromFileNames(list);

    this->setPlayList(playList);
    this->setCurrentIndex(index);
}

void PlayListModel::setRemotePlayListItems(const QVariantList &list, int index)
{
    QVector<PlayItem> playList;

    for (const QVariant &var : list)
        playList.append(var.value<PlayItem>());

    this->setPlayList(playList);
    this->setCurrentIndex(index);
}

void PlayListModel::setPlayList(const QString &name)
{
    QVariantHash hash = this->m_settings.value(SETTING_PLAYLIST, QVariantHash()).toHash();
    QVector<PlayItem> playList;
    int curIndex = 0;
    QString baseURL;
    QVariantList list = hash[name].toList();

    if (list.isEmpty())
        return;

    for (const QVariant &item : qAsConst(list))
    {
        switch (item.type())
        {
            case QVariant::Int:
                curIndex = item.toInt();
                break;
            case QVariant::String:
                baseURL = item.toString();
                break;
            default:
                playList.append(item.value<PlayItem>());
                break;
        }
    }

    if (curIndex >= playList.count())
        curIndex = playList.count() - 1;

    this->setPlayList(playList);
    this->setCurrentIndex(curIndex);

    this->m_name = name;
    this->m_baseURL = baseURL;
}

bool PlayListModel::existPlayList(const QString &name) const
{
    QVariantHash hash = this->m_settings.value(SETTING_PLAYLIST, QVariantHash()).toHash();

    return hash.contains(name);
}

void PlayListModel::savePlayListAs(const QString &name)
{
    QVariantHash hash = this->m_settings.value(SETTING_PLAYLIST, QVariantHash()).toHash();
    QVariantList list;

    for (const PlayItem &item : qAsConst(this->m_playItems))
        list.append(QVariant::fromValue(item));

    list.append(this->m_currentIndex);

    if (!this->m_baseURL.isEmpty())
        list.append(this->m_baseURL);

    hash[name] = list;
    this->m_settings.setValue(SETTING_PLAYLIST, hash);

    this->m_name = name;
    this->m_isAutoSave = true;
}

void PlayListModel::updateCurrentPlayList()
{
    this->savePlayListAs(this->m_name);
}

QString PlayListModel::getName() const
{
    return this->m_name;
}

void PlayListModel::setBaseURL(const QString url)
{
    this->m_baseURL = url;
}

QString PlayListModel::getBaseURL() const
{
    return this->m_baseURL;
}

void PlayListModel::setName(const QString &name)
{
    this->m_name = name;
    this->m_isAutoSave = false;
}

void PlayListModel::clearPlayList()
{
    this->stop();
    this->m_name.clear();

    this->beginResetModel();
    this->m_playItems.clear();
    this->endResetModel();
}

void PlayListModel::stop()
{
    this->m_updater.setStop(true);
    this->m_updater.wait();
}

int PlayListModel::updateURLPickerData(const QUuid &unique, const QVector<URLPickerInterface::Item> &items)
{
    int index = this->findIndex(unique);

    if (index >= 0)
    {
        PlayItem &data = this->m_playItems[index];

        data.urlPickerData = items;
        data.urlPickerIndex = this->findURLPickerIndex(this->m_lastURLPickerQuality, this->m_lastURLPickerMime, items);

        if (data.urlPickerIndex == 0)
        {
            this->m_lastURLPickerMime.clear();
            this->m_lastURLPickerQuality.clear();
        }

        return data.urlPickerIndex;
    }

    return 0;
}
