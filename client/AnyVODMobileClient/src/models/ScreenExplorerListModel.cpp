﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ScreenExplorerListModel.h"
#include "ScreenExplorerProvider.h"
#include "core/AnyVODWindow.h"
#include "media/FrameExtractor.h"
#include "utils/ConvertingUtils.h"
#include "utils/MediaTypeUtils.h"
#include "utils/RemoteFileUtils.h"

#include <QQmlEngine>
#include <QFileInfo>
#include <QDebug>

const QEvent::Type ScreenExplorerListModel::ADD_ITEM_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type ScreenExplorerListModel::UPDATE_PARENT_EVENT = (QEvent::Type)QEvent::registerEventType();

const QString ScreenExplorerListModel::SCREEN_ID = "ScreenExplorer";

ScreenExplorerListModel::ScreenExplorerListModel(QObject *parent) :
    QAbstractListModel(parent),
    m_updater(this),
    m_stopAddingItem(false),
    m_provider(nullptr)
{
    this->m_roleNames[ScreenRole] = "screen";
    this->m_roleNames[TimeRole] = "time";
    this->m_roleNames[TimeStringRole] = "timeString";

    this->m_provider = (ScreenExplorerProvider*)AnyVODWindow::getInstance()->engine()->imageProvider(SCREEN_ID);
}

ScreenExplorerListModel::~ScreenExplorerListModel()
{
    this->m_updater.stopUpdate();
}

double ScreenExplorerListModel::getDuration(const QString &path) const
{
    FrameExtractor ex;

    if (!ex.open(path, true))
        return 0.0;

    return ex.getDuration();
}

void ScreenExplorerListModel::clear()
{
    this->beginResetModel();
    this->m_items.clear();
    this->endResetModel();

    this->m_provider->clear();
}

bool ScreenExplorerListModel::isMovie(const QString &path) const
{
    QFileInfo info(path);
    bool enable = false;

    if ((!RemoteFileUtils::determinRemoteFile(path) && MediaTypeUtils::isExtension(info.suffix(), MT_VIDEO)) ||
            (!RemoteFileUtils::determinRemoteFile(path) && RemoteFileUtils::determinRemoteProtocol(path)))
        enable = true;

    return enable;
}

int ScreenExplorerListModel::rowCount(const QModelIndex &parent) const
{
    (void)parent;

    return this->m_items.count();
}

QVariant ScreenExplorerListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row();

    if (row < 0 || row >= this->m_items.count())
        return QVariant();

    QVariant ret;
    const Item &item = this->m_items[row];

    switch (role)
    {
        case ScreenRole:
        {
            QImage img = item.screen;
            QTransform trans;
            QString id = QString::number(row);

            trans.translate((img.width() / 2.0f), (img.height() / 2.0f));
            trans.rotate(item.rotation);
            trans.translate((-img.width() / 2.0f), (-img.height() / 2.0f));

            this->m_provider->addImage(id, img.transformed(trans));

            ret = "image://" + SCREEN_ID + "/" + id;
            break;
        }
        case TimeRole:
        {
            ret = item.time;
            break;
        }
        case TimeStringRole:
        {
            QString time;

            ConvertingUtils::getTimeString(item.time, ConvertingUtils::TIME_HH_MM_SS, &time);

            ret = time;
            break;
        }
        default:
        {
            ret = QVariant();
            break;
        }
    }

    return ret;
}

QHash<int, QByteArray> ScreenExplorerListModel::roleNames() const
{
    return this->m_roleNames;
}

void ScreenExplorerListModel::customEvent(QEvent *event)
{
    if (event->type() == ADD_ITEM_EVENT)
    {
        if (this->m_stopAddingItem)
            return;

        AddItemEvent *e = (AddItemEvent*)event;
        int pos = this->m_items.count();
        Item item;

        item.screen = e->getScreen();
        item.time = e->getTime();
        item.rotation = e->getRotation();

        this->beginInsertRows(this->index(pos), pos, pos);
        this->m_items.append(item);
        this->endInsertRows();
    }
    else if (event->type() == UPDATE_PARENT_EVENT)
    {
        this->m_stopAddingItem = false;
    }
    else
    {
        QAbstractListModel::customEvent(event);
    }
}

void ScreenExplorerListModel::updateScreen(const QString &path, int second)
{
    if (this->m_updater.isRunning())
    {
        this->m_stopAddingItem = true;
        this->m_updater.stopUpdate();
    }

    this->clear();
    this->m_updater.startUpdate(path, second);
}
