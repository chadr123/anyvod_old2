﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ToastDelegate.h"

#ifdef Q_OS_ANDROID
# include <QtAndroid>
#endif

ToastDelegate::ToastDelegate()
{

}

void ToastDelegate::show(const QString &msg) const
{
#ifdef Q_OS_ANDROID
    QAndroidJniObject activity = QtAndroid::androidActivity();

    if (activity.isValid())
        activity.callMethod<void>("showToast", "(Ljava/lang/String;)V", QAndroidJniObject::fromString(msg).object<jstring>());
#else
    (void)msg;
#endif
}

int ToastDelegate::getLength() const
{
#ifdef Q_OS_ANDROID
    QAndroidJniObject activity = QtAndroid::androidActivity();

    if (activity.isValid())
        return activity.callMethod<int>("getToastLength");
#else
#endif
    return 0;
}

bool ToastDelegate::isSupported() const
{
#ifdef Q_OS_ANDROID
    return true;
#else
    return false;
#endif
}
