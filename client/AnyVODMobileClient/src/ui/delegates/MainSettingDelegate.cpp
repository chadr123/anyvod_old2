﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "MainSettingDelegate.h"
#include "setting/Settings.h"
#include "setting/SettingsFactory.h"
#include "setting/SettingBackupAndRestore.h"
#include "utils/AnyVODEnumsUtils.h"
#include "video/ScreenCaptureHelper.h"

#include <QDir>
#include <QSettings>

const QHash<AnyVODEnums::ShortcutKey, QString> MainSettingDelegate::SHORTCUTKEY_TABLE =
{
    { AnyVODEnums::SK_LANG,                             SETTING_LANGUAGE },
    { AnyVODEnums::SK_FONT,                             SETTING_FONT },
    { AnyVODEnums::SK_USE_HW_DECODER,                   SETTING_USE_HW_DECODER },
    { AnyVODEnums::SK_USE_FRAME_DROP,                   SETTING_USE_FRAME_DROP },
    { AnyVODEnums::SK_SHOW_ALBUM_JACKET,                SETTING_SHOW_ALBUM_JACKET },
    { AnyVODEnums::SK_SELECT_CAPTURE_EXT_ORDER,         SETTING_SCREEN_CAPTURE_EXT },
    { AnyVODEnums::SK_SELECT_CAPTURE_DIRECTORY,         SETTING_SCREEN_CAPTURE_DIR },
    { AnyVODEnums::SK_DEINTERLACE_METHOD_ORDER,         SETTING_DEINTERLACER_METHOD },
    { AnyVODEnums::SK_DEINTERLACE_ALGORITHM_ORDER,      SETTING_DEINTERLACER_ALGORITHM },
    { AnyVODEnums::SK_LAST_PLAY,                        SETTING_ENABLE_LAST_PLAY },
    { AnyVODEnums::SK_SEEK_KEYFRAME,                    SETTING_SEEK_KEYFRAME },
    { AnyVODEnums::SK_USE_BUFFERING_MODE,               SETTING_USE_BUFFERING_MODE },
    { AnyVODEnums::SK_SUBTITLE_TOGGLE,                  SETTING_SHOW_SUBTITLE },
    { AnyVODEnums::SK_SEARCH_SUBTITLE_COMPLEX,          SETTING_SEARCH_SUBTITLE_COMPLEX },
    { AnyVODEnums::SK_SUBTITLE_HALIGN_ORDER,            SETTING_HALIGN_SUBTITLE },
    { AnyVODEnums::SK_SUBTITLE_VALIGN_ORDER,            SETTING_VALIGN_SUBTITLE },
    { AnyVODEnums::SK_OPEN_TEXT_ENCODING,               SETTING_TEXT_ENCODING },
    { AnyVODEnums::SK_ENABLE_SEARCH_SUBTITLE,           SETTING_ENABLE_SEARCH_SUBTITLE },
    { AnyVODEnums::SK_ENABLE_SEARCH_LYRICS,             SETTING_ENABLE_SEARCH_LYRICS },
    { AnyVODEnums::SK_AUDIO_NORMALIZE,                  SETTING_USE_NORMALIZER },
    { AnyVODEnums::SK_AUDIO_EQUALIZER,                  SETTING_USE_EQUALIZER },
    { AnyVODEnums::SK_SUBTITLE_CACHE_MODE,              SETTING_USE_SUBTITLE_CACHE_MODE },
    { AnyVODEnums::SK_DELETE_MEDIA_WITH_SUBTITLE,       SETTING_DELETE_MEDIA_WITH_SUBTITLE },
    { AnyVODEnums::SK_USE_LOW_QUALITY_MODE,             SETTING_USE_LOW_QUALITY_MODE },
    { AnyVODEnums::SK_AUDIO_DEVICE_ORDER,               SETTING_AUDIO_DEVICE },
    { AnyVODEnums::SK_SPDIF_AUDIO_DEVICE_ORDER,         SETTING_SPDIF_DEVICE },
    { AnyVODEnums::SK_USE_SPDIF_ENCODING_ORDER,         SETTING_SPDIF_ENCODING },
    { AnyVODEnums::SK_SPDIF_SAMPLE_RATE_ORDER,          SETTING_SPDIF_SAMPLE_RATE },
    { AnyVODEnums::SK_USE_SPDIF,                        SETTING_USE_SPDIF },
    { AnyVODEnums::SK_KEYBOARD_LANGUAGE,                SETTING_KEYBOARD_LANGUAGE },
    { AnyVODEnums::SK_AUTO_SAVE_SEARCH_LYRICS,          SETTING_AUTO_SAVE_SEARCH_LYRICS },
    { AnyVODEnums::SK_UPDATE_FILENAME,                  SETTING_UPDATE_FILENAME },
    { AnyVODEnums::SK_CHECK_UPDATE_GAP,                 SETTING_CHECK_UPDATE_GAP },
    { AnyVODEnums::SK_CURRENT_UPDATE_DATE,              SETTING_CURRENT_UPDATE_DATE },
    { AnyVODEnums::SK_PROXY_INFO,                       SETTING_PROXY_INFO },
    { AnyVODEnums::SK_BLUETOOTH_AUDIO_SYNC,             SETTING_BLUETOOTH_AUDIO_SYNC },
    { AnyVODEnums::SK_SERVER_ADDRESS,                   SETTING_ADDRESS },
    { AnyVODEnums::SK_SERVER_COMMAND_PORT,              SETTING_COMMAND_PORT },
    { AnyVODEnums::SK_SERVER_STREAM_PORT,               SETTING_STREAM_PORT },
    { AnyVODEnums::SK_PLAY_ORDER,                       SETTING_PLAYING_METHOD },
};

MainSettingDelegate::MainSettingDelegate()
{

}

void MainSettingDelegate::setSetting(int key, QVariant value) const
{
    QSettings &settings(SettingsFactory::getInstance(SettingsFactory::ST_GLOBAL));
    QString name = this->shortcutKeyToName((AnyVODEnums::ShortcutKey)key);

    if (!name.isEmpty())
        settings.setValue(name, value);
}

QVariant MainSettingDelegate::getSetting(int key) const
{
    const QSettings &settings(SettingsFactory::getInstance(SettingsFactory::ST_GLOBAL));
    QVariant value;
    QString name = this->shortcutKeyToName((AnyVODEnums::ShortcutKey)key);

    if (!name.isEmpty())
    {
        value = settings.value(name);

        if (value.type() == QVariant::String)
        {
            QString b = value.toString().toLower();

            if (b == "false" || b == "true")
                return value.toBool();
        }
    }

    if (value.isNull())
        value = "";

    if (key == AnyVODEnums::SK_SELECT_CAPTURE_DIRECTORY)
    {
        QDir capDir(value.toString());

        if (!capDir.exists())
            value = ScreenCaptureHelper::getDefaultSavePath();
    }

    return value;
}

bool MainSettingDelegate::isSupported(int key) const
{
    return AnyVODEnumsUtils::isSupported((AnyVODEnums::ShortcutKey)key);
}

bool MainSettingDelegate::saveSettings(const QString &to) const
{
    return SettingBackupAndRestore::saveSettings(to);
}

bool MainSettingDelegate::loadSettings(const QString &from) const
{
    return SettingBackupAndRestore::loadSettings(from);
}

QString MainSettingDelegate::shortcutKeyToName(AnyVODEnums::ShortcutKey key) const
{
    return SHORTCUTKEY_TABLE[key];
}
