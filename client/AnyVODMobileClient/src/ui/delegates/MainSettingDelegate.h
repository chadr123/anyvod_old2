﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "core/AnyVODEnums.h"

#include <QQuickItem>

class MainSettingDelegate : public QQuickItem
{
    Q_OBJECT
public:
    MainSettingDelegate();

    Q_INVOKABLE void setSetting(int key, QVariant value) const;
    Q_INVOKABLE QVariant getSetting(int key) const;
    Q_INVOKABLE bool isSupported(int key) const;

    Q_INVOKABLE bool saveSettings(const QString &to) const;
    Q_INVOKABLE bool loadSettings(const QString &from) const;

private:
    QString shortcutKeyToName(AnyVODEnums::ShortcutKey key) const;

private:
    static const QHash<AnyVODEnums::ShortcutKey, QString> SHORTCUTKEY_TABLE;
};
