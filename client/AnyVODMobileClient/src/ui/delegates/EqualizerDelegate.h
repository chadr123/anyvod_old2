﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "ui/RenderScreen.h"

#include <QQuickItem>

class QSettings;

class EqualizerDelegate : public QQuickItem
{
    Q_OBJECT
public:
    EqualizerDelegate();

    Q_INVOKABLE void use(bool use) const;
    Q_INVOKABLE bool isUse() const;

    Q_INVOKABLE QStringList getPresetNames() const;

    Q_INVOKABLE int getBandGain(int band) const;
    Q_INVOKABLE int getCurrentIndex() const;
    Q_INVOKABLE int getPreamp() const;

    Q_INVOKABLE void initValue();
    Q_INVOKABLE void setPreset(int preset) const;
    Q_INVOKABLE void setBandForSave(int band, int value);

    Q_INVOKABLE void savePreamp(int value) const;
    Q_INVOKABLE void saveBand() const;

    Q_INVOKABLE float valueTodB(int value) const;

private:
    void initPreset(int maxBand);

    int getPresetBandCount(int preset) const;
    int getPresetBandGain(int preset, int band) const;

    int dBToValue(float dB) const;

private:
    struct Preset
    {
        QString desc;
        QVector<RenderScreen::EqualizerItem> gains;
    };

private:
    QSettings &m_settings;
    QVector<Preset> m_presets;
    QVector<RenderScreen::EqualizerItem> m_saveGains;
};
