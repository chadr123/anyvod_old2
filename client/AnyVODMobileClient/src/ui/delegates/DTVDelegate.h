﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "device/DTVReader.h"

#include <QQuickItem>

class DTVListModel;

class DTVDelegate : public QQuickItem
{
    Q_OBJECT
public:
    DTVDelegate();
    ~DTVDelegate();

    Q_INVOKABLE void init();
    Q_INVOKABLE void scan(int adapter, int systemType, int country, int start, int end);
    Q_INVOKABLE void stop();
    Q_INVOKABLE void save();

    Q_INVOKABLE QString getPath(int index);

    Q_INVOKABLE QStringList getAdapterInfos() const;
    Q_INVOKABLE QVector<int> getAdapterInfoValues() const;

    Q_INVOKABLE QStringList getSystemTypeInfoDescs() const;
    Q_INVOKABLE QVector<int> getSystemTypeInfoValues() const;

    Q_INVOKABLE QStringList getCountryInfoDescs() const;
    Q_INVOKABLE QVector<int> getCountryInfoValues() const;

    Q_INVOKABLE int getCurrentAdapterValue();
    Q_INVOKABLE int getCurrentSystemTypeValue() const;
    Q_INVOKABLE int getCurrentCountryValue() const;

    Q_INVOKABLE bool setChannelCountry(int country, int type);
    Q_INVOKABLE int getChannelCount() const;
    Q_INVOKABLE int getChannelFirst() const;
    Q_INVOKABLE int getChannelLast() const;

    QVector<DTVReaderInterface::SystemTypeInfo> getSystemTypeList() const;
    QVector<DTVReaderInterface::CountryInfo> getCountryList() const;

private:
    void findModel();
    int getAdapterInfoValue(int index) const;

private:
    DTVReader &m_reader;
    DTVListModel *m_model;
};
