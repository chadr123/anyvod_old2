﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RenderScreen.h"
#include "FontInfo.h"
#include "core/PlayItem.h"
#include "core/Common.h"
#include "core/FileExtensions.h"
#include "core/AnyVODWindow.h"
#include "core/EnumsTranslator.h"
#include "audio/SPDIFSampleRates.h"
#include "utils/PlayItemUtils.h"
#include "utils/SeparatingUtils.h"
#include "utils/ConvertingUtils.h"
#include "utils/FloatPointUtils.h"
#include "utils/FileListUtils.h"
#include "utils/RotationUtils.h"
#include "utils/RemoteFileUtils.h"
#include "utils/DeviceUtils.h"
#include "net/SubtitleImpl.h"
#include "net/Socket.h"
#include "net/VirtualFile.h"
#include "video/Camera.h"
#include "models/PlayListModel.h"
#include "device/DTVReader.h"
#include "device/RadioReader.h"
#include "media/FrameExtractor.h"
#include "media/VideoRenderer.h"
#include "media/AudioRenderer.h"
#include "pickers/URLPicker.h"
#include "setting/RenderScreenSettingLoader.h"
#include "setting/RenderScreenSettingSaver.h"
#include "../../../../common/SubtitleFileNameGenerator.h"
#include "../../../../common/version.h"

#include <QQuickWindow>
#include <QGuiApplication>
#include <QDir>
#include <QTemporaryFile>
#include <QStandardPaths>
#include <QRegularExpression>

#ifdef Q_OS_ANDROID
# include <QAndroidJniObject>
# include <QtAndroid>
#endif

#ifdef Q_OS_IOS
# include "../core/IOSDelegate.h"
#endif

#include <QDebug>

const QEvent::Type RenderScreen::PLAYING_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type RenderScreen::EMPTY_BUFFER_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type RenderScreen::AUDIO_SUBTITLE_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type RenderScreen::ABORT_EVENT = (QEvent::Type)QEvent::registerEventType();

RenderScreen *RenderScreen::m_self = nullptr;
QMutex RenderScreen::m_selfLock;

QDataStream& operator << (QDataStream &out, const RenderScreen::EqualizerItem &item)
{
    out << item.gain;

    return out;
}

QDataStream& operator >> (QDataStream &in, RenderScreen::EqualizerItem &item)
{
    in >> item.gain;

    return in;
}

RenderScreen::RenderScreen() :
    m_renderer(&this->m_presenter, this),
    m_wantToPlay(false),
    m_disableLastPosTmp(false),
    m_gotoTimeTmp(0.0),
    m_status(Stopped),
    m_gotoLastPlay(false),
    m_priorSubtitleDirectory(false),
    m_useSearchSubtitleComplex(false),
    m_playingMethod(AnyVODEnums::PM_TOTAL),
    m_spdifSampleRates(SPDIFSampleRates::getInstance()),
    m_pausedByInactive(false),
    m_isLandscape(false),
    m_useHeadTracking(false),
    m_appSuspended(qApp->applicationState() == Qt::ApplicationSuspended),
    m_subtitleSize(-1.0f),
    m_playList(nullptr),
    m_statusUpdater(this),
    m_rotationSensor(this, &this->m_presenter),
#ifdef Q_OS_IOS
    m_isPausedByIOSNotify(false),
#endif
    m_lyrics1(nullptr),
    m_lyrics2(nullptr),
    m_lyrics3(nullptr),
    m_empty(nullptr)
{
    connect(qApp, &QGuiApplication::applicationStateChanged, this, &RenderScreen::handleApplicationStateChanged);
    connect(this, &RenderScreen::windowChanged, this, &RenderScreen::handleWindowChanged);
    connect(this, &RenderScreen::exitFromRenderer, this, &RenderScreen::handleExitFromRenderer);

#ifdef Q_OS_IOS
    this->m_presenter.setIOSNotifyCallback(RenderScreen::iosNotifyCallback);
#endif

    EventCallback endedcb;
    EventCallback playingcb;

    endedcb.callback = RenderScreen::endedCallback;
    endedcb.userData = this;

    playingcb.callback = RenderScreen::playingCallback;
    playingcb.userData = this;

    this->m_presenter.setStatusChangedCallback(&playingcb, &endedcb);

    EmptyBufferCallback ecb;

    ecb.callback = RenderScreen::emptyBufferCallback;
    ecb.userData = this;
    this->m_presenter.setEmptyBufferCallback(ecb);

    AudioSubtitleCallback asc;

    asc.callback = RenderScreen::audioSubtitleCallback;
    asc.userData = this;
    this->m_presenter.setAudioSubtitleCallback(asc);

    PaintCallback pcb;

    pcb.callback = RenderScreen::paintCallback;
    pcb.userData = this;
    this->m_presenter.setPaintCallback(pcb);

    AbortCallback atcb;

    atcb.callback = RenderScreen::abortCallback;
    atcb.userData = this;
    this->m_presenter.setAbortCallback(atcb);

    EventCallback rccb;

    rccb.callback = RenderScreen::recoverCallback;
    rccb.userData = this;
    this->m_presenter.setRecoverCallback(rccb);
}

RenderScreen::~RenderScreen()
{
    this->close();
}

bool RenderScreen::open(const QString &path, ExtraPlayData &data, const QUuid &unique)
{
    QFileInfo info(path);
    QString fontFamily = QGuiApplication::font().family();
    PlayItem item = this->m_playList->getPlayItem(this->m_playList->findIndex(unique));
    QString audioPath;

    this->close();

    RenderScreen::m_selfLock.lock();
    RenderScreen::m_self = this;
    RenderScreen::m_selfLock.unlock();

    QString cacheDir = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    FontInfo &fontInfo = FontInfo::getInstance();
    QFile fontFile(fontInfo.getPath());
    QString fontPath = cacheDir + QDir::separator() + QFileInfo(fontInfo.getPath()).fileName();
    bool existFont = false;

    if (QFileInfo::exists(fontPath))
        existFont = true;
    else
        existFont = fontFile.copy(fontPath);

    if (existFont)
    {
        this->m_presenter.setASSFontPath(fontPath);
        this->m_presenter.setASSFontFamily(fontInfo.getFamily());
    }

    if (!item.urlPickerData.isEmpty())
    {
        for (const URLPickerInterface::Item &yItem : qAsConst(item.urlPickerData))
        {
            if (yItem.url == path)
            {
                URLPicker picker;

                if (!yItem.dashmpd.isEmpty())
                    picker.getAudioStreamByMimeType(yItem.orgUrl, yItem.dashmpd, yItem.mimeType, &audioPath);
                else if (yItem.noAudio)
                    picker.getProperAudioStreamByMimeType(yItem.orgUrl, item.urlPickerData, yItem.mimeType, &audioPath);

                break;
            }
        }
    }

    QString title = item.title.isEmpty() ? info.fileName() : item.title;

    if (this->m_presenter.open(path, title, data, fontFamily, 15, 2, audioPath))
    {
        if (data.userData.isEmpty())
            this->m_lastPlayPath = path;
        else
            this->m_lastPlayPath = data.userData;

        this->m_filePath = path;
        this->m_retainedFilePath = path;
        this->m_unique = unique;
        this->m_title = title;
        this->m_playData = data;

        if (!this->m_presenter.isRemoteFile() && !this->m_presenter.isRemoteProtocol())
            this->tryOpenSubtitle(SeparatingUtils::adjustNetworkPath(path));

        this->retreiveExternalSubtitle(path);
        this->m_playList->setCurrentIndexByUnique(unique);

        if (this->play())
        {
#ifndef Q_OS_RASPBERRY_PI
            if (this->isEnabledVideo())
                this->m_renderer.scheduleUseColorConversion(this->m_presenter.IsHDRVideo() ? GLRenderer::CC_ON : GLRenderer::CC_OFF);
            else
                this->m_renderer.scheduleUseColorConversion(GLRenderer::CC_OFF);
#endif
            this->checkGOMSubtitle();
            return true;
        }
    }

    return false;
}

bool RenderScreen::open(const QString &path)
{
    if (RemoteFileUtils::determinRemoteProtocol(path))
        this->openExternal(path);
    else
        this->updatePlayList(path);

    return true;
}

bool RenderScreen::openExternal(const QString &path)
{
    QVector<PlayItem> itemVector;
    QString baseURL;
    QString adjustedPath = path.trimmed();
    int index = adjustedPath.indexOf(PROTOCOL_POSTFIX);

    if (index >= 0)
    {
        index = adjustedPath.lastIndexOf(NEW_LINE, index);

        if (index < 0)
            index = adjustedPath.lastIndexOf(" ", index);

        if (index >= 0)
            adjustedPath = adjustedPath.mid(index).trimmed();
    }

    QString name = this->getURLPickerPlayItems(adjustedPath, &itemVector, &baseURL);

    this->m_playList->setName(name);
    this->m_playList->setBaseURL(baseURL);

    this->setPlayList(itemVector);

    if (!itemVector.isEmpty())
    {
        this->m_unique = itemVector.first().unique;
        this->m_playList->setCurrentIndexByUnique(this->m_unique);
    }

    return !itemVector.isEmpty();
}

bool RenderScreen::play()
{
    bool success = this->m_presenter.play();

    this->m_status = Started;
    this->callChanged();

    if (success)
        this->m_status = Playing;
    else
        this->m_status = Stopped;

    this->callChanged();

    if (success)
    {
        if (this->m_gotoLastPlay && !this->m_disableLastPosTmp)
        {
            double lastPos = this->m_lastPlay.get(SeparatingUtils::removeFFMpegSeparator(this->m_lastPlayPath));

            if (lastPos > 0.0)
                this->seek(lastPos, true);
        }

        if (this->m_disableLastPosTmp)
            this->seek(this->m_gotoTimeTmp, true);

        QString desc;

        if (this->m_playData.userData.isEmpty())
            desc = QFileInfo(SeparatingUtils::removeFFMpegSeparator(this->m_filePath)).fileName();
        else
            desc = this->m_title;

        desc += " (";

        if (this->isMovieSubtitleVisiable())
        {
            if (this->m_presenter.existSubtitle())
                desc += tr("자막 있음");
            else
                desc += tr("자막 없음");
        }
        else
        {
            if (this->m_presenter.existSubtitle())
                desc += tr("가사 있음");
            else
                desc += tr("가사 없음");
        }

        desc += ")";

        this->m_presenter.showOptionDesc(desc);
    }

    if (this->m_disableLastPosTmp)
    {
        this->m_disableLastPosTmp = false;
        this->m_gotoTimeTmp = 0.0;
    }

    return success;
}

void RenderScreen::pause()
{
    if (this->m_presenter.isRunning())
    {
        this->m_presenter.pause();
        this->m_status = Paused;
    }
    else
    {
        this->m_status = Stopped;
    }

    this->callChanged();

    this->m_presenter.showOptionDesc(tr("일시정지"));
}

void RenderScreen::resume()
{
    if (this->m_presenter.isRunning())
    {
        this->m_presenter.resume();
        this->m_status = Playing;
    }
    else
    {
        this->m_status = Stopped;
    }

    this->callChanged();

    this->m_presenter.showOptionDesc(tr("재생"));
}

void RenderScreen::stop()
{
    this->updateLastPlay();

    this->m_presenter.stop();
    this->m_status = Stopped;

    this->callChanged();
}

void RenderScreen::close()
{
    RenderScreen::m_selfLock.lock();
    RenderScreen::m_self = nullptr;
    RenderScreen::m_selfLock.unlock();

    if (!this->m_filePath.isEmpty())
    {
        this->updateLastPlay();

        this->m_presenter.close();
        this->m_filePath.clear();
        this->m_unique = QUuid();

        this->setScreenKeepOn(false);

        this->m_status = Stopped;
        this->callChanged();
    }
}

void RenderScreen::closeAndGoBack()
{
    this->m_renderer.scheduleCleanup();
}

void RenderScreen::navigate(double distance)
{
    if (!this->hasDuration())
        return;

    double current = this->m_presenter.getCurrentPosition();

    if (this->m_status == Ended)
    {
        this->m_status = Paused;
        this->callChanged();
    }

    double dist = current + distance;
    double duration = this->m_presenter.getDuration();

    if (dist < 0.0)
        dist = 0.0;
    else if (dist > duration)
        dist = duration;

    this->seek(dist, false);

    QString desc;
    QString time;

    if (this->isMovieSubtitleVisiable() && this->isSeekKeyFrame())
    {
        if (distance > 0.0)
            desc = tr("앞으로 %1초 키프레임 이동");
        else
            desc = tr("뒤로 %1초 키프레임 이동");
    }
    else
    {
        if (distance > 0.0)
            desc = tr("앞으로 %1초 이동");
        else
            desc = tr("뒤로 %1초 이동");
    }

    ConvertingUtils::getTimeString((uint32_t)dist, ConvertingUtils::TIME_HH_MM_SS, &time);

    desc = desc.arg(fabs(distance));
    desc += QString(" (%1, %2%)").arg(time).arg(dist / duration * 100, 0, 'f', 2);

    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::seek(double time, bool any)
{
    this->m_presenter.seek(time, any);

    double current = this->m_presenter.getCurrentPosition();
    double max = this->m_presenter.getDuration();

    if (current < max && this->m_status == Ended)
    {
        this->m_status = Paused;
        this->callChanged();
    }
}

void RenderScreen::setScreenKeepOn(bool on)
{
    if (on)
        this->m_screenSaverPreventer.disable();
    else
        this->m_screenSaverPreventer.enable();
}

bool RenderScreen::setKeepActive(bool on)
{
#if defined Q_OS_ANDROID
    QAndroidJniObject activity = QtAndroid::androidActivity();

    if (activity.isValid())
    {
        if (on)
            activity.callMethod<void>("setKeepActive");
        else
            activity.callMethod<void>("setKeepInActive");

        return true;
    }

    return false;
#else
    (void)on;
    return false;
#endif
}

bool RenderScreen::setScreenOrientation(bool landscape)
{
#ifdef Q_OS_ANDROID
    QAndroidJniObject activity = QtAndroid::androidActivity();
    bool success = false;

    if (activity.isValid())
    {
        if (landscape)
            success = activity.callMethod<jboolean>("setOrientationLandscape");
        else
            success = activity.callMethod<jboolean>("setOrientationPortrait");

        if (success)
            this->m_isLandscape = landscape;

        return success;
    }

    return success;
#else
    (void)landscape;
    return false;
#endif
}

bool RenderScreen::setScreenOrientationRestore()
{
#ifdef Q_OS_ANDROID
    QAndroidJniObject activity = QtAndroid::androidActivity();
    bool success = false;

    if (activity.isValid())
    {
        success = activity.callMethod<jboolean>("setOrientationRestore");

        if (success)
            this->m_isLandscape = false;

        return success;
    }

    return success;
#else
    return false;
#endif
}

bool RenderScreen::getScreenOrientation() const
{
    return this->m_isLandscape;
}

bool RenderScreen::isTablet() const
{
#ifdef Q_OS_ANDROID
    QAndroidJniObject activity = QtAndroid::androidActivity();

    if (activity.isValid())
        return activity.callMethod<jboolean>("isTablet");

    return false;
#else
    return true;
#endif
}

bool RenderScreen::wantToPlay() const
{
    return this->m_wantToPlay;
}

void RenderScreen::setWantToPlay(bool want)
{
    this->m_wantToPlay = want;
}

bool RenderScreen::disableLastPosTmp() const
{
    return this->m_disableLastPosTmp;
}

void RenderScreen::setDisableLastPosTmp(bool disable)
{
    this->m_disableLastPosTmp = disable;
}

double RenderScreen::gotoTimeTmp() const
{
    return this->m_gotoTimeTmp;
}

void RenderScreen::setGotoTimeTmp(double time)
{
    this->m_gotoTimeTmp = time;
}

void RenderScreen::sync()
{
    AnyVODWindow *window = (AnyVODWindow*)this->window();

    if (!window)
        return;

    if (this->m_wantToPlay)
    {
        this->m_wantToPlay = false;

        if (!this->playCurrent())
        {
            emit this->failedPlay();
            emit this->screenUpdated();
        }
    }

    QSize viewPort = window->size() * window->devicePixelRatio();

    this->m_renderer.setViewPortSize(viewPort, window);
}

void RenderScreen::handleWindowChanged(QQuickWindow *window)
{
    if (window)
    {
        connect(window, &QQuickWindow::beforeSynchronizing, this, &RenderScreen::sync, Qt::DirectConnection);
        connect(window, &QQuickWindow::sceneGraphInvalidated, &this->m_renderer, &GLRenderer::cleanup, Qt::DirectConnection);
        connect(window, &QQuickWindow::beforeRendering, &this->m_renderer, &GLRenderer::paint, Qt::DirectConnection);
        connect(this, &RenderScreen::screenUpdated, window, &QQuickWindow::update);

        this->m_playList = this->findChild<PlayListModel*>("playListModel");
        this->m_empty = this->findChild<QQuickItem*>("empty");

        this->m_lyrics1 = this->findChild<QQuickItem*>("lyrics1");
        this->m_lyrics2 = this->findChild<QQuickItem*>("lyrics2");
        this->m_lyrics3 = this->findChild<QQuickItem*>("lyrics3");

        this->m_presenter.setDevicePixelRatio(window->devicePixelRatio());

        window->setColor(Qt::black);
    }
}

void RenderScreen::handleApplicationStateChanged(Qt::ApplicationState state)
{
    switch (state)
    {
        case Qt::ApplicationSuspended:
        {
            this->saveSettings();
            this->m_appSuspended = true;
            Q_FALLTHROUGH();
        }
        case Qt::ApplicationHidden:
        case Qt::ApplicationInactive:
        {
            if (this->m_presenter.isVideo() && this->m_status == Playing && !this->m_pausedByInactive)
            {
                this->pause();
                this->m_pausedByInactive = true;
            }

            break;
        }
        case Qt::ApplicationActive:
        {
            if (this->m_presenter.isVideo() && this->m_status == Paused)
            {
                if (this->m_pausedByInactive)
                {
                    this->resume();
                    this->m_pausedByInactive = false;
                }
#ifdef Q_OS_IOS
                if (this->m_appSuspended && this->m_presenter.isOpenedHWDecoder())
                    this->recover();
#endif
                this->m_appSuspended = false;
            }

            break;
        }
    }
}

void RenderScreen::handleExitFromRenderer()
{
    this->close();

    this->m_status = Exit;
    this->callChanged();
}

#ifdef Q_OS_IOS
void CALLBACK RenderScreen::iosNotifyCallback(DWORD status)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return;

    if (status == BASS_IOSNOTIFY_INTERRUPT)
    {
        if (screen->m_isPausedByIOSNotify)
            return;

        if (screen->getStatus() == RenderScreen::Playing || screen->getStatus() == RenderScreen::Started)
        {
            screen->m_isPausedByIOSNotify = true;
            screen->pause();
        }
    }
    else if (status == BASS_IOSNOTIFY_INTERRUPT_END)
    {
        if (!screen->m_isPausedByIOSNotify)
            return;

        if (screen->getStatus() == RenderScreen::Paused)
        {
            screen->m_isPausedByIOSNotify = false;
            screen->resume();
        }
    }
}
#endif

void RenderScreen::handlePlayingEvent()
{
#ifdef Q_OS_IOS
        if (this->isAudio())
            IOSDelegate::updateNowPlayingInfo();
#endif
        emit this->playing();
}

void RenderScreen::handleEmptyBufferEvent(QEvent *event)
{
    EmptyBufferEvent *e = (EmptyBufferEvent*)event;

    this->m_empty->setProperty("visible", e->isEmpty());
    this->m_empty->setProperty("running", e->isEmpty());
}

void RenderScreen::handleAudioSubtitleEvent(QEvent *event)
{
    if (!this->m_presenter.existExternalSubtitle())
        return;

    AudioSubtitleEvent *e = (AudioSubtitleEvent*)event;
    QVector<Lyrics> lines = e->getLines();
    QVector<Lyrics> mergedLines;
    QVector<Lyrics> *realLine = nullptr;
    QVector<QQuickItem*> outputs;
    int i;

    outputs.append(this->m_lyrics1);
    outputs.append(this->m_lyrics2);
    outputs.append(this->m_lyrics3);

    if (lines.count() > 0 && lines[0].text.indexOf(NEW_LINE) >= 0)
    {
        QStringList texts = lines[0].text.split(NEW_LINE);
        Lyrics lyrics;

        for (i = 0; i < texts.count(); i++)
        {
            lyrics.color = lines[0].color;
            lyrics.text = texts[i];

            mergedLines.append(lyrics);
        }

        realLine = &mergedLines;
    }
    else
    {
        realLine = &lines;
    }

    for (i = 0; i < outputs.size() && i < realLine->size(); i++)
    {
        QColor color;

        if (this->existAudioSubtitleGender())
        {
            color = (*realLine)[i].color;
        }
        else
        {
            if (i == 0)
                color = Qt::white;
            else
                color = Qt::gray;
        }

        color.setAlphaF(this->getSubtitleOpaque());

        outputs[i]->setProperty("color", color);
        outputs[i]->setProperty("text", (*realLine)[i].text.trimmed().remove(QRegularExpression("\\r|\\n")));
    }

    for (; i < outputs.size(); i++)
        outputs[i]->setProperty("text", QString());
}

void RenderScreen::handleAbortEvent(QEvent *event)
{
    char buf[2048] = {0, };
    AbortEvent *e = (AbortEvent*)event;

    av_strerror(e->getReason(), buf, sizeof(buf));

    this->closeAndGoBack();
}

void RenderScreen::customEvent(QEvent *event)
{
    if (event->type() == PLAYING_EVENT)
        this->handlePlayingEvent();
    else if (event->type() == EMPTY_BUFFER_EVENT)
        this->handleEmptyBufferEvent(event);
    else if (event->type() == AUDIO_SUBTITLE_EVENT)
        this->handleAudioSubtitleEvent(event);
    else if (event->type() == ABORT_EVENT)
        this->handleAbortEvent(event);
    else
        QQuickItem::customEvent(event);
}

void RenderScreen::callChanged()
{
    this->m_statusUpdater.putStatus(this->getStatus());
}

void RenderScreen::playingCallback(void *userData)
{
    QGuiApplication::postEvent((QObject*)userData, new PlayingEvent());
}

void RenderScreen::endedCallback(void *userData)
{
    RenderScreen *parent = (RenderScreen*)userData;

    parent->m_status = Ended;
    parent->callChanged();
}

void RenderScreen::emptyBufferCallback(void *userData, bool empty)
{
    QGuiApplication::postEvent((QObject*)userData, new EmptyBufferEvent(empty));
}

void RenderScreen::audioSubtitleCallback(void *userData, const QVector<Lyrics> &lines)
{
    QGuiApplication::postEvent((QObject*)userData, new AudioSubtitleEvent(lines));
}

void RenderScreen::paintCallback(void *userData)
{
    RenderScreen *parent = (RenderScreen*)userData;
    AnyVODWindow *window = (AnyVODWindow*)parent->window();

    if (window)
    {
        window->setAcceptEvent(false);
        emit parent->screenUpdated();
    }
}

void RenderScreen::abortCallback(void *userData, int reason)
{
    QGuiApplication::postEvent((QObject*)userData, new AbortEvent(reason));
}

void RenderScreen::recoverCallback(void *userData)
{
    RenderScreen *parent = (RenderScreen*)userData;

    emit parent->recovered();
}

bool RenderScreen::isOpened() const
{
    return !this->m_filePath.isEmpty();
}

QString RenderScreen::getSubtitlePath() const
{
    return this->m_presenter.getSubtitlePath();
}

QString RenderScreen::getSubtitleFileName() const
{
    return QFileInfo(this->getSubtitlePath()).fileName();
}

QString RenderScreen::getDirectoryOfSubtitle() const
{
    return QFileInfo(this->getSubtitlePath()).absolutePath();
}

bool RenderScreen::openSubtitle(const QString &filePath)
{
    QFileInfo info(filePath);
    bool success = false;

    this->m_presenter.closeAllExternalSubtitles();
    success = this->m_presenter.openSubtitle(filePath, false);

    if (success)
    {
        if (this->isMovieSubtitleVisiable())
            this->m_presenter.showOptionDesc(tr("자막 열기 : %1").arg(info.fileName()));
        else
            this->m_presenter.showOptionDesc(tr("가사 열기 : %1").arg(info.fileName()));
    }

    return success;
}

bool RenderScreen::saveSubtitleAs(const QString &filePath)
{
    if (!this->existFileSubtitle())
        return false;

    QString desc;
    QFileInfo info(filePath);
    bool success = this->m_presenter.saveSubtitleAs(filePath);

    if (success)
    {
        if (this->isMovieSubtitleVisiable())
            desc = tr("자막이 저장 되었습니다 : %1");
        else
            desc = tr("가사가 저장 되었습니다 : %1");
    }
    else
    {
        if (this->isMovieSubtitleVisiable())
            desc = tr("자막이 저장 되지 않았습니다 : %1");
        else
            desc = tr("가사가 저장 되지 않았습니다 : %1");
    }

    this->m_presenter.showOptionDesc(desc.arg(info.fileName()));

    return success;
}

bool RenderScreen::saveSubtitle()
{
    return this->saveSubtitleAs(this->getSubtitlePath());
}

void RenderScreen::closeAllExternalSubtitles()
{
    this->m_presenter.closeAllExternalSubtitles();

    if (this->isMovieSubtitleVisiable())
        this->m_presenter.showOptionDesc(tr("외부 자막 닫기"));
    else
        this->m_presenter.showOptionDesc(tr("외부 가사 닫기"));
}

QString RenderScreen::getTitle() const
{
    return this->m_title;
}

QString RenderScreen::getArtist() const
{
    return this->m_presenter.getArtist();
}

QString RenderScreen::getFilePath() const
{
    return this->m_filePath;
}

QString RenderScreen::getRetainedFilePath() const
{
    return this->m_retainedFilePath;
}

void RenderScreen::tryOpenSubtitle(const QString &filePath)
{
    QFileInfo info(filePath);
    QString path = info.absolutePath();

    SeparatingUtils::appendDirSeparator(&path);

    this->m_presenter.closeAllExternalSubtitles();

    vector_wstring fileNames;
    SubtitleFileNameGenerator gen;
    bool opened = false;
    QStringList searchPaths = this->m_subtitleDirectory;

    if (this->m_priorSubtitleDirectory)
        searchPaths.append(path);
    else
        searchPaths.prepend(path);

    for (const QString &searchPath : searchPaths)
    {
        fileNames.clear();
        gen.getFileNamesWithExt(filePath.toStdWString(), &fileNames);

        for (size_t i = 0; i < fileNames.size(); i++)
        {
            QString subtitleFilePath = searchPath + QString::fromStdWString(fileNames[i]);

            if (this->m_presenter.openSubtitle(subtitleFilePath, false))
            {
                opened = true;
                break;
            }
        }

        if (!opened && this->m_useSearchSubtitleComplex)
        {
            fileNames.clear();
            gen.getFileNames(filePath.toStdWString(), &fileNames);

            for (size_t i = 0; i < fileNames.size(); i++)
            {
                QString subtitleFilePath = searchPath + QString::fromStdWString(fileNames[i]);

                if (this->m_presenter.openSubtitle(subtitleFilePath, false))
                {
                    opened = true;
                    break;
                }
            }
        }

        if (opened)
            break;
    }
}

void RenderScreen::setBluetoothHeadsetConnected(bool connected)
{
    this->m_presenter.setBluetoothHeadsetConnected(connected);
}

void RenderScreen::setBluetoothHeadsetSync(double sync)
{
    QString desc;

    this->m_presenter.setBluetoothHeadsetSync(sync);

    if (sync > 0.0)
        desc = tr("블루투스 싱크 %1초 빠르게");
    else
        desc = tr("블루투스 싱크 %1초 느리게");

    desc = desc.arg(fabs(sync));

    this->m_presenter.showOptionDesc(desc);
}

double RenderScreen::getBluetoothHeadsetSync() const
{
    return this->m_presenter.getBluetoothHeadsetSync();
}

void RenderScreen::retreiveLyrics(const QString &filePath)
{
    SubtitleImpl subtitle;
    wstring ret;

    if (!this->isEnableSearchLyrics())
        return;

    if (subtitle.getLyrics(filePath.toStdWString(), &ret))
    {
        QString tmpFilePath = QDir::tempPath();

        SeparatingUtils::appendDirSeparator(&tmpFilePath);
        tmpFilePath += "XXXXXX";
        tmpFilePath += ".lrc";

        QTemporaryFile tmpFile(tmpFilePath);

        if (tmpFile.open())
        {
            QFile file(tmpFile.fileName());

            if (file.open(QIODevice::WriteOnly | QIODevice::Text))
            {
                QTextStream stream(&file);

                stream.setCodec("UTF-8");
                stream.setGenerateByteOrderMark(true);

                stream << QString::fromStdWString(ret);

                file.close();
                tmpFile.close();

                if (this->openSubtitle(tmpFile.fileName()) && this->isAutoSaveSearchLyrics())
                {
                    QFileInfo info(filePath);
                    QString lyricsPath = info.absolutePath() + QDir::separator() + info.completeBaseName() + ".lrc";

                    this->m_presenter.saveSubtitleAs(lyricsPath);
                }
            }
        }
    }
}

void RenderScreen::retreiveSubtitleURL(const QString &filePath)
{
    SubtitleImpl subtitle;
    QFileInfo info(filePath);
    string url;

    if (!this->isEnableSearchSubtitle())
        return;

    if (subtitle.existSubtitle(filePath.toStdWString(), info.fileName().toUtf8().constData(), &url))
        this->m_presenter.setSubtitleURL(QString::fromStdString(url));
}

void RenderScreen::retreiveExternalSubtitle(const QString &filePath)
{
    if (!this->m_presenter.existExternalSubtitle())
    {
        if (this->m_presenter.isRemoteFile())
        {
            char proto[strlen(RemoteFileUtils::ANYVOD_PROTOCOL_NAME) + 1];
            char path[MAX_FILEPATH_CHAR_SIZE];
            QString realPath = SeparatingUtils::removeFFMpegSeparator(filePath);

            av_url_split(proto, (int)sizeof(proto), nullptr, 0, nullptr, 0, nullptr, path, (int)sizeof(path), realPath.toUtf8().constData());
            this->m_presenter.openRemoteSubtitle(QString::fromUtf8(path));

            if (!this->existSubtitle() && !this->m_presenter.isAudio() && this->isEnableSearchSubtitle())
            {
                QString url;

                Socket::getInstance().existSubtitleURL(QString::fromUtf8(path), true, nullptr, &url);
                this->m_presenter.setSubtitleURL(url);
            }
        }
        else if (this->m_presenter.isRemoteProtocol())
        {
            if (!this->m_playData.userData.isEmpty())
                this->m_presenter.openSubtitle(this->m_playData.userData, true);
        }
        else
        {
            if (this->m_presenter.isAudio())
                this->retreiveLyrics(filePath);
            else
                this->retreiveSubtitleURL(filePath);
        }
    }
}

int RenderScreen::getCurrentVideoStreamIndex() const
{
    return this->m_presenter.getCurrentVideoStreamIndex();
}

QVector<VideoStreamInfo> RenderScreen::getVideoStreamInfo() const
{
    QVector<VideoStreamInfo> ret;

    this->m_presenter.getVideoStreamInfo(&ret);
    return ret;
}

QStringList RenderScreen::getVideoStreamDescs() const
{
    QStringList descs;
    const auto infos = this->getVideoStreamInfo();

    for (const VideoStreamInfo &info : infos)
        descs.append(info.name);

    return descs;
}

QVector<int> RenderScreen::getVideoStreamIndice() const
{
    QVector<int> indice;
    const auto infos = this->getVideoStreamInfo();

    for (const VideoStreamInfo &info : infos)
        indice.append(info.index);

    return indice;
}

void RenderScreen::showDetail(bool show)
{
    this->m_presenter.showDetail(show);
}

bool RenderScreen::isShowDetail() const
{
    return this->m_presenter.isShowDetail();
}

void RenderScreen::showOptionDesc(const QString &desc)
{
    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::showSubtitle(bool show)
{
    this->m_presenter.showSubtitle(show);

    QString desc;

    if (this->isMovieSubtitleVisiable())
    {
        if (show)
            desc = tr("자막 보이기");
        else
            desc = tr("자막 숨기기");
    }
    else
    {
        if (show)
            desc = tr("가사 보이기");
        else
            desc = tr("가사 숨기기");
    }

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isShowSubtitle() const
{
    return this->m_presenter.isShowSubtitle();
}

void RenderScreen::setSearchSubtitleComplex(bool use)
{
    this->m_useSearchSubtitleComplex = use;

    QString desc;

    if (this->isMovieSubtitleVisiable())
    {
        if (use)
            desc = tr("고급 자막 검색 사용");
        else
            desc = tr("고급 자막 검색 사용 안 함");
    }
    else
    {
        if (use)
            desc = tr("고급 가사 검색 사용");
        else
            desc = tr("고급 가사 검색 사용 안 함");
    }

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::getSearchSubtitleComplex() const
{
    return this->m_useSearchSubtitleComplex;
}

bool RenderScreen::existSubtitle()
{
    return this->m_presenter.existSubtitle();
}

bool RenderScreen::existFileSubtitle()
{
    return this->m_presenter.existFileSubtitle();
}

bool RenderScreen::existExternalSubtitle()
{
    return this->m_presenter.existExternalSubtitle();
}

bool RenderScreen::existAudioSubtitle()
{
    return this->m_presenter.existAudioSubtitle();
}

bool RenderScreen::existAudioSubtitleGender() const
{
    return this->m_presenter.existAudioSubtitleGender();
}

QStringList RenderScreen::getSubtitleClasses()
{
    QStringList classNames;

    this->m_presenter.getSubtitleClasses(&classNames);
    return classNames;
}

QString RenderScreen::getCurrentSubtitleClass()
{
    QString className;

    this->m_presenter.getCurrentSubtitleClass(&className);
    return className;
}

bool RenderScreen::setCurrentSubtitleClass(const QString &className)
{
    QString desc;

    if (this->isMovieSubtitleVisiable())
        desc = tr("자막 언어 변경");
    else
        desc = tr("가사 언어 변경");

    desc += QString(" (%1)").arg(className);

    bool success = this->m_presenter.setCurrentSubtitleClass(className);

    this->m_presenter.showOptionDesc(desc);

    return success;
}

void RenderScreen::setVerticalSubtitlePosition(int pos)
{
    QString desc;

    this->m_presenter.setVerticalSubtitlePosition(pos);

    desc = tr("세로 자막 위치 : %1");

    this->m_presenter.showOptionDesc(desc.arg(this->getVerticalSubtitlePosition()));
}

void RenderScreen::setHorizontalSubtitlePosition(int pos)
{
    QString desc;

    this->m_presenter.setHorizontalSubtitlePosition(pos);

    desc = tr("가로 자막 위치 : %1");

    this->m_presenter.showOptionDesc(desc.arg(this->getHorizontalSubtitlePosition()));
}

int RenderScreen::getVerticalSubtitlePosition()
{
    return this->m_presenter.getVerticalSubtitlePosition();
}

int RenderScreen::getHorizontalSubtitlePosition()
{
    return this->m_presenter.getHorizontalSubtitlePosition();
}

void RenderScreen::setVertical3DSubtitleOffset(int offset)
{
    QString desc;

    this->m_presenter.setVertical3DSubtitleOffset(offset);

    desc = tr("세로 3D 자막 위치 : %1");

    this->m_presenter.showOptionDesc(desc.arg(this->getVertical3DSubtitleOffset()));
}

void RenderScreen::setHorizontal3DSubtitleOffset(int offset)
{
    QString desc;

    this->m_presenter.setHorizontal3DSubtitleOffset(offset);

    desc = tr("가로 3D 자막 위치 : %1");

    this->m_presenter.showOptionDesc(desc.arg(this->getHorizontal3DSubtitleOffset()));
}

int RenderScreen::getVertical3DSubtitleOffset()
{
    return this->m_presenter.getVertical3DSubtitleOffset();
}

int RenderScreen::getHorizontal3DSubtitleOffset()
{
    return this->m_presenter.getHorizontal3DSubtitleOffset();
}

void RenderScreen::setVirtual3DDepth(qreal depth)
{
    QString desc;

    this->m_presenter.setVirtual3DDepth(depth);

    desc = tr("가상 3D 입체감 : %1");

    this->m_presenter.showOptionDesc(desc.arg(this->getVirtual3DDepth(), 0, 'f', 2));
}

qreal RenderScreen::getVirtual3DDepth() const
{
    return this->m_presenter.getVirtual3DDepth();
}

qreal RenderScreen::getDefaultVirtual3DDepth() const
{
   return VideoRenderer::DEFAULT_VIRTUAL_3D_DEPTH;
}

void RenderScreen::setRepeatStart(double start)
{
    QString desc;
    QString time;

    ConvertingUtils::getTimeString(start, ConvertingUtils::TIME_HH_MM_SS_ZZZ, &time);

    desc = tr("구간 반복 시작 : %1").arg(time);

    this->m_presenter.setRepeatStart(start);
    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::setRepeatEnd(double end)
{
    QString desc;
    QString time;

    ConvertingUtils::getTimeString(end, ConvertingUtils::TIME_HH_MM_SS_ZZZ, &time);

    desc = tr("구간 반복 끝 : %1").arg(time);

    this->m_presenter.setRepeatEnd(end);
    this->m_presenter.setRepeatEnable(true);

    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::setRepeatEnable(bool enable)
{
    QString desc;
    QString en;
    QString start;
    QString end;

    if (enable)
        en = tr("구간 반복 활성화");
    else
        en = tr("구간 반복 비활성화");

    ConvertingUtils::getTimeString(this->getRepeatStart(), ConvertingUtils::TIME_HH_MM_SS_ZZZ, &start);
    ConvertingUtils::getTimeString(this->getRepeatEnd(), ConvertingUtils::TIME_HH_MM_SS_ZZZ, &end);

    if (start == end && enable)
        desc = tr("시작과 끝 시각이 같으므로 활성화 되지 않습니다");
    else
        desc = QString("%1 : %2 ~ %3").arg(en, start, end);

    this->m_presenter.setRepeatEnable(enable);
    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::getRepeatEnable() const
{
    return this->m_presenter.getRepeatEnable();
}

double RenderScreen::getRepeatStart() const
{
    return this->m_presenter.getRepeatStart();
}

double RenderScreen::getRepeatEnd() const
{
    return this->m_presenter.getRepeatEnd();
}

void RenderScreen::setSeekKeyFrame(bool keyFrame)
{
    QString desc;

    if (keyFrame)
        desc = tr("키프레임 단위로 이동 함");
    else
        desc = tr("키프레임 단위로 이동 안 함");

    this->m_presenter.setSeekKeyFrame(keyFrame);
    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isSeekKeyFrame() const
{
    return this->m_presenter.isSeekKeyFrame();
}

void RenderScreen::setSubtitleDirectory(const QStringList &paths, bool prior)
{
    this->m_subtitleDirectory = paths;
    this->m_priorSubtitleDirectory = prior;
}

void RenderScreen::getSubtitleDirectory(QStringList *path, bool *prior) const
{
    *path = this->m_subtitleDirectory;
    *prior = this->m_priorSubtitleDirectory;
}

void RenderScreen::set3DMethod(AnyVODEnums::Video3DMethod method)
{
    QString methodDesc = this->get3DMethodDesc(method);
    QString methodCategoryDesc = this->get3DMethodCategory(method);

    this->setScheduleRecomputeSubtitleSize();

    this->m_presenter.set3DMethod(method);

    QString desc;

    if (method == AnyVODEnums::V3M_NONE)
        desc = tr("3D 영상 (%1)").arg(methodDesc);
    else
        desc = tr("3D 영상 (%1 (%2))").arg(methodDesc, methodCategoryDesc);

    this->m_presenter.showOptionDesc(desc);
}

AnyVODEnums::Video3DMethod RenderScreen::get3DMethod() const
{
    return this->m_presenter.get3DMethod();
}

void RenderScreen::setSubtitle3DMethod(AnyVODEnums::Subtitle3DMethod method)
{
    QString methodDesc = this->getSubtitle3DMethodDesc(method);

    this->m_presenter.setSubtitle3DMethod(method);
    this->m_presenter.showOptionDesc(tr("3D 자막 (%1)").arg(methodDesc));
}

AnyVODEnums::Subtitle3DMethod RenderScreen::getSubtitle3DMethod() const
{
    return this->m_presenter.getSubtitle3DMethod();
}

void RenderScreen::setVRInputSource(AnyVODEnums::VRInputSource source)
{
    QString sourceDesc = this->getVRInputSourceDesc(source);

    this->m_presenter.setVRInputSource(source);
    this->m_presenter.showOptionDesc(tr("VR 입력 영상 (%1)").arg(sourceDesc));

    emit this->vrInputSourceChanged();
}

int RenderScreen::getVRInputSource() const
{
    return this->m_presenter.getVRInputSource();
}

void RenderScreen::useHeadTracking(bool use)
{
    QString desc;

    this->m_useHeadTracking = use;

    if (use)
        desc = tr("헤드 트래킹 사용 함");
    else
        desc = tr("헤드 트래킹 사용 안 함");

    this->m_presenter.showOptionDesc(desc);

    emit this->vrInputSourceChanged();
}

bool RenderScreen::isUseHeadTracking() const
{
    return this->m_useHeadTracking;
}

void RenderScreen::useDistortion(bool use)
{
    QString desc;

    this->m_presenter.useDistortion(use);

    if (use)
        desc = tr("VR 왜곡 보정 사용 함");
    else
        desc = tr("VR 왜곡 보정 사용 안 함");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUseDistortion() const
{
    return this->m_presenter.isUseDistortion();
}

void RenderScreen::setBarrelDistortionCoefficients(const QVector2D &coefficients)
{
    QString desc = tr("평면 VR 왜곡 보정 계수 설정 (k1 : %1, k2 : %2)");

    this->m_presenter.setBarrelDistortionCoefficients(coefficients);

    desc = desc.arg(coefficients.x()).arg(coefficients.y());

    this->m_presenter.showOptionDesc(desc);
}

QVector2D RenderScreen::getBarrelDistortionCoefficients() const
{
    return this->m_presenter.getBarrelDistortionCoefficients();
}

void RenderScreen::setPincushionDistortionCoefficients(const QVector2D &coefficients)
{
    QString desc = tr("360도 VR 왜곡 보정 계수 설정 (k1 : %1, k2 : %2)");

    this->m_presenter.setPincushionDistortionCoefficients(coefficients);

    desc = desc.arg(coefficients.x()).arg(coefficients.y());

    this->m_presenter.showOptionDesc(desc);
}

QVector2D RenderScreen::getPincushionDistortionCoefficients() const
{
    return this->m_presenter.getPincushionDistortionCoefficients();
}

void RenderScreen::setDistortionLensCenter(const QVector2D &lensCenter)
{
    QString desc = tr("VR 렌즈 센터 설정 (X : %1, Y : %2)");

    this->m_presenter.setDistortionLensCenter(lensCenter);

    desc = desc.arg(lensCenter.x()).arg(lensCenter.y());

    this->m_presenter.showOptionDesc(desc);
}

QVector2D RenderScreen::getDistortionLensCenter() const
{
    return this->m_presenter.getDistortionLensCenter();
}

void RenderScreen::set360DegreeAngles(const QVector3D &angles)
{
    Camera::getInstance().setAngles(QPointF(angles.x(), angles.y()));
    this->m_presenter.setSensorRotation(angles.z());
}

void RenderScreen::reset360DegreeAngles()
{
    Camera::getInstance().resetAngles();
    this->m_presenter.setSensorRotation(0.0);
}

void RenderScreen::setCameraAngles(const QPointF &angles)
{
    Camera::getInstance().setAngles(angles);
}

QPointF RenderScreen::getCameraAngles()
{
    return Camera::getInstance().getAngles();
}

void RenderScreen::useNormalizer(bool use)
{
    this->m_presenter.useNormalizer(use);

    QString desc;

    if (use)
        desc = tr("노멀라이저 켜짐");
    else
        desc = tr("노멀라이저 꺼짐");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUsingNormalizer() const
{
    return this->m_presenter.isUsingNormalizer();
}

void RenderScreen::useEqualizer(bool use)
{
    this->m_presenter.useEqualizer(use);

    QString desc;

    if (use)
        desc = tr("이퀄라이저 켜짐");
    else
        desc = tr("이퀄라이저 꺼짐");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUsingEqualizer() const
{
    return this->m_presenter.isUsingEqualizer();
}

void RenderScreen::useLowerMusic(bool use)
{
    this->m_presenter.useLowerMusic(use);

    QString desc;

    if (use)
        desc = tr("음악 줄임 켜짐");
    else
        desc = tr("음악 줄임 꺼짐");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUsingLowerMusic() const
{
    return this->m_presenter.isUsingLowerMusic();
}

void RenderScreen::setSubtitleOpaque(float opaque)
{
    QString desc;

    if (this->isMovieSubtitleVisiable())
        desc = tr("자막 투명도");
    else
        desc = tr("가사 투명도");

    opaque = FloatPointUtils::zeroDouble(opaque);

    if (opaque < 0.0f)
        opaque = 0.0f;

    if (opaque > 1.0f)
        opaque = 1.0f;

    this->m_presenter.setSubtitleOpaque(opaque);
    this->m_presenter.showOptionDesc(QString("%1 (%2%)").arg(desc).arg((int)(ceil(opaque * 100))));
}

float RenderScreen::getSubtitleOpaque() const
{
    return this->m_presenter.getSubtitleOpaque();
}

float RenderScreen::getSubtitleSize() const
{
    if (FloatPointUtils::zeroDouble(this->m_subtitleSize) >= 0.0f)
        return this->m_subtitleSize;
    else
        return this->m_presenter.getSubtitleSize();
}

void RenderScreen::setSubtitleSize(float size)
{
    QString desc = tr("자막 크기");

    size = FloatPointUtils::zeroDouble(size);

    if (size < 0.0f)
        size = 0.0f;

    this->m_presenter.setSubtitleSize(size);
    this->m_presenter.showOptionDesc(QString("%1 (%2%)").arg(desc).arg((int)(ceil(size * 100))));
}

void RenderScreen::setScheduleRecomputeSubtitleSize()
{
    this->m_presenter.scheduleRecomputeSubtitleSize();
}

void RenderScreen::saveSubtitleSize(float size)
{
    this->m_subtitleSize = size;
    this->applySubtitleSize();
}

void RenderScreen::applySubtitleSize()
{
    if (FloatPointUtils::zeroDouble(this->m_subtitleSize) >= 0.0)
    {
        this->setScheduleRecomputeSubtitleSize();
        this->setSubtitleSize(this->m_subtitleSize);

        this->m_subtitleSize = -1.0f;
    }
}

void RenderScreen::useHWDecoder(bool enable)
{
    QString desc;

    this->m_presenter.useHWDecoder(enable);

    if (enable)
    {
        desc = tr("하드웨어 디코더 사용 함");

        if (!this->m_presenter.isOpenedHWDecoder())
            desc += tr(" (그래픽 카드 또는 코덱이 지원하지 않을 경우 활성화가 안 될 수 있습니다)");
    }
    else
    {
        desc = tr("하드웨어 디코더 사용 안 함");
    }

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUseHWDecoder() const
{
    return this->m_presenter.isUseHWDecoder();
}

void RenderScreen::useLowQualityMode(bool enable)
{
    QString desc;

    this->m_presenter.useLowQualityMode(enable);

    if (enable)
        desc = tr("저화질 모드 사용 함");
    else
        desc = tr("저화질 모드 사용 안 함");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUseLowQualityMode() const
{
    return this->m_presenter.isUseLowQualityMode();
}

void RenderScreen::useFrameDrop(bool enable)
{
    QString desc;

    this->m_presenter.useFrameDrop(enable);

    if (enable)
        desc = tr("프레임 드랍 사용 함");
    else
        desc = tr("프레임 드랍 사용 안 함");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUseFrameDrop() const
{
    return this->m_presenter.isUseFrameDrop();
}

void RenderScreen::useBufferingMode(bool enable)
{
    QString desc;

    this->m_presenter.useBufferingMode(enable);

    if (enable)
        desc = tr("버퍼링 모드 사용 함");
    else
        desc = tr("버퍼링 모드 사용 안 함");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUseBufferingMode() const
{
    return this->m_presenter.isUseBufferingMode();
}

void RenderScreen::use3DFull(bool enable)
{
    this->setScheduleRecomputeSubtitleSize();
    this->m_presenter.use3DFull(enable);

    QString desc;

    if (enable)
        desc = tr("3D 전체 해상도 사용");
    else
        desc = tr("3D 전체 해상도 사용 안 함");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUse3DFull() const
{
    return this->m_presenter.isUse3DFull();
}

void RenderScreen::useLowerVoice(bool use)
{
    this->m_presenter.useLowerVoice(use);

    QString desc;

    if (use)
        desc = tr("음성 줄임 켜짐");
    else
        desc = tr("음성 줄임 꺼짐");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUsingLowerVoice() const
{
    return this->m_presenter.isUsingLowerVoice();
}

void RenderScreen::useHigherVoice(bool use)
{
    this->m_presenter.useHigherVoice(use);

    QString desc;

    if (use)
        desc = tr("음성 강조 켜짐");
    else
        desc = tr("음성 강조 꺼짐");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUsingHigherVoice() const
{
    return this->m_presenter.isUsingHigherVoice();
}

bool RenderScreen::setPreAmp(float dB)
{
    return this->m_presenter.setPreAmp(dB);
}

float RenderScreen::getPreAmp() const
{
    return this->m_presenter.getPreAmp();
}

bool RenderScreen::setEqualizerGain(int band, float gain)
{
    return this->m_presenter.setEqualizerGain(band, gain);
}

float RenderScreen::getEqualizerGain(int band) const
{
    return this->m_presenter.getEqualizerGain(band);
}

int RenderScreen::getBandCount() const
{
    return this->m_presenter.getBandCount();
}

bool RenderScreen::recover()
{
    return this->m_presenter.recover(this->m_presenter.getCurrentPosition());
}

void RenderScreen::toggle()
{
    if (this->m_status == Paused || this->m_status == Ended)
        this->resume();
    else if (this->m_status == Playing)
        this->pause();
}

void RenderScreen::updateLastPlay()
{
    if (this->m_gotoLastPlay)
    {
        if (this->m_status != Stopped && this->m_status != Ended)
            this->m_lastPlay.update(SeparatingUtils::removeFFMpegSeparator(this->m_lastPlayPath), this->getCurrentPosition());
    }
    else
    {
        this->m_lastPlay.clear(SeparatingUtils::removeFFMpegSeparator(this->m_lastPlayPath));
    }
}

bool RenderScreen::updatePlayList(const QString &path)
{
    QVector<PlayItem> playItems;

#if defined Q_OS_RASPBERRY_PI
    if (DTVReader::determinDTV(path))
    {
        QVector<DTVReader::ChannelInfo> scanned;
        const DTVReader &reader = DTVReader::getInstance();

        reader.getScannedChannels(&scanned);

        for (const DTVReader::ChannelInfo &channelInfo : qAsConst(scanned))
        {
            QStringList urlList;
            QVector<PlayItem> single;
            QString url = DTVReader::makeDTVPath(channelInfo);

            urlList.append(url);

            single = PlayItemUtils::getPlayItemFromFileNames(urlList);

            if (single.isEmpty())
                continue;

            DTVReaderInterface::AdapterInfo info;

            if (reader.getAdapterInfo(channelInfo.adapter, &info))
            {
                if (channelInfo.name.isEmpty())
                {
                    single[0].title = QString("%1 (%2)")
                            .arg(channelInfo.channel)
                            .arg(info.name);
                }
                else
                {
                    single[0].title = QString("%1 (%2, %3)")
                            .arg(channelInfo.name)
                            .arg(channelInfo.channel)
                            .arg(info.name);
                }

                single[0].extraData.userData = url.toUtf8();
            }

            playItems += single;
        }
    }
    else if (RadioReader::determinRadio(path))
    {
        QVector<RadioReader::ChannelInfo> scanned;
        const RadioReader &reader = RadioReader::getInstance();

        reader.getScannedChannels(&scanned);

        QMap<int, RadioReaderInterface::AdapterInfo> adapterInfos;

        for (const RadioReader::ChannelInfo &channelInfo : qAsConst(scanned))
        {
            QStringList urlList;
            QVector<PlayItem> single;
            QString url = RadioReader::makeRadioPath(channelInfo);

            urlList.append(url);

            single = PlayItemUtils::getPlayItemFromFileNames(urlList);

            if (single.isEmpty())
                continue;

            RadioReaderInterface::AdapterInfo info;
            auto iter = adapterInfos.find(channelInfo.adapter);

            if (iter == adapterInfos.end())
            {
                if (reader.getAdapterInfo(channelInfo.adapter, &info))
                {
                    adapterInfos.insert(channelInfo.adapter, info);
                    iter = adapterInfos.find(channelInfo.adapter);
                }
            }

            if (iter != adapterInfos.end())
            {
                info = *iter;

                single[0].title = QString("%1 %2Hz (%3)")
                        .arg(reader.demodulatorTypeToString(channelInfo.type),
                             ConvertingUtils::valueToString(channelInfo.freq),
                             info.name);
                single[0].extraData.userData = url.toUtf8();
            }

            playItems += single;
        }
    }
    else
#endif
    {
        QFileInfo info(path);

        if (info.isDir())
        {
            QStringList filePathList = FileListUtils::getOnlyMediaLocalFileList(path, FileExtensions::MEDIA_EXTS_LIST, false);

            playItems = PlayItemUtils::getPlayItemFromFileNames(filePathList);

            this->m_playList->setName(info.fileName().trimmed());
            this->m_playList->setBaseURL(path);
        }
        else
        {
            QDir dir(info.absoluteDir());
            const QFileInfoList files = dir.entryInfoList(QDir::Files | QDir::NoDotAndDotDot, QDir::Name);

            for (const QFileInfo &file : files)
            {
                QString filePath = file.absoluteFilePath();
                QStringList filePathList = FileListUtils::getOnlyMediaLocalFileList(filePath, FileExtensions::MEDIA_EXTS_LIST, false);

                if (!filePathList.empty())
                    playItems += PlayItemUtils::getPlayItemFromFileNames(filePathList);
            }
        }
    }

    this->setPlayList(playItems);

    for (int i = 0; i < this->m_playList->getCount(); i++)
    {
        PlayItem item = this->m_playList->getPlayItem(i);

        if (path == item.path)
        {
            this->m_unique = item.unique;
            this->m_playList->setCurrentIndexByUnique(this->m_unique);

            break;
        }
    }

    return !playItems.isEmpty();
}

QString RenderScreen::getURLPickerPlayItems(const QString &address, QVector<PlayItem> *itemVector, QString *playListURL)
{
    QVector<URLPickerInterface::Item> urls;
    URLPicker picker;
    QString title;

    urls = picker.pickURL(address);

    if (picker.isPlayList())
        title = picker.getPlayListTitle();
    else if (!urls.isEmpty())
        title = urls.first().title;

    if (urls.isEmpty())
    {
        QStringList list;

        list.append(address);
        *itemVector = PlayItemUtils::getPlayItemFromFileNames(list);
    }
    else
    {
        bool isPlayList = picker.isPlayList();

        if (isPlayList)
        {
            *playListURL = picker.getPlayListURL();

            QVector<URLPickerInterface::Item> playlist;

            if (urls.length() > 0)
                playlist = picker.pickURL(urls.first().url);

            urls = playlist.mid(0, 1) + urls.mid(1);
        }

        *itemVector = PlayItemUtils::getPlayItemFromURLPicker(urls, isPlayList);
    }

    return title;
}

void RenderScreen::setPlayList(const QVector<PlayItem> &list)
{
    this->addToPlayList(list);
}

void RenderScreen::addToPlayList(const QVector<PlayItem> &list)
{
    this->m_playList->addPlayList(list);
    this->m_playList->setCurrentIndexByUnique(this->m_unique);
}

bool RenderScreen::isRotatable() const
{
    bool ret = !RotationUtils::isPortrait(this->m_presenter.getRotation());

    if (ret)
    {
        int width;
        int height;

        if (this->m_presenter.getFrameSize(&width, &height))
            ret = width > height;
    }

    return ret && !this->m_presenter.isAudio();
}

#ifdef Q_OS_ANDROID
void RenderScreen::callActivityEvent(const QString &funcName) const
{
    QAndroidJniObject activity = QtAndroid::androidActivity();

    if (activity.isValid())
        activity.callMethod<void>(funcName.toLatin1());
}
#endif

bool RenderScreen::isPlayOrPause() const
{
    return this->m_status == Playing || this->m_status == Paused;
}

float RenderScreen::getTempo() const
{
    return this->m_presenter.getTempo();
}

void RenderScreen::setTempo(float percent)
{
    QString desc;

    if (FloatPointUtils::zeroDouble(percent) == 0.0)
    {
        percent = 0.0f;
        desc = tr("재생 속도 초기화");
    }
    else
    {
        desc = tr("재생 속도 : %1배").arg(1.0f + percent / 100.0f);
    }

    this->m_presenter.setTempo(percent);
    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::setUserAspectRatio(UserAspectRatioInfo &ratio)
{
    QString desc;

    if (ratio.use)
    {
        desc = tr("화면 비율 사용 함");

        if (ratio.fullscreen)
            desc += tr(" (화면 채우기)");
        else
            desc += QString(" (%1:%2)").arg(ratio.width).arg(ratio.height);
    }
    else
    {
        desc = tr("화면 비율 사용 안 함");
    }

    this->setScheduleRecomputeSubtitleSize();

    this->m_presenter.setUserAspectRatio(ratio);
    this->m_presenter.showOptionDesc(desc);
}

QString RenderScreen::getGOMSubtitleURL() const
{
    QString ret;

    this->m_presenter.getSubtitleURL(&ret);

    QUrl encode(ret, QUrl::StrictMode);
    QString encoded = encode.toEncoded();

    return encoded;
}

int RenderScreen::getCurrentAudioStreamIndex() const
{
    return this->m_presenter.getCurrentAudioStreamIndex();
}

QVector<AudioStreamInfo> RenderScreen::getAudioStreamInfo() const
{
    QVector<AudioStreamInfo> ret;

    this->m_presenter.getAudioStreamInfo(&ret);
    return ret;
}

QStringList RenderScreen::getAudioStreamDescs() const
{
    QStringList descs;
    const auto infos = this->getAudioStreamInfo();

    for (const AudioStreamInfo &info : infos)
        descs.append(info.name);

    return descs;
}

QVector<int> RenderScreen::getAudioStreamIndice() const
{
    QVector<int> indice;
    const auto infos = this->getAudioStreamInfo();

    for (const AudioStreamInfo &info : infos)
        indice.append(info.index);

    return indice;
}

void RenderScreen::setDeinterlaceMethod(AnyVODEnums::DeinterlaceMethod method)
{
    QString methodDesc;

    this->m_presenter.getDeinterlacer().setMethod(method);

    switch (method)
    {
        case AnyVODEnums::DM_AUTO:
            methodDesc = tr("자동 판단");
            break;
        case AnyVODEnums::DM_USE:
            methodDesc = tr("항상 사용");
            break;
        case AnyVODEnums::DM_NOUSE:
            methodDesc = tr("사용 안 함");
            break;
        default:
            break;
    }

    this->m_presenter.showOptionDesc(tr("디인터레이스 (%1)").arg(methodDesc));
}

void RenderScreen::setDeinterlaceAlgorithm(AnyVODEnums::DeinterlaceAlgorithm algorithm)
{
    QString algorithmDesc = this->getDeinterlaceDesc(algorithm);

    this->m_presenter.setDeinterlacerAlgorithm(algorithm);
    this->m_presenter.showOptionDesc(tr("디인터레이스 알고리즘 (%1)").arg(algorithmDesc));
}

AnyVODEnums::DeinterlaceMethod RenderScreen::getDeinterlaceMethod()
{
    return this->m_presenter.getDeinterlacer().getMethod();
}

AnyVODEnums::DeinterlaceAlgorithm RenderScreen::getDeinterlaceAlgorithm()
{
    return this->m_presenter.getDeinterlacer().getAlgorithm();
}

void RenderScreen::setHAlign(AnyVODEnums::HAlignMethod align)
{
    QString desc;
    QString alignDesc;

    if (this->isMovieSubtitleVisiable())
        desc = tr("자막 가로 정렬 변경");
    else
        desc = tr("가사 가로 정렬 변경");

    switch (align)
    {
        case AnyVODEnums::HAM_AUTO:
            alignDesc = tr("자동 정렬");
            break;
        case AnyVODEnums::HAM_LEFT:
            alignDesc = tr("왼쪽 정렬");
            break;
        case AnyVODEnums::HAM_RIGHT:
            alignDesc = tr("오른쪽 정렬");
            break;
        case AnyVODEnums::HAM_MIDDLE:
            alignDesc = tr("가운데 정렬");
            break;
        case AnyVODEnums::HAM_NONE:
            alignDesc = tr("기본 정렬");
            break;
        default:
            break;
    }

    desc += QString(" (%1)").arg(alignDesc);

    this->m_presenter.setHAlign(align);
    this->m_presenter.showOptionDesc(desc);
}

AnyVODEnums::HAlignMethod RenderScreen::getHAlign() const
{
    return this->m_presenter.getHAlign();
}

void RenderScreen::setVAlign(AnyVODEnums::VAlignMethod align)
{
    QString desc;
    QString alignDesc;

    if (this->isMovieSubtitleVisiable())
        desc = tr("자막 세로 정렬 변경");
    else
        desc = tr("가사 세로 정렬 변경");

    switch (align)
    {
        case AnyVODEnums::VAM_TOP:
            alignDesc = tr("상단 정렬");
            break;
        case AnyVODEnums::VAM_MIDDLE:
            alignDesc = tr("가운데 정렬");
            break;
        case AnyVODEnums::VAM_BOTTOM:
            alignDesc = tr("하단 정렬");
            break;
        case AnyVODEnums::VAM_NONE:
            alignDesc = tr("기본 정렬");
            break;
        default:
            break;
    }

    desc += QString(" (%1)").arg(alignDesc);

    this->m_presenter.setVAlign(align);
    this->m_presenter.showOptionDesc(desc);
}

AnyVODEnums::VAlignMethod RenderScreen::getVAlign() const
{
    return this->m_presenter.getVAlign();
}

void RenderScreen::subtitleSync(double value)
{
    double current = this->m_presenter.getSubtitleSync();
    double target = value + current;

    this->m_presenter.setSubtitleSync(target);

    QString desc;

    if (this->isMovieSubtitleVisiable())
    {
        if (value > 0.0)
            desc = tr("자막 싱크 %1초 빠르게");
        else
            desc = tr("자막 싱크 %1초 느리게");
    }
    else
    {
        if (value > 0.0)
            desc = tr("가사 싱크 %1초 빠르게");
        else
            desc = tr("가사 싱크 %1초 느리게");
    }

    desc = desc.arg(fabs(value));

    target = FloatPointUtils::zeroDouble(target);
    desc += tr(" (%1초)").arg(target);

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isEnableSearchSubtitle() const
{
    return this->m_presenter.isEnableSearchSubtitle();
}

bool RenderScreen::isEnableSearchLyrics() const
{
    return this->m_presenter.isEnableSearchLyrics();
}

void RenderScreen::enableSearchSubtitle(bool enable)
{
    QString desc;

    if (enable)
        desc = tr("자막 찾기 켜짐");
    else
        desc = tr("자막 찾기 꺼짐");

    this->m_presenter.enableSearchSubtitle(enable);
    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::enableSearchLyrics(bool enable)
{
    QString desc;

    if (enable)
        desc = tr("가사 찾기 켜짐");
    else
        desc = tr("가사 찾기 꺼짐");

    this->m_presenter.enableSearchLyrics(enable);
    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::enableAutoSaveSearchLyrics(bool enable)
{
    QString desc;

    if (enable)
        desc = tr("찾은 가사 자동 저장 켜짐");
    else
        desc = tr("찾은 가사 자동 저장 꺼짐");

    this->m_presenter.enableAutoSaveSearchLyrics(enable);
    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isAutoSaveSearchLyrics() const
{
    return this->m_presenter.isAutoSaveSearchLyrics();
}

void RenderScreen::audioSync(double value)
{
    double current = this->m_presenter.getAudioSync();
    double target = value + current;

    this->m_presenter.setAudioSync(target);

    QString desc;

    if (value > 0.0)
        desc = tr("소리 싱크 %1초 빠르게");
    else
        desc = tr("소리 싱크 %1초 느리게");

    desc = desc.arg(fabs(value));

    target = FloatPointUtils::zeroDouble(target);
    desc += tr(" (%1초)").arg(target);

    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::rewind(double distance)
{
    this->navigate(-distance);
}

void RenderScreen::forward(double distance)
{
    this->navigate(distance);
}

int RenderScreen::getMaxVolume() const
{
    return this->m_presenter.getMaxVolume();
}

void RenderScreen::volume(int volume)
{
    this->m_presenter.volume((uint8_t)volume);

    int perVolume = (int)((float)volume / this->m_presenter.getMaxVolume() * 100);
    QString desc = tr("소리 (%1%)").arg(perVolume);

    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::mute(bool mute)
{
    this->m_presenter.mute(mute);

    QString desc;

    if (mute)
        desc = tr("소리 꺼짐");
    else
        desc = tr("소리 켜짐");

    this->m_presenter.showOptionDesc(desc);
}

int RenderScreen::getVolume() const
{
    return this->m_presenter.getVolume();
}

double RenderScreen::getDuration() const
{
    return this->m_presenter.getDuration();
}

double RenderScreen::getCurrentPosition()
{
    return this->m_presenter.getCurrentPosition();
}

bool RenderScreen::hasDuration() const
{
    return this->m_presenter.hasDuration();
}

RenderScreen::Status RenderScreen::getStatus()
{
    if (!this->m_presenter.isRunning() && this->m_status != Exit)
        this->m_status = Stopped;

    return this->m_status;
}

bool RenderScreen::canCapture() const
{
    return this->m_presenter.canCapture();
}

bool RenderScreen::isEnabledVideo() const
{
    return this->m_presenter.isEnabledVideo();
}

bool RenderScreen::isAudio() const
{
    return this->m_presenter.isAudio();
}

bool RenderScreen::isVideo() const
{
    return this->m_presenter.isVideo();
}

bool RenderScreen::isAlignable()
{
    return this->m_presenter.isAlignable();
}

bool RenderScreen::isMovieSubtitleVisiable() const
{
    return this->isVideo() || !this->isOpened();
}

void RenderScreen::showAlbumJacket(bool show)
{
    QString desc;

    if (show)
        desc = tr("앨범 자켓 보이기");
    else
        desc = tr("앨범 자켓 숨기기");

    this->m_presenter.showAlbumJacket(show);
    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isShowAlbumJacket() const
{
    return this->m_presenter.isShowAlbumJacket();
}

void RenderScreen::showCurrentPos(double pos, double initPos)
{
    QString time;
    QString desc = tr("이동 : %1 [%2]");
    double diff = pos - initPos;
    QString diffTime;

    ConvertingUtils::getTimeString((uint32_t)pos, ConvertingUtils::TIME_HH_MM_SS, &time);
    ConvertingUtils::getTimeString((uint32_t)qAbs(diff), ConvertingUtils::TIME_HH_MM_SS, &diffTime);

    if (diff >= 0.0)
        diffTime.prepend("+");
    else
        diffTime.prepend("-");

    this->m_presenter.showOptionDesc(desc.arg(time, diffTime));
}

void RenderScreen::enableGotoLastPos(bool enable, bool raw)
{
    this->m_gotoLastPlay = enable;

    if (!raw)
    {
        QString desc;

        if (enable)
            desc = tr("재생 위치 기억 켜짐");
        else
            desc = tr("재생 위치 기억 꺼짐");

        this->m_presenter.showOptionDesc(desc);
    }
}

bool RenderScreen::isGotoLastPos() const
{
    return this->m_gotoLastPlay;
}

void RenderScreen::setPlayingMethod(AnyVODEnums::PlayingMethod method)
{
    this->m_playingMethod = method;
}

void RenderScreen::selectPlayingMethod(int method)
{
    this->setPlayingMethod((AnyVODEnums::PlayingMethod)method);
}

AnyVODEnums::PlayingMethod RenderScreen::getPlayingMethod() const
{
    return this->m_playingMethod;
}

QString RenderScreen::get3DMethodDesc(int method) const
{
    return EnumsTranslator::getInstance().get3DMethodDesc((AnyVODEnums::Video3DMethod)method);
}

QString RenderScreen::get3DMethodCategory(int method) const
{
    return EnumsTranslator::getInstance().get3DMethodCategoryDesc((AnyVODEnums::Video3DMethod)method);
}

QString RenderScreen::getSubtitle3DMethodDesc(int method) const
{
    return EnumsTranslator::getInstance().getSubtitle3DMethodDesc((AnyVODEnums::Subtitle3DMethod)method);
}

QString RenderScreen::getAnaglyphAlgoritmDesc(int algorithm) const
{
    return EnumsTranslator::getInstance().getAnaglyphAlgoritmDesc((AnyVODEnums::AnaglyphAlgorithm)algorithm);
}

QString RenderScreen::getDeinterlaceDesc(int algorithm) const
{
    return EnumsTranslator::getInstance().getDeinterlaceDesc((AnyVODEnums::DeinterlaceAlgorithm)algorithm);
}

QString RenderScreen::getVRInputSourceDesc(int source) const
{
    return EnumsTranslator::getInstance().getVRInputSourceDesc((AnyVODEnums::VRInputSource)source);
}

QString RenderScreen::getDistortionAdjustModeDesc(int mode) const
{
    return EnumsTranslator::getInstance().getDistortionAdjustModeDesc((AnyVODEnums::DistortionAdjustMode)mode);
}

bool RenderScreen::addFOV(float value)
{
    return this->m_presenter.addFOV(value);
}

void RenderScreen::resetFOV()
{
    this->m_presenter.resetFOV();
}

void RenderScreen::rewindProper()
{
    if (this->isAudio())
        this->rewind5();
    else
        this->rewind10();
}

void RenderScreen::forwardProper()
{
    if (this->isAudio())
        this->forward5();
    else
        this->forward10();
}

void RenderScreen::rewind5()
{
    this->rewind(5.0);
}

void RenderScreen::forward5()
{
    this->forward(5.0);
}

void RenderScreen::rewind10()
{
    this->rewind(10.0);
}

void RenderScreen::forward10()
{
    this->forward(10.0);
}

bool RenderScreen::prev(bool check)
{
    if (check)
    {
        if (this->isPrevEnabled())
            return this->playPrev();
    }
    else
    {
        return this->playPrev();
    }

    return false;
}

bool RenderScreen::next(bool check)
{
    if (check)
    {
        if (this->isNextEnabled())
            return this->playNext();
    }
    else
    {
        return this->playNext();
    }

    return false;
}

void RenderScreen::selectVideoStream(int index)
{
    if (this->m_presenter.getCurrentVideoStreamIndex() != index)
    {
        if (!this->m_presenter.changeVideoStream(index))
            this->m_presenter.showOptionDesc(tr("영상을 변경 할 수 없습니다"));
    }
}

void RenderScreen::selectAudioStream(int index)
{
    if (this->m_presenter.getCurrentAudioStreamIndex() != index)
    {
        if (!this->m_presenter.changeAudioStream(index))
            this->m_presenter.showOptionDesc(tr("음성을 변경 할 수 없습니다"));
    }
}

void RenderScreen::selectDeinterlacerMethod(int method)
{
    this->setDeinterlaceMethod((AnyVODEnums::DeinterlaceMethod)method);
}

void RenderScreen::selectDeinterlacerAlgorithem(int algorithm)
{
    this->setDeinterlaceAlgorithm((AnyVODEnums::DeinterlaceAlgorithm)algorithm);
}

void RenderScreen::selectSubtitleHAlignMethod(int method)
{
    if (this->isAlignable())
        this->setHAlign((AnyVODEnums::HAlignMethod)method);
}

void RenderScreen::captureSingle()
{
    if (this->canCapture())
    {
        ScreenCaptureHelper &capHelper = this->m_renderer.getCaptureHelper();

        capHelper.setCaptureByOriginalSize(true);
        capHelper.setTotalCount(1);
        capHelper.start();
    }
}

void RenderScreen::selectCaptureExt(const QString &ext)
{
    this->m_renderer.getCaptureHelper().setExtension(ext.toUpper());
}

QString RenderScreen::getCaptureExt() const
{
    return this->m_renderer.getCaptureHelper().getExtention();
}

void RenderScreen::setCaptureDirectory(const QString &path)
{
    QString newPath = path;

    SeparatingUtils::appendDirSeparator(&newPath);
    this->m_renderer.getCaptureHelper().setSavePath(newPath);
}

QString RenderScreen::getCaptureDirectory() const
{
    return this->m_renderer.getCaptureHelper().getSavePath();
}

void RenderScreen::selectAudioDevice(int device)
{
    this->setAudioDevice(device);
}

void RenderScreen::enableSearchSubtitle()
{
    this->enableSearchSubtitle(!this->isEnableSearchSubtitle());

    if (this->isEnableSearchSubtitle())
    {
        this->retreiveExternalSubtitle(this->getFilePath());
        this->checkGOMSubtitle();
    }
}

void RenderScreen::enableSearchLyrics()
{
    this->enableSearchLyrics(!this->isEnableSearchLyrics());

    if (this->isEnableSearchLyrics())
    {
        this->retreiveExternalSubtitle(this->getFilePath());

        if (this->existAudioSubtitle())
        {
            this->setScreenKeepOn(true);

            if (!this->isTablet())
                this->setScreenOrientation(true);

            emit this->viewLyrics();
        }
    }
}

void RenderScreen::selectSubtitleVAlignMethod(int method)
{
    if (this->isAlignable())
        this->setVAlign((AnyVODEnums::VAlignMethod)method);
}

void RenderScreen::selectUserAspectRatio(int index)
{
    UserAspectRatioInfo ratio;
    UserAspectRatios::Item item = this->m_userAspectRatios.getRatio(index);

    if (item.isInvalid())
    {
        ratio.use = false;
    }
    else if (item.isFullScreen())
    {
        ratio.use = true;
        ratio.fullscreen = true;
    }
    else
    {
        ratio.use = true;

        ratio.width = item.width;
        ratio.height = item.height;
    }

    this->m_userAspectRatios.selectRatio(index);
    this->setUserAspectRatio(ratio);
}

int RenderScreen::getCurrentUserAspectRatioIndex() const
{
    return this->m_userAspectRatios.getSelectedRatioIndex();
}

int RenderScreen::getUserAspectRatioIndex() const
{
    return this->m_userAspectRatios.getUserRatioIndex();
}

void RenderScreen::setCustomUserAspectRatio(const QSizeF &size)
{
    UserAspectRatios::Item item = this->m_userAspectRatios.getUserRatio();

    item.width = size.width();
    item.height = size.height();

    this->m_userAspectRatios.setUserRatio(item);
}

QSizeF RenderScreen::getCustomUserAspectRatio() const
{
    const UserAspectRatios::Item item = this->m_userAspectRatios.getUserRatio();

    return QSizeF(item.width, item.height);
}

QStringList RenderScreen::getUserAspectRatioDescs() const
{
    QStringList descs;

    for (int i = 0; i < this->m_userAspectRatios.getCount(); ++i)
    {
        const UserAspectRatios::Item item = this->m_userAspectRatios.getRatio(i);
        QString desc;

        if (item.isInvalid())
            desc = tr("사용 안 함");
        else if (item.isFullScreen())
            desc = tr("화면 채우기");
        else if (i == this->m_userAspectRatios.getUserRatioIndex())
            desc = tr("사용자 지정 (%1:%2)").arg(item.width).arg(item.height);
        else
            desc = QString("%1:%2").arg(item.width).arg(item.height);

        descs.append(desc);
    }

    return descs;
}

void RenderScreen::useHWDecoder()
{
    this->useHWDecoder(!this->isUseHWDecoder());
}

void RenderScreen::select3DMethod(int method)
{
    this->set3DMethod((AnyVODEnums::Video3DMethod)method);
    ShaderCompositer::getInstance().set3DMethod((AnyVODEnums::Video3DMethod)method);
}

void RenderScreen::selectVRInputSource(int source)
{
    this->setVRInputSource((AnyVODEnums::VRInputSource)source);
}

void RenderScreen::closeExternalSubtitle()
{
    if (this->existExternalSubtitle())
    {
        this->closeAllExternalSubtitles();
        this->resetLyrics();
    }
}

void RenderScreen::showAlbumJacket()
{
    this->showAlbumJacket(!this->isShowAlbumJacket());
}

void RenderScreen::useFrameDrop()
{
    this->useFrameDrop(!this->isUseFrameDrop());
}

void RenderScreen::selectAnaglyphAlgorithm(int algorithm)
{
    ShaderCompositer::getInstance().setAnaglyphAlgorithm((AnyVODEnums::AnaglyphAlgorithm)algorithm);
}

void RenderScreen::select3DSubtitleMethod(int method)
{
    this->setSubtitle3DMethod((AnyVODEnums::Subtitle3DMethod)method);
}

void RenderScreen::use3DFull()
{
    if (this->get3DMethod() != AnyVODEnums::V3M_NONE)
        this->use3DFull(!this->isUse3DFull());
}

void RenderScreen::selectScreenRotationDegree(int degree)
{
    this->setScreenRotationDegree((AnyVODEnums::ScreenRotationDegree)degree);
}

void RenderScreen::useBufferingMode()
{
    this->useBufferingMode(!this->isUseBufferingMode());
}

bool RenderScreen::isPrevEnabled() const
{
    QQuickItem *prev = this->findChild<QQuickItem*>("prevButton");

    if (prev)
        return prev->isEnabled();

    return false;
}

bool RenderScreen::isNextEnabled() const
{
    QQuickItem *next = this->findChild<QQuickItem*>("nextButton");

    if (next)
        return next->isEnabled();

    return false;
}

bool RenderScreen::isValid() const
{
    return this->m_presenter.isValid();
}

bool RenderScreen::isLogined() const
{
    return Socket::getInstance().isLogined() && Socket::getStreamInstance().isLogined();
}

void RenderScreen::logout()
{
    QString error;

    Socket::getInstance().logout(&error);

    if (this->m_presenter.isRemoteFile())
        this->close();

    Socket::getStreamInstance().logout(&error);
}

bool RenderScreen::playNext()
{
    this->m_playList->increaseCurrentIndex();
    return this->playCurrent();
}

bool RenderScreen::playPrev()
{
    this->m_playList->decreaseCurrentIndex();
    return this->playCurrent();
}

bool RenderScreen::playCurrent()
{
    bool success = false;

    this->m_playList->adjustCurrentIndex();

    if (this->m_playList->exist())
    {
        PlayItem item = this->m_playList->getCurrentPlayItem();

        if (item.path.startsWith(RemoteFileUtils::ANYVOD_PROTOCOL))
        {
            if (!this->isLogined())
            {
                this->logout();
                emit this->needLogin();

                return true;
            }

            item.path += SeparatingUtils::FFMPEG_SEPARATOR;
            item.path += QString().setNum((qulonglong)&VirtualFile::getInstance());
        }
        else if (item.path.startsWith(DTVReader::DTV_PROTOCOL))
        {
            item.path += SeparatingUtils::FFMPEG_SEPARATOR;
            item.path += QString().setNum((qulonglong)&DTVReader::getInstance());
        }
        else if (item.path.startsWith(RadioReader::RADIO_PROTOCOL))
        {
            item.path += SeparatingUtils::FFMPEG_SEPARATOR;
            item.path += QString().setNum((qulonglong)&RadioReader::getInstance());
        }

        QVector<URLPickerInterface::Item> urls;
        URLPicker picker;
        QString path;
        int urlPikcerIndex = 0;

        urls = picker.pickURL(item.path);

        if (urls.isEmpty())
        {
            path = item.path;
        }
        else
        {
            urlPikcerIndex = this->m_playList->updateURLPickerData(item.unique, urls);

            path = urls[urlPikcerIndex].url;
            item.extraData.query = urls[urlPikcerIndex].query;
        }

        success = this->open(path, item.extraData, item.unique);

        if (!success && !urls.isEmpty() && ++urlPikcerIndex < urls.length())
        {
            if (this->m_playList->selectOtherQuality(urlPikcerIndex))
                success = this->playCurrent();
        }

        if (!success)
        {
            this->m_playList->decreaseCurrentIndex();

            if (this->m_playList->isVaildCurrentIndex())
                success = this->playCurrent();
        }
    }

    return success;
}

FilterGraph& RenderScreen::getFilterGraph()
{
    return this->m_presenter.getFilterGraph();
}

bool RenderScreen::configFilterGraph()
{
    return this->m_presenter.configFilterGraph();
}

int RenderScreen::getCurrentURLPickerIndex() const
{
    const PlayItem &item = this->m_playList->getCurrentPlayItem();

    return item.urlPickerIndex;
}

QString RenderScreen::getLargeCoverFilePath() const
{
    QString path = QStandardPaths::writableLocation(QStandardPaths::TempLocation) + QDir::separator();

    return path + "anyvod." + COMPANY + "large_cover.png";
}

PlayListModel *RenderScreen::getPlayList() const
{
    return this->m_playList;
}

RenderScreen* RenderScreen::getSelf()
{
    RenderScreen *self;

    RenderScreen::m_selfLock.lock();
    self = RenderScreen::m_self;
    RenderScreen::m_selfLock.unlock();

    return self;
}

void RenderScreen::scheduleRebuildShader()
{
    this->m_renderer.scheduleRebuildShader();
}

void RenderScreen::checkGOMSubtitle()
{
    QString url;

    url = this->getGOMSubtitleURL();

    if (url.count() > 0)
        emit this->showSubtitlePopup();
}

void RenderScreen::callStartedEvent()
{
    this->setScreenKeepOn(!this->isAudio() || this->existAudioSubtitle());

    if (!this->isTablet())
        this->setScreenOrientation(this->isRotatable() || this->existAudioSubtitle());

    this->setKeepActive(true);

    QQuickWindow *window = this->window();

    if (window)
        window->setClearBeforeRendering(false);

    this->setUseCustomControl(this->isEnabledVideo() && !this->isAudio());

#if !defined Q_OS_IOS
    this->setFullScreen();
#endif

    QString coverPath = this->getLargeCoverFilePath();
    QFile cover(coverPath);

    if (cover.exists())
        cover.remove();

    if (this->isAudio() && !this->m_presenter.isRemoteFile() && !this->m_presenter.isRadio())
    {
        FrameExtractor extractor;
        FrameExtractor::FrameItem item;

        if (extractor.open(this->getFilePath(), true) && extractor.getFrame(0.0, true, &item))
        {
            if (!item.frame.scaledToWidth(512).save(coverPath, Q_NULLPTR, 100))
            {
                QFile cover(coverPath);

                if (cover.exists())
                    cover.remove();
            }

            extractor.close();
        }
    }

#if defined Q_OS_IOS
    if (this->isAudio())
    {
        if (!IOSDelegate::isRegisteredMediaCenterCommand())
            IOSDelegate::registerMediaCenterCommand();

        IOSDelegate::updateMediaCenterCommand();
        IOSDelegate::updateNowPlayingInfo();
    }
    else
    {
        IOSDelegate::unRegisterMediaCenterCommand();
    }

    this->setBluetoothHeadsetConnected(IOSDelegate::isBluetoothHeadsetConnected());
#elif defined Q_OS_ANDROID
    this->callActivityEvent("prepair");

    QAndroidJniObject activity = QtAndroid::androidActivity();

    if (activity.isValid())
        activity.callMethod<void>("requestUpdateBluetoothHeadsetIsConnected");

    this->callActivityEvent("started");
#endif

    emit this->started();
}

void RenderScreen::callPlayingEvent()
{
#if defined Q_OS_ANDROID
    this->callActivityEvent("playing");
#endif

    emit this->resumed();
}

void RenderScreen::callPausedEvent()
{
#if defined Q_OS_ANDROID
    this->callActivityEvent("paused");
#endif

    emit this->paused();
}

void RenderScreen::callStoppedEvent()
{
    AnyVODWindow *window = (AnyVODWindow*)this->window();

    if (window)
    {
        window->setAcceptEvent(true);
#ifdef Q_OS_IOS
        window->setVisibility(QQuickWindow::AutomaticVisibility);
#endif
    }

    this->setUseCustomControl(false);
    this->setKeepActive(false);

    QFile cover(this->getLargeCoverFilePath());

    if (cover.exists())
        cover.remove();

#if defined Q_OS_ANDROID
    this->callActivityEvent("stopped");
#endif

    this->saveSettings();

    emit this->stopped();
}

void RenderScreen::callEndEvent()
{
    this->m_lastPlay.clear(SeparatingUtils::removeFFMpegSeparator(this->m_lastPlayPath));

    emit this->ended();

    switch (this->m_playingMethod)
    {
        case AnyVODEnums::PM_TOTAL:
        {
            if (this->m_playList->getCurrentIndex() + 1 < this->m_playList->getCount())
            {
                this->playNext();
            }
            else
            {
                this->m_playList->setCurrentIndex(0);
                this->closeAndGoBack();
            }

            break;
        }
        case AnyVODEnums::PM_TOTAL_REPEAT:
        {
            if (this->m_playList->getCurrentIndex() + 1 >= this->m_playList->getCount())
                this->m_playList->resetCurrentIndex();

            this->playNext();

            break;
        }
        case AnyVODEnums::PM_SINGLE:
        {
            this->closeAndGoBack();
            break;
        }
        case AnyVODEnums::PM_SINGLE_REPEAT:
        {
            this->playCurrent();
            break;
        }
        case AnyVODEnums::PM_RANDOM:
        {
            this->m_playList->setRandomCurrentIndex();
            this->playCurrent();

            break;
        }
        default:
        {
            break;
        }
    }
}

void RenderScreen::callExitEvent()
{
    AnyVODWindow *window = (AnyVODWindow*)this->window();

    if (window)
    {
        window->setClearBeforeRendering(true);
#ifndef Q_OS_IOS
        window->setVisibility(QQuickWindow::AutomaticVisibility);
#endif
    }

    this->m_playList->stop();

    if (!this->isTablet())
        this->setScreenOrientationRestore();

#if defined Q_OS_IOS
    IOSDelegate::unRegisterMediaCenterCommand();
#elif defined Q_OS_ANDROID
    this->callActivityEvent("exit");
#endif

    emit this->exit();
}

void RenderScreen::loadSettings()
{
    RenderScreenSettingLoader(this).load();
}

void RenderScreen::saveSettings()
{
    RenderScreenSettingSaver(this).save();
}

void RenderScreen::setOptionDescY(int y)
{
    this->m_presenter.setOptionDescY(y);
}

void RenderScreen::setUseCustomControl(bool use)
{
    AnyVODWindow *window = (AnyVODWindow*)this->window();

    if (window)
        window->setUseCustomControl(use);
}

bool RenderScreen::getUseCustomControl() const
{
    AnyVODWindow *window = (AnyVODWindow*)this->window();

    if (window)
        return window->getUseCustomControl();
    else
        return false;
}

QString RenderScreen::getTimeString(double value) const
{
    QString time;

    ConvertingUtils::getTimeString(value, ConvertingUtils::TIME_HH_MM_SS, &time);
    return time;
}

void RenderScreen::setSubtitleSync(double sync)
{
    QString desc;

    this->m_presenter.setSubtitleSync(sync);

    if (this->isMovieSubtitleVisiable())
    {
        if (sync > 0.0)
            desc = tr("자막 싱크 %1초 빠르게");
        else
            desc = tr("자막 싱크 %1초 느리게");
    }
    else
    {
        if (sync > 0.0)
            desc = tr("가사 싱크 %1초 빠르게");
        else
            desc = tr("가사 싱크 %1초 느리게");
    }

    desc = desc.arg(fabs(sync));

    this->m_presenter.showOptionDesc(desc);
}

double RenderScreen::getSubtitleSync() const
{
    return this->m_presenter.getSubtitleSync();
}

void RenderScreen::setAudioSync(double sync)
{
    QString desc;

    this->m_presenter.setAudioSync(sync);

    if (sync > 0.0)
        desc = tr("소리 싱크 %1초 빠르게");
    else
        desc = tr("소리 싱크 %1초 느리게");

    desc = desc.arg(fabs(sync));

    this->m_presenter.showOptionDesc(desc);
}

double RenderScreen::getAudioSync() const
{
    return this->m_presenter.getAudioSync();
}

void RenderScreen::useSubtitleCacheMode(bool use)
{
    QString desc;

    this->m_presenter.useSubtitleCacheMode(use);

    if (use)
        desc = tr("자막 캐시 모드 사용");
    else
        desc = tr("자막 캐시 모드 사용 안 함");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUseSubtitleCacheMode() const
{
    return this->m_presenter.isUseSubtitleCacheMode();
}

void RenderScreen::loadEqualizer()
{
    RenderScreenSettingLoader(this).loadEqualizer();
}

void RenderScreen::saveEqualizer()
{
    RenderScreenSettingSaver(this).saveEqualizer();
}

bool RenderScreen::isDTVOpened() const
{
    return DTVReader::getInstance().isOpened();
}

void RenderScreen::resetLyrics()
{
    this->m_lyrics1->setProperty("color", QColor(Qt::black));
    this->m_lyrics2->setProperty("color", QColor(Qt::black));
    this->m_lyrics3->setProperty("color", QColor(Qt::black));

    this->m_lyrics1->setProperty("text", QString());
    this->m_lyrics2->setProperty("text", QString());
    this->m_lyrics3->setProperty("text", QString());
}

void RenderScreen::exitAtCppSide()
{
    QQuickWindow *window = this->window();

    if (window)
        window->setColor(Qt::white);
}

void RenderScreen::setFullScreen()
{
    AnyVODWindow *window = (AnyVODWindow*)this->window();

    if (!window)
        return;

    if (this->isAudio())
        window->setVisibility(QQuickWindow::AutomaticVisibility);
    else
        window->setVisibility(QQuickWindow::FullScreen);
}

QStringList RenderScreen::getSPDIFAudioDevices()
{
    QStringList ret;

    this->m_presenter.getSPDIFAudioDevices(&ret);

    return ret;
}

bool RenderScreen::setSPDIFAudioDevice(int device)
{
    QStringList list;
    QString name;

    this->m_presenter.getSPDIFAudioDevices(&list);

    if (device == -1)
    {
        name = tr("기본 장치");
    }
    else
    {
        for (int i = 0; i < list.count(); i++)
        {
            if (i == device)
            {
                name = list[i];
                break;
            }
        }
    }

    bool useSPDIF = this->isUseSPDIF();
    bool success = this->m_presenter.setSPDIFAudioDevice(device);
    QString desc;

    if (this->isUseSPDIF() == useSPDIF)
        desc = tr("S/PDIF 소리 출력 장치 변경 (%1)").arg(name);
    else
        desc = tr("S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.");

    this->m_presenter.showOptionDesc(desc);

    emit this->spdifEnabled();

    return success;
}

int RenderScreen::getCurrentSPDIFAudioDevice() const
{
    return this->m_presenter.getCurrentSPDIFAudioDevice();
}

void RenderScreen::useSPDIF(bool enable)
{
    this->m_presenter.useSPDIF(enable);

    QString desc;

    if (this->isUseSPDIF() == enable)
    {
        if (enable)
            desc = tr("S/PDIF 출력 사용");
        else
            desc = tr("S/PDIF 출력 사용 안 함");
    }
    else
    {
        desc = tr("S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.");
    }

    this->m_presenter.showOptionDesc(desc);

    emit this->spdifEnabled();
}

bool RenderScreen::isUseSPDIF() const
{
    return this->m_presenter.isUseSPDIF();
}

void RenderScreen::setSPDIFEncodingMethod(int method)
{
    QString desc;

    this->m_presenter.setSPDIFEncodingMethod((AnyVODEnums::SPDIFEncodingMethod)method);

    if (this->getSPDIFEncodingMethod() == method)
    {
        switch (method)
        {
            case AnyVODEnums::SEM_NONE:
                desc = tr("S/PDIF 출력 시 인코딩 사용 안 함");
                break;
            case AnyVODEnums::SEM_AC3:
                desc = tr("S/PDIF 출력 시 AC3 인코딩 사용");
                break;
            case AnyVODEnums::SEM_DTS:
                desc = tr("S/PDIF 출력 시 DTS 인코딩 사용");
                break;
            default:
                break;
        }
    }
    else
    {
        desc = tr("S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.");
    }

    this->m_presenter.showOptionDesc(desc);

    emit this->spdifEnabled();
}

int RenderScreen::getSPDIFEncodingMethod() const
{
    return this->m_presenter.getSPDIFEncodingMethod();
}

void RenderScreen::resetSPDIF()
{
    bool useSPDIF = this->isUseSPDIF();
    QString desc;
    int sampleRate = this->m_spdifSampleRates.getSelectedSampleRate();

    this->m_presenter.resetSPDIF();

    if (this->isUseSPDIF() == useSPDIF)
    {
        desc = tr("S/PDIF 샘플 속도 (%1)");

        if (sampleRate == 0)
            desc = desc.arg(tr("기본 속도"));
        else
            desc = desc.arg(QString("%1kHz").arg(sampleRate / 1000.0f, 0, 'f', 1));
    }
    else
    {
        desc = tr("S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.");
    }

    this->m_presenter.showOptionDesc(desc);

    emit this->spdifEnabled();
}

void RenderScreen::selectSPDIFSampleRate(int sampleRate)
{
    this->m_spdifSampleRates.selectSampleRate(sampleRate);
    this->resetSPDIF();
}

void RenderScreen::setDistortionAdjustMode(int mode)
{
    QString desc;

    this->m_presenter.setDistortionAdjustMode((AnyVODEnums::DistortionAdjustMode)mode);

    switch (mode)
    {
        case AnyVODEnums::DAM_NONE:
            desc = tr("왜곡 보정 모드 사용 안 함");
            break;
        case AnyVODEnums::DAM_BARREL:
        case AnyVODEnums::DAM_PINCUSION:
            desc = tr("왜곡 보정 모드 사용 : %1").arg(this->getDistortionAdjustModeDesc(mode));
            break;
        default:
            break;
    }

    this->m_presenter.showOptionDesc(desc);
}

int RenderScreen::getDistortionAdjustMode() const
{
    return this->m_presenter.getDistortionAdjustMode();
}

void RenderScreen::setVerticalScreenOffset(int offset)
{
    this->m_presenter.setVerticalScreenOffset(offset);
}

void RenderScreen::setHorizontalScreenOffset(int offset)
{
    this->m_presenter.setHorizontalScreenOffset(offset);
}

void RenderScreen::determineToStartRotation()
{
    this->m_rotationSensor.determineToStartRotation();
}

void RenderScreen::calibrateRotation()
{
    this->m_rotationSensor.calibrateRotation();
}

bool RenderScreen::setAudioDevice(int device)
{
    QStringList list = AudioRenderer::getDevices();
    QString name;

    if (device == -1)
    {
        name = tr("기본 장치");
    }
    else
    {
        for (int i = 0; i < list.count(); i++)
        {
            if (i == device)
            {
                name = list[i];
                break;
            }
        }
    }

    bool success = this->m_presenter.setAudioDevice(device);

    this->m_presenter.showOptionDesc(tr("소리 출력 장치 변경 (%1)").arg(name));

    return success;
}

int RenderScreen::getCurrentAudioDevice() const
{
    return this->m_presenter.getCurrentAudioDevice();
}

void RenderScreen::setScreenRotationDegree(AnyVODEnums::ScreenRotationDegree degree)
{
    QString desc;

    switch (degree)
    {
        case AnyVODEnums::SRD_NONE:
            desc = tr("사용 안 함");
            break;
        case AnyVODEnums::SRD_90:
            desc = tr("90도");
            break;
        case AnyVODEnums::SRD_180:
            desc = tr("180도");
            break;
        case AnyVODEnums::SRD_270:
            desc = tr("270도");
            break;
        default:
            break;
    }

    this->setScheduleRecomputeSubtitleSize();

    this->m_presenter.setScreenRotationDegree(degree);
    this->m_presenter.showOptionDesc(tr("화면 회전 각도 (%1)").arg(desc));
}

AnyVODEnums::ScreenRotationDegree RenderScreen::getScreenRotationDegree() const
{
    return this->m_presenter.getScreenRotationDegree();
}

bool RenderScreen::playAt(int index)
{
    this->m_playList->setCurrentIndex(index);
    return this->playCurrent();
}
