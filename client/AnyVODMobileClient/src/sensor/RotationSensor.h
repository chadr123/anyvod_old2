﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QRotationSensor>
#include <QRotationFilter>
#include <QCompass>

class MediaPresenter;
class RenderScreen;

class RotationSensor : public QRotationSensor, public QRotationFilter
{
    Q_OBJECT
public:
    RotationSensor(RenderScreen *screen, MediaPresenter *presenter);
    virtual ~RotationSensor();

    void determineToStartRotation();
    void calibrateRotation();

public:
    static bool isSupported();

private:
    bool start();
    void stop();

    bool isLandscape() const;

    qreal getZ(QRotationReading *reading) const;
    qreal getY(QRotationReading *reading) const;
    qreal getX(QRotationReading *reading) const;

    void calibrateRotation(QRotationReading *reading);

private:
    virtual bool filter(QRotationReading *reading);

private:
    RenderScreen *m_screen;
    MediaPresenter *m_presenter;
    QCompass m_compass;
    qreal m_rotationZ;
    qreal m_rotationY;
    qreal m_rotationX;
    qreal m_preRotationZ;
    qreal m_preRotationY;
    int m_overZ;
    int m_overY;
    bool m_needCalibrateRotation;
    int m_skipCalibrateRotationCount;
};
