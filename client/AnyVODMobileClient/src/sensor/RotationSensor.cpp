﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RotationSensor.h"

#include "ui/RenderScreen.h"
#include "media/MediaPresenter.h"

RotationSensor::RotationSensor(RenderScreen *screen, MediaPresenter *presenter) :
    m_screen(screen),
    m_presenter(presenter),
    m_rotationZ(0.0),
    m_rotationY(0.0),
    m_rotationX(0.0),
    m_preRotationZ(0.0),
    m_preRotationY(0.0),
    m_overZ(0),
    m_overY(0),
    m_needCalibrateRotation(false),
    m_skipCalibrateRotationCount(0)
{
    this->addFilter(this);

    this->setSkipDuplicates(true);
    this->m_compass.setSkipDuplicates(true);
}

RotationSensor::~RotationSensor()
{

}

void RotationSensor::determineToStartRotation()
{
    this->stop();

    if (this->m_screen->isUseHeadTracking())
    {
        if (this->start())
            this->calibrateRotation();
        else
            this->stop();
    }

    this->m_screen->reset360DegreeAngles();

    this->m_screen->setHorizontalScreenOffset(0);
    this->m_screen->setVerticalScreenOffset(0);
}

void RotationSensor::calibrateRotation()
{
    this->m_needCalibrateRotation = true;
}

bool RotationSensor::isSupported()
{
    QList<QByteArray> list = QSensor::sensorTypes();

    return list.contains(QRotationSensor::type);
}

bool RotationSensor::start()
{
#ifdef Q_OS_ANDROID
    this->setDataRate(120);
    this->m_compass.setDataRate(120);
#endif

    bool success = QRotationSensor::start();

    if (!this->hasZ())
        success &= this->m_compass.start();

    return success;
}

void RotationSensor::stop()
{
    QRotationSensor::stop();
    this->m_compass.stop();
}

bool RotationSensor::isLandscape() const
{
    return (this->m_screen->width() / this->m_screen->height()) > 1.0;
}

qreal RotationSensor::getZ(QRotationReading *reading) const
{
    return this->hasZ() ? reading->z() + 180.0 : -this->m_compass.reading()->azimuth();
}

qreal RotationSensor::getY(QRotationReading *reading) const
{
    if (this->isLandscape())
        return reading->y() + 180.0;
    else
        return -(reading->x() + 90.0);
}

qreal RotationSensor::getX(QRotationReading *reading) const
{
    if (this->isLandscape())
        return reading->x() + 90.0;
    else
        return reading->y() + 180.0;
}

void RotationSensor::calibrateRotation(QRotationReading *reading)
{
    this->m_rotationZ = this->getZ(reading);
    this->m_rotationY = this->getY(reading);
    this->m_rotationX = this->getX(reading);

    this->m_preRotationZ = this->m_rotationZ;
    this->m_preRotationY = this->m_rotationY;

    this->m_overZ = 0;
    this->m_overY = 0;
}

bool RotationSensor::filter(QRotationReading *reading)
{
    ShaderCompositer &shader = ShaderCompositer::getInstance();

    if (this->m_needCalibrateRotation && this->m_skipCalibrateRotationCount++ > 40)
    {
        this->calibrateRotation(reading);

        this->m_needCalibrateRotation = false;
        this->m_skipCalibrateRotationCount = 0;
    }

    qreal z = this->getZ(reading);
    qreal y = this->getY(reading);

    if (shader.is360Degree())
    {
        qreal x = this->getX(reading);
        QVector3D angles(z - this->m_rotationZ + 90.0, y - this->m_rotationY, this->m_rotationX - x);

        this->m_screen->set360DegreeAngles(angles);
    }
    else
    {
        qreal margin = 120.0;
        qreal adjustedZ = z;
        qreal adjustedY = y;

        if ((this->m_preRotationZ >= (360.0 - margin) && this->m_preRotationZ <= 360.0) && (z >= 0.0 && z <= margin))
            this->m_overZ++;
        else if ((this->m_preRotationZ >= 0.0 && this->m_preRotationZ <= margin) && (z >= (360.0 - margin) && z <= 360.0))
            this->m_overZ--;

        if ((this->m_preRotationY >= (360.0 - margin) && this->m_preRotationY <= 360.0) && (y >= 0.0 && y <= margin))
            this->m_overY++;
        else if ((this->m_preRotationY >= 0.0 && this->m_preRotationY <= margin) && (y >= (360.0 - margin) && y <= 360.0))
            this->m_overY--;

        adjustedZ += this->m_overZ * 360;
        adjustedY += this->m_overY * 360;

        this->m_screen->setHorizontalScreenOffset((adjustedZ - this->m_rotationZ) * 3.0);
        this->m_screen->setVerticalScreenOffset((this->m_rotationY - adjustedY) * 3.0);

        this->m_preRotationZ = z;
        this->m_preRotationY = y;
    }

    return false;
}
