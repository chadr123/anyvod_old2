﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "decoders/HWDecoderInterface.h"

#include "OMX_Video.h"
#include "OMX_VideoExt.h"

#include <QQueue>

#define OMX_QCOM_COLOR_FormatYVU420SemiPlanar                   0x7FA30C00
#define OMX_QCOM_COLOR_FormatYUV420SemiPlanar32m                0x7FA30C04
#define OMX_IndexVendorSetYUV420pMode                           0x7f000003
#define OMX_TI_COLOR_FormatYUV420PackedSemiPlanar               0x7F000100
#define OMA_TI_COLOR_FormatYUV420PackedSemiPlanarInterlaced     0x7f000001
#define QOMX_COLOR_FormatYUV420PackedSemiPlanar64x32Tile2m8ka   0x7FA30C03

class QAndroidJniEnvironment;
class QAndroidJniObject;

class MediaCodec : public HWDecoderInterface
{
public:
    MediaCodec();
    virtual ~MediaCodec();

    virtual bool open(AVCodecContext *codec);
    virtual void close();

    virtual bool prepare(AVCodecContext *codec);

    virtual bool getBuffer(AVFrame *ret);
    virtual void releaseBuffer(uint8_t *data[AV_NUM_DATA_POINTERS]);

    virtual AVPixelFormat getFormat() const;
    virtual QString getName() const;

    virtual bool decodePicture(const AVPacket &packet, AVFrame *ret);
    virtual bool copyPicture(const AVFrame &src, AVFrame *ret);

    virtual bool isDecodable(AVPixelFormat format) const;
    virtual void getDecoderDesc(QString *ret) const;

    virtual void flushSurfaceQueue();
    virtual int getSurfaceQueueCount() const;

    virtual bool isUseDefaultGetBuffer() const;

private:
    struct CodecDesc
    {
        AVCodecID id;
        const char *mime;
    };

    struct OMXToH264ProfileIDC
    {
        OMX_VIDEO_AVCPROFILETYPE omxProfile;
        int profileIDC;
    };

    struct OMXToHEVCProfileIDC
    {
        OMX_VIDEO_HEVCPROFILETYPE omxProfile;
        int profileIDC;
    };

    struct OMXToVP9ProfileIDC
    {
        OMX_VIDEO_VP9PROFILETYPE omxProfile;
        int profileIDC;
    };

    struct OMXToMPEG4ProfileIDC
    {
        OMX_VIDEO_MPEG4PROFILETYPE omxProfile;
        int profileIDC;
    };

    struct MethodDesc
    {
        const char *name;
        const char *sig;
        const char *className;
    };

    struct H264ConvertState
    {
        H264ConvertState()
        {
            nalLen = 0;
            nalPos = 0;
        }

        unsigned int nalLen;
        unsigned int nalPos;
    };

    enum CLASS_NAME
    {
        CN_MEDIA_CODEC_LIST,
        CN_MEDIA_CODEC,
        CN_MEDIA_FORMAT,
        CN_MEDIA_CODEC_$_BUFFERINFO,
        CN_BYTE_BUFFER,
        CN_COUNT
    };

    enum METHOD_NAME
    {
        MN_TO_STRING,
        MN_GET_CODEC_COUNT,
        MN_GET_CODEC_INFO_AT,
        MN_IS_ENCODER,
        MN_GET_SUPPORTED_TYPES,
        MN_GET_NAME,
        MN_GET_CAPABILITIES_FOR_TYPE,
        MN_PROFILE_LEVELS,
        MN_PROFILE,
        MN_LEVEL,
        MN_CREATE_BY_CODEC_NAME,
        MN_CONFIGURE,
        MN_START,
        MN_STOP,
        MN_FLUSH,
        MN_RELEASE,
        MN_GET_OUTPUT_FORMAT,
        MN_GET_INPUT_BUFFERS,
        MN_GET_OUTPUT_BUFFERS,
        MN_DEQUEUE_INPUT_BUFFER,
        MN_DEQUEUE_OUTPUT_BUFFER,
        MN_QUEUE_INPUT_BUFFER,
        MN_RELEASE_OUTPUT_BUFFER,
        MN_CREATE_VIDEO_FORMAT,
        MN_SET_INTEGER,
        MN_GET_INTEGER,
        MN_SET_BYTE_BUFFER,
        MN_INIT,
        MN_SIZE,
        MN_OFFSET,
        MN_PRESENTATION_TIME_US,
        MN_ALLOCATE_DIRECT,
        MN_LIMIT,
        MN_COUNT
    };

private:
    template<typename T1, typename T2>
    int convertOMXToProfileIDC(T1 profile, const T2 &array) const;

private:
    bool reOpen();
    bool isNoPaddingDecoder(const QString &name) const;
    QString getMime(AVCodecID id) const;

    void closeInternal();

    bool extractDecodedFrame(QAndroidJniEnvironment &env, int timeout, AVFrame *frame);

    void copyHWPicture(AVFrame *frame, uint8_t *ptr) const;

    void yuv420PlanarCopy(uint8_t *ptr, AVFrame *frame) const;
    void yuv420SemiPlanarCopy(uint8_t *ptr, AVFrame *frame) const;
    void yuv420PackedSemiPlanarCopy(uint8_t *ptr, AVFrame *frame) const;
    void qcomCopy(uint8_t *ptr, AVFrame *frame) const;
    int qcomTilePos(int x, int y, int width, int height) const;

    AVPixelFormat convertToAVPixFormat(int pixFormat) const;
    void convertH264ToAnnexB(uint8_t *data, unsigned int size, unsigned int nalSize, H264ConvertState *state) const;

    void convertH264SPSAndPPS(uint8_t *srcData, int srcSize, uint8_t *dstData, int dstSize, int *realSize, unsigned int *retNalSize) const;
    void convertHEVCVPSAndSPSAndPPS(uint8_t *srcData, int srcSize, uint8_t *dstData, int dstSize, int *realSize, unsigned int *retNalSize) const;
    void convertSMTPE(uint8_t *srcData, uint8_t *dstData, int width, int height, int *realSize) const;
    bool convertVC1Data(uint8_t *srcData, int srcSize, uint8_t *dstData, int dstSize, int *realSize) const;

private:
    QAndroidJniObject *m_codec;
    QAndroidJniObject *m_inputBuffers;
    QAndroidJniObject *m_outputBuffers;
    QAndroidJniObject *m_bufferInfo;
    AVCodecContext *m_codecContext;
    QString m_codecName;
    bool m_started;
    QQueue<int64_t> m_ptsQueue;
    int m_pixFormat;
    unsigned int m_nalSize;
    int m_sliceHeight;
    int m_stride;
    int m_cropLeft;
    int m_cropRight;
    int m_cropTop;
    int m_cropBottom;

private:
    static const CodecDesc CODEC_DESCS[];
    static const OMXToH264ProfileIDC OMX_H264_PROFILE_IDC[];
    static const OMXToHEVCProfileIDC OMX_HEVC_PROFILE_IDC[];
    static const OMXToVP9ProfileIDC OMX_VP9_PROFILE_IDC[];
    static const OMXToMPEG4ProfileIDC OMX_MPEG4_PROFILE_IDC[];
    static const char *CLASS_NAMES[CN_COUNT];
    static const MethodDesc METHOD_DESCS[MN_COUNT];
};
