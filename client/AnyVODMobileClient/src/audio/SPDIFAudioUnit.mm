﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SPDIFAudioUnit.h"

#include <QDebug>

#import <AVFoundation/AVFoundation.h>

SPDIFAudioUnit::SPDIFAudioUnit() :
    m_audioUnit(nullptr),
    m_latency(0.0)
{

}

SPDIFAudioUnit::~SPDIFAudioUnit()
{
    this->closeInternal();
}

bool SPDIFAudioUnit::open()
{
    if (!this->initAudio())
    {
        this->close();
        return false;
    }

    return true;
}

void SPDIFAudioUnit::close()
{
    this->closeInternal();
}

bool SPDIFAudioUnit::checkSupport()
{
    return true;
}

bool SPDIFAudioUnit::play()
{
    return AudioOutputUnitStart(this->m_audioUnit) == noErr;
}

bool SPDIFAudioUnit::pause()
{
    return this->pauseInternal();
}

bool SPDIFAudioUnit::resume()
{
    return this->play();
}

bool SPDIFAudioUnit::stop()
{
    return this->stopInternal();
}

bool SPDIFAudioUnit::fillBuffer(void *buffer, int size)
{
    (void)buffer;
    (void)size;

    return false;
}

unsigned int SPDIFAudioUnit::getNeedBufferSize() const
{
    return 0;
}

void SPDIFAudioUnit::getDeviceList(QStringList *ret)
{
    (void)ret;
}

int SPDIFAudioUnit::getDeviceCount() const
{
    return 0;
}

double SPDIFAudioUnit::getLatency()
{
    return this->m_bufferLength / 1000.0 + this->m_latency;
}

bool SPDIFAudioUnit::isSupportPull() const
{
    return true;
}

int SPDIFAudioUnit::getAlignSize() const
{
    return 0;
}

bool SPDIFAudioUnit::canFillBufferBeforeStart() const
{
    return false;
}

bool SPDIFAudioUnit::initAudio()
{
    AudioStreamBasicDescription audioFormat = {};

    audioFormat.mFramesPerPacket = 1;
    audioFormat.mBitsPerChannel = 16;
    audioFormat.mFormatFlags |= kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
    audioFormat.mFormatID = kAudioFormatLinearPCM;

    audioFormat.mChannelsPerFrame = this->m_channelCount;
    audioFormat.mSampleRate = this->m_sampleRate;
    audioFormat.mBytesPerFrame = audioFormat.mBitsPerChannel * audioFormat.mChannelsPerFrame / 8;
    audioFormat.mBytesPerPacket = audioFormat.mBytesPerFrame * audioFormat.mFramesPerPacket;

    this->m_bytesPerSample = audioFormat.mBytesPerFrame;

    AudioComponentDescription description = {};

    description.componentType = kAudioUnitType_Output;
    description.componentSubType = kAudioUnitSubType_RemoteIO;
    description.componentManufacturer = kAudioUnitManufacturer_Apple;

    AudioComponent component = AudioComponentFindNext(nullptr, &description);

    if (component == nullptr)
        return false;

    if (AudioComponentInstanceNew(component, &this->m_audioUnit) != noErr)
        return false;

    if (!this->setHardwareSampleRate())
        return false;

    if (this->m_sampleRate != this->getHardwareSampleRate())
        return false;

    if (AudioUnitSetProperty(this->m_audioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 0, &audioFormat, sizeof(audioFormat)) != noErr)
        return false;

    AURenderCallbackStruct callbackStruct = {};

    callbackStruct.inputProc = SPDIFCallback;
    callbackStruct.inputProcRefCon = this;

    if (AudioUnitSetProperty(this->m_audioUnit, kAudioUnitProperty_SetRenderCallback, kAudioUnitScope_Input, 0, &callbackStruct, sizeof(callbackStruct)) != noErr)
        return false;

    if (AudioUnitInitialize(this->m_audioUnit) != noErr)
        return false;

    AVAudioSession *audioSession = [AVAudioSession sharedInstance];

    this->m_bufferLength = [audioSession IOBufferDuration] * 1000;
    this->m_latency = [audioSession outputLatency];

    return true;
}

void SPDIFAudioUnit::deInitAudio()
{
    if (this->m_audioUnit)
    {
        this->stopInternal();

        AudioUnitUninitialize(this->m_audioUnit);
        AudioComponentInstanceDispose(this->m_audioUnit);

        this->m_audioUnit = nullptr;
    }

    this->m_latency = 0.0;
}

void SPDIFAudioUnit::closeInternal()
{
    this->deInitAudio();
}

bool SPDIFAudioUnit::stopInternal()
{
    return this->pauseInternal();
}

bool SPDIFAudioUnit::pauseInternal()
{
    return AudioOutputUnitStop(this->m_audioUnit) == noErr;
}

bool SPDIFAudioUnit::setHardwareSampleRate() const
{
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];

    return [audioSession setPreferredSampleRate: this->m_sampleRate error: nil];
}

int SPDIFAudioUnit::getHardwareSampleRate() const
{
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];

    return (int)[audioSession sampleRate];
}

OSStatus SPDIFAudioUnit::SPDIFCallback(void *inRefCon, AudioUnitRenderActionFlags *ioActionFlags,
                                       const AudioTimeStamp *inTimeStamp, UInt32 inOutputBusNumber,
                                       UInt32 inNumberFrames, AudioBufferList *ioData)
{
    (void)ioActionFlags;
    (void)inTimeStamp;
    (void)inOutputBusNumber;
    (void)inNumberFrames;
    SPDIFAudioUnit *parent = (SPDIFAudioUnit*)inRefCon;

    for (unsigned int i = 0; i < ioData->mNumberBuffers; i++)
    {
        AudioBuffer &buffer = ioData->mBuffers[i];

        buffer.mDataByteSize = parent->m_callback(buffer.mData, buffer.mDataByteSize, parent->m_user);
    }

    return noErr;
}
