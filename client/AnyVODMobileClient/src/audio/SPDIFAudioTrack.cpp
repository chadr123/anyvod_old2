﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SPDIFAudioTrack.h"

#include <QAndroidJniEnvironment>
#include <QtAndroid>
#include <QFile>
#include <QThread>
#include <QDebug>

#include <stdint.h>

extern "C"
{
# include <libavformat/avformat.h>
# include <libavutil/opt.h>
}

int SPDIFAudioTrack::STREAM_MUSIC               = 3;
int SPDIFAudioTrack::MODE_STREAM                = 0x00000001;

int SPDIFAudioTrack::ENCODING_PCM_16BIT         = 0x00000002;
int SPDIFAudioTrack::ENCODING_IEC61937          = 0x0000000d;

int SPDIFAudioTrack::CHANNEL_OUT_STEREO         = 0x0000000c;
int SPDIFAudioTrack::CHANNEL_OUT_5POINT1        = 0x000000fc;

int SPDIFAudioTrack::CHANNEL_OUT_SIDE_LEFT      = 0x00000800;
int SPDIFAudioTrack::CHANNEL_OUT_SIDE_RIGHT     = 0x00001000;

int SPDIFAudioTrack::STATE_INITIALIZED          = 0x00000001;

const char *SPDIFAudioTrack::AUDIO_MANAGER = "android/media/AudioManager";
const char *SPDIFAudioTrack::AUDIO_TRACK   = "android/media/AudioTrack";
const char *SPDIFAudioTrack::AUDIO_FORMAT  = "android/media/AudioFormat";

SPDIFAudioTrack::SPDIFAudioTrack() :
    m_audioTrack(nullptr),
    m_buffer(nullptr),
    m_audioFormat(0)
{
    STREAM_MUSIC = QAndroidJniObject::getStaticField<int>(AUDIO_MANAGER, "STREAM_MUSIC");

    MODE_STREAM = QAndroidJniObject::getStaticField<int>(AUDIO_TRACK, "MODE_STREAM");
    STATE_INITIALIZED = QAndroidJniObject::getStaticField<int>(AUDIO_TRACK, "STATE_INITIALIZED");

    ENCODING_PCM_16BIT = QAndroidJniObject::getStaticField<int>(AUDIO_FORMAT, "ENCODING_PCM_16BIT");
    ENCODING_IEC61937 = QAndroidJniObject::getStaticField<int>(AUDIO_FORMAT, "ENCODING_IEC61937");

    CHANNEL_OUT_STEREO = QAndroidJniObject::getStaticField<int>(AUDIO_FORMAT, "CHANNEL_OUT_STEREO");
    CHANNEL_OUT_5POINT1 = QAndroidJniObject::getStaticField<int>(AUDIO_FORMAT, "CHANNEL_OUT_5POINT1");
    CHANNEL_OUT_SIDE_LEFT = QAndroidJniObject::getStaticField<int>(AUDIO_FORMAT, "CHANNEL_OUT_SIDE_LEFT");
    CHANNEL_OUT_SIDE_RIGHT = QAndroidJniObject::getStaticField<int>(AUDIO_FORMAT, "CHANNEL_OUT_SIDE_RIGHT");
}

SPDIFAudioTrack::~SPDIFAudioTrack()
{
    this->closeInternal();
}

bool SPDIFAudioTrack::open()
{
    int encoding = ENCODING_IEC61937 == 0 ? ENCODING_PCM_16BIT : ENCODING_IEC61937;
    int channelMask;

    if (this->m_channelCount == 8)
        channelMask = CHANNEL_OUT_5POINT1 | CHANNEL_OUT_SIDE_LEFT | CHANNEL_OUT_SIDE_RIGHT;
    else
        channelMask = CHANNEL_OUT_STEREO;

    if (this->isAML() && ENCODING_IEC61937 == 0)
        channelMask = CHANNEL_OUT_STEREO;

    unsigned int minBufferSize;
    int minBuffer = this->getMinBufferSize(this->m_sampleRate, channelMask, encoding);

    if (minBuffer < 0)
    {
        this->close();
        return false;
    }

    minBufferSize = (unsigned int)minBuffer;
    minBufferSize *= 2;

    if (this->isAML() && ENCODING_IEC61937 == 0)
    {
        if (this->m_sampleRate > 48000)
            minBufferSize *= (this->m_sampleRate / 48000);
    }

    this->m_bytesPerSample = 2 * this->m_channelCount;

    int frameSize;

    if (this->isAML() && ENCODING_IEC61937 == 0)
        frameSize = 2 * av_get_bytes_per_sample(AV_SAMPLE_FMT_S16);
    else
        frameSize = this->m_bytesPerSample;

    this->m_bytesPerSample = frameSize;
    this->m_bufferLength = (double)(minBufferSize / frameSize) / (double)this->m_sampleRate * 1000.0;

    if (!this->createAudioTrack(this->m_sampleRate, channelMask, encoding, minBufferSize))
    {
        this->close();
        return false;
    }

    return true;
}

void SPDIFAudioTrack::close()
{
    this->closeInternal();
}

bool SPDIFAudioTrack::checkSupport()
{
    return this->isAML() || ENCODING_IEC61937 != 0;
}

bool SPDIFAudioTrack::play()
{
    this->audioPlay();
    return true;
}

bool SPDIFAudioTrack::pause()
{
    this->audioPause();
    return true;
}

bool SPDIFAudioTrack::resume()
{
    return this->play();
}

bool SPDIFAudioTrack::stop()
{
    return this->pause();
}

bool SPDIFAudioTrack::fillBuffer(void *buffer, int size)
{
    int written = 0;
    int loopWritten = 0;
    int sizeLeft = size;
    int frames = size / this->m_bytesPerSample;
    uint8_t *outBuf = (uint8_t*)buffer;

    while (written < size && !this->m_quit)
    {
        loopWritten = this->audioWrite(outBuf, sizeLeft);

        if (loopWritten == 0)
        {
            double sleepTime;

            sleepTime = frames / this->m_bytesPerSample / 2.0 / (double)this->m_sampleRate * 1000;
            QThread::msleep(sleepTime);

            continue;
        }

        if (loopWritten < 0)
            return false;

        written += loopWritten;
        sizeLeft -= loopWritten;

        if (written < size)
            outBuf += loopWritten;
    }

    return true;
}

unsigned int SPDIFAudioTrack::getNeedBufferSize() const
{
    return 0;
}

void SPDIFAudioTrack::getDeviceList(QStringList *ret)
{
    (void)ret;
}

int SPDIFAudioTrack::getDeviceCount() const
{
    return 0;
}

double SPDIFAudioTrack::getLatency()
{
    return this->m_bufferLength / 1000.0;
}

bool SPDIFAudioTrack::isSupportPull() const
{
    return false;
}

int SPDIFAudioTrack::getAlignSize() const
{
    return 0;
}

bool SPDIFAudioTrack::canFillBufferBeforeStart() const
{
    return true;
}

bool SPDIFAudioTrack::createAudioTrack(int sampleRate, int channelMask, int audioFormat, int bufferSize)
{
    QAndroidJniEnvironment env;

    this->m_audioTrack = new QAndroidJniObject(AUDIO_TRACK, "(IIIIII)V",
                                               STREAM_MUSIC, sampleRate, channelMask,
                                               audioFormat, bufferSize, MODE_STREAM);

    if (env->ExceptionCheck())
    {
        env->ExceptionClear();
        return false;
    }

    if (!this->m_audioTrack->isValid())
        return false;

    if (this->audioGetState() != STATE_INITIALIZED)
        return false;

    if (audioFormat == ENCODING_IEC61937)
        this->m_buffer = new QAndroidJniObject(env->NewShortArray(bufferSize / sizeof(uint16_t)));
    else
        this->m_buffer = new QAndroidJniObject(env->NewByteArray(bufferSize));

    this->m_audioFormat = audioFormat;

    return true;
}

bool SPDIFAudioTrack::isAML() const
{
    QFile aml("/sys/class/audiodsp/digital_raw");

    return aml.exists();
}

void SPDIFAudioTrack::closeInternal()
{
    if (this->m_audioTrack)
    {
        if (this->audioGetState() == STATE_INITIALIZED)
        {
            this->audioStop();
            this->audioFlush();
        }

        this->audioRelease();

        delete this->m_audioTrack;
        this->m_audioTrack = nullptr;
    }

    if (this->m_buffer)
    {
        delete this->m_buffer;
        this->m_buffer = nullptr;
    }
}

int SPDIFAudioTrack::getMinBufferSize(int sampleRate, int channelMask, int encoding) const
{
    return QAndroidJniObject::callStaticMethod<int>(AUDIO_TRACK, "getMinBufferSize", "(III)I",
                                                    sampleRate, channelMask, encoding);
}

void SPDIFAudioTrack::audioPlay()
{
    if (this->m_audioTrack)
        this->m_audioTrack->callMethod<void>("play", "()V");
}

void SPDIFAudioTrack::audioPause()
{
    if (this->m_audioTrack)
        this->m_audioTrack->callMethod<void>("pause", "()V");
}

void SPDIFAudioTrack::audioStop()
{
    if (!this->m_audioTrack)
        return;

    QAndroidJniEnvironment env;

    this->m_audioTrack->callMethod<void>("stop", "()V");

    if (env->ExceptionCheck())
        env->ExceptionClear();
}

void SPDIFAudioTrack::audioFlush()
{
    if (this->m_audioTrack)
        this->m_audioTrack->callMethod<void>("flush", "()V");
}

void SPDIFAudioTrack::audioRelease()
{
    if (this->m_audioTrack)
        this->m_audioTrack->callMethod<void>("release", "()V");
}

int SPDIFAudioTrack::audioWrite(void *buffer, int size)
{
    if (!this->m_audioTrack)
        return -1;

    QAndroidJniEnvironment env;
    int written = -1;
    jarray javaArray = this->m_buffer->object<jarray>();
    int len = env->GetArrayLength(javaArray);
    char *array = (char*)env->GetPrimitiveArrayCritical(javaArray, nullptr);

    if (!array)
        return written;

    len = qMin(len, size);
    memcpy(array, buffer, len);

    env->ReleasePrimitiveArrayCritical(javaArray, array, 0);

    if (this->m_audioFormat == ENCODING_IEC61937)
    {
        written = this->m_audioTrack->callMethod<int>("write", "([SII)I", javaArray, 0, len / sizeof(uint16_t));
        written *= sizeof(uint16_t);
    }
    else
    {
        written = this->m_audioTrack->callMethod<int>("write", "([BII)I", javaArray, 0, len);
    }

    return written;
}

int SPDIFAudioTrack::audioGetState()
{
    if (!this->m_audioTrack)
        return -1;

    return this->m_audioTrack->callMethod<int>("getState", "()I");
}
