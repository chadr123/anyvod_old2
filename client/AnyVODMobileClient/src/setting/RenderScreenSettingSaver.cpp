﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RenderScreenSettingSaver.h"
#include "audio/SPDIFSampleRates.h"
#include "ui/RenderScreen.h"
#include "parsers/subtitle/GlobalSubtitleCodecName.h"
#include "setting/Settings.h"
#include "models/PlayListModel.h"

#include <QSettings>

RenderScreenSettingSaver::RenderScreenSettingSaver(RenderScreen *screen) :
    RenderScreenSettingBase(screen)
{

}

void RenderScreenSettingSaver::save() const
{
    this->m_settings.setValue(SETTING_FIRST_LOADING, false);

    this->m_settings.setValue(SETTING_VOLUME, this->m_screen->getVolume());
    this->m_settings.setValue(SETTING_SHOW_SUBTITLE, this->m_screen->isShowSubtitle());
    this->m_settings.setValue(SETTING_USE_NORMALIZER, this->m_screen->isUsingNormalizer());
    this->m_settings.setValue(SETTING_PLAYING_METHOD, (int)this->m_screen->getPlayingMethod());
    this->m_settings.setValue(SETTING_DEINTERLACER_METHOD, (int)this->m_screen->getDeinterlaceMethod());
    this->m_settings.setValue(SETTING_DEINTERLACER_ALGORITHM, (int)this->m_screen->getDeinterlaceAlgorithm());
    this->m_settings.setValue(SETTING_HALIGN_SUBTITLE, (int)this->m_screen->getHAlign());
    this->m_settings.setValue(SETTING_SEEK_KEYFRAME, this->m_screen->isSeekKeyFrame());

    this->m_settings.setValue(SETTING_SCREEN_CAPTURE_DIR, this->m_screen->getCaptureDirectory());
    this->m_settings.setValue(SETTING_SCREEN_CAPTURE_EXT, this->m_screen->getCaptureExt());

    this->m_settings.setValue(SETTING_ENABLE_SEARCH_SUBTITLE, this->m_screen->isEnableSearchSubtitle());
    this->m_settings.setValue(SETTING_ENABLE_SEARCH_LYRICS, this->m_screen->isEnableSearchLyrics());

    this->m_settings.setValue(SETTING_VALIGN_SUBTITLE, (int)this->m_screen->getVAlign());
    this->m_settings.setValue(SETTING_SUBTITLE_SIZE, this->m_screen->getSubtitleSize());

    QSizeF aspectSize = this->m_screen->getCustomUserAspectRatio();
    this->m_settings.setValue(SETTING_USER_ASPECT_RATIO, aspectSize);

    this->m_settings.setValue(SETTING_USE_HW_DECODER, this->m_screen->isUseHWDecoder());
    this->m_settings.setValue(SETTING_SHOW_ALBUM_JACKET, this->m_screen->isShowAlbumJacket());
    this->m_settings.setValue(SETTING_ENABLE_LAST_PLAY, this->m_screen->isGotoLastPos());
    this->m_settings.setValue(SETTING_TEXT_ENCODING, GlobalSubtitleCodecName::getInstance().getCodecName());
    this->m_settings.setValue(SETTING_USE_FRAME_DROP, this->m_screen->isUseFrameDrop());

    QStringList subtitleDirectory;
    bool priorSubtitleDirectory;

    this->m_screen->getSubtitleDirectory(&subtitleDirectory, &priorSubtitleDirectory);
    this->m_settings.setValue(SETTING_SUBTITLE_DIRECTORY, subtitleDirectory);
    this->m_settings.setValue(SETTING_PRIOR_SUBTITLE_DIRECTORY, priorSubtitleDirectory);

    this->m_settings.setValue(SETTING_SEARCH_SUBTITLE_COMPLEX, this->m_screen->getSearchSubtitleComplex());
    this->m_settings.setValue(SETTING_USE_BUFFERING_MODE, this->m_screen->isUseBufferingMode());
    this->m_settings.setValue(SETTING_USE_SUBTITLE_CACHE_MODE, this->m_screen->isUseSubtitleCacheMode());
    this->m_settings.setValue(SETTING_USE_LOW_QUALITY_MODE, this->m_screen->isUseLowQualityMode());
    this->m_settings.setValue(SETTING_AUDIO_DEVICE, this->m_screen->getCurrentAudioDevice());

    this->m_settings.setValue(SETTING_SPDIF_DEVICE, this->m_screen->getCurrentSPDIFAudioDevice());
    this->m_settings.setValue(SETTING_SPDIF_ENCODING, this->m_screen->getSPDIFEncodingMethod());
    this->m_settings.setValue(SETTING_SPDIF_SAMPLE_RATE, SPDIFSampleRates::getInstance().getSelectedSampleRate());
    this->m_settings.setValue(SETTING_USE_SPDIF, this->m_screen->isUseSPDIF());

    this->m_settings.setValue(SETTING_VR_USE_DISTORTION, this->m_screen->isUseDistortion());
    this->m_settings.setValue(SETTING_VR_BARREL_DISTORTION_COEFFICIENTS, this->m_screen->getBarrelDistortionCoefficients().toPointF());
    this->m_settings.setValue(SETTING_VR_PINCUSHION_DISTORTION_COEFFICIENTS, this->m_screen->getPincushionDistortionCoefficients().toPointF());
    this->m_settings.setValue(SETTING_VR_DISTORTION_LENS_CENTER, this->m_screen->getDistortionLensCenter().toPointF());

    this->m_settings.setValue(SETTING_AUTO_SAVE_SEARCH_LYRICS, this->m_screen->isAutoSaveSearchLyrics());
    this->m_settings.setValue(SETTING_BLUETOOTH_AUDIO_SYNC, this->m_screen->getBluetoothHeadsetSync());

    this->saveEqualizer();

    PlayListModel *playList = this->m_screen->getPlayList();

    if (playList && playList->canSave())
        playList->updateCurrentPlayList();
}

void RenderScreenSettingSaver::saveEqualizer() const
{
    this->m_settings.setValue(SETTING_USE_EQUALIZER, this->m_screen->isUsingEqualizer());
    this->m_settings.setValue(SETTING_EQUALIZER_PREAMP, this->m_screen->getPreAmp());

    QVariantList list;
    QVector<RenderScreen::EqualizerItem> eqGains;

    for (int i = 0; i < this->m_screen->getBandCount(); i++)
    {
        RenderScreen::EqualizerItem item;

        item.gain = this->m_screen->getEqualizerGain(i);

        eqGains.append(item);
    }

    for (int i = 0; i < eqGains.count(); i++)
        list.append(QVariant::fromValue(eqGains[i]));

    this->m_settings.setValue(SETTING_EQUALIZER_GAINS, list);
}
