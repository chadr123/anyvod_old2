﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RenderScreenSettingLoader.h"
#include "audio/SPDIFSampleRates.h"
#include "ui/RenderScreen.h"
#include "setting/Settings.h"
#include "parsers/subtitle/GlobalSubtitleCodecName.h"

#include <QSettings>
#include <QDir>
#include <QTextCodec>

RenderScreenSettingLoader::RenderScreenSettingLoader(RenderScreen *screen) :
    RenderScreenSettingBase(screen)
{

}

void RenderScreenSettingLoader::load() const
{
    uint8_t volume = this->m_settings.value(SETTING_VOLUME, this->m_screen->getVolume()).toInt();
    this->m_screen->volume(volume);

    bool showSubtitle = this->m_settings.value(SETTING_SHOW_SUBTITLE, this->m_screen->isShowSubtitle()).toBool();
    this->m_screen->showSubtitle(showSubtitle);

    bool useNormalizer = this->m_settings.value(SETTING_USE_NORMALIZER, this->m_screen->isUsingNormalizer()).toBool();
    this->m_screen->useNormalizer(useNormalizer);

    AnyVODEnums::PlayingMethod method = (AnyVODEnums::PlayingMethod)this->m_settings.value(SETTING_PLAYING_METHOD, (int)this->m_screen->getPlayingMethod()).toInt();
    this->m_screen->setPlayingMethod(method);

    AnyVODEnums::DeinterlaceMethod deMethod = (AnyVODEnums::DeinterlaceMethod)this->m_settings.value(SETTING_DEINTERLACER_METHOD, this->m_screen->getDeinterlaceMethod()).toInt();
    this->m_screen->setDeinterlaceMethod(deMethod);

    AnyVODEnums::DeinterlaceAlgorithm deAlgorithm = (AnyVODEnums::DeinterlaceAlgorithm)this->m_settings.value(SETTING_DEINTERLACER_ALGORITHM, this->m_screen->getDeinterlaceAlgorithm()).toInt();
    this->m_screen->setDeinterlaceAlgorithm(deAlgorithm);

    AnyVODEnums::HAlignMethod halign = (AnyVODEnums::HAlignMethod)this->m_settings.value(SETTING_HALIGN_SUBTITLE, this->m_screen->getHAlign()).toInt();
    this->m_screen->setHAlign(halign);

    bool seekKeyFrame = this->m_settings.value(SETTING_SEEK_KEYFRAME, this->m_screen->isSeekKeyFrame()).toBool();
    this->m_screen->setSeekKeyFrame(seekKeyFrame);

    this->m_screen->setCaptureDirectory(this->m_settings.value(SETTING_SCREEN_CAPTURE_DIR, this->m_screen->getCaptureDirectory()).toString());
    this->m_screen->selectCaptureExt(this->m_settings.value(SETTING_SCREEN_CAPTURE_EXT, this->m_screen->getCaptureExt()).toString());

    QDir capDir(this->m_screen->getCaptureDirectory());

    if (!capDir.exists())
        this->m_screen->setCaptureDirectory(ScreenCaptureHelper::getDefaultSavePath());

    bool searchSubtitle = this->m_settings.value(SETTING_ENABLE_SEARCH_SUBTITLE, this->m_screen->isEnableSearchSubtitle()).toBool();
    this->m_screen->enableSearchSubtitle(searchSubtitle);

    bool searchLyrics = this->m_settings.value(SETTING_ENABLE_SEARCH_LYRICS, this->m_screen->isEnableSearchLyrics()).toBool();
    this->m_screen->enableSearchLyrics(searchLyrics);

    AnyVODEnums::VAlignMethod valign = (AnyVODEnums::VAlignMethod)this->m_settings.value(SETTING_VALIGN_SUBTITLE, this->m_screen->getVAlign()).toInt();
    this->m_screen->setVAlign(valign);

    float subtitleSize = (float)this->m_settings.value(SETTING_SUBTITLE_SIZE, this->m_screen->getSubtitleSize()).toFloat();
    this->m_screen->setSubtitleSize(subtitleSize);

    QSizeF aspectSize = this->m_screen->getCustomUserAspectRatio();
    QSizeF aspectRatio = this->m_settings.value(SETTING_USER_ASPECT_RATIO, aspectSize).toSizeF();
    this->m_screen->setCustomUserAspectRatio(aspectRatio);

    bool useHWDecoder = this->m_settings.value(SETTING_USE_HW_DECODER, this->m_screen->isUseHWDecoder()).toBool();
    this->m_screen->useHWDecoder(useHWDecoder);

    bool showAlbumJacket = this->m_settings.value(SETTING_SHOW_ALBUM_JACKET, this->m_screen->isShowAlbumJacket()).toBool();
    this->m_screen->showAlbumJacket(showAlbumJacket);

    bool enableLastPlay = this->m_settings.value(SETTING_ENABLE_LAST_PLAY, this->m_screen->isGotoLastPos()).toBool();
    this->m_screen->enableGotoLastPos(enableLastPlay, true);

    GlobalSubtitleCodecName::getInstance().setCodecName(this->m_settings.value(SETTING_TEXT_ENCODING, QTextCodec::codecForLocale()->name()).toString());

    bool useFrameDrop = this->m_settings.value(SETTING_USE_FRAME_DROP, this->m_screen->isUseFrameDrop()).toBool();
    this->m_screen->useFrameDrop(useFrameDrop);

    QStringList subtitleDirectory;
    bool priorSubtitleDirectory;

    this->m_screen->getSubtitleDirectory(&subtitleDirectory, &priorSubtitleDirectory);

    subtitleDirectory = this->m_settings.value(SETTING_SUBTITLE_DIRECTORY, subtitleDirectory).toStringList();
    priorSubtitleDirectory = this->m_settings.value(SETTING_PRIOR_SUBTITLE_DIRECTORY, priorSubtitleDirectory).toBool();

    this->m_screen->setSubtitleDirectory(subtitleDirectory, priorSubtitleDirectory);

    bool searchSubtitleComplex = this->m_settings.value(SETTING_SEARCH_SUBTITLE_COMPLEX, this->m_screen->getSearchSubtitleComplex()).toBool();
    this->m_screen->setSearchSubtitleComplex(searchSubtitleComplex);

    bool useBufferingMode = this->m_settings.value(SETTING_USE_BUFFERING_MODE, this->m_screen->isUseBufferingMode()).toBool();
    this->m_screen->useBufferingMode(useBufferingMode);

    bool useSubtitleCacheMode = this->m_settings.value(SETTING_USE_SUBTITLE_CACHE_MODE, this->m_screen->isUseSubtitleCacheMode()).toBool();
    this->m_screen->useSubtitleCacheMode(useSubtitleCacheMode);

    bool useLowQualityMode = this->m_settings.value(SETTING_USE_LOW_QUALITY_MODE, this->m_screen->isUseLowQualityMode()).toBool();
    this->m_screen->useLowQualityMode(useLowQualityMode);

    int audioDevice = this->m_settings.value(SETTING_AUDIO_DEVICE, this->m_screen->getCurrentAudioDevice()).toInt();
    this->m_screen->setAudioDevice(audioDevice);

    int spdifDevice = this->m_settings.value(SETTING_SPDIF_DEVICE, this->m_screen->getCurrentSPDIFAudioDevice()).toInt();
    QStringList tmpList = this->m_screen->getSPDIFAudioDevices();
    this->m_screen->setSPDIFAudioDevice(spdifDevice);

    AnyVODEnums::SPDIFEncodingMethod spdifEncodingMethod = (AnyVODEnums::SPDIFEncodingMethod)this->m_settings.value(SETTING_SPDIF_ENCODING, this->m_screen->getSPDIFEncodingMethod()).toInt();
    this->m_screen->setSPDIFEncodingMethod(spdifEncodingMethod);

    int spdifSampleRate = this->m_settings.value(SETTING_SPDIF_SAMPLE_RATE, SPDIFSampleRates::getInstance().getSelectedSampleRate()).toInt();
    SPDIFSampleRates::getInstance().selectSampleRate(spdifSampleRate);

    bool useSPDIF = this->m_settings.value(SETTING_USE_SPDIF, this->m_screen->isUseSPDIF()).toBool();
    this->m_screen->useSPDIF(useSPDIF);

    bool useDistortion = this->m_settings.value(SETTING_VR_USE_DISTORTION, this->m_screen->isUseDistortion()).toBool();
    this->m_screen->useDistortion(useDistortion);

    QVector2D barrelCoefficients(this->m_settings.value(SETTING_VR_BARREL_DISTORTION_COEFFICIENTS, this->m_screen->getBarrelDistortionCoefficients().toPointF()).toPointF());
    this->m_screen->setBarrelDistortionCoefficients(barrelCoefficients);

    QVector2D pincushionCoefficients(this->m_settings.value(SETTING_VR_PINCUSHION_DISTORTION_COEFFICIENTS, this->m_screen->getPincushionDistortionCoefficients().toPointF()).toPointF());
    this->m_screen->setPincushionDistortionCoefficients(pincushionCoefficients);

    QVector2D lensCenter(this->m_settings.value(SETTING_VR_DISTORTION_LENS_CENTER, this->m_screen->getDistortionLensCenter().toPointF()).toPointF());
    this->m_screen->setDistortionLensCenter(lensCenter);

    bool autoSaveSearchLyrics = this->m_settings.value(SETTING_AUTO_SAVE_SEARCH_LYRICS, this->m_screen->isAutoSaveSearchLyrics()).toBool();
    this->m_screen->enableAutoSaveSearchLyrics(autoSaveSearchLyrics);

    double bluetoothSync = this->m_settings.value(SETTING_BLUETOOTH_AUDIO_SYNC, this->m_screen->getBluetoothHeadsetSync()).toDouble();
    this->m_screen->setBluetoothHeadsetSync(bluetoothSync);

    this->loadEqualizer();
}

void RenderScreenSettingLoader::loadEqualizer() const
{
    bool useEqualizer = this->m_settings.value(SETTING_USE_EQUALIZER, this->m_screen->isUsingEqualizer()).toBool();
    this->m_screen->useEqualizer(useEqualizer);

    float preamp = this->m_settings.value(SETTING_EQUALIZER_PREAMP, this->m_screen->getPreAmp()).toFloat();
    this->m_screen->setPreAmp(preamp);

    QVariantList list = this->m_settings.value(SETTING_EQUALIZER_GAINS, QVariantList()).toList();

    for (int i = 0; i < list.count(); i++)
        this->m_screen->setEqualizerGain(i, list[i].value<RenderScreen::EqualizerItem>().gain);
}
