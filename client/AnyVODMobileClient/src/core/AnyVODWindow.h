﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "qtquick2applicationviewer.h"

class AnyVODWindow : public QtQuick2ApplicationViewer
{
    Q_OBJECT
public:
    explicit AnyVODWindow();

    void setUseCustomControl(bool use);
    bool getUseCustomControl() const;

    void setAcceptEvent(bool accept);

public:
    static AnyVODWindow* getInstance();

private:
    virtual bool event(QEvent *event);

private:
    static AnyVODWindow *INSTANCE;

private:
    bool m_useCustomControl;
    bool m_accept;
};
