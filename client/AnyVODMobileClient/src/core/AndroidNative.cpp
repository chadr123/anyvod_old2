﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ui/RenderScreen.h"
#include "models/PlayListModel.h"
#include "AutoStart.h"

#include <jni.h>
#include <android/log.h>

#include <QAndroidJniObject>
#include <QDebug>

static bool g_isPausedByNative = false;

static void onResumeMediaOwnState(JNIEnv * /*env*/, jobject /*this*/)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return;

    if (!g_isPausedByNative)
        return;

    if (screen->getStatus() == RenderScreen::Paused)
    {
        g_isPausedByNative = false;
        screen->resume();
    }
}

static void onPauseMediaOwnState(JNIEnv * /*env*/, jobject /*this*/)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return;

    if (screen->getStatus() == RenderScreen::Playing || screen->getStatus() == RenderScreen::Started)
    {
        g_isPausedByNative = true;
        screen->pause();
    }
}

static void onResumeMedia(JNIEnv * /*env*/, jobject /*this*/)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return;

    if (screen->getStatus() == RenderScreen::Paused)
        screen->resume();
}

static void onPauseMedia(JNIEnv * /*env*/, jobject /*this*/)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return;

    if (screen->getStatus() == RenderScreen::Playing || screen->getStatus() == RenderScreen::Started)
        screen->pause();
}

static void onToggleMedia(JNIEnv * /*env*/, jobject /*this*/)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return;

    if (screen->isPlayOrPause())
        screen->toggle();
}

static void onStopMedia(JNIEnv * /*env*/, jobject /*this*/)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return;

    if (screen->isPlayOrPause())
        screen->closeAndGoBack();
}

static void onRewindMedia(JNIEnv * /*env*/, jobject /*this*/)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return;

    if (screen->isPlayOrPause())
        screen->rewindProper();
}

static void onForwardMedia(JNIEnv * /*env*/, jobject /*this*/)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return;

    if (screen->isPlayOrPause())
        screen->forwardProper();
}

static void onPreviousMedia(JNIEnv * /*env*/, jobject /*this*/)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return;

    if (screen->isPlayOrPause())
        screen->prev(false);
}

static void onNextMedia(JNIEnv * /*env*/, jobject /*this*/)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return;

    if (screen->isPlayOrPause())
        screen->next(false);
}

static void onAudioRouteSelected(JNIEnv * /*env*/, jobject /*this*/, jboolean isBluetooth)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return;

    screen->setBluetoothHeadsetConnected(isBluetooth == JNI_TRUE ? true : false);
}

static jboolean isVideo(JNIEnv * /*env*/, jobject /*this*/)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return JNI_FALSE;

    return screen->isVideo() ? JNI_TRUE : JNI_FALSE;
}

static jboolean isValid(JNIEnv * /*env*/, jobject /*this*/)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return JNI_FALSE;

    return screen->isValid() ? JNI_TRUE : JNI_FALSE;
}

static jstring getTitle(JNIEnv *env, jobject /*this*/)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return env->NewStringUTF("");

    return env->NewStringUTF(screen->getTitle().toUtf8());
}

static jstring getArtist(JNIEnv *env, jobject /*this*/)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return env->NewStringUTF("");

    return env->NewStringUTF(screen->getArtist().toUtf8());
}

static jlong getCurrentPosition(JNIEnv * /*env*/, jobject /*this*/)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return 0;

    return screen->getCurrentPosition() * 1000;
}

static jlong getDuration(JNIEnv * /*env*/, jobject /*this*/)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return 0;

    return screen->getDuration() * 1000;
}

static jlong getTotalPlayListCount(JNIEnv * /*env*/, jobject /*this*/)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return -1;

    return screen->getPlayList()->getCount();
}

static jlong getCurrentPlayIndex(JNIEnv * /*env*/, jobject /*this*/)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return -1;

    return screen->getPlayList()->getCurrentIndex();
}

static jstring getCoverFilePath(JNIEnv *env, jobject /*this*/)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return env->NewStringUTF("");

    return env->NewStringUTF(screen->getLargeCoverFilePath().toUtf8());
}

static void setStartingURL(JNIEnv * /*env*/, jobject /*this*/, jstring url)
{
    QAndroidJniObject javaURL(url);

    AutoStart::getInstance().setStartingURL(javaURL.toString());
}

static JNINativeMethod methods[] =
{
    {"onResumeMediaOwnState",       "()V",                          (void*)onResumeMediaOwnState},
    {"onPauseMediaOwnState",        "()V",                          (void*)onPauseMediaOwnState},
    {"onResumeMedia",               "()V",                          (void*)onResumeMedia},
    {"onPauseMedia",                "()V",                          (void*)onPauseMedia},
    {"onToggleMedia",               "()V",                          (void*)onToggleMedia},
    {"onStopMedia",                 "()V",                          (void*)onStopMedia},
    {"onRewindMedia",               "()V",                          (void*)onRewindMedia},
    {"onForwardMedia",              "()V",                          (void*)onForwardMedia},
    {"onPreviousMedia",             "()V",                          (void*)onPreviousMedia},
    {"onNextMedia",                 "()V",                          (void*)onNextMedia},
    {"onAudioRouteSelected",        "(Z)V",                         (void*)onAudioRouteSelected},
    {"isVideo",                     "()Z",                          (void*)isVideo},
    {"isValid",                     "()Z",                          (void*)isValid},
    {"getTitle",                    "()Ljava/lang/String;",         (void*)getTitle},
    {"getArtist",                   "()Ljava/lang/String;",         (void*)getArtist},
    {"getCurrentPosition",          "()J",                          (void*)getCurrentPosition},
    {"getDuration",                 "()J",                          (void*)getDuration},
    {"getTotalPlayListCount",       "()J",                          (void*)getTotalPlayListCount},
    {"getCurrentPlayIndex",         "()J",                          (void*)getCurrentPlayIndex},
    {"getCoverFilePath",            "()Ljava/lang/String;",         (void*)getCoverFilePath},
    {"setStartingURL",              "(Ljava/lang/String;)V",        (void*)setStartingURL},
};

JNIEXPORT jint JNI_OnLoad(JavaVM *vm, void * /*reserved*/)
{
    JNIEnv *env;

    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK)
        return JNI_ERR;

    jclass javaClass = env->FindClass("com/dcple/anyvod/NativeFunctions");

    if (!javaClass)
        return JNI_ERR;

    if (env->RegisterNatives(javaClass, methods, sizeof(methods) / sizeof(methods[0])) < 0)
        return JNI_ERR;

    return JNI_VERSION_1_6;
}
