﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "AnyVODWindow.h"

#include <QDebug>

AnyVODWindow* AnyVODWindow::INSTANCE = nullptr;

AnyVODWindow::AnyVODWindow() :
    m_useCustomControl(false),
    m_accept(true)
{
    if (!INSTANCE)
        INSTANCE = this;
}

void AnyVODWindow::setUseCustomControl(bool use)
{
    this->m_useCustomControl = use;
}

bool AnyVODWindow::getUseCustomControl() const
{
    return this->m_useCustomControl;
}

void AnyVODWindow::setAcceptEvent(bool accept)
{
    this->m_accept = accept;
}

AnyVODWindow* AnyVODWindow::getInstance()
{
    return INSTANCE;
}

bool AnyVODWindow::event(QEvent *event)
{
    if (this->m_useCustomControl)
    {
        if (event->type() == QEvent::UpdateRequest)
        {
            if (this->m_accept)
            {
                event->accept();
                return true;
            }
        }
    }

    return QQuickView::event(event);
}
