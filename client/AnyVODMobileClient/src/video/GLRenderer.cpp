﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "GLRenderer.h"
#include "media/MediaPresenter.h"
#include "video/ShaderCompositer.h"
#include "core/AnyVODWindow.h"
#include "core/Common.h"
#include "utils/PathUtils.h"
#include "utils/SeparatingUtils.h"
#include "utils/ConvertingUtils.h"
#include "ui/RenderScreen.h"

#include <qmath.h>

#include <QOpenGLFramebufferObject>
#include <QDir>
#include <QDebug>

const QStringList GLRenderer::BGRA_BLACK_LIST = (QStringList() <<
                                                 "Mali-T760" <<
                                                 "Apple A7 GPU");

GLRenderer::GLRenderer(MediaPresenter *presenter, RenderScreen *parent) :
    m_presenter(presenter),
    m_offScreen(nullptr),
    m_parent(parent),
    m_wantToCleanup(false),
    m_rebuildShader(false),
    m_isReady(false),
    m_isReadyToRender(false),
    m_setupColorConversion(CC_NONE)
{
    this->m_captureHelper.setSavePath(ScreenCaptureHelper::getDefaultSavePath());
    PathUtils::createDirectory(this->m_captureHelper.getSavePath());
}

GLRenderer::~GLRenderer()
{

}

void GLRenderer::scheduleRebuildShader()
{
    this->m_rebuildShader = true;
}

void GLRenderer::setViewPortSize(const QSize &size, AnyVODWindow *window)
{
    if (!this->m_isReady)
    {
        this->initializeOpenGLFunctions();
        this->m_presenter->setGL(this);

        this->setup();

        this->m_window = window;
        this->m_isReady = true;
        this->m_isReadyToRender = true;
    }

    if (this->m_viewPortSize != size)
    {
        glViewport(0, 0, size.width(), size.height());

        this->m_viewPortSize = size;

        this->m_presenter->setGL(this);
        this->m_presenter->resetScreen(size.width(), size.height(), this->m_texInfo, true);
    }
}

ScreenCaptureHelper& GLRenderer::getCaptureHelper()
{
    return this->m_captureHelper;
}

const ScreenCaptureHelper& GLRenderer::getCaptureHelper() const
{
    return this->m_captureHelper;
}

void GLRenderer::scheduleCleanup()
{
    this->m_wantToCleanup = true;
}

void GLRenderer::scheduleUseColorConversion(ColorConversion use)
{
    this->m_setupColorConversion = use;
}

void GLRenderer::deleteTexture(TextureID type)
{
    TextureInfo &info = this->m_texInfo[type];

    for (unsigned int i = 0; i < info.textureCount; i++)
    {
        if (info.id[i])
        {
            glDeleteTextures(1, &info.id[i]);
            info.id[i] = 0;
        }
    }
}

void GLRenderer::genTexture(TextureID type)
{
    TextureInfo &info = this->m_texInfo[type];

    info.textureCount = 1;

    for (unsigned int i = 0; i < info.textureCount; i++)
    {
        glGenTextures(1, &info.id[i]);

        glBindTexture(GL_TEXTURE_2D, info.id[i]);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }

    glBindTexture(GL_TEXTURE_2D, 0);
}

void GLRenderer::genTextureLuma(TextureID type)
{
    TextureInfo &info = this->m_texInfo[type];

    info.textureCount = MAX_TEXTURE_COUNT;

    for (unsigned int i = 0; i < info.textureCount; i++)
    {
        glGenTextures(1, &info.id[i]);

        glBindTexture(GL_TEXTURE_2D, info.id[i]);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }

    glBindTexture(GL_TEXTURE_2D, 0);
}

void GLRenderer::initTextures()
{
    GLint maxSize;

    int width;
    int height;

    if (this->m_presenter->getFrameSize(&width, &height))
    {
        maxSize = std::max(width, height);

        int powValue;

        frexp(maxSize, &powValue);
        maxSize = ::pow(2, powValue);
    }
    else
    {
        maxSize = DEFAULT_MIN_TEXTURE_SIZE;
    }

    glEnable(GL_TEXTURE_2D);

    for (int i = 0; i < TEX_COUNT; i++)
        this->deleteTexture((TextureID)i);

    this->genTexture(TEX_MOVIE_FRAME);
    this->genTexture(TEX_FFMPEG_SUBTITLE);
    this->genTexture(TEX_ASS_SUBTITLE);

    this->genTextureLuma(TEX_YUV_0);
    this->genTextureLuma(TEX_YUV_1);
    this->genTextureLuma(TEX_YUV_2);

    this->genOffScreen(maxSize);

    this->m_presenter->setMaxTextureSize(maxSize);
}

void GLRenderer::genOffScreen(int size)
{
    if (this->m_offScreen)
        delete this->m_offScreen;

    this->m_offScreen = new QOpenGLFramebufferObject(size, size);

    glBindTexture(GL_TEXTURE_2D, this->m_offScreen->texture());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBindTexture(GL_TEXTURE_2D, 0);

    this->m_offScreen->release();
}

bool GLRenderer::isForceRGBA()
{
    const char *renderer = (const char*)glGetString(GL_RENDERER);

    return BGRA_BLACK_LIST.contains(renderer, Qt::CaseInsensitive);
}

void GLRenderer::capturePrologue(const QSize &orgSize)
{
    int width;
    int height;

    if (this->m_captureHelper.getCapturedCount() <= 0)
    {
        if (this->m_parent->getStatus() == RenderScreen::Paused)
            this->m_captureHelper.markPaused();
        else
            this->m_captureHelper.unmarkPaused();

        if (this->m_captureHelper.isPaused())
            this->m_presenter->resume();
    }

    if (this->m_captureHelper.isCaptureByOriginalSize())
    {
        if (!this->m_presenter->getFrameSize(&width, &height))
        {
            width = orgSize.width();
            height = orgSize.height();
        }
    }
    else
    {
        QSize size = this->m_captureHelper.getSize();

        width = size.width();
        height = size.height();
    }

    this->setViewPortSize(QSize(width, height), this->m_window);
}

void GLRenderer::captureEpilogue(bool captureBound, const QSize &orgSize)
{
    QSize offSize = this->m_offScreen->size();
    QRect rect;
    QSize targetSize;
    QRect captureRect;
    int targetWidth;
    int targetHeight;

    this->m_presenter->pause();

    if (this->m_captureHelper.isCaptureByOriginalSize())
    {
        targetWidth = orgSize.width();
        targetHeight = orgSize.height();

        this->m_presenter->getFrameSize(&targetWidth, &targetHeight);
    }
    else
    {
        QSize size = this->m_captureHelper.getSize();

        targetWidth = size.width();
        targetHeight = size.height();
    }

    targetSize = QSize(targetWidth, targetHeight);
    captureRect = QRect(0, offSize.height() - targetHeight, targetWidth, targetHeight);

    this->setViewPortSize(orgSize, this->m_window);
    this->m_presenter->getPictureRect(&rect);

    GLfloat width = targetSize.width() / (GLfloat)this->m_offScreen->width();
    GLfloat height = targetSize.height() / (GLfloat)this->m_offScreen->height();

    glClear(GL_COLOR_BUFFER_BIT);

    glBindTexture(GL_TEXTURE_2D, this->m_offScreen->texture());

    this->renderCapturePicture(rect, QSizeF(width, height));

    glBindTexture(GL_TEXTURE_2D, 0);

    QImage captured;
    QString path("%1%2_%3_%4.%5");
    QString time;
    QString savePath = this->m_captureHelper.getSavePath();

    if (this->isForceRGBA())
    {
        captureBound = this->m_offScreen->bind();

        if (captureBound)
        {
            QImage rgbaImage(offSize, QImage::Format_RGBA8888_Premultiplied);

            glReadPixels(0, 0, offSize.width(), offSize.height(), GL_RGBA, GL_UNSIGNED_BYTE, rgbaImage.bits());
            captured = rgbaImage.mirrored();

            this->m_offScreen->release();
        }
    }
    else
    {
        captured = this->m_offScreen->toImage();
    }

    PathUtils::createDirectory(savePath);
    SeparatingUtils::appendDirSeparator(&savePath);
    ConvertingUtils::getTimeString(this->m_presenter->getCurrentPosition(), ConvertingUtils::TIME_HH_MM_SS_ZZZ_DIR, &time);

    QString fileName;

    if (this->m_presenter->isPlayUserDataEmpty())
        fileName = QFileInfo(this->m_presenter->getRealFilePath()).baseName();
    else
        fileName = this->m_presenter->getTitle();

    path = path.arg(savePath, fileName, time)
            .arg(this->m_captureHelper.getCapturedCount())
            .arg(this->m_captureHelper.getExtention());

    path = QDir::toNativeSeparators(path);
    captured = captured.copy(captureRect);

    QString desc;

    if (captureBound && captured.save(path, nullptr, this->m_captureHelper.getQuality()))
        desc = tr("캡쳐 성공");
    else
        desc = tr("캡쳐 실패");

    desc += " : " + QFileInfo(path).fileName();

    this->m_presenter->resume();
    this->m_presenter->showOptionDesc(desc);

    this->m_captureHelper.reduceRemainedCount();

    if (!this->m_captureHelper.isRemained())
    {
        this->m_captureHelper.stop();

        if (this->m_captureHelper.isPaused())
            this->m_presenter->pause();
    }

    if (!this->m_parent->isPlayOrPause())
        this->m_captureHelper.stop();
}

void GLRenderer::renderCapturePicture(const QRect &picArea, const QSizeF &texSize)
{
    QVector4D vColor(1.0, 1.0, 1.0, 1.0);
    QVector3D vertices[] =
    {
        QVector3D(picArea.left(), picArea.bottom(), 0.0f),
        QVector3D(picArea.right(), picArea.bottom(), 0.0f),
        QVector3D(picArea.left(), picArea.top(), 0.0f),
        QVector3D(picArea.right(), picArea.top(), 0.0f)
    };
    QVector2D texCoords[] =
    {
        QVector2D(0.0, 0.0),
        QVector2D(texSize.width(), 0.0),
        QVector2D(0.0, texSize.height()),
        QVector2D(texSize.width(), texSize.height())
    };

    ShaderCompositer &shader = ShaderCompositer::getInstance();
    bool enabledBlend = glIsEnabled(GL_BLEND);
    QMatrix4x4 ortho;

    shader.startSimple();

    glDisable(GL_BLEND);

    ortho.ortho(0.0f, this->m_viewPortSize.width(), this->m_viewPortSize.height(), 0.0f, -1.0f, 1.0f);
    shader.setRenderData(ShaderCompositer::ST_SIMPLE, ortho, QMatrix4x4(), vertices, texCoords, QRectF(), vColor, AV_PIX_FMT_NONE);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    shader.endSimple();

    if (enabledBlend)
        glEnable(GL_BLEND);
}

void GLRenderer::paint()
{
    ShaderCompositer &shader = ShaderCompositer::getInstance();

    glClearColor(0.0, 0.0, 0.0, 1.0);

    if (this->m_wantToCleanup)
    {
        glClear(GL_COLOR_BUFFER_BIT);

        this->cleanup();
        this->m_wantToCleanup = false;
        this->m_isReadyToRender = false;

        emit this->m_parent->exitFromRenderer();

        return;
    }

    if (!this->m_isReadyToRender)
        return;

    bool captureBound = false;
    bool capture = this->m_captureHelper.isStarted();
    QSize orgViewPort;

    glViewport(0, 0, this->m_viewPortSize.width(), this->m_viewPortSize.height());

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_STENCIL_TEST);

    if (this->m_setupColorConversion != CC_NONE)
    {
        shader.useColorConversion(this->m_setupColorConversion == CC_ON);
        this->m_rebuildShader = true;
        this->m_setupColorConversion = CC_NONE;
    }

    if (this->m_rebuildShader)
    {
        shader.build();
        this->m_rebuildShader = false;
    }

    this->m_window->setAcceptEvent(true);

    if (capture)
    {
        orgViewPort = this->m_viewPortSize;

        this->m_presenter->setCaptureMode(true);
        this->capturePrologue(orgViewPort);

        captureBound = this->m_offScreen->bind();
    }

    this->m_presenter->render();

    if (capture)
    {
        this->m_offScreen->release();

        this->captureEpilogue(captureBound, orgViewPort);
        this->m_presenter->setCaptureMode(false);
    }

    if (capture && !captureBound)
        this->m_presenter->showOptionDesc(tr("캡쳐를 시작하지 못했습니다"));
}

void GLRenderer::cleanup()
{
    for (int i = 0; i < TEX_COUNT; i++)
        this->deleteTexture((TextureID)i);

    this->m_presenter->clearFonts();
    this->m_presenter->clearFrameBuffers();

    if (this->m_offScreen)
    {
        delete this->m_offScreen;
        this->m_offScreen = nullptr;
    }

    ShaderCompositer::getInstance().deInitShaders();
}

void GLRenderer::setup()
{
    ShaderCompositer &shader = ShaderCompositer::getInstance();

    this->initTextures();

    shader.initShaders();
    shader.build();
}
