﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0
import "." as AnyVOD

Rectangle {
    id: topRoot
    focus: true

    property MainSettingDelegate query: null
    property MediaPlayerSettingDelegate setting: null
    property bool interactMode: false
    property bool playingMode: false
    property int type: -100
    property real roundValue: 10
    property real defaultValue: 0
    property real defaultStartValue: 0
    property real scaleValue: 1
    property string unitPostfix: ""
    property alias sliderWidth: root.width
    property alias sliderHeight: root.height
    property alias title: header.title
    property alias desc: desc.text
    property alias slider: sliderItem
    property alias label: sliderItemValue

    signal goBack
    signal interactModeSelected
    signal playingModeSelected

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: goBack()
        onCanceled: goBack()
    }

    Rectangle {
        id: root
        width: parent.width * 0.8
        height: Math.min(parent.height * 0.7, 210)
        radius: 3
        border.color: "gray"

        function getSliderValue()
        {
            var value = sliderItem.value * scaleValue + defaultStartValue

            return value.toFixed(roundValue) + unitPostfix
        }

        function reload()
        {
            if (query)
                sliderItem.value = query.getSetting(type)
            else
                sliderItem.value = setting.getSetting(type)

            sliderItemValue.text = getSliderValue()
        }

        MouseArea {
            anchors.fill: parent

            ColumnLayout {
                anchors.fill: parent
                spacing: 0

                Header {
                    id: header
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight
                }

                Rectangle {
                    id: sliderRoot
                    color: "white"
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    ColumnLayout {
                        anchors.fill: parent
                        spacing: 0

                        Rectangle {
                            color: "transparent"
                            Layout.fillWidth: true
                            Layout.minimumHeight: 10
                            Layout.maximumHeight: Layout.minimumHeight
                        }

                        RowLayout {
                            Layout.fillWidth: true
                            Layout.minimumHeight: 30
                            Layout.maximumHeight: Layout.minimumHeight

                            Rectangle {
                                color: "transparent"
                                Layout.minimumWidth: 10
                                Layout.maximumWidth: Layout.minimumWidth
                                Layout.fillHeight: true
                            }

                            Text {
                                id: desc
                                color: "gray"
                                wrapMode: Text.WordWrap
                                Layout.fillWidth: true
                                Layout.fillHeight: true
                            }

                            Rectangle {
                                color: "transparent"
                                Layout.minimumWidth: 10
                                Layout.maximumWidth: Layout.minimumWidth
                                Layout.fillHeight: true
                            }

                            Rectangle {
                                id: resetBox
                                anchors.topMargin: 10
                                color: "gray"
                                Layout.minimumWidth: reset.width + 20
                                Layout.maximumWidth: Layout.minimumWidth
                                Layout.fillHeight: true

                                ImageButton {
                                    id: reset
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    height: parent.height
                                    text.text: qsTr("초기화")
                                    text.color: "white"
                                    text.verticalAlignment: Text.AlignVCenter
                                    text.horizontalAlignment: Text.AlignHCenter

                                    onClicked: {
                                        sliderItem.value = defaultValue

                                        if (query)
                                            query.setSetting(type, sliderItem.value)
                                        else
                                            setting.setSetting(type, sliderItem.value)

                                        root.reload()
                                    }
                                }
                            }

                            Rectangle {
                                color: "transparent"
                                Layout.minimumWidth: 10
                                Layout.maximumWidth: Layout.minimumWidth
                                Layout.fillHeight: true
                            }
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.fillWidth: true
                            Layout.minimumHeight: 20
                            Layout.maximumHeight: Layout.minimumHeight
                        }

                        RowLayout {
                            Layout.fillWidth: true
                            Layout.minimumHeight: 25
                            Layout.maximumHeight: Layout.minimumHeight

                            Rectangle {
                                color: "transparent"
                                Layout.minimumWidth: 10
                                Layout.maximumWidth: Layout.minimumWidth
                                Layout.fillHeight: true
                            }

                            ImageButton {
                                id: decrease
                                Layout.minimumWidth: parent.height
                                Layout.maximumWidth: Layout.minimumWidth
                                Layout.fillHeight: true
                                Layout.alignment: Qt.AlignHCenter

                                Rectangle {
                                    anchors.fill: parent
                                    radius: width
                                    color: "lightgreen"

                                    Text {
                                        anchors.fill: parent
                                        text: "-"
                                        font.bold: true
                                        color: "white"
                                        verticalAlignment: Text.AlignVCenter
                                        horizontalAlignment: Text.AlignHCenter
                                    }
                                }

                                onClicked: {
                                    slider.value -= slider.stepSize
                                }
                            }

                            AnyVOD.Slider {
                                id: sliderItem
                                backgroundHeightRatio: 0.2
                                backgroundLeftMargin: 6
                                handleBorderWidth: 2
                                Layout.fillWidth: true
                                Layout.fillHeight: true

                                Component.onCompleted: {
                                    root.reload()
                                }

                                onValueChanged: {
                                    if (query)
                                        query.setSetting(type, value)
                                    else
                                        setting.setSetting(type, value)

                                    sliderItemValue.text = root.getSliderValue()
                                }
                            }

                            Text {
                                id: sliderItemValue
                                verticalAlignment: Text.AlignVCenter
                                horizontalAlignment: Text.AlignHCenter
                                color: "gray"
                                Layout.fillHeight: true
                            }

                            Rectangle {
                                color: "transparent"
                                Layout.minimumWidth: 0
                                Layout.maximumWidth: Layout.minimumWidth
                                Layout.fillHeight: true
                            }

                            ImageButton {
                                id: increase
                                Layout.minimumWidth: parent.height
                                Layout.maximumWidth: Layout.minimumWidth
                                Layout.fillHeight: true
                                Layout.alignment: Qt.AlignHCenter

                                Rectangle {
                                    anchors.fill: parent
                                    radius: width
                                    color: "lightgreen"

                                    Text {
                                        anchors.fill: parent
                                        text: "+"
                                        font.bold: true
                                        color: "white"
                                        verticalAlignment: Text.AlignVCenter
                                        horizontalAlignment: Text.AlignHCenter
                                    }
                                }

                                onClicked: {
                                    slider.value += slider.stepSize
                                }
                            }

                            Rectangle {
                                color: "transparent"
                                Layout.minimumWidth: 10
                                Layout.maximumWidth: Layout.minimumWidth
                                Layout.fillHeight: true
                            }
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.fillWidth: true
                            Layout.minimumHeight: 10
                            Layout.maximumHeight: Layout.minimumHeight
                        }
                    }
                }

                FooterWithButtons {
                    types: AnyVODEnums.BT_CLOSE
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight

                    onCloseClicked: {
                        goBack()
                    }
                }
            }
        }
    }

    Timer {
        id: interactModeTimer
        interval: 1

        onTriggered: {
            interactModeSelected()
        }
    }

    Timer {
        id: playingModeTimer
        interval: 1

        onTriggered: {
            playingModeSelected()
        }
    }

    Component.onCompleted: {
        if (interactMode)
        {
            root.anchors.bottom = topRoot.bottom
            root.anchors.right = topRoot.right

            topRoot.color = "transparent"

            interactModeTimer.restart()
        }
        else
        {
            root.anchors.centerIn = topRoot
            topRoot.color = "#88000000"
        }

        if (playingMode)
            playingModeTimer.restart()
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            goBack()
        }
    }
}
