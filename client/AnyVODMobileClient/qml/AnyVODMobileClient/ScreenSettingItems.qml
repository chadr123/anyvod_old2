﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import com.dcple.anyvod 1.0

ListModel {
    ListElement { desc: qsTr("비율"); type: AnyVODEnums.SK_USER_ASPECT_RATIO_ORDER; category: qsTr("화면 비율"); check: false }
    ListElement { desc: qsTr("사용자 지정"); type: AnyVODEnums.SK_USER_ASPECT_RATIO; category: qsTr("화면 비율"); check: false }
    ListElement { desc: qsTr("영상"); type: AnyVODEnums.SK_VIDEO_ORDER; category: qsTr("영상"); check: false }
    ListElement { desc: qsTr("회전 각도"); type: AnyVODEnums.SK_SCREEN_ROTATION_DEGREE_ORDER; category: qsTr("영상"); check: false }
    ListElement { desc: qsTr("밝기"); type: AnyVODEnums.SK_BRIGHTNESS; category: qsTr("영상 속성"); check: false }
    ListElement { desc: qsTr("채도"); type: AnyVODEnums.SK_SATURATION; category: qsTr("영상 속성"); check: false }
    ListElement { desc: qsTr("색상"); type: AnyVODEnums.SK_HUE; category: qsTr("영상 속성"); check: false }
    ListElement { desc: qsTr("대비"); type: AnyVODEnums.SK_CONTRAST; category: qsTr("영상 속성"); check: false }
    ListElement { desc: qsTr("날카롭게"); type: AnyVODEnums.SK_SHARPLY; category: qsTr("영상 효과"); check: true }
    ListElement { desc: qsTr("선명하게"); type: AnyVODEnums.SK_SHARPEN; category: qsTr("영상 효과"); check: true }
    ListElement { desc: qsTr("부드럽게"); type: AnyVODEnums.SK_SOFTEN; category: qsTr("영상 효과"); check: true }
    ListElement { desc: qsTr("히스토그램 이퀄라이저"); type: AnyVODEnums.SK_HISTOGRAM_EQ; category: qsTr("영상 효과"); check: true }
    ListElement { desc: qsTr("디밴드"); type: AnyVODEnums.SK_DEBAND; category: qsTr("영상 효과"); check: true }
    ListElement { desc: qsTr("디블록"); type: AnyVODEnums.SK_DEBLOCK; category: qsTr("영상 효과"); check: true }
    ListElement { desc: qsTr("노멀라이즈"); type: AnyVODEnums.SK_VIDEO_NORMALIZE; category: qsTr("영상 효과"); check: true }
    ListElement { desc: qsTr("3D 노이즈 제거"); type: AnyVODEnums.SK_HIGH_QUALITY_3D_DENOISE; category: qsTr("영상 효과"); check: true }
    ListElement { desc: qsTr("적응 시간 평균 노이즈 제거"); type: AnyVODEnums.SK_ATA_DENOISE; category: qsTr("영상 효과"); check: true }
    ListElement { desc: qsTr("Overcomplete Wavelet 노이즈 제거"); type: AnyVODEnums.SK_OW_DENOISE; category: qsTr("영상 효과"); check: true }
    ListElement { desc: qsTr("Non-local Means 노이즈 제거"); type: AnyVODEnums.SK_NLMEANS_DENOISE; category: qsTr("영상 효과"); check: true }
    ListElement { desc: qsTr("Vague 노이즈 제거"); type: AnyVODEnums.SK_VAGUE_DENOISE; category: qsTr("영상 효과"); check: true }
    ListElement { desc: qsTr("좌우 반전"); type: AnyVODEnums.SK_LEFT_RIGHT_INVERT; category: qsTr("영상 효과"); check: true }
    ListElement { desc: qsTr("상하 반전"); type: AnyVODEnums.SK_TOP_BOTTOM_INVERT; category: qsTr("영상 효과"); check: true }
    ListElement { desc: qsTr("캡처 확장자"); type: AnyVODEnums.SK_SELECT_CAPTURE_EXT_ORDER; category: qsTr("캡처"); check: false }
    ListElement { desc: qsTr("캡처 저장 디렉토리 설정"); type: AnyVODEnums.SK_SELECT_CAPTURE_DIRECTORY; category: qsTr("캡처"); check: false }
    ListElement { desc: qsTr("활성화 기준"); type: AnyVODEnums.SK_DEINTERLACE_METHOD_ORDER; category: qsTr("디인터레이스"); check: false }
    ListElement { desc: qsTr("알고리즘"); type: AnyVODEnums.SK_DEINTERLACE_ALGORITHM_ORDER; category: qsTr("디인터레이스"); check: false }
    ListElement { desc: qsTr("하드웨어 디코더 사용"); type: AnyVODEnums.SK_USE_HW_DECODER; category: qsTr("화면"); check: true }
    ListElement { desc: qsTr("저화질 모드 사용"); type: AnyVODEnums.SK_USE_LOW_QUALITY_MODE; category: qsTr("화면"); check: true }
    ListElement { desc: qsTr("프레임 드랍 사용"); type: AnyVODEnums.SK_USE_FRAME_DROP; category: qsTr("화면"); check: true }
    ListElement { desc: qsTr("앨범 자켓 보기"); type: AnyVODEnums.SK_SHOW_ALBUM_JACKET; category: qsTr("화면"); check: true }
    ListElement { desc: qsTr("전체 해상도 사용"); type: AnyVODEnums.SK_USE_3D_FULL; category: qsTr("3D 영상"); check: true }
    ListElement { desc: qsTr("출력 방법"); type: AnyVODEnums.SK_3D_VIDEO_METHOD_ORDER; category: qsTr("3D 영상"); check: false }
    ListElement { desc: qsTr("애너글리프 알고리즘"); type: AnyVODEnums.SK_ANAGLYPH_ALGORITHM_ORDER; category: qsTr("3D 영상"); check: false }
    ListElement { desc: qsTr("입력 영상 출력 방법"); type: AnyVODEnums.SK_VR_INPUT_SOURCE_ORDER; category: qsTr("VR 영상 - 공통"); check: false }
    ListElement { desc: qsTr("왜곡 보정 사용"); type: AnyVODEnums.SK_VR_USE_DISTORTION; category: qsTr("VR 영상 - 공통"); check: true }
    ListElement { desc: qsTr("렌즈 센터 설정"); type: AnyVODEnums.SK_VR_LENS_CENTER; category: qsTr("VR 영상 - 공통"); check: false }
    ListElement { desc: qsTr("왜곡 보정 모드"); type: AnyVODEnums.SK_DISTORION_ADJUST_MODE; category: qsTr("VR 영상 - 공통"); check: false }
    ListElement { desc: qsTr("가상 3D 입체감"); type: AnyVODEnums.SK_VIRTUAL_3D_DEPTH; category: qsTr("VR 영상 - 공통"); check: false }
    ListElement { desc: qsTr("왜곡 보정 계수"); type: AnyVODEnums.SK_VR_BARREL_COEFFICIENTS; category: qsTr("VR 영상 - 평면"); check: false }
    ListElement { desc: qsTr("360도 영상 모드 사용"); type: AnyVODEnums.SK_USE_360_DEGREE; category: qsTr("VR 영상 - 360도"); check: true }
    ListElement { desc: qsTr("프로젝션 종류"); type: AnyVODEnums.SK_PROJECTION_TYPE_ORDER; category: qsTr("VR 영상 - 360도"); check: false }
    ListElement { desc: qsTr("헤드 트래킹 사용"); type: AnyVODEnums.SK_HEAD_TRACKING; category: qsTr("VR 영상 - 360도"); check: true }
    ListElement { desc: qsTr("왜곡 보정 계수"); type: AnyVODEnums.SK_VR_PINCUSHION_COEFFICIENTS; category: qsTr("VR 영상 - 360도"); check: false }
}
