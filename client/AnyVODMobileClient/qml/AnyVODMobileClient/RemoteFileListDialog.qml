﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0

Rectangle {
    id: topRoot
    focus: true
    color: "#88000000"

    property string title
    property string curDirectory
    property string selectedPath
    property alias windowWidth: root.width
    property alias windowHeight: root.height

    signal goBack
    signal accepted(var list, int index)
    signal ignore

    function updateTitle()
    {
        header.title = title

        if (curDirectory.length > 0)
            header.title += " - " + curDirectory
    }

    function resetToTop()
    {
        curDirectory = ""
        header.back.visible = false
        remoteFileListModel.clearScrollPos()
    }

    function showBackButton()
    {
        header.back.visible = true
    }

    function updateCurrentPath()
    {
        selectedPath = remoteFileListModel.curDirPath

        if (remoteFileListModel.curDirName === remoteFileListModel.topSignature)
            resetToTop()
        else
            curDirectory = remoteFileListModel.curDirName

        updateTitle()
    }

    function showMenu(menu, parent, path)
    {
        var globalCoord = parent.mapToItem(null, parent.x, parent.y)
        var showX = parent.x - menu.menuWidth + parent.width
        var showY = globalCoord.y - parent.height
        var offset = 10
        var maxHeight = showY + menu.menuHeight + offset

        if (maxHeight >= root.height)
            showY = root.height - (menu.menuHeight + offset)

        menu.path = path
        menu.show(showX, showY)
    }

    function exit()
    {
        goBack()
        ignore()
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: exit()
        onCanceled: exit()
    }

    RemoteFileListModel {
        id: remoteFileListModel

        Component.onCompleted: {
            init()
            header.back.visible = false
        }

        onModelReset: {
            noFiles.visible = rowCount() === 0
        }
    }

    Menus {
        id: menu
        menuHeight: 40
        model: RemoteDirItemMenuItems {}

        property string path

        onMenuItemSelected: {
            var item = model.get(index)

            switch (item.menuID)
            {
                case AnyVODEnums.MID_PLAY:
                    var fileList = remoteFileListModel.getFilePaths(path)

                    goBack()
                    accepted(fileList, 0)
                    break
            }
        }
    }

    Rectangle {
        id: root
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 10
        width: parent.width * 0.9
        height: parent.height * 0.8
        radius: 3
        border.color: "gray"

        MouseArea {
            anchors.fill: parent

            ColumnLayout {
                anchors.fill: parent
                spacing: 0

                HeaderWithBack {
                    id: header
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight

                    onBackClicked: {
                        var contY = remoteFileListModel.cdup()

                        remoteFileListModel.update()

                        var maxHeight = list.contentHeight - list.height

                        if (maxHeight < 0.0)
                            maxHeight = list.height

                        if (contY < 0.0)
                            contY = 0.0
                        else if (contY > maxHeight)
                            contY = maxHeight

                        list.contentY = contY

                        scrollbar.showScrollbar()
                        scrollbar.hideScrollbar()

                        menu.hide()
                        topRoot.updateCurrentPath()
                        forceActiveFocus()
                    }
                }

                Rectangle {
                    id: storageRoot
                    color: "white"
                    clip: true
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    ListView {
                        id: list
                        anchors.fill: parent
                        model: remoteFileListModel

                        delegate: Rectangle {
                            id: itemRoot
                            width: list.width
                            height: 48
                            color: storageRoot.color

                            Rectangle {
                                x: 10
                                width: parent.width - x
                                height: parent.height
                                color: "transparent"

                                MouseArea {
                                    id: mouseArea
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.fill: parent

                                    RowLayout {
                                        anchors.fill: parent
                                        spacing: 10

                                        Rectangle {
                                            id: thumbnailRoot
                                            color: "transparent"
                                            border.width: model.type === FileListModel.Directory ? 0 : 1
                                            border.color: model.type === FileListModel.Directory ? "transparent" : "#d0d0d0"
                                            Layout.minimumWidth: Math.round(Layout.minimumHeight * 1.33)
                                            Layout.maximumWidth: Layout.minimumWidth
                                            Layout.minimumHeight: parent.height - 12
                                            Layout.maximumHeight: Layout.minimumHeight
                                            Layout.alignment: Qt.AlignHCenter

                                            Image {
                                                id: thumbnail
                                                width: parent.width - thumbnailRoot.border.width * 2
                                                height: parent.height - thumbnailRoot.border.width * 2
                                                anchors.verticalCenter: parent.verticalCenter
                                                anchors.horizontalCenter: parent.horizontalCenter
                                                source: model.thumbnail
                                                fillMode: Image.PreserveAspectCrop
                                            }
                                        }

                                        ColumnLayout {
                                            spacing: 1
                                            Layout.fillWidth: true
                                            Layout.fillHeight: true
                                            Layout.alignment: Qt.AlignHCenter

                                            Text {
                                                id: fileName
                                                text: model.fileName
                                                font.pixelSize: itemRoot.height * 0.25
                                                elide: Text.ElideMiddle
                                                Layout.fillWidth: true
                                            }

                                            Text {
                                                id: fileInfo
                                                color: "red"
                                                text: model.fileInfo.length === 0 ? qsTr("갱신 중...") : model.type === FileListModel.Directory ? "" : model.fileInfo
                                                font.pixelSize: fileName.font.pixelSize
                                                elide: Text.ElideMiddle
                                                visible: model.type === FileListModel.File
                                                Layout.fillWidth: true
                                            }
                                        }

                                        ImageButton {
                                            id: openMenu
                                            image.width: 30
                                            image.height: 30
                                            visible: model.type === FileListModel.Directory
                                            source: "assets/itemMenu.png"
                                            Layout.minimumWidth: 45
                                            Layout.maximumWidth: Layout.minimumWidth
                                            Layout.minimumHeight: image.height
                                            Layout.maximumHeight: Layout.minimumHeight
                                            Layout.alignment: Qt.AlignHCenter

                                            onClicked: {
                                                topRoot.showMenu(menu, this, model.path)
                                            }
                                        }
                                    }

                                    onClicked: {
                                        topRoot.showBackButton()
                                        selectedPath = model.path

                                        forceActiveFocus()

                                        if (model.type === FileListModel.File)
                                        {
                                            var fileList = remoteFileListModel.getFileItems()
                                            var index = remoteFileListModel.getIndexByPath(fileList, model.path)

                                            goBack()
                                            accepted(fileList, index)
                                        }
                                        else
                                        {
                                            curDirectory = model.name
                                            topRoot.updateTitle()
                                            remoteFileListModel.cd(model.path, list.contentY)
                                        }
                                    }

                                    onPressed: {
                                        itemRoot.color = "lightgray"
                                        itemRoot.opacity = 0.5
                                    }

                                    onCanceled: {
                                        itemRoot.color = storageRoot.color
                                        itemRoot.opacity = 1.0
                                    }

                                    onReleased: {
                                        onCanceled()
                                    }
                                }
                            }
                        }
                    }

                    Rectangle {
                        id: noFiles
                        anchors.fill: parent
                        visible: false

                        ColumnLayout {
                            anchors.fill: parent
                            spacing: 10

                            Rectangle {
                                color: "transparent"
                                Layout.fillWidth: true
                                Layout.fillHeight: true
                            }

                            Image {
                                id: noFileImg
                                source: "assets/nofiles.png"
                                fillMode: Image.PreserveAspectFit
                                Layout.minimumHeight: 100
                                Layout.maximumHeight: Layout.minimumHeight
                                Layout.fillWidth: true
                            }

                            Text {
                                horizontalAlignment: Text.AlignHCenter
                                text: qsTr("파일 또는 디렉토리가 없습니다")
                                Layout.fillWidth: true
                            }

                            Rectangle {
                                color: "transparent"
                                Layout.fillWidth: true
                                Layout.fillHeight: true
                            }
                        }
                    }

                    Scrollbar {
                        id: scrollbar
                        flickable: list
                    }
                }

                FooterWithButtons {
                    types: AnyVODEnums.BT_RELOAD | AnyVODEnums.BT_CLOSE
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight

                    onReloadClicked: {
                        remoteFileListModel.update()
                    }

                    onCloseClicked: {
                        exit()
                    }
                }
            }
        }
    }

    Component.onCompleted: {
        updateTitle()
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            exit()
        }
    }
}
