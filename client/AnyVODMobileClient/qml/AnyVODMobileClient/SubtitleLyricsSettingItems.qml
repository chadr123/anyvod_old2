﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import com.dcple.anyvod 1.0

ListModel {
    ListElement { desc: qsTr("열기"); type: AnyVODEnums.SK_OPEN_SUBTITLE; category: qsTr("일반"); check: false }
    ListElement { desc: qsTr("외부 닫기"); type: AnyVODEnums.SK_CLOSE_EXTERNAL_SUBTITLE; category: qsTr("일반"); check: false }
    ListElement { desc: qsTr("보이기"); type: AnyVODEnums.SK_SUBTITLE_TOGGLE; category: qsTr("일반"); check: true }
    ListElement { desc: qsTr("고급 검색"); type: AnyVODEnums.SK_SEARCH_SUBTITLE_COMPLEX; category: qsTr("일반"); check: true }
    ListElement { desc: qsTr("투명도"); type: AnyVODEnums.SK_SUBTITLE_OPAQUE; category: qsTr("일반"); check: false }
    ListElement { desc: qsTr("크기"); type: AnyVODEnums.SK_SUBTITLE_SIZE; category: qsTr("일반"); check: false }
    ListElement { desc: qsTr("싱크"); type: AnyVODEnums.SK_SUBTITLE_SYNC; category: qsTr("일반"); check: false }
    ListElement { desc: qsTr("자막 캐시 모드 사용"); type: AnyVODEnums.SK_SUBTITLE_CACHE_MODE; category: qsTr("일반"); check: true }
    ListElement { desc: qsTr("가로 위치"); type: AnyVODEnums.SK_LEFTRIGHT_SUBTITLE_POSITION; category: qsTr("정렬"); check: false }
    ListElement { desc: qsTr("세로 위치"); type: AnyVODEnums.SK_UPDOWN_SUBTITLE_POSITION; category: qsTr("정렬"); check: false }
    ListElement { desc: qsTr("가로 정렬"); type: AnyVODEnums.SK_SUBTITLE_HALIGN_ORDER; category: qsTr("정렬"); check: false }
    ListElement { desc: qsTr("세로 정렬"); type: AnyVODEnums.SK_SUBTITLE_VALIGN_ORDER; category: qsTr("정렬"); check: false }
    ListElement { desc: qsTr("언어"); type: AnyVODEnums.SK_SUBTITLE_LANGUAGE_ORDER; category: qsTr("언어"); check: false }
    ListElement { desc: qsTr("인코딩"); type: AnyVODEnums.SK_OPEN_TEXT_ENCODING; category: qsTr("언어"); check: false }
    ListElement { desc: qsTr("자막 찾기"); type: AnyVODEnums.SK_ENABLE_SEARCH_SUBTITLE; category: qsTr("찾기"); check: true }
    ListElement { desc: qsTr("가사 찾기"); type: AnyVODEnums.SK_ENABLE_SEARCH_LYRICS; category: qsTr("찾기"); check: true }
    ListElement { desc: qsTr("찾은 가사 자동 저장"); type: AnyVODEnums.SK_AUTO_SAVE_SEARCH_LYRICS; category: qsTr("찾기"); check: true }
    ListElement { desc: qsTr("저장"); type: AnyVODEnums.SK_SAVE_SUBTITLE; category: qsTr("저장"); check: false }
    ListElement { desc: qsTr("다른 이름으로 저장"); type: AnyVODEnums.SK_SAVE_AS_SUBTITLE; category: qsTr("저장"); check: false }
    ListElement { desc: qsTr("가로 거리"); type: AnyVODEnums.SK_LEFTRIGHT_3D_SUBTITLE_OFFSET; category: qsTr("3D 자막"); check: false }
    ListElement { desc: qsTr("세로 거리"); type: AnyVODEnums.SK_UPDOWN_3D_SUBTITLE_OFFSET; category: qsTr("3D 자막"); check: false }
    ListElement { desc: qsTr("출력 방법"); type: AnyVODEnums.SK_3D_SUBTITLE_METHOD_ORDER; category: qsTr("3D 자막"); check: false }
}
