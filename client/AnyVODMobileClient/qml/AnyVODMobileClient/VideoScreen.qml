﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Window 2.12
import com.dcple.anyvod 1.0
import "." as AnyVOD

RenderScreen {
    id: screen
    objectName: "screen"
    focus: true
    anchors.fill: parent

    property int hookKeyCount: 0
    property bool hookKeyPressed: false
    property bool hookKeyPressedProcessing: false

    readonly property int settingListDefaultHeight: height * 0.8
    readonly property int settingListDefaultWidth: width * 0.8

    readonly property int settingListLongHeight: height * 0.9
    readonly property int settingListLongWidth: width * 0.9

    function showControlPanal()
    {
        lockButton.opacity = 1.0
        lockButton.visible = true

        if (!lockButton.checked)
        {
            controlPanel.opacity = 1.0
            controlPanel.visible = true

            screen.setOptionDescYValue()
        }

        showTimer.restart()
    }

    function hideControlPanel()
    {
        lockButton.opacity = 0.0
        controlPanel.opacity = 0.0
        hideTimer.restart()
    }

    function showPlayList()
    {
        playList.visible = true
        playList.opacity = 1
        playList.forceActiveFocus()
    }

    function hidePlayList()
    {
        playList.opacity = 0
        playListHideTimer.restart()
    }

    MediaPlayerSettingDelegate {
        id: settingDelegate
    }

    VersionDelegate {
        id: versionDelegate
    }

    StatusBarDelegate {
        id: statusBarDelegate
    }

    AudioDeviceDelegate {
        id: audioDeviceDelegate
    }

    SPDIFDelegate {
        id: spdifDelegate
    }

    CaptureExtDelegate {
        id: captureExtDelegate
    }

    FloatPointUtilDelegate {
        id: floatPointUtilDelegate
    }

    TextCodecDelegate {
        id: textCodecDelegate
    }

    PlayListModel {
        id: playListModel
        objectName: "playListModel"
    }

    PlayList {
        id: playList
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        playListModel: playListModel
        settingDelegate: settingDelegate
        screen: screen
        z: 10
        layer.enabled: true

        Timer {
            id: playListHideTimer
            interval: playListOpacityAni.duration

            onTriggered: {
                playList.visible = false
                screen.forceActiveFocus()
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: playListOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onGoBack: {
            hidePlayList()
        }

        onItemSelected: {
            screen.playAt(index)
        }

        onItemSelectedAndGoto: {
            var goLast = screen.isGotoLastPos()

            screen.enableGotoLastPos(!goLast, true)
            screen.playAt(index)
            screen.enableGotoLastPos(goLast, true)

            screen.seek(time, true)
        }

        onNewQualitySelected: {
            playListModel.selectOtherQuality(index, quality)
            itemSelected(index)
        }
    }

    AnyVOD.BusyIndicator {
        id: empty
        objectName: "empty"
        anchors.centerIn: parent
        width: 50
        height: 50
        visible: false
        running: false
    }

    RepeatRange {
        id: repeatRange
        setting: settingDelegate
        windowWidth: 300
        windowHeight: 70
    }

    Component {
        id: subtitlePopup

        AnyVOD.Popup {
            windowX: popupLoader.width - windowWidth - 20
            windowY: popupLoader.height - windowHeight - 20
            windowWidth: 240
            windowHeight: 110
            title: qsTr("자막 찾음")
            desc: popupLoader.desc
            link: popupLoader.link
        }
    }

    Component {
        id: equalizer

        Equalizer {
            title: qsTr("이퀄라이저")

            Component.onDestruction: {
                screen.loadEqualizer()
            }
        }
    }

    Component {
        id: epg

        ViewEPG {
            title: qsTr("채널 편성표")
            channel: screen.getTitle()
        }
    }

    Component {
        id: login

        Login {
            windowHeight: Math.min(settingListDefaultHeight, 220)
            windowWidth: Math.min(settingListDefaultWidth, 280)
            title: qsTr("원격 서버 로그인")

            onAccepted: {
                screen.playCurrent()
            }

            onIgnore: {
                screen.closeAndGoBack()
            }
        }
    }

    Timer {
        id: hideTimer
        interval: controlPanelOpacityAni.duration

        onTriggered: {
            lockButton.visible = false
            controlPanel.visible = false
            screen.setOptionDescY(0)
        }
    }

    Timer {
        id: showTimer
        interval: 3000

        onTriggered: {
            hideControlPanel()
        }
    }

    PinchArea {
        id: pinchArea
        y: 0
        width: parent.width
        height: parent.height - y
        enabled: !lockButton.checked
        pinch.dragAxis: Pinch.XAndYAxis

        property real orgDist: 0.0

        function distance(point1, point2)
        {
            var x = point1.x - point2.x
            var y = point1.y - point2.y

            return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2))
        }

        MouseArea {
            id: touchArea
            anchors.fill: parent

            property int axis: Drag.None
            property real threshold: 10.0
            property real thresholdY: 20.0
            property real thresholdX: 50.0
            property int volumeStart: 0

            property real startX: 0
            property real startY: 0
            property bool dragging: false
            property bool dragged: false

            property bool is360DegreeAndNoHeadTracking: false
            property point prevAngles

            property int doubleTouchThreshold: 500
            property bool touched: false
            signal doubleTouched

            onPositionChanged: {
                if (lockButton.checked)
                    return

                var p = mouse

                if (pressed)
                {
                    var xDelta = Math.abs(p.x - startX)
                    var yDelta = Math.abs(p.y - startY)

                    if (!dragging && (xDelta >= threshold || yDelta >= threshold))
                    {
                        if (xDelta > yDelta)
                        {
                            axis = Drag.XAxis
                        }
                        else
                        {
                            axis = Drag.YAxis
                            volumeStart = screen.getVolume()
                        }

                        dragging = true
                        dragged = true
                    }

                    if (dragging)
                    {
                        if (is360DegreeAndNoHeadTracking)
                        {
                            var angles = prevAngles
                            var xNoABSDelta = p.x - startX
                            var yNoABSDelta = p.y - startY

                            xNoABSDelta /= 5.0
                            yNoABSDelta /= 5.0

                            angles.x += xNoABSDelta
                            angles.y -= yNoABSDelta

                            screen.setCameraAngles(angles)

                            startX = p.x
                            startY = p.y

                            prevAngles = angles
                        }
                        else
                        {
                            var coefficients
                            var delta = 0.0

                            if (axis == Drag.XAxis)
                            {
                                delta = p.x - startX

                                if (Math.abs(delta) >= thresholdX)
                                {
                                    if (screen.getDistortionAdjustMode() === AnyVODEnums.DAM_NONE)
                                    {
                                        if (delta > 0)
                                            screen.forwardProper()
                                        else
                                            screen.rewindProper()
                                    }
                                    else
                                    {
                                        if (screen.getDistortionAdjustMode() === AnyVODEnums.DAM_BARREL)
                                        {
                                            coefficients = screen.getBarrelDistortionCoefficients()

                                            if (delta > 0)
                                                coefficients.x += 0.001
                                            else
                                                coefficients.x -= 0.001

                                            screen.setBarrelDistortionCoefficients(coefficients)
                                        }
                                        else if (screen.getDistortionAdjustMode() === AnyVODEnums.DAM_PINCUSION)
                                        {
                                            coefficients = screen.getPincushionDistortionCoefficients()

                                            if (delta > 0)
                                                coefficients.x += 0.001
                                            else
                                                coefficients.x -= 0.001

                                            screen.setPincushionDistortionCoefficients(coefficients)
                                        }
                                    }

                                    startX = p.x
                                }
                            }
                            else if (axis == Drag.YAxis)
                            {
                                if (isUseSPDIF())
                                    return

                                delta = startY - p.y

                                if (Math.abs(delta) >= thresholdY)
                                {
                                    if (screen.getDistortionAdjustMode() === AnyVODEnums.DAM_NONE)
                                    {
                                        var step = 10
                                        var value = volumeStart + (step * (delta / thresholdY))

                                        if (value < 0)
                                            value = 0
                                        else if (value > screen.getMaxVolume())
                                            value = screen.getMaxVolume()

                                        screen.volume(value)
                                        muteButton.checked = false
                                    }
                                    else
                                    {
                                        if (screen.getDistortionAdjustMode() === AnyVODEnums.DAM_BARREL)
                                        {
                                            coefficients = screen.getBarrelDistortionCoefficients()

                                            if (delta > 0)
                                                coefficients.y += 0.001
                                            else
                                                coefficients.y -= 0.001

                                            screen.setBarrelDistortionCoefficients(coefficients)
                                        }
                                        else if (screen.getDistortionAdjustMode() === AnyVODEnums.DAM_PINCUSION)
                                        {
                                            coefficients = screen.getPincushionDistortionCoefficients()

                                            if (delta > 0)
                                                coefficients.y += 0.001
                                            else
                                                coefficients.y -= 0.001

                                            screen.setPincushionDistortionCoefficients(coefficients)
                                        }
                                    }
                                }
                            }

                            if (controlPanel.visible)
                                showTimer.restart()
                        }
                    }
                }
            }

            onPressed: {
                var p = mouse

                startX = p.x
                startY = p.y

                dragged = false
                prevAngles = screen.getCameraAngles()
                is360DegreeAndNoHeadTracking = !settingDelegate.getSetting(AnyVODEnums.SK_HEAD_TRACKING) &&
                                               settingDelegate.getSetting(AnyVODEnums.SK_USE_360_DEGREE)
            }

            onReleased: {
                axis = Drag.None
                startX = 0
                startY = 0
                dragging = false
                is360DegreeAndNoHeadTracking = false
            }

            onClicked: {
                if (!dragged)
                {
                    if (touched)
                    {
                        doubleTouchTimer.stop()
                        touched = false

                        doubleTouched()
                    }
                    else
                    {
                        doubleTouchTimer.restart()
                        touched = true
                    }

                    if (showTimer.running)
                    {
                        showTimer.stop()
                        hideControlPanel()
                    }
                    else
                    {
                        showControlPanal()
                        hideTimer.stop()
                    }
                }
            }

            onDoubleTouched: {
                if (lockButton.checked)
                    return

                screen.toggle()
            }

            onPressAndHold: {
                if (lockButton.checked)
                    return

                screen.calibrateRotation()
                screen.resetFOV()
            }

            Timer {
                id: doubleTouchTimer
                interval: parent.doubleTouchThreshold

                onTriggered: {
                    parent.touched = false
                }
            }
        }

        onPinchStarted: {
            pinch.accepted = settingDelegate.getSetting(AnyVODEnums.SK_USE_360_DEGREE)
            orgDist = distance(pinch.startPoint1, pinch.startPoint2)
        }

        onPinchUpdated: {
            var newDist = distance(pinch.point1, pinch.point2)
            var dist = (orgDist - newDist) / 5.0

            if (floatPointUtilDelegate.zeroDouble(newDist) === 0.0)
                return

            screen.addFOV(dist)
            orgDist = newDist
        }
    }

    Component {
        id: notSupported

        MessageBox {
            windowWidth: Math.min(settingListDefaultWidth, 350)
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("정보")
            message: qsTr("지원하지 않는 기능입니다.")
        }
    }

    Component {
        id: videoStream

        SettingList {
            listHeight: Math.min(settingListDefaultHeight, 239)
            title: qsTr("영상")
            setting: settingDelegate
            type: AnyVODEnums.SK_VIDEO_ORDER
            model: ListModel {
                Component.onCompleted: {
                    var descs = screen.getVideoStreamDescs()
                    var indice = screen.getVideoStreamIndice()
                    var i = 0

                    descs.forEach(function(desc) {
                        append({ "desc": desc, "value": indice[i++] })
                    })
                }
            }
        }
    }

    Component {
        id: deinterlaceMethod

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 239)
            title: qsTr("디인터레이스 활성화 기준")
            setting: settingDelegate
            type: AnyVODEnums.SK_DEINTERLACE_METHOD_ORDER
            model: ListModel {
                ListElement { desc: qsTr("자동"); value: AnyVODEnums.DM_AUTO }
                ListElement { desc: qsTr("사용"); value: AnyVODEnums.DM_USE }
                ListElement { desc: qsTr("사용 안 함"); value: AnyVODEnums.DM_NOUSE }
            }
        }
    }

    Component {
        id: deinterlaceAlgorithm

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 423)
            title: qsTr("디인터레이스 알고리즘")
            setting: settingDelegate
            type: AnyVODEnums.SK_DEINTERLACE_ALGORITHM_ORDER
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < AnyVODEnums.DA_COUNT; i++)
                    {
                        var desc = screen.getDeinterlaceDesc(i)

                        append({ "desc": desc, "value": i })
                    }
                }
            }
        }
    }

    Component {
        id: userAspectRatio

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 469)
            title: qsTr("화면 비율")
            setting: settingDelegate
            type: AnyVODEnums.SK_USER_ASPECT_RATIO_ORDER
            model: ListModel {
                Component.onCompleted: {
                    var descs = screen.getUserAspectRatioDescs()
                    var i = 0

                    descs.forEach(function(desc) {
                        append({ "desc": desc, "value": i++ })
                    })
                }
            }
        }
    }

    Component {
        id: customUserAspectRatio

        UserAspectRatio {
            windowWidth: Math.min(settingListDefaultWidth, 365)
            windowHeight: Math.min(settingListDefaultHeight, 160)
            title: qsTr("화면 비율 사용자 지정")
            setting: settingDelegate
            aspectSize: settingDelegate.getSetting(AnyVODEnums.SK_USER_ASPECT_RATIO)
        }
    }

    Component {
        id: vrBarrelCoefficients

        SelectVector2D {
            windowWidth: Math.min(settingListDefaultWidth, 350)
            windowHeight: Math.min(settingListDefaultHeight, 160)
            title: qsTr("평면 VR 왜곡 보정 계수 설정")
            firstTitle: qsTr("k1")
            secondTitle: qsTr("k2")
            setting: settingDelegate
            initValue: settingDelegate.getSetting(AnyVODEnums.SK_VR_BARREL_COEFFICIENTS)
            key: AnyVODEnums.SK_VR_BARREL_COEFFICIENTS
            firstMinValue: -10.0
            firstMaxValue: 10.0
            secondMinValue: -10.0
            secondMaxValue: 10.0
        }
    }

    Component {
        id: vrPincushionCoefficients

        SelectVector2D {
            windowWidth: Math.min(settingListDefaultWidth, 350)
            windowHeight: Math.min(settingListDefaultHeight, 160)
            title: qsTr("360도 VR 왜곡 보정 계수 설정")
            firstTitle: qsTr("k1")
            secondTitle: qsTr("k2")
            setting: settingDelegate
            initValue: settingDelegate.getSetting(AnyVODEnums.SK_VR_PINCUSHION_COEFFICIENTS)
            key: AnyVODEnums.SK_VR_PINCUSHION_COEFFICIENTS
            firstMinValue: -10.0
            firstMaxValue: 10.0
            secondMinValue: -10.0
            secondMaxValue: 10.0
        }
    }

    Component {
        id: vrLensCenter

        SelectVector2D {
            windowWidth: Math.min(width * 0.9, 400)
            windowHeight: Math.min(settingListDefaultHeight, 160)
            title: qsTr("VR 렌즈 센터 설정")
            firstTitle: qsTr("좌/우")
            secondTitle: qsTr("상/하")
            setting: settingDelegate
            initValue: settingDelegate.getSetting(AnyVODEnums.SK_VR_LENS_CENTER)
            key: AnyVODEnums.SK_VR_LENS_CENTER
            firstMinValue: -1.0
            firstMaxValue: 1.0
            secondMinValue: -1.0
            secondMaxValue: 1.0
        }
    }

    Component {
        id: projectionType

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 285)
            title: qsTr("프로젝션 종류")
            setting: settingDelegate
            type: AnyVODEnums.SK_PROJECTION_TYPE_ORDER
            model: ListModel {
                ListElement { desc: "Equirectangular"; value: AnyVODEnums.V3T_EQR }
                ListElement { desc: "Cubemap (2x3)"; value: AnyVODEnums.V3T_CUBEMAP_2X3 }
                ListElement { desc: "Equi-Angular Cubemap (2x3)"; value: AnyVODEnums.V3T_EAC_2X3 }
                ListElement { desc: "Equi-Angular Cubemap (3x2)"; value: AnyVODEnums.V3T_EAC_3X2 }
            }
        }
    }

    Component {
        id: rotationDegree

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 285)
            title: qsTr("영상 회전 각도")
            setting: settingDelegate
            type: AnyVODEnums.SK_SCREEN_ROTATION_DEGREE_ORDER
            model: ListModel {
                ListElement { desc: qsTr("사용 안 함"); value: AnyVODEnums.SRD_NONE }
                ListElement { desc: qsTr("90도"); value: AnyVODEnums.SRD_90 }
                ListElement { desc: qsTr("180도"); value: AnyVODEnums.SRD_180 }
                ListElement { desc: qsTr("270도"); value: AnyVODEnums.SRD_270 }
            }
        }
    }

    Component {
        id: captureExt

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 285)
            title: qsTr("캡처 확장자")
            setting: settingDelegate
            type: AnyVODEnums.SK_SELECT_CAPTURE_EXT_ORDER
            model: ListModel {
                Component.onCompleted: {
                    var captureExts = captureExtDelegate.getCaptureExts()

                    captureExts.forEach(function(ext) {
                        append({ "desc": ext, "value": ext })
                    })
                }
            }
        }
    }

    Component {
        id: video3DMethod

        SettingList {
            title: qsTr("3D 영상 출력 방법")
            setting: settingDelegate
            type: AnyVODEnums.SK_3D_VIDEO_METHOD_ORDER
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < AnyVODEnums.V3M_COUNT; i++)
                    {
                        var desc = screen.get3DMethodDesc(i)
                        var cat = screen.get3DMethodCategory(i)

                        append({ "desc": desc, "value": i, "category": cat })
                    }
                }
            }
        }
    }

    Component {
        id: anaglyphAlgorithm

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 285)
            title: qsTr("3D 영상 애너글리프 알고리즘")
            setting: settingDelegate
            type: AnyVODEnums.SK_ANAGLYPH_ALGORITHM_ORDER
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < AnyVODEnums.AGA_COUNT; i++)
                    {
                        var desc = screen.getAnaglyphAlgoritmDesc(i)

                        append({ "desc": desc, "value": i })
                    }
                }
            }
        }
    }

    Component {
        id: vrInputSource

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 400)
            listHeight: Math.min(settingListDefaultHeight, 285)
            title: qsTr("VR 입력 영상 출력 방법")
            setting: settingDelegate
            type: AnyVODEnums.SK_VR_INPUT_SOURCE_ORDER
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < AnyVODEnums.VRI_COUNT; i++)
                    {
                        var desc = screen.getVRInputSourceDesc(i)

                        append({ "desc": desc, "value": i })
                    }
                }
            }
        }
    }

    Component {
        id: distortionAdjustMode

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 239)
            title: qsTr("왜곡 보정 모드")
            setting: settingDelegate
            type: AnyVODEnums.SK_DISTORION_ADJUST_MODE
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < AnyVODEnums.DAM_COUNT; i++)
                    {
                        var desc = screen.getDistortionAdjustModeDesc(i)

                        append({ "desc": desc, "value": i })
                    }
                }
            }
        }
    }

    Component {
        id: playOrder

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 331)
            title: qsTr("재생 순서 설정")
            setting: settingDelegate
            type: AnyVODEnums.SK_PLAY_ORDER
            model: ListModel {
                ListElement { desc: qsTr("전체 순차 재생"); value: AnyVODEnums.PM_TOTAL }
                ListElement { desc: qsTr("전체 반복 재생"); value: AnyVODEnums.PM_TOTAL_REPEAT }
                ListElement { desc: qsTr("한 개 재생"); value: AnyVODEnums.PM_SINGLE }
                ListElement { desc: qsTr("한 개 반복 재생"); value: AnyVODEnums.PM_SINGLE_REPEAT }
                ListElement { desc: qsTr("무작위 재생"); value: AnyVODEnums.PM_RANDOM }
            }
        }
    }

    Component {
        id: brightness

        SettingSlider {
            sliderWidth: Math.min(settingListDefaultWidth, 350)
            title: qsTr("밝기")
            interactMode: true
            setting: settingDelegate
            type: AnyVODEnums.SK_BRIGHTNESS
            defaultValue: 1
            roundValue: 2
            unitPostfix: qsTr("배")
            slider.from: 0
            slider.to: 3
            slider.stepSize: 0.01
        }
    }

    Component {
        id: saturation

        SettingSlider {
            sliderWidth: Math.min(settingListDefaultWidth, 350)
            title: qsTr("채도")
            interactMode: true
            setting: settingDelegate
            type: AnyVODEnums.SK_SATURATION
            defaultValue: 1
            roundValue: 2
            unitPostfix: qsTr("배")
            slider.from: 0
            slider.to: 3
            slider.stepSize: 0.01
        }
    }

    Component {
        id: hue

        SettingSlider {
            sliderWidth: Math.min(settingListDefaultWidth, 350)
            title: qsTr("색상")
            interactMode: true
            setting: settingDelegate
            type: AnyVODEnums.SK_HUE
            defaultValue: 0
            roundValue: 0
            unitPostfix: qsTr("°")
            slider.from: 0
            slider.to: 360
            slider.stepSize: 1
        }
    }

    Component {
        id: contrast

        SettingSlider {
            sliderWidth: Math.min(settingListDefaultWidth, 350)
            title: qsTr("대비")
            interactMode: true
            setting: settingDelegate
            type: AnyVODEnums.SK_CONTRAST
            defaultValue: 1
            roundValue: 2
            unitPostfix: qsTr("배")
            slider.from: 0
            slider.to: 3
            slider.stepSize: 0.01
        }
    }

    Component {
        id: playbackSpeed

        SettingSlider {
            sliderWidth: Math.min(settingListDefaultWidth, 350)
            title: qsTr("재생 속도 설정")
            interactMode: true
            playingMode: true
            setting: settingDelegate
            type: AnyVODEnums.SK_PLAYBACK_SPEED
            defaultValue: 0
            defaultStartValue: 1
            roundValue: 1
            scaleValue: 0.01
            unitPostfix: qsTr("배")
            slider.from: -90
            slider.to: 400
            slider.stepSize: 10
        }
    }

    Component {
        id: subtitleOpaque

        SettingSlider {
            sliderWidth: Math.min(settingListDefaultWidth, 350)
            title: qsTr("투명도")
            interactMode: true
            setting: settingDelegate
            type: AnyVODEnums.SK_SUBTITLE_OPAQUE
            defaultValue: 100
            roundValue: 0
            unitPostfix: qsTr("%")
            slider.from: 0
            slider.to: 100
            slider.stepSize: 1
        }
    }

    Component {
        id: subtitleSize

        SettingSlider {
            sliderWidth: Math.min(settingListDefaultWidth, 350)
            title: qsTr("크기")
            interactMode: true
            setting: settingDelegate
            type: AnyVODEnums.SK_SUBTITLE_SIZE
            defaultValue: 100
            roundValue: 0
            unitPostfix: qsTr("%")
            slider.from: 0
            slider.to: 300
            slider.stepSize: 1
        }
    }

    Component {
        id: subtitleSync

        SettingSlider {
            sliderWidth: Math.min(settingListLongWidth, 350)
            title: qsTr("싱크")
            interactMode: true
            playingMode: true
            setting: settingDelegate
            type: AnyVODEnums.SK_SUBTITLE_SYNC
            defaultValue: 0
            roundValue: 1
            unitPostfix: qsTr("초")
            slider.from: -300
            slider.to: 300
            slider.stepSize: 0.5
        }
    }

    Component {
        id: subtitleLeftRightPosition

        SettingSlider {
            sliderWidth: Math.min(settingListLongWidth, 350)
            title: qsTr("가로 위치")
            desc: qsTr("값이 클 수록 오른쪽으로 이동합니다.")
            interactMode: true
            setting: settingDelegate
            type: AnyVODEnums.SK_LEFTRIGHT_SUBTITLE_POSITION
            defaultValue: 0
            roundValue: 0
            slider.from: -100
            slider.to: 100
            slider.stepSize: 1
        }
    }

    Component {
        id: subtitleUpDownPosition

        SettingSlider {
            sliderWidth: Math.min(settingListLongWidth, 350)
            title: qsTr("세로 위치")
            desc: qsTr("값이 클 수록 윗쪽으로 이동합니다.")
            interactMode: true
            setting: settingDelegate
            type: AnyVODEnums.SK_UPDOWN_SUBTITLE_POSITION
            defaultValue: 0
            roundValue: 0
            slider.from: -100
            slider.to: 100
            slider.stepSize: 1
        }
    }

    Component {
        id: subtitleHAlign

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 331)
            title: qsTr("가로 정렬")
            setting: settingDelegate
            type: AnyVODEnums.SK_SUBTITLE_HALIGN_ORDER
            model: ListModel {
                ListElement { desc: qsTr("기본 정렬"); value: AnyVODEnums.HAM_NONE }
                ListElement { desc: qsTr("자동 정렬"); value: AnyVODEnums.HAM_AUTO }
                ListElement { desc: qsTr("왼쪽 정렬"); value: AnyVODEnums.HAM_LEFT }
                ListElement { desc: qsTr("가운데 정렬"); value: AnyVODEnums.HAM_MIDDLE }
                ListElement { desc: qsTr("오른쪽 정렬"); value: AnyVODEnums.HAM_RIGHT }
            }
        }
    }

    Component {
        id: subtitleVAlign

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 285)
            title: qsTr("세로 정렬")
            setting: settingDelegate
            type: AnyVODEnums.SK_SUBTITLE_VALIGN_ORDER
            model: ListModel {
                ListElement { desc: qsTr("기본 정렬"); value: AnyVODEnums.VAM_NONE }
                ListElement { desc: qsTr("상단 정렬"); value: AnyVODEnums.VAM_TOP }
                ListElement { desc: qsTr("가운데 정렬"); value: AnyVODEnums.VAM_MIDDLE }
                ListElement { desc: qsTr("하단 정렬"); value: AnyVODEnums.VAM_BOTTOM }
            }
        }
    }

    Component {
        id: subtitleEncoding

        SettingList {
            title: qsTr("텍스트 인코딩")
            setting: settingDelegate
            type: AnyVODEnums.SK_OPEN_TEXT_ENCODING
            model: ListModel {
                Component.onCompleted: {
                    var codecs = textCodecDelegate.names

                    codecs.forEach(function(codec) {
                        append({ "desc": codec, "value": codec })
                    })
                }
            }
        }
    }

    Component {
        id: subtitleLanguage

        SettingList {
            title: qsTr("언어")
            setting: settingDelegate
            type: AnyVODEnums.SK_SUBTITLE_LANGUAGE_ORDER
            model: ListModel {
                Component.onCompleted: {
                    var classes = screen.getSubtitleClasses()

                    classes.forEach(function(clazz) {
                        append({ "desc": clazz, "value": clazz })
                    })
                }
            }
        }
    }

    Component {
        id: subtitle3DUpDownPosition

        SettingSlider {
            sliderWidth: Math.min(settingListLongWidth, 350)
            title: qsTr("세로 거리")
            desc: qsTr("값이 클 수록 가까워집니다. VR 모드일 경우 반대 입니다.")
            interactMode: true
            setting: settingDelegate
            type: AnyVODEnums.SK_UPDOWN_3D_SUBTITLE_OFFSET
            defaultValue: 0
            roundValue: 0
            slider.from: -30
            slider.to: 30
            slider.stepSize: 1
        }
    }

    Component {
        id: subtitle3DLeftRightPosition

        SettingSlider {
            sliderWidth: Math.min(settingListLongWidth, 350)
            title: qsTr("가로 거리")
            desc: qsTr("값이 클 수록 가까워집니다. VR 모드일 경우 반대 입니다.")
            interactMode: true
            setting: settingDelegate
            type: AnyVODEnums.SK_LEFTRIGHT_3D_SUBTITLE_OFFSET
            defaultValue: 2
            roundValue: 0
            slider.from: -30
            slider.to: 30
            slider.stepSize: 1
        }
    }

    Component {
        id: virtual3DDepth

        SettingSlider {
            sliderWidth: Math.min(settingListLongWidth, 350)
            title: qsTr("가상 3D 입체감")
            desc: qsTr("값이 클 수록 입체감이 강해집니다.")
            interactMode: true
            setting: settingDelegate
            type: AnyVODEnums.SK_VIRTUAL_3D_DEPTH
            defaultValue: screen.getDefaultVirtual3DDepth()
            roundValue: 2
            slider.from: 0.0
            slider.to: 1.0
            slider.stepSize: 0.01
        }
    }

    Component {
        id: subtitle3DMethod

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 423)
            title: qsTr("3D 자막 출력 방법")
            setting: settingDelegate
            type: AnyVODEnums.SK_3D_SUBTITLE_METHOD_ORDER
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < AnyVODEnums.S3M_COUNT; i++)
                    {
                        var desc = screen.getSubtitle3DMethodDesc(i)

                        append({ "desc": desc, "value": i })
                    }
                }
            }
        }
    }

    Component {
        id: audioSync

        SettingSlider {
            sliderWidth: Math.min(settingListLongWidth, 350)
            title: qsTr("싱크")
            interactMode: true
            playingMode: true
            setting: settingDelegate
            type: AnyVODEnums.SK_AUDIO_SYNC
            defaultValue: 0
            roundValue: 1
            unitPostfix: qsTr("초")
            slider.from: -300
            slider.to: 300
            slider.stepSize: 0.5
        }
    }

    Component {
        id: bluetoothAudioSync

        SettingSlider {
            sliderWidth: Math.min(settingListLongWidth, 350)
            title: qsTr("블루투스 싱크")
            desc: qsTr("기기에 따라 값이 적용 되지 않을 수 있습니다.")
            interactMode: true
            playingMode: true
            setting: settingDelegate
            type: AnyVODEnums.SK_BLUETOOTH_AUDIO_SYNC
            defaultValue: 0
            roundValue: 1
            unitPostfix: qsTr("초")
            slider.from: -300
            slider.to: 300
            slider.stepSize: 0.5
        }
    }

    Component {
        id: openSubtitle

        FileSystemDialog {
            fileListObjName: "openSubtitle"
            title: qsTr("열기")
            onlySubtitle: true

            onAccepted: {
                if (!screen.openSubtitle(selectedPath))
                {
                    messageBoxLoader.focusParent = settingLoader
                    messageBoxLoader.sourceComponent = failedOpenSubtitleBox
                    messageBoxLoader.forceActiveFocus()
                }
            }
        }
    }

    Component {
        id: openSaveAsSubtitle

        FileSystemDialog {
            fileListObjName: "openSaveAsSubtitle"
            title: qsTr("다른 이름으로 저장")
            onlySubtitle: true
            saveMode: true
            startDirectory: screen.getDirectoryOfSubtitle()
            selectedFileName : screen.getSubtitleFileName()

            onAccepted: {
                messageBoxLoader.focusParent = settingLoader

                if (screen.saveSubtitleAs(selectedPath))
                    messageBoxLoader.sourceComponent = successSaveSubtitleBox
                else
                    messageBoxLoader.sourceComponent = failedSaveSubtitleBox

                messageBoxLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: openCaptureSaveDir

        FileSystemDialog {
            fileListObjName: "openCaptureSaveDir"
            title: qsTr("저장 디렉토리 설정")
            onlyDirectory: true
            startDirectory: settingDelegate.getSetting(AnyVODEnums.SK_SELECT_CAPTURE_DIRECTORY)

            onAccepted: {
                settingDelegate.setSetting(AnyVODEnums.SK_SELECT_CAPTURE_DIRECTORY, selectedPath)
            }
        }
    }

    Component {
        id: audioStream

        SettingList {
            listHeight: Math.min(settingListDefaultHeight, 239)
            title: qsTr("음성")
            setting: settingDelegate
            type: AnyVODEnums.SK_AUDIO_ORDER
            model: ListModel {
                Component.onCompleted: {
                    var descs = screen.getAudioStreamDescs()
                    var indice = screen.getAudioStreamIndice()
                    var i = 0

                    descs.forEach(function(desc) {
                        append({ "desc": desc, "value": indice[i++] })
                    })
                }
            }
        }
    }

    Component {
        id: audioDevice

        SettingList {
            title: qsTr("오디오 장치")
            setting: settingDelegate
            type: AnyVODEnums.SK_AUDIO_DEVICE_ORDER
            model: ListModel {
                Component.onCompleted: {
                    var devices = audioDeviceDelegate.getDevices()

                    append({ "desc": qsTr("기본 장치"), "value": -1 })

                    var i = 0
                    devices.forEach(function(device) {
                        append({ "desc": device, "value": i++ })
                    })
                }
            }
        }
    }

    Component {
        id: spdifAudioDevice

        SettingList {
            title: qsTr("출력 장치")
            setting: settingDelegate
            type: AnyVODEnums.SK_SPDIF_AUDIO_DEVICE_ORDER
            model: ListModel {
                Component.onCompleted: {
                    var devices = spdifDelegate.getDevices()

                    append({ "desc": qsTr("기본 장치"), "value": -1 })

                    var i = 0
                    devices.forEach(function(device) {
                        append({ "desc": device.replace(/\n/gi, " "), "value": i++ })
                    })
                }
            }
        }
    }

    Component {
        id: spdifEncoding

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 239)
            title: qsTr("인코딩")
            setting: settingDelegate
            type: AnyVODEnums.SK_USE_SPDIF_ENCODING_ORDER
            model: ListModel {
                ListElement { desc: qsTr("사용 안 함"); value: AnyVODEnums.SEM_NONE }
                ListElement { desc: qsTr("AC3"); value: AnyVODEnums.SEM_AC3 }
                ListElement { desc: qsTr("DTS"); value: AnyVODEnums.SEM_DTS }
            }
        }
    }

    Component {
        id: spdifSampleRate

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 285)
            title: qsTr("샘플링 속도")
            setting: settingDelegate
            type: AnyVODEnums.SK_SPDIF_SAMPLE_RATE_ORDER
            model: ListModel {
                Component.onCompleted: {
                    var samplerates = spdifDelegate.getSampleRates()
                    var descs = spdifDelegate.getSampleRateDescs()
                    var i = 0

                    samplerates.forEach(function(samplerate) {
                        append({ "desc": descs[i++], "value": samplerate })
                    })
                }
            }
        }
    }

    Component {
        id: screenSettings

        SettingDialog {
            title: qsTr("화면")
            model: ScreenSettingItems {}
            setting: settingDelegate

            onNotSupportedFunction: {
                messageBoxLoader.sourceComponent = notSupported
                messageBoxLoader.forceActiveFocus()
            }

            onItemSelected: {
                var item = model.get(index)

                switch (item.type)
                {
                    case AnyVODEnums.SK_VIDEO_ORDER:
                        settingDlgLoader.sourceComponent = videoStream
                        break
                    case AnyVODEnums.SK_DEINTERLACE_METHOD_ORDER:
                        settingDlgLoader.sourceComponent = deinterlaceMethod
                        break
                    case AnyVODEnums.SK_DEINTERLACE_ALGORITHM_ORDER:
                        settingDlgLoader.sourceComponent = deinterlaceAlgorithm
                        break
                    case AnyVODEnums.SK_USER_ASPECT_RATIO_ORDER:
                        settingDlgLoader.sourceComponent = userAspectRatio
                        break
                    case AnyVODEnums.SK_USER_ASPECT_RATIO:
                        settingDlgLoader.sourceComponent = customUserAspectRatio
                        break
                    case AnyVODEnums.SK_SCREEN_ROTATION_DEGREE_ORDER:
                        settingDlgLoader.sourceComponent = rotationDegree
                        break
                    case AnyVODEnums.SK_BRIGHTNESS:
                        settingDlgLoader.sourceComponent = brightness
                        break
                    case AnyVODEnums.SK_SATURATION:
                        settingDlgLoader.sourceComponent = saturation
                        break
                    case AnyVODEnums.SK_HUE:
                        settingDlgLoader.sourceComponent = hue
                        break
                    case AnyVODEnums.SK_CONTRAST:
                        settingDlgLoader.sourceComponent = contrast
                        break
                    case AnyVODEnums.SK_SELECT_CAPTURE_EXT_ORDER:
                        settingDlgLoader.sourceComponent = captureExt
                        break
                    case AnyVODEnums.SK_SELECT_CAPTURE_DIRECTORY:
                        settingDlgLoader.sourceComponent = openCaptureSaveDir
                        break
                    case AnyVODEnums.SK_3D_VIDEO_METHOD_ORDER:
                        settingDlgLoader.sourceComponent = video3DMethod
                        break
                    case AnyVODEnums.SK_ANAGLYPH_ALGORITHM_ORDER:
                        settingDlgLoader.sourceComponent = anaglyphAlgorithm
                        break
                    case AnyVODEnums.SK_VR_INPUT_SOURCE_ORDER:
                        settingDlgLoader.sourceComponent = vrInputSource
                        break
                    case AnyVODEnums.SK_VR_BARREL_COEFFICIENTS:
                        settingDlgLoader.sourceComponent = vrBarrelCoefficients
                        break
                    case AnyVODEnums.SK_VR_PINCUSHION_COEFFICIENTS:
                        settingDlgLoader.sourceComponent = vrPincushionCoefficients
                        break
                    case AnyVODEnums.SK_VR_LENS_CENTER:
                        settingDlgLoader.sourceComponent = vrLensCenter
                        break
                    case AnyVODEnums.SK_DISTORION_ADJUST_MODE:
                        settingDlgLoader.sourceComponent = distortionAdjustMode
                        break
                    case AnyVODEnums.SK_VIRTUAL_3D_DEPTH:
                        settingDlgLoader.sourceComponent = virtual3DDepth
                        break
                    case AnyVODEnums.SK_PROJECTION_TYPE_ORDER:
                        settingDlgLoader.sourceComponent = projectionType
                        break
                }

                if (settingDlgLoader.sourceComponent != undefined)
                    settingDlgLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: playSettings

        SettingDialog {
            title: qsTr("재생")
            model: PlaySettingItems {}
            setting: settingDelegate

            onItemSelected: {
                var item = model.get(index)

                switch (item.type)
                {
                    case AnyVODEnums.SK_REPEAT_RANGE:
                        goBack()
                        screen.showRepeatRange()
                        break
                    case AnyVODEnums.SK_PLAYBACK_SPEED:
                        settingDlgLoader.sourceComponent = playbackSpeed
                        break
                    case AnyVODEnums.SK_PLAY_ORDER:
                        settingDlgLoader.sourceComponent = playOrder
                        break
                }

                if (settingDlgLoader.sourceComponent != undefined)
                    settingDlgLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: subtitleLyricsSettings

        SettingDialog {
            title: qsTr("자막 / 가사")
            model: SubtitleLyricsSettingItems {}
            setting: settingDelegate

            onItemSelected: {
                var item = model.get(index)

                switch (item.type)
                {
                    case AnyVODEnums.SK_OPEN_SUBTITLE:
                        settingDlgLoader.sourceComponent = openSubtitle
                        break
                    case AnyVODEnums.SK_CLOSE_EXTERNAL_SUBTITLE:
                        screen.closeExternalSubtitle()
                        messageBoxLoader.focusParent = settingLoader
                        messageBoxLoader.sourceComponent = closeExternalSubtitleBox
                        break
                    case AnyVODEnums.SK_SUBTITLE_OPAQUE:
                        settingDlgLoader.sourceComponent = subtitleOpaque
                        break
                    case AnyVODEnums.SK_SUBTITLE_SIZE:
                        settingDlgLoader.sourceComponent = subtitleSize
                        break
                    case AnyVODEnums.SK_SUBTITLE_SYNC:
                        settingDlgLoader.sourceComponent = subtitleSync
                        break
                    case AnyVODEnums.SK_LEFTRIGHT_SUBTITLE_POSITION:
                        settingDlgLoader.sourceComponent = subtitleLeftRightPosition
                        break
                    case AnyVODEnums.SK_UPDOWN_SUBTITLE_POSITION:
                        settingDlgLoader.sourceComponent = subtitleUpDownPosition
                        break
                    case AnyVODEnums.SK_SUBTITLE_HALIGN_ORDER:
                        settingDlgLoader.sourceComponent = subtitleHAlign
                        break
                    case AnyVODEnums.SK_SUBTITLE_VALIGN_ORDER:
                        settingDlgLoader.sourceComponent = subtitleVAlign
                        break
                    case AnyVODEnums.SK_SUBTITLE_LANGUAGE_ORDER:
                        settingDlgLoader.sourceComponent = subtitleLanguage
                        break
                    case AnyVODEnums.SK_OPEN_TEXT_ENCODING:
                        settingDlgLoader.sourceComponent = subtitleEncoding
                        break
                    case AnyVODEnums.SK_SAVE_SUBTITLE:
                        messageBoxLoader.focusParent = settingLoader

                        if (screen.saveSubtitle())
                            messageBoxLoader.sourceComponent = successSaveSubtitleBox
                        else
                            messageBoxLoader.sourceComponent = failedSaveSubtitleBox

                        break
                    case AnyVODEnums.SK_SAVE_AS_SUBTITLE:
                        settingDlgLoader.sourceComponent = openSaveAsSubtitle
                        break
                    case AnyVODEnums.SK_UPDOWN_3D_SUBTITLE_OFFSET:
                        settingDlgLoader.sourceComponent = subtitle3DUpDownPosition
                        break
                    case AnyVODEnums.SK_LEFTRIGHT_3D_SUBTITLE_OFFSET:
                        settingDlgLoader.sourceComponent = subtitle3DLeftRightPosition
                        break
                    case AnyVODEnums.SK_3D_SUBTITLE_METHOD_ORDER:
                        settingDlgLoader.sourceComponent = subtitle3DMethod
                        break
                }

                if (settingDlgLoader.sourceComponent != undefined)
                    settingDlgLoader.forceActiveFocus()

                if (messageBoxLoader.sourceComponent != undefined)
                    messageBoxLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: soundSettings

        SettingDialog {
            title: qsTr("소리")
            model: SoundSettingItems {}
            setting: settingDelegate

            onNotSupportedFunction: {
                messageBoxLoader.focusParent = this
                messageBoxLoader.sourceComponent = notSupported
                messageBoxLoader.forceActiveFocus()
            }

            onItemSelected: {
                var item = model.get(index)

                switch (item.type)
                {
                    case AnyVODEnums.SK_AUDIO_ORDER:
                        settingDlgLoader.sourceComponent = audioStream
                        break
                    case AnyVODEnums.SK_AUDIO_SYNC:
                        settingDlgLoader.sourceComponent = audioSync
                        break
                    case AnyVODEnums.SK_AUDIO_DEVICE_ORDER:
                        settingDlgLoader.sourceComponent = audioDevice
                        break
                    case AnyVODEnums.SK_SPDIF_AUDIO_DEVICE_ORDER:
                        settingDlgLoader.sourceComponent = spdifAudioDevice
                        break
                    case AnyVODEnums.SK_USE_SPDIF_ENCODING_ORDER:
                        settingDlgLoader.sourceComponent = spdifEncoding
                        break
                    case AnyVODEnums.SK_SPDIF_SAMPLE_RATE_ORDER:
                        settingDlgLoader.sourceComponent = spdifSampleRate
                        break
                    case AnyVODEnums.SK_BLUETOOTH_AUDIO_SYNC:
                        settingDlgLoader.sourceComponent = bluetoothAudioSync
                        break
                }

                if (settingDlgLoader.sourceComponent != undefined)
                    settingDlgLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: info

        SettingTextViewer {
            title: qsTr("AnyVOD 정보")
            desc.text: versionDelegate.get()
            desc.wrapMode: TextEdit.Wrap
            flickDir: Flickable.VerticalFlick
        }
    }

    Component {
        id: infoSettings

        SettingDialog {
            title: qsTr("정보")
            model: InfoSettingItems {}
            setting: settingDelegate

            onCheckItemSelected: {
                var item = model.get(index)

                switch (item.type)
                {
                    case AnyVODEnums.SK_DETAIL:
                        if (screen.isEnabledVideo())
                            break

                        if (checked && noVideo.visible)
                            noVideo.visible = false
                        else if (!checked && !noVideo.visible)
                            noVideo.visible = true

                        break
                }
            }

            onItemSelected: {
                var item = model.get(index)

                switch (item.type)
                {
                    case AnyVODEnums.SK_INFO:
                        settingDlgLoader.sourceComponent = info
                        break
                }

                if (settingDlgLoader.sourceComponent != undefined)
                    settingDlgLoader.forceActiveFocus()
            }
        }
    }

    Menus {
        id: screenMenu
        timer: showTimer
        menuHeight: Math.min(screen.height - 20, 240)
        model: VideoScreenMenuItems {}
        boundsBehavior: Flickable.OvershootBounds

        onMenuItemSelected: {
            var item = model.get(index)

            switch (item.menuID)
            {
                case AnyVODEnums.MID_EQUALIZER:
                    settingLoader.sourceComponent = equalizer
                    break
                case AnyVODEnums.MID_SCREEN:
                    settingLoader.sourceComponent = screenSettings
                    break
                case AnyVODEnums.MID_PLAY:
                    settingLoader.sourceComponent = playSettings
                    break
                case AnyVODEnums.MID_SUBTITLE_LYRICS:
                    settingLoader.sourceComponent = subtitleLyricsSettings
                    break
                case AnyVODEnums.MID_SOUND:
                    settingLoader.sourceComponent = soundSettings
                    break
                case AnyVODEnums.MID_INFO:
                    settingLoader.sourceComponent = infoSettings
                    break
            }

            if (settingLoader.sourceComponent != undefined)
                settingLoader.forceActiveFocus()
        }
    }

    Component {
        id: closeExternalSubtitleBox

        MessageBox {
            windowWidth: Math.min(settingListDefaultWidth, 300)
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("정보")
            message: qsTr("외부 자막을 닫았습니다.")
        }
    }

    Component {
        id: successSaveSubtitleBox

        MessageBox {
            windowWidth: Math.min(settingListDefaultWidth, 350)
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("정보")
            message: qsTr("자막 / 가사가 저장 되었습니다.")
        }
    }

    Component {
        id: failedSaveSubtitleBox

        MessageBox {
            windowWidth: Math.min(settingListDefaultWidth, 350)
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("오류")
            message: qsTr("자막 / 가사가 저장 되지 않았습니다.")
        }
    }

    Component {
        id: failedOpenSubtitleBox

        MessageBox {
            windowWidth: Math.min(settingListDefaultWidth, 300)
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("오류")
            message: qsTr("자막 / 가사를 열지 못했습니다.")
        }
    }

    Rectangle {
        id: lyrics
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height * 0.07
        height: 80
        visible: false
        radius: 5
        color: "#80000000"

        ColumnLayout {
            anchors.centerIn: parent
            width: parent.width
            spacing: 5

            Text {
                id: lyrics1
                objectName: "lyrics1"
                elide: Text.ElideMiddle
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.bold: true
                style: Text.Outline
                styleColor: "black"
                Layout.fillWidth: true
                Layout.minimumHeight: 20
                Layout.maximumHeight: Layout.minimumHeight
            }

            Text {
                id: lyrics2
                objectName: "lyrics2"
                elide: lyrics1.elide
                verticalAlignment: lyrics1.verticalAlignment
                horizontalAlignment: lyrics1.horizontalAlignment
                style: lyrics1.style
                styleColor: lyrics1.styleColor
                Layout.fillWidth: true
                Layout.minimumHeight: 20
                Layout.maximumHeight: Layout.minimumHeight
            }

            Text {
                id: lyrics3
                objectName: "lyrics3"
                elide: lyrics1.elide
                verticalAlignment: lyrics1.verticalAlignment
                horizontalAlignment: lyrics1.horizontalAlignment
                style: lyrics1.style
                styleColor: lyrics1.styleColor
                Layout.fillWidth: true
                Layout.minimumHeight: 20
                Layout.maximumHeight: Layout.minimumHeight
            }
        }
    }

    Rectangle {
        id: controlPanel
        anchors.fill: parent
        visible: false
        color: "transparent"
        opacity: 0.0
        layer.enabled: true

        onVisibleChanged: {
            if (repeatRange.visible)
                screen.showRepeatRange()
        }

        Behavior on opacity {
            NumberAnimation {
                id: controlPanelOpacityAni
                duration: 500
                easing.type: Easing.OutQuad
            }
        }

        Rectangle {
            id: topBar
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            height: 40
            color: "#7f000000"

            RowLayout {
                anchors.fill: parent

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 10
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }

                ImageButton {
                    id: backButton
                    resetTimer: showTimer
                    image.width: 20
                    image.height: image.width
                    source: "assets/back.png"
                    Layout.minimumWidth: 30
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignHCenter

                    onClicked: {
                        closeAndGoBack()
                    }
                }

                MarqueeText {
                    id: title
                    text.verticalAlignment: Text.AlignVCenter
                    text.color: "white"
                    text.font.pixelSize: parent.height * 0.4
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                }

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 10
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }

                ImageCheckButton {
                    id: muteButton
                    objectName: "muteButton"
                    resetTimer: showTimer
                    checkedSource: "assets/speaker_mute.png"
                    unCheckedSource: "assets/speaker.png"
                    Layout.minimumWidth: 20
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.minimumHeight: 20
                    Layout.maximumHeight: Layout.minimumHeight
                    Layout.alignment: Qt.AlignHCenter

                    onToggled: {
                        screen.mute(checked)
                    }
                }

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 20
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }

                ImageButton {
                    id: menuButton
                    image.width: 20
                    image.height: image.width
                    image.x: 10
                    source: "assets/menu.png"
                    Layout.minimumWidth: 35
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignHCenter

                    onClicked: {
                        screen.showMenu()
                    }
                }
            }
        }

        ImageButton {
            id: rotateButton
            resetTimer: showTimer
            x: topBar.width - width - 15
            y: topBar.y + topBar.height + 15
            width: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? 0 : 30
            height: width
            image.width: width
            image.height: height
            source: "assets/rotate.png"
            visible: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? false : true

            onClicked: {
                screen.setScreenOrientation(!screen.getScreenOrientation())
            }
        }

        ImageButton {
            id: playListButton
            resetTimer: showTimer
            x: topBar.width - width - 15
            y: rotateButton.y + rotateButton.height + 15
            width: 30
            height: width
            image.width: width
            image.height: height
            source: "assets/play_list.png"

            onClicked: {
                showPlayList()
            }
        }

        ImageButton {
            id: captureButton
            resetTimer: showTimer
            x: topBar.width - width - 15
            y: playListButton.y + playListButton.height + 15
            width: 30
            height: width
            image.width: width
            image.height: height
            source: "assets/capture.png"

            onClicked: {
                if (canCapture())
                {
                    captureSingle()
                }
                else
                {
                    messageBoxLoader.sourceComponent = notSupported
                    messageBoxLoader.forceActiveFocus()
                }
            }
        }

        ImageButton {
            id: epgButton
            resetTimer: showTimer
            x: topBar.width - width - 15
            y: captureButton.y + captureButton.height + 15
            width: visible ? 30 : 0
            height: width
            image.width: width
            image.height: height
            source: "assets/epg.png"

            onClicked: {
                epgLoader.sourceComponent = epg
                epgLoader.forceActiveFocus()
            }
        }

        Rectangle {
            id: seekBarPanel
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: controlBar.top
            height: 40
            color: "#7f000000"

            RowLayout {
                anchors.fill: parent
                spacing: 0

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 10
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }

                Text {
                    id: curTime
                    objectName: "curTime"
                    color : "white"
                    text: "00:00:00"
                }

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 10
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }

                AnyVOD.Slider {
                    id: seekBar
                    objectName: "seekBar"
                    live: false
                    handleBorderWidth: 2
                    Layout.fillWidth: true
                    Layout.minimumHeight: 20
                    Layout.maximumHeight: Layout.minimumHeight

                    property Timer resetTimer: showTimer
                    property double initPos: 0.0

                    function seekTo(value) {
                        screen.seek(value, true)
                    }

                    onHandleXChanged: {
                        if (pressed)
                            screen.showCurrentPos(visualPosition * to, initPos)
                    }

                    onPressedChanged: {
                        if (pressed)
                        {
                            if (resetTimer != null)
                                resetTimer.stop()

                            initPos = value
                        }
                        else
                        {
                            if (resetTimer != null)
                                resetTimer.restart()

                            initPos = 0.0
                            seekTo(value)
                        }
                    }
                }

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 10
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }

                Text {
                    id: totalTime
                    objectName: "totalTime"
                    color : "white"
                    text: "00:00:00"
                }

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 10
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }
            }
        }

        Rectangle {
            id: controlBar
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            height: 60
            color: "#7f000000"

            Grid {
                anchors.centerIn: parent
                horizontalItemAlignment: Grid.AlignHCenter
                verticalItemAlignment: Grid.AlignTop
                columns: 5
                spacing: 30

                ImageButton {
                    id: prevButton
                    objectName: "prevButton"
                    resetTimer: showTimer
                    width: 40
                    height: width
                    image.width: width
                    image.height: height
                    source: "assets/prev.png"

                    onClicked: {
                        screen.prev(true)
                        screen.setOptionDescYValue()
                    }
                }

                ImageButton {
                    id: backwardButton
                    resetTimer: showTimer
                    width: 40
                    height: width
                    image.width: width
                    image.height: height
                    source: "assets/backward.png"

                    onClicked: {
                        screen.rewindProper()
                    }
                }

                ImageButton {
                    id: resumeButton
                    resetTimer: showTimer
                    width: 40
                    height: width
                    image.width: width
                    image.height: height
                    source: "assets/resume.png"
                    visible: false

                    onClicked: {
                        screen.resume()
                    }
                }

                ImageButton {
                    id: pauseButton
                    resetTimer: showTimer
                    width: 40
                    height: width
                    image.width: width
                    image.height: height
                    source: "assets/pause.png"

                    onClicked: {
                        screen.pause()
                    }
                }

                ImageButton {
                    id: forwardButton
                    resetTimer: showTimer
                    width: 40
                    height: width
                    image.width: width
                    image.height: height
                    source: "assets/forward.png"

                    onClicked: {
                        screen.forwardProper()
                    }
                }

                ImageButton {
                    id: nextButton
                    objectName: "nextButton"
                    resetTimer: showTimer
                    width: 40
                    height: width
                    image.width: width
                    image.height: height
                    source: "assets/next.png"

                    onClicked: {
                        screen.next(true)
                        screen.setOptionDescYValue()
                    }
                }
            }
        }
    }

    ImageCheckButton {
        id: lockButton
        resetTimer: showTimer
        visible: false
        opacity: 0.0
        x: 15
        y: topBar.y + topBar.height + 50
        width: 30
        height: width
        image.width: width
        image.height: height
        checkedSource: "assets/lock.png"
        unCheckedSource: "assets/unlock.png"

        Behavior on opacity {
            NumberAnimation {
                duration: controlPanelOpacityAni.duration
                easing.type: Easing.OutQuad
            }
        }

        onToggled: {
            pinchArea.enabled = !checked

            if (checked)
            {
                showOptionDesc(qsTr("화면 잠김"))
                hideControlPanel()
            }
            else
            {
                showOptionDesc(qsTr("화면 잠김 해제"))
                showControlPanal()
            }
        }
    }

    Rectangle {
        id: noVideo
        anchors.fill: parent
        visible: false
        color: "transparent"

        Image {
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            width: 100
            source: "assets/big_icon.png"
            fillMode: Image.PreserveAspectFit
        }
    }

    Loader {
        id: settingDlgLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        z: 10
        layer.enabled: true

        property bool isSilder: false
        property bool isPlaying: false

        function exit()
        {
            settingLoader.forceActiveFocus()

            if (isSilder)
            {
                settingLoader.opacity = 1.0
                isSilder = false
            }

            if (isPlaying)
            {
                screen.setUseCustomControl(false)
                screen.pause()

                isPlaying = false
            }

            opacity = 0.0
            settingDlgHideTimer.restart()
        }

        Timer {
            id: settingDlgHideTimer
            interval: settingDlgOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: settingDlgOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: settingDlgLoader.item

            function onGoBack() {
                settingDlgLoader.exit()
            }

            function onInteractModeSelected() {
                settingDlgLoader.isSilder = true
                settingLoader.opacity = 0.0

                if (controlPanel.visible)
                    screen.hideControlPanel()
            }

            function onPlayingModeSelected() {
                settingDlgLoader.isPlaying = true

                screen.setUseCustomControl(true)
                screen.resume()
            }
        }
    }

    Loader {
        id: settingLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        layer.enabled: true

        property bool useCustomControl: false
        property int playStatus: -1

        function exit()
        {
            screen.forceActiveFocus()

            opacity = 0.0
            settingHideTimer.restart()
        }

        Timer {
            id: settingHideTimer
            interval: settingOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined

                screen.setUseCustomControl(parent.useCustomControl)

                if (parent.playStatus !== RenderScreen.Paused &&
                        !screen.isAudio() &&
                        screen.getDistortionAdjustMode() === AnyVODEnums.DAM_NONE)
                {
                    screen.resume()
                }
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: settingOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0

            playStatus = screen.getStatus()

            if (playStatus !== RenderScreen.Paused && !screen.isAudio())
                screen.pause()

            useCustomControl = screen.getUseCustomControl()
            screen.setUseCustomControl(false)
        }

        Connections {
            ignoreUnknownSignals: true
            target: settingLoader.item

            function onGoBack() {
                settingLoader.exit()
            }
        }
    }

    Loader {
        id: messageBoxLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        layer.enabled: true

        property Item focusParent: null

        function exit()
        {
            if (focusParent != null)
            {
                focusParent.forceActiveFocus()
                focusParent = null
            }

            opacity = 0.0
            messageBoxHideTimer.restart()
        }

        Timer {
            id: messageBoxHideTimer
            interval: messageBoxOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: messageBoxOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: messageBoxLoader.item

            function onGoBack() {
                messageBoxLoader.exit()
            }
        }
    }

    Loader {
        id: popupLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        layer.enabled: true

        property string desc
        property string link

        function exit()
        {
            opacity = 0.0
            popupHideTimer.restart()
        }

        Timer {
            id: popupHideTimer
            interval: popupOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Timer {
            id: popupShowTimer
            interval: 4000

            onTriggered: {
                popupLoader.exit()
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: popupOpacityAni
                duration: 500
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
            popupShowTimer.restart()
        }

        Connections {
            ignoreUnknownSignals: true
            target: popupLoader.item

            function onGoBack() {
                popupLoader.exit()
            }
        }
    }

    Loader {
        id: epgLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        layer.enabled: true

        function exit()
        {
            screen.forceActiveFocus()

            opacity = 0.0
            epgHideTimer.restart()
        }

        Timer {
            id: epgHideTimer
            interval: epgOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: epgOpacityAni
                duration: 500
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: epgLoader.item

            function onGoBack() {
                epgLoader.exit()
            }
        }
    }

    Loader {
        id: dlgLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        layer.enabled: true

        function exit()
        {
            screen.forceActiveFocus()

            opacity = 0.0
            dlgHideTimer.restart()
        }

        Timer {
            id: dlgHideTimer
            interval: dlgOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: dlgOpacityAni
                duration: 500
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: dlgLoader.item

            function onGoBack() {
                dlgLoader.exit()
            }
        }
    }

    Timer {
        id: fullScreenTimer
        interval: 100

        onTriggered: {
            screen.setFullScreen()
        }
    }

    Timer {
        id: headsetHookTimer
        interval: 500

        onTriggered: {
            switch (hookKeyCount)
            {
                case 1:
                    if (hookKeyPressed)
                    {
                        screen.hookKeyPressedProcessing = true
                        screen.forwardProper()

                        restart()
                    }
                    else
                    {
                        screen.toggle()
                        screen.hookKeyCount = 0
                    }

                    break
                case 2:
                    if (hookKeyPressed)
                    {
                        screen.hookKeyPressedProcessing = true
                        screen.rewindProper()

                        restart()
                    }
                    else
                    {
                        screen.next(true)
                        screen.hookKeyCount = 0
                    }

                    break
                case 3:
                    screen.prev(true)
                    screen.hookKeyCount = 0

                    break
            }
        }
    }

    function showMenu()
    {
        var showX = menu.x - screenMenu.menuWidth + menu.width - 10
        var showY = menu.y + 10

        screenMenu.show(showX, showY)
    }

    function showRepeatRange()
    {
        var showX
        var showY

        showX = seekBarPanel.width - repeatRange.windowWidth

        if (seekBarPanel.visible)
            showY = seekBarPanel.y - repeatRange.windowHeight
        else
            showY = screen.height - repeatRange.windowHeight

        if (repeatRange.visible)
            repeatRange.move(showX, showY)
        else
            repeatRange.show(showX, showY)
    }

    function setOptionDescYValue()
    {
        if (getVRInputSource() === AnyVODEnums.VRI_NONE)
            setOptionDescY(topBar.height + 3)
        else
            setOptionDescY(0)
    }

    function adjustSPDIFAudioUI()
    {
        var toggle = isUseSPDIF()

        muteButton.enabled = !toggle
    }

    onShowSubtitlePopup: {
        popupLoader.desc = qsTr("외부 서버에 자막이 존재합니다.\n이동하시려면 여기를 클릭하세요.")
        popupLoader.link = screen.getGOMSubtitleURL()
        popupLoader.sourceComponent = subtitlePopup
    }

    onViewLyrics: {
        resetLyrics()
        lyrics.visible = screen.existAudioSubtitle()
    }

    onFailedPlay: {
        closeAndGoBack()
    }

    onPlaying: {
        var pos = screen.getCurrentPosition()

        curTime.text = screen.getTimeString(pos)

        if (!seekBar.pressed)
            seekBar.value = pos
    }

    onStarted: {
        var dur = screen.getDuration()
        var playItemCount = playListModel.getCount()
        var currItemIndex = playListModel.getCurrentIndex()

        curTime.text = screen.getTimeString(0.0)
        totalTime.text = screen.getTimeString(dur)

        seekBar.from = 0.0
        seekBar.to = dur
        seekBar.value = 0.0

        if (playItemCount > 0)
            title.text.text = "[" + (currItemIndex + 1) + " / " + playItemCount + "] " + screen.getTitle()
        else
            title.text.text = screen.getTitle()

        title.restart()

        if (playItemCount <= 1)
        {
            prevButton.enabled = false
            nextButton.enabled = false
        }
        else if (currItemIndex - 1 < 0)
        {
            prevButton.enabled = false
            nextButton.enabled = true
        }
        else if (currItemIndex + 1 >= playItemCount)
        {
            prevButton.enabled = true
            nextButton.enabled = false
        }
        else
        {
            prevButton.enabled = true
            nextButton.enabled = true
        }

        if (screen.hasDuration())
        {
            backwardButton.enabled = true
            forwardButton.enabled = true
        }
        else
        {
            backwardButton.enabled = false
            forwardButton.enabled = false
        }

        noVideo.visible = !screen.isEnabledVideo() && !screen.isShowDetail()
        epgButton.visible = screen.isDTVOpened()
        pinchArea.y = screen.isAudio() || Qt.platform.os === "linux" ? 0 : 20

        viewLyrics()

        fullScreenTimer.restart()
        statusBarDelegate.show(false)

        vrInputSourceChanged()
        adjustSPDIFAudioUI()
    }

    onResumed: {
        resumeButton.visible = false
        pauseButton.visible = true
    }

    onPaused: {
        resumeButton.visible = true
        pauseButton.visible = false
    }

    onStopped: {
        title.text.text = ""

        resetLyrics()
    }

    onEnded: {
        resetLyrics()
    }

    onExit: {
        exitAtCppSide()
        statusBarDelegate.show(true)

        goBack(screen.getRetainedFilePath())
    }

    onRecovered: {
        noVideo.visible = !screen.isEnabledVideo()
    }

    onSpdifEnabled: {
        adjustSPDIFAudioUI()
    }

    onVrInputSourceChanged: {
        screen.determineToStartRotation()
    }

    onNeedLogin: {
        dlgLoader.sourceComponent = login
        dlgLoader.forceActiveFocus()
    }

    Component.onCompleted: {
        loadSettings()

        if (playListName.length > 0)
        {
            wantToPlay = true

            playListModel.setPlayList(playListName)
            playList.useReload = playListModel.getBaseURL().length > 0
        }
        else if (playListItems.length > 0)
        {
            wantToPlay = true

            playListModel.setPlayListItems(playListItems, playListItemIndex)
        }
        else if (remotePlayListItems.length > 0)
        {
            wantToPlay = true

            playListModel.setRemotePlayListItems(remotePlayListItems, remotePlayListItemIndex)
        }
        else if (disableLastPos && filePath.length > 0)
        {
            gotoTimeTmp = gotoTime
            disableLastPosTmp = disableLastPos

            wantToPlay = open(filePath)
        }
        else if (filePath.length > 0)
        {
            wantToPlay = open(filePath)
        }
    }

    Keys.onPressed: {
        if (lockButton.checked)
            return

        if (event.key === 0)
            hookKeyPressed = true
    }

    Keys.onReleased: {
        if (lockButton.checked)
        {
            event.accepted = true
            return
        }

        var volumeValue

        switch (event.key)
        {
            case Qt.Key_Back:
                event.accepted = true
                closeAndGoBack()

                break
            case Qt.Key_MediaPlay:
                event.accepted = true

                if (Qt.platform.os === "android")
                    toggle()
                else
                    resume()

                break
            case Qt.Key_MediaPause:
                event.accepted = true
                pause()

                break
            case Qt.Key_MediaStop:
                event.accepted = true
                stop()

                break
            case Qt.Key_MediaPrevious:
                event.accepted = true
                prev(true)

                break
            case Qt.Key_MediaNext:
                event.accepted = true
                next(true)

                break
            case Qt.Key_MediaTogglePlayPause:
            case Qt.Key_Return:
            case Qt.Key_Enter:
            case Qt.Key_Space:
                event.accepted = true
                toggle()

                break
            case Qt.Key_Left:
                event.accepted = true
                rewindProper()

                break
            case Qt.Key_Right:
                event.accepted = true
                forwardProper()

                break
            case Qt.Key_Up:
                event.accepted = true

                if (isUseSPDIF())
                    break

                volumeValue = getVolume() + 10

                if (volumeValue > getMaxVolume())
                    volumeValue = getMaxVolume()

                volume(volumeValue)
                muteButton.checked = false

                break
            case Qt.Key_Down:
                event.accepted = true

                if (isUseSPDIF())
                    break

                volumeValue = getVolume() - 10

                if (volumeValue < 0)
                    volumeValue = 0

                volume(volumeValue)
                muteButton.checked = false

                break
            case 0:
                event.accepted = true
                hookKeyPressed = false

                if (hookKeyPressedProcessing)
                {
                    hookKeyCount = 0
                    hookKeyPressedProcessing = false
                }
                else
                {
                    hookKeyCount++
                    headsetHookTimer.restart()
                }

                break
        }
    }
}
