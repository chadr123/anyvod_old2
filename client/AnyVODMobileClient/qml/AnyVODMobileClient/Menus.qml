﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtGraphicalEffects 1.0

Rectangle {
    id: topRoot
    anchors.fill: parent
    opacity: 0.0
    visible: false
    z: 10
    color: "transparent"
    layer.enabled: true

    signal menuItemSelected(int index)

    readonly property int noSelected: -100

    property Timer timer: null
    property int selectedItem: noSelected
    property string selectedColor: "red"

    property alias menuWidth: root.width
    property alias menuHeight: root.height
    property alias model: menuList.model
    property alias boundsBehavior: menuList.boundsBehavior

    function show(toX, toY)
    {
        root.x = toX
        root.y = toY
        visible = true
        opacity = 1.0

        if (timer != null)
            timer.stop()
    }

    function hide()
    {
        opacity = 0.0
        hideTimer.restart()

        if (timer != null)
            timer.restart()
    }

    Timer {
        id: hideTimer
        interval: opacityAni.duration

        onTriggered: {
            visible = false
            menuList.contentY = 0
        }
    }

    Behavior on opacity {
        NumberAnimation {
            id: opacityAni
            duration: 200
            easing.type: Easing.OutQuad
        }
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: topRoot.hide()
        onCanceled: topRoot.hide()
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    Rectangle {
        id: root
        color: "#f8f8f8"
        clip: true
        radius: 3

        ListView {
            id: menuList
            anchors.fill: parent
            boundsBehavior: Flickable.StopAtBounds

            property int maxWidth: 0

            delegate: Item {
                x: 10
                width: menuList.width
                height: model.separator !== undefined && model.separator ? 10 : 40

                ImageButton {
                    anchors.fill: parent
                    spacing: source == "" ? 0 : 5
                    image.width: source == "" ? 0 : 20
                    image.height: source == "" ? 0 : 20
                    text.text: model.text === undefined ? "" : model.text
                    text.color: model.menuID === selectedItem ? topRoot.selectedColor : "black"
                    source: model.icon === undefined ? "" : model.icon
                    visible: model.separator !== undefined && model.separator ? false : true

                    onClicked: {
                        menuItemSelected(index)
                        topRoot.hide()
                    }

                    Component.onCompleted: {
                        menuList.maxWidth = Math.max(menuList.maxWidth, Math.abs(text.advance.width))
                        root.width = menuList.maxWidth + image.width + parent.x * 2 + 10
                    }
                }

                Rectangle {
                    anchors.centerIn: parent
                    width: parent.width
                    height: 1
                    color: "lightgray"
                    visible: model.separator !== undefined && model.separator ? true : false
                }
            }
        }

        Scrollbar {
            id: scrollbar
            flickable: menuList
        }
    }
}
