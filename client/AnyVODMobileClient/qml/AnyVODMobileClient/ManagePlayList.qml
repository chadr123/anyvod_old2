﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import com.dcple.anyvod 1.0

Item {
    id: topRoot
    anchors.fill: parent
    focus: true

    readonly property int settingListDefaultHeight: height * 0.8

    property alias title: header.title

    signal selected(string name)
    signal goBack

    function exit()
    {
        goBack()
    }

    function showMenu(parent, name)
    {
        var globalCoord = parent.mapToItem(null, parent.x, parent.y)
        var showX = parent.x - menu.menuWidth + parent.width
        var showY = globalCoord.y - parent.height
        var offset = 10
        var maxHeight = showY + menu.menuHeight + offset

        if (maxHeight >= root.height)
            showY = root.height - (menu.menuHeight + offset)

        menu.name = name
        menu.show(showX, showY)
    }

    function updateNoItem()
    {
        noItems.visible = managePlayListModel.rowCount() === 0
    }

    ManagePlayListModel {
        id: managePlayListModel

        onModelReset: {
            updateNoItem()
        }

        onRowsRemoved: {
            updateNoItem()
        }
    }

    Component {
        id: confirmDelete

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("질문")
            message: qsTr("삭제 하시겠습니까?")
            useCancel: true

            onOk: {
                managePlayListModel.deleteItem(name)
            }
        }
    }

    Menus {
        id: menu
        menuHeight: 40
        model: ListModel {
            ListElement {
                text: qsTr("삭제")
                menuID: AnyVODEnums.MID_DELETE
            }
        }

        property string name

        onMenuItemSelected: {
            var item = model.get(index)

            switch (item.menuID)
            {
                case AnyVODEnums.MID_DELETE:
                    messageBoxLoader.name = name
                    messageBoxLoader.sourceComponent = confirmDelete

                    break
            }

            if (messageBoxLoader.sourceComponent != undefined)
                messageBoxLoader.forceActiveFocus()
        }
    }

    ColumnLayout {
        id: root
        anchors.fill: parent
        spacing: 0

        HeaderWithBack {
            id: header
            Layout.fillWidth: true
            Layout.minimumHeight: 50
            Layout.maximumHeight: Layout.minimumHeight

            onBackClicked: {
                topRoot.exit()
            }
        }

        Rectangle {
            id: listRoot
            color: "white"
            clip: true
            Layout.fillWidth: true
            Layout.fillHeight: true

            Rectangle {
                anchors.fill: parent
                color: "#f8f8f8"

                ListView {
                    id: list
                    anchors.fill: parent
                    model: managePlayListModel

                    delegate: Rectangle {
                        id: itemRoot
                        width: list.width
                        height: 50

                        MouseArea {
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.fill: parent

                            Rectangle {
                                anchors.fill: parent
                                anchors.leftMargin: 15
                                color: "transparent"

                                RowLayout {
                                    anchors.fill: parent

                                    Text {
                                        text: model.name
                                        verticalAlignment: Text.AlignVCenter
                                        elide: Text.ElideMiddle
                                        Layout.fillWidth: true
                                        Layout.fillHeight: true
                                    }

                                    ImageButton {
                                        id: openItemMenu
                                        image.width: 30
                                        image.height: 30
                                        source: "assets/itemMenu.png"
                                        Layout.minimumWidth: 40
                                        Layout.maximumWidth: Layout.minimumWidth
                                        Layout.minimumHeight: image.height
                                        Layout.maximumHeight: Layout.minimumHeight
                                        Layout.alignment: Qt.AlignHCenter

                                        onClicked: {
                                            showMenu(this, model.name)
                                        }
                                    }
                                }
                            }

                            onClicked: {
                                goBack()
                                selected(model.name)
                            }

                            onPressed: {
                                itemRoot.color = "lightgray"
                                itemRoot.opacity = 0.5
                            }

                            onCanceled: {
                                itemRoot.color = listRoot.color
                                itemRoot.opacity = 1.0
                            }

                            onReleased: {
                                onCanceled()
                            }
                        }

                        Rectangle {
                            anchors.bottom: parent.bottom
                            width: parent.width
                            height: 1
                            color: model.index === list.count - 1 ? "white" : "lightgray"
                        }
                    }

                    remove: Transition {
                         NumberAnimation { property: "opacity"; to: 0.0; duration: 200 }
                    }

                    removeDisplaced: Transition {
                        NumberAnimation { properties: "x,y"; duration: 200 }
                    }
                }

                Rectangle {
                    id: noItems
                    anchors.fill: parent
                    visible: false

                    ColumnLayout {
                        anchors.fill: parent
                        spacing: 10

                        Rectangle {
                            color: "transparent"
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                        }

                        Image {
                            source: "assets/nofiles.png"
                            fillMode: Image.PreserveAspectFit
                            Layout.minimumHeight: 100
                            Layout.maximumHeight: Layout.minimumHeight
                            Layout.fillWidth: true
                        }

                        Text {
                            horizontalAlignment: Text.AlignHCenter
                            text: qsTr("재생 목록이 없습니다.")
                            Layout.fillWidth: true
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                        }
                    }
                }

                Scrollbar {
                    id: scrollbar
                    flickable: list
                }
            }
        }
    }

    Loader {
        id: messageBoxLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        layer.enabled: true

        property Item focusParent: null
        property string name

        function exit()
        {
            if (focusParent != null)
            {
                focusParent.forceActiveFocus()
                focusParent = null
            }

            opacity = 0.0
            messageBoxHideTimer.restart()
        }

        Timer {
            id: messageBoxHideTimer
            interval: messageBoxOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: messageBoxOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: messageBoxLoader.item

            function onGoBack() {
                messageBoxLoader.exit()
            }
        }
    }

    Component.onCompleted: {
        updateNoItem()
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            topRoot.exit()
        }
    }
}
