﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0

Rectangle {
    id: topRoot
    anchors.fill: parent
    opacity: 0.0
    visible: false
    z: 10
    color: "transparent"
    layer.enabled: true

    property MediaPlayerSettingDelegate setting: null
    property alias windowWidth: root.width
    property alias windowHeight: root.height

    function show(toX, toY)
    {
        move(toX, toY)
        visible = true
        opacity = 1.0
    }

    function move(toX, toY)
    {
        root.x = toX
        root.y = toY
    }

    function hide()
    {
        opacity = 0.0
        hideTimer.restart()
    }

    TimeUtilDelegate {
        id: timeUtilDelegate
    }

    Timer {
        id: hideTimer
        interval: opacityAni.duration

        onTriggered: {
            visible = false
        }
    }

    Behavior on opacity {
        NumberAnimation {
            id: opacityAni
            duration: 200
            easing.type: Easing.OutQuad
        }
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: topRoot.hide()
        onCanceled: topRoot.hide()
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    Rectangle {
        id: root
        color: "#7f000000"
        radius: 3

        function getTimeString(time, defaultValue)
        {
            return time === 0.0 ? defaultValue : timeUtilDelegate.getTimeStringWithMSec(time)
        }

        ColumnLayout {
            anchors.fill: parent
            spacing: 5

            Rectangle {
                color: "transparent"
                Layout.fillWidth: true
                Layout.minimumHeight: 0
                Layout.maximumHeight: Layout.minimumHeight
            }

            Text {
                id: title
                width: parent.width
                font.pixelSize: 12
                verticalAlignment: Text.AlignVCenter
                text: qsTr("구간 반복 설정")
                color: "white"
                Layout.leftMargin: 10
                Layout.rightMargin: 10
            }

            RowLayout {
                spacing: 5
                Layout.fillWidth: true
                Layout.fillHeight: true

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 0
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }

                Rectangle {
                    id: startBox
                    color: "gray"
                    Layout.minimumWidth: start.width + 40
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignHCenter

                    ImageButton {
                        id: start
                        anchors.horizontalCenter: parent.horizontalCenter
                        height: parent.height
                        text.font.pixelSize: 15
                        text.text: root.getTimeString(setting.getSetting(AnyVODEnums.SK_REPEAT_RANGE_START), qsTr("시작"))
                        text.color: "white"
                        text.verticalAlignment: Text.AlignVCenter
                        text.horizontalAlignment: Text.AlignHCenter

                        onClicked: {
                            var start = setting.getSetting(AnyVODEnums.SK_REPEAT_RANGE_START)
                            var end = setting.getSetting(AnyVODEnums.SK_REPEAT_RANGE_END)

                            if (start === 0.0)
                                setting.setSetting(AnyVODEnums.SK_REPEAT_RANGE_START, true)
                            else
                                setting.setSetting(AnyVODEnums.SK_REPEAT_RANGE_START, false)

                            start = setting.getSetting(AnyVODEnums.SK_REPEAT_RANGE_START)
                            setting.setSetting(AnyVODEnums.SK_REPEAT_RANGE_ENABLE, start !== 0.0 && end !== 0.0)

                            text.text = root.getTimeString(start, qsTr("시작"))
                        }
                    }
                }

                Text {
                    text: "~"
                    color: "white"
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                }

                Rectangle {
                    id: endBox
                    color: "gray"
                    Layout.minimumWidth: end.width + 40
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignHCenter

                    ImageButton {
                        id: end
                        anchors.horizontalCenter: parent.horizontalCenter
                        height: parent.height
                        text.font.pixelSize: 15
                        text.text: root.getTimeString(setting.getSetting(AnyVODEnums.SK_REPEAT_RANGE_END), qsTr("종료"))
                        text.color: "white"
                        text.verticalAlignment: Text.AlignVCenter
                        text.horizontalAlignment: Text.AlignHCenter

                        onClicked: {
                            var start = setting.getSetting(AnyVODEnums.SK_REPEAT_RANGE_START)
                            var end = setting.getSetting(AnyVODEnums.SK_REPEAT_RANGE_END)

                            if (end === 0.0)
                                setting.setSetting(AnyVODEnums.SK_REPEAT_RANGE_END, true)
                            else
                                setting.setSetting(AnyVODEnums.SK_REPEAT_RANGE_END, false)

                            end = setting.getSetting(AnyVODEnums.SK_REPEAT_RANGE_END)
                            setting.setSetting(AnyVODEnums.SK_REPEAT_RANGE_ENABLE, start !== 0.0 && end !== 0.0)

                            text.text = root.getTimeString(end, qsTr("종료"))
                        }
                    }
                }

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 0
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }
            }

            Rectangle {
                color: "transparent"
                Layout.fillWidth: true
                Layout.minimumHeight: 0
                Layout.maximumHeight: Layout.minimumHeight
            }
        }
    }
}
