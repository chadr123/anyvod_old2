﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0

Rectangle {
    id: topRoot
    focus: true
    color: "#88000000"

    property int defaultValue: -1
    property alias listWidth: root.width
    property alias listHeight: root.height
    property alias title: header.title
    property alias model: list.model

    signal goBack
    signal itemSelected(int index)

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: goBack()
        onCanceled: goBack()
    }

    Rectangle {
        id: root
        anchors.centerIn: parent
        height: parent.height * 0.8
        radius: 3
        border.color: "gray"

        MouseArea {
            anchors.fill: parent

            ColumnLayout {
                anchors.fill: parent
                spacing: 0

                Header {
                    id: header
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight
                }

                Rectangle {
                    id: listRoot
                    color: "#f8f8f8"
                    clip: true
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    ListView {
                        id: list
                        anchors.fill: parent

                        property ImageCheckButton checkedItem: null

                        delegate: Rectangle {
                            width: list.width
                            height: 46

                            Rectangle {
                                id: itemRoot
                                anchors.fill: parent
                                color: "white"

                                MouseArea {
                                    id: mouseArea
                                    anchors.fill: parent

                                    Text {
                                        x: 10
                                        width: parent.width - checkButton.width - checkButton.anchors.rightMargin - 15
                                        height: parent.height
                                        verticalAlignment: Text.AlignVCenter
                                        font.pixelSize: parent.height * 0.4
                                        text: model.desc
                                        elide: Text.ElideMiddle
                                    }

                                    ImageCheckButton {
                                        id: checkButton
                                        anchors.verticalCenter: parent.verticalCenter
                                        anchors.right: parent.right
                                        anchors.rightMargin: scrollbar.width + 20
                                        enabled: false
                                        transparentWhenDisabled: false
                                        width: 20
                                        height: width
                                        checked: model.value === defaultValue
                                        checkedSource: "assets/checked.png"
                                        unCheckedSource: "assets/unchecked.png"

                                        Component.onCompleted: {
                                            if (checked)
                                                list.checkedItem = this
                                        }
                                    }

                                    onClicked: {
                                        if (list.checkedItem != null)
                                            list.checkedItem.checked = false

                                        checkButton.checked = true
                                        itemSelected(index)
                                        goBack()
                                    }

                                    onPressed: {
                                        itemRoot.color = "lightgray"
                                        itemRoot.opacity = 0.5
                                    }

                                    onCanceled: {
                                        itemRoot.color = "white"
                                        itemRoot.opacity = 1.0
                                    }

                                    onReleased: {
                                        onCanceled()
                                    }
                                }
                            }

                            Rectangle {
                                anchors.bottom: parent.bottom
                                width: parent.width
                                height: 1
                                color: model.index === list.count - 1 ? "white" : "lightgray"
                            }
                        }
                    }

                    Scrollbar {
                        id: scrollbar
                        flickable: list
                    }
                }

                FooterWithButtons {
                    types: AnyVODEnums.BT_CLOSE
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight

                    onCloseClicked: {
                        goBack()
                    }
                }
            }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            goBack()
        }
    }
}
