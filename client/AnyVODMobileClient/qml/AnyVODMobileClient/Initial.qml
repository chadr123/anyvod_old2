﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import com.dcple.anyvod 1.0
import "." as AnyVOD

Item {
    focus: true

    property bool willExit: false

    readonly property int settingListDefaultHeight: height * 0.8
    readonly property int settingListDefaultWidth: width * 0.8
    readonly property int settingListWideHeight: height * 0.9
    readonly property int settingListWideWidth: width * 0.9

    Component {
        id: captureExt

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 285)
            title: qsTr("캡처 확장자")
            query: settingDelegate
            type: AnyVODEnums.SK_SELECT_CAPTURE_EXT_ORDER
            model: ListModel {
                Component.onCompleted: {
                    var captureExts = captureExtDelegate.getCaptureExts()

                    captureExts.forEach(function(ext) {
                        append({ "desc": ext, "value": ext })
                    })
                }
            }
        }
    }

    Component {
        id: font

        SettingList {
            title: qsTr("글꼴")
            query: settingDelegate
            type: AnyVODEnums.SK_FONT
            model: ListModel {
                Component.onCompleted: {
                    var fonts = fontInfoDelegate.families

                    fonts.forEach(function(font) {
                        append({ "desc": font, "value": font , "font": font })
                    })
                }
            }

            onItemSelected: {
                messageBoxLoader.activeItem = settingLoader
                messageBoxLoader.sourceComponent = restartFont
                messageBoxLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: deinterlaceMethod

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 239)
            title: qsTr("디인터레이스 활성화 기준")
            query: settingDelegate
            type: AnyVODEnums.SK_DEINTERLACE_METHOD_ORDER
            model: ListModel {
                ListElement { desc: qsTr("자동"); value: AnyVODEnums.DM_AUTO }
                ListElement { desc: qsTr("사용"); value: AnyVODEnums.DM_USE }
                ListElement { desc: qsTr("사용 안 함"); value: AnyVODEnums.DM_NOUSE }
            }
        }
    }

    Component {
        id: deinterlaceAlgorithm

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 423)
            title: qsTr("디인터레이스 알고리즘")
            query: settingDelegate
            type: AnyVODEnums.SK_DEINTERLACE_ALGORITHM_ORDER
            model: ListModel {
                ListElement { desc: qsTr("Blend"); value: AnyVODEnums.DA_BLEND }
                ListElement { desc: qsTr("BOB"); value: AnyVODEnums.DA_BOB }
                ListElement { desc: qsTr("YADIF"); value: AnyVODEnums.DA_YADIF }
                ListElement { desc: qsTr("YADIF(BOB)"); value: AnyVODEnums.DA_YADIF_BOB }
                ListElement { desc: qsTr("Weston 3 Field"); value: AnyVODEnums.DA_W3FDIF }
                ListElement { desc: qsTr("Adaptive Kernel"); value: AnyVODEnums.DA_KERNDEINT }
                ListElement { desc: qsTr("Motion Compensation"); value: AnyVODEnums.DA_MCDEINT }
                ListElement { desc: qsTr("Bob Weaver"); value: AnyVODEnums.DA_BWDIF }
                ListElement { desc: qsTr("Bob Weaver(BOB)"); value: AnyVODEnums.DA_BWDIF_BOB }
            }
        }
    }

    Component {
        id: subtitleHAlign

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 331)
            title: qsTr("자막 / 가사 가로 정렬")
            query: settingDelegate
            type: AnyVODEnums.SK_SUBTITLE_HALIGN_ORDER
            model: ListModel {
                ListElement { desc: qsTr("기본 정렬"); value: AnyVODEnums.HAM_NONE }
                ListElement { desc: qsTr("자동 정렬"); value: AnyVODEnums.HAM_AUTO }
                ListElement { desc: qsTr("왼쪽 정렬"); value: AnyVODEnums.HAM_LEFT }
                ListElement { desc: qsTr("가운데 정렬"); value: AnyVODEnums.HAM_MIDDLE }
                ListElement { desc: qsTr("오른쪽 정렬"); value: AnyVODEnums.HAM_RIGHT }
            }
        }
    }

    Component {
        id: subtitleVAlign

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 239)
            title: qsTr("자막 / 가사 세로 정렬")
            query: settingDelegate
            type: AnyVODEnums.SK_SUBTITLE_VALIGN_ORDER
            model: ListModel {
                ListElement { desc: qsTr("기본 정렬"); value: AnyVODEnums.VAM_NONE }
                ListElement { desc: qsTr("상단 정렬"); value: AnyVODEnums.VAM_TOP }
                ListElement { desc: qsTr("가운데 정렬"); value: AnyVODEnums.VAM_MIDDLE }
                ListElement { desc: qsTr("하단 정렬"); value: AnyVODEnums.VAM_BOTTOM }
            }
        }
    }

    Component {
        id: subtitleEncoding

        SettingList {
            title: qsTr("자막 / 가사 텍스트 인코딩")
            query: settingDelegate
            type: AnyVODEnums.SK_OPEN_TEXT_ENCODING
            model: ListModel {
                Component.onCompleted: {
                    var codecs = textCodecDelegate.names

                    codecs.forEach(function(codec) {
                        append({ "desc": codec, "value": codec })
                    })
                }
            }
        }
    }

    Component {
        id: lang

        SettingList {
            title: qsTr("Languages")
            query: settingDelegate
            type: AnyVODEnums.SK_LANG
            model: ListModel {
                Component.onCompleted: {
                    var langs = languageDelegate.getAllList()

                    langs.forEach(function(lang) {
                        append({ "desc": languageDelegate.getDesc(lang), "value": lang })
                    })
                }
            }

            onItemSelected: {
                messageBoxLoader.activeItem = settingLoader
                messageBoxLoader.sourceComponent = restartApp
                messageBoxLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: audioDevice

        SettingList {
            title: qsTr("오디오 장치")
            query: settingDelegate
            type: AnyVODEnums.SK_AUDIO_DEVICE_ORDER
            model: ListModel {
                Component.onCompleted: {
                    var devices = audioDeviceDelegate.getDevices()

                    append({ "desc": qsTr("기본 장치"), "value": -1 })

                    var i = 0
                    devices.forEach(function(device) {
                        append({ "desc": device, "value": i++ })
                    })
                }
            }
        }
    }

    Component {
        id: spdifAudioDevice

        SettingList {
            title: qsTr("출력 장치")
            query: settingDelegate
            type: AnyVODEnums.SK_SPDIF_AUDIO_DEVICE_ORDER
            model: ListModel {
                Component.onCompleted: {
                    var devices = spdifDelegate.getDevices()

                    append({ "desc": qsTr("기본 장치"), "value": -1 })

                    var i = 0
                    devices.forEach(function(device) {
                        append({ "desc": device.replace(/\n/gi, " "), "value": i++ })
                    })
                }
            }
        }
    }

    Component {
        id: spdifEncoding

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 239)
            title: qsTr("인코딩")
            query: settingDelegate
            type: AnyVODEnums.SK_USE_SPDIF_ENCODING_ORDER
            model: ListModel {
                ListElement { desc: qsTr("사용 안 함"); value: AnyVODEnums.SEM_NONE }
                ListElement { desc: qsTr("AC3"); value: AnyVODEnums.SEM_AC3 }
                ListElement { desc: qsTr("DTS"); value: AnyVODEnums.SEM_DTS }
            }
        }
    }

    Component {
        id: spdifSampleRate

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 285)
            title: qsTr("샘플링 속도")
            query: settingDelegate
            type: AnyVODEnums.SK_SPDIF_SAMPLE_RATE_ORDER
            model: ListModel {
                Component.onCompleted: {
                    var samplerates = spdifDelegate.getSampleRates()
                    var descs = spdifDelegate.getSampleRateDescs()
                    var i = 0

                    samplerates.forEach(function(samplerate) {
                        append({ "desc": descs[i++], "value": samplerate })
                    })
                }
            }
        }
    }

    Component {
        id: keyboardLanguage

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 285)
            title: qsTr("언어")
            query: settingDelegate
            type: AnyVODEnums.SK_KEYBOARD_LANGUAGE
            model: ListModel {
                Component.onCompleted: {
                    var languages =
                    [
                        "ar_AR",
                        "bg_BG",
                        "cs_CZ",
                        "da_DK",
                        "de_DE",
                        "el_GR",
                        "en_GB",
                        "en_US",
                        "es_ES",
                        "es_MX",
                        "et_EE",
                        "fa_FA",
                        "fi_FI",
                        "fr_CA",
                        "fr_FR",
                        "he_IL",
                        "hi_IN",
                        "hr_HR",
                        "hu_HU",
                        "id_ID",
                        "it_IT",
                        "ja_JP",
                        "ko_KR",
                        "ms_MY",
                        "nb_NO",
                        "nl_NL",
                        "pl_PL",
                        "pt_BR",
                        "pt_PT",
                        "ro_RO",
                        "ru_RU",
                        "sk_SK",
                        "sl_SI",
                        "sq_AL",
                        "sr_SP",
                        "sv_SE",
                        "th_TH",
                        "tr_TR",
                        "uk_UA",
                        "vi_VN",
                        "zh_CN",
                        "zh_TW"
                    ]

                    var i = 0
                    languages.forEach(function(language) {
                        append({ "desc": languageDelegate.getDesc(language), "value": language })
                    })
                }
            }

            onItemSelected: {
                messageBoxLoader.activeItem = settingLoader
                messageBoxLoader.sourceComponent = restartApp
                messageBoxLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: checkUpdateGap

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 423)
            title: qsTr("앱 자동 업데이트 확인 주기")
            query: settingDelegate
            type: AnyVODEnums.SK_CHECK_UPDATE_GAP
            model: ListModel {
                ListElement { desc: qsTr("확인하지 않음"); value: AnyVODEnums.CUG_NONE }
                ListElement { desc: qsTr("매 실행 시"); value: AnyVODEnums.CUG_ALWAYS }
                ListElement { desc: qsTr("1일"); value: AnyVODEnums.CUG_EVERY_DAY }
                ListElement { desc: qsTr("1주일"); value: AnyVODEnums.CUG_WEEK }
                ListElement { desc: qsTr("한 달"); value: AnyVODEnums.CUG_MONTH }
                ListElement { desc: qsTr("반 년"); value: AnyVODEnums.CUG_HALF_YEAR }
                ListElement { desc: qsTr("1년"); value: AnyVODEnums.CUG_YEAR }
            }

            onItemSelected: {
                updaterDelegate.updateCurrentDate()
            }
        }
    }

    Component {
        id: equalizer

        Equalizer {
            title: qsTr("이퀄라이저")
        }
    }

    Component {
        id: external

        OpenExternal {
            windowHeight: Math.min(settingListDefaultHeight, 200)
            title: qsTr("외부 열기")

            onAccepted: {
                if (url.length > 0)
                    root.openMedia(url)
            }
        }
    }

    Component {
        id: proxyInfo

        ProxyInfo {
            windowHeight: Math.min(settingListDefaultHeight, 245)
            title: qsTr("프록시 설정")
            url: settingDelegate.getSetting(AnyVODEnums.SK_PROXY_INFO)

            onAccepted: {
                settingDelegate.setSetting(AnyVODEnums.SK_PROXY_INFO, url)
            }
        }
    }

    Component {
        id: serverSetting

        ServerSetting {
            windowHeight: Math.min(settingListDefaultHeight, 230)
            windowWidth: Math.min(settingListDefaultWidth, 300)
            title: qsTr("서버 설정")
            address: settingDelegate.getSetting(AnyVODEnums.SK_SERVER_ADDRESS)
            commandPort: settingDelegate.getSetting(AnyVODEnums.SK_SERVER_COMMAND_PORT)
            streamPort: settingDelegate.getSetting(AnyVODEnums.SK_SERVER_STREAM_PORT)

            onAccepted: {
                settingDelegate.setSetting(AnyVODEnums.SK_SERVER_ADDRESS, address)
                settingDelegate.setSetting(AnyVODEnums.SK_SERVER_COMMAND_PORT, commandPort)
                settingDelegate.setSetting(AnyVODEnums.SK_SERVER_STREAM_PORT, streamPort)
            }
        }
    }

    Component {
        id: notSupported

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("정보")
            message: qsTr("지원하지 않는 기능입니다.")
        }
    }

    Component {
        id: failedPlayBox

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("오류")
            message: qsTr("파일을 열 수 없습니다.")
        }
    }

    Component {
        id: failedDeleteFileBox

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("오류")
            message: qsTr("파일을 삭제 할 수 없습니다.")
        }
    }

    Component {
        id: failedDeleteDirectoryBox

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("오류")
            message: qsTr("디렉토리를 삭제 할 수 없습니다.")
        }
    }

    Component {
        id: failedDeleteFileAndroidSDCardBox

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 165)
            windowWidth: settingListWideWidth
            title: qsTr("오류")
            message: qsTr("외장 메모리의 파일을 삭제 할 수 없습니다.\n이는 안드로이드 정책에 의한 것일 수 있습니다.")
        }
    }

    Component {
        id: failedDeleteDirectoryAndroidSDCardBox

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 165)
            windowWidth: settingListWideWidth
            title: qsTr("오류")
            message: qsTr("외장 메모리의 디렉토리를 삭제 할 수 없습니다.\n이는 안드로이드 정책에 의한 것일 수 있습니다.")
        }
    }

    Component {
        id: restartFont

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("정보")
            message: qsTr("변경된 글꼴로 적용하기 위해서는 앱을 재 시작 해주세요.")
        }
    }

    Component {
        id: restartApp

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("Info")
            message: qsTr("Restart the app to apply changes.")
        }
    }

    Component {
        id: confirmExit

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("질문")
            message: qsTr("종료 하시겠습니까?")
            useCancel: true
            usePowerOff: Qt.platform.os === "linux"

            onOk: {
                Qt.quit()
            }

            onPowerOff: {
                Qt.exit(AnyVODEnums.ET_POWER_OFF)
            }
        }
    }

    Component {
        id: failedLoadSettingsBox

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("오류")
            message: qsTr("설정을 불러 오지 못했습니다.")
        }
    }

    Component {
        id: failedSaveSettingsBox

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("오류")
            message: qsTr("설정을 저장하지 못했습니다.")
        }
    }

    Component {
        id: restartSettings

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("정보")
            message: qsTr("설정을 불러왔습니다. 적용하려면 앱를 재 시작 해주세요.")
        }
    }

    Component {
        id: confirmDirectoryDelete

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("질문")
            message: qsTr("삭제 하시겠습니까?")
            useCancel: true

            onOk: {
                if (!fileListModel.deletePath(messageBoxLoader.path))
                {
                    if (fileListModel.isSecondaryPath(messageBoxLoader.path) && Qt.platform.os === "android")
                        messageBoxSecondLoader.sourceComponent = failedDeleteDirectoryAndroidSDCardBox
                    else
                        messageBoxSecondLoader.sourceComponent = failedDeleteDirectoryBox
                }

                if (messageBoxSecondLoader.sourceComponent != undefined)
                    messageBoxSecondLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: confirmFileDelete

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("질문")
            message: qsTr("삭제 하시겠습니까?")
            useCancel: true

            onOk: {
                if (!fileListModel.deletePath(messageBoxLoader.path))
                {
                    if (fileListModel.isSecondaryPath(messageBoxLoader.path) && Qt.platform.os === "android")
                        messageBoxSecondLoader.sourceComponent = failedDeleteFileAndroidSDCardBox
                    else
                        messageBoxSecondLoader.sourceComponent = failedDeleteFileBox
                }

                if (messageBoxSecondLoader.sourceComponent != undefined)
                    messageBoxSecondLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: logout

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("질문")
            message: qsTr("로그아웃 하시겠습니까?")
            useCancel: true

            onOk: {
                socketDelegate.logout()
            }
        }
    }

    Component {
        id: needLogin

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("오류")
            message: qsTr("로그인을 해야 합니다.")
        }
    }

    Component {
        id: info

        SettingTextViewer {
            title: qsTr("AnyVOD 정보")
            desc.text: versionDelegate.get()
            desc.wrapMode: TextEdit.Wrap
            flickDir: Flickable.VerticalFlick
        }
    }

    Component {
        id: license

        SettingTextViewer {
            title: qsTr("라이센스")
            desc.text: licenseDelegate.get()
            desc.font.pixelSize: 12
        }
    }

    Component {
        id: openCaptureSaveDir

        FileSystemDialog {
            fileListObjName: "openCaptureSaveDir"
            title: qsTr("저장 디렉토리 설정")
            onlyDirectory: true
            startDirectory: settingDelegate.getSetting(AnyVODEnums.SK_SELECT_CAPTURE_DIRECTORY)

            onAccepted: {
                settingDelegate.setSetting(AnyVODEnums.SK_SELECT_CAPTURE_DIRECTORY, selectedPath)
            }
        }
    }

    Component {
        id: loadSettings

        FileSystemDialog {
            fileListObjName: "loadSettings"
            title: qsTr("설정 불러오기")
            onlyETC: true

            onAccepted: {
                messageBoxLoader.activeItem = settingLoader

                if (settingDelegate.loadSettings(selectedPath))
                    messageBoxLoader.sourceComponent = restartSettings
                else
                    messageBoxLoader.sourceComponent = failedLoadSettingsBox

                if (messageBoxLoader.sourceComponent != undefined)
                    messageBoxLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: saveSettings

        FileSystemDialog {
            fileListObjName: "saveSettings"
            title: qsTr("설정 저장하기")
            onlyETC: true
            saveMode: true

            onAccepted: {
                if (!settingDelegate.saveSettings(selectedPath))
                {
                    messageBoxLoader.activeItem = settingLoader
                    messageBoxLoader.sourceComponent = failedSaveSettingsBox
                }

                if (messageBoxLoader.sourceComponent != undefined)
                    messageBoxLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: bluetoothAudioSync

        SettingSlider {
            sliderWidth: Math.min(settingListDefaultHeight, 350)
            title: qsTr("블루투스 싱크")
            desc: qsTr("기기에 따라 값이 적용 되지 않을 수 있습니다.")
            query: settingDelegate
            type: AnyVODEnums.SK_BLUETOOTH_AUDIO_SYNC
            defaultValue: 0
            roundValue: 1
            unitPostfix: qsTr("초")
            slider.from: -300
            slider.to: 300
            slider.stepSize: 0.5
        }
    }

    Component {
        id: playOrder

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 331)
            title: qsTr("재생 순서 설정")
            query: settingDelegate
            type: AnyVODEnums.SK_PLAY_ORDER
            model: ListModel {
                ListElement { desc: qsTr("전체 순차 재생"); value: AnyVODEnums.PM_TOTAL }
                ListElement { desc: qsTr("전체 반복 재생"); value: AnyVODEnums.PM_TOTAL_REPEAT }
                ListElement { desc: qsTr("한 개 재생"); value: AnyVODEnums.PM_SINGLE }
                ListElement { desc: qsTr("한 개 반복 재생"); value: AnyVODEnums.PM_SINGLE_REPEAT }
                ListElement { desc: qsTr("무작위 재생"); value: AnyVODEnums.PM_RANDOM }
            }
        }
    }

    Component {
        id: settings

        SettingDialog {
            title: qsTr("설정")
            model: MainSettingItems {}
            query: settingDelegate

            onNotSupportedFunction: {
                messageBoxLoader.activeItem = this
                messageBoxLoader.sourceComponent = notSupported
                messageBoxLoader.forceActiveFocus()
            }

            onItemSelected: {
                var item = model.get(index)

                switch (item.type)
                {
                    case AnyVODEnums.SK_INFO:
                        settingDlgLoader.sourceComponent = info
                        break
                    case AnyVODEnums.SK_LICENSE:
                        settingDlgLoader.sourceComponent = license
                        break
                    case AnyVODEnums.SK_LANG:
                        settingDlgLoader.sourceComponent = lang
                        break
                    case AnyVODEnums.SK_FONT:
                        settingDlgLoader.sourceComponent = font
                        break
                    case AnyVODEnums.SK_CHECK_UPDATE_GAP:
                        settingDlgLoader.sourceComponent = checkUpdateGap
                        break
                    case AnyVODEnums.SK_SELECT_CAPTURE_EXT_ORDER:
                        settingDlgLoader.sourceComponent = captureExt
                        break
                    case AnyVODEnums.SK_SELECT_CAPTURE_DIRECTORY:
                        settingDlgLoader.sourceComponent = openCaptureSaveDir
                        break
                    case AnyVODEnums.SK_DEINTERLACE_METHOD_ORDER:
                        settingDlgLoader.sourceComponent = deinterlaceMethod
                        break
                    case AnyVODEnums.SK_DEINTERLACE_ALGORITHM_ORDER:
                        settingDlgLoader.sourceComponent = deinterlaceAlgorithm
                        break
                    case AnyVODEnums.SK_PLAY_ORDER:
                        settingDlgLoader.sourceComponent = playOrder
                        break
                    case AnyVODEnums.SK_SUBTITLE_HALIGN_ORDER:
                        settingDlgLoader.sourceComponent = subtitleHAlign
                        break
                    case AnyVODEnums.SK_SUBTITLE_VALIGN_ORDER:
                        settingDlgLoader.sourceComponent = subtitleVAlign
                        break
                    case AnyVODEnums.SK_OPEN_TEXT_ENCODING:
                        settingDlgLoader.sourceComponent = subtitleEncoding
                        break
                    case AnyVODEnums.SK_AUDIO_DEVICE_ORDER:
                        settingDlgLoader.sourceComponent = audioDevice
                        break
                    case AnyVODEnums.SK_SPDIF_AUDIO_DEVICE_ORDER:
                        settingDlgLoader.sourceComponent = spdifAudioDevice
                        break
                    case AnyVODEnums.SK_USE_SPDIF_ENCODING_ORDER:
                        settingDlgLoader.sourceComponent = spdifEncoding
                        break
                    case AnyVODEnums.SK_SPDIF_SAMPLE_RATE_ORDER:
                        settingDlgLoader.sourceComponent = spdifSampleRate
                        break
                    case AnyVODEnums.SK_KEYBOARD_LANGUAGE:
                        settingDlgLoader.sourceComponent = keyboardLanguage
                        break
                    case AnyVODEnums.SK_PROXY_INFO:
                        settingDlgLoader.sourceComponent = proxyInfo
                        break
                    case AnyVODEnums.SK_BLUETOOTH_AUDIO_SYNC:
                        settingDlgLoader.sourceComponent = bluetoothAudioSync
                        break
                    case AnyVODEnums.SK_OPEN_SERVER_SETTING:
                        settingDlgLoader.sourceComponent = serverSetting
                        break
                    case AnyVODEnums.SK_LOAD_SETTINGS:
                        settingDlgLoader.sourceComponent = loadSettings
                        break
                    case AnyVODEnums.SK_SAVE_SETTINGS:
                        settingDlgLoader.sourceComponent = saveSettings
                        break
                }

                if (settingDlgLoader.sourceComponent != undefined)
                    settingDlgLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: scanDTV

        ScanDTVChannel {
            title: qsTr("채널 검색")
        }
    }

    Component {
        id: dtvChannelList

        DTVChannelList {
            title: qsTr("채널 목록")

            onGoBack: {
                if (playDTV)
                {
                    root.openMedia(dtvPath)
                    settingLoader.exit()
                }
            }
        }
    }

    Component {
        id: openDTV

        SettingDialog {
            title: qsTr("DTV 열기")
            model: DTVSettingItems {}
            query: settingDelegate

            onItemSelected: {
                var item = model.get(index)

                switch (item.type)
                {
                    case AnyVODEnums.SK_OPEN_DTV_CHANNEL_LIST:
                        settingDlgLoader.sourceComponent = dtvChannelList
                        break
                    case AnyVODEnums.SK_OPEN_DTV_SCAN_CHANNEL:
                        settingDlgLoader.sourceComponent = scanDTV
                        break
                }

                if (settingDlgLoader.sourceComponent != undefined)
                    settingDlgLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: scanRadio

        ScanRadioChannel {
            title: qsTr("채널 검색")
        }
    }

    Component {
        id: radioChannelList

        RadioChannelList {
            title: qsTr("채널 목록")

            onGoBack: {
                if (playRadio)
                {
                    root.openMedia(radioPath)
                    settingLoader.exit()
                }
            }
        }
    }

    Component {
        id: openRadio

        SettingDialog {
            title: qsTr("라디오 열기")
            model: RadioSettingItems {}
            query: settingDelegate

            onItemSelected: {
                var item = model.get(index)

                switch (item.type)
                {
                    case AnyVODEnums.SK_OPEN_RADIO_CHANNEL_LIST:
                        settingDlgLoader.sourceComponent = radioChannelList
                        break
                    case AnyVODEnums.SK_OPEN_RADIO_SCAN_CHANNEL:
                        settingDlgLoader.sourceComponent = scanRadio
                        break
                }

                if (settingDlgLoader.sourceComponent != undefined)
                    settingDlgLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: remoteServer

        SettingDialog {
            title: qsTr("원격 서버")
            model: RemoteServerSettingItems {}
            query: settingDelegate

            onItemSelected: {
                var item = model.get(index)

                switch (item.type)
                {
                    case AnyVODEnums.SK_LOGIN:
                        if (socketDelegate.isLogined())
                        {
                            messageBoxLoader.activeItem = settingLoader
                            messageBoxLoader.sourceComponent = logout
                        }
                        else
                        {
                            settingDlgLoader.sourceComponent = login
                        }

                        break
                    case AnyVODEnums.SK_OPEN_REMOTE_FILE_LIST:
                        if (socketDelegate.isLogined())
                        {
                            settingDlgLoader.sourceComponent = remoteFileList
                        }
                        else
                        {
                            messageBoxLoader.activeItem = settingLoader
                            messageBoxLoader.sourceComponent = needLogin
                        }

                        break
                }

                if (settingDlgLoader.sourceComponent != undefined)
                    settingDlgLoader.forceActiveFocus()

                if (messageBoxLoader.sourceComponent != undefined)
                    messageBoxLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: managePlayList

        ManagePlayList {
            title: qsTr("재생 목록")

            onSelected: {
                root.openPlayList(name)
            }
        }
    }

    Component {
        id: searchMedia

        SearchMedia {
            title: qsTr("검색")
            listModel: fileListModel

            onAccepted: {
                root.openPlayListItems(list, index)
            }
        }
    }

    Component {
        id: remoteFileList

        RemoteFileListDialog {
            title: qsTr("원격 파일목록")

            onAccepted: {
                settingLoader.exit()
                root.openRemotePlayListItems(list, index)
            }
        }
    }

    Component {
        id: login

        Login {
            windowHeight: Math.min(settingListDefaultHeight, 220)
            windowWidth: Math.min(settingListDefaultWidth, 280)
            title: qsTr("원격 서버 로그인")

            onAccepted: {
                settingDlgSecondLoader.sourceComponent = remoteFileList
                settingDlgSecondLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: screenExplorer

        ScreenExplorer {
            title: qsTr("장면 탐색")
            listModel: screenExplorerListModel

            onSelected: {
                root.openMediaWithoutLastPos(filePath, time)
            }

            Component.onCompleted: {
                filePath = messageBoxLoader.path
                init()
            }
        }
    }

    Component {
        id: getUpdate

        GetUpdate {
            windowHeight: Math.min(settingListDefaultHeight, 180)
            title: qsTr("업데이트 다운로드")
            link: messageBoxLoader.path
            desc: qsTr("AnyVOD 업데이트가 존재합니다.\n업데이트를 다운로드 하려면 여기를 클릭하세요.")
        }
    }

    MainSettingDelegate {
        id: settingDelegate
    }

    LanguageDelegate {
        id: languageDelegate
    }

    VersionDelegate {
        id: versionDelegate
    }

    LicenseDelegate {
        id: licenseDelegate
    }

    ToastDelegate {
        id: toastDelegate
    }

    StatusBarDelegate {
        id: statusBarDelegate
    }

    AudioDeviceDelegate {
        id: audioDeviceDelegate
    }

    SPDIFDelegate {
        id: spdifDelegate
    }

    FileTypeDelegate {
        id: fileTypeDelegate
    }

    CaptureExtDelegate {
        id: captureExtDelegate
    }

    RemoteFileUtilDelegate {
        id: remoteFileUtilDelegate
    }

    TextCodecDelegate {
        id: textCodecDelegate
    }

    FontInfoDelegate {
        id: fontInfoDelegate
    }

    UpdaterDelegate {
        id: updaterDelegate
    }

    SocketDelegate {
        id: socketDelegate
    }

    AutoStartDelegate {
        id: autoStartDelegate

        onNewStarted: {
            root.checkStartingByURL()
        }
    }

    ScreenExplorerListModel {
        id: screenExplorerListModel
    }

    FileListModel {
        id: fileListModel
        objectName: "mainFileList"

        function checkEmpty() {
            noFiles.visible = rowCount() === 0
        }

        Component.onCompleted: {
            init()

            switch (quickBarType)
            {
                case FileListModel.InternalType:
                    primary.checked = true
                    break
                case FileListModel.ExternalType:
                    secondary.checked = true
                    break
                case FileListModel.VideoType:
                    video.checked = true
                    break
                case FileListModel.AudioType:
                    audio.checked = true
                    break
            }
        }

        onModelReset: {
            checkEmpty()
        }

        onRowsRemoved: {
            checkEmpty()
        }
    }

    Menus {
        id: mainMenu
        menuHeight: 320
        model: MainMenuItems {}

        onMenuItemSelected: {
            var item = model.get(index)

            switch (item.menuID)
            {
                case AnyVODEnums.MID_EQUALIZER:
                    settingLoader.sourceComponent = equalizer
                    break
                case AnyVODEnums.MID_OPEN_EXTERNAL:
                    settingLoader.hideParent = false
                    settingLoader.sourceComponent = external
                    break
                case AnyVODEnums.MID_OPEN_DTV:
                    if (Qt.platform.os === "linux")
                        settingLoader.sourceComponent = openDTV
                    else
                        messageBoxLoader.sourceComponent = notSupported

                    break
                case AnyVODEnums.MID_OPEN_RADIO:
                    if (Qt.platform.os === "linux")
                        settingLoader.sourceComponent = openRadio
                    else
                        messageBoxLoader.sourceComponent = notSupported

                    break
                case AnyVODEnums.MID_SETTINGS:
                    settingLoader.sourceComponent = settings
                    break
                case AnyVODEnums.MID_SEARCH:
                    settingLoader.hideParent = false
                    settingLoader.sourceComponent = searchMedia
                    break
                case AnyVODEnums.MID_MANAGE_PLAY_LIST:
                    settingLoader.sourceComponent = managePlayList
                    break
                case AnyVODEnums.MID_REMOTE_SERVER:
                    settingLoader.sourceComponent = remoteServer
                    break
            }

            if (settingLoader.sourceComponent != undefined)
                settingLoader.forceActiveFocus()

            if (messageBoxLoader.sourceComponent != undefined)
                messageBoxLoader.forceActiveFocus()
        }
    }

    Menus {
        id: sortMenu
        menuHeight: 330
        model: SortMenuItems {}

        onMenuItemSelected: {
            var item = model.get(index)

            fileListModel.setSortType(item.menuID)
            root.updateCurrentFileList()
        }
    }

    Menus {
        id: dirItemMenu
        menuHeight: 80
        model: DirItemMenuItems {}

        property string path

        onMenuItemSelected: {
            var item = model.get(index)

            switch (item.menuID)
            {
                case AnyVODEnums.MID_PLAY:
                    visible = false
                    root.openMedia(path)

                    break
                case AnyVODEnums.MID_DELETE:
                    messageBoxLoader.path = path
                    messageBoxLoader.sourceComponent = confirmDirectoryDelete

                    break
            }

            if (messageBoxLoader.sourceComponent != undefined)
                messageBoxLoader.forceActiveFocus()
        }
    }

    Menus {
        id: fileItemMenu
        menuHeight: 80
        model: FileItemMenuItems {}

        property string path

        onMenuItemSelected: {
            var item = model.get(index)

            switch (item.menuID)
            {
                case AnyVODEnums.MID_SCREEN_EXPLORER:
                    if (screenExplorerListModel.isMovie(path))
                    {
                        messageBoxLoader.path = path
                        messageBoxLoader.sourceComponent = screenExplorer
                    }
                    else
                    {
                        messageBoxLoader.sourceComponent = notSupported
                    }

                    break
                case AnyVODEnums.MID_DELETE:
                    messageBoxLoader.path = path
                    messageBoxLoader.sourceComponent = confirmFileDelete

                    break
            }

            if (messageBoxLoader.sourceComponent != undefined)
                messageBoxLoader.forceActiveFocus()
        }
    }

    Menus {
        id: secondaryPathMenu
        model: ListModel {}

        onMenuItemSelected: {
            var item = model.get(index)

            root.selectSecondaryPath(item.menuID)
        }
    }

    ColumnLayout {
        id: root
        anchors.fill: parent
        spacing: 0

        Behavior on opacity {
            NumberAnimation {
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.minimumHeight: 50
            Layout.maximumHeight: Layout.minimumHeight
            gradient: HeaderGradient {}

            RowLayout {
                id: navigation
                anchors.fill: parent

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 10
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }

                ImageButton {
                    id: titleIcon
                    image.width: 20
                    image.height: image.width
                    source: "assets/icon.png"
                    enabled: Qt.platform.os === "android" ? false : true
                    transparentWhenDisabled: false
                    Layout.minimumWidth: 30
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignHCenter

                    onClicked: {
                        messageBoxLoader.sourceComponent = confirmExit
                        messageBoxLoader.forceActiveFocus()
                    }
                }

                ImageButton {
                    id: back
                    image.width: 20
                    image.height: image.width
                    source: "assets/back.png"
                    visible: false
                    Layout.minimumWidth: 30
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignHCenter

                    signal goBack

                    onClicked: {
                        goBack()
                    }

                    onGoBack: {
                        var contY = fileListModel.cdup()

                        fileListModel.update()

                        var maxHeight = storage.contentHeight - storage.height

                        if (maxHeight < 0.0)
                            maxHeight = storage.height

                        if (contY < 0.0)
                            contY = 0.0
                        else if (contY > maxHeight)
                            contY = maxHeight

                        storage.contentY = contY

                        scrollbar.showScrollbar()
                        scrollbar.hideScrollbar()

                        root.hideMenus()
                        root.setTitleText(fileListModel.curDirName)

                        if (fileListModel.curDirName === fileListModel.topSignature)
                            root.resetToTop()
                    }
                }

                MarqueeText {
                    id: title
                    text.verticalAlignment: Text.AlignVCenter
                    text.color: "white"
                    text.font.pixelSize: parent.height * 0.3
                    textValue: fileListModel.topSignature
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                }

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 7
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }

                ImageButton {
                    id: reload
                    image.width: 20
                    image.height: image.width
                    source: "assets/reload.png"
                    Layout.minimumWidth: 30
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignHCenter

                    onClicked: {
                        root.updateCurrentFileList()
                    }
                }

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 0
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }

                ImageButton {
                    id: sort
                    image.width: 20
                    image.height: image.width
                    source: "assets/sort.png"
                    Layout.minimumWidth: 30
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignHCenter

                    onClicked: {
                        root.showSortMenu()
                    }
                }

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 0
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }

                ImageButton {
                    id: menu
                    image.width: 20
                    image.height: image.width
                    source: "assets/menu.png"
                    Layout.minimumWidth: 40
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignHCenter

                    onClicked: {
                        root.showMainMenu()
                    }
                }
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.minimumHeight: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? 1 : 40
            Layout.maximumHeight: Layout.minimumHeight

            gradient: Gradient {
                GradientStop { position: 0.0; color: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? "#c9c9c9" : "#f0f0f0" }
                GradientStop { position: 0.98; color: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? "#c9c9c9" : "#f0f0f0" }
                GradientStop { position: 1.0; color: "#c9c9c9" }
            }

            RowLayout {
                id: quickbar
                visible: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? false : true
                anchors.fill: parent
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: 0

                Rectangle {
                    color: "transparent"
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                }

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: primary.width + 15
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true

                    ImageButton {
                        id: primary
                        height: parent.height / 2
                        spacing: 6
                        checked: false
                        image.width: height * 0.8
                        image.height: image.width
                        text.text: qsTr("내장메모리")
                        text.font.pixelSize: height * 0.6
                        anchors.centerIn: parent
                        source: "assets/internal.png"
                        checkedSource: "assets/internal_checked.png"
                        textColor: "black"
                        checkedTextColor: "#1f88f9"

                        onClicked: {
                            fileListModel.update(fileListModel.primaryPath)

                            root.resetToTop()
                            root.setTitleText(fileListModel.curDirName)

                            checked = true

                            video.checked = false
                            audio.checked = false
                            secondary.checked = false
                        }
                    }
                }

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: secondary.width + 15
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true

                    ImageButton {
                        id: secondary
                        height: parent.height / 2
                        spacing: primary.spacing
                        checked: false
                        image.width: primary.image.width
                        image.height: primary.image.height
                        text.text: qsTr("외장메모리")
                        text.font.pixelSize: primary.text.font.pixelSize
                        anchors.centerIn: parent
                        source: "assets/external.png"
                        checkedSource: "assets/external_checked.png"
                        textColor: "black"
                        checkedTextColor: "#1f88f9"

                        onClicked: {
                            if (fileListModel.existSecondaryPath())
                                root.showSecondaryPathMenu()
                            else
                                root.selectSecondaryPath(0)
                        }
                    }
                }

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: video.width + 15
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true

                    ImageButton {
                        id: video
                        height: parent.height / 2
                        spacing: primary.spacing
                        checked: false
                        image.width: primary.image.width
                        image.height: primary.image.height
                        text.text: qsTr("동영상")
                        text.font.pixelSize: primary.text.font.pixelSize
                        anchors.centerIn: parent
                        source: "assets/video.png"
                        checkedSource: "assets/video_checked.png"
                        textColor: "black"
                        checkedTextColor: "#1f88f9"

                        onClicked: {
                            fileListModel.updateFromDatabase(FileListModel.Video)

                            root.resetToTop()
                            root.setTitleText(fileListModel.curDirName)

                            checked = true

                            audio.checked = false
                            primary.checked = false
                            secondary.checked = false
                        }
                    }
                }

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: audio.width + 15
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true

                    ImageButton {
                        id: audio
                        height: parent.height / 2
                        spacing: primary.spacing
                        checked: false
                        image.width: primary.image.width
                        image.height: primary.image.height
                        text.text: qsTr("음악")
                        text.font.pixelSize: primary.text.font.pixelSize
                        anchors.centerIn: parent
                        source: "assets/audio.png"
                        checkedSource: "assets/audio_checked.png"
                        textColor: "black"
                        checkedTextColor: "#1f88f9"

                        onClicked: {
                            fileListModel.updateFromDatabase(FileListModel.Audio)

                            root.resetToTop()
                            root.setTitleText(fileListModel.curDirName)

                            checked = true

                            video.checked = false
                            primary.checked = false
                            secondary.checked = false
                        }
                    }
                }

                Rectangle {
                    color: "transparent"
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                }
            }
        }

        Rectangle {
            id: storageRoot
            color: "white"
            clip: true
            Layout.fillWidth: true
            Layout.fillHeight: true

            Rectangle {
                anchors.fill: parent
                color: "white"

                ListView {
                    id: storage
                    anchors.fill: parent
                    model: fileListModel

                    delegate: Rectangle {
                        id: itemRoot
                        width: storage.width
                        height: 69
                        color: storageRoot.color

                        Rectangle {
                            x: 10
                            width: parent.width - x
                            height: parent.height
                            color: "transparent"

                            MouseArea {
                                anchors.verticalCenter: parent.verticalCenter
                                width: parent.width
                                height: parent.height - 10

                                RowLayout {
                                    anchors.fill: parent
                                    spacing: 10

                                    Rectangle {
                                        id: thumbnailRoot
                                        color: "transparent"
                                        border.width: model.type === FileListModel.Directory ? 0 : 1
                                        border.color: model.type === FileListModel.Directory ? "transparent" : "#d0d0d0"
                                        Layout.minimumWidth: Math.round(Layout.minimumHeight * 1.33)
                                        Layout.maximumWidth: Layout.minimumWidth
                                        Layout.minimumHeight: parent.height - 8
                                        Layout.maximumHeight: Layout.minimumHeight
                                        Layout.alignment: Qt.AlignHCenter

                                        Image {
                                            id: thumbnail
                                            width: parent.width - thumbnailRoot.border.width * 2
                                            height: parent.height - thumbnailRoot.border.width * 2
                                            anchors.verticalCenter: parent.verticalCenter
                                            anchors.horizontalCenter: parent.horizontalCenter
                                            source: thumbnailImage()
                                            fillMode: Image.PreserveAspectCrop

                                            function thumbnailImage()
                                            {
                                                if (model.type === FileListModel.Directory)
                                                {
                                                    if (model.dirEntryCount === 0)
                                                        return fileListModel.defaultDirectoryIcon
                                                    else
                                                        return fileListModel.openDirectoryIcon
                                                }
                                                else
                                                {
                                                    return model.thumbnail
                                                }
                                            }
                                        }
                                    }

                                    ColumnLayout {
                                        spacing: 1
                                        Layout.fillWidth: true
                                        Layout.fillHeight: true
                                        Layout.alignment: Qt.AlignHCenter

                                        Text {
                                            id: fileName
                                            text: model.name
                                            font.pixelSize: itemRoot.height * 0.22
                                            elide: Text.ElideMiddle
                                            Layout.fillWidth: true
                                        }

                                        Text {
                                            id: fileInfo
                                            color: "red"
                                            text: model.fileInfo.length === 0 ? qsTr("갱신 중...") : model.type === FileListModel.Directory ? qsTr("%1 항목").arg(model.fileInfo) : model.fileInfo
                                            font.pixelSize: fileName.font.pixelSize
                                            elide: Text.ElideMiddle
                                            Layout.fillWidth: true
                                        }

                                        AnyVOD.ProgressBar {
                                            id: curPos
                                            visible: model.type === FileListModel.File && model.curPosition > 0.0 && settingDelegate.getSetting(AnyVODEnums.SK_LAST_PLAY)
                                            from: 0.0
                                            to: model.duration >= 0.0 ? model.duration : 0.0
                                            value: model.curPosition >= 0.0 ? model.curPosition : 0.0
                                            Layout.fillWidth: true
                                            Layout.minimumHeight: 2
                                            Layout.maximumHeight: Layout.minimumHeight
                                        }
                                    }

                                    ImageButton {
                                        id: openItemMenu
                                        image.width: 30
                                        image.height: 30
                                        source: "assets/itemMenu.png"
                                        Layout.minimumWidth: 45
                                        Layout.maximumWidth: Layout.minimumWidth
                                        Layout.minimumHeight: image.height
                                        Layout.maximumHeight: Layout.minimumHeight
                                        Layout.alignment: Qt.AlignHCenter

                                        onClicked: {
                                            if (model.type === FileListModel.Directory)
                                                root.showMediaItemMenu(dirItemMenu, this, model.path)
                                            else if (model.type === FileListModel.File)
                                                root.showMediaItemMenu(fileItemMenu, this, model.path)
                                        }
                                    }
                                }

                                onClicked: {
                                    if (fileTypeDelegate.isMedia(model.path))
                                    {
                                        switch (fileListModel.quickBarType)
                                        {
                                            case FileListModel.InternalType:
                                            case FileListModel.ExternalType:
                                                root.openMedia(model.path)
                                                break
                                            case FileListModel.VideoType:
                                            case FileListModel.AudioType:
                                                root.openPlayListItems(fileListModel.getItems(), model.index)
                                                break
                                        }
                                    }
                                    else
                                    {
                                        titleIcon.visible = false
                                        back.visible = true

                                        root.setTitleText(model.name)

                                        fileListModel.cd(model.path, storage.contentY)
                                    }
                                }

                                onPressed: {
                                    itemRoot.color = "lightgray"
                                    itemRoot.opacity = 0.5
                                }

                                onCanceled: {
                                    itemRoot.color = storageRoot.color
                                    itemRoot.opacity = 1.0
                                }

                                onReleased: {
                                    onCanceled()
                                }
                            }
                        }
                    }

                    remove: Transition {
                         NumberAnimation { property: "opacity"; to: 0.0; duration: 200 }
                    }

                    removeDisplaced: Transition {
                        NumberAnimation { properties: "x,y"; duration: 200 }
                    }
                }

                Scrollbar {
                    id: scrollbar
                    flickable: storage
                }
            }

            Rectangle {
                id: noFiles
                anchors.fill: parent
                visible: false

                ColumnLayout {
                    anchors.fill: parent
                    spacing: 10

                    Rectangle {
                        color: "transparent"
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                    }

                    Image {
                        id: noFileImg
                        source: "assets/nofiles.png"
                        fillMode: Image.PreserveAspectFit
                        Layout.minimumHeight: 100
                        Layout.maximumHeight: Layout.minimumHeight
                        Layout.fillWidth: true
                    }

                    Text {
                        horizontalAlignment: Text.AlignHCenter
                        text: qsTr("파일이 없습니다")
                        Layout.fillWidth: true
                    }

                    Rectangle {
                        color: "transparent"
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                    }
                }
            }
        }

        function updateCurrentFileList()
        {
            switch (fileListModel.quickBarType)
            {
                case FileListModel.InternalType:
                case FileListModel.ExternalType:
                    fileListModel.update()
                    break
                case FileListModel.VideoType:
                    fileListModel.updateFromDatabase(FileListModel.Video)
                    break
                case FileListModel.AudioType:
                    fileListModel.updateFromDatabase(FileListModel.Audio)
                    break
            }
        }

        function selectSecondaryPath(index)
        {
            fileListModel.selectSecondaryPath(index)
            fileListModel.update(fileListModel.secondaryPath)

            root.resetToTop()
            root.setTitleText(fileListModel.curDirName)

            secondary.checked = true

            video.checked = false
            audio.checked = false
            primary.checked = false
        }

        function showSecondaryPathMenu()
        {
            var globalCoord = secondary.mapToItem(null, secondary.x, secondary.y)
            var showX = globalCoord.x
            var showY = globalCoord.y
            var index = fileListModel.getSelectedSecondaryPath()

            if (index < 0)
                index = secondaryPathMenu.noSelected

            secondaryPathMenu.model.clear()

            var i = 0
            var paths = fileListModel.getSecondaryPathNames()

            paths.forEach(function(path) {
                secondaryPathMenu.model.append({ "text": path, "menuID": i++ })
            })

            var itemHeight = 40
            var maxHeight = 4 * itemHeight
            var height = fileListModel.getSecondaryPathCount() * itemHeight

            secondaryPathMenu.menuHeight = Math.min(height, maxHeight)
            secondaryPathMenu.selectedItem = index

            secondaryPathMenu.show(showX, showY)
        }

        function showMainMenu()
        {
            var showX = menu.x - mainMenu.menuWidth + menu.width - 10
            var showY = menu.y + 10

            mainMenu.show(showX, showY)
        }

        function showSortMenu()
        {
            var showX = sort.x - sortMenu.menuWidth + sort.width - 10
            var showY = sort.y + 10

            sortMenu.selectedItem = fileListModel.getSortType()
            sortMenu.show(showX, showY)
        }

        function showMediaItemMenu(menu, parent, path)
        {
            var globalCoord = parent.mapToItem(null, parent.x, parent.y)
            var showX = parent.x - menu.menuWidth + parent.width
            var showY = globalCoord.y - parent.height
            var offset = 10
            var maxHeight = showY + menu.menuHeight + offset

            if (maxHeight >= root.height)
                showY = root.height - (menu.menuHeight + offset)

            menu.path = path
            menu.show(showX, showY)
        }

        function hideMenus()
        {
            mainMenu.hide()
            sortMenu.hide()
            dirItemMenu.hide()
            fileItemMenu.hide()
            secondaryPathMenu.hide()
        }

        function resetToTop()
        {
            titleIcon.visible = true
            back.visible = false
            fileListModel.clearScrollPos()
        }

        function openMedia(filePath)
        {
            closeAllDialogs()

            screenLoader.filePath = filePath
            screenLoader.source = "VideoScreen.qml"
            screenLoader.forceActiveFocus()
        }

        function openMediaWithoutLastPos(filePath, time)
        {
            closeAllDialogs()

            screenLoader.gotoTime = time
            screenLoader.disableLastPos = true
            screenLoader.filePath = filePath
            screenLoader.source = "VideoScreen.qml"
            screenLoader.forceActiveFocus()
        }

        function openPlayList(name)
        {
            closeAllDialogs()

            screenLoader.playListName = name
            screenLoader.source = "VideoScreen.qml"
            screenLoader.forceActiveFocus()
        }

        function openPlayListItems(list, index)
        {
            closeAllDialogs()

            screenLoader.playListItems = list
            screenLoader.playListItemIndex = index
            screenLoader.source = "VideoScreen.qml"
            screenLoader.forceActiveFocus()
        }

        function openRemotePlayListItems(list, index)
        {
            closeAllDialogs()

            screenLoader.remotePlayListItems = list
            screenLoader.remotePlayListItemIndex = index
            screenLoader.source = "VideoScreen.qml"
            screenLoader.forceActiveFocus()
        }

        function closeAllDialogs()
        {
            hideMenus()

            if (messageBoxSecondLoader.sourceComponent !== undefined)
            {
                messageBoxSecondLoader.now = true
                messageBoxSecondLoader.exit()
            }

            if (messageBoxLoader.sourceComponent !== undefined)
            {
                messageBoxLoader.now = true
                messageBoxLoader.exit()
            }

            if (settingDlgSecondLoader.sourceComponent !== undefined)
            {
                settingDlgSecondLoader.now = true
                settingDlgSecondLoader.exit()
            }

            if (settingDlgLoader.sourceComponent !== undefined)
            {
                settingDlgLoader.now = true
                settingDlgLoader.exit()
            }

            if (settingLoader.sourceComponent !== undefined)
            {
                settingLoader.now = true
                settingLoader.exit()
            }
        }

        function setTitleText(text)
        {
            title.text.text = text
            title.restart()
        }

        function checkStartingByURL()
        {
            var startingURL = autoStartDelegate.getStartingURL()

            if (startingURL.length <= 0)
                return

            autoStartDelegate.setStartingURL("")

            if (remoteFileUtilDelegate.isRemoteProtocol(startingURL))
            {
                if (screenLoader.item == null)
                {
                    root.openMedia(startingURL)
                }
                else
                {
                    delayedStartingTimer.nextFilePath = startingURL
                    autoStartDelegate.forceStopMedia()
                }
            }
        }
    }

    Timer {
        id: delayedStartingCheckTimer
        interval: 1000

        property string nextFilePath

        onTriggered: {
            root.checkStartingByURL()
        }
    }

    Timer {
        id: delayedStartingTimer
        interval: 1000

        property string nextFilePath

        onTriggered: {
            root.openMedia(nextFilePath)
            nextFilePath = ""
        }
    }

    Timer {
        id: reloadCurrentPosTimer
        interval: 10

        property string filePath

        onTriggered: {
            fileListModel.updateCurrentPos(filePath)
            filePath = ""
        }
    }

    Loader {
        id: screenLoader
        anchors.fill: parent
        visible: false
        focus: true

        property string filePath
        property string playListName
        property var playListItems: []
        property int playListItemIndex: -1
        property var remotePlayListItems: []
        property int remotePlayListItemIndex: -1
        property bool disableLastPos: false
        property double gotoTime: 0.0
        property FileListModel listModel: fileListModel
        property bool isFailed: false

        function exit(lastPlayFilePath)
        {
            source = ""
            filePath = ""
            playListName = ""
            playListItems = []
            playListItemIndex = -1
            remotePlayListItems = []
            remotePlayListItemIndex = -1
            disableLastPos = false
            gotoTime = 0.0

            root.visible = true
            root.opacity = 1.0

            title.restart()

            if (!isFailed)
                root.forceActiveFocus()

            visible = false
            isFailed = false

            if (settingDelegate.getSetting(AnyVODEnums.SK_LAST_PLAY))
            {
                if (!remoteFileUtilDelegate.isRemoteProtocol(lastPlayFilePath))
                {
                    reloadCurrentPosTimer.filePath = lastPlayFilePath
                    reloadCurrentPosTimer.restart()
                }
            }
        }

        onLoaded: {
            root.visible = false
            root.opacity = 0.0

            visible = true
        }

        Connections {
            ignoreUnknownSignals: true
            target: screenLoader.item

            function onGoBack() {
                if (screenLoader.isFailed)
                {
                    messageBoxLoader.sourceComponent = failedPlayBox
                    messageBoxLoader.forceActiveFocus()
                }

                screenLoader.exit(screenLoader.filePath)

                if (delayedStartingTimer.nextFilePath.length > 0)
                    delayedStartingTimer.restart()
            }

            function onFailedPlay() {
                screenLoader.isFailed = true
            }
        }
    }

    Loader {
        id: settingDlgSecondLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        z: 10
        layer.enabled: true

        property bool now: false

        function exitNow()
        {
            visible = false
            sourceComponent = undefined
        }

        function exit()
        {
            settingLoader.forceActiveFocus()

            opacity = 0.0

            if (now)
            {
                now = false
                exitNow()
            }
            else
            {
                settingDlgSecondHideTimer.restart()
            }
        }

        Timer {
            id: settingDlgSecondHideTimer
            interval: settingDlgSecondOpacityAni.duration

            onTriggered: {
                parent.exitNow()
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: settingDlgSecondOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: settingDlgSecondLoader.item

            function onGoBack() {
                settingDlgSecondLoader.exit()
            }
        }
    }

    Loader {
        id: settingDlgLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        z: 10
        layer.enabled: true

        property bool now: false

        function exitNow()
        {
            visible = false
            sourceComponent = undefined
        }

        function exit()
        {
            settingLoader.forceActiveFocus()

            opacity = 0.0

            if (now)
            {
                now = false
                exitNow()
            }
            else
            {
                settingDlgHideTimer.restart()
            }
        }

        Timer {
            id: settingDlgHideTimer
            interval: settingDlgOpacityAni.duration

            onTriggered: {
                parent.exitNow()
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: settingDlgOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: settingDlgLoader.item

            function onGoBack() {
                settingDlgLoader.exit()
            }
        }
    }

    Loader {
        id: settingLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        layer.enabled: true

        property bool hideParent: true
        property bool now: false

        function exitNow()
        {
            visible = false
            hideParent = true

            sourceComponent = undefined
        }

        function exit()
        {
            if (hideParent)
                root.visible = true

            root.forceActiveFocus()

            opacity = 0.0

            if (now)
            {
                now = false
                exitNow()
            }
            else
            {
                settingHideTimer.restart()
            }
        }

        Timer {
            id: settingHideTimer
            interval: settingOpacityAni.duration

            onTriggered: {
                parent.exitNow()
            }
        }

        Timer {
            id: settingShowTimer
            interval: settingOpacityAni.duration

            onTriggered: {
                if (parent.hideParent)
                    root.visible = false
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: settingOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0

            settingShowTimer.restart()
        }

        Connections {
            ignoreUnknownSignals: true
            target: settingLoader.item

            function onGoBack() {
                settingLoader.exit()
            }
        }
    }

    Loader {
        id: messageBoxLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        layer.enabled: true

        property Item activeItem: null
        property string path
        property bool now: false

        function exitNow()
        {
            visible = false

            if (parent.hideParent !== undefined)
                parent.hideParent = true

            sourceComponent = undefined
        }

        function exit()
        {
            if (activeItem == null)
                root.forceActiveFocus()
            else
                activeItem.forceActiveFocus()

            activeItem = null
            opacity = 0.0

            if (now)
            {
                now = false
                exitNow()
            }
            else
            {
                messageBoxHideTimer.restart()
            }
        }

        Timer {
            id: messageBoxHideTimer
            interval: messageBoxOpacityAni.duration

            onTriggered: {
                parent.exitNow()
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: messageBoxOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: messageBoxLoader.item

            function onGoBack() {
                messageBoxLoader.exit()
            }
        }
    }

    Loader {
        id: messageBoxSecondLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        layer.enabled: true

        property Item activeItem: null
        property bool now: false

        function exitNow()
        {
            visible = false

            if (parent.hideParent !== undefined)
                parent.hideParent = true

            sourceComponent = undefined
        }

        function exit()
        {
            if (activeItem == null)
                messageBoxLoader.forceActiveFocus()
            else
                activeItem.forceActiveFocus()

            activeItem = null
            opacity = 0.0

            if (now)
            {
                now = false
                exitNow()
            }
            else
            {
                messageBoxSecondHideTimer.restart()
            }
        }

        Timer {
            id: messageBoxSecondHideTimer
            interval: messageBoxSecondOpacityAni.duration

            onTriggered: {
                parent.exitNow()
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: messageBoxSecondOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: messageBoxSecondLoader.item

            function onGoBack() {
                messageBoxSecondLoader.exit()
            }
        }
    }

    Timer {
        id: exitTimer

        onTriggered: {
            willExit = false
        }
    }

    Component.onCompleted: {
        statusBarDelegate.show(true)

        var updateURL = updaterDelegate.getUpdateURL()

        if (updateURL.length > 0)
        {
            messageBoxLoader.path = updateURL
            messageBoxLoader.sourceComponent = getUpdate
            messageBoxLoader.forceActiveFocus()
        }

        updaterDelegate.check()
        autoStartDelegate.init()

        delayedStartingCheckTimer.restart()
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            if (fileListModel.curDirName === fileListModel.topSignature)
            {
                if (willExit || !toastDelegate.isSupported())
                {
                    Qt.quit()
                }
                else
                {
                    willExit = true

                    exitTimer.interval = toastDelegate.getLength()
                    exitTimer.restart()

                    toastDelegate.show(qsTr("뒤로 버튼을 한 번 더 누르시면 종료합니다."))
                }
            }
            else
            {
                back.goBack()
            }
        }
    }
}
