﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0

Rectangle {
    id: topRoot
    focus: true
    color: "#88000000"

    property alias windowWidth: root.width
    property alias windowHeight: root.height
    property alias title: header.title
    property FileListModel listModel: null

    signal goBack
    signal accepted(var list, int index)
    signal ignore

    function exit()
    {
        goBack()
        ignore()
    }

    VirtualKeyboardPanel {
        id: keyboard
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: exit()
        onCanceled: exit()
    }

    SearchMediaModel {
        id: searchMediaModel
    }

    Component {
        id: noMedia

        MessageBox {
            windowHeight: 150
            title: qsTr("정보")
            message: qsTr("검색 결과가 없습니다.")
        }
    }

    Rectangle {
        id: root
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 10
        width: parent.width * 0.95
        height: parent.height * 0.9
        radius: 3
        border.color: "gray"

        MouseArea {
            anchors.fill: parent

            ColumnLayout {
                anchors.fill: parent
                spacing: 0

                Header {
                    id: header
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight
                }

                Rectangle {
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight

                    ContextMenuTextInput {
                        id: searchText
                        anchors.fill: parent
                        anchors.topMargin: 10
                        anchors.bottomMargin: 5
                        anchors.leftMargin: 10
                        anchors.rightMargin: 10
                        inputMethodHints: Qt.ImhNone

                        onFinished: {
                            if (!searchMediaModel.search(listModel.primaryPath, text))
                            {
                                messageBoxLoader.sourceComponent = noMedia
                                messageBoxLoader.forceActiveFocus()
                            }
                        }
                    }
                }

                Rectangle {
                    id: searchRoot
                    color: "white"
                    clip: true
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    ListView {
                        id: list
                        anchors.fill: parent
                        model: searchMediaModel

                        delegate: Rectangle {
                            id: itemRoot
                            width: list.width
                            height: 38
                            color: searchRoot.color

                            Rectangle {
                                x: 10
                                width: parent.width - x
                                height: parent.height
                                color: "transparent"

                                MouseArea {
                                    id: mouseArea
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.fill: parent

                                    RowLayout {
                                        anchors.fill: parent
                                        spacing: 10

                                        Rectangle {
                                            id: thumbnailRoot
                                            color: "transparent"
                                            border.width: 1
                                            border.color: "#d0d0d0"
                                            Layout.minimumWidth: 30
                                            Layout.maximumWidth: Layout.minimumWidth
                                            Layout.minimumHeight: 30
                                            Layout.maximumHeight: Layout.minimumHeight
                                            Layout.alignment: Qt.AlignHCenter

                                            Image {
                                                id: thumbnail
                                                width: parent.width - thumbnailRoot.border.width * 2
                                                height: parent.height - thumbnailRoot.border.width * 2
                                                anchors.verticalCenter: parent.verticalCenter
                                                anchors.horizontalCenter: parent.horizontalCenter
                                                source: model.thumbnail
                                                fillMode: Image.PreserveAspectCrop
                                            }
                                        }

                                        Text {
                                            verticalAlignment: Text.AlignVCenter
                                            font.pixelSize: parent.height * 0.38
                                            elide: Text.ElideMiddle
                                            text: model.fileName
                                            Layout.fillWidth: true
                                            Layout.fillHeight: true
                                        }

                                        Rectangle {
                                            color: "transparent"
                                            Layout.minimumWidth: 5
                                            Layout.maximumWidth: Layout.minimumWidth
                                            Layout.fillHeight: true
                                        }
                                    }

                                    onClicked: {
                                        goBack()
                                        accepted(searchMediaModel.getResult(), model.index)
                                    }

                                    onPressed: {
                                        itemRoot.color = "lightgray"
                                        itemRoot.opacity = 0.5
                                    }

                                    onCanceled: {
                                        itemRoot.color = searchRoot.color
                                        itemRoot.opacity = 1.0
                                    }

                                    onReleased: {
                                        onCanceled()
                                    }
                                }
                            }
                        }
                    }

                    Scrollbar {
                        id: scrollbar
                        flickable: list
                    }
                }

                FooterWithButtons {
                    types: AnyVODEnums.BT_CLOSE
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight

                    onCloseClicked: {
                        exit()
                    }
                }
            }
        }
    }

    Loader {
        id: messageBoxLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        layer.enabled: true

        property Item focusParent: null
        property string text

        function exit()
        {
            if (focusParent != null)
            {
                focusParent.forceActiveFocus()
                focusParent = null
            }

            opacity = 0.0
            messageBoxHideTimer.restart()
        }

        Timer {
            id: messageBoxHideTimer
            interval: messageBoxOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: messageBoxOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: messageBoxLoader.item

            function onGoBack() {
                messageBoxLoader.exit()
            }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            exit()
        }
    }
}
