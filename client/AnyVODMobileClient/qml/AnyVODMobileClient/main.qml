﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import com.dcple.anyvod 1.0

Item {
    anchors.fill: parent

    Loader {
        id: mainLoader
        anchors.fill: parent
        focus: true
        asynchronous: true
        opacity: 0.0
        visible: false

        SplashScreenDelegate {
            id: delegate
        }

        onLoaded: {
            delegate.hideSplashScreen()
            splash.visible = false

            visible = true
            opacity = 1.0
        }

        Behavior on opacity {
            NumberAnimation {
                duration: 200
                easing.type: Easing.OutQuad
            }
        }
    }

    Image {
        id: splash
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
        visible: false
    }

    Component.onCompleted: {
        if (Qt.platform.os === "linux")
        {
            splash.source = "assets/splash_icon.png"
            splash.visible = true
        }

        mainLoader.source = "Initial.qml"
    }
}
