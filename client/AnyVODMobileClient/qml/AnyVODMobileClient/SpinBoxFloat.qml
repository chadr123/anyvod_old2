﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12

SpinBox {
    property real valueF: 0
    property real toF: 99
    property real fromF: 0
    property int decimals: 1

    readonly property real realValue: value / div
    readonly property real div: Math.pow(10, decimals)

    function toValue(value)
    {
        return Number.fromLocaleString(Qt.locale(), (value * div).toFixed(decimals))
    }

    onValueFChanged: {
        value = toValue(valueF)
    }

    onToFChanged: {
        to = toValue(toF)
    }

    onFromFChanged: {
        from = toValue(fromF)
    }

    validator: DoubleValidator {
        bottom: Math.min(parent.from, parent.to)
        top:  Math.max(parent.from, parent.to)
    }

    textFromValue: function(value, locale) {
        return Number(value / div).toLocaleString(locale, 'f', decimals)
    }

    valueFromText: function(text, locale) {
        return Number.fromLocaleString(locale, text) * div
    }
}
