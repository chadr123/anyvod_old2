﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0

Rectangle {
    id: topRoot
    focus: true
    color: "#88000000"

    property string title
    property string curDirectory
    property string startDirectory
    property string selectedPath
    property bool onlyDirectory: false
    property bool onlySubtitle: false
    property bool onlyETC: false
    property bool saveMode: false
    property alias fileListObjName: fileListModel.objectName
    property alias windowWidth: root.width
    property alias windowHeight: root.height
    property alias selectedFileName: selectedFileName.text

    signal goBack
    signal accepted
    signal ignore

    function updateTitle()
    {
        header.title = title

        if (curDirectory.length > 0)
            header.title += " - " + curDirectory
    }

    function selectSecondaryPath(index)
    {
        fileListModel.selectSecondaryPath(index)
        fileListModel.update(fileListModel.secondaryPath)

        topRoot.selectedPath = fileListModel.curDirPath

        secondary.checked = true
        primary.checked = false

        secondary.forceActiveFocus()

        topRoot.resetToTop()
        topRoot.updateTitle()
    }

    function showSecondaryPathMenu()
    {
        var globalCoord = secondary.mapToItem(null, secondary.x, secondary.y)
        var showX = globalCoord.x
        var showY = globalCoord.y
        var index = fileListModel.getSelectedSecondaryPath()

        if (index < 0)
            index = secondaryPathMenu.noSelected

        secondaryPathMenu.model.clear()

        var i = 0
        var paths = fileListModel.getSecondaryPathNames()

        paths.forEach(function(path) {
            secondaryPathMenu.model.append({ "text": path, "menuID": i++ })
        })

        var itemHeight = 40
        var maxHeight = 4 * itemHeight
        var height = fileListModel.getSecondaryPathCount() * itemHeight

        secondaryPathMenu.menuHeight = Math.min(height, maxHeight)
        secondaryPathMenu.selectedItem = index

        secondaryPathMenu.show(showX, showY)
    }

    function hideMenus()
    {
        secondaryPathMenu.hide()
    }

    function resetToTop()
    {
        curDirectory = ""
        header.back.visible = false
        fileListModel.clearScrollPos()
    }

    function showBackButton()
    {
        header.back.visible = true
    }

    function updateCurrentPath()
    {
        selectedPath = fileListModel.curDirPath

        if (fileListModel.curDirName === fileListModel.topSignature)
        {
            resetToTop()
        }
        else
        {
            curDirectory = fileListModel.curDirName
            showBackButton()
        }

        updateTitle()
    }

    function finish()
    {
        selectedFileName.finish()
        selectedPath = fileListModel.appendEntry(selectedPath, selectedFileName.text)

        goBack()
        accepted()
    }

    function exit()
    {
        goBack()
        ignore()
    }

    VirtualKeyboardPanel {
        id: keyboard
    }

    FileTypeDelegate {
        id: fileTypeDelegate
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: exit()
        onCanceled: exit()
    }

    Menus {
        id: secondaryPathMenu
        model: ListModel {}

        onMenuItemSelected: {
            var item = model.get(index)

            topRoot.selectSecondaryPath(item.menuID)
        }
    }

    FileListModel {
        id: fileListModel

        Component.onCompleted: {
            onlyDirectory = topRoot.onlyDirectory
            onlySubtitle  = topRoot.onlySubtitle
            onlyETC = topRoot.onlyETC
            startDirectory = topRoot.startDirectory
            header.back.visible = false

            init()
            topRoot.updateCurrentPath()

            switch (fileListModel.quickBarType)
            {
                case FileListModel.InternalType:
                    primary.checked = true
                    break
                case FileListModel.ExternalType:
                    secondary.checked = true
                    break
            }
        }

        onModelReset: {
            noFiles.visible = rowCount() === 0
        }
    }

    Rectangle {
        id: root
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 10
        width: parent.width * 0.9
        height: parent.height * 0.8
        radius: 3
        border.color: "gray"

        MouseArea {
            anchors.fill: parent

            ColumnLayout {
                anchors.fill: parent
                spacing: 0

                HeaderWithBack {
                    id: header
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight

                    onBackClicked: {
                        var contY = fileListModel.cdup()

                        fileListModel.update()

                        var maxHeight = list.contentHeight - list.height

                        if (maxHeight < 0.0)
                            maxHeight = list.height

                        if (contY < 0.0)
                            contY = 0.0
                        else if (contY > maxHeight)
                            contY = maxHeight

                        list.contentY = contY

                        scrollbar.showScrollbar()
                        scrollbar.hideScrollbar()

                        topRoot.hideMenus()
                        topRoot.updateCurrentPath()
                        forceActiveFocus()
                    }
                }

                Rectangle {
                    Layout.fillWidth: true
                    Layout.minimumHeight: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? 1 : 40
                    Layout.maximumHeight: Layout.minimumHeight

                    gradient: Gradient {
                        GradientStop { position: 0.0; color: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? "#c9c9c9" : "#f0f0f0" }
                        GradientStop { position: 0.98; color: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? "#c9c9c9" : "#f0f0f0" }
                        GradientStop { position: 1.0; color: "#c9c9c9" }
                    }

                    RowLayout {
                        id: quickbar
                        visible: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? false : true
                        anchors.fill: parent

                        Rectangle {
                            color: "transparent"
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: primary.width + 15
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true

                            ImageButton {
                                id: primary
                                height: 20
                                spacing: 10
                                checked: false
                                image.width: 20
                                image.height: 20
                                text.text: qsTr("내장 메모리")
                                text.font.pixelSize: height * 0.7
                                anchors.centerIn: parent
                                source: "assets/internal.png"
                                checkedSource: "assets/internal_checked.png"
                                textColor: "black"
                                checkedTextColor: "#1f88f9"

                                onClicked: {
                                    fileListModel.update(fileListModel.primaryPath)

                                    selectedPath = fileListModel.curDirPath

                                    checked = true
                                    secondary.checked = false

                                    forceActiveFocus()

                                    topRoot.resetToTop()
                                    topRoot.updateTitle()
                                }
                            }
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.minimumWidth: secondary.width + 15
                            Layout.maximumWidth: Layout.minimumWidth
                            Layout.fillHeight: true

                            ImageButton {
                                id: secondary
                                height: 20
                                spacing: 10
                                checked: false
                                image.width: 20
                                image.height: 20
                                text.text: qsTr("외장 메모리")
                                text.font.pixelSize: height * 0.7
                                anchors.centerIn: parent
                                source: "assets/external.png"
                                checkedSource: "assets/external_checked.png"
                                textColor: "black"
                                checkedTextColor: "#1f88f9"

                                onClicked: {
                                    if (fileListModel.getSecondaryPathCount() > 1)
                                        topRoot.showSecondaryPathMenu()
                                    else
                                        topRoot.selectSecondaryPath(0)
                               }
                            }
                        }

                        Rectangle {
                            color: "transparent"
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                        }
                    }
                }

                Rectangle {
                    visible: !topRoot.onlyDirectory
                    Layout.fillWidth: true
                    Layout.minimumHeight: 35
                    Layout.maximumHeight: Layout.minimumHeight

                    RowLayout {
                        anchors.fill: parent
                        anchors.topMargin: 5
                        anchors.bottomMargin: 5
                        anchors.leftMargin: 10
                        anchors.rightMargin: 10
                        spacing: 10

                        Text {
                            text: qsTr("파일 이름 :")
                            horizontalAlignment: Text.AlignHCenter
                            Layout.fillHeight: true
                        }

                        ContextMenuTextInput {
                            id: selectedFileName
                            inputFocus: false
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                        }
                    }
                }

                Rectangle {
                    id: storageRoot
                    color: "white"
                    clip: true
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    ListView {
                        id: list
                        anchors.fill: parent
                        model: fileListModel

                        delegate: Rectangle {
                            id: itemRoot
                            width: list.width
                            height: 48
                            color: storageRoot.color

                            Rectangle {
                                x: 10
                                width: parent.width - x
                                height: parent.height
                                color: "transparent"

                                MouseArea {
                                    id: mouseArea
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.fill: parent

                                    RowLayout {
                                        anchors.fill: parent
                                        spacing: 10

                                        Rectangle {
                                            id: thumbnailRoot
                                            color: "transparent"
                                            border.width: model.type === FileListModel.Directory ? 0 : 1
                                            border.color: model.type === FileListModel.Directory ? "transparent" : "#d0d0d0"
                                            Layout.minimumWidth: Math.round(Layout.minimumHeight * 1.33)
                                            Layout.maximumWidth: Layout.minimumWidth
                                            Layout.minimumHeight: parent.height - 12
                                            Layout.maximumHeight: Layout.minimumHeight
                                            Layout.alignment: Qt.AlignHCenter

                                            Image {
                                                id: thumbnail
                                                width: parent.width - thumbnailRoot.border.width * 2
                                                height: parent.height - thumbnailRoot.border.width * 2
                                                anchors.verticalCenter: parent.verticalCenter
                                                anchors.horizontalCenter: parent.horizontalCenter
                                                source: thumbnailImage()
                                                fillMode: Image.PreserveAspectCrop

                                                function thumbnailImage()
                                                {
                                                    if (model.type === FileListModel.Directory)
                                                    {
                                                        if (model.dirEntryCount === 0)
                                                            return fileListModel.defaultDirectoryIcon
                                                        else
                                                            return fileListModel.openDirectoryIcon
                                                    }
                                                    else
                                                    {
                                                        return model.thumbnail
                                                    }
                                                }
                                            }
                                        }

                                        ColumnLayout {
                                            spacing: 1
                                            Layout.fillWidth: true
                                            Layout.fillHeight: true
                                            Layout.alignment: Qt.AlignHCenter

                                            Text {
                                                id: fileName
                                                text: model.fileName
                                                font.pixelSize: itemRoot.height * 0.25
                                                elide: Text.ElideMiddle
                                                Layout.fillWidth: true
                                            }

                                            Text {
                                                id: fileInfo
                                                color: "red"
                                                text: model.fileInfo.length === 0 ? qsTr("갱신 중...") : model.type === FileListModel.Directory ? qsTr("%1 항목").arg(model.fileInfo) : model.fileInfo
                                                font.pixelSize: fileName.font.pixelSize
                                                elide: Text.ElideMiddle
                                                Layout.fillWidth: true
                                            }
                                        }
                                    }

                                    onClicked: {
                                        topRoot.showBackButton()
                                        selectedPath = model.path
                                        forceActiveFocus()

                                        if (fileTypeDelegate.isFile(model.path))
                                        {
                                            selectedFileName.text = model.fileName
                                        }
                                        else
                                        {
                                            curDirectory = model.name
                                            topRoot.updateTitle()
                                            fileListModel.cd(model.path, list.contentY)
                                        }
                                    }

                                    onPressed: {
                                        itemRoot.color = "lightgray"
                                        itemRoot.opacity = 0.5
                                    }

                                    onCanceled: {
                                        itemRoot.color = storageRoot.color
                                        itemRoot.opacity = 1.0
                                    }

                                    onReleased: {
                                        onCanceled()
                                    }
                                }
                            }
                        }
                    }

                    Rectangle {
                        id: noFiles
                        anchors.fill: parent
                        visible: false

                        ColumnLayout {
                            anchors.fill: parent
                            spacing: 10

                            Rectangle {
                                color: "transparent"
                                Layout.fillWidth: true
                                Layout.fillHeight: true
                            }

                            Image {
                                id: noFileImg
                                source: "assets/nofiles.png"
                                fillMode: Image.PreserveAspectFit
                                Layout.minimumHeight: 100
                                Layout.maximumHeight: Layout.minimumHeight
                                Layout.fillWidth: true
                            }

                            Text {
                                horizontalAlignment: Text.AlignHCenter
                                text: qsTr("파일 또는 디렉토리가 없습니다")
                                Layout.fillWidth: true
                            }

                            Rectangle {
                                color: "transparent"
                                Layout.fillWidth: true
                                Layout.fillHeight: true
                            }
                        }
                    }

                    Scrollbar {
                        id: scrollbar
                        flickable: list
                    }
                }

                FooterWithButtons {
                    types: (saveMode ? AnyVODEnums.BT_SAVE : AnyVODEnums.BT_OPEN) | AnyVODEnums.BT_CANCEL
                    Layout.fillWidth: true
                    Layout.minimumHeight: 50
                    Layout.maximumHeight: Layout.minimumHeight

                    onOpenClicked: {
                        finish()
                    }

                    onSaveClicked: {
                        finish()
                    }

                    onCancelClicked: {
                        exit()
                    }
                }
            }
        }
    }

    Component.onCompleted: {
        updateTitle()
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            exit()
        }
    }
}
