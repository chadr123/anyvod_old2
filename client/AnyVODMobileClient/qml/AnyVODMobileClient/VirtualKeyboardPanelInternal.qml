﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.VirtualKeyboard 2.4
import QtQuick.VirtualKeyboard.Settings 2.2
import com.dcple.anyvod 1.0

InputPanel {
    id: inputPanel
    z: 99
    y: parent.height
    anchors.left: parent.left
    anchors.right: parent.right
    states: State {
        name: "visible"
        when: inputPanel.active

        PropertyChanges {
            target: inputPanel
            y: inputPanel.parent.height - inputPanel.height
        }
    }

    transitions: Transition {
        from: ""
        to: "visible"
        reversible: true

        ParallelAnimation {
            NumberAnimation {
                properties: "y"
                duration: 250
                easing.type: Easing.InOutQuad
            }
        }
    }

    function setLocale(locale)
    {
        var activeLang = []

        activeLang.push(locale)

        if (locale !== "en_US")
            activeLang.push("en_US")

        VirtualKeyboardSettings.activeLocales = activeLang
        VirtualKeyboardSettings.locale = locale
    }

    AnyVODGlobal {
        id: global
    }

    Connections {
        target: VirtualKeyboardSettings

        function onAvailableLocalesChanged() {
            var list = VirtualKeyboardSettings.availableLocales
            var lang = global.getCurrentLanguage()
            var keyboardLang = global.getCurrentKeyboardLanguage()

            if (keyboardLang.length > 0)
            {
                inputPanel.setLocale(keyboardLang)
                return
            }

            if (lang.length <= 0)
                lang = "ko"

            for (var i = 0; i < list.length; i++)
            {
                var locale = list[i]

                if (locale.indexOf(lang) === 0)
                {
                    inputPanel.setLocale(locale)
                    break
                }
            }
        }
    }
}
