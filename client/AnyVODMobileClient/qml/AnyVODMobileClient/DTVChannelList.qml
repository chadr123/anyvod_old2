﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import com.dcple.anyvod 1.0

Item {
    id: topRoot
    anchors.fill: parent
    focus: true

    property string dtvPath
    property bool playDTV: false
    property alias title: header.title

    signal goBack

    function exit()
    {
        goBack()
    }

    function updateNoItem()
    {
        noItems.visible = dtvListModel.rowCount() === 0
    }

    DTVDelegate {
        id: delegate

        DTVListModel {
            id: dtvListModel
            objectName: "dtvListModel"

            onModelReset: {
                updateNoItem()
            }
        }

        Component.onCompleted: {
            init()
        }
    }

    ColumnLayout {
        id: root
        anchors.fill: parent
        spacing: 0

        HeaderWithBack {
            id: header
            Layout.fillWidth: true
            Layout.minimumHeight: 50
            Layout.maximumHeight: Layout.minimumHeight

            onBackClicked: {
                topRoot.exit()
            }
        }

        Rectangle {
            id: listRoot
            color: "white"
            clip: true
            Layout.fillWidth: true
            Layout.fillHeight: true

            Rectangle {
                anchors.fill: parent
                color: "#f8f8f8"

                ListView {
                    id: list
                    anchors.fill: parent
                    model: dtvListModel

                    delegate: Rectangle {
                        id: itemRoot
                        width: list.width
                        height: 60

                        MouseArea {
                            anchors.verticalCenter: parent.verticalCenter
                            width: parent.width
                            height: parent.height - 10

                            Rectangle {
                                anchors.fill: parent
                                anchors.leftMargin: 20
                                anchors.rightMargin: 20
                                color: "transparent"

                                ColumnLayout {
                                    anchors.fill: parent
                                    spacing: 0

                                    Text {
                                        text: qsTr("채널 ") + model.channel + qsTr(", 신호 감도 ") + model.signalStrength + "%"
                                    }

                                    Text {
                                        text: model.name === "" ? qsTr("이름 없음") : model.name
                                        color: "red"
                                    }
                                }
                            }

                            onClicked: {
                                dtvPath = delegate.getPath(model.index)
                                playDTV = true
                                exit()
                            }

                            onPressed: {
                                itemRoot.color = "lightgray"
                                itemRoot.opacity = 0.5
                            }

                            onCanceled: {
                                itemRoot.color = listRoot.color
                                itemRoot.opacity = 1.0
                            }

                            onReleased: {
                                onCanceled()
                            }
                        }

                        Rectangle {
                            anchors.bottom: parent.bottom
                            width: parent.width
                            height: 1
                            color: model.index === list.count - 1 ? "white" : "lightgray"
                        }
                    }
                }

                Text {
                    id: noItems
                    anchors.fill: parent
                    visible: false
                    text: qsTr("채널 목록이 없습니다.")
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }

                Scrollbar {
                    id: scrollbar
                    flickable: list
                }
            }
        }
    }

    Loader {
        id: comboLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        z: 10
        layer.enabled: true

        function exit()
        {
            root.forceActiveFocus()

            opacity = 0.0
            comboHideTimer.restart()
        }

        Timer {
            id: comboHideTimer
            interval: comboOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: comboOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: comboLoader.item

            function onGoBack() {
                comboLoader.exit()
            }
        }
    }

    Component.onCompleted: {
        updateNoItem()
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            topRoot.exit()
        }
    }
}
