﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0

Rectangle {
    id: topRoot
    focus: true
    color: "transparent"

    property string desc
    property string link
    property alias title: title.text
    property alias windowX: root.x
    property alias windowY: root.y
    property alias windowWidth: root.width
    property alias windowHeight: root.height

    signal goBack

    function getDesc()
    {
        return desc.replace(/\n/gi, "<br>")
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: goBack()
        onCanceled: goBack()
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    Rectangle {
        id: root
        color: "#7f000000"
        radius: 3

        ColumnLayout {
            anchors.fill: parent
            spacing: 5

            Rectangle {
                color: "transparent"
                Layout.fillWidth: true
                Layout.minimumHeight: 0
                Layout.maximumHeight: Layout.minimumHeight
            }

            Text {
                id: title
                width: parent.width
                font.pixelSize: 12
                verticalAlignment: Text.AlignVCenter
                color: "white"
                Layout.leftMargin: 10
                Layout.rightMargin: 10
            }

            RowLayout {
                spacing: 5
                Layout.fillWidth: true
                Layout.fillHeight: true

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 0
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }

                Text {
                    id: link
                    focus: true
                    text: "<a style='color: lightblue;text-decoration: none;' href='" + topRoot.link + "'>" + getDesc() + "</a>"
                    textFormat: Text.RichText
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: 13
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    onLinkActivated: {
                        Qt.openUrlExternally(topRoot.link)
                        goBack()
                    }
                }

                Rectangle {
                    color: "transparent"
                    Layout.minimumWidth: 0
                    Layout.maximumWidth: Layout.minimumWidth
                    Layout.fillHeight: true
                }
            }

            Rectangle {
                color: "transparent"
                Layout.fillWidth: true
                Layout.minimumHeight: 0
                Layout.maximumHeight: Layout.minimumHeight
            }
        }
    }
}
