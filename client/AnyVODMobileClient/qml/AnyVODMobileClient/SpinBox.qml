﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

import QtQuick 2.12
import QtQuick.Controls 2.12

SpinBox {
    property int fontSize: 12
    property int indicatorWidth: 30

    value: 0
    from: 99
    to: 0
    editable: true

    contentItem: TextInput {
        text: parent.textFromValue(parent.value)
        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment: Qt.AlignVCenter
        validator: parent.validator
        inputMethodHints: Qt.ImhFormattedNumbersOnly
        font.pixelSize: parent.fontSize
    }

    background: Rectangle {
        color: "transparent"
        border.color: "gray"
    }

    up.indicator: Rectangle {
        x: parent.mirrored ? 0 : parent.width - width
        height: parent.height
        width: parent.indicatorWidth
        color: parent.up.pressed ? "#e4e4e4" : "#f6f6f6"
        border.color: "gray"

        Text {
            text: "+"
            font.pixelSize: parent.parent.font.pixelSize * 2
            anchors.fill: parent
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }

    down.indicator: Rectangle {
        x: parent.mirrored ? parent.width - width : 0
        height: parent.height
        width: parent.indicatorWidth
        color: parent.down.pressed ? "#e4e4e4" : "#f6f6f6"
        border.color: "gray"

        Text {
            text: "-"
            font.pixelSize: parent.parent.font.pixelSize * 2
            anchors.fill: parent
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }
}
