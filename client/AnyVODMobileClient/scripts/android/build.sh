#!/bin/bash

. ../../../../installer/common/functions.sh

./make.sh
check

./deploy.sh $1 $2
check

exit 0
