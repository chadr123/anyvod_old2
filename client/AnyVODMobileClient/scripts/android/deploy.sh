#!/bin/bash

. ../../../../installer/common/functions.sh

version=`cat ../../../AnyVODClient/scripts/linux/version`
qt_path=`../../../AnyVODClient/scripts/linux/qt_path.sh`
password=$1
export PATH=$HOME/$qt_path/$version/android/bin:$PATH

. ./toolchain.sh

if [ ! -d ../../../../package ]; then
  mkdir ../../../../package
  check
fi

./replaceabi.sh replace $2
check

androiddeployqt --input ../../../AnyVODMobileClient-build-android/android-AnyVODMobileClient-deployment-settings.json --output ../../../AnyVODMobileClient-build-android/android-build --deployment bundled --android-platform android-29 --jdk $JDK --gradle --sign $HOME/android_release.keystore anyvod --storepass $password --keypass $password
ret=$?
if [ $ret -ne 0 ]; then
  ./replaceabi.sh unreplace
  exit $ret
fi

./replaceabi.sh unreplace
check

exit 0
