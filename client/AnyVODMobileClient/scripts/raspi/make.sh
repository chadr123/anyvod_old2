#!/bin/bash

. ../../../../installer/common/functions.sh

export PATH=/usr/local/qt5/bin:$PATH

./premake.sh
check

cd ../../../
check

if [ -e AnyVODMobileClient-build-raspi ]; then
  rm -rf AnyVODMobileClient-build-raspi
  check
fi

mkdir AnyVODMobileClient-build-raspi
check

cd AnyVODMobileClient-build-raspi
check

make distclean -w

qmake ../AnyVODMobileClient/AnyVODMobileClient.pro -r -spec devices/linux-rasp-pi3-vc4-g++ CONFIG+=qtquickcompiler
check

make -w -j4
check

cd ../AnyVODMobileClient/scripts/android
check

exit 0
