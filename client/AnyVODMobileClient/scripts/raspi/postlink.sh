#!/bin/bash

. ../../installer/common/functions.sh

export PATH=/usr/local/qt5/bin:$PATH

lrelease ../AnyVODMobileClient/AnyVODMobileClient.pro
check

ldconfig -n ../AnyVODMobileClient/libs/raspi
check

exit 0
