/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

package com.dcple.anyvod;

import android.app.Service;
import android.app.Notification;

import android.os.IBinder;
import android.os.Binder;

import android.content.Intent;

import android.util.Log;

import java.lang.Thread;
import java.lang.Runnable;

import java.util.Date;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.NotificationCompat;

public class PreventKillService extends Service
{
    private static final int NOTIFICATION_ID = 2;
    private static final String CHANNEL_ID = BuildConfig.APPLICATION_ID + "playback_channel" + NOTIFICATION_ID;

    private final IBinder m_binder = new LocalBinder();
    private boolean m_playOrPause = false;
    private Thread m_task = null;

    public class LocalBinder extends Binder
    {
        PreventKillService getService()
        {
            return PreventKillService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return this.m_binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        super.onStartCommand(intent, flags, startId);

        Utils.createChannel(this, CHANNEL_ID, Notification.VISIBILITY_SECRET);
        this.startForeground(NOTIFICATION_ID, this.createNotification());

        this.m_task = new Thread(new Runnable()
        {
            public void run()
            {
                PreventKillService service = PreventKillService.this;

                while (!Thread.currentThread().isInterrupted())
                {
                    NotificationManagerCompat.from(service).notify(NOTIFICATION_ID, service.createNotification());

                    try
                    {
                        Thread.sleep(1000);
                    }
                    catch (InterruptedException e)
                    {
                        Thread.currentThread().interrupt();
                    }
                }

                NotificationManagerCompat.from(service).cancel(NOTIFICATION_ID);
            }
        });

        this.m_task.start();

        return START_STICKY;
    }

    @Override
    public void onDestroy()
    {
        this.stopUpdate();
        this.stopForeground(true);

        super.onDestroy();
    }

    private Notification createNotification()
    {
        int smallIcon;
        String text = this.m_playOrPause ? "Playing" : "Paused";

        if (this.m_playOrPause)
            smallIcon = R.drawable.resume_white;
        else
            smallIcon = R.drawable.pause_white;

        long current = NativeFunctions.getCurrentPosition();
        long duration = NativeFunctions.getDuration();

        if (duration >= 0)
        {
            String timeFormat;

            if (duration >= 360000)
                timeFormat = "HH:mm:ss";
            else
                timeFormat = "mm:ss";

            DateFormat formatter = new SimpleDateFormat(timeFormat);
            String currentString = formatter.format(new Date(current));
            String durString = formatter.format(new Date(duration));

            text += " - " + currentString + " / " + durString;
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);

        builder.setVisibility(NotificationCompat.VISIBILITY_SECRET);
        builder.setAutoCancel(false);
        builder.setOngoing(true);
        builder.setSmallIcon(smallIcon);
        builder.setContentText(text);

        return builder.build();
    }

    public void updateNotification(boolean playOrPause)
    {
        this.m_playOrPause = playOrPause;
    }

    public void stopUpdate()
    {
        if (this.m_task != null)
        {
            try
            {
                this.m_task.interrupt();
                this.m_task.join();
                this.m_task = null;
            }
            catch (InterruptedException e) {}
        }
    }
}
