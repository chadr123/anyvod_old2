/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

package com.dcple.anyvod;

import android.content.Context;
import android.content.Intent;

import android.view.KeyEvent;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

public class HeadsetHookHandler
{
    private Context m_context = null;
    private int m_hookKeyCount = 0;
    private boolean m_hookKeyPressed = false;
    private boolean m_hookKeyPressedProcessing = false;
    private Timer m_timer = null;

    private TimerTask getTimerHandler()
    {
        TimerTask task = new TimerTask()
        {
            @Override
            public void run()
            {
                HeadsetHookHandler handler = HeadsetHookHandler.this;
                Intent i = null;

                switch (handler.m_hookKeyCount)
                {
                    case 1:
                    {
                        if (handler.m_hookKeyPressed)
                        {
                            i = new Intent(AnyVODActivity.ACTION_REMOTE_FORWARD);

                            handler.m_hookKeyPressedProcessing = true;
                            handler.restartTimer();
                        }
                        else
                        {
                            i = new Intent(AnyVODActivity.ACTION_REMOTE_TOGGLE);
                            handler.m_hookKeyCount = 0;
                        }

                        break;
                    }
                    case 2:
                    {
                        if (handler.m_hookKeyPressed)
                        {
                            i = new Intent(AnyVODActivity.ACTION_REMOTE_REWIND);

                            handler.m_hookKeyPressedProcessing = true;
                            handler.restartTimer();
                        }
                        else
                        {
                            i = new Intent(AnyVODActivity.ACTION_REMOTE_NEXT);
                            handler.m_hookKeyCount = 0;
                        }

                        break;
                    }
                    case 3:
                    {
                        i = new Intent(AnyVODActivity.ACTION_REMOTE_PREVIOUS);
                        handler.m_hookKeyCount = 0;

                        break;
                    }
                    default:
                    {
                        break;
                    }
                }

                if (i != null)
                    handler.m_context.sendBroadcast(i);
            }
        };

        return task;
    }

    private void restartTimer()
    {
        if (this.m_timer != null)
            this.m_timer.cancel();

        this.m_timer = new Timer();
        this.m_timer.schedule(this.getTimerHandler(), 500);
    }

    public void processKey(Context context, int action)
    {
        this.m_context = context;

        if (action == KeyEvent.ACTION_DOWN)
        {
            this.m_hookKeyPressed = true;
        }
        else if (action == KeyEvent.ACTION_UP)
        {
            this.m_hookKeyPressed = false;

            if (this.m_hookKeyPressedProcessing)
            {
                this.m_hookKeyCount = 0;
                this.m_hookKeyPressedProcessing = false;
            }
            else
            {
                this.m_hookKeyCount++;
                this.restartTimer();
            }
        }
    }
}
