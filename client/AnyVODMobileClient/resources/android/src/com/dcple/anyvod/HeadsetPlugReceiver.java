/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

package com.dcple.anyvod;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class HeadsetPlugReceiver extends BroadcastReceiver
{
    private AnyVODActivity m_activity = null;
    private int m_state = 0;

    public HeadsetPlugReceiver(AnyVODActivity activity)
    {
        this.m_activity = activity;
    }

    public void resetState()
    {
        this.m_state = 0;
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {
        int state = intent.getIntExtra("state", 0);

        if (state == this.m_state)
            return;

        this.m_state = state;

        boolean isPlugged = this.m_state == 1 ? true : false;

        if (isPlugged)
        {
            NativeFunctions.onResumeMediaOwnState();
        }
        else
        {
            if (!this.m_activity.isBluetoothHeadsetConnected())
                NativeFunctions.onPauseMediaOwnState();
        }
    }
}
