#!/bin/bash

. ../../../../installer/common/functions.sh

version=`cat ../linux/version`
qt_path=`../linux/qt_path.sh`
DEPLOY_PATH="../../../AnyVODClient-build-desktop/AnyVODClient.app/Contents/MacOS"

if [ ! -d ../../../../package ]; then
  mkdir ../../../../package
  check
fi

if [ -d ${DEPLOY_PATH}/licenses ]; then
  rm -rf ${DEPLOY_PATH}/licenses
  check
fi

mkdir ${DEPLOY_PATH}/licenses
check

cp ../../../../licenses/* ${DEPLOY_PATH}/licenses
check

if [ -d ${DEPLOY_PATH}/notes ]; then
  rm -rf ${DEPLOY_PATH}/notes
  check
fi

mkdir ${DEPLOY_PATH}/notes
check

cp ../../../../notes/* ${DEPLOY_PATH}/notes
check

cp -a ../../libs/mac/*.dylib ${DEPLOY_PATH}
check

cp ../../equalizer_template.ini ${DEPLOY_PATH}/equalizer.ini
check

cp ../../settings_template.ini ${DEPLOY_PATH}/settings.ini
check

if [ -d ${DEPLOY_PATH}/fonts ]; then
  rm -rf ${DEPLOY_PATH}/fonts
  check
fi

mkdir ${DEPLOY_PATH}/fonts
check

cp ../../fonts/* ${DEPLOY_PATH}/fonts
check

if [ -d ${DEPLOY_PATH}/skins ]; then
  rm -rf ${DEPLOY_PATH}/skins
  check
fi

mkdir ${DEPLOY_PATH}/skins
check

cp ../../skins/* ${DEPLOY_PATH}/skins
check

if [ -d ${DEPLOY_PATH}/shaders ]; then
  rm -rf ${DEPLOY_PATH}/shaders
  check
fi

mkdir ${DEPLOY_PATH}/shaders
check

cp ../../shaders/* ${DEPLOY_PATH}/shaders
check

if [ -d ${DEPLOY_PATH}/languages ]; then
  rm -rf ${DEPLOY_PATH}/languages
  check
fi

mkdir ${DEPLOY_PATH}/languages
check

cp $HOME/$qt_path/$version/clang_64/translations/qt_zh_??.qm ${DEPLOY_PATH}/languages
check

cp $HOME/$qt_path/$version/clang_64/translations/qt_??.qm ${DEPLOY_PATH}/languages
check

cp $HOME/$qt_path/$version/clang_64/translations/qtbase_??.qm ${DEPLOY_PATH}/languages
check

cp ../../languages/*.qm ${DEPLOY_PATH}/languages
check

if [ -d ../../../../package/client ]; then
  rm -rf ../../../../package/client
  check
fi

mkdir ../../../../package/client
check

cp -a ${DEPLOY_PATH}/../../../AnyVODClient.app ../../../../package/client
check

rm -rf ../../../AnyVODClient-build-desktop
check

rm -rf ../../../build-AnyVODClient-Desktop
check

exit 0
