#!/bin/bash

qt_path_online_installer=Qt
qt_path_offline_installer=Qt5.15.2

qt_path=$qt_path_online_installer

if [ -d $HOME/$qt_path_online_installer ]; then
  qt_path=$qt_path_online_installer
fi

if [ -d $HOME/$qt_path_offline_installer ]; then
  qt_path=$qt_path_offline_installer
fi

echo $qt_path
