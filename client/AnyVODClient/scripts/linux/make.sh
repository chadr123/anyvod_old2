#!/bin/bash

. ../../../../installer/common/functions.sh

version=`cat version`
qt_path=`./qt_path.sh`
export PATH=$HOME/$qt_path/$version/gcc_64/bin:$PATH

cd ../../../
check

if [ -e AnyVODClient-build-desktop ]; then
  rm -rf AnyVODClient-build-desktop
  check
fi

mkdir AnyVODClient-build-desktop
check

cd AnyVODClient-build-desktop
check

make distclean -w

qmake ../AnyVODClient/AnyVODClient.pro -r -spec linux-g++ CONFIG+=release
check

make -w -j8
check

cd ../AnyVODClient/scripts/linux
check

exit 0
