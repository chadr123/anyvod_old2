﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SettingsFactory.h"
#include "Settings.h"
#include "core/Common.h"

#include <QSettings>
#include <QStandardPaths>
#include <QDir>

SettingsFactory::SettingsFactory()
{

}

QSettings& SettingsFactory::getInstance(SettingsFactory::SettingType type)
{
    switch (type)
    {
        case ST_GLOBAL:
        {
#if defined Q_OS_MOBILE
            static QSettings instance(SettingsFactory::getSettingPath() + SETTING_FILE_NAME, QSettings::IniFormat);
#else
            static QSettings instance(QSettings::IniFormat, QSettings::UserScope, ORG_NAME, APP_NAME);
#endif

            return instance;
        }
        case ST_LAST_PLAY:
        {
            static QSettings instance(SettingsFactory::getSettingPath() + SETTING_LAST_PLAY_FILE_NAME, QSettings::IniFormat);

            return instance;
        }
        default:
        {
            static QSettings instance;

            return instance;
        }
    }
}

QString SettingsFactory::getSettingPath()
{
#if defined Q_OS_MOBILE
    QString path = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + QDir::separator();

# if defined Q_OS_RASPBERRY_PI
    path += QString("AnyVODMobile") + QDir::separator();
# endif

    return path;
#else
    return QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + QDir::separator();
#endif
}
