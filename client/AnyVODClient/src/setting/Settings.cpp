﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "Settings.h"

#include <QString>

const QString ENV_ITEM_DELIMITER = "=";
const QString ENV_FILENAME = "settings.ini";
const QString ENV_REMOTE_ADDR_NAME = "address";
const QString ENV_FONT_PATH_NAME = "font";
const QString ENV_REMOTE_PORT_NAME = "port";
const QString ENV_REMOTE_STREAM_PORT_NAME = "stream_port";
const QString ENV_FONT_SIZE_NAME = "font_size";
const QString ENV_FONT_SUBTITLE_OUTLINE_SIZE_NAME = "subtitle_outline_size";
const QString ENV_SKIN_NAME = "skin";
const QString ENV_SHADER_NAME = "shader";
const QString SETTING_FILE_NAME = "env_settings.ini";
const QString SETTING_LAST_PLAY_FILE_NAME = "lastplay.ini";
const QString SETTING_EQUALIZER_FILE_NAME = "equalizer.ini";
const QString SETTING_MAINWINDOW_GEOMETRY = "main_geometry";
const QString SETTING_MAINWINDOW_MIN_SIZE = "main_minsize";
const QString SETTING_MAINWINDOW_MAX_SIZE = "main_maxsize";
const QString SETTING_MAINWINDOW_MOSTTOP = "main_mosttop";
const QString SETTING_MAINWINDOW_OPAQUE = "main_opaque";
const QString SETTING_MAINWINDOW_LAST_DIR = "screen_lastdir";
const QString SETTING_SCREEN_VISIBLE = "screen_visible";
const QString SETTING_SCREEN_CAPTURE_DIR = "screen_capturedir";
const QString SETTING_SCREEN_CAPTURE_EXT = "screen_captureext";
const QString SETTING_SPECTRUM_TYPE = "spectrum_type";
const QString SETTING_VOLUME = "volume";
const QString SETTING_PLAYLIST = "playlist";
const QString SETTING_PLAYLIST_SHOW = "playlist_show";
const QString SETTING_PLAYLIST_GEOMETRY = "playlist_geometry";
const QString SETTING_CURRENT_PLAYING = "current_playing";
const QString SETTING_SHOW_SUBTITLE = "show_subtitle";
const QString SETTING_HALIGN_SUBTITLE = "halign_subtitle";
const QString SETTING_USE_NORMALIZER = "use_normalizer";
const QString SETTING_USE_EQUALIZER = "use_equalizer";
const QString SETTING_EQUALIZER_PREAMP = "equalizer_preamp";
const QString SETTING_EQUALIZER_GAINS = "equalizer_gains";
const QString SETTING_DEINTERLACER_METHOD = "deinterlacer_method";
const QString SETTING_DEINTERLACER_ALGORITHM = "deinterlacer_algorithm";
const QString SETTING_SEEK_KEYFRAME = "seek_keyframe";
const QString SETTING_SKIP_OPENING = "skip_opening";
const QString SETTING_SKIP_ENDING = "skip_ending";
const QString SETTING_SKIP_USING = "skip_using";
const QString SETTING_SKIP_RANGE = "skip_range";
const QString SETTING_SHORTCUTS = "shortcuts";
const QString SETTING_FILE_ASSOCIATION_VIDEO = "file_association_video";
const QString SETTING_FILE_ASSOCIATION_AUDIO = "file_association_audio";
const QString SETTING_FILE_ASSOCIATION_SUBTITLE = "file_association_subtitle";
const QString SETTING_FILE_ASSOCIATION_PLAYLIST = "file_association_playlist";
const QString SETTING_ENABLE_SEARCH_SUBTITLE = "enable_search_subtitle";
const QString SETTING_ENABLE_SEARCH_LYRICS = "enable_search_lyrics";
const QString SETTING_VALIGN_SUBTITLE = "valign_subtitle";
const QString SETTING_SUBTITLE_SIZE = "subtitle_size";
const QString SETTING_USER_ASPECT_RATIO = "user_aspect_ratio";
const QString SETTING_USE_HW_DECODER = "use_hw_decoder";
const QString SETTING_USE_PBO = "use_hw_pbo";
const QString SETTING_USE_VSYNC = "use_vsync";
const QString SETTING_LANGUAGE = "language";
const QString SETTING_CLEAR_PLAYLIST = "clear_playlist";
const QString SETTING_SHOW_ALBUM_JACKET = "show_album_jacket";
const QString SETTING_ENABLE_LAST_PLAY = "enable_last_play";
const QString SETTING_TEXT_ENCODING = "text_encoding";
const QString SETTING_ADDRESS = "address";
const QString SETTING_COMMAND_PORT = "command_port";
const QString SETTING_STREAM_PORT = "stream_port";
const QString SETTING_USE_FRAME_DROP = "use_frame_drop";
const QString SETTING_SUBTITLE_DIRECTORY = "subtitle_directory";
const QString SETTING_PRIOR_SUBTITLE_DIRECTORY = "prior_subtitle_directory";
const QString SETTING_DTV_SCANNED_CHANNELS = "dtv_scanned_channels";
const QString SETTING_DTV_CURRENT_COUNTRY = "dtv_current_country";
const QString SETTING_DTV_CURRENT_TYPE = "dtv_current_type";
const QString SETTING_SEARCH_SUBTITLE_COMPLEX = "search_subtitle_complex";
const QString SETTING_SPDIF_DEVICE = "setting_spdif_device";
const QString SETTING_USE_BUFFERING_MODE = "setting_use_buffering_mode";
const QString SETTING_FIRST_LOADING = "first_loading";
const QString SETTING_USE_SUBTITLE_CACHE_MODE = "use_subtitle_cache_mode";
const QString SETTING_FONT = "font";
const QString SETTING_CURRENT_QUICKBAR_ITEM = "current_quickbar_item";
const QString SETTING_DELETE_MEDIA_WITH_SUBTITLE = "delete_media_with_subtitle";
const QString SETTING_USE_LOW_QUALITY_MODE = "use_low_quality_mode";
const QString SETTING_AUDIO_DEVICE = "audio_device";
const QString SETTING_SPDIF_ENCODING = "spdif_encoding";
const QString SETTING_SPDIF_SAMPLE_RATE = "spdif_sample_rate";
const QString SETTING_USE_SPDIF = "use_spdif";
const QString SETTING_VR_USE_DISTORTION = "vr_use_distortion";
const QString SETTING_VR_BARREL_DISTORTION_COEFFICIENTS = "vr_barrel_distortion_coefficients";
const QString SETTING_VR_PINCUSHION_DISTORTION_COEFFICIENTS = "vr_pincushion_distortion_coefficients";
const QString SETTING_VR_DISTORTION_LENS_CENTER = "vr_distortion_lens_center";
const QString SETTING_USE_GPU_CONVERT = "use_gpu_convert";
const QString SETTING_KEYBOARD_LANGUAGE = "keyboard_language";
const QString SETTING_AUTO_SAVE_SEARCH_LYRICS = "auto_save_search_lyrics";
const QString SETTING_UPDATE_FILENAME = "update_filename";
const QString SETTING_CHECK_UPDATE_GAP = "check_update_gap";
const QString SETTING_CURRENT_UPDATE_DATE = "current_update_date";
const QString SETTING_CHECK_UPDATE_GAP_FIX = "check_update_gap_fix";
const QString SETTING_PROXY_INFO = "proxy_info";
const QString SETTING_BLUETOOTH_AUDIO_SYNC = "bluetooth_audio_sync";
const QString SETTING_USE_HDR = "use_hdr";
const QString SETTING_HW_DECODER = "hw_decoder";
const QString SETTING_USE_AUTO_COLOR_CONVERSION = "use_auto_color_conversion";
const QString SETTING_USE_IVTC = "use_ivtc";
const QString SETTING_PLAYING_METHOD = "main_playing_method";
const QString SETTING_RADIO_SCANNED_CHANNELS = "radio_scanned_channels";
const QString SETTING_RADIO_CURRENT_COUNTRY = "radio_current_country";
const QString SETTING_RADIO_CURRENT_TYPE = "radio_current_type";
