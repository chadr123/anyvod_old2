﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "MainWindowSettingSaver.h"
#include "Settings.h"
#include "core/Common.h"
#include "ui/MainWindow.h"
#include "ui/Spectrum.h"
#include "ui/UserAspectRatios.h"
#include "language/GlobalLanguage.h"
#include "parsers/subtitle/GlobalSubtitleCodecName.h"

#include <QSettings>

MainWindowSettingSaver::MainWindowSettingSaver(MainWindowInterface *mainWindow) :
    MainWindowSettingBase(mainWindow)
{

}

void MainWindowSettingSaver::save() const
{
    this->m_settings.setValue(SETTING_MAINWINDOW_MIN_SIZE, this->m_mainWindow->minimumSize());
    this->m_settings.setValue(SETTING_MAINWINDOW_MAX_SIZE, this->m_mainWindow->maximumSize());
    this->m_settings.setValue(SETTING_MAINWINDOW_GEOMETRY, this->m_mainWindow->geometry());
    this->m_settings.setValue(SETTING_MAINWINDOW_MOSTTOP, IS_BIT_SET(this->m_mainWindow->windowFlags(), Qt::WindowStaysOnTopHint));
    this->m_settings.setValue(SETTING_MAINWINDOW_OPAQUE, this->m_mainWindow->windowOpacity());
    this->m_settings.setValue(SETTING_MAINWINDOW_LAST_DIR, this->m_mainWindow->getLastSelectedDirectory());
    this->m_settings.setValue(SETTING_SPECTRUM_TYPE, (int)this->m_mainWindow->getSpectrum()->getType());

    UserAspectRatios::Item item = this->m_mainWindow->getUserAspectRatios().getUserRatio();
    QSizeF aspectRatio(item.width, item.height);
    this->m_settings.setValue(SETTING_USER_ASPECT_RATIO, aspectRatio);

    this->m_settings.setValue(SETTING_LANGUAGE, GlobalLanguage::getInstance().getLanguage());
    this->m_settings.setValue(SETTING_TEXT_ENCODING, GlobalSubtitleCodecName::getInstance().getCodecName());

    RemoteServerInformation info = this->m_mainWindow->getRemoteServerInformation();

    this->m_settings.setValue(SETTING_ADDRESS, info.address);
    this->m_settings.setValue(SETTING_COMMAND_PORT, info.commandPort);
    this->m_settings.setValue(SETTING_STREAM_PORT, info.streamPort);
}
