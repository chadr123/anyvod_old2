﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SettingsSynchronizer.h"

#ifndef Q_OS_MOBILE
# include "ui/MainWindowInterface.h"
# include "ui/Screen.h"
# include "ui/ShortcutKey.h"
# include "factories/MainWindowFactory.h"
# include "media/MediaPlayer.h"
# include "setting/MainWindowSettingSaver.h"
# include "setting/ScreenSettingSaver.h"
# include "setting/MediaPlayerSettingSaver.h"
# include "setting/PlayListSettingSaver.h"
# include "setting/ShortcutKeySettingSaver.h"
#endif

#include "device/DTVReader.h"
#include "device/RadioReader.h"
#include "setting/DTVReaderSettingSaver.h"
#include "setting/RadioReaderSettingSaver.h"
#include "setting/SettingsFactory.h"

#include <QSettings>

SettingsSynchronizer::SettingsSynchronizer()
{

}

void SettingsSynchronizer::sync()
{
#ifdef Q_OS_MOBILE
    DTVReaderSettingSaver(&DTVReader::getInstance()).save();
    RadioReaderSettingSaver(&RadioReader::getInstance()).save();
#else
    MainWindowInterface *window = MainWindowFactory::getInstance();

    MainWindowSettingSaver(window).save();
    ScreenSettingSaver(Screen::getInstance()).save();
    DTVReaderSettingSaver(&DTVReader::getInstance()).save();
    RadioReaderSettingSaver(&RadioReader::getInstance()).save();
    MediaPlayerSettingSaver(&MediaPlayer::getInstance()).save();
    PlayListSettingSaver(&window->getPlayList()).save();
    ShortcutKeySettingSaver(&ShortcutKey::getInstance()).save();
    MediaPlayer::getInstance().getLastPlay().save();
#endif

    for (int i = 0; i < SettingsFactory::ST_COUNT; i++)
        SettingsFactory::getInstance((SettingsFactory::SettingType)i).sync();
}
