﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ShortcutKeySettingLoader.h"
#include "Settings.h"
#include "ui/ShortcutKey.h"

#include <QSettings>
#include <QShortcut>

ShortcutKeySettingLoader::ShortcutKeySettingLoader(ShortcutKey *shortcutKey) :
    ShortcutKeySettingBase(shortcutKey)
{

}

void ShortcutKeySettingLoader::load() const
{
    QVariantList shortcutTmp;

    for (int i = 0; i < AnyVODEnums::SK_COUNT; i++)
    {
        QString key;
        QShortcut *shortcut = this->m_shortcutKey->getKey((AnyVODEnums::ShortcutKey)i);

        if (shortcut)
            key = shortcut->key().toString();

        shortcutTmp.append(key);
    }

    shortcutTmp = this->m_settings.value(SETTING_SHORTCUTS, shortcutTmp).toList();

    for (int i = 0; i < shortcutTmp.count() && i < AnyVODEnums::SK_COUNT; i++)
    {
        QShortcut *shortcut = this->m_shortcutKey->getKey((AnyVODEnums::ShortcutKey)i);

        if (shortcut)
            shortcut->setKey(QKeySequence::fromString(shortcutTmp[i].value<QString>()));
    }
}
