﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

class QString;

extern const QString ENV_ITEM_DELIMITER;
extern const QString ENV_FILENAME;
extern const QString ENV_REMOTE_ADDR_NAME;
extern const QString ENV_FONT_PATH_NAME;
extern const QString ENV_REMOTE_PORT_NAME;
extern const QString ENV_REMOTE_STREAM_PORT_NAME;
extern const QString ENV_FONT_SIZE_NAME;
extern const QString ENV_FONT_SUBTITLE_OUTLINE_SIZE_NAME;
extern const QString ENV_SKIN_NAME;
extern const QString ENV_SHADER_NAME;
extern const QString SETTING_FILE_NAME;
extern const QString SETTING_LAST_PLAY_FILE_NAME;
extern const QString SETTING_EQUALIZER_FILE_NAME;
extern const QString SETTING_MAINWINDOW_GEOMETRY;
extern const QString SETTING_MAINWINDOW_MIN_SIZE;
extern const QString SETTING_MAINWINDOW_MAX_SIZE;
extern const QString SETTING_MAINWINDOW_MOSTTOP;
extern const QString SETTING_MAINWINDOW_OPAQUE;
extern const QString SETTING_MAINWINDOW_LAST_DIR;
extern const QString SETTING_SCREEN_VISIBLE;
extern const QString SETTING_SCREEN_CAPTURE_DIR;
extern const QString SETTING_SCREEN_CAPTURE_EXT;
extern const QString SETTING_SPECTRUM_TYPE;
extern const QString SETTING_VOLUME;
extern const QString SETTING_PLAYLIST;
extern const QString SETTING_PLAYLIST_SHOW;
extern const QString SETTING_PLAYLIST_GEOMETRY;
extern const QString SETTING_CURRENT_PLAYING;
extern const QString SETTING_SHOW_SUBTITLE;
extern const QString SETTING_HALIGN_SUBTITLE;
extern const QString SETTING_USE_NORMALIZER;
extern const QString SETTING_USE_EQUALIZER;
extern const QString SETTING_EQUALIZER_PREAMP;
extern const QString SETTING_EQUALIZER_GAINS;
extern const QString SETTING_DEINTERLACER_METHOD;
extern const QString SETTING_DEINTERLACER_ALGORITHM;
extern const QString SETTING_SEEK_KEYFRAME;
extern const QString SETTING_SKIP_OPENING;
extern const QString SETTING_SKIP_ENDING;
extern const QString SETTING_SKIP_USING;
extern const QString SETTING_SKIP_RANGE;
extern const QString SETTING_SHORTCUTS;
extern const QString SETTING_FILE_ASSOCIATION_VIDEO;
extern const QString SETTING_FILE_ASSOCIATION_AUDIO;
extern const QString SETTING_FILE_ASSOCIATION_SUBTITLE;
extern const QString SETTING_FILE_ASSOCIATION_PLAYLIST;
extern const QString SETTING_ENABLE_SEARCH_SUBTITLE;
extern const QString SETTING_ENABLE_SEARCH_LYRICS;
extern const QString SETTING_VALIGN_SUBTITLE;
extern const QString SETTING_SUBTITLE_SIZE;
extern const QString SETTING_USER_ASPECT_RATIO;
extern const QString SETTING_USE_HW_DECODER;
extern const QString SETTING_USE_PBO;
extern const QString SETTING_USE_VSYNC;
extern const QString SETTING_LANGUAGE;
extern const QString SETTING_CLEAR_PLAYLIST;
extern const QString SETTING_SHOW_ALBUM_JACKET;
extern const QString SETTING_ENABLE_LAST_PLAY;
extern const QString SETTING_TEXT_ENCODING;
extern const QString SETTING_ADDRESS;
extern const QString SETTING_COMMAND_PORT;
extern const QString SETTING_STREAM_PORT;
extern const QString SETTING_USE_FRAME_DROP;
extern const QString SETTING_SUBTITLE_DIRECTORY;
extern const QString SETTING_PRIOR_SUBTITLE_DIRECTORY;
extern const QString SETTING_DTV_SCANNED_CHANNELS;
extern const QString SETTING_DTV_CURRENT_COUNTRY;
extern const QString SETTING_DTV_CURRENT_TYPE;
extern const QString SETTING_SEARCH_SUBTITLE_COMPLEX;
extern const QString SETTING_SPDIF_DEVICE;
extern const QString SETTING_USE_BUFFERING_MODE;
extern const QString SETTING_FIRST_LOADING;
extern const QString SETTING_USE_SUBTITLE_CACHE_MODE;
extern const QString SETTING_FONT;
extern const QString SETTING_CURRENT_QUICKBAR_ITEM;
extern const QString SETTING_DELETE_MEDIA_WITH_SUBTITLE;
extern const QString SETTING_USE_LOW_QUALITY_MODE;
extern const QString SETTING_AUDIO_DEVICE;
extern const QString SETTING_SPDIF_ENCODING;
extern const QString SETTING_SPDIF_SAMPLE_RATE;
extern const QString SETTING_USE_SPDIF;
extern const QString SETTING_VR_USE_DISTORTION;
extern const QString SETTING_VR_BARREL_DISTORTION_COEFFICIENTS;
extern const QString SETTING_VR_PINCUSHION_DISTORTION_COEFFICIENTS;
extern const QString SETTING_VR_DISTORTION_LENS_CENTER;
extern const QString SETTING_USE_GPU_CONVERT;
extern const QString SETTING_KEYBOARD_LANGUAGE;
extern const QString SETTING_AUTO_SAVE_SEARCH_LYRICS;
extern const QString SETTING_UPDATE_FILENAME;
extern const QString SETTING_CHECK_UPDATE_GAP;
extern const QString SETTING_CURRENT_UPDATE_DATE;
extern const QString SETTING_CHECK_UPDATE_GAP_FIX;
extern const QString SETTING_PROXY_INFO;
extern const QString SETTING_BLUETOOTH_AUDIO_SYNC;
extern const QString SETTING_USE_HDR;
extern const QString SETTING_HW_DECODER;
extern const QString SETTING_USE_AUTO_COLOR_CONVERSION;
extern const QString SETTING_USE_IVTC;
extern const QString SETTING_PLAYING_METHOD;
extern const QString SETTING_RADIO_SCANNED_CHANNELS;
extern const QString SETTING_RADIO_CURRENT_COUNTRY;
extern const QString SETTING_RADIO_CURRENT_TYPE;
