﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "core/FrameMetaData.h"
#include "Surface.h"

#include <QMutex>

class VideoPicture
{
public:
    VideoPicture();

    bool create(int width, int height, int orgWidth, int orgHeight, AVPixelFormat format);
    bool create(int width, int height, AVPixelFormat format);
    void destory();

    void lock();
    void unlock();

    bool isValid() const;

    void setLeftOrTop3D(bool leftOrTop3D);
    bool isLeftOrTop() const;

    void addTime(uint32_t time);
    void setTime(uint32_t time);
    uint32_t getTime() const;

    void setPts(double pts);
    double getPts() const;

    void setLuminanceAverage(uint8_t lumAvg);
    uint8_t getLuminanceAverage() const;

    void setFrameMetaData(const FrameMetaData &frameMetaData);
    FrameMetaData getFrameMetaData() const;

    QSize getOrgSize() const;
    int getOrgWidth() const;
    int getOrgHeight() const;

    const Surface& getSurface() const;

    void resize(int width, int height, VideoPicture &dest) const;
    void copy(int width, int height, VideoPicture &dest) const;
    void fillAlpha(uint8_t value);

private:
    void copyMembers(int width, int height, VideoPicture &dest) const;

private:
    Surface m_surface;
    int m_orgWidth;
    int m_orgHeight;
    double m_pts;
    uint32_t m_time;
    uint8_t m_lumAvg;
    bool m_leftOrTop3D;
    FrameMetaData m_frameMetaData;
    QMutex m_lock;
};
