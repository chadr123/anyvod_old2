﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "Concurrent.h"
#include "VideoPicture.h"

#define VIDEO_PICTURE_QUEUE_SIZE (2)

class VideoFrameQueue
{
public:
    VideoFrameQueue();

    int getSize();
    int getSizeWithoutLock();

    void wait(const QVector<volatile bool*> &quitConditions);
    void wakeUp();
    void pop();
    void push();
    void flush();

    VideoPicture& getFrame(int index);
    VideoPicture& getReadingFrame();
    VideoPicture& getWritingFrame();
    VideoPicture& getPrevFrame();

    VideoPicture& getAudioPicture();

    void savePrevFrameIndex();
    void releaseFrames();

private:
    bool canQuit(const QVector<volatile bool*> &quitConditions) const;
    void resetFrame(int index);
    void resetAudioPicture();

private:
    struct QueueData
    {
        QueueData()
        {
            size = 0;
            rIndex = 0;
            wIndex = 0;
        }

        int size;
        int rIndex;
        int wIndex;
        Concurrent lock;
    };

private:
    VideoPicture m_queue[VIDEO_PICTURE_QUEUE_SIZE];
    QueueData m_data;
    VideoPicture m_audioPicture;
    int m_prevPictureIndex;
};
