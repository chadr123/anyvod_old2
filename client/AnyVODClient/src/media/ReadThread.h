﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "MediaThreadBase.h"
#include "Callbacks.h"

class ReadThread : public MediaThreadBase
{
    Q_OBJECT
public:
    explicit ReadThread(QObject *parent);

    void setEndedCallback(const EventCallback &callback);

private:
    bool readAudio() const;

protected:
    virtual void run();

private:
    static const int PAUSE_REFRESH_DELAY;
    static const int READ_CONTINUE_DELAY;
    static const int CHECK_END_DELAY;
    static const int MAX_READ_RETRY_COUNT;

private:
    EventCallback m_ended;
};
