﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <stdint.h>
#include <QImage>
#include <QVector>

extern "C"
{
# include <libavutil/avutil.h>
# include <libavfilter/avfilter.h>
}

struct AVFormatContext;
struct AVCodecContext;
struct AVPacket;
struct SwsContext;

class FrameExtractor
{
public:
    struct FrameItem
    {
        FrameItem()
        {
            time = -1.0;
            realTime = -1.0;
            buffer = nullptr;
            rotation = 0.0;
        }

        double time;
        double realTime;
        double rotation;
        QImage frame;
        uint8_t *buffer;
    };

    FrameExtractor();
    ~FrameExtractor();

    bool open(const QString &filePath, bool autoColorConversion);
    bool openWithType(const QString &filePath, bool autoColorConversion, bool *isVideo);
    void close();

    bool isVideo(const QString &filePath);

    void setPixFormat(AVPixelFormat pixFormat);
    AVPixelFormat getPixFormat() const;

    bool getFrame(double time, bool ignoreTime, FrameItem *ret);
    bool getFramesByPeriod(double period, QVector<FrameItem> *ret);

    double getDuration() const;
    int getWidth() const;
    int getHeight() const;

private:
    bool initFilter();
    void unInitFilter();
    bool linkFilter();
    bool readPacket(double *retPTS, AVPacket *ret);
    bool decodePacket(double time, bool ignoreTime, AVPacket &packet, FrameItem *ret);
    bool getFilteredFrame(const AVFrame &in, AVFrame *out, AVPixelFormat *retFormat);

private:
    static const int MAX_READ_RETRY_COUNT;

private:
    double m_duration;
    int m_width;
    int m_height;
    AVFormatContext *m_format;
    AVCodecContext *m_ctx;
    SwsContext *m_imageConverter;
    int m_index;
    double m_rotation;
    AVPixelFormat m_pixFormat;
    bool m_useAutoColorConversion;
    AVFilterContext *m_in;
    AVFilterContext *m_colorConversion;
    AVFilterContext *m_eq;
    AVFilterContext *m_out;
    AVFilterGraph *m_graph;
    QVector<AVFilterContext*> m_filters;
};
