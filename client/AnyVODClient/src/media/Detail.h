﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QAtomicInt>
#include <QSize>

#include <stdint.h>

struct Detail
{
    Detail()
    {
        cpuUsage = 0.0f;
        usingMemSize = 0;
        currentTime = 0.0;
        totalTime = 0.0;
        timePercentage = 0.0;
        dtvSignal = false;
        dtvSignalStrength = 0.0;
        videoInputBits = 0;
        videoInputByteRate = 0;
        videoInputByteCount = 0;
        videoOutputBits = 0;
        videoOutputByteRate = 0;
        videoOutputByteCount = 0;
        videoFrameCount = 0;
        videoFPS = 0.0;
        videoTotalFrame = 0;
        videoCurrentFrame = 0;
        videoFrameDropCount = 0;
        videoInterlaced = false;
        videoDeinterlaced = false;
        audioInputBits = 0;
        audioInputSampleRate = 0;
        audioInputChannels = 0;
        audioInputByteRate = 0;
        audioInputByteCount = 0;
        audioOutputSampleRate = 0;
        audioOutputChannels = 0;
        audioOutputBits = 0;
        audioOutputByteRate = 0;
        audioOutputByteCount = 0;
        subtitleBitmap = false;
        subtitleValidColor = true;
        subtitleColorCount = 0;
    }

    float cpuUsage;
    uint64_t usingMemSize;

    QString fileName;
    double currentTime;
    double totalTime;
    double timePercentage;
    bool dtvSignal;
    double dtvSignalStrength;

    QString videoCodec;
    QString videoHWDecoder;

    QString videoInputType;
    int videoInputBits;
    QSize videoInputSize;
    bool videoInterlaced;
    bool videoDeinterlaced;

    int videoInputByteRate;
    QAtomicInt videoInputByteCount;

    QString videoOutputType;
    int videoOutputBits;
    QSize videoOutputSize;

    int videoOutputByteRate;
    QAtomicInt videoOutputByteCount;

    QAtomicInt videoFrameCount;
    float videoFPS;

    int videoTotalFrame;
    QAtomicInt videoCurrentFrame;
    QAtomicInt videoFrameDropCount;

    QString audioCodec;
    QString audioCodecSimple;

    QString audioInputType;
    int audioInputBits;
    int audioInputSampleRate;
    int audioInputChannels;

    int audioInputByteRate;
    QAtomicInt audioInputByteCount;

    QString audioSPDIFOutputDevice;
    QString audioOutputType;
    int audioOutputSampleRate;
    int audioOutputChannels;
    int audioOutputBits;

    int audioOutputByteRate;
    QAtomicInt audioOutputByteCount;

    QString subtitleCodec;
    bool subtitleBitmap;
    bool subtitleValidColor;
    int subtitleColorCount;

    QString fileFormat;
};
