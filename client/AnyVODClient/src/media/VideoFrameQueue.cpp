﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "VideoFrameQueue.h"

#include <QMutexLocker>
#include <QVector>

VideoFrameQueue::VideoFrameQueue() :
    m_prevPictureIndex(0)
{

}

int VideoFrameQueue::getSize()
{
    QMutexLocker locker(&this->m_data.lock.mutex);

    return this->getSizeWithoutLock();
}

int VideoFrameQueue::getSizeWithoutLock()
{
    return this->m_data.size;
}

void VideoFrameQueue::wait(const QVector<volatile bool*> &quitConditions)
{
    this->m_data.lock.mutex.lock();

    while (this->m_data.size >= VIDEO_PICTURE_QUEUE_SIZE && !this->canQuit(quitConditions))
        this->m_data.lock.cond.wait(&this->m_data.lock.mutex);

    this->m_data.lock.mutex.unlock();
}

void VideoFrameQueue::wakeUp()
{
    this->m_data.lock.mutex.lock();
    this->m_data.lock.cond.wakeOne();
    this->m_data.lock.mutex.unlock();
}

void VideoFrameQueue::pop()
{
    if (++this->m_data.rIndex >= VIDEO_PICTURE_QUEUE_SIZE)
        this->m_data.rIndex = 0;

    this->m_data.lock.mutex.lock();

    if (this->m_data.size-- == 0)
        this->m_data.size = 0;

    this->m_data.lock.cond.wakeOne();
    this->m_data.lock.mutex.unlock();
}

void VideoFrameQueue::push()
{
    if (++this->m_data.wIndex >= VIDEO_PICTURE_QUEUE_SIZE)
        this->m_data.wIndex = 0;

    this->m_data.lock.mutex.lock();
    this->m_data.size++;
    this->m_data.lock.mutex.unlock();
}

void VideoFrameQueue::flush()
{
    QMutexLocker locker(&this->m_data.lock.mutex);

    this->m_data.rIndex = 0;
    this->m_data.wIndex = 0;
    this->m_data.size = 0;
}

VideoPicture& VideoFrameQueue::getFrame(int index)
{
    return this->m_queue[index];
}

VideoPicture& VideoFrameQueue::getReadingFrame()
{
    return this->m_queue[this->m_data.rIndex];
}

VideoPicture& VideoFrameQueue::getWritingFrame()
{
    return this->m_queue[this->m_data.wIndex];
}

VideoPicture& VideoFrameQueue::getPrevFrame()
{
    return this->m_queue[this->m_prevPictureIndex];
}

VideoPicture& VideoFrameQueue::getAudioPicture()
{
    return this->m_audioPicture;
}

void VideoFrameQueue::savePrevFrameIndex()
{
    this->m_prevPictureIndex = this->m_data.rIndex;
}

void VideoFrameQueue::releaseFrames()
{
    for (int i = 0; i < VIDEO_PICTURE_QUEUE_SIZE; i++)
        this->resetFrame(i);

    this->resetAudioPicture();
}

void VideoFrameQueue::resetFrame(int index)
{
    this->m_queue[index].destory();
}

void VideoFrameQueue::resetAudioPicture()
{
    this->m_audioPicture.destory();
}

bool VideoFrameQueue::canQuit(const QVector<volatile bool*> &quitConditions) const
{
    for (volatile bool *condition : quitConditions)
    {
        if (*condition)
            return true;
    }

    return false;
}
