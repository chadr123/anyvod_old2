﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RefreshThread.h"
#include "MediaState.h"

#include <limits>

#include <QDebug>

using namespace std;

RefreshThread::RefreshThread(QObject *parent) :
    QThread(parent),
    m_stop(false)
{

}

RefreshThread::~RefreshThread()
{
    this->stop();
}

void RefreshThread::setVideoEnabled(bool enable)
{
    this->m_isVideo = enable;
}

void RefreshThread::setCallback(const PaintCallback &callback)
{
    this->m_callback = callback;
}

void RefreshThread::refreshTimer(int refreshTime)
{
    this->m_lock.lock();

#ifdef Q_OS_MOBILE
    if (this->m_refresh.isEmpty())
#endif
        this->m_refresh.enqueue(refreshTime);

    this->m_wait.wakeOne();
    this->m_lock.unlock();
}

void RefreshThread::stop()
{
    this->m_stop = true;
    this->m_wait.wakeOne();

    if (this->isRunning())
        this->wait();

    this->m_lock.lock();
    this->m_refresh.clear();
    this->m_lock.unlock();

    this->m_stop = false;
}

void RefreshThread::run()
{
    while (!this->m_stop)
    {
        int refresh = 0;

        this->m_lock.lock();

        if (this->m_refresh.isEmpty())
            this->m_wait.wait(&this->m_lock);

        if (this->m_stop)
        {
            this->m_lock.unlock();
            break;
        }

        refresh = this->m_refresh.dequeue();

        this->m_lock.unlock();

        if (refresh > 0)
            this->msleep(refresh);

#if !defined Q_OS_MOBILE
        if (this->m_isVideo)
        {
#endif
            if (this->m_callback.callback)
                this->m_callback.callback(this->m_callback.userData);
#if !defined Q_OS_MOBILE
        }
#endif
    }
}
