﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "core/AnyVODEnums.h"

#include <stdint.h>

extern "C"
{
# include <libavutil/pixfmt.h>
# include <libavutil/frame.h>
}

class ASSParser;
class YouTubeParser;
class NaverTVParser;
class SAMIParser;
class SRTParser;
class AVParser;
class SPDIF;
class HWDecoder;
class Deinterlacer;
class FilterGraph;

struct Stream;
struct Detail;
struct FrameMetaData;

class MediaPresenterInterface
{
public:
    MediaPresenterInterface();
    virtual ~MediaPresenterInterface();

    virtual ASSParser& getASSParser() = 0;
    virtual YouTubeParser& getYoutubeParser() = 0;
    virtual NaverTVParser& getNaverTVParser() = 0;
    virtual AVParser& getAVParser() = 0;
    virtual SAMIParser& getSAMIParser() = 0;
    virtual SRTParser& getSRTParser() = 0;

    virtual HWDecoder& getHWDecoder() = 0;
    virtual Stream* getStream(int index) = 0;
    virtual const Detail& getDetail() const = 0;
    virtual Detail& getRDetail() = 0;
    virtual Deinterlacer& getDeinterlacer() = 0;
    virtual FilterGraph& getFilterGraph() = 0;

    virtual AnyVODEnums::Video3DMethod get3DMethod() const = 0;
    virtual AnyVODEnums::SPDIFEncodingMethod getSPDIFEncodingMethod() const = 0;
    virtual int getCurrentAudioStreamIndex() const = 0;

    virtual void setFormat(AVPixelFormat format) = 0;
    virtual AVPixelFormat getFormat() const = 0;

    virtual int64_t getAbsoluteClock() const = 0;
    virtual double getMasterClock() = 0;

    virtual double getDuration() const = 0;
    virtual bool existSubtitle() = 0;

    virtual double getAudioClockOffset() const = 0;

    virtual void pause() = 0;
    virtual void resume() = 0;
    virtual void seek() = 0;

    virtual void abort(int reason) = 0;
    virtual void callEmptyCallback(bool show) = 0;
    virtual void callAudioSubtitleCallback() = 0;

    virtual double synchronizeVideo(AVFrame *srcFrame, double pts, const AVRational timeBase) = 0;
    virtual bool convertPicture(AVFrame &inFrame, double pts, AVPixelFormat format, bool leftOrTop3D, const FrameMetaData &frameMeta) = 0;

    virtual bool isEnabledVideo() const = 0;
    virtual bool isAudio() const = 0;
    virtual bool isVideo() const = 0;
    virtual bool isRemoteFile() const = 0;
    virtual bool isRemoteProtocol() const = 0;
    virtual bool isDevice() const = 0;
    virtual bool isUseBufferingMode() const = 0;
    virtual bool isUseFrameDrop() const = 0;
    virtual bool isDTV() const = 0;
    virtual bool isRadio() const = 0;
    virtual bool isLive() const = 0;
    virtual bool isUseGPUConvert(AVPixelFormat format) const = 0;
    virtual bool isUsingSPDIFEncoding() const = 0;
    virtual bool isValidSubtitleWithoutClass() = 0;

    virtual void startAudio() = 0;
    virtual void resumeAudio() = 0;
    virtual int getRemainedAudioDataSize() const = 0;

public:
    static const int EMPTY_BUFFER_WAIT_DELAY;
    static const double NOSYNC_THRESHOLD;
};
