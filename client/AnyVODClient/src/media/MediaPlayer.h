﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "MediaPresenter.h"
#include "LastPlay.h"

#include <QUuid>
#include <QCoreApplication>

class MediaPlayer
{
    Q_DECLARE_TR_FUNCTIONS(MediaPlayer)
private:
    MediaPlayer();
    ~MediaPlayer();

public:
    enum Status
    {
        Started,
        Playing,
        Paused,
        Stopped,
        Ended
    };

    static MediaPlayer& getInstance();

    void setPlayingCallback(EventCallback &callback);
    void setStatusChangedCallback(EventCallback &callback);
    void setEmptyBufferCallback(EmptyBufferCallback &callback);
    void setShowAudioOptionDescCallback(ShowAudioOptionDescCallback &callback);
    void setAudioSubtitleCallback(AudioSubtitleCallback &callback);
    void setPaintCallback(PaintCallback &callback);
    void setAbortCallback(AbortCallback &callback);
    void setNonePlayingDescCallback(NonePlayingDescCallback &callback);

    void setDevicePixelRatio(double ratio);

    bool reOpen();
    bool open(const QString &filePath, const QString &title, const ExtraPlayData &data, const QUuid &unique,
              const QString &fontFamily, const int fontSize, const int subtitleOutlineSize, const QString &audioPath);
    void close();
    bool isOpened() const;

    bool resetScreen(const int width, const int height, TextureInfo *texInfo, bool inContext);

    QString getSubtitlePath() const;
    bool openSubtitle(const QString &filePath, bool showDesc);
    bool saveSubtitleAs(const QString &filePath);
    bool saveSubtitle();
    void closeAllExternalSubtitles();

    QString getFileName() const;
    QString getFilePath() const;

    QUuid getUnique() const;
    const QVector<ChapterInfo>& getChapters() const;

    void pause(bool useRaw);
    void resume(bool useRaw);
    bool recover();

    bool play();
    void pause();
    void resume();
    void stop();

    void toggle();

    bool isPlayUserDataEmpty() const;
    QString getTitle() const;

    bool isPlayOrPause() const;
    bool isRemoteFile() const;

    void prevFrame(int count);
    void nextFrame(int count);

    bool canMoveChapter() const;
    bool canMoveFrame() const;

    bool isTempoUsable() const;
    float getTempo() const;
    void setTempo(float percent);

    void setCaptureMode(bool capture);
    bool getCaptureMode() const;

    void setUserAspectRatio(UserAspectRatioInfo &ratio);
    void getUserAspectRatio(UserAspectRatioInfo *ret) const;

    void setPlayingMethod(AnyVODEnums::PlayingMethod method);
    AnyVODEnums::PlayingMethod getPlayingMethod() const;

    bool render();

    void getGOMSubtitleURL(QString *ret) const;
    void retreiveExternalSubtitle(const QString &filePath);

    int getCurrentVideoStreamIndex() const;
    void getVideoStreamInfo(QVector<VideoStreamInfo> *ret) const;
    bool changeVideoStream(const int index);

    int getCurrentAudioStreamIndex() const;
    void getAudioStreamInfo(QVector<AudioStreamInfo> *ret) const;
    bool changeAudioStream(const int index);
    HSTREAM getAudioHandle() const;

    void setDeinterlaceMethod(AnyVODEnums::DeinterlaceMethod method);
    void setDeinterlaceAlgorithm(AnyVODEnums::DeinterlaceAlgorithm algorithm);
    AnyVODEnums::DeinterlaceMethod getDeinterlaceMethod();
    AnyVODEnums::DeinterlaceAlgorithm getDeinterlaceAlgorithm();

    void useDeinterlaceIVTC(bool use);
    bool isUseDeinterlaceIVTC();

    bool isDeinterlaceAVFilter();

    void setHAlign(AnyVODEnums::HAlignMethod align);
    AnyVODEnums::HAlignMethod getHAlign() const;
    void setVAlign(AnyVODEnums::VAlignMethod align);
    AnyVODEnums::VAlignMethod getVAlign() const;
    bool isAlignable();

    void showDetail(bool show);
    bool isShowDetail() const;
    const Detail& getDetail() const;

    bool setAudioDevice(int device);
    int getCurrentAudioDevice() const;

    void getSPDIFAudioDevices(QStringList *ret);
    bool setSPDIFAudioDevice(int device);
    int getCurrentSPDIFAudioDevice() const;

    void setScreenRotationDegree(AnyVODEnums::ScreenRotationDegree degree);
    AnyVODEnums::ScreenRotationDegree getScreenRotationDegree() const;

    void showOptionDesc(const QString &desc);
    void setASSFontPath(const QString &path);
    void setASSFontFamily(const QString &family);

    void showSubtitle(bool show);
    void showSubtitle(bool show, bool raw);
    bool isShowSubtitle() const;

    void setSearchSubtitleComplex(bool use);
    bool getSearchSubtitleComplex() const;

    bool existSubtitle();
    bool existFileSubtitle();
    bool existExternalSubtitle();
    bool existAudioSubtitle();
    bool existAudioSubtitleGender() const;

    void getSubtitleClasses(QStringList *classNames);
    void getCurrentSubtitleClass(QString *className);
    bool setCurrentSubtitleClass(const QString &className);

    void resetSubtitlePosition();
    void setVerticalSubtitlePosition(int pos);
    void setHorizontalSubtitlePosition(int pos);
    int getVerticalSubtitlePosition();
    int getHorizontalSubtitlePosition();

    void reset3DSubtitleOffset();
    void setVertical3DSubtitleOffset(int offset);
    void setHorizontal3DSubtitleOffset(int offset);
    int getVertical3DSubtitleOffset();
    int getHorizontal3DSubtitleOffset();

    void setRepeatStart(double start);
    void setRepeatEnd(double end);
    void setRepeatEnable(bool enable);
    bool getRepeatEnable() const;
    double getRepeatStart() const;
    double getRepeatEnd() const;

    void setRepeatStartMove(double offset);
    void setRepeatEndMove(double offset);
    void setRepeatMove(double offset);

    void setSeekKeyFrame(bool keyFrame);
    bool isSeekKeyFrame() const;

    void setSubtitleDirectory(const QStringList &paths, bool prior);
    void getSubtitleDirectory(QStringList *path, bool *prior) const;

    void set3DMethod(AnyVODEnums::Video3DMethod method);
    AnyVODEnums::Video3DMethod get3DMethod() const;

    void setSubtitle3DMethod(AnyVODEnums::Subtitle3DMethod method);
    AnyVODEnums::Subtitle3DMethod getSubtitle3DMethod() const;

    void setSkipRanges(const QVector<MediaPresenter::Range> &ranges);
    void getSkipRanges(QVector<MediaPresenter::Range> *ret) const;

    void setSkipOpening(bool skip);
    bool getSkipOpening() const;
    void setSkipEnding(bool skip);
    bool getSkipEnding() const;
    void setUseSkipRange(bool use);
    bool getUseSkipRange() const;

    void useNormalizer(bool use);
    bool isUsingNormalizer() const;

    void useEqualizer(bool use);
    bool isUsingEqualizer() const;

    void useLowerVoice(bool use);
    bool isUsingLowerVoice() const;

    void useHigherVoice(bool use);
    bool isUsingHigherVoice() const;

    void useLowerMusic(bool use);
    bool isUsingLowerMusic() const;

    void addSubtitleOpaque(float inc);
    float getSubtitleOpaque() const;
    void resetSubtitleOpaque();

    void addSubtitleSize(float inc);
    float getSubtitleSize() const;
    void setSubtitleSize(float size);
    void resetSubtitleSize();

    void useHWDecoder(bool enable, bool raw);
    bool isUseHWDecoder() const;
    void changeUseHWDecoder(bool enable);

    int getHWDecoderCount() const;
    bool setHWDecoderIndex(int index);
    int getHWDecoderIndex() const;
    QString getHWDecoderName(int index) const;

    void useFrameDrop(bool enable);
    bool isUseFrameDrop() const;

    void useBufferingMode(bool enable);
    bool isUseBufferingMode() const;

    void useGPUConvert(bool use);
    bool isUseGPUConvert() const;

    void useHDR(bool use);
    bool isUseHDR() const;

    void applyColorConversion();
    void useAutoColorConversion(bool use);
    bool isUsingAutoColorConversion() const;

    void setHDRTextureSupported(bool supported);
    bool isHDRTextureSupported() const;

    void useSPDIF(bool enable);
    bool isUseSPDIF() const;

    void setSPDIFEncodingMethod(AnyVODEnums::SPDIFEncodingMethod method);
    AnyVODEnums::SPDIFEncodingMethod getSPDIFEncodingMethod() const;

    void use3DFull(bool enable);
    bool isUse3DFull() const;

    void usePBO(bool enable);
    bool isUsePBO() const;
    bool isUsablePBO() const;

    QSize getSize() const;

    void resetSPDIF();

    bool setPreAmp(float dB);
    float getPreAmp() const;

    bool setEqualizerGain(int band, float gain);
    float getEqualizerGain(int band) const;
    int getBandCount() const;

    void prevSubtitleSync(double amount);
    void nextSubtitleSync(double amount);
    void resetSubtitleSync();

    void prevAudioSync(double amount);
    void nextAudioSync(double amount);
    void resetAudioSync();

    bool isEnableSearchSubtitle() const;
    bool isEnableSearchLyrics() const;
    void enableSearchSubtitle(bool enable);
    void enableSearchLyrics(bool enable);

    void enableAutoSaveSearchLyrics(bool enable);
    bool isAutoSaveSearchLyrics() const;

    void seek(double time, bool any);
    void rewind(double distance);
    void forward(double distance);

    void volume(uint8_t volume);
    void mute(bool mute, bool showDesc);

    uint8_t getMaxVolume() const;
    uint8_t getVolume() const;

    double getDuration() const;
    double getCurrentPosition();
    bool hasDuration() const;

    bool IsHDRVideo() const;

    double getAspectRatio(bool widthPrio) const;

    void setMaxTextureSize(int size);
    int getMaxTextureSize() const;

    Status getStatus();

    bool canCapture() const;

    bool isEnabledVideo() const;
    bool isAudio() const;
    bool isVideo() const;
    bool isSubtitleMoveable();
    bool is3DSubtitleMoveable();
    bool isMovieSubtitleVisiable() const;

    void showAlbumJacket(bool show);
    bool isShowAlbumJacket() const;

    bool getFrameSize(int *width, int *height) const;
    bool getPictureRect(QRect *rect);

    void enableGotoLastPos(bool enable, bool raw);
    bool isGotoLastPos() const;

    void addPincushionK1(float value);
    void addPincushionK2(float value);

    float getPincushionK1() const;
    float getPincushionK2() const;

    void addBarrelK1(float value);
    void addBarrelK2(float value);

    float getBarrelK1() const;
    float getBarrelK2() const;

    void addVirtual3DDepth(qreal depth);

    QVector2D getPincushionDistortionCoefficients() const;
    void setPincushionDistortionCoefficients(const QVector2D &coefficients);

    QVector2D getBarrelDistortionCoefficients() const;
    void setBarrelDistortionCoefficients(const QVector2D &coefficients);

    void setDistortionAdjustMode(AnyVODEnums::DistortionAdjustMode mode);
    AnyVODEnums::DistortionAdjustMode getDistortionAdjustMode() const;

    void setDistortionLensCenter(const QVector2D &lensCenter);
    QVector2D getDistortionLensCenter() const;

    void setVRInputSource(AnyVODEnums::VRInputSource source);
    AnyVODEnums::VRInputSource getVRInputSource() const;

    void useDistortion(bool use);
    bool isUseDistortion() const;

    void resetFOV();
    float getFOV() const;
    bool addFOV(float value);

    FilterGraph& getFilterGraph();
    LastPlay& getLastPlay();

    bool configFilterGraph();
    void clearFrameBuffers();

private:
    void navigate(double distance);

    void subtitleSync(double value);
    void audioSync(double value);

    void callChanged();

    void tryOpenSubtitle(const QString &filePath);

    void retreiveLyrics(const QString &filePath);
    void retreiveSubtitleURL(const QString &filePath);

    void updateLastPlay();

private:
    static void playingCallback(void *userData);
    static void endedCallback(void *userData);

private:
    MediaPresenter m_presenter;
    Status m_status;
    QString m_filePath;
    QString m_audioPath;
    QString m_title;
    QString m_lastPlayPath;
    QUuid m_unique;
    ExtraPlayData m_playData;
    QString m_fontFamily;
    int m_fontSize;
    int m_subtitleOutlineSize;
    EventCallback m_playing;
    EventCallback m_statusChanged;
    LastPlay m_lastPlay;
    bool m_gotoLastPlay;
    bool m_priorSubtitleDirectory;
    bool m_useSearchSubtitleComplex;
    bool m_useAutoColorConversion;
    QStringList m_subtitleDirectory;
    AnyVODEnums::PlayingMethod m_playingMethod;
};
