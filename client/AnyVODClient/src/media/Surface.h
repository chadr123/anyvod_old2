﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <stdint.h>

extern "C"
{
# include <libavutil/pixfmt.h>
# include <libavutil/frame.h>
}

#define PICTURE_MAX_PLANE AV_NUM_DATA_POINTERS

class QImage;
class QSize;

class Surface
{
public:
    Surface();
    ~Surface();

    QImage getImage() const;

    QSize getSize() const;
    int getPlain() const;
    int getWidth() const;
    int getHeight() const;

    AVPixelFormat getFormat() const;

    uint8_t** getPixels() const;
    int* getLineSize() const;

    Surface getDummy() const;

    bool create(int width, int height, AVPixelFormat format);
    void destory();
    void clearPixels();
    void fillAlpha(uint8_t value);

    bool isValid() const;
    bool isDummy() const;

private:
    uint8_t *m_pixels[PICTURE_MAX_PLANE];
    int m_lineSize[PICTURE_MAX_PLANE];
    int m_plane;
    int m_width;
    int m_height;
    AVPixelFormat m_format;
};

