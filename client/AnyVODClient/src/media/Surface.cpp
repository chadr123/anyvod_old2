﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "Surface.h"
#include "core/Common.h"

#include <QImage>
#include <QSize>

extern "C"
{
# include <libavutil/pixdesc.h>
# include <libavutil/imgutils.h>
}

Surface::Surface() :
    m_plane(0),
    m_width(0),
    m_height(0),
    m_format(AV_PIX_FMT_NONE)
{
    memset(this->m_pixels, 0, sizeof(this->m_pixels));
    memset(this->m_lineSize, 0, sizeof(this->m_lineSize));
}

Surface::~Surface()
{
    this->destory();
}

QImage Surface::getImage() const
{
    if (this->m_pixels[0])
        return QImage(this->m_pixels[0], this->m_width, this->m_height,
                this->m_format == AV_PIX_FMT_YUV420P ? QImage::Format_Grayscale8 : QImage::Format_ARGB32);
    else
        return QImage();
}

QSize Surface::getSize() const
{
    return QSize(this->m_width, this->m_height);
}

int Surface::getPlain() const
{
    return this->m_plane;
}

int Surface::getWidth() const
{
    return this->m_width;
}

int Surface::getHeight() const
{
    return this->m_height;
}

AVPixelFormat Surface::getFormat() const
{
    return this->m_format;
}

uint8_t** Surface::getPixels() const
{
    return (uint8_t**)this->m_pixels;
}

int* Surface::getLineSize() const
{
    return (int*)this->m_lineSize;
}

Surface Surface::getDummy() const
{
    Surface dummy = *this;

    dummy.m_pixels[0] = nullptr;

    return dummy;
}

bool Surface::create(int width, int height, AVPixelFormat format)
{
    int planes = av_pix_fmt_count_planes(format);

    if (planes <= 0)
        return false;

    for (int i = 0; i < planes; i++)
    {
        int lineSize = av_image_get_linesize(format, width, i);

        if (lineSize > 0)
        {
            this->m_lineSize[i] = lineSize;
            this->m_pixels[i] = (uint8_t*)av_mallocz(this->m_lineSize[i] * height);
        }
    }

    this->m_height = height;
    this->m_width = width;
    this->m_format = format;
    this->m_plane = planes;

    return true;
}

void Surface::destory()
{
    for (int i = 0; i < this->m_plane; i++)
    {
        if (this->m_pixels[i])
            av_freep(&this->m_pixels[i]);

        this->m_lineSize[i] = 0;
    }

    this->m_plane = 0;
    this->m_width = 0;
    this->m_height = 0;
    this->m_format = AV_PIX_FMT_NONE;
}

void Surface::clearPixels()
{
    memset(this->m_pixels[0], 0, this->m_height * this->m_lineSize[0]);
}

void Surface::fillAlpha(uint8_t value)
{
    const AVPixFmtDescriptor *fmtDesc = av_pix_fmt_desc_get(this->m_format);

    if (!(fmtDesc && IS_BIT_SET(fmtDesc->flags, AV_PIX_FMT_FLAG_ALPHA | AV_PIX_FMT_FLAG_RGB)))
        return;

    const int alphaComponent = fmtDesc->nb_components - 1;

    for (int col = 0; col < this->m_height; col++)
    {
        for (int row = 0; row < this->m_lineSize[0]; row += fmtDesc->nb_components)
        {
            uint8_t *alpha = (&this->m_pixels[0][col * this->m_lineSize[0] + row]) + alphaComponent;

            *alpha = value;
        }
    }
}

bool Surface::isValid() const
{
    return this->m_format != AV_PIX_FMT_NONE;
}

bool Surface::isDummy() const
{
    return this->m_pixels[0] == nullptr;
}
