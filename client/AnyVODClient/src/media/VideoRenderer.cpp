﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include <QGlobalStatic>

#define GL_NV_geometry_program4

#ifndef Q_OS_WIN
# define GL_GLEXT_PROTOTYPES
#endif

#include "VideoRenderer.h"
#include "MediaPresenterInterface.h"
#include "MediaState.h"
#include "Detail.h"
#include "core/Common.h"
#include "core/TextureInfo.h"
#include "device/DTVReader.h"
#include "decoders/HWDecoder.h"
#include "audio/SPDIF.h"
#include "video/FilterGraph.h"
#include "video/Sphere.h"
#include "video/Cube.h"
#include "video/Camera.h"
#include "parsers/subtitle/ASSParser.h"
#include "parsers/subtitle/YouTubeParser.h"
#include "parsers/subtitle/NaverTVParser.h"
#include "parsers/subtitle/AVParser.h"
#include "utils/RotationUtils.h"
#include "utils/ConvertingUtils.h"
#include "utils/DeviceUtils.h"
#include "utils/MathUtils.h"
#include "utils/PixelUtils.h"
#include "utils/FloatPointUtils.h"

#if !defined Q_OS_MOBILE
# include <QGuiApplication>
# include <QScreen>
#endif

#include <algorithm>
#include <QFontMetrics>
#include <QOpenGLFramebufferObject>

#if defined Q_OS_MOBILE
# include <QOpenGLFunctions>
#endif

extern "C"
{
# include <libavutil/pixdesc.h>
# include <libswscale/swscale.h>
}

using namespace std;

#ifdef Q_OS_WIN
extern PFNGLBINDBUFFERARBPROC glBindBuffer;
extern PFNGLBUFFERDATAARBPROC glBufferData;
extern PFNGLMAPBUFFERARBPROC glMapBuffer;
extern PFNGLUNMAPBUFFERARBPROC glUnmapBuffer;
extern PFNGLACTIVETEXTUREPROC glActiveTexture;
#endif

const int VideoRenderer::DEFAULT_VIRT_SUBTITLE_RATIO = 10;
const int VideoRenderer::DEFAULT_HORI_SUBTITLE_RATIO = 10;

const float VideoRenderer::PERSPECTIVE_FOV_Y_DEFAULT = 60.0f;
const float VideoRenderer::PERSPECTIVE_FOV_Y_MIN = 30.0f;
const float VideoRenderer::PERSPECTIVE_FOV_Y_MAX = 120.0f;
const float VideoRenderer::PERSPECTIVE_ZNEAR = 0.1f;
const float VideoRenderer::PERSPECTIVE_ZFAR = 10.0f;

const QPoint VideoRenderer::DEFAULT_3D_SUBTITLE_OFFSET = QPoint(2, 0);
const QPoint VideoRenderer::DEFAULT_VR_SUBTITLE_OFFSET = QPoint(-4, 0);
const qreal VideoRenderer::DEFAULT_VIRTUAL_3D_DEPTH = 0.15;
const AVPixelFormat VideoRenderer::DEFAULT_PIX_FORMAT = AV_PIX_FMT_BGR32;

#if defined Q_OS_MOBILE
const AVPixelFormat VideoRenderer::DEFAULT_HDR_PIX_FORMAT = AV_PIX_FMT_BGR32;
#else
const AVPixelFormat VideoRenderer::DEFAULT_HDR_PIX_FORMAT = AV_PIX_FMT_RGBA64;
#endif

VideoRenderer::VideoRenderer(MediaPresenterInterface *presenter) :
    m_presenter(presenter),
    m_state(nullptr),
    m_width(0),
    m_height(0),
    m_format(DEFAULT_PIX_FORMAT),
    m_showDetail(false),
    m_subtitleFontSize(0),
    m_showSubtitle(true),
    m_showOptionDesc(false),
    m_showingOptionDesc(false),
    m_fontSize(0),
    m_fontOutlineSize(1),
    m_subtitleOutlineSize(0),
    m_subtitleMaxOutlineSize(0),
    m_texInfo(nullptr),
    m_halign(AnyVODEnums::HAM_NONE),
    m_valign(AnyVODEnums::VAM_NONE),
    m_3dMethod(AnyVODEnums::V3M_NONE),
    m_3dSubtitleMethod(AnyVODEnums::S3M_NONE),
    m_vrInputSource(AnyVODEnums::VRI_NONE),
    m_subtitleOpaque(1.0f),
    m_subtitleSize(1.0f),
    m_usePBO(false),
    m_screenRotationDegree(AnyVODEnums::SRD_NONE),
    m_use3DFull(false),
    m_maxTextureSize(0),
#if defined Q_OS_MOBILE
    m_useGPUConvert(true),
#else
    m_useGPUConvert(false),
#endif
    m_useHDR(false),
    m_hdrTextureSupported(false),
    m_anaglyphFrameBuffer(nullptr),
    m_distortionFrameBuffer(nullptr),
    m_leftDistortionFrameBuffer(nullptr),
    m_rightDistortionFrameBuffer(nullptr),
#if defined Q_OS_MOBILE
    m_gl(nullptr),
#endif
    m_rotation(0.0),
    m_sensorRotation(0.0),
    m_optionDescY(0),
    m_scheduleRecomputeSubtitleSize(false),
    m_useSubtitleCacheMode(false),
    m_devicePixelRatio(1.0),
    m_captureMode(false),
    m_useLowQualityMode(true),
    m_screenOffset(0, 0),
    m_useDistortion(true),
    m_barrelDistortionCoefficients(0.05f, 0.20f),
    m_pincushionDistortionCoefficients(0.075f, -0.005f),
    m_distortionLensCenter(0.0f, 0.0f),
    m_distortionAdjustMode(AnyVODEnums::DAM_NONE),
    m_virtual3DDepth(DEFAULT_VIRTUAL_3D_DEPTH),
    m_fov(PERSPECTIVE_FOV_Y_DEFAULT)
{

}

void VideoRenderer::setup()
{
    this->m_font.getQFont().setFamily(this->m_fontFamily);
    this->m_font.getQFont().setPixelSize(this->m_fontSize);
}

void VideoRenderer::tearDown()
{
    this->m_subtitleFontSize = 0;
    this->m_rotation = 0.0;
    this->m_optionDescY = 0;
    this->m_format = DEFAULT_PIX_FORMAT;
    this->m_scheduleRecomputeSubtitleSize = false;

    this->setSensorRotation(0.0);
    this->scheduleInitTextures();
}

void VideoRenderer::start(int width, int height, MediaState *state)
{
    this->m_state = state;

    this->computeFrameSize();
    this->computeSubtitleSize();

    this->m_context.assFrame.create(width, height, AV_PIX_FMT_BGR32);
}

void VideoRenderer::stop()
{
    this->m_state = nullptr;
    this->m_context.assFrame.destory();
}

void VideoRenderer::setDevicePixelRatio(double ratio)
{
    this->m_devicePixelRatio = ratio;
}

#if defined Q_OS_MOBILE
void VideoRenderer::setGL(QOpenGLFunctions *gl)
{
    this->m_gl = gl;
    this->m_font.setGL(gl);
    this->m_subtitleFont.setGL(gl);
}
#endif

void VideoRenderer::setFontFamily(const QString &family)
{
    this->m_fontFamily = family;
}

QString VideoRenderer::getFontFamily() const
{
    return this->m_fontFamily;
}

void VideoRenderer::setFontSize(int size)
{
    this->m_fontSize = size * this->m_devicePixelRatio;
}

void VideoRenderer::setFontOutlineSize(int size)
{
    this->m_fontOutlineSize = size * this->m_devicePixelRatio;
}

void VideoRenderer::setSubtitleMaxOutlineSize(int size)
{
    this->m_subtitleMaxOutlineSize = size * this->m_devicePixelRatio;
}

void VideoRenderer::setRotation(double rotation)
{
    this->m_rotation = rotation;
}

void VideoRenderer::showDetail(bool show)
{
    this->m_showDetail = show;
}

bool VideoRenderer::isShowDetail() const
{
    return this->m_showDetail;
}

void VideoRenderer::showSubtitle(bool show)
{
    this->m_showSubtitle = show;
}

bool VideoRenderer::isShowSubtitle() const
{
    return this->m_showSubtitle;
}

double VideoRenderer::getRotation() const
{
    return this->m_rotation;
}

void VideoRenderer::setSensorRotation(double rot)
{
    QMutexLocker locker(&this->m_senserLocker);

    this->m_sensorRotation = rot;
}

double VideoRenderer::getSensorRotation()
{
    double rot;

    this->m_senserLocker.lock();
    rot = this->m_sensorRotation;
    this->m_senserLocker.unlock();

    return rot;
}

AnyVODEnums::HAlignMethod VideoRenderer::getHAlign() const
{
    return this->m_halign;
}

void VideoRenderer::setHAlign(AnyVODEnums::HAlignMethod align)
{
    this->m_halign = align;
}

AnyVODEnums::VAlignMethod VideoRenderer::getVAlign() const
{
    return this->m_valign;
}

void VideoRenderer::setVAlign(AnyVODEnums::VAlignMethod align)
{
    this->m_valign = align;
}

void VideoRenderer::resetSubtitlePosition()
{
    this->m_subtitlePosition = QPoint();
}

void VideoRenderer::addVerticalSubtitlePosition(int pos)
{
    this->m_subtitlePosition.ry() += pos;
}

void VideoRenderer::addHorizontalSubtitlePosition(int pos)
{
    this->m_subtitlePosition.rx() += pos;
}

void VideoRenderer::setVerticalSubtitlePosition(int pos)
{
    this->m_subtitlePosition.ry() = pos;
}

void VideoRenderer::setHorizontalSubtitlePosition(int pos)
{
    this->m_subtitlePosition.rx() = pos;
}

int VideoRenderer::getVerticalSubtitlePosition() const
{
    return this->m_subtitlePosition.y();
}

int VideoRenderer::getHorizontalSubtitlePosition() const
{
    return this->m_subtitlePosition.x();
}

void VideoRenderer::reset3DSubtitleOffset()
{
    if (this->m_vrInputSource == AnyVODEnums::VRI_NONE)
        this->m_3dSubtitleOffset = DEFAULT_3D_SUBTITLE_OFFSET;
    else
        this->m_3dSubtitleOffset = DEFAULT_VR_SUBTITLE_OFFSET;
}

void VideoRenderer::addVertical3DSubtitleOffset(int pos)
{
    this->m_3dSubtitleOffset.ry() += pos;
}

void VideoRenderer::addHorizontal3DSubtitleOffset(int pos)
{
    this->m_3dSubtitleOffset.rx() += pos;
}

void VideoRenderer::setVertical3DSubtitleOffset(int pos)
{
    this->m_3dSubtitleOffset.ry() = pos;
}

void VideoRenderer::setHorizontal3DSubtitleOffset(int pos)
{
    this->m_3dSubtitleOffset.rx() = pos;
}

int VideoRenderer::getVertical3DSubtitleOffset() const
{
    return this->m_3dSubtitleOffset.y();
}

int VideoRenderer::getHorizontal3DSubtitleOffset() const
{
    return this->m_3dSubtitleOffset.x();
}

void VideoRenderer::resetVirtual3DDepth()
{
    this->m_virtual3DDepth = DEFAULT_VIRTUAL_3D_DEPTH;
}

void VideoRenderer::setVirtual3DDepth(qreal depth)
{
    this->m_virtual3DDepth = depth;
}

qreal VideoRenderer::getVirtual3DDepth() const
{
    return this->m_virtual3DDepth;
}

void VideoRenderer::setCaptureMode(bool capture)
{
    this->m_captureMode = capture;
}

bool VideoRenderer::getCaptureMode() const
{
    return this->m_captureMode;
}

void VideoRenderer::set3DMethod(AnyVODEnums::Video3DMethod method)
{
    this->m_3dMethod = method;
    this->computeFrameSize();
}

AnyVODEnums::Video3DMethod VideoRenderer::get3DMethod() const
{
    return this->m_3dMethod;
}

void VideoRenderer::setSubtitle3DMethod(AnyVODEnums::Subtitle3DMethod method)
{
    this->m_3dSubtitleMethod = method;
}

AnyVODEnums::Subtitle3DMethod VideoRenderer::getSubtitle3DMethod() const
{
    return this->m_3dSubtitleMethod;
}

void VideoRenderer::setVRInputSource(AnyVODEnums::VRInputSource source)
{
    this->m_vrInputSource = source;

    this->computeFrameSize();
    this->reset3DSubtitleOffset();
}

AnyVODEnums::VRInputSource VideoRenderer::getVRInputSource() const
{
    return this->m_vrInputSource;
}

void VideoRenderer::setDistortionAdjustMode(AnyVODEnums::DistortionAdjustMode mode)
{
    this->m_distortionAdjustMode = mode;
}

AnyVODEnums::DistortionAdjustMode VideoRenderer::getDistortionAdjustMode() const
{
    return this->m_distortionAdjustMode;
}

void VideoRenderer::setSubtitleOpaque(float opaque)
{
    this->m_subtitleOpaque = opaque;
}

float VideoRenderer::getSubtitleOpaque() const
{
    return this->m_subtitleOpaque;
}

void VideoRenderer::setSubtitleSize(float size)
{
    this->m_subtitleSize = size;

    if (!this->m_scheduleRecomputeSubtitleSize)
        this->computeSubtitleSize();
}

float VideoRenderer::getSubtitleSize() const
{
    return this->m_subtitleSize;
}

void VideoRenderer::scheduleRecomputeSubtitleSize()
{
    this->m_scheduleRecomputeSubtitleSize = true;
}

void VideoRenderer::useGPUConvert(bool use)
{
    this->m_useGPUConvert = use;
}

bool VideoRenderer::isUseGPUConvert() const
{
    return this->m_useGPUConvert;
}

void VideoRenderer::useHDR(bool use)
{
    this->m_useHDR = use;
}

bool VideoRenderer::isUseHDR() const
{
    return this->m_useHDR;
}

void VideoRenderer::setHDRTextureSupported(bool supported)
{
    this->m_hdrTextureSupported = supported;
}

bool VideoRenderer::isHDRTextureSupported() const
{
    return this->m_hdrTextureSupported;
}

void VideoRenderer::setFormat(AVPixelFormat format)
{
    this->m_format = format;
}

AVPixelFormat VideoRenderer::getFormat() const
{
    return this->m_format;
}

int VideoRenderer::getOptionDescY() const
{
    return this->m_optionDescY;
}

void VideoRenderer::setOptionDescY(int y)
{
    this->m_optionDescY = y;
}

void VideoRenderer::setVerticalScreenOffset(int offset)
{
    QMutexLocker locker(&this->m_screenOffsetLocker);

    this->m_screenOffset.setY(offset);
}

void VideoRenderer::setHorizontalScreenOffset(int offset)
{
    QMutexLocker locker(&this->m_screenOffsetLocker);

    this->m_screenOffset.setX(offset);
}

void VideoRenderer::useDistortion(bool use)
{
    this->m_useDistortion = use;
}

bool VideoRenderer::isUseDistortion() const
{
    return this->m_useDistortion;
}

void VideoRenderer::setBarrelDistortionCoefficients(const QVector2D &coefficients)
{
    this->m_barrelDistortionCoefficients = coefficients;
}

QVector2D VideoRenderer::getBarrelDistortionCoefficients() const
{
    return this->m_barrelDistortionCoefficients;
}

void VideoRenderer::setPincushionDistortionCoefficients(const QVector2D &coefficients)
{
    this->m_pincushionDistortionCoefficients = coefficients;
}

QVector2D VideoRenderer::getPincushionDistortionCoefficients() const
{
    return this->m_pincushionDistortionCoefficients;
}

void VideoRenderer::setDistortionLensCenter(const QVector2D &lensCenter)
{
    this->m_distortionLensCenter = lensCenter;
}

QVector2D VideoRenderer::getDistortionLensCenter() const
{
    return this->m_distortionLensCenter;
}

void VideoRenderer::resetFOV()
{
    this->m_fov = PERSPECTIVE_FOV_Y_DEFAULT;
}

float VideoRenderer::getFOV() const
{
    return this->m_fov;
}

bool VideoRenderer::addFOV(float value)
{
    float result = this->m_fov + value;

    if (result >= PERSPECTIVE_FOV_Y_MIN && result <= PERSPECTIVE_FOV_Y_MAX)
    {
        this->m_fov = result;
        return true;
    }

    return false;
}

void VideoRenderer::setUserAspectRatio(UserAspectRatioInfo &ratio)
{
    this->m_userRatio = ratio;
    this->computeFrameSize();
}

void VideoRenderer::getUserAspectRatio(UserAspectRatioInfo *ret) const
{
    *ret = this->m_userRatio;
}

void VideoRenderer::setMaxTextureSize(int size)
{
    this->m_maxTextureSize = size;

    this->initFrameBufferObject(&this->m_anaglyphFrameBuffer, size, size);
}

int VideoRenderer::getMaxTextureSize() const
{
    return this->m_maxTextureSize;
}

void VideoRenderer::useSubtitleCacheMode(bool use)
{
    this->m_useSubtitleCacheMode = use;
}

bool VideoRenderer::isUseSubtitleCacheMode() const
{
    return this->m_useSubtitleCacheMode;
}

void VideoRenderer::showOptionDesc(const QString &desc)
{
    QMutexLocker locker(&this->m_optionDescMutex);

    this->m_optionDesc = desc;
    this->m_showOptionDesc = true;
}

bool VideoRenderer::isShowOptionDesc() const
{
    return this->m_showOptionDesc;
}

void VideoRenderer::displayOptionDesc()
{
    this->m_showOptionDesc = false;
    this->m_showingOptionDesc = true;
}

void VideoRenderer::hideOptionDesc()
{
    this->m_showingOptionDesc = false;
}

bool VideoRenderer::isShowingOptionDesc() const
{
    return this->m_showingOptionDesc;
}

void VideoRenderer::clearFonts()
{
    this->m_font.clear();
    this->m_subtitleFont.clear();
}

void VideoRenderer::clearFrameBuffers()
{
    this->destroyFrameBufferObject(&this->m_anaglyphFrameBuffer);
    this->destroyFrameBufferObject(&this->m_distortionFrameBuffer);
    this->destroyFrameBufferObject(&this->m_leftDistortionFrameBuffer);
    this->destroyFrameBufferObject(&this->m_rightDistortionFrameBuffer);
}

void VideoRenderer::useLowQualityMode(bool enable)
{
    this->m_useLowQualityMode = enable;
}

bool VideoRenderer::isUseLowQualityMode() const
{
    return this->m_useLowQualityMode;
}

void VideoRenderer::scheduleInitTextures()
{
    if (this->m_texInfo)
    {
        for (int i = 0; i < TEX_COUNT; i++)
        {
            for (unsigned int j = 0; j < this->m_texInfo[i].textureCount; j++)
                this->m_texInfo[i].init[j] = false;
        }
    }
}

void VideoRenderer::processSubtitleResize()
{
    if (this->m_scheduleRecomputeSubtitleSize)
    {
        this->computeSubtitleSize();
        this->m_scheduleRecomputeSubtitleSize = false;
    }
}

void VideoRenderer::selectPixelFormat(AVPixelFormat format)
{
    if (this->isUseGPUConvert(format))
        this->m_format = format;
    else
        this->m_format = this->getCompatibleFormat(format);
}

void VideoRenderer::selectDefaultPixelFormat()
{
    this->m_format = this->getDefaultFormat();
}

void VideoRenderer::setRestartVideoCallback(RestartVideoCallback &callback)
{
    this->m_restartVideoCallback = callback;
}

QSize VideoRenderer::getSize() const
{
    return QSize(this->m_width, this->m_height);
}

bool VideoRenderer::resetScreen(const int width, const int height, TextureInfo *texInfo, bool inContext)
{
    this->m_width = width;
    this->m_height = height;
    this->m_texInfo = texInfo;

    this->m_ortho.setToIdentity();
    this->m_ortho.ortho(0.0f, (float)width, (float)height, 0.0f, -1.0f, 1.0f);

    this->m_font.setOrtho(this->m_ortho);
    this->m_subtitleFont.setOrtho(this->m_ortho);

    if (inContext)
    {
        this->initFrameBufferObject(&this->m_distortionFrameBuffer, width, height);
        this->initFrameBufferObject(&this->m_leftDistortionFrameBuffer, width, height);
        this->initFrameBufferObject(&this->m_rightDistortionFrameBuffer, width, height);
    }

    this->scheduleInitTextures();

    if (this->m_state && this->m_state->video.stream.stream)
    {
        this->computeFrameSize();
        return true;
    }

    return false;
}

void VideoRenderer::initFrameBufferObject(QOpenGLFramebufferObject **object, int width, int height)
{
    if (*object)
    {
        if ((*object)->width() != width || (*object)->height() != height)
            delete *object;
        else
            return;
    }

    QOpenGLFramebufferObjectFormat format;

#ifndef Q_OS_MOBILE
    if (this->m_useHDR)
        format.setInternalTextureFormat(GL_RGBA16);
#endif

    *object = new QOpenGLFramebufferObject(width, height, format);

    GL_PREFIX glBindTexture(GL_TEXTURE_2D, (*object)->texture());

    GL_PREFIX glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    GL_PREFIX glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    GL_PREFIX glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    GL_PREFIX glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    GL_PREFIX glBindTexture(GL_TEXTURE_2D, 0);

    (*object)->release();
}

void VideoRenderer::destroyFrameBufferObject(QOpenGLFramebufferObject **object)
{
    if (*object)
    {
        delete *object;
        *object = nullptr;
    }
}

void VideoRenderer::checkVideoFrameSize(const VideoPicture &vp)
{
    const QSize &surfaceSize = vp.getSurface().getSize();

    if (!this->m_videoPictureSize.isValid())
    {
        this->m_videoPictureSize = surfaceSize;
    }
    else if (this->m_restartVideoCallback.callback && this->m_videoPictureSize != surfaceSize)
    {
        this->m_videoPictureSize = surfaceSize;

        this->m_restartVideoCallback.callback(this->m_restartVideoCallback.userData, surfaceSize);
        this->scheduleInitTextures();
    }
}

void VideoRenderer::computeFrameSize()
{
    MediaState *ms = this->m_state;

    if (ms)
    {
        FrameSize &frameSize = ms->frameSize;
        AVCodecContext *codec = ms->video.stream.ctx;
        int orgHeight = this->m_height;
        int orgWidth = this->m_width;
        int codecHeight = codec->height;
        int codecWidth = codec->width;
        int height = 0;
        int width = 0;
        double rot = RotationUtils::convertSRDtoNumeric(this->m_screenRotationDegree);

        if (RotationUtils::isPortrait(this->m_rotation + rot))
            std::swap(orgWidth, orgHeight);

        if (this->m_userRatio.use && this->m_userRatio.fullscreen)
        {
            height = orgHeight;
            width = orgWidth;
        }
        else
        {
            double aspectRatio = 0.0;
            float widthScale = 1.0f;
            float heightScale = 1.0f;

            if (this->m_use3DFull && this->m_3dMethod != AnyVODEnums::V3M_NONE)
            {
                if (this->isSideBySide())
                {
                    widthScale = 0.5f;
                    heightScale = 1.0f;
                }
                else
                {
                    widthScale = 1.0f;
                    heightScale = 0.5f;
                }
            }

            if (this->m_vrInputSource != AnyVODEnums::VRI_NONE)
                widthScale *= 2.0f;

            if (this->m_userRatio.use)
                aspectRatio = this->m_userRatio.getRatio();
            else if (codec->sample_aspect_ratio.num == 0)
                aspectRatio = 0.0;
            else
                aspectRatio = av_q2d(codec->sample_aspect_ratio) * this->getAspectRatioByVR(widthScale, codecWidth, heightScale, codecHeight);

            if (aspectRatio <= 0.0)
                aspectRatio = this->getAspectRatioByVR(widthScale, codecWidth, heightScale, codecHeight);

            if (this->m_vrInputSource == AnyVODEnums::VRI_NONE)
            {
                height = orgHeight;
                width = ((int)rint(height * aspectRatio)) & -3;

                if (width > orgWidth)
                {
                    width = orgWidth;
                    height = ((int)rint(width / aspectRatio)) & -3;
                }
            }
            else
            {
                width = orgWidth;
                height = ((int)rint(width * aspectRatio)) & -3;

                if (height > orgHeight)
                {
                    height = orgHeight;
                    width = ((int)rint(height / aspectRatio)) & -3;
                }
            }
        }

        frameSize.height = height;
        frameSize.width = width;

        this->m_presenter->getRDetail().videoOutputSize = QSize(frameSize.width, frameSize.height);

#if defined Q_OS_MOBILE
        if (!this->m_captureMode &&
                this->m_vrInputSource == AnyVODEnums::VRI_NONE &&
                this->m_useSubtitleCacheMode &&
                this->m_font.willBeInvalidate(this->m_fontOutlineSize))
        {
            bool isPaused = ms->pause.pause;

            if (!isPaused)
                this->m_presenter->pause();

            this->m_font.setCacheMode(true);
            this->drawDetail(VideoPicture());
            this->m_font.setCacheMode(false);

            if (!isPaused)
                this->m_presenter->resume();
        }
#endif

        if (!this->m_scheduleRecomputeSubtitleSize)
            this->computeSubtitleSize();
    }
}

void VideoRenderer::computeSubtitleSize()
{
    MediaState *ms = this->m_state;

    if (ms)
    {
        int height = ms->frameSize.height;
        int prevSize = this->m_subtitleFontSize;

        this->m_subtitleFontSize = height * 0.064f * this->m_subtitleSize;

        if (this->m_subtitleFontSize > 0 && this->m_subtitleFontSize != prevSize)
        {
            int fontRatio;

#if defined Q_OS_MOBILE
            fontRatio = 24;

            if (this->m_vrInputSource != AnyVODEnums::VRI_NONE)
                this->m_subtitleFontSize *= 2;
#else
            fontRatio = 12;
#endif
            this->m_subtitleFont.getQFont().setFamily(this->m_fontFamily);
            this->m_subtitleFont.getQFont().setPixelSize(this->m_subtitleFontSize);

            this->m_subtitleOutlineSize = this->m_subtitleFontSize / fontRatio;

            if (this->m_subtitleOutlineSize <= 0)
                this->m_subtitleOutlineSize = 1 * this->m_devicePixelRatio;
            else if (this->m_subtitleOutlineSize > this->m_subtitleMaxOutlineSize)
                this->m_subtitleOutlineSize = this->m_subtitleMaxOutlineSize;

#if defined Q_OS_MOBILE
            uint32_t duration = this->m_presenter->getDuration() * KILO / 3;

            if (!this->m_captureMode &&
                    this->m_vrInputSource == AnyVODEnums::VRI_NONE &&
                    this->m_useSubtitleCacheMode && duration > 0 && this->m_presenter->existSubtitle() &&
                    this->m_subtitleFont.willBeInvalidate(this->m_subtitleOutlineSize))
            {
                VideoPicture dummy;
                const int gap = 500;
                bool isPaused = ms->pause.pause;

                if (!isPaused)
                    this->m_presenter->pause();

                this->m_subtitleFont.setCacheMode(true);

                for (dummy.setTime(0); dummy.getTime() < duration; dummy.addTime(gap))
                    this->drawSubtitles(dummy);

                this->m_subtitleFont.setCacheMode(false);

                if (!isPaused)
                    this->m_presenter->resume();
            }
#endif
        }
    }
}


double VideoRenderer::getAspectRatioByVR(float widthScale, int codecWidth, float heightScale, int codecHeight) const
{
    if (this->m_vrInputSource == AnyVODEnums::VRI_NONE)
        return ((double)codecWidth * widthScale) / ((double)codecHeight * heightScale);
    else
        return ((double)codecHeight * heightScale) / ((double)codecWidth * widthScale);
}

bool VideoRenderer::isSideBySide() const
{
    switch (this->m_3dMethod)
    {
        case AnyVODEnums::V3M_HALF_LEFT:
        case AnyVODEnums::V3M_HALF_RIGHT:
        case AnyVODEnums::V3M_FULL_LEFT_RIGHT:
        case AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_ROW_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_ROW_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_COL_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_COL_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_RIGHT_PRIOR:
            return true;
        default:
            return false;
    }
}

void VideoRenderer::drawDetail(const VideoPicture &vp)
{
    ShaderCompositer &shader = ShaderCompositer::getInstance();
    Detail &detail = this->m_presenter->getRDetail();
    const DTVReader &dtvReader = DTVReader::getInstance();
    QString text;
    QString currentTime;
    QString totalTime;
    QString timeFormat = ConvertingUtils::TIME_HH_MM_SS;
    QColor headerColor(255, 255, 255);
    QColor color(255, 173, 114);
    int initX = 10 * this->m_devicePixelRatio;
    int x = initX;
    int y = 10 * this->m_devicePixelRatio;
    QString subtitle;
    float opaque = 1.0f;
    int lineHeight = this->m_fontSize + (5 * this->m_devicePixelRatio);
    int optionDescYGap = 10;

    if (this->m_vrInputSource != AnyVODEnums::VRI_NONE)
    {
        x /= 2;
        y /= 2;

        initX /= 2;
        lineHeight /= 2;
        optionDescYGap /= 2;
    }

    if (this->m_presenter->existSubtitle())
    {
        if (this->m_presenter->isAudio())
            subtitle = tr("가사 있음");
        else
            subtitle = tr("자막 있음");
    }
    else
    {
        if (this->m_presenter->isAudio())
            subtitle = tr("가사 없음");
        else
            subtitle = tr("자막 없음");
    }

    if (this->m_showingOptionDesc && !this->m_captureMode)
        y += this->m_fontSize + ((this->m_optionDescY + optionDescYGap) * this->m_devicePixelRatio);

    text = tr("파일 이름 : ");
    x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, headerColor, opaque, vp);
    text = detail.fileName;
    x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
    text = QString(" (%1)").arg(subtitle);
    this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
    y += lineHeight;
    x = initX;

    bool showPlayingPos = this->m_presenter->getDuration() > 0.0;

#if !defined Q_OS_ANDROID && !defined Q_OS_IOS
    showPlayingPos &= !this->m_presenter->isDevice();
#endif

    if (showPlayingPos)
    {
        text = tr("재생 위치 : ");
        x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, headerColor, opaque, vp);
        text = QString("%1 / %2 (%3%)")
                .arg(*ConvertingUtils::getTimeString(detail.currentTime, timeFormat, &currentTime),
                     *ConvertingUtils::getTimeString(detail.totalTime, timeFormat, &totalTime))
                .arg(detail.timePercentage, 0, 'f', 2);
        this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
        y += lineHeight;
        y += lineHeight;
        x = initX;
    }
    else
    {
        y += lineHeight;
    }

    text = tr("파일 포맷 : ");
    x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, headerColor, opaque, vp);
    text = detail.fileFormat;
    x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
    y += lineHeight;
    x = initX;

    text = tr("CPU 사용률 : ");
    x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, headerColor, opaque, vp);

    if (detail.cpuUsage >= 0.0f)
        text = QString("%1%").arg(detail.cpuUsage, 0, 'f', 2);
    else
        text = "N/A";

    this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
    y += lineHeight;
    x = initX;

    text = tr("메모리 사용량 : ");
    x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, headerColor, opaque, vp);
    text = QString("%1B").arg(ConvertingUtils::sizeToString(detail.usingMemSize));
    this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
    y += lineHeight;
    x = initX;

    if (dtvReader.isOpened())
    {
        if (detail.dtvSignal)
        {
            detail.dtvSignalStrength = dtvReader.getSignalStrength();
            detail.dtvSignal = false;
        }

        text = tr("DTV 신호 감도 : ");
        x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, headerColor, opaque, vp);
        text = QString("%1%").arg(detail.dtvSignalStrength, 0, 'f', 2);
        this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
        y += lineHeight;
        x = initX;
    }

    y += lineHeight;

    if (this->m_presenter->isEnabledVideo())
    {
        AVCodecContext *ctx = this->m_state->video.stream.ctx;

        text = tr("비디오 코덱 : ");
        x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, headerColor, opaque, vp);
        text = detail.videoCodec;
        x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
        text = QString(" (Type %1, Threads : %2)").arg(ctx->active_thread_type).arg(ctx->thread_count);
        this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
        y += lineHeight;
        x = initX;

        if (this->m_presenter->getHWDecoder().isOpened())
        {
            text = tr("하드웨어 디코더 : ");
            x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, headerColor, opaque, vp);
            text = detail.videoHWDecoder;
            this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
            y += lineHeight;
            x = initX;
        }

        text = tr("입력 : ");
        x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, headerColor, opaque, vp);
        text = QString("%1 (%2bits), %3x%4%5, %L6kbps (%L7KiBps / %L8KiB)")
                .arg(detail.videoInputType)
                .arg(detail.videoInputBits)
                .arg(detail.videoInputSize.width())
                .arg(detail.videoInputSize.height())
                .arg(detail.videoInterlaced ? "i" : "p")
                .arg(detail.videoInputByteRate / 1000.0 * 8.0, 0, 'f', 2)
                .arg(detail.videoInputByteRate / 1024.0, 0, 'f', 2)
                .arg(this->m_state->video.stream.queue.getBufferSizeInByte() / 1024.0, 0, 'f', 2);
        this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
        y += lineHeight;
        x = initX;

        text = tr("출력 : ");
        x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, headerColor, opaque, vp);
        text = QString("%1%2%3 (%4bits), %5x%6%7, %8, %L9kbps (%L10KiBps)")
                .arg(detail.videoOutputType,
                     this->isUsingPBO() ? " PBO" : "",
                     this->m_useHDR ? " HDR" : "")
                .arg(detail.videoOutputBits)
                .arg(detail.videoOutputSize.width())
                .arg(detail.videoOutputSize.height())
                .arg(detail.videoInterlaced && !detail.videoDeinterlaced ? "i" : "p")
                .arg(detail.videoFPS, 0, 'f', 2)
                .arg(detail.videoOutputByteRate / 1000.0 * 8.0, 0, 'f', 2)
                .arg(detail.videoOutputByteRate / 1024.0, 0, 'f', 2);
        this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
        y += lineHeight;
        x = initX;

        if (shader.isUsingColorConversion())
        {
            text = tr("색상 변환 : ");
            x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, headerColor, opaque, vp);
            text = "BT2020 -> BT709";
            x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
            y += lineHeight;
            x = initX;
        }

#if !defined Q_OS_ANDROID && !defined Q_OS_IOS
        if (this->m_presenter->isDevice())
        {
            if (this->m_presenter->isUseFrameDrop())
            {
                text = tr("프레임 : ");
                x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, headerColor, opaque, vp);
                text = QString("%1 drops").arg(detail.videoFrameDropCount.fetchAndAddOrdered(0));
                x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
                y += lineHeight;
                x = initX;
            }
        }
        else
#endif
        {
            text = tr("프레임 : ");
            x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, headerColor, opaque, vp);
            text = QString("%1 / %2")
                    .arg(detail.videoCurrentFrame.fetchAndAddOrdered(0))
                    .arg(detail.videoTotalFrame);
            x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);

            if (this->m_presenter->isUseFrameDrop())
            {
                text = QString(" (%1 drops)").arg(detail.videoFrameDropCount.fetchAndAddOrdered(0));
                x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
            }

            y += lineHeight;
            x = initX;
        }

        y += lineHeight;
    }

    if (this->m_presenter->getCurrentAudioStreamIndex() != -1)
    {
        text = tr("오디오 코덱 : ");
        x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, headerColor, opaque, vp);
        text = detail.audioCodec;
        x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
        text = QString(" (Type %1)").arg(this->m_state->audio.stream.ctx->active_thread_type);
        this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
        y += lineHeight;
        x = initX;

        if (SPDIF::getInstance().isOpened())
        {
            text = tr("S/PDIF 오디오 장치 : ");
            x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, headerColor, opaque, vp);
            text = detail.audioSPDIFOutputDevice;
            x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);

            if (this->m_presenter->isUsingSPDIFEncoding())
            {
                text = ", " + tr("인코딩 사용");
                x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);

                QString encoding = " (%1)";

                switch (this->m_presenter->getSPDIFEncodingMethod())
                {
                    case AnyVODEnums::SEM_AC3:
                        encoding = encoding.arg("AC3");
                        break;
                    case AnyVODEnums::SEM_DTS:
                        encoding = encoding.arg("DTS");
                        break;
                    default:
                        encoding = encoding.arg("Unknown");
                        break;
                }

                text = encoding;
                x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
            }

            y += lineHeight;
            x = initX;
        }

        text = tr("입력 : ");
        x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, headerColor, opaque, vp);
        text = QString("%1, %2Hz, %3Ch, %4bits, %L5kbps (%L6KiBps / %L7KiB)")
                .arg(detail.audioInputType)
                .arg(detail.audioInputSampleRate)
                .arg(detail.audioInputChannels)
                .arg(detail.audioInputBits)
                .arg(detail.audioInputByteRate / 1000.0 * 8.0, 0, 'f', 2)
                .arg(detail.audioInputByteRate / 1024.0, 0, 'f', 2)
                .arg(this->m_state->audio.stream.queue.getBufferSizeInByte() / 1024.0, 0, 'f', 2);
        this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
        y += lineHeight;
        x = initX;

        text = tr("출력 : ");
        x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, headerColor, opaque, vp);
        text = QString("%1, %2Hz, %3Ch, %4bits, %L5kbps (%L6KiBps)")
                .arg(detail.audioOutputType)
                .arg(detail.audioOutputSampleRate)
                .arg(detail.audioOutputChannels)
                .arg(detail.audioOutputBits)
                .arg(detail.audioOutputByteRate / 1000.0 * 8.0, 0, 'f', 2)
                .arg(detail.audioOutputByteRate / 1024.0, 0, 'f', 2);
        this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
        y += lineHeight;
        y += lineHeight;
        x = initX;
    }

    if (this->m_presenter->existSubtitle() && this->m_presenter->isValidSubtitleWithoutClass())
    {
        if (this->m_presenter->isAudio())
            text = tr("가사 코덱 : ");
        else
            text = tr("자막 코덱 : ");

        x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, headerColor, opaque, vp);
        text = detail.subtitleCodec;
        x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);

        if (this->m_presenter->getASSParser().isExist())
        {
            QString desc;

            if (this->m_presenter->getAVParser().getDesc(&desc))
            {
                text = QString(" (%1)").arg(desc);
                x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
            }
        }
        else if (this->m_presenter->getYoutubeParser().isExist() || this->m_presenter->getNaverTVParser().isExist())
        {
            QString lang;

            if (this->m_presenter->getYoutubeParser().isExist())
                this->m_presenter->getYoutubeParser().getDefaultLanguage(&lang);
            else if (this->m_presenter->getNaverTVParser().isExist())
                this->m_presenter->getNaverTVParser().getDefaultLanguage(&lang);

            text = QString(" (%1)").arg(lang);
            x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
        }

        if (detail.subtitleBitmap)
        {
            if (!detail.subtitleValidColor)
            {
                text = QString(" Invalid");
                x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
            }

            text = QString(" Colors (%1)").arg(detail.subtitleColorCount);
            x += this->drawOutlined(this->m_font, QPoint(x, y), text, this->m_fontOutlineSize, color, opaque, vp);
        }
    }
}

void VideoRenderer::setScreenRotationDegree(AnyVODEnums::ScreenRotationDegree degree)
{
    this->m_screenRotationDegree = degree;
    this->computeFrameSize();
}

AnyVODEnums::ScreenRotationDegree VideoRenderer::getScreenRotationDegree() const
{
    return this->m_screenRotationDegree;
}

void VideoRenderer::use3DFull(bool enable)
{
    this->m_use3DFull = enable;
    this->computeFrameSize();
}

bool VideoRenderer::isUse3DFull() const
{
    return this->m_use3DFull;
}

void VideoRenderer::usePBO(bool enable)
{
    this->m_usePBO = enable;
}

bool VideoRenderer::isUsePBO() const
{
    return this->m_usePBO;
}

bool VideoRenderer::isUsingPBO() const
{
    return this->isUsePBO() && this->isUsablePBO();
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Waddress"

bool VideoRenderer::isUsablePBO() const
{
#if defined Q_OS_MOBILE
    return false;
#else
    return (glBindBuffer != nullptr) && (glBufferData != nullptr) && (glMapBuffer != nullptr) && (glUnmapBuffer != nullptr);
#endif
}

#pragma GCC diagnostic pop

void VideoRenderer::drawSubtitles(const VideoPicture &vp)
{
    int maxWidth = 0;
    bool forcedLeft = false;
    QSize size;
    QFontMetrics fm(this->m_subtitleFont.getQFont());
    int32_t time = vp.getTime();
    int32_t delay = this->m_state->frameTimer.lastDelay * 1000;

    if (this->m_presenter->getHWDecoder().isOpened())
        time -= this->m_presenter->getHWDecoder().getSurfaceQueueCount() * delay;

    if (this->m_presenter->getFilterGraph().hasFilters())
        time -= this->m_presenter->getFilterGraph().getDelayCount() * delay;

    if (this->m_presenter->getSAMIParser().isExist())
    {
        SAMIParser::Paragraph para;
        QString className;

        this->m_presenter->getSAMIParser().getDefaultClassName(&className);

        if (this->m_presenter->getSAMIParser().get(className, time, &para))
        {
            this->applyWordWrap(fm, &para);

            if (this->needMaxWidth())
            {
                for (int i = 0; i < para.lines.count(); i++)
                {
                    SAMIParser::Line &line = para.lines[i];
                    QString totalText;

                    for (int j = 0; j < line.subtitles.count(); j++)
                        totalText += line.subtitles[j].text;

                    this->getSubtitleSize(fm, totalText, &size);
                    maxWidth = max(maxWidth, size.width());

                    forcedLeft |= this->isLeftAlignLine(totalText);
                }
            }

            for (int i = 0; i < para.lines.count(); i++)
            {
                SAMIParser::Line &line = para.lines[i];
                int left = 0;
                QString totalText;

                for (int j = 0; j < line.subtitles.count(); j++)
                    totalText += line.subtitles[j].text;

                for (int j = 0; j < line.subtitles.count(); j++)
                {
                    SAMIParser::Text &text = line.subtitles[j];
                    Font::Context context(text.color, text.bold, text.underline, text.italic, text.strike);

                    left += this->drawSubtitleLine(i, para.lines.count(), text.text, QPoint(left, 0),
                                                   context, totalText, maxWidth, forcedLeft, AnyVODEnums::HAM_NONE, AnyVODEnums::VAM_NONE, vp);
                }
            }
        }
    }
    else if (this->m_presenter->getASSParser().isExist())
    {
        this->drawASS(ASSRM_FILE, time, vp);
    }
    else if (this->m_presenter->getSRTParser().isExist() || this->m_presenter->getYoutubeParser().isExist() ||
             this->m_presenter->getNaverTVParser().isExist())
    {
        QVector<SRTParser::Item> items;
        bool exist = false;

        if (this->m_presenter->getSRTParser().isExist())
            exist = this->m_presenter->getSRTParser().get(time, &items);
        else if (this->m_presenter->getYoutubeParser().isExist())
            exist = this->m_presenter->getYoutubeParser().get(time, &items);
        else if (this->m_presenter->getNaverTVParser().isExist())
            exist = this->m_presenter->getNaverTVParser().get(time, &items);

        if (exist)
        {
            for (const SRTParser::Item &item : qAsConst(items))
            {
                SAMIParser::Paragraph paragraph = item.paragraph;

                this->applyWordWrap(fm, &paragraph);

                if (this->needMaxWidth())
                {
                    for (int i = 0; i < paragraph.lines.count(); i++)
                    {
                        SAMIParser::Line &line = paragraph.lines[i];
                        QString totalText;

                        for (int j = 0; j < line.subtitles.count(); j++)
                            totalText += line.subtitles[j].text;

                        this->getSubtitleSize(fm, totalText, &size);
                        maxWidth = max(maxWidth, size.width());

                        forcedLeft |= this->isLeftAlignLine(totalText);
                    }
                }

                for (int i = 0; i < paragraph.lines.count(); i++)
                {
                    SAMIParser::Line &line = paragraph.lines[i];
                    int left = 0;
                    QString totalText;

                    for (int j = 0; j < line.subtitles.count(); j++)
                        totalText += line.subtitles[j].text;

                    for (int j = 0; j < line.subtitles.count(); j++)
                    {
                        SAMIParser::Text &text = line.subtitles[j];
                        Font::Context context(text.color, text.bold, text.underline, text.italic, text.strike);

                        left += this->drawSubtitleLine(i, paragraph.lines.count(), text.text, QPoint(left, 0),
                                                       context, totalText, maxWidth, forcedLeft, item.halign, item.valign, vp);
                    }
                }
            }
        }
    }
    else if (this->m_presenter->getAVParser().isExist())
    {
        AVSubtitle *sp;

        if (this->m_presenter->getAVParser().get(time, &sp))
        {
            QSize size;

            this->m_presenter->getAVParser().getSize(&size);

            if (this->drawAVSubtitle(fm, size, sp, vp))
                this->drawASS(ASSRM_AV, time, vp);
        }
    }
    else
    {
        MediaState *ms = this->m_state;
        Subtitle &subtitle = ms->subtitle;

        if (subtitle.stream.stream == nullptr)
            return;

        if (subtitle.isASS)
        {
            this->drawASS(ASSRM_SINGLE, time, vp);
        }
        else
        {
            SubtitleFrames &frames = ms->subtitleFrames;
            QMutexLocker locker(&frames.lock);
            int count = frames.items.count();

            if (count <= 0)
                return;

            AVSubtitle *sp = nullptr;

            for (int i = count - 1; i >= 0; i--)
            {
                SubtitleElement &item = frames.items[i];
                AVSubtitle &spItem = item.subtitle;
                uint64_t base = (uint64_t)(item.pts * 1000);

                if (base + spItem.start_display_time <= (uint64_t)time && (uint64_t)time <= base + spItem.end_display_time)
                {
                    sp = &spItem;
                    break;
                }
            }

            if (sp)
            {
                AVCodecContext *ctx = subtitle.stream.ctx;

                this->drawAVSubtitle(fm, QSize(ctx->width, ctx->height), sp, vp);
            }
        }
    }
}

int VideoRenderer::drawOutlined(Font &font, const QPoint &pos, const QString &text,
                                 int outline, const Font::Context &context, float opaque, const VideoPicture &vp)
{
    ShaderCompositer &shader = ShaderCompositer::getInstance();
    QColor outlineColor(0, 0, 0);
    int len;

    if (this->m_3dSubtitleMethod == AnyVODEnums::S3M_NONE)
    {
        shader.startSimple();
        len = font.renderText(ShaderCompositer::ST_SIMPLE, pos, text, context, outline, outlineColor, opaque, AnyVODEnums::SD_NONE, 1.0f, false);
        shader.endSimple();
    }
    else
    {
        QPoint firstPos;
        QPoint secondPos;
        AnyVODEnums::ScaleDirection direction;
        QRect frameRect;
        bool colorBlend = false;
        float scale = 0.5f;
        Font::Context firstContext = context;
        Font::Context secondContext = context;

        this->getPictureRect(&frameRect);

        switch (this->m_3dSubtitleMethod)
        {
            case AnyVODEnums::S3M_TOP_BOTTOM:
            {
                firstPos.setX(pos.x());
                firstPos.setY(pos.y() / 2 + frameRect.y());

                secondPos.setX(pos.x());
                secondPos.setY(frameRect.y() + (pos.y() + frameRect.height()) / 2);

                direction = AnyVODEnums::SD_HEIGHT;

                break;
            }
            case AnyVODEnums::S3M_LEFT_RIGHT:
            {
                firstPos.setX(pos.x() / 2 + frameRect.x());
                firstPos.setY(pos.y() + frameRect.y());

                secondPos.setX(frameRect.x() + (pos.x() + frameRect.width()) / 2);
                secondPos.setY(firstPos.y());

                direction = AnyVODEnums::SD_WIDTH;

                break;
            }
            case AnyVODEnums::S3M_PAGE_FLIP:
            {
                firstPos = pos;
                secondPos = pos;

                direction = AnyVODEnums::SD_HEIGHT;
                scale = 1.0f;

                break;
            }
            case AnyVODEnums::S3M_INTERLACED:
            case AnyVODEnums::S3M_CHECKER_BOARD:
            {
                firstPos = pos;
                secondPos = pos;

                direction = AnyVODEnums::SD_HEIGHT;
                scale = 1.0f;

                break;
            }
            case AnyVODEnums::S3M_ANAGLYPH:
            {
                firstPos = pos;
                secondPos = pos;

                direction = AnyVODEnums::SD_HEIGHT;
                colorBlend = true;
                scale = 1.0f;

                break;
            }
            default:
            {
                direction = AnyVODEnums::SD_NONE;
                break;
            }
        }

        if (this->m_vrInputSource != AnyVODEnums::VRI_NONE)
        {
            scale = 0.5f;
            direction = AnyVODEnums::SD_ALL;
        }

        firstPos -= this->m_3dSubtitleOffset * this->m_devicePixelRatio;
        secondPos += this->m_3dSubtitleOffset * this->m_devicePixelRatio;

        switch (this->m_3dSubtitleMethod)
        {
            case AnyVODEnums::S3M_PAGE_FLIP:
            {
                shader.startSimple();

                if (vp.isLeftOrTop())
                    len = font.renderText(ShaderCompositer::ST_SIMPLE, firstPos, text, firstContext, outline, outlineColor, opaque, direction, scale, colorBlend);
                else
                    len = font.renderText(ShaderCompositer::ST_SIMPLE, secondPos, text, secondContext, outline, outlineColor, opaque, direction, scale, colorBlend);

                shader.endSimple();

                break;
            }
            case AnyVODEnums::S3M_INTERLACED:
            {
                shader.startSubtitleInterlace(shader.getLeftOrTop());
                font.renderText(ShaderCompositer::ST_SUBTITLE_INTERLACE, firstPos, text, firstContext, outline, outlineColor, opaque, direction, scale, colorBlend);
                shader.endSubtitleInterlace();

                shader.startSubtitleInterlace(!shader.getLeftOrTop());
                len = font.renderText(ShaderCompositer::ST_SUBTITLE_INTERLACE, secondPos, text, secondContext, outline, outlineColor, opaque, direction, scale, colorBlend);
                shader.endSubtitleInterlace();

                break;
            }
            case AnyVODEnums::S3M_CHECKER_BOARD:
            {
                shader.startSubtitleCheckerBoard(shader.getLeftOrTop());
                font.renderText(ShaderCompositer::ST_SUBTITLE_CHECKER_BOARD, firstPos, text, firstContext, outline, outlineColor, opaque, direction, scale, colorBlend);
                shader.endSubtitleCheckerBoard();

                shader.startSubtitleCheckerBoard(!shader.getLeftOrTop());
                len = font.renderText(ShaderCompositer::ST_SUBTITLE_CHECKER_BOARD, secondPos, text, secondContext, outline, outlineColor, opaque, direction, scale, colorBlend);
                shader.endSubtitleCheckerBoard();

                break;
            }
            case AnyVODEnums::S3M_ANAGLYPH:
            {
                shader.startSubtitleAnaglyph(shader.getLeftOrTop());
                font.renderText(ShaderCompositer::ST_SUBTITLE_ANAGLYPH, firstPos, text, firstContext, outline, outlineColor, opaque, direction, scale, colorBlend);
                shader.endSubtitleAnaglyph();

                shader.startSubtitleAnaglyph(!shader.getLeftOrTop());
                len = font.renderText(ShaderCompositer::ST_SUBTITLE_ANAGLYPH, secondPos, text, secondContext, outline, outlineColor, opaque, direction, scale, colorBlend);
                shader.endSubtitleAnaglyph();

                break;
            }
            default:
            {
                shader.startSimple();
                font.renderText(ShaderCompositer::ST_SIMPLE, firstPos, text, firstContext, outline, outlineColor, opaque, direction, scale, colorBlend);
                len = font.renderText(ShaderCompositer::ST_SIMPLE, secondPos, text, secondContext, outline, outlineColor, opaque, direction, scale, colorBlend);
                shader.endSimple();

                break;
            }
        }
    }

    return len;
}

void VideoRenderer::getSubtitleSize(const QFontMetrics &fm, const QString &text, QSize *ret) const
{
    int heightDiv = 1;

    if (this->m_vrInputSource != AnyVODEnums::VRI_NONE)
        heightDiv = 2;

    ret->setWidth(fm.horizontalAdvance(text));
    ret->setHeight(fm.height() / heightDiv);
}

bool VideoRenderer::isLeftAlignLine(const QString &text) const
{
    if (this->m_halign != AnyVODEnums::HAM_AUTO)
        return false;

    QString tmp = text.trimmed();

    if (tmp.startsWith(0x200E))
        tmp = tmp.mid(1);

    if (tmp.startsWith("-"))
        return true;
    else
        return false;
}

int VideoRenderer::findWordWrapPos(const QFontMetrics &fm, const QString &text) const
{
    QSize len;
    int pos = -1;
    int left = 0;
    int right = text.count() - 1;
    int mid = right / 2;
    int maxWidth = this->m_width - fm.maxWidth();

    while (true)
    {
        if (left < right - 1)
        {
            QString sub = text.mid(0, mid);

            this->getSubtitleSize(fm, sub, &len);

            if (len.width() >= maxWidth)
                right = mid;
            else
                left = mid;

            mid = (left + right) / 2;
            continue;
        }

        bool found = false;

        for (int k = mid; k >= 0; k--)
        {
            if (text[k].isSpace())
            {
                pos = k + 1;
                found = true;
                break;
            }
        }

        if (found)
            break;

        pos = mid;
        break;
    }

    return pos;
}

void VideoRenderer::applyWordWrap(const QFontMetrics &fm, SAMIParser::Paragraph *ret) const
{
    int maxWidth = this->m_width - fm.maxWidth();

    for (int i = 0; i < ret->lines.count(); i++)
    {
        SAMIParser::Line &line = ret->lines[i];
        QString totalText;
        QSize len;

        for (int j = 0; j < line.subtitles.count(); j++)
            totalText += line.subtitles[j].text;

        if (totalText.isEmpty())
            continue;

        this->getSubtitleSize(fm, totalText, &len);

        if (len.width() < maxWidth)
            continue;

        int pos = this->findWordWrapPos(fm, totalText);
        int sum = 0;
        int prevSum = 0;
        int toMove = -1;

        if (pos <= 0)
            continue;

        for (int j = 0; j < line.subtitles.count(); j++)
        {
            int count = line.subtitles[j].text.count();

            sum += count;

            if (sum >= pos)
            {
                toMove = j;
                break;
            }

            prevSum = sum;
        }

        SAMIParser::Line newLine;

        for (int j = toMove; j < line.subtitles.count(); j++)
            newLine.subtitles.append(line.subtitles[j]);

        line.subtitles = line.subtitles.mid(0, toMove + 1);

        SAMIParser::Text &oldText = line.subtitles.last();
        SAMIParser::Text &newText = newLine.subtitles.first();

        if (prevSum <= pos)
            pos -= prevSum;

        oldText.text = oldText.text.mid(0, pos).trimmed();
        newText.text = newText.text.mid(pos).trimmed();

        ret->lines.insert(i + 1, newLine);
    }
}

bool VideoRenderer::needMaxWidth() const
{
    return this->m_halign != AnyVODEnums::HAM_MIDDLE && this->m_halign != AnyVODEnums::HAM_NONE;
}

bool VideoRenderer::getPictureRect(QRect *rect)
{
    if (this->m_state)
    {
        const FrameSize &frameSize = this->m_state->frameSize;
        QPoint screenOffset;

        this->m_screenOffsetLocker.lock();
        screenOffset = this->m_screenOffset;
        this->m_screenOffsetLocker.unlock();

        rect->setX((screenOffset.x() * this->m_devicePixelRatio) + ((this->m_width - frameSize.width) / 2));
        rect->setY((screenOffset.y() * this->m_devicePixelRatio) + ((this->m_height - frameSize.height) / 2));
        rect->setWidth(frameSize.width + 1);
        rect->setHeight(frameSize.height + 1);

        return true;
    }

    return false;
}

int VideoRenderer::drawSubtitleLine(int lineNum, int totalLineCount, const QString &text,
                                     const QPoint &margin, const Font::Context &context, const QString &totalText, int maxWidth,
                                     bool forcedLeft, AnyVODEnums::HAlignMethod halignOption, AnyVODEnums::VAlignMethod valignOption, const VideoPicture &vp)
{
    int defaultLineMargin = 5;
    QPoint point;
    int defaultVMargin;
    int defaultBaseMargin;
    int currentLine;
    QSize size;
    int left = 0;
    int base;
    int dir;
    QFontMetrics fm(this->m_subtitleFont.getQFont());
    int baseWidth;

    this->getSubtitleSize(fm, totalText, &size);

    defaultVMargin = -size.height() / 2;
    defaultBaseMargin = size.height();

    if (this->m_3dSubtitleMethod == AnyVODEnums::S3M_LEFT_RIGHT)
        baseWidth = this->m_state->frameSize.width;
    else
        baseWidth = this->m_width;

    AnyVODEnums::HAlignMethod halign = halignOption == AnyVODEnums::HAM_NONE ? this->m_halign : halignOption;

    switch (halign)
    {
        case AnyVODEnums::HAM_LEFT:
        {
            left = (baseWidth - maxWidth) / 2;
            break;
        }
        case AnyVODEnums::HAM_MIDDLE:
        {
            left = (baseWidth - size.width()) / 2;
            break;
        }
        case AnyVODEnums::HAM_RIGHT:
        {
            left = (baseWidth - maxWidth) / 2 + maxWidth - size.width();
            break;
        }
        default:
        {
            if (forcedLeft)
                left = (baseWidth - maxWidth) / 2;
            else
                left = (baseWidth - size.width()) / 2;

            break;
        }
    }

    point.setX(left + margin.x());

    AnyVODEnums::VAlignMethod valign = valignOption == AnyVODEnums::VAM_NONE ? this->m_valign : valignOption;

    if (valign == AnyVODEnums::VAM_TOP || valign == AnyVODEnums::VAM_MIDDLE)
    {
        int baseHeight;

        currentLine = lineNum;

        if (this->m_vrInputSource != AnyVODEnums::VRI_NONE || this->m_3dSubtitleMethod == AnyVODEnums::S3M_NONE)
        {
            base = 0;
            baseHeight = this->m_height;
        }
        else
        {
            QRect frameRect;

            this->getPictureRect(&frameRect);

            base = frameRect.y();
            baseHeight = this->m_state->frameSize.height;
        }

        if (valign == AnyVODEnums::VAM_MIDDLE)
        {
            int totolLineHeight = totalLineCount * size.height();

            base += (baseHeight - totolLineHeight) / 2;

            defaultVMargin = 0;
            defaultLineMargin = 0;
            defaultBaseMargin = 0;
        }

        dir = 1;
    }
    else
    {
        currentLine = totalLineCount - lineNum - 1;

        if (this->m_3dSubtitleMethod == AnyVODEnums::S3M_NONE)
        {
            base = this->m_height;
        }
        else if (this->m_3dSubtitleMethod == AnyVODEnums::S3M_LEFT_RIGHT)
        {
            base = this->m_state->frameSize.height;
        }
        else
        {
            QRect frameRect;

            this->getPictureRect(&frameRect);

            base = frameRect.y() + frameRect.height() - 1;
        }

        dir = -1;
    }

    point.setY(base + dir * (defaultBaseMargin + currentLine * size.height()) + defaultVMargin + margin.y());

    QPoint posOffset;

    this->getSubtitlePositionOffset(&posOffset);

    point -= posOffset;
    point.ry() += defaultLineMargin * dir;

    return this->drawOutlined(this->m_subtitleFont, point, text, this->m_subtitleOutlineSize, context, this->m_subtitleOpaque, vp);
}

bool VideoRenderer::drawAVSubtitle(const QFontMetrics &fm, const QSize &canvasSize, const AVSubtitle *sp, const VideoPicture &vp)
{
    ShaderCompositer &shader = ShaderCompositer::getInstance();
    MediaState *ms = this->m_state;
    int currentCount = 0;
    int maxWidth = 0;
    bool forcedLeft = false;
    bool isASS = false;
    QSize size;
    TextureInfo &texInfo = this->m_texInfo[TEX_FFMPEG_SUBTITLE];

    for (unsigned int i = 0; i < sp->num_rects; i++)
    {
        AVSubtitleRect *rect = sp->rects[i];

        if (rect->type == SUBTITLE_BITMAP)
        {
            QRect rc;
            QRect picRect;
            AVPixelFormat srcFMT;
            QPoint posOffset;
            QPoint newPos(rect->x, rect->y);

            this->getSubtitlePositionOffsetByFrame(vp.getOrgSize(), &posOffset);
            this->getPictureRect(&picRect);

            if (rect->x + rect->w > vp.getOrgWidth())
                newPos.rx() = vp.getOrgWidth() - rect->w;

            if (rect->y + rect->h > vp.getOrgHeight())
                newPos.ry() = vp.getOrgHeight() - rect->h;

            if (newPos.x() < 0)
            {
                rc.setX(posOffset.x() + picRect.x());
                rc.setWidth(ms->frameSize.width);
            }
            else
            {
                int x = newPos.x();
                int w = rect->w;

                if (canvasSize.width() > 0)
                {
                    x = MathUtils::mapTo(canvasSize.width(), vp.getOrgWidth(), x);
                    w = MathUtils::mapTo(canvasSize.width(), vp.getOrgWidth(), w);
                }

                x = MathUtils::mapTo(vp.getOrgWidth(), ms->frameSize.width, x);
                w = MathUtils::mapTo(vp.getOrgWidth(), ms->frameSize.width, w);

                rc.setX(x - posOffset.x() + picRect.x());
                rc.setWidth(w);
            }

            if (newPos.y() < 0)
            {
                rc.setY(posOffset.y() + picRect.y());
                rc.setHeight(ms->frameSize.height);
            }
            else
            {
                int y = newPos.y();
                int h = rect->h;

                if (canvasSize.height() > 0)
                {
                    y = MathUtils::mapTo(canvasSize.height(), vp.getOrgHeight(), y);
                    h = MathUtils::mapTo(canvasSize.height(), vp.getOrgHeight(), h);
                }

                y = MathUtils::mapTo(vp.getOrgHeight(), ms->frameSize.height, y);
                h = MathUtils::mapTo(vp.getOrgHeight(), ms->frameSize.height, h);

                rc.setY(y - posOffset.y() + picRect.y());
                rc.setHeight(h);
            }

            if (rect->nb_colors == 2)
            {
                srcFMT = AV_PIX_FMT_MONOWHITE;
            }
            else if (rect->nb_colors >= 4 && rect->nb_colors <= 256)
            {
                srcFMT = AV_PIX_FMT_PAL8;
            }
            else if (rect->nb_colors == 65536)
            {
                srcFMT = AV_PIX_FMT_RGB555;
            }
            else if (rect->nb_colors == 16777216)
            {
                srcFMT = AV_PIX_FMT_RGB24;
            }
            else
            {
                this->m_presenter->getRDetail().subtitleValidColor = false;
                return false;
            }

            this->m_presenter->getRDetail().subtitleBitmap = true;
            this->m_presenter->getRDetail().subtitleColorCount = rect->nb_colors;

            Surface subtitleImg;

            if (subtitleImg.create(rect->w, rect->h, AV_PIX_FMT_BGR32))
            {
                ms->subtitleConverter = sws_getCachedContext(
                                            ms->subtitleConverter,
                                            rect->w, rect->h, srcFMT,
                                            rect->w, rect->h, AV_PIX_FMT_BGR32,
                                            SWS_POINT, nullptr, nullptr, nullptr);

                if (ms->subtitleConverter)
                {
                    sws_scale(ms->subtitleConverter, rect->data, rect->linesize, 0, rect->h,
                              subtitleImg.getPixels(), subtitleImg.getLineSize());
                }

                bool enabledBlend = GL_PREFIX glIsEnabled(GL_BLEND);

                GL_PREFIX glEnable(GL_BLEND);

                QRectF renderRect;

                renderRect = QRectF(0.0, 0.0, 1.0, 1.0);

                for (unsigned int i = 0; i < texInfo.textureCount; i++)
                    texInfo.init[i] = false;

                if (this->m_3dSubtitleMethod == AnyVODEnums::S3M_NONE)
                {
                    QColor color = Qt::white;

                    color.setAlphaF(this->m_subtitleOpaque);

                    shader.startSimple();
                    GL_PREFIX glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                    this->renderTexture(ShaderCompositer::ST_SIMPLE, rc, texInfo, subtitleImg, renderRect, color, QMatrix4x4(), true);
                    shader.endSimple();
                }
                else
                {
                    QColor firstColorValue = Qt::white;
                    QColor secondColorValue = Qt::white;
                    QRect firstRect = rc;
                    QRect secondRect = rc;

                    firstColorValue.setAlphaF(this->m_subtitleOpaque);
                    secondColorValue.setAlphaF(this->m_subtitleOpaque);

                    switch (this->m_3dSubtitleMethod)
                    {
                        case AnyVODEnums::S3M_TOP_BOTTOM:
                        {
                            firstRect.setTop((rc.y() + picRect.y()) / 2);
                            firstRect.setHeight(rc.height() / 2);

                            secondRect.setTop(firstRect.y() + (picRect.height() - 1) / 2);
                            secondRect.setHeight(rc.height() / 2);

                            break;
                        }
                        case AnyVODEnums::S3M_LEFT_RIGHT:
                        {
                            firstRect.setLeft((rc.x() + picRect.x()) / 2);
                            firstRect.setWidth(rc.width() / 2);

                            secondRect.setLeft(firstRect.x() + (picRect.width() - 1) / 2);
                            secondRect.setWidth(rc.width() / 2);

                            break;
                        }
                        default:
                        {
                            break;
                        }
                    }

                    firstRect.translate(-this->m_3dSubtitleOffset * this->m_devicePixelRatio);
                    secondRect.translate(this->m_3dSubtitleOffset * this->m_devicePixelRatio);

                    switch (this->m_3dSubtitleMethod)
                    {
                        case AnyVODEnums::S3M_PAGE_FLIP:
                        {
                            shader.startSimple();
                            GL_PREFIX glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                            if (vp.isLeftOrTop())
                                this->renderTexture(ShaderCompositer::ST_SIMPLE, firstRect, texInfo, subtitleImg, renderRect, firstColorValue, QMatrix4x4(), true);
                            else
                                this->renderTexture(ShaderCompositer::ST_SIMPLE, secondRect, texInfo, subtitleImg, renderRect, firstColorValue, QMatrix4x4(), true);

                            shader.endSimple();

                            break;
                        }
                        case AnyVODEnums::S3M_INTERLACED:
                        {
                            GL_PREFIX glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                            shader.startSubtitleInterlace(shader.getLeftOrTop());
                            this->renderTexture(ShaderCompositer::ST_SUBTITLE_INTERLACE, firstRect, texInfo, subtitleImg, renderRect, firstColorValue, QMatrix4x4(), true);
                            shader.endSubtitleInterlace();

                            shader.startSubtitleInterlace(!shader.getLeftOrTop());
                            this->renderTexture(ShaderCompositer::ST_SUBTITLE_INTERLACE, secondRect, texInfo, subtitleImg, renderRect, firstColorValue, QMatrix4x4(), false);
                            shader.endSubtitleInterlace();

                            break;
                        }
                        case AnyVODEnums::S3M_CHECKER_BOARD:
                        {
                            GL_PREFIX glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                            shader.startSubtitleCheckerBoard(shader.getLeftOrTop());
                            this->renderTexture(ShaderCompositer::ST_SUBTITLE_CHECKER_BOARD, firstRect, texInfo, subtitleImg, renderRect, firstColorValue, QMatrix4x4(), true);
                            shader.endSubtitleCheckerBoard();

                            shader.startSubtitleCheckerBoard(!shader.getLeftOrTop());
                            this->renderTexture(ShaderCompositer::ST_SUBTITLE_CHECKER_BOARD, secondRect, texInfo, subtitleImg, renderRect, firstColorValue, QMatrix4x4(), false);
                            shader.endSubtitleCheckerBoard();

                            break;
                        }
                        case AnyVODEnums::S3M_ANAGLYPH:
                        {
                            GL_PREFIX glBlendFunc(GL_SRC_COLOR, GL_ONE);

                            shader.startSubtitleAnaglyph(shader.getLeftOrTop());
                            this->renderTexture(ShaderCompositer::ST_SUBTITLE_ANAGLYPH, firstRect, texInfo, subtitleImg, renderRect, firstColorValue, QMatrix4x4(), true);
                            shader.endSubtitleAnaglyph();

                            shader.startSubtitleAnaglyph(!shader.getLeftOrTop());
                            this->renderTexture(ShaderCompositer::ST_SUBTITLE_ANAGLYPH, secondRect, texInfo, subtitleImg, renderRect, secondColorValue, QMatrix4x4(), false);
                            shader.endSubtitleAnaglyph();

                            break;
                        }
                        default:
                        {
                            GL_PREFIX glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                            shader.startSimple();
                            this->renderTexture(ShaderCompositer::ST_SIMPLE, firstRect, texInfo, subtitleImg, renderRect, firstColorValue, QMatrix4x4(), true);
                            this->renderTexture(ShaderCompositer::ST_SIMPLE, secondRect, texInfo, subtitleImg, renderRect, secondColorValue, QMatrix4x4(), false);
                            shader.endSimple();

                            break;
                        }
                    }
                }

                if (!enabledBlend)
                    GL_PREFIX glDisable(GL_BLEND);

                subtitleImg.destory();
            }
        }
        else if (rect->type == SUBTITLE_TEXT)
        {
            QString text = QString::fromUtf8(rect->text);
            QStringList texts;

            text.remove('\r');
            texts = text.split(NEW_LINE);

            if (this->needMaxWidth())
            {
                for (int i = 0; i < texts.count(); i++)
                {
                    this->getSubtitleSize(fm, texts[i], &size);
                    maxWidth = max(maxWidth, size.width());

                    forcedLeft |= this->isLeftAlignLine(texts[i]);
                }
            }

            for (int j = 0; j < texts.count(); j++, currentCount++)
            {
                this->drawSubtitleLine(currentCount, texts.count() * sp->num_rects, texts[j], QPoint(0, 0),
                                       Font::Context(Qt::white), texts[j], maxWidth, forcedLeft, AnyVODEnums::HAM_NONE, AnyVODEnums::VAM_NONE, vp);
            }
        }
        else if (rect->type == SUBTITLE_ASS)
        {
            isASS = true;
        }
    }

    return isASS;
}

void VideoRenderer::drawASS(const AssRenderMethod method, const int32_t time, const VideoPicture &vp)
{
    ASS_Image *images = nullptr;
    bool changed = false;
    bool success = false;

    switch (method)
    {
        case ASSRM_FILE:
            success = this->m_presenter->getASSParser().get(time, &images, &changed);
            break;
        case ASSRM_SINGLE:
            success = this->m_presenter->getASSParser().getSingle(time, &images, &changed);
            break;
        case ASSRM_AV:
            success = this->m_presenter->getAVParser().getASSImage(time, &images, &changed);
            break;
    }

    if (success)
        this->renderASS(images, changed, vp);
}

void VideoRenderer::renderASS(ASS_Image *ass, bool blend, const VideoPicture &vp)
{
    ShaderCompositer &shader = ShaderCompositer::getInstance();

    if (ass->w <= 0 || ass->h <= 0)
        return;

    Surface &frame = this->m_context.assFrame;

    if (!frame.isValid())
        return;

    if (blend)
    {
        frame.clearPixels();

        while (ass)
        {
            this->blendASS(ass, frame);
            ass = ass->next;
        }
    }

    QRect rect;
    QPoint posOffset;

    this->getPictureRect(&rect);
    this->getSubtitlePositionOffsetByFrame(frame.getSize(), &posOffset);

    rect.translate(-posOffset);

    bool enabledBlend = GL_PREFIX glIsEnabled(GL_BLEND);

    GL_PREFIX glEnable(GL_BLEND);

    if (this->m_3dSubtitleMethod == AnyVODEnums::S3M_NONE)
    {
        QColor color = Qt::white;

        color.setAlphaF(this->m_subtitleOpaque);

        GL_PREFIX glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        shader.startSimple();
        this->renderASSSub(ShaderCompositer::ST_SIMPLE, rect, frame, blend, color, true);
        shader.endSimple();
    }
    else
    {
        QColor firstColorValue = Qt::white;
        QColor secondColorValue = Qt::white;
        QRect firstRect = rect;
        QRect secondRect = rect;

        firstColorValue.setAlphaF(this->m_subtitleOpaque);
        secondColorValue.setAlphaF(this->m_subtitleOpaque);

        switch (this->m_3dSubtitleMethod)
        {
            case AnyVODEnums::S3M_TOP_BOTTOM:
            {
                firstRect.setHeight(firstRect.height() / 2);
                secondRect.setY(secondRect.y() + firstRect.height());

                break;
            }
            case AnyVODEnums::S3M_LEFT_RIGHT:
            {
                firstRect.setWidth(firstRect.width() / 2);
                secondRect.setX(secondRect.x() + firstRect.width());

                break;
            }
            default:
            {
                break;
            }
        }

        firstRect.translate(-this->m_3dSubtitleOffset * this->m_devicePixelRatio);
        secondRect.translate(this->m_3dSubtitleOffset * this->m_devicePixelRatio);

        switch (this->m_3dSubtitleMethod)
        {
            case AnyVODEnums::S3M_PAGE_FLIP:
            {
                GL_PREFIX glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                shader.startSimple();

                if (vp.isLeftOrTop())
                    this->renderASSSub(ShaderCompositer::ST_SIMPLE, firstRect, frame, blend, firstColorValue, true);
                else
                    this->renderASSSub(ShaderCompositer::ST_SIMPLE, secondRect, frame, blend, firstColorValue, true);

                shader.endSimple();

                break;
            }
            case AnyVODEnums::S3M_INTERLACED:
            {
                GL_PREFIX glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                shader.startSubtitleInterlace(shader.getLeftOrTop());
                this->renderASSSub(ShaderCompositer::ST_SUBTITLE_INTERLACE, firstRect, frame, blend, firstColorValue, true);
                shader.endSubtitleInterlace();

                shader.startSubtitleInterlace(!shader.getLeftOrTop());
                this->renderASSSub(ShaderCompositer::ST_SUBTITLE_INTERLACE, secondRect, frame, blend, firstColorValue, false);
                shader.endSubtitleInterlace();

                break;
            }
            case AnyVODEnums::S3M_CHECKER_BOARD:
            {
                GL_PREFIX glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                shader.startSubtitleCheckerBoard(shader.getLeftOrTop());
                this->renderASSSub(ShaderCompositer::ST_SUBTITLE_CHECKER_BOARD, firstRect, frame, blend, firstColorValue, true);
                shader.endSubtitleCheckerBoard();

                shader.startSubtitleCheckerBoard(!shader.getLeftOrTop());
                this->renderASSSub(ShaderCompositer::ST_SUBTITLE_CHECKER_BOARD, secondRect, frame, blend, firstColorValue, false);
                shader.endSubtitleCheckerBoard();

                break;
            }
            case AnyVODEnums::S3M_ANAGLYPH:
            {
                GL_PREFIX glBlendFunc(GL_SRC_COLOR, GL_ONE_MINUS_SRC_COLOR);

                shader.startSubtitleAnaglyph(shader.getLeftOrTop());
                this->renderASSSub(ShaderCompositer::ST_SUBTITLE_ANAGLYPH, firstRect, frame, blend, firstColorValue, true);
                shader.endSubtitleAnaglyph();

                shader.startSubtitleAnaglyph(!shader.getLeftOrTop());
                this->renderASSSub(ShaderCompositer::ST_SUBTITLE_ANAGLYPH, secondRect, frame, blend, secondColorValue, false);
                shader.endSubtitleAnaglyph();

                break;
            }
            default:
            {
                GL_PREFIX glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

                shader.startSimple();
                this->renderASSSub(ShaderCompositer::ST_SIMPLE, firstRect, frame, blend, firstColorValue, true);
                this->renderASSSub(ShaderCompositer::ST_SIMPLE, secondRect, frame, blend, secondColorValue, false);
                shader.endSimple();

                break;
            }
        }
    }

    if (!enabledBlend)
        GL_PREFIX glDisable(GL_BLEND);
}

void VideoRenderer::renderASSSub(ShaderCompositer::ShaderType type, const QRect &rect,
                                  const Surface &frame, bool blend, const QColor &color, bool updateTexture) const
{
    QRectF renderRect(0.0, 0.0, 1.0, 1.0);

    if (blend)
        this->renderTexture(type, rect, this->m_texInfo[TEX_ASS_SUBTITLE], frame, renderRect, color, QMatrix4x4(), updateTexture);
    else
        this->renderTexture(type, rect, this->m_texInfo[TEX_ASS_SUBTITLE], frame.getDummy(), renderRect, color, QMatrix4x4(), updateTexture);
}

void VideoRenderer::blendASS(ASS_Image *single, Surface &frame) const
{
    int pixelSize = GET_PIXEL_SIZE(true);
    uint8_t r = GET_RED_VALUE(single->color);
    uint8_t g = GET_GREEN_VALUE(single->color);
    uint8_t b = GET_BLUE_VALUE(single->color);
    uint8_t a = 255 - GET_ALPHA_VALUE(single->color);
    uint8_t *src = single->bitmap;
    uint8_t *dst = frame.getPixels()[0] + single->dst_y * frame.getLineSize()[0] + single->dst_x * pixelSize;

    for (int y = 0; y < single->h; ++y)
    {
        for (int x = 0; x < single->w; ++x)
        {
            uint8_t k = src[x] * a / 255;
            uint8_t revOp = 255 - k;
            int xOffset = x * pixelSize;

            if (pixelSize == 4)
            {
                int aOffset = xOffset + 3;
                uint8_t alphaDst = dst[aOffset];

                if (k == 0 && alphaDst == 0)
                    continue;

                dst[aOffset] = (k * 255 + revOp * alphaDst) / 255;
            }

            int rOffset = xOffset + 0;
            int gOffset = xOffset + 1;
            int bOffset = xOffset + 2;

            dst[rOffset] = (k * r + revOp * dst[rOffset]) / 255;
            dst[gOffset] = (k * g + revOp * dst[gOffset]) / 255;
            dst[bOffset] = (k * b + revOp * dst[bOffset]) / 255;
        }

        src += single->stride;
        dst += frame.getLineSize()[0];
    }
}

void VideoRenderer::getSubtitlePositionOffset(QPoint *ret) const
{
    QRect desktopRect;

#if defined Q_OS_MOBILE
    desktopRect = QRect(0, 0, this->m_width, this->m_height);
#else
    desktopRect = QGuiApplication::primaryScreen()->geometry();
#endif

    ret->setX(MathUtils::mapTo(desktopRect.width(), this->m_width, this->m_subtitlePosition.x() * DEFAULT_HORI_SUBTITLE_RATIO * this->m_devicePixelRatio));
    ret->setY(MathUtils::mapTo(desktopRect.height(), this->m_height, this->m_subtitlePosition.y() * DEFAULT_VIRT_SUBTITLE_RATIO * this->m_devicePixelRatio));
}

void VideoRenderer::getSubtitlePositionOffsetByFrame(const QSize &org, QPoint *ret) const
{
    const FrameSize &frame = this->m_state->frameSize;

    ret->setX(MathUtils::mapTo(org.width(), frame.width, this->m_subtitlePosition.x() * DEFAULT_HORI_SUBTITLE_RATIO * this->m_devicePixelRatio));
    ret->setY(MathUtils::mapTo(org.height(), frame.height, this->m_subtitlePosition.y() * DEFAULT_VIRT_SUBTITLE_RATIO * this->m_devicePixelRatio));
}

void VideoRenderer::renderTexture2D(ShaderCompositer::ShaderType type, const QRect &rect,
                                     AVPixelFormat format, const QMatrix4x4 &modelView, const QRectF &texCoord, const QColor &color) const
{
    QVector4D vColor(color.redF(), color.greenF(), color.blueF(), color.alphaF());
    QVector3D vertices[] =
    {
        QVector3D(rect.left(), rect.bottom(), 0.0f),
        QVector3D(rect.right(), rect.bottom(), 0.0f),
        QVector3D(rect.left(), rect.top(), 0.0f),
        QVector3D(rect.right(), rect.top(), 0.0f)
    };
    QVector2D texCoords[] =
    {
        QVector2D(texCoord.left(), texCoord.bottom()),
        QVector2D(texCoord.right(), texCoord.bottom()),
        QVector2D(texCoord.left(), texCoord.top()),
        QVector2D(texCoord.right(), texCoord.top())
    };

    ShaderCompositer::getInstance().setRenderData(type, this->m_ortho, modelView, vertices, texCoords, texCoord, vColor, format);

    GL_PREFIX glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

const QVector2D* VideoRenderer::getTexCoordsFor3D(bool leftOrTop, const QRectF &renderRect) const
{
    ShaderCompositer &shader = ShaderCompositer::getInstance();
    const double half = 0.5 + 0.25;

    switch (this->m_vrInputSource)
    {
        case AnyVODEnums::VRI_LEFT_RIGHT_LEFT_PRIOR:
        {
            if (shader.isCubeMap())
                return Cube::getSBSTexCoords(leftOrTop);
            else
                return Sphere::getSBSTexCoords(leftOrTop);
        }
        case AnyVODEnums::VRI_LEFT_RIGHT_RIGHT_PRIOR:
        {
            if (shader.isCubeMap())
                return Cube::getSBSTexCoords(!leftOrTop);
            else
                return Sphere::getSBSTexCoords(!leftOrTop);
        }
        case AnyVODEnums::VRI_TOP_BOTTOM_TOP_PRIOR:
        {
            if (shader.isCubeMap())
                return Cube::getTABTexCoords(leftOrTop);
            else
                return Sphere::getTABTexCoords(leftOrTop);
        }
        case AnyVODEnums::VRI_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            if (shader.isCubeMap())
                return Cube::getTABTexCoords(!leftOrTop);
            else
                return Sphere::getTABTexCoords(!leftOrTop);
        }
        default:
        {
            if (FloatPointUtils::zeroDouble(renderRect.left()) == 0.0 && FloatPointUtils::zeroDouble(renderRect.top()) == 0.0 &&
                renderRect.width() < half && FloatPointUtils::oneDouble(renderRect.height()) == 1.0)
            {
                if (shader.isCubeMap())
                    return Cube::getSBSTexCoords(true);
                else
                    return Sphere::getSBSTexCoords(true);
            }
            else if (renderRect.left() < half && FloatPointUtils::zeroDouble(renderRect.top()) == 0.0 &&
                     renderRect.width() < half && FloatPointUtils::oneDouble(renderRect.height()) == 1.0)
            {
                if (shader.isCubeMap())
                    return Cube::getSBSTexCoords(false);
                else
                    return Sphere::getSBSTexCoords(false);
            }
            else if (FloatPointUtils::zeroDouble(renderRect.left()) == 0.0 && FloatPointUtils::zeroDouble(renderRect.top()) == 0.0 &&
                     FloatPointUtils::oneDouble(renderRect.width()) == 1.0 && renderRect.height() < half)
            {
                if (shader.isCubeMap())
                    return Cube::getTABTexCoords(true);
                else
                    return Sphere::getTABTexCoords(true);
            }
            else if (FloatPointUtils::zeroDouble(renderRect.left()) == 0.0 && renderRect.top() < half &&
                     FloatPointUtils::oneDouble(renderRect.width()) == 1.0 && renderRect.height() < half)
            {
                if (shader.isCubeMap())
                    return Cube::getTABTexCoords(false);
                else
                    return Sphere::getTABTexCoords(false);
            }
            else
            {
                if (shader.isCubeMap())
                    return Cube::getTexCoords();
                else
                    return Sphere::getTexCoords();
            }
        }
    }
}

void VideoRenderer::renderTexture3D(ShaderCompositer::ShaderType type,
                                     const QRect &rect, AVPixelFormat format, const QMatrix4x4 &modelView,
                                     const QRectF &renderRect, const QColor &color, bool leftOrTop) const
{
    ShaderCompositer &shader = ShaderCompositer::getInstance();
    QVector4D vColor(color.redF(), color.greenF(), color.blueF(), color.alphaF());
    int indexCount = 0;
    const unsigned short *indices = nullptr;
    const QVector3D *vertices = nullptr;
    const QVector2D *texCoords = this->getTexCoordsFor3D(leftOrTop, renderRect);

    if (shader.isCubeMap())
    {
        indexCount = Cube::getIndexCount();
        indices = Cube::getIndices();
        vertices = Cube::getVertices();
    }
    else
    {
        indexCount = Sphere::getIndexCount();
        indices = Sphere::getIndices();
        vertices = Sphere::getVertices();
    }

    QMatrix4x4 perspective;

    perspective.setToIdentity();

    float fov = this->m_fov * (shader.isCubeMap() ? 2.0f : 1.0f);

    if (this->m_vrInputSource == AnyVODEnums::VRI_NONE)
    {
        perspective.perspective(fov / 2.0f, (float)this->m_width / this->m_height, PERSPECTIVE_ZNEAR, PERSPECTIVE_ZFAR);

        shader.setRenderData(type, perspective, modelView, vertices, texCoords, renderRect, vColor, format);

        GL_PREFIX glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_SHORT, indices);
    }
    else
    {
        GLint viewport[4];

        perspective.perspective(fov, (float)rect.width() / this->m_height, PERSPECTIVE_ZNEAR, PERSPECTIVE_ZFAR);

        GL_PREFIX glGetIntegerv(GL_VIEWPORT, viewport);
        GL_PREFIX glViewport(rect.x(), 0, rect.width(), this->m_height);

        shader.setRenderData(type, perspective, modelView, vertices, texCoords, renderRect, vColor, format);

        GL_PREFIX glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_SHORT, indices);
        GL_PREFIX glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);
    }
}

void VideoRenderer::setupTextureAndSampler(ShaderCompositer::ShaderType type, const TextureInfo &texInfo, const Surface &surface) const
{
    if (this->isUseGPUConvert(surface.getFormat()) && type == ShaderCompositer::ST_SCREEN)
    {
        for (int i = 0; i < surface.getPlain(); i++)
        {
            const TextureInfo &info = this->m_texInfo[TEX_YUV_0 + i];

            GL_PREFIX glActiveTexture(GL_TEXTURE0 + i);
            GL_PREFIX glBindTexture(GL_TEXTURE_2D, info.id[info.index]);

            ShaderCompositer::getInstance().setTextureSampler(type, i, surface.getFormat());
        }
    }
    else
    {
        GL_PREFIX glBindTexture(GL_TEXTURE_2D, texInfo.id[texInfo.index]);
    }
}

void VideoRenderer::uploadTextureForGPUConverting(GLvoid *pixels[PICTURE_MAX_PLANE], const Surface &surface) const
{
    bool usePBO = this->isUsingPBO();
    GLsizei widths[surface.getPlain()];
    GLsizei heights[surface.getPlain()];
    GLenum intformats[surface.getPlain()];
    GLenum formats[surface.getPlain()];
    GLint aligns[surface.getPlain()];
    GLint types[surface.getPlain()];

    memset(widths, 0, sizeof(widths));
    memset(heights, 0, sizeof(heights));
    memset(intformats, 0, sizeof(intformats));
    memset(formats, 0, sizeof(formats));
    memset(aligns, 0, sizeof(aligns));
    memset(types, 0, sizeof(types));

    switch (surface.getFormat())
    {
        case AV_PIX_FMT_YUV420P:
        {
            widths[0] = surface.getWidth();
            widths[1] = surface.getWidth() / 2;
            widths[2] = surface.getWidth() / 2;

            heights[0] = surface.getHeight();
            heights[1] = surface.getHeight() / 2;
            heights[2] = surface.getHeight() / 2;

            intformats[0] = formats[0] = GL_LUMINANCE;
            intformats[1] = formats[1] = GL_LUMINANCE;
            intformats[2] = formats[2] = GL_LUMINANCE;

            aligns[0] = 1;
            aligns[1] = 1;
            aligns[2] = 1;

            types[0] = GL_UNSIGNED_BYTE;
            types[1] = GL_UNSIGNED_BYTE;
            types[2] = GL_UNSIGNED_BYTE;

            break;
        }
        case AV_PIX_FMT_NV12:
        case AV_PIX_FMT_NV21:
        {
            widths[0] = surface.getWidth();
            widths[1] = surface.getWidth() / 2;

            heights[0] = surface.getHeight();
            heights[1] = surface.getHeight() / 2;

            intformats[0] = formats[0] = GL_LUMINANCE;
            intformats[1] = formats[1] = GL_LUMINANCE_ALPHA;

            aligns[0] = 1;
            aligns[1] = 2;

            types[0] = GL_UNSIGNED_BYTE;
            types[1] = GL_UNSIGNED_BYTE;

            break;
        }
        case AV_PIX_FMT_YUYV422:
        case AV_PIX_FMT_UYVY422:
        case AV_PIX_FMT_YVYU422:
        {
            widths[0] = surface.getWidth() / 2;
            heights[0] = surface.getHeight();
            intformats[0] = formats[0] = GL_RGBA;
            aligns[0] = 4;
            types[0] = GL_UNSIGNED_BYTE;

            break;
        }
#if !defined Q_OS_MOBILE
        case AV_PIX_FMT_P010:
        case AV_PIX_FMT_P016:
        {
            widths[0] = surface.getWidth();
            widths[1] = surface.getWidth() / 2;

            heights[0] = surface.getHeight();
            heights[1] = surface.getHeight() / 2;

            intformats[0] = GL_R16;
            intformats[1] = GL_RG16;

            formats[0] = GL_RED;
            formats[1] = GL_RG;

            aligns[0] = 2;
            aligns[1] = 4;

            types[0] = GL_UNSIGNED_SHORT;
            types[1] = GL_UNSIGNED_SHORT;

            break;
        }
        case AV_PIX_FMT_YUV420P9:
        case AV_PIX_FMT_YUV420P10:
        case AV_PIX_FMT_YUV420P12:
        case AV_PIX_FMT_YUV420P14:
        case AV_PIX_FMT_YUV420P16:
        {
            widths[0] = surface.getWidth();
            widths[1] = surface.getWidth() / 2;
            widths[2] = surface.getWidth() / 2;

            heights[0] = surface.getHeight();
            heights[1] = surface.getHeight() / 2;
            heights[2] = surface.getHeight() / 2;

            intformats[0] = GL_R16;
            intformats[1] = GL_R16;
            intformats[2] = GL_R16;

            formats[0] = GL_RED;
            formats[1] = GL_RED;
            formats[2] = GL_RED;

            aligns[0] = 2;
            aligns[1] = 2;
            aligns[2] = 2;

            types[0] = GL_UNSIGNED_SHORT;
            types[1] = GL_UNSIGNED_SHORT;
            types[2] = GL_UNSIGNED_SHORT;

            break;
        }
#endif
        default:
        {
            break;
        }
    }

    for (int i = 0; i < surface.getPlain(); i++)
    {
        TextureInfo &info = this->m_texInfo[TEX_YUV_0 + i];

        GL_PREFIX glPixelStorei(GL_UNPACK_ALIGNMENT, aligns[i]);
        GL_PREFIX glBindTexture(GL_TEXTURE_2D, info.id[info.index]);

        if (usePBO)
        {
#if !defined Q_OS_MOBILE
            GL_PREFIX glBindBuffer(GL_PIXEL_UNPACK_BUFFER, info.idPBO[info.indexPBO]);
#endif
        }

        if (info.init[info.index])
        {
            GL_PREFIX glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, widths[i], heights[i], formats[i], types[i], pixels[i]);
        }
        else
        {
            GL_PREFIX glTexImage2D(GL_TEXTURE_2D, 0, intformats[i], widths[i], heights[i], 0, formats[i], types[i], pixels[i]);
            info.init[info.index] = true;
        }
    }
}

void VideoRenderer::uploadTextureForNormal(ShaderCompositer::ShaderType type, TextureInfo &texInfo, GLvoid *pixels[PICTURE_MAX_PLANE], const Surface &surface) const
{
    GLint align;
    GLint intFormat;
    GLint dataType;

    if (this->m_useHDR && type == ShaderCompositer::ST_SCREEN)
    {
#if defined Q_OS_MOBILE
        intFormat = GL_RGBA;
        dataType = GL_UNSIGNED_BYTE;
        align = 4;
#else
        intFormat = GL_RGBA16;
        dataType = GL_UNSIGNED_SHORT;
        align = 8;
#endif
    }
    else
    {
        intFormat = GL_RGBA;
        dataType = GL_UNSIGNED_BYTE;
        align = 4;
    }

    GL_PREFIX glPixelStorei(GL_UNPACK_ALIGNMENT, align);

    if (texInfo.init[texInfo.index])
    {
        GL_PREFIX glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, surface.getWidth(), surface.getHeight(), GL_RGBA, dataType, pixels[0]);
    }
    else
    {
        GL_PREFIX glTexImage2D(GL_TEXTURE_2D, 0, intFormat, surface.getWidth(), surface.getHeight(), 0, GL_RGBA, dataType, pixels[0]);
        texInfo.init[texInfo.index] = true;
    }
}

void VideoRenderer::uploadTexture(ShaderCompositer::ShaderType type, TextureInfo &texInfo, GLvoid *pixels[PICTURE_MAX_PLANE], const Surface &surface) const
{
    if (surface.getPlain() > 0 && this->isUseGPUConvert(surface.getFormat()) && type == ShaderCompositer::ST_SCREEN)
        this->uploadTextureForGPUConverting(pixels, surface);
    else
        this->uploadTextureForNormal(type, texInfo, pixels, surface);
}

bool VideoRenderer::preparePixelBuffers(ShaderCompositer::ShaderType type, TextureInfo &texInfo, const Surface &surface, GLvoid *pixels[PICTURE_MAX_PLANE]) const
{
    bool uploadable = true;
    bool usePBO = this->isUsingPBO();

    if (usePBO)
    {
#if !defined Q_OS_MOBILE
        if (this->isUseGPUConvert(surface.getFormat()) && type == ShaderCompositer::ST_SCREEN)
        {
            int hShift = 0;
            int vShift = 0;

            av_pix_fmt_get_chroma_sub_sample(surface.getFormat(), &hShift, &vShift);

            for (int i = 0; i < surface.getPlain(); i++)
            {
                TextureInfo &info = this->m_texInfo[TEX_YUV_0 + i];
                size_t bufSize = surface.getLineSize()[i] * (i > 0 ? AV_CEIL_RSHIFT(surface.getHeight(), vShift) : surface.getHeight());
                GLubyte *buf;

                GL_PREFIX glBindBuffer(GL_PIXEL_UNPACK_BUFFER, info.idPBO[info.indexPBO]);
                GL_PREFIX glBufferData(GL_PIXEL_UNPACK_BUFFER, bufSize, nullptr, GL_STREAM_DRAW);

                buf = (GLubyte*)GL_PREFIX glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY);

                if (buf)
                {
                    memcpy(buf, surface.getPixels()[i], bufSize);
                    GL_PREFIX glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
                }
                else
                {
                    uploadable = false;
                    break;
                }

                pixels[i] = (GLvoid*)0;
            }
        }
        else
        {
            size_t bufSize = surface.getLineSize()[0] * surface.getHeight();
            GLubyte *buf;

            GL_PREFIX glBindBuffer(GL_PIXEL_UNPACK_BUFFER, texInfo.idPBO[texInfo.indexPBO]);
            GL_PREFIX glBufferData(GL_PIXEL_UNPACK_BUFFER, bufSize, nullptr, GL_STREAM_DRAW);

            buf = (GLubyte*)GL_PREFIX glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY);

            if (buf)
            {
                memcpy(buf, surface.getPixels()[0], bufSize);
                GL_PREFIX glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
            }
            else
            {
                uploadable = false;
            }

            texInfo.indexPBO = (texInfo.indexPBO + 1) % MAX_PBO_COUNT;

            pixels[0] = (GLvoid*)0;
        }
#endif
    }
    else
    {
        (void)type;
        (void)texInfo;

        for (int i = 0; i < surface.getPlain(); i++)
            pixels[i] = surface.getPixels()[i];
    }

    return uploadable;
}

void VideoRenderer::updateTexture(ShaderCompositer::ShaderType type, TextureInfo &texInfo, const Surface &surface) const
{
    if (surface.isDummy())
        return;

    GLvoid *pixels[PICTURE_MAX_PLANE] = {nullptr, };

    if (this->preparePixelBuffers(type, texInfo, surface, pixels))
        this->uploadTexture(type, texInfo, pixels, surface);
}

void VideoRenderer::renderDistortionAdjustmentLines() const
{
    float ratio = this->m_width / 2560.0f;
    int verticalGap = 50 * ratio;
    int horizentaGap = 50 * ratio;
    int verticalCount = this->m_width / verticalGap + 1;
    int horizentalCount = this->m_height / horizentaGap + 1;
    bool enabledBlend = GL_PREFIX glIsEnabled(GL_BLEND);
    GLfloat lineWidth = 0.0f;
    ShaderCompositer &shader = ShaderCompositer::getInstance();

    GL_PREFIX glGetFloatv(GL_LINE_WIDTH, &lineWidth);
    GL_PREFIX glDisable(GL_BLEND);
    GL_PREFIX glLineWidth(2.0f);

    QVector4D vColor(1.0f, 1.0f, 1.0f, 1.0f);
    QVector<QVector3D> vertices;

    for (int i = 0; i < verticalCount; i++)
    {
        vertices.append(QVector3D(i * verticalGap, 0, 0));
        vertices.append(QVector3D(i * verticalGap, this->m_height, 0));
    }

    for (int i = 0; i < horizentalCount; i++)
    {
        vertices.append(QVector3D(0, i * horizentaGap, 0));
        vertices.append(QVector3D(this->m_width, i * horizentaGap, 0));
    }

    shader.startLine();
    shader.setRenderData(ShaderCompositer::ST_LINE, this->m_ortho, QMatrix4x4(), vertices.data(), nullptr, QRectF(), vColor, AV_PIX_FMT_NONE);

    GL_PREFIX glDrawArrays(GL_LINES, 0, vertices.size());

    shader.endLine();

    if (enabledBlend)
        GL_PREFIX glEnable(GL_BLEND);

    GL_PREFIX glLineWidth(lineWidth);
}

void VideoRenderer::cleanUpRender(ShaderCompositer::ShaderType type, const Surface &surface, bool updateTexture) const
{
    if (this->isUseGPUConvert(surface.getFormat()) && type == ShaderCompositer::ST_SCREEN)
    {
        const MediaState *ms = this->m_state;
        const Pause &pause = ms->pause;
        const Seek &seek = ms->seek;
        bool incIndex = updateTexture && !pause.pause && !seek.requestPauseOnRender && !surface.isDummy();

        for (int i = surface.getPlain() - 1; i >= 0 ; i--)
        {
            GL_PREFIX glActiveTexture(GL_TEXTURE0 + i);

            if (updateTexture)
            {
#if !defined Q_OS_MOBILE
                if (this->isUsingPBO())
                    GL_PREFIX glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
#endif
            }

            GL_PREFIX glBindTexture(GL_TEXTURE_2D, 0);

            TextureInfo &info = this->m_texInfo[TEX_YUV_0 + i];

            if (incIndex)
            {
                info.index = (info.index + 1) % info.textureCount;
                info.indexPBO = (info.indexPBO + 1) % MAX_PBO_COUNT;
            }
        }
    }
    else
    {
        if (updateTexture)
        {
#if !defined Q_OS_MOBILE
            if (this->isUsingPBO())
                GL_PREFIX glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
#endif
        }

        GL_PREFIX glBindTexture(GL_TEXTURE_2D, 0);
    }
}

void VideoRenderer::renderTexture(ShaderCompositer::ShaderType type, const QColor &color, const QMatrix4x4 &modelView, const QRectF &renderRect, const QRect &rect, const Surface &surface, bool updateTexture) const
{
    if (ShaderCompositer::getInstance().is360Degree() && type == ShaderCompositer::ST_SCREEN)
        this->renderTexture3D(type, rect, surface.getFormat(), modelView, renderRect, color, updateTexture);
    else
        this->renderTexture2D(type, rect, surface.getFormat(), modelView, renderRect, color);
}

void VideoRenderer::renderTexture(ShaderCompositer::ShaderType type,
                                   const QRect &rect, TextureInfo &texInfo, const Surface &surface,
                                   const QRectF &renderRect, const QColor &color, const QMatrix4x4 &modelView,
                                   bool updateTexture) const
{
    this->setupTextureAndSampler(type, texInfo, surface);

    if (updateTexture)
        this->updateTexture(type, texInfo, surface);

    if (this->m_distortionAdjustMode == AnyVODEnums::DAM_NONE)
        this->renderTexture(type, color, modelView, renderRect, rect, surface, updateTexture);
    else
        this->renderDistortionAdjustmentLines();

    this->cleanUpRender(type, surface, updateTexture);
}

bool VideoRenderer::get3DParameters(bool leftOrTop3D, const QSizeF &surfaceSize, QRectF *renderRect, bool *sideBySide, bool *leftOrTop)
{
    switch (this->m_3dMethod)
    {
        case AnyVODEnums::V3M_HALF_LEFT:
        {
            *renderRect = QRectF(0.0, 0.0, surfaceSize.width() / 2.0, surfaceSize.height());
            break;
        }
        case AnyVODEnums::V3M_HALF_RIGHT:
        {
            *renderRect = QRectF(surfaceSize.width() / 2.0, 0.0, surfaceSize.width() / 2.0, surfaceSize.height());
            break;
        }
        case AnyVODEnums::V3M_HALF_TOP:
        {
            *renderRect = QRectF(0.0, 0.0, surfaceSize.width(), surfaceSize.height() / 2.0);
            break;
        }
        case AnyVODEnums::V3M_HALF_BOTTOM:
        {
            *renderRect = QRectF(0.0, surfaceSize.height() / 2.0, surfaceSize.width(), surfaceSize.height() / 2.0);
            break;
        }
        case AnyVODEnums::V3M_FULL_LEFT_RIGHT:
        case AnyVODEnums::V3M_FULL_TOP_BOTTOM:
        {
            *renderRect = QRectF(0.0, 0.0, surfaceSize.width(), surfaceSize.height());
            break;
        }
        case AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_RIGHT_PRIOR:
        {
            if (leftOrTop3D)
                *renderRect = QRectF(0.0, 0.0, surfaceSize.width() / 2.0, surfaceSize.height());
            else
                *renderRect = QRectF(surfaceSize.width() / 2.0, 0.0, surfaceSize.width() / 2.0, surfaceSize.height());

            break;
        }
        case AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            if (leftOrTop3D)
                *renderRect = QRectF(0.0, 0.0, surfaceSize.width(), surfaceSize.height() / 2.0);
            else
                *renderRect = QRectF(0.0, surfaceSize.height() / 2.0, surfaceSize.width(), surfaceSize.height() / 2.0);

            break;
        }
        case AnyVODEnums::V3M_ROW_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_ROW_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_COL_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_COL_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_TOP_PRIOR:
        {
            *leftOrTop = true;
            *renderRect = QRectF(0.0, 0.0, surfaceSize.width(), surfaceSize.height());

            break;
        }
        case AnyVODEnums::V3M_ROW_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_ROW_TOP_BOTTOM_BOTTOM_PRIOR:
        case AnyVODEnums::V3M_COL_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_COL_TOP_BOTTOM_BOTTOM_PRIOR:
        case AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_BOTTOM_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_BOTTOM_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_BOTTOM_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_BOTTOM_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_BOTTOM_PRIOR:
        case AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            *leftOrTop = false;
            *renderRect = QRectF(0.0, 0.0, surfaceSize.width(), surfaceSize.height());

            break;
        }
        default:
        {
            return false;
        }
    }

    *sideBySide = this->isSideBySide();

    return true;
}

bool VideoRenderer::isUseGPUConvert(AVPixelFormat format) const
{
    bool use = this->m_useGPUConvert;

#ifdef Q_OS_WIN
    use &= glActiveTexture != nullptr;
#endif

    return use && this->isYUV(format);
}

bool VideoRenderer::isYUV(AVPixelFormat format) const
{
    if (this->m_hdrTextureSupported && this->isYUVHDRTexture(format))
        return true;

    switch (format)
    {
        case AV_PIX_FMT_YUV420P:
        case AV_PIX_FMT_YUYV422:
        case AV_PIX_FMT_UYVY422:
        case AV_PIX_FMT_YVYU422:
        case AV_PIX_FMT_NV12:
        case AV_PIX_FMT_NV21:
            return true;
        default:
            return false;
    }
}

bool VideoRenderer::isHDRTexture(AVPixelFormat format) const
{
    switch (format)
    {
        case AV_PIX_FMT_YUV420P9:
        case AV_PIX_FMT_YUV420P10:
        case AV_PIX_FMT_YUV420P12:
        case AV_PIX_FMT_YUV420P14:
        case AV_PIX_FMT_YUV420P16:
        case AV_PIX_FMT_P010:
        case AV_PIX_FMT_P016:
        case DEFAULT_HDR_PIX_FORMAT:
            return true;
        default:
            return false;
    }
}

bool VideoRenderer::isYUVHDRTexture(AVPixelFormat format) const
{
    return this->isHDRTexture(format) && format != DEFAULT_HDR_PIX_FORMAT;
}

void VideoRenderer::drawOptionDesc(const VideoPicture &vp)
{
    QMutexLocker locker(&this->m_optionDescMutex);
    const QColor color(0, 148, 255);
    const int outline = this->m_fontOutlineSize;
    QFontMetrics fm(this->m_font.getQFont());
    QString optionDesc = fm.elidedText(this->m_optionDesc, Qt::ElideMiddle, this->m_width - (10 * 2 * this->m_devicePixelRatio));

    this->drawOutlined(this->m_font, QPoint(10 * this->m_devicePixelRatio, (this->m_optionDescY + 10) * this->m_devicePixelRatio),
                       optionDesc, outline, Font::Context(color), 1.0f, vp);
}

bool VideoRenderer::isDistortionNecessary() const
{
    return this->m_vrInputSource != AnyVODEnums::VRI_NONE || ShaderCompositer::getInstance().is360Degree();
}

AVPixelFormat VideoRenderer::getCompatibleFormat(AVPixelFormat format) const
{
    if (!this->m_useLowQualityMode)
        return this->getDefaultFormat();

#if defined Q_OS_MOBILE
    if (this->isYUVHDRTexture(format))
        return AV_PIX_FMT_YUV420P;
    else
        return this->getDefaultFormat();
#else
    (void)format;

    return this->getDefaultFormat();
#endif
}

AVPixelFormat VideoRenderer::getDefaultFormat() const
{
    if (this->m_useHDR)
        return DEFAULT_HDR_PIX_FORMAT;
    else
        return DEFAULT_PIX_FORMAT;
}

QPair<QMatrix4x4, QMatrix4x4> VideoRenderer::buildModelViewMatrices()
{
    QMatrix4x4 modelViewLeft;
    QMatrix4x4 modelViewRight;
    float degree;
    const ShaderCompositer &shader = ShaderCompositer::getInstance();

    this->m_senserLocker.lock();
    degree = (float)(this->m_rotation + this->m_sensorRotation);
    this->m_senserLocker.unlock();

    degree += RotationUtils::convertSRDtoNumeric(this->m_screenRotationDegree);

    if (shader.is360Degree())
    {
        Camera &camera = Camera::getInstance();

        if (this->m_vrInputSource == AnyVODEnums::VRI_VIRTUAL_3D)
        {
            modelViewLeft.setToIdentity();
            modelViewLeft.rotate(-degree, 1.0f, 0.0f, 0.0f);

            modelViewRight = modelViewLeft;

            camera.lock();

            camera.moveLeftRight(-this->m_virtual3DDepth);
            modelViewLeft = camera.getMatrix() * modelViewLeft;

            camera.moveLeftRight(this->m_virtual3DDepth);
            modelViewRight = camera.getMatrix() * modelViewRight;

            camera.resetPosition();

            camera.unlock();

            if (shader.isCubeMap())
            {
                modelViewLeft.setColumn(3, QVector4D(0.0f, 0.0f, 0.0f, 1.0f));
                modelViewRight.setColumn(3, QVector4D(0.0f, 0.0f, 0.0f, 1.0f));
            }
        }
        else
        {
            modelViewLeft = camera.getMatrix();
            modelViewLeft.rotate(-degree, 1.0f, 0.0f, 0.0f);

            if (shader.isCubeMap())
                modelViewLeft.setColumn(3, QVector4D(0.0f, 0.0f, 0.0f, 1.0f));
        }
    }
    else
    {
        modelViewLeft.setToIdentity();

        modelViewLeft.translate((this->m_width / 2.0f), (this->m_height / 2.0f), 0.0f);
        modelViewLeft.rotate(degree, 0.0f, 0.0f, 1.0f);
        modelViewLeft.translate(-(this->m_width / 2.0f), -(this->m_height / 2.0f), 0.0f);

        if (this->m_vrInputSource == AnyVODEnums::VRI_VIRTUAL_3D)
        {
            modelViewRight = modelViewLeft;

            modelViewLeft.translate(-80.0f, 0.0f);
            modelViewRight.translate(80.0f, 0.0f);
        }
    }

    return qMakePair(modelViewLeft, modelViewRight);
}

void VideoRenderer::prepareAnaglyph(bool distortionBound)
{
    if (distortionBound)
        this->m_distortionFrameBuffer->release();

    this->m_anaglyphFrameBuffer->bind();

    GL_PREFIX glClear(GL_COLOR_BUFFER_BIT);
}

void VideoRenderer::applyAnaglyph(bool distortionBound)
{
    this->m_anaglyphFrameBuffer->release();

    if (distortionBound)
        this->m_distortionFrameBuffer->bind();

    ShaderCompositer &shader = ShaderCompositer::getInstance();
    GLfloat width = this->m_width / (GLfloat)this->m_anaglyphFrameBuffer->width();
    GLfloat height = this->m_height / (GLfloat)this->m_anaglyphFrameBuffer->height();
    bool enabledBlend = GL_PREFIX glIsEnabled(GL_BLEND);
    QRect rect;

    this->getPictureRect(&rect);

    shader.startSimple();

    GL_PREFIX glEnable(GL_BLEND);
    GL_PREFIX glBlendFunc(GL_SRC_COLOR, GL_ONE);

    GL_PREFIX glBindTexture(GL_TEXTURE_2D, this->m_anaglyphFrameBuffer->texture());

    QVector4D vColor(1.0f, 1.0f, 1.0f, 1.0f);
    QVector3D vertices[] =
    {
        QVector3D(rect.left(), rect.bottom(), 0.0f),
        QVector3D(rect.right(), rect.bottom(), 0.0f),
        QVector3D(rect.left(), rect.top(), 0.0f),
        QVector3D(rect.right(), rect.top(), 0.0f)
    };
    QVector2D texCoords[] =
    {
        QVector2D(0.0f, 0.0f),
        QVector2D(width, 0.0f),
        QVector2D(0.0f, height),
        QVector2D(width, height)
    };

    shader.setRenderData(ShaderCompositer::ST_SIMPLE, this->m_ortho, QMatrix4x4(), vertices, texCoords, QRectF(), vColor, AV_PIX_FMT_NONE);

    GL_PREFIX glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    GL_PREFIX glBindTexture(GL_TEXTURE_2D, 0);

    shader.endSimple();

    if (!enabledBlend)
        GL_PREFIX glDisable(GL_BLEND);
}

void VideoRenderer::drawFonts(const VideoPicture &vp)
{
    bool distortionBound = this->m_distortionFrameBuffer->isBound();

    if (this->m_3dSubtitleMethod == AnyVODEnums::S3M_ANAGLYPH)
        this->prepareAnaglyph(distortionBound);

    if (this->m_showSubtitle)
        this->drawSubtitles(vp);

    if (this->m_showDetail)
        this->drawDetail(vp);

    if (this->m_showingOptionDesc && !this->m_captureMode)
        this->drawOptionDesc(vp);

    if (this->m_3dSubtitleMethod == AnyVODEnums::S3M_ANAGLYPH)
        this->applyAnaglyph(distortionBound);
}

void VideoRenderer::drawVideo(const VideoPicture &vp)
{
    const Surface &surface = vp.getSurface();

    if (surface.isValid())
    {
        ShaderCompositer &shader = ShaderCompositer::getInstance();
        QRectF renderRect;
        TextureInfo &texInfo = this->m_texInfo[this->m_useHDR ? TEX_HDR_MOVIE_FRAME : TEX_MOVIE_FRAME];
        bool leftOrTop = false;
        bool sideBySide = false;
        QSizeF surfaceSize(surface.getSize());
        QSizeF screenSize(1.0, 1.0);
        QRect rect;

        this->getPictureRect(&rect);

        if (!this->get3DParameters(vp.isLeftOrTop(), screenSize, &renderRect, &sideBySide, &leftOrTop))
            renderRect = QRectF(QPointF(0.0, 0.0), screenSize);

        shader.setup3D(sideBySide, leftOrTop);
        shader.startScreen(surfaceSize, screenSize, this->m_presenter->getMasterClock(), vp.getLuminanceAverage() / 255.0,
                           surface.getFormat(), vp.getFrameMetaData());

        QPair<QMatrix4x4, QMatrix4x4> modelViews = this->buildModelViewMatrices();
        QColor color = Qt::white;

        color.setAlphaF(1.0);

        switch (this->m_vrInputSource)
        {
            case AnyVODEnums::VRI_LEFT_RIGHT_LEFT_PRIOR:
            case AnyVODEnums::VRI_LEFT_RIGHT_RIGHT_PRIOR:
            case AnyVODEnums::VRI_TOP_BOTTOM_TOP_PRIOR:
            case AnyVODEnums::VRI_TOP_BOTTOM_BOTTOM_PRIOR:
            {
                QRect firstRect = rect;
                QRect secondRect = rect;

                firstRect.setWidth(rect.width() / 2);

                secondRect.setLeft(rect.x() + (rect.width() - 1) / 2);
                secondRect.setWidth(rect.width() / 2);

                QRectF firstTexRect;
                QRectF secondTexRect;

                if (shader.is360Degree())
                {
                    if (shader.isCubeMap())
                    {
                        if (this->m_vrInputSource == AnyVODEnums::VRI_LEFT_RIGHT_LEFT_PRIOR ||
                            this->m_vrInputSource == AnyVODEnums::VRI_LEFT_RIGHT_RIGHT_PRIOR)
                        {
                            firstTexRect = QRectF(0.0, 0.0, screenSize.width() / 2.0, screenSize.height());
                            secondTexRect = QRectF(screenSize.width() / 2.0, 0.0, screenSize.width() / 2.0, screenSize.height());
                        }
                        else
                        {
                            firstTexRect = QRectF(0.0, 0.0, screenSize.width(), screenSize.height() / 2.0);
                            secondTexRect = QRectF(0.0, screenSize.height() / 2.0, screenSize.width(), screenSize.height() / 2.0);
                        }
                    }
                    else
                    {
                        firstTexRect = renderRect;
                        secondTexRect = renderRect;
                    }
                }
                else
                {
                    firstTexRect = renderRect;
                    secondTexRect = renderRect;

                    if (this->m_vrInputSource == AnyVODEnums::VRI_LEFT_RIGHT_LEFT_PRIOR ||
                        this->m_vrInputSource == AnyVODEnums::VRI_LEFT_RIGHT_RIGHT_PRIOR)
                    {
                        firstTexRect.setWidth(renderRect.width() / 2.0);
                        secondTexRect.setLeft(renderRect.right() / 2.0);
                    }
                    else
                    {
                        firstTexRect.setHeight(renderRect.height() / 2.0);
                        secondTexRect.setTop(renderRect.bottom() / 2.0);
                    }
                }

                this->renderTexture(ShaderCompositer::ST_SCREEN, firstRect, texInfo, surface, firstTexRect, color, modelViews.first, true);
                this->renderTexture(ShaderCompositer::ST_SCREEN, secondRect, texInfo, surface, secondTexRect, color, modelViews.first, false);

                break;
            }
            case AnyVODEnums::VRI_COPY:
            {
                QRect firstRect = rect;
                QRect secondRect = rect;

                firstRect.setWidth(rect.width() / 2);

                secondRect.setLeft(rect.x() + (rect.width() - 1) / 2);
                secondRect.setWidth(rect.width() / 2);

                this->renderTexture(ShaderCompositer::ST_SCREEN, firstRect, texInfo, surface, renderRect, color, modelViews.first, true);
                this->renderTexture(ShaderCompositer::ST_SCREEN, secondRect, texInfo, surface, renderRect, color, modelViews.first, false);

                break;
            }
            case AnyVODEnums::VRI_VIRTUAL_3D:
            {
                QRect firstRect = rect;
                QRect secondRect = rect;

                firstRect.setWidth(rect.width() / 2);

                secondRect.setLeft(rect.x() + (rect.width() - 1) / 2);
                secondRect.setWidth(rect.width() / 2);

                this->renderTexture(ShaderCompositer::ST_SCREEN, firstRect, texInfo, surface, renderRect, color, modelViews.first, true);
                this->renderTexture(ShaderCompositer::ST_SCREEN, secondRect, texInfo, surface, renderRect, color, modelViews.second, false);

                break;
            }
            default:
            {
                this->renderTexture(ShaderCompositer::ST_SCREEN, rect, texInfo, surface, renderRect, color, modelViews.first, true);
                break;
            }
        }

        shader.endScreen(surface.getFormat());

        this->m_presenter->getRDetail().videoFrameCount.fetchAndAddOrdered(1);
    }
}

void VideoRenderer::prepareDistortion()
{
    this->m_distortionFrameBuffer->bind();

    GL_PREFIX glClear(GL_COLOR_BUFFER_BIT);
}

void VideoRenderer::apply360DegreeDistortion()
{
    QRect renderRect = QRect(QPoint(0, 0), QSize(this->m_width, this->m_height));
    GLfloat width = this->m_width / (GLfloat)this->m_distortionFrameBuffer->width();
    GLfloat height = this->m_height / (GLfloat)this->m_distortionFrameBuffer->height();
    QVector4D vColor(1.0f, 1.0f, 1.0f, 1.0f);
    ShaderCompositer &shader = ShaderCompositer::getInstance();

    QVector3D vertices[] =
    {
        QVector3D(0.0f, renderRect.bottom(), 0.0f),
        QVector3D(renderRect.right(), renderRect.bottom(), 0.0f),
        QVector3D(renderRect.left(), renderRect.top(), 0.0f),
        QVector3D(renderRect.right(), renderRect.top(), 0.0f)
    };

    QVector2D texCoords[] =
    {
        QVector2D(0.0f, 0.0f),
        QVector2D(width, 0.0f),
        QVector2D(0.0f, height),
        QVector2D(width, height)
    };

    ShaderCompositer::ShaderType shaderType;

    if (this->m_useDistortion)
    {
        shaderType = ShaderCompositer::ST_PINCUSHION_DISTORTION;
        shader.startPincushionDistortion(QVector2D(1.0f, 1.0f), this->m_distortionLensCenter,
                                         this->m_pincushionDistortionCoefficients, 1.0f);
    }
    else
    {
        shaderType = ShaderCompositer::ST_SIMPLE;
        shader.startSimple();
    }

    shader.setRenderData(shaderType, this->m_ortho, QMatrix4x4(), vertices, texCoords, QRectF(), vColor, AV_PIX_FMT_NONE);

    GL_PREFIX glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    if (this->m_useDistortion)
        shader.endPincushionDistortion();
    else
        shader.endSimple();
}

void VideoRenderer::applyVRDistortion()
{
    QRect renderRect = QRect(QPoint(0, 0), QSize(this->m_width, this->m_height));
    GLfloat width = this->m_width / (GLfloat)this->m_distortionFrameBuffer->width();
    GLfloat height = this->m_height / (GLfloat)this->m_distortionFrameBuffer->height();
    QVector4D vColor(1.0f, 1.0f, 1.0f, 1.0f);
    ShaderCompositer &shader = ShaderCompositer::getInstance();

    QVector3D vertices[] =
    {
        QVector3D(0.0f, renderRect.bottom(), 0.0f),
        QVector3D(renderRect.right(), renderRect.bottom(), 0.0f),
        QVector3D(renderRect.left(), renderRect.top(), 0.0f),
        QVector3D(renderRect.right(), renderRect.top(), 0.0f)
    };

    QVector2D leftTexCoords[] =
    {
        QVector2D(0.0f, 0.0f),
        QVector2D(width / 2.0f, 0.0f),
        QVector2D(0.0f, height),
        QVector2D(width / 2.0f, height)
    };
    QVector2D rightTexCoords[] =
    {
        QVector2D(0.0f + (width / 2.0f), 0.0f),
        QVector2D(width, 0.0f),
        QVector2D(0.0f + (width / 2.0f), height),
        QVector2D(width, height)
    };

    bool enabledBlend = GL_PREFIX glIsEnabled(GL_BLEND);

    GL_PREFIX glDisable(GL_BLEND);

    shader.startSimple();

    this->m_leftDistortionFrameBuffer->bind();
    shader.setRenderData(ShaderCompositer::ST_SIMPLE, this->m_ortho, QMatrix4x4(), vertices, leftTexCoords, QRectF(), vColor, AV_PIX_FMT_NONE);
    GL_PREFIX glClear(GL_COLOR_BUFFER_BIT);
    GL_PREFIX glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    this->m_leftDistortionFrameBuffer->release();

    this->m_rightDistortionFrameBuffer->bind();
    shader.setRenderData(ShaderCompositer::ST_SIMPLE, this->m_ortho, QMatrix4x4(), vertices, rightTexCoords, QRectF(), vColor, AV_PIX_FMT_NONE);
    GL_PREFIX glClear(GL_COLOR_BUFFER_BIT);
    GL_PREFIX glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    this->m_rightDistortionFrameBuffer->release();

    GL_PREFIX glBindTexture(GL_TEXTURE_2D, 0);

    shader.endSimple();

    if (enabledBlend)
        GL_PREFIX glEnable(GL_BLEND);

    width = this->m_width / (GLfloat)this->m_leftDistortionFrameBuffer->width();
    height = this->m_height / (GLfloat)this->m_leftDistortionFrameBuffer->height();
    enabledBlend = GL_PREFIX glIsEnabled(GL_BLEND);

    ShaderCompositer::ShaderType shaderType;

    if (this->m_useDistortion)
    {
        if (shader.is360Degree())
        {
            shaderType = ShaderCompositer::ST_PINCUSHION_DISTORTION;
            shader.startPincushionDistortion(QVector2D(1.0f, 1.0f), this->m_distortionLensCenter,
                                         this->m_pincushionDistortionCoefficients, 1.0f);
        }
        else
        {
            shaderType = ShaderCompositer::ST_BARREL_DISTORTION;
            shader.startBarrelDistortion(QVector2D(1.0f, 1.0f), this->m_distortionLensCenter,
                                         this->m_barrelDistortionCoefficients, 1.0f);
        }
    }
    else
    {
        shaderType = ShaderCompositer::ST_SIMPLE;
        shader.startSimple();
    }

    GL_PREFIX glDisable(GL_BLEND);

    QVector3D leftVertices[] =
    {
        QVector3D(0.0f, renderRect.bottom(), 0.0f),
        QVector3D(renderRect.right() / 2, renderRect.bottom(), 0.0f),
        QVector3D(renderRect.left(), renderRect.top(), 0.0f),
        QVector3D(renderRect.right() / 2, renderRect.top(), 0.0f)
    };
    QVector3D rightVertices[] =
    {
        QVector3D(renderRect.left() + (renderRect.right() / 2), renderRect.bottom(), 0.0f),
        QVector3D(renderRect.right(), renderRect.bottom(), 0.0f),
        QVector3D(renderRect.left() + (renderRect.right() / 2), renderRect.top(), 0.0f),
        QVector3D(renderRect.right(), renderRect.top(), 0.0f)
    };
    QVector2D distortionTexCoords[] =
    {
        QVector2D(0.0f, 0.0f),
        QVector2D(width, 0.0f),
        QVector2D(0.0f, height),
        QVector2D(width, height)
    };

    shader.setRenderData(shaderType, this->m_ortho, QMatrix4x4(), leftVertices, distortionTexCoords, QRectF(), vColor, AV_PIX_FMT_NONE);

    GL_PREFIX glBindTexture(GL_TEXTURE_2D, this->m_leftDistortionFrameBuffer->texture());
    GL_PREFIX glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    shader.setRenderData(shaderType, this->m_ortho, QMatrix4x4(), rightVertices, distortionTexCoords, QRectF(), vColor, AV_PIX_FMT_NONE);

    GL_PREFIX glBindTexture(GL_TEXTURE_2D, this->m_rightDistortionFrameBuffer->texture());
    GL_PREFIX glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    if (this->m_useDistortion)
    {
        if (shader.is360Degree())
            shader.endPincushionDistortion();
        else
            shader.endBarrelDistortion();
    }
    else
    {
        shader.endSimple();
    }

    if (enabledBlend)
        GL_PREFIX glEnable(GL_BLEND);
}

void VideoRenderer::applyDistortion()
{
    this->m_distortionFrameBuffer->release();

    GL_PREFIX glBindTexture(GL_TEXTURE_2D, this->m_distortionFrameBuffer->texture());

    if (this->m_vrInputSource != AnyVODEnums::VRI_NONE)
        this->applyVRDistortion();
    else if (ShaderCompositer::getInstance().is360Degree())
        this->apply360DegreeDistortion();

    GL_PREFIX glBindTexture(GL_TEXTURE_2D, 0);
}

void VideoRenderer::displayVideo(const VideoPicture &vp)
{
    GL_PREFIX glClear(GL_COLOR_BUFFER_BIT);

    if (this->m_state == nullptr)
        return;

    ShaderCompositer &shader = ShaderCompositer::getInstance();

    this->checkVideoFrameSize(vp);

    if (this->isDistortionNecessary())
        this->prepareDistortion();

    this->drawVideo(vp);

    if (!shader.is360Degree())
        this->drawFonts(vp);

    if (this->isDistortionNecessary())
        this->applyDistortion();

    if (shader.is360Degree())
        this->drawFonts(vp);
}
