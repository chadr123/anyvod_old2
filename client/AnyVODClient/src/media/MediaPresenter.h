﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "MediaPresenterInterface.h"
#include "Callbacks.h"
#include "Detail.h"
#include "VideoThread.h"
#include "SubtitleThread.h"
#include "ReadThread.h"
#include "RefreshThread.h"
#include "AudioRenderer.h"
#include "VideoRenderer.h"
#include "core/ExtraPlayData.h"
#include "core/ChapterInfo.h"
#include "parsers/subtitle/SAMIParser.h"
#include "parsers/subtitle/ASSParser.h"
#include "parsers/subtitle/SRTParser.h"
#include "parsers/subtitle/LRCParser.h"
#include "parsers/subtitle/AVParser.h"
#include "parsers/subtitle/YouTubeParser.h"
#include "parsers/subtitle/NaverTVParser.h"
#include "video/Deinterlacer.h"
#include "video/ShaderCompositer.h"
#include "video/FilterGraph.h"
#include "decoders/HWDecoder.h"

#include <QThread>
#include <QMutex>
#include <QVector>

enum SyncType;

struct Stream;

class DTVReader;
class RadioReader;
class VirtualFile;

class MediaPresenter : public QThread, public MediaPresenterInterface
{
    Q_OBJECT
public:
    struct Range
    {
        Range()
        {
            start = 0.0;
            end = 0.0;
            enable = false;
        }

        double start;
        double end;
        bool enable;
    };

    MediaPresenter();
    ~MediaPresenter();

    bool open(const QString &filePath, const QString &title, const ExtraPlayData &data,
              const QString &fontFamily, const int fontSize, const int subtitleOutlineSize,
              const QString &audioPath);
    void close();

    void setDevicePixelRatio(double ratio);

#if defined Q_OS_MOBILE
    void setGL(QOpenGLFunctions *gl);
#endif

    bool saveSubtitleAs(const QString &filePath);
    bool saveSubtitle();

    QString getSubtitlePath() const;

    void openRemoteSubtitle(const QString &filePath);
    bool openSubtitle(const QString &filePath, bool isExternal);
    void closeAllExternalSubtitles();

    bool resetScreen(const int width, const int height, TextureInfo *texInfo, bool inContext);

    bool play();
    void stop();

    bool isPlayUserDataEmpty() const;
    QString getTitle() const;
    QString getArtist() const;

    void prevFrame(int count);
    void nextFrame(int count);

    bool render();

    void setSubtitleSync(double value);
    double getSubtitleSync() const;

    void setAudioSync(double value);
    double getAudioSync() const;

    void showDetail(bool show);
    bool isShowDetail() const;

    bool setAudioDevice(int device);
    int getCurrentAudioDevice() const;

    void getSPDIFAudioDevices(QStringList *ret);
    bool setSPDIFAudioDevice(int device);
    int getCurrentSPDIFAudioDevice() const;

    bool setAudioDeviceAfter();

    void showSubtitle(bool show);
    bool isShowSubtitle() const;

    bool existFileSubtitle();
    bool existExternalSubtitle();

    double getRotation() const;
    void setSensorRotation(double rot);

    bool isAlignable();
    AnyVODEnums::HAlignMethod getHAlign() const;
    void setHAlign(AnyVODEnums::HAlignMethod align);
    AnyVODEnums::VAlignMethod getVAlign() const;
    void setVAlign(AnyVODEnums::VAlignMethod align);

    bool existAudioSubtitle();
    bool existAudioSubtitleGender() const;

    void getSubtitleClasses(QStringList *classNames);
    void getCurrentSubtitleClass(QString *className);
    bool setCurrentSubtitleClass(const QString &className);

    const QVector<ChapterInfo>& getChapters() const;

    void setSubtitleURL(const QString &url);
    void getSubtitleURL(QString *ret) const;

    void setASSFontPath(const QString &path);
    void setASSFontFamily(const QString &family);

    void resetSubtitlePosition();
    void addVerticalSubtitlePosition(int pos);
    void addHorizontalSubtitlePosition(int pos);
    void setVerticalSubtitlePosition(int pos);
    void setHorizontalSubtitlePosition(int pos);
    int getVerticalSubtitlePosition() const;
    int getHorizontalSubtitlePosition() const;

    void reset3DSubtitleOffset();
    void addVertical3DSubtitleOffset(int pos);
    void addHorizontal3DSubtitleOffset(int pos);
    void setVertical3DSubtitleOffset(int pos);
    void setHorizontal3DSubtitleOffset(int pos);
    int getVertical3DSubtitleOffset() const;
    int getHorizontal3DSubtitleOffset() const;

    void resetVirtual3DDepth();
    void setVirtual3DDepth(qreal depth);
    qreal getVirtual3DDepth() const;

    void setRepeatStart(double start);
    void setRepeatEnd(double end);
    void setRepeatEnable(bool enable);
    bool getRepeatEnable() const;
    double getRepeatStart() const;
    double getRepeatEnd() const;

    void setCaptureMode(bool capture);
    bool getCaptureMode() const;

    void setSeekKeyFrame(bool keyFrame);
    bool isSeekKeyFrame() const;

    void set3DMethod(AnyVODEnums::Video3DMethod method);

    void setSubtitle3DMethod(AnyVODEnums::Subtitle3DMethod method);
    AnyVODEnums::Subtitle3DMethod getSubtitle3DMethod() const;

    void setVRInputSource(AnyVODEnums::VRInputSource source);
    AnyVODEnums::VRInputSource getVRInputSource() const;

    void setDistortionAdjustMode(AnyVODEnums::DistortionAdjustMode mode);
    AnyVODEnums::DistortionAdjustMode getDistortionAdjustMode() const;

    void setSkipRanges(const QVector<Range> &ranges);
    void getSkipRanges(QVector<Range> *ret) const;

    void setSkipOpening(bool skip);
    bool getSkipOpening() const;
    void setSkipEnding(bool skip);
    bool getSkipEnding() const;
    void setUseSkipRange(bool use);
    bool getUseSkipRange() const;

    double getOpeningSkipTime() const;
    double getEndingSkipTime() const;

    void useNormalizer(bool use);
    bool isUsingNormalizer() const;

    void useEqualizer(bool use);
    bool isUsingEqualizer() const;

    void useLowerVoice(bool use);
    bool isUsingLowerVoice() const;

    void useHigherVoice(bool use);
    bool isUsingHigherVoice() const;

    void useLowerMusic(bool use);
    bool isUsingLowerMusic() const;

    void setSubtitleOpaque(float opaque);
    float getSubtitleOpaque() const;

    void setSubtitleSize(float size);
    float getSubtitleSize() const;
    void scheduleRecomputeSubtitleSize();

    void useHWDecoder(bool enable);
    bool isUseHWDecoder() const;
    bool isOpenedHWDecoder() const;
    void changeUseHWDecoder(bool enable);

    int getHWDecoderCount() const;
    bool setHWDecoderIndex(int index);
    int getHWDecoderIndex() const;
    QString getHWDecoderName(int index) const;

    void useLowQualityMode(bool enable);
    bool isUseLowQualityMode() const;

    void useSPDIF(bool enable);
    bool isUseSPDIF() const;

    void setSPDIFEncodingMethod(AnyVODEnums::SPDIFEncodingMethod method);

    void setScreenRotationDegree(AnyVODEnums::ScreenRotationDegree degree);
    AnyVODEnums::ScreenRotationDegree getScreenRotationDegree() const;

    void use3DFull(bool enable);
    bool isUse3DFull() const;

    void usePBO(bool enable);
    bool isUsePBO() const;
    bool isUsablePBO() const;
    bool isUsingPBO() const;

    QSize getSize() const;

    void resetSPDIF();

    bool setPreAmp(float dB);
    float getPreAmp() const;

    bool setEqualizerGain(int band, float gain);
    float getEqualizerGain(int band) const;
    int getBandCount() const;

    bool isEnableSearchSubtitle() const;
    bool isEnableSearchLyrics() const;
    void enableSearchSubtitle(bool enable);
    void enableSearchLyrics(bool enable);

    void enableAutoSaveSearchLyrics(bool enable);
    bool isAutoSaveSearchLyrics() const;

    void showAlbumJacket(bool show);
    bool isShowAlbumJacket() const;

    void useFrameDrop(bool enable);

    void useBufferingMode(bool enable);

    void useGPUConvert(bool use);
    bool isUseGPUConvert() const;

    void useHDR(bool use);
    bool isUseHDR() const;

    void setHDRTextureSupported(bool supported);
    bool isHDRTextureSupported() const;

    void seek(double time, bool any);
    bool recover(double clock);

    void volume(uint8_t volume);
    void mute(bool mute);

    uint8_t getVolume() const;
    uint8_t getMaxVolume() const;

    double getCurrentPosition();
    bool hasDuration() const;

    bool IsHDRVideo() const;

    double getAspectRatio(bool widthPrio) const;

    bool isValid() const;

    bool canCapture() const;

    bool isTempoUsable() const;
    float getTempo() const;
    void setTempo(float percent);

    void setUserAspectRatio(UserAspectRatioInfo &ratio);
    void getUserAspectRatio(UserAspectRatioInfo *ret) const;

    void setMaxTextureSize(int size);
    int getMaxTextureSize() const;

    void useSubtitleCacheMode(bool use);
    bool isUseSubtitleCacheMode() const;

    void showOptionDesc(const QString &desc);
    void clearFonts();
    void clearFrameBuffers();

    bool getFrameSize(int *width, int *height) const;
    bool getPictureRect(QRect *rect);

    bool changeStream(int newIndex, int oldIndex, bool isAudio, int *lastStreamIndex, volatile bool *quitFlag);

    int getCurrentVideoStreamIndex() const;
    void getVideoStreamInfo(QVector<VideoStreamInfo> *ret) const;
    bool changeVideoStream(int index);

    void getAudioStreamInfo(QVector<AudioStreamInfo> *ret) const;
    bool changeAudioStream(int index);
    bool resetAudioStream();
    HSTREAM getAudioHandle() const;

    void setDeinterlacerAlgorithm(AnyVODEnums::DeinterlaceAlgorithm algorithm);
    void useDeinterlacerIVTC(bool use);

    bool configFilterGraph();

    int getOptionDescY() const;
    void setOptionDescY(int y);

    void setVerticalScreenOffset(int offset);
    void setHorizontalScreenOffset(int offset);

    void useDistortion(bool use);
    bool isUseDistortion() const;

    void setBarrelDistortionCoefficients(const QVector2D &coefficients);
    QVector2D getBarrelDistortionCoefficients() const;

    void setPincushionDistortionCoefficients(const QVector2D &coefficients);
    QVector2D getPincushionDistortionCoefficients() const;

    void setDistortionLensCenter(const QVector2D &lensCenter);
    QVector2D getDistortionLensCenter() const;

    QString getRealFilePath() const;

    void setBluetoothHeadsetConnected(bool connected);
    bool getBluetoothHeadsetConnected() const;

    void setBluetoothHeadsetSync(double sync);
    double getBluetoothHeadsetSync() const;

    void resetFOV();
    float getFOV() const;
    bool addFOV(float value);

#ifdef Q_OS_IOS
    void setIOSNotifyCallback(IOSNOTIFYPROC *proc);
#endif
    void setStatusChangedCallback(EventCallback *playing, EventCallback *ended);
    void setEmptyBufferCallback(EmptyBufferCallback &callback);
    void setShowAudioOptionDescCallback(ShowAudioOptionDescCallback &callback);
    void setAudioSubtitleCallback(AudioSubtitleCallback &callback);
    void setPaintCallback(PaintCallback &callback);
    void setAbortCallback(AbortCallback &callback);
    void setNonePlayingDescCallback(NonePlayingDescCallback &callback);
    void setRecoverCallback(EventCallback &callback);
    void setDisableHWDecoderCallback(EventCallback &callback);

public:
    static void init();
    static void deInit();

public:
    virtual void pause();
    virtual void resume();

    virtual bool isEnabledVideo() const;
    virtual bool isAudio() const;
    virtual bool isVideo() const;
    virtual bool isRemoteFile() const;
    virtual bool isRemoteProtocol() const;
    virtual bool isUseBufferingMode() const;
    virtual bool isUseFrameDrop() const;
    virtual bool isDTV() const;
    virtual bool isRadio() const;

    virtual AnyVODEnums::Video3DMethod get3DMethod() const;
    virtual AnyVODEnums::SPDIFEncodingMethod getSPDIFEncodingMethod() const;
    virtual int getCurrentAudioStreamIndex() const;

    virtual Deinterlacer& getDeinterlacer();
    virtual FilterGraph& getFilterGraph();

    virtual void setFormat(AVPixelFormat format);
    virtual AVPixelFormat getFormat() const;

    virtual double getMasterClock();

    virtual const Detail& getDetail() const;
    virtual double getDuration() const;
    virtual bool existSubtitle();

private:
    virtual bool isDevice() const;
    virtual bool isLive() const;
    virtual bool isUseGPUConvert(AVPixelFormat format) const;
    virtual bool isUsingSPDIFEncoding() const;
    virtual bool isValidSubtitleWithoutClass();

    virtual ASSParser& getASSParser();
    virtual YouTubeParser& getYoutubeParser();
    virtual NaverTVParser& getNaverTVParser();
    virtual AVParser& getAVParser();
    virtual SAMIParser& getSAMIParser();
    virtual SRTParser& getSRTParser();

    virtual HWDecoder& getHWDecoder();
    virtual Stream* getStream(int index);
    virtual Detail& getRDetail();

    virtual int64_t getAbsoluteClock() const;
    virtual double getAudioClockOffset() const;

    virtual void seek();
    virtual void abort(int reason);
    virtual void callEmptyCallback(bool show);
    virtual void callAudioSubtitleCallback();

    virtual double synchronizeVideo(AVFrame *srcFrame, double pts, const AVRational timeBase);
    virtual bool convertPicture(AVFrame &inFrame, double pts, AVPixelFormat format, bool leftOrTop3D, const FrameMetaData &frameMeta);

    virtual void startAudio();
    virtual void resumeAudio();
    virtual int getRemainedAudioDataSize() const;

private:
    enum SubtitleType
    {
        ST_NONE,
        ST_SAMI,
        ST_SRT,
        ST_ASS,
        ST_LRC,
        ST_AV,
        ST_YOUTUBE,
        ST_NAVER_TV
    };

private:
    double getVideoClock();
    double getExternalClock() const;
    double frameNumberToClock(int number) const;

    double calFrameDelay(double pts);
    void refreshSchedule(int delay);
    bool update();
    void updateVideoRefreshTimer();

    void allocPicture();
    void releasePictures();

    uint8_t getLuminanceAvg(uint8_t *data, int size, AVPixelFormat format) const;

    void releaseSubtitles();

    void resizePicture(const VideoPicture &src, VideoPicture &dest) const;
    void copyPicture(const VideoPicture &src, VideoPicture &dest) const;

    bool openStream();
    void closeStream();
    void closeInternal();
    bool openAudioStream(int *ret);
    bool initIOOptions(const QString &filePath, AVFormatContext *context) const;
    QString adjustProtocol(QString url) const;

    void seekStream(double pos, double dir, int flag);

    bool isUseAudioPath() const;

    bool openStreamComponent(unsigned int streamIndex, bool isAudio);
    void closeStreamComponent(unsigned int streamIndex, bool isAudio);

    const char* findProfileName(const AVProfile *profiles, int profile) const;

    const AVCodec* tryInternalHWDecoder(AVCodecContext *context, const QString &postFix) const;
    const AVCodec* tryCrystalHDDecoder(AVCodecContext *context) const;

    void reloadDeinteralcerCodec();

#if defined Q_OS_RASPBERRY_PI
    const AVCodec* tryMMALDecoder(AVCodecContext *context) const;
    const AVCodec* tryV4L2M2MDecoder(AVCodecContext *context) const;
#elif defined Q_OS_ANDROID
    const AVCodec* tryMediaCodecDecoder(AVCodecContext *context) const;
#endif

    SyncType getRecommandSyncType() const;

    bool openSAMI(const QString &filePath);
    bool openASS(const QString &filePath);
    bool openSRT(const QString &filePath);
    bool openLRC(const QString &filePath);
    bool openAVParser(const QString &filePath);
    bool openYouTube(const QString &path);
    bool openNaverTV(const QString &path);

    bool isRemoteSubtitle(SubtitleType type) const;

    void startReadThread();
    void processSkipRange();

    void callEmptyCallbackInternal(bool show);

    static int decodingInterruptCallback(void *userData);

    static int getBuffer(AVCodecContext *ctx, AVFrame *pic, int flag);
    static AVPixelFormat getFormat(struct AVCodecContext *ctx, const AVPixelFormat *fmt);

    static void restartVideo(void *userData, const QSize &newSize);

protected:
    virtual void run();

private:
    volatile bool m_forceExit;

private:
    QString m_filePath;
    QString m_realFilePath;
    QString m_audioPath;
    QString m_title;
    QString m_artist;
    MediaState *m_state;
    double m_subtitleSync;
    bool m_isRemoteFile;
    bool m_isRemoteProtocol;
    bool m_isDTV;
    bool m_isRadio;
    bool m_isDevice;
    bool m_isLive;
    QString m_GOMSubtitleURL;
    QVector<VideoStreamInfo> m_videoStreamInfo;
    QVector<AudioStreamInfo> m_audioStreamInfo;
    QVector<SubtitleStreamInfo> m_subtitleStreamInfo;
    int m_lastVideoStream;
    int m_lastAudioStream;
    int m_lastSubtitleStream;
    bool m_isAudioExt;
    bool m_seekKeyFrame;
    bool m_skipOpening;
    bool m_skipEnding;
    bool m_useSkipRange;
    bool m_useHWDecoder;
    bool m_useFrameDrop;
    bool m_useBufferingMode;
    bool m_enableSearchSubtitle;
    bool m_enableSearchLyrics;
    bool m_autoSaveSearchLyrics;
    bool m_showAlbumJacket;
    QVector<Range> m_skipRanges;
    QVector<ChapterInfo> m_chapters;
    Range m_repeatRange;
    Deinterlacer m_deinterlacer;
    VideoThread m_videoThread;
    SubtitleThread m_subtitleThread;
    ReadThread m_readThread;
    RefreshThread m_refreshThread;
    AudioRenderer m_audioRenderer;
    VideoRenderer m_videoRenderer;
    Detail m_detail;
    ExtraPlayData m_playData;
    SAMIParser m_samiParser;
    ASSParser m_assParser;
    SRTParser m_srtParser;
    LRCParser m_lrcParser;
    AVParser m_avParser;
    YouTubeParser m_youtubeParser;
    NaverTVParser m_naverTVParser;
    QString m_subtitleFilePath;
    HWDecoder m_hwDecoder;
    FilterGraph m_filterGraph;
    EventCallback m_playing;
    EventCallback m_ended;
    EmptyBufferCallback m_emptyBufferCallback;
    ShowAudioOptionDescCallback m_showAudioOptionDescCallback;
    AudioSubtitleCallback m_audioSubtitleCallback;
    AbortCallback m_abortCallback;
    NonePlayingDescCallback m_nonePlayingDescCallback;
    EventCallback m_recoverCallback;
    QString m_assFontFamily;
    QMutex m_controlLocker;
    SubtitleType m_subtitleType;
    VirtualFile &m_virtualFile;
    DTVReader &m_dtvReader;
    RadioReader &m_radioReader;

public:
    static const int OPTION_DESC_TIME;

private:
    static const int WATCHER_DELAY;
    static const int FIRST_REFRESH_DELAY;
    static const int DEFAULT_REFRESH_DELAY;
    static const int NO_AUDIO_ALBUM_JACKET_DELAY;
    static const int FRAME_LOW_WARNING_THRESHOLD;

    static const QString SUBTITLE_CODEC_FORMAT;

    static const double SYNC_THRESHOLD_MULTI;
    static const double SYNC_THRESHOLD;
};

Q_DECLARE_METATYPE(MediaPresenter::Range)
QDataStream& operator << (QDataStream &out, const MediaPresenter::Range &item);
QDataStream& operator >> (QDataStream &in, MediaPresenter::Range &item);
