﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "core/Lyrics.h"

#include <QSize>

struct EventCallback
{
    EventCallback()
    {
        callback = nullptr;
        userData = nullptr;
    }

    void(*callback)(void*);
    void *userData;
};

struct EmptyBufferCallback
{
    EmptyBufferCallback()
    {
        callback = nullptr;
        userData = nullptr;
    }

    void(*callback)(void*, bool);
    void *userData;
};

struct ShowAudioOptionDescCallback
{
    ShowAudioOptionDescCallback()
    {
        callback = nullptr;
        userData = nullptr;
    }

    void(*callback)(void*, const QString&, bool);
    void *userData;
};

struct AudioSubtitleCallback
{
    AudioSubtitleCallback()
    {
        callback = nullptr;
        userData = nullptr;
    }

    void(*callback)(void*, const QVector<Lyrics>&);
    void *userData;
};

struct PaintCallback
{
    PaintCallback()
    {
        callback = nullptr;
        userData = nullptr;
    }

    void(*callback)(void*);
    void *userData;
};

struct AbortCallback
{
    AbortCallback()
    {
        callback = nullptr;
        userData = nullptr;
    }

    void(*callback)(void*, int);
    void *userData;
};

struct NonePlayingDescCallback
{
    NonePlayingDescCallback()
    {
        callback = nullptr;
        userData = nullptr;
    }

    void(*callback)(void*, const QString&);
    void *userData;
};

struct RestartVideoCallback
{
    RestartVideoCallback()
    {
        callback = nullptr;
        userData = nullptr;
    }

    void(*callback)(void*, const QSize &);
    void *userData;
};
