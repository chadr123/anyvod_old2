﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "../../RadioReaderInterface.h"

#include <gnuradio/hier_block2.h>

class RadioReceiverInterface : public gr::hier_block2
{
public:
    RadioReceiverInterface(const std::string &name);
    virtual ~RadioReceiverInterface();

    virtual bool configure() = 0;

    virtual void setQuadRate(float rate) = 0;
    virtual void setAudioSampleRate(float rate) = 0;

    virtual void setEmphasis(double emphasis) = 0;

    virtual void setFilterParams(double low, double high, double transWidth) = 0;
    virtual void setDemodulator(RadioReaderInterface::DemodulatorType type) = 0;

    virtual float getSignalLevel() = 0;
};
