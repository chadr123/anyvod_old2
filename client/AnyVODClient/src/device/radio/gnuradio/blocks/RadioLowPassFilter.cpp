﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RadioLowPassFilter.h"

#include <gnuradio/filter/firdes.h>

RadioLowPassFilter::RadioLowPassFilter()
    : gr::hier_block2("RadioLowPassFilter",
                      gr::io_signature::make(1, 1, sizeof(float)),
                      gr::io_signature::make(1, 1, sizeof(float)))
{

}

RadioLowPassFilter::~RadioLowPassFilter()
{

}

void RadioLowPassFilter::setParameters(double sampleRate, double cutoffFreq, double transWidth, double gain)
{
    this->disconnect_all();
    this->m_filter.reset();

    std::vector<float> taps = gr::filter::firdes::low_pass(gain, sampleRate, cutoffFreq, transWidth);

    this->m_filter = gr::filter::fir_filter_fff::make(1, taps);

    this->connect(this->self(), 0, this->m_filter, 0);
    this->connect(this->m_filter, 0, this->self(), 0);
}
