﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#if __has_include(<gnuradio/filter/fir_filter_ccf.h>)
#include <gnuradio/filter/fir_filter_ccf.h>
#else
#include <gnuradio/filter/fir_filter_blk.h>
#endif

#include <gnuradio/hier_block2.h>

class RadioFIRDecimator : public gr::hier_block2
{
public:
    RadioFIRDecimator();
    virtual ~RadioFIRDecimator();

    void setDecimation(int decimation);

private:
    struct Stage
    {
        int decimation;
        int ratio;
        int length;
        const float *kernel;
    };

private:
    static const Stage STAGES[];

private:
    gr::filter::fir_filter_ccf::sptr m_firFirst;
    gr::filter::fir_filter_ccf::sptr m_firSecond;
    gr::filter::fir_filter_ccf::sptr m_firThird;
};
