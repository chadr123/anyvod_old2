﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RadioFMDemodulator.h"

RadioFMDemodulator::RadioFMDemodulator() :
    gr::hier_block2("RadioFMDemodulator",
                    gr::io_signature::make(1, 1, sizeof(gr_complex)),
                    gr::io_signature::make(1, 1, sizeof(float)))
{

}

RadioFMDemodulator::~RadioFMDemodulator()
{

}

void RadioFMDemodulator::setParameters(float quadRate, float maxDev, double tau)
{
    this->disconnect_all();

    float gain = float(quadRate / (2.0 * M_PI * maxDev));

    this->m_demodulator.reset();
    this->m_deemphasis.reset();

    this->m_demodulator = gr::analog::quadrature_demod_cf::make(gain);
    this->m_deemphasis = gnuradio::get_initial_sptr(new RadioFMDeemphasis());

    this->m_deemphasis->setParameters(quadRate, tau);

    this->connect(this->self(), 0, this->m_demodulator, 0);
    this->connect(this->m_demodulator, 0, this->m_deemphasis, 0);
    this->connect(this->m_deemphasis, 0, this->self(), 0);
}
