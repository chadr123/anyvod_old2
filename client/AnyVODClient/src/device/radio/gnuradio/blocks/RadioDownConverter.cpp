﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RadioDownConverter.h"

#include <gnuradio/filter/firdes.h>

RadioDownConverter::RadioDownConverter() :
    gr::hier_block2("RadioDownConverter",
                    gr::io_signature::make(1, 1, sizeof(gr_complex)),
                    gr::io_signature::make(1, 1, sizeof(gr_complex)))
{

}

RadioDownConverter::~RadioDownConverter()
{

}

void RadioDownConverter::setParameters(int decimation, double sampleRate)
{
    this->disconnect_all();

    if (decimation > 1)
    {
        this->m_filter.reset();
        this->m_filter = gr::filter::freq_xlating_fir_filter_ccf::make(decimation, {1}, 0.0, sampleRate);

        this->connect(this->self(), 0, this->m_filter, 0);
        this->connect(this->m_filter, 0, this->self(), 0);

        const double outRate = sampleRate / decimation;
        const double lpfCutOff = 120e3;

        this->m_filter->set_taps(gr::filter::firdes::low_pass(1.0, sampleRate, lpfCutOff, outRate - (2 * lpfCutOff)));
        this->m_filter->set_center_freq(0.0);
    }
    else
    {
        this->m_rotator.reset();
        this->m_rotator = gr::blocks::rotator_cc::make(0.0);

        this->connect(this->self(), 0, this->m_rotator, 0);
        this->connect(this->m_rotator, 0, this->self(), 0);

        this->m_rotator->set_phase_inc(0.0);
    }
}
