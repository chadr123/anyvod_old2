﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RadioAudioSink.h"

#include <QDeadlineTimer>

const int RadioAudioSink::MAX_QUEUE_SIZE = 1024 * 1024 * 1;

RadioAudioSink::RadioAudioSink() :
    gr::sync_block("RadioAudioSink",
                   gr::io_signature::make(1, 2, sizeof(float)),
                   gr::io_signature::make(0, 0, 0)),
    m_data(MAX_QUEUE_SIZE)
{

}

RadioAudioSink::~RadioAudioSink()
{

}

int RadioAudioSink::work(int numItems, gr_vector_const_void_star &inputItems, gr_vector_void_star &outputItems)
{
    (void)outputItems;
    const size_t sampleSize = inputItems.size() * numItems * sizeof(float);

    this->m_dataLock.lock();

    if (this->m_data.GetWritableSize() >= sampleSize)
    {
        if (inputItems.size() == 2)
        {
            const float *left = (const float*)inputItems[0];
            const float *right = (const float*)inputItems[1];

            for (int i = numItems; i > 0; i--)
            {
                this->m_data.Write((char*)left, sizeof(float));
                this->m_data.Write((char*)right, sizeof(float));

                left++;
                right++;
            }
        }
        else
        {
            const float *data = (const float*)inputItems[0];

            for (int i = numItems; i > 0; i--)
            {
                this->m_data.Write((char*)data, sizeof(float));
                data++;
            }
        }
    }

    this->m_dataCond.wakeOne();
    this->m_dataLock.unlock();

    return numItems;
}

void RadioAudioSink::lockBuffer()
{
    this->m_dataLock.lock();
}

void RadioAudioSink::unlockBuffer()
{
    this->m_dataLock.unlock();
}

bool RadioAudioSink::waitBuffer(int msecs)
{
    return this->m_dataCond.wait(&this->m_dataLock, QDeadlineTimer(msecs));
}

MemBuffer& RadioAudioSink::getBuffer()
{
    return this->m_data;
}
