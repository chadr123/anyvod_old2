﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RadioFMDeemphasis.h"

RadioFMDeemphasis::RadioFMDeemphasis() :
    gr::hier_block2("RadioFMDeemphasis",
                    gr::io_signature::make(1, 1, sizeof(float)),
                    gr::io_signature::make(1, 1, sizeof(float)))
{

}

RadioFMDeemphasis::~RadioFMDeemphasis()
{

}

void RadioFMDeemphasis::setParameters(float quadRate, double tau)
{
    this->disconnect_all();

    this->m_feedForwardTaps.resize(2);
    this->m_feedBackTaps.resize(2);

    this->calIIRTabs(quadRate, tau);

    this->m_iirFilter.reset();
    this->m_iirFilter = gr::filter::iir_filter_ffd::make(this->m_feedForwardTaps, this->m_feedBackTaps, false);

    this->connect(this->self(), 0, this->m_iirFilter, 0);
    this->connect(this->m_iirFilter, 0, this->self(), 0);
}

void RadioFMDeemphasis::calIIRTabs(float quadRate, double tau)
{
    if (tau > 1.0e-9)
    {
        double w_c;
        double w_ca;
        double k, z1, p1, b0;
        double fs = quadRate;

        w_c = 1.0 / tau;
        w_ca = 2.0 * fs * tan(w_c / (2.0 * fs));

        k = -w_ca / (2.0 * fs);
        z1 = -1.0;
        p1 = (1.0 + k) / (1.0 - k);
        b0 = -k / (1.0 - k);

        this->m_feedForwardTaps[0] = b0;
        this->m_feedForwardTaps[1] = -z1 * b0;
        this->m_feedBackTaps[0] = 1.0;
        this->m_feedBackTaps[1] = -p1;
    }
    else
    {
        this->m_feedForwardTaps[0] = 1.0;
        this->m_feedForwardTaps[1] = 0.0;
        this->m_feedBackTaps[0] = 0.0;
        this->m_feedBackTaps[1] = 0.0;
    }
}
