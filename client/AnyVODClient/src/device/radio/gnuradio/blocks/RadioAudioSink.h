﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "../../../common/MemBuffer.h"

#include <QMutex>
#include <QWaitCondition>

#include <gnuradio/sync_block.h>

class RadioAudioSink : public gr::sync_block
{
public:
    RadioAudioSink();
    virtual ~RadioAudioSink();

    virtual int work(int numItems, gr_vector_const_void_star &inputItems, gr_vector_void_star &outputItems);

    void lockBuffer();
    void unlockBuffer();
    bool waitBuffer(int msecs);

    MemBuffer& getBuffer();

private:
    static const int MAX_QUEUE_SIZE;

private:
    QMutex m_dataLock;
    QWaitCondition m_dataCond;
    MemBuffer m_data;
};
