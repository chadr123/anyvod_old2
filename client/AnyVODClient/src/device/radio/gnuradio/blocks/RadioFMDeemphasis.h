﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <gnuradio/filter/iir_filter_ffd.h>
#include <gnuradio/hier_block2.h>

class RadioFMDeemphasis : public gr::hier_block2
{
public:
    RadioFMDeemphasis();
    virtual ~RadioFMDeemphasis();

    void setParameters(float quadRate, double tau);

private:
    void calIIRTabs(float quadRate, double tau);

private:
    gr::filter::iir_filter_ffd::sptr m_iirFilter;

    std::vector<double> m_feedForwardTaps;
    std::vector<double> m_feedBackTaps;
};
