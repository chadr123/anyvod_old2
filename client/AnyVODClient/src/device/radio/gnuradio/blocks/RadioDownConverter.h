﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#if __has_include(<gnuradio/filter/freq_xlating_fir_filter_ccf.h>)
#include <gnuradio/filter/freq_xlating_fir_filter_ccf.h>
#else
#include <gnuradio/filter/freq_xlating_fir_filter.h>
#endif

#include <gnuradio/blocks/rotator_cc.h>
#include <gnuradio/hier_block2.h>

class RadioDownConverter : public gr::hier_block2
{
public:
    RadioDownConverter();
    virtual ~RadioDownConverter();

    void setParameters(int decimation, double sampleRate);

private:
    gr::filter::freq_xlating_fir_filter_ccf::sptr m_filter;
    gr::blocks::rotator_cc::sptr m_rotator;
};
