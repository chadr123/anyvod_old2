﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RadioGNURadio.h"
#include "gnuradio/RadioReceiverWideBandFM.h"

#include <QVector>
#include <QtEndian>
#include <QDebug>

#include <osmosdr/device.h>
#include <bass/bass.h>
#include <bass/bass_fx.h>

extern "C"
{
# include <libavutil/common.h>
# include <libavutil/error.h>
}

#define AUDIO_GAIN_IN_DB (-10.0)

const float RadioGNURadio::DEFAULT_AUDIO_GAIN = float(BASS_BFX_dB2Linear(AUDIO_GAIN_IN_DB));
const QPair<QString, uint64_t> RadioGNURadio::DEFAULT_INPUT_RATES[] =
{
    qMakePair(QString("rtl"), 1800000),
    qMakePair(QString("sdr-iq"), 111111),
    qMakePair(QString("sdr-ip"), 250000),
    qMakePair(QString("netsdr"), 250000),
    qMakePair(QString("cloudiq"), 240000),
    qMakePair(QString("redpitaya"), 250000),
    qMakePair(QString("sdrplay"), 2000000),
    qMakePair(QString("lime"), 5000000),
    qMakePair(QString("plutosdr"), 1500000),
    qMakePair(QString("perseus"), 192000),
};

const RadioGNURadio::FilterParams RadioGNURadio::FILTER_PARAMS[RadioReaderInterface::DT_COUNT] =
{
    {-80000.0, 80000.0},
    {-80000.0, 80000.0},
};

RadioGNURadio::RadioGNURadio() :
    m_audioGainLeft(gr::blocks::multiply_const_ff::make(DEFAULT_AUDIO_GAIN)),
    m_audioGainRight(gr::blocks::multiply_const_ff::make(DEFAULT_AUDIO_GAIN)),
    m_inputSampleRate(96000),
    m_audioSampleRate(48000),
    m_decimation(1),
    m_emphasis(0.0),
    m_phase(PH_NONE),
    m_channelCount(0)
{

}

RadioGNURadio::~RadioGNURadio()
{
    this->closeInternal();
}

bool RadioGNURadio::open(const QString &path)
{
    (void)path;
    QVector<RadioReaderInterface::AdapterInfo> adapters;

    this->getAdapterList(&adapters);

    if (this->m_adapter >= adapters.count() || this->m_adapter < 0)
        return false;

    QString devArgs = adapters[this->m_adapter].data.args;

    try
    {
        this->m_source = osmosdr::source::make(devArgs.toStdString());
        this->m_source->set_bandwidth(0.0);
    }
    catch (std::runtime_error&)
    {
        return false;
    }

    this->m_topBlock = gr::make_top_block("RadioGNURadio");

    this->selectInputRate(devArgs);
    this->m_phase = PH_HEADER;

    return true;
}

void RadioGNURadio::close()
{
    this->closeInternal();
}

int RadioGNURadio::read(uint8_t *buf, int size)
{
    int read;

    switch (this->m_phase)
    {
        case PH_HEADER:
        {
            const RIFFHeader header =
            {
                MKTAG('R', 'I', 'F', 'F'),
                qToLittleEndian(quint32(std::numeric_limits<uint32_t>::max() - 8)),
                MKTAG('W', 'A', 'V', 'E'),
                MKTAG('f', 'm', 't', ' '),
                qToLittleEndian(quint32(16)),
                qToLittleEndian(quint16(3)),
                qToLittleEndian(quint16(this->m_channelCount)),
                qToLittleEndian(quint32(this->m_audioSampleRate)),
                qToLittleEndian(quint32(this->m_audioSampleRate * this->m_channelCount * sizeof(float))),
                qToLittleEndian(quint16(this->m_channelCount * sizeof(float))),
                qToLittleEndian(quint16(sizeof(float) * 8)),
                MKTAG('d', 'a', 't', 'a'),
                qToLittleEndian(quint32(std::numeric_limits<uint32_t>::max() - sizeof(header))),
            };

            memcpy(buf, &header, sizeof(header));
            read = sizeof(header);

            this->m_phase = PH_BODY;
            break;
        }
        case PH_BODY:
        {
            this->m_audioSink->lockBuffer();

            while (this->m_audioSink->getBuffer().IsEmpty())
            {
                if (!this->m_audioSink->waitBuffer(250))
                {
                    this->m_audioSink->unlockBuffer();
                    return 0;
                }
            }

            read = this->m_audioSink->getBuffer().Read((char*)buf, size);

            this->m_audioSink->unlockBuffer();

            break;
        }
        case PH_TRAILER:
        {
            read = 0;
            this->m_phase = PH_NONE;
            break;
        }
        default:
        {
            return AVERROR(EINTR);
        }
    }

    return read;
}

void RadioGNURadio::setFrequency(double freqHz)
{
    this->m_source->set_center_freq(freqHz);
}

double RadioGNURadio::getFrequency()
{
    return this->m_source->get_center_freq();
}

float RadioGNURadio::getSignalStrength() const
{
    return this->m_rx->getSignalLevel();
}

bool RadioGNURadio::setDecimation(int decimation)
{
    QVector<int> possible;

    if (this->m_inputSampleRate >= 96000)
        possible.append(2);

    if (this->m_inputSampleRate >= 192000)
        possible.append(4);

    if (this->m_inputSampleRate >= 384000)
        possible.append(8);

    if (this->m_inputSampleRate >= 768000)
        possible.append(16);

    if (this->m_inputSampleRate >= 1536000)
        possible.append(32);

    if (this->m_inputSampleRate >= 3072000)
        possible.append(64);

    if (this->m_inputSampleRate >= 6144000)
        possible.append(128);

    if (possible.contains(decimation))
    {
        this->m_decimation = decimation;
        return true;
    }

    return false;
}

int RadioGNURadio::getDecimation() const
{
    return this->m_decimation;
}

void RadioGNURadio::setAudioGain(float db)
{
    float k = float(BASS_BFX_dB2Linear(db));

    this->m_audioGainLeft->set_k(k);
    this->m_audioGainRight->set_k(k);
}

float RadioGNURadio::getAudioGain() const
{
    return float(BASS_BFX_Linear2dB(this->m_audioGainLeft->k()));
}

void RadioGNURadio::enableAGC(bool enable)
{
    this->m_source->set_gain_mode(enable);
}

bool RadioGNURadio::isEnabledAGC() const
{
    return this->m_source->get_gain_mode();
}

void RadioGNURadio::setEmphasis(double emphasis)
{
    this->m_emphasis = emphasis;
}

void RadioGNURadio::setAntenna(const QString &antenna)
{
    this->m_source->set_antenna(antenna.toStdString());
}

QStringList RadioGNURadio::getAntennas() const
{
    QStringList antennas;

    for (const std::string &antenna : this->m_source->get_antennas())
        antennas.append(QString::fromStdString(antenna));

    return antennas;
}

bool RadioGNURadio::tune()
{
    double decimationRate = this->m_inputSampleRate;

    if (this->m_decimation >= 2)
    {
        this->m_firDecimator = gnuradio::get_initial_sptr(new RadioFIRDecimator());
        this->m_firDecimator->setDecimation(this->m_decimation);

        decimationRate /= this->m_decimation;
    }

    const double targetQuadRate = 1e6;
    int ddcDecimation = std::max(1, int(decimationRate / targetQuadRate));
    double quadRate = decimationRate / ddcDecimation;

    this->m_downConverter = gnuradio::get_initial_sptr(new RadioDownConverter());
    this->m_downConverter->setParameters(ddcDecimation, decimationRate);

    this->m_audioSink = gnuradio::get_initial_sptr(new RadioAudioSink());

    switch (this->m_type)
    {
        case RadioReaderInterface::DT_WIDE_FM_MONO:
        case RadioReaderInterface::DT_WIDE_FM_STEREO:
            this->m_rx = gnuradio::get_initial_sptr(new RadioReceiverWideBandFM());
            break;
        default:
            return false;
    }

    switch (this->m_type)
    {
        case RadioReaderInterface::DT_WIDE_FM_MONO:
            this->m_channelCount = 1;
            break;
        case RadioReaderInterface::DT_WIDE_FM_STEREO:
            this->m_channelCount = 2;
            break;
        default:
            return false;
    }

    const FilterParams &params = FILTER_PARAMS[this->m_type];
    double transWidth = std::abs(params.high - params.low) * 0.2;

    this->m_rx->setFilterParams(params.low, params.high, transWidth);
    this->m_rx->setDemodulator(this->m_type);

    this->m_rx->setQuadRate(quadRate);
    this->m_rx->setAudioSampleRate(this->m_audioSampleRate);
    this->m_rx->setEmphasis(this->m_emphasis);

    if (!this->m_rx->configure())
        return false;

    this->connectAll();
    this->start();

    return true;
}

bool RadioGNURadio::getAdapterList(QVector<RadioReaderInterface::AdapterInfo> *ret) const
{
    osmosdr::devices_t devices = osmosdr::device::find(osmosdr::device_t("nofake"));

    for (osmosdr::devices_t::size_type i = 0; i < devices.size(); i++)
    {
        osmosdr::device_t &device = devices[i];
        osmosdr::device_t::iterator label = device.find("label");
        osmosdr::device_t::iterator soapy = device.find("soapy");
        RadioReaderInterface::AdapterInfo info;

        if (soapy != device.end())
        {
            osmosdr::device_t::iterator driver = device.find("driver");

            if (driver != device.end() && driver->second == "audio")
                continue;
        }

        if (label == device.end())
        {
            info.name = "Unknown";
        }
        else
        {
            info.name = QString::fromStdString(label->second);
            device.erase(label);
        }

        info.data.args = QString::fromStdString(device.to_string());
        info.data.index = i;

        ret->append(info);
    }

    return ret->count() > 0;
}

void RadioGNURadio::start()
{
    this->m_topBlock->start();
}

void RadioGNURadio::stop()
{
    if (this->m_topBlock)
    {
        this->m_topBlock->stop();
        this->m_topBlock->wait();

        this->m_topBlock->disconnect_all();
    }
}

void RadioGNURadio::closeInternal()
{
    this->stop();

    this->m_topBlock.reset();
    this->m_source.reset();
    this->m_firDecimator.reset();
    this->m_downConverter.reset();
    this->m_audioSink.reset();
    this->m_rx.reset();

    this->m_phase = PH_TRAILER;
}

void RadioGNURadio::connectAll()
{
    gr::basic_block_sptr block = this->m_source;

    if (this->m_firDecimator)
    {
        this->m_topBlock->connect(block, 0, this->m_firDecimator, 0);
        block = this->m_firDecimator;
    }

    this->m_topBlock->connect(block, 0, this->m_downConverter, 0);
    this->m_topBlock->connect(this->m_downConverter, 0, this->m_rx, 0);
    this->m_topBlock->connect(this->m_rx, 0, this->m_audioGainLeft, 0);
    this->m_topBlock->connect(this->m_audioGainLeft, 0, this->m_audioSink, 0);

    if (this->m_channelCount == 2)
    {
        this->m_topBlock->connect(this->m_rx, 1, this->m_audioGainRight, 0);
        this->m_topBlock->connect(this->m_audioGainRight, 0, this->m_audioSink, 1);
    }
}

void RadioGNURadio::selectInputRate(const QString &devArgs)
{
    for (const auto &inputRate : DEFAULT_INPUT_RATES)
    {
        if (devArgs.startsWith(inputRate.first))
        {
            this->m_inputSampleRate = this->m_source->set_sample_rate(inputRate.second);
            return;
        }
    }

    this->m_inputSampleRate = this->m_source->get_sample_rate();
}
