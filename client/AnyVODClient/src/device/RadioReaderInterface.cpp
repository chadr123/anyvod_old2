﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RadioReaderInterface.h"

#include <QDataStream>

QDataStream& operator << (QDataStream &out, const RadioReaderInterface::AdapterData &data)
{
    out << data.index;
    out << data.args;

    return out;
}

QDataStream& operator >> (QDataStream &in, RadioReaderInterface::AdapterData &data)
{
    in >> data.index;
    in >> data.args;

    return in;
}

RadioReaderInterface::RadioReaderInterface() :
    m_adapter(-1),
    m_type(DT_NONE)
{

}

RadioReaderInterface::~RadioReaderInterface()
{

}

void RadioReaderInterface::setAdapter(long adapter, DemodulatorType type)
{
    this->m_adapter = adapter;
    this->m_type = type;
}

void RadioReaderInterface::getAdapter(long *adapter, DemodulatorType *type) const
{
    *adapter = this->m_adapter;
    *type = this->m_type;
}

void RadioReaderInterface::reset()
{
    this->m_adapter = -1;
    this->m_type = DT_NONE;
}
