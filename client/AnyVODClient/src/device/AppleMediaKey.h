﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QThread>
#include <QThreadPool>

#import <Cocoa/Cocoa.h>
#import <IOKit/hidsystem/ev_keymap.h>

class AppleMediaKey : public QThread
{
public:
    AppleMediaKey();
    ~AppleMediaKey();

    bool init();
    void deInit();

protected:
    virtual void run();

private:
    bool start();
    void stop();

    void startWatchingAppSwitching();
    void stopWatchingAppSwitching() const;

    bool startWatchingMediaKeys();
    void stopWatchingMediaKeys();

    void interceptMediaKeyEvents(bool onoff);
    void resetMediaKeyJumpLater();

    CGEventRef processTapEvent(CGEventTapProxy proxy, CGEventType type, CGEventRef event);
    void handleMediaKeyEvent(NSEvent *event);
    void handleMediaKeyAppListChanged();
    void handleFrontMostAppChanged(NSDictionary *userInfo);
    void handleAppTerminated(NSDictionary *userInfo);

    NSArray* getMediaKeyUserBundleIdentifiers() const;

private:
    static const int SYSTEM_DEFINED_EVENT_MEDIA_KEYS;

private:
    static CGEventRef tapEventCallback(CGEventTapProxy proxy, CGEventType type, CGEventRef event, void *refcon);

private:
    CFMachPortRef m_eventPort;
    CFRunLoopSourceRef m_eventPortSource;
    CFRunLoopRef m_tapThreadRunLoop;
    NSMutableArray<NSRunningApplication*> *m_mediaKeyAppList;
    bool m_interceptMediaKeyEvents;
    bool m_mediaKeyJustJumped;
    QThreadPool m_eventThreadPool;
    id m_appActivatedObj;
    id m_appTerminatedObj;
};
