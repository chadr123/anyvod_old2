﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RadioChannelMap.h"

#include <QStringList>

RadioChannelMap::RadioChannelMap()
{
    this->addSouthKorea();

    this->m_country = this->getCountryCode(QLocale::SouthKorea, RadioReaderInterface::DT_WIDE_FM_MONO);
}

RadioChannelMap::~RadioChannelMap()
{

}

bool RadioChannelMap::setChannelCountry(QLocale::Country country, RadioReaderInterface::DemodulatorType type)
{
    QString code = this->getCountryCode(country, type);

    if (this->m_channel.contains(code))
    {
        this->m_country = code;
        return true;
    }

    return false;
}

void RadioChannelMap::getChannelCountry(QLocale::Country *country, RadioReaderInterface::DemodulatorType *type) const
{
    QStringList list = this->m_country.split("-");

    *country = (QLocale::Country)list[0].toInt();
    *type = (RadioReaderInterface::DemodulatorType)list[1].toInt();
}

void RadioChannelMap::getChannels(QVector<Channel> *ret) const
{
    *ret = this->m_channel[this->m_country];
}

bool RadioChannelMap::getChannel(int index, Channel *ret) const
{
    bool found = false;
    QVector<Channel> channels;

    this->getChannels(&channels);

    if (index < channels.count())
    {
        *ret = channels[index];
        found = true;
    }

    return found;
}

QString RadioChannelMap::getCountryCode(QLocale::Country country, RadioReaderInterface::DemodulatorType type) const
{
    return QString("%1-%2").arg((int)country).arg((int)type);
}

void RadioChannelMap::addSouthKorea()
{
    QVector<RadioChannelMap::Channel> channels;

    channels.clear();

    for (uint64_t freq = 88000000, i = 1; freq <= 108000000; freq += 100000, i++)
        channels.append(Channel(int(i), freq, 75.0e-6));

    this->m_channel[this->getCountryCode(QLocale::SouthKorea, RadioReaderInterface::DT_WIDE_FM_MONO)] = channels;
    this->m_channel[this->getCountryCode(QLocale::SouthKorea, RadioReaderInterface::DT_WIDE_FM_STEREO)] = channels;
}
