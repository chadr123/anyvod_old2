﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "RadioReaderInterface.h"

#include <QString>
#include <QMap>
#include <QVector>
#include <QLocale>

#include <stdint.h>

class RadioChannelMap
{
public:
    struct Channel
    {
        Channel() :
            channel(0),
            freq(0),
            emphasis(0.0)
        {

        }

        Channel(int ichannel, uint64_t ifreq, double iemphasis) :
            channel(ichannel),
            freq(ifreq),
            emphasis(iemphasis)
        {

        }

        int channel;
        uint64_t freq;
        double emphasis;
    };

public:
    RadioChannelMap();
    ~RadioChannelMap();

    bool setChannelCountry(QLocale::Country country, RadioReaderInterface::DemodulatorType type);
    void getChannelCountry(QLocale::Country *country, RadioReaderInterface::DemodulatorType *type) const;
    void getChannels(QVector<Channel> *ret) const;
    bool getChannel(int index, Channel *ret) const;

private:
    QString getCountryCode(QLocale::Country country, RadioReaderInterface::DemodulatorType type) const;
    void addSouthKorea();

private:
    QString m_country;
    QMap<QString, QVector<Channel>> m_channel;
};
