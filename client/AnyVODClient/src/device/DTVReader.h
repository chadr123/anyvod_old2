﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

extern "C"
{
# include <libavformat/avio.h>
# include <libavformat/url.h>
}

#include "DTVReaderInterface.h"
#include "DTVChannelMap.h"

#include <QMetaType>

class DTVReader
{
public:
    struct ChannelInfo
    {
        ChannelInfo()
        {
            adapter = -1;
            type = DTVReaderInterface::ST_NONE;
            country = QLocale::AnyCountry;
            index = -1;
            channel = 0;
            signal = 0.0f;
        }

        long adapter;
        DTVReaderInterface::SystemType type;
        QLocale::Country country;
        int index;
        int channel;
        float signal;
        QString name;
    };

private:
    DTVReader();
    ~DTVReader();

public:
    static DTVReader& getInstance();

public:
    bool isSupport() const;

    bool setChannelCountry(QLocale::Country country, DTVReaderInterface::SystemType type);
    void getChannelCountry(QLocale::Country *country, DTVReaderInterface::SystemType *type) const;
    void getChannels(QVector<DTVChannelMap::Channel> *ret) const;

    bool isOpened() const;
    unsigned int enumSystems();
    float getSignalStrength() const;
    float getSignalNoiseRatio() const;
    QString getChannelName();
    void getEPG(QVector<DTVReaderInterface::EPG> *ret);
    QDateTime getCurrentDateTime();

    void getAdapter(long *adapter, DTVReaderInterface::SystemType *type) const;
    bool getAdapterInfo(long adapter, DTVReaderInterface::AdapterInfo *ret) const;
    bool getAdapterList(QVector<DTVReaderInterface::AdapterInfo> *ret) const;
    bool scan(long adapter, QLocale::Country country, DTVReaderInterface::SystemType type, const DTVChannelMap::Channel &channel, int index, ChannelInfo *ret);

    void setScannedChannels(const QVector<ChannelInfo> &list);
    void getScannedChannels(QVector<ChannelInfo> *ret) const;

    QString systemTypeToString(DTVReaderInterface::SystemType type) const;

public:
    static URLProtocol* getProtocol();

#if !defined Q_OS_ANDROID && !defined Q_OS_IOS
    static bool determinDTV(const QString &path);
    static QString makeDTVPath(const DTVReader::ChannelInfo &info);
#endif

public:
    static const QString DTV_PROTOCOL;

private:
    void setAdapter(long adapter, DTVReaderInterface::SystemType type);
    bool tune(QLocale::Country country, DTVReaderInterface::SystemType type, int index);

    bool setDefaultInversion();

    /* DVB-C */
    bool setDefaultDVBC(const DTVChannelMap::Channel &channelInfo);
    bool setDefaultDVBC2(const DTVChannelMap::Channel &channelInfo);

    /* DVB-S */
    bool setDefaultDVBS(const DTVChannelMap::Channel &channelInfo);
    bool setDefaultDVBS2(const DTVChannelMap::Channel &channelInfo);
    bool setDefaultSEC(const DTVChannelMap::Channel &channelInfo);

    /* DVB-T */
    bool setDefaultDVBT(const DTVChannelMap::Channel &channelInfo);
    bool setDefaultDVBT2(const DTVChannelMap::Channel &channelInfo);

    /* ATSC */
    bool setDefaultATSC(const DTVChannelMap::Channel &channelInfo);
    bool setDefaultCQAM(const DTVChannelMap::Channel &channelInfo);

    /* ISDB-C */
    bool setDefaultISDBC(const DTVChannelMap::Channel &channelInfo);

    /* ISDB-S */
    bool setDefaultISDBS(const DTVChannelMap::Channel &channelInfo);

    /* ISDB-T */
    bool setDefaultISDBT(const DTVChannelMap::Channel &channelInfo);

private:
    static int open(URLContext *h, const char *uri, int);
    static int read(URLContext *h, uint8_t *buf, int size);
    static int close(URLContext *h);

private:
    static URLProtocol PROTOCOL;
    static const char DTV_PROTOCOL_NAME[];

private:
    DTVReaderInterface *m_reader;
    AVIOInterruptCB m_intCallback;
    DTVChannelMap m_channelMap;
    QVector<ChannelInfo> m_scannedChannel;
    bool m_isOpened;
};

Q_DECLARE_METATYPE(DTVReader::ChannelInfo)
QDataStream& operator << (QDataStream &out, const DTVReader::ChannelInfo &item);
QDataStream& operator >> (QDataStream &in, DTVReader::ChannelInfo &item);
