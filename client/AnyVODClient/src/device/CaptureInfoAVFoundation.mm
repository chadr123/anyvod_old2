﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "CaptureInfoAVFoundation.h"

#include <QVector>

#import <AVFoundation/AVFoundation.h>

CaptureInfoAVFoundation::CaptureInfoAVFoundation()
{

}

CaptureInfoAVFoundation::~CaptureInfoAVFoundation()
{

}

void CaptureInfoAVFoundation::getVideoDevices(QVector<CaptureInfo::Info> *ret) const
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    int index = 0;

    for (AVCaptureDevice *device in devices)
    {
        CaptureInfo::Info info;

        info.desc = [[device localizedName] UTF8String];

        index = [devices indexOfObject:device];
        info.name = QString::number(index);

        ret->append(info);

        index++;
    }

    uint32_t screenCount;

    CGGetActiveDisplayList(0, nullptr, &screenCount);

    if (screenCount > 0)
    {
        CGDirectDisplayID screens[screenCount];

        CGGetActiveDisplayList(screenCount, screens, &screenCount);

        for (uint32_t i = 0; i < screenCount; i++)
        {
            CaptureInfo::Info info;

            info.desc = QString("Capture screen%1").arg(i);
            info.name = QString::number(index + i);

            ret->append(info);
        }
    }

    [pool release];
}

void CaptureInfoAVFoundation::getAudioDevices(QVector<CaptureInfo::Info> *ret) const
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio];

    for (AVCaptureDevice *device in devices)
    {
        CaptureInfo::Info info;

        info.desc = [[device localizedName] UTF8String];
        info.name = QString::number([devices indexOfObject:device]);

        ret->append(info);
    }

    [pool release];
}
