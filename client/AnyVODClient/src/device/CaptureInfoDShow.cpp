﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "CaptureInfoDShow.h"
#include "utils/COMUtils.h"

#include <QVector>

#define WIN32_LEAN_AND_MEAN
#define NO_DSHOW_STRSAFE

#include <dshow.h>

CaptureInfoDShow::CaptureInfoDShow()
{
    COMUtils::initCOM();
}

CaptureInfoDShow::~CaptureInfoDShow()
{
    COMUtils::unInitCOM();
}

void CaptureInfoDShow::getVideoDevices(QVector<CaptureInfo::Info> *ret) const
{
    this->getDevices(true, ret);
}

void CaptureInfoDShow::getAudioDevices(QVector<CaptureInfo::Info> *ret) const
{
    this->getDevices(false, ret);
}

void CaptureInfoDShow::getDevices(bool isVideo, QVector<CaptureInfo::Info> *ret) const
{
    HRESULT r;
    ICreateDevEnum *devEnum = nullptr;

    r = CoCreateInstance(CLSID_SystemDeviceEnum, nullptr, CLSCTX_INPROC_SERVER, IID_ICreateDevEnum, (LPVOID*)&devEnum);

    if (r != S_OK)
        return;

    IEnumMoniker *classEnum = nullptr;
    IMoniker *m = nullptr;
    const GUID &guid = isVideo ? CLSID_VideoInputDeviceCategory : CLSID_AudioInputDeviceCategory;

    r = devEnum->CreateClassEnumerator(guid, (IEnumMoniker**)&classEnum, 0);

    if (r != S_OK)
        return;

    while (classEnum->Next(1, &m, nullptr) == S_OK)
    {
        CaptureInfo::Info info;
        IPropertyBag *bag = nullptr;
        VARIANT var;
        IBindCtx *bindCtx = nullptr;
        LPOLESTR oleStr = nullptr;
        LPMALLOC coMalloc = nullptr;

        r = CoGetMalloc(MEMCTX_TASK, &coMalloc);

        if (r != S_OK)
            goto fail;

        r = CreateBindCtx(0, &bindCtx);

        if (r != S_OK)
            goto fail;

        r = m->GetDisplayName(bindCtx, nullptr, &oleStr);

        if (r != S_OK)
            goto fail;

        info.name = QString::fromWCharArray(oleStr);
        info.name = info.name.replace(":", "_");

        r = m->BindToStorage(0, 0, IID_IPropertyBag, (void**)&bag);

        if (r != S_OK)
            goto fail;

        var.vt = VT_BSTR;
        r = bag->Read(L"FriendlyName", &var, nullptr);

        if (r != S_OK)
            goto fail;

        info.desc = QString::fromWCharArray(var.bstrVal);

fail:
        VariantClear(&var);

        if (oleStr && coMalloc)
            coMalloc->Free(oleStr);

        if (bindCtx)
            bindCtx->Release();

        if (bag)
            bag->Release();

        m->Release();

        if (coMalloc)
            coMalloc->Release();

        if (!info.desc.isEmpty() || !info.name.isEmpty())
            ret->append(info);
    }

    classEnum->Release();
}
