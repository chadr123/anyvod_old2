﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "ScreenSaverPreventerInterface.h"

#include <dbus/dbus.h>

class LinuxPreventer : public ScreenSaverPreventerInterface
{
public:
    LinuxPreventer();
    virtual ~LinuxPreventer();

    virtual void enable();
    virtual void disable();

private:
    enum ScreenSaverAPI
    {
        SSAPI_FDO_SS,
        SSAPI_FDO_PM,
        SSAPI_MATE,
        SSAPI_GNOME,
        SSAPI_COUNT
    };

private:
    bool debusInit();
    void debusDeinit();

    DBusMessage* createMessage(bool enable);
    void freeMessage(DBusMessage *msg);

private:
    static const char DBUS_SERVICE[][40];
    static const char DBUS_PATH[][33];

private:
    DBusConnection *m_dbusConn;
    DBusPendingCall *m_dbusPending;
    dbus_uint32_t m_dbusCookie;
    ScreenSaverAPI m_screenSaverAPI;
};
