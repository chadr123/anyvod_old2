﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "LinuxPreventer.h"
#include "core/Common.h"

const char LinuxPreventer::DBUS_SERVICE[][40] =
{
    "org.freedesktop.ScreenSaver",
    "org.freedesktop.PowerManagement.Inhibit",
    "org.mate.SessionManager",
    "org.gnome.SessionManager",
};

const char LinuxPreventer::DBUS_PATH[][33] =
{
    "/ScreenSaver",
    "/org/freedesktop/PowerManagement",
    "/org/mate/SessionManager",
    "/org/gnome/SessionManager",
};

LinuxPreventer::LinuxPreventer()
{
    if (!this->debusInit())
        this->debusDeinit();
}

LinuxPreventer::~LinuxPreventer()
{
    this->debusDeinit();
}

void LinuxPreventer::enable()
{
    DBusMessage *msg = this->createMessage(true);

    if (msg == nullptr)
        return;

    if (!dbus_message_append_args(msg, DBUS_TYPE_UINT32, &this->m_dbusCookie, DBUS_TYPE_INVALID) ||
            !dbus_connection_send(this->m_dbusConn, msg, nullptr))
    {
        this->m_dbusCookie = 0;
    }

    this->freeMessage(msg);
}

void LinuxPreventer::disable()
{
    DBusMessage *msg = this->createMessage(false);

    if (msg == nullptr)
        return;

    const char *app = APP_NAME;
    const char *reason = "playback";
    dbus_bool_t ret;

    switch (this->m_screenSaverAPI)
    {
        case SSAPI_MATE:
        case SSAPI_GNOME:
        {
            dbus_uint32_t xid = 0;
            dbus_uint32_t gflags = 0xC;

            ret = dbus_message_append_args(msg, DBUS_TYPE_STRING, &app, DBUS_TYPE_UINT32, &xid,
                                           DBUS_TYPE_STRING, &reason, DBUS_TYPE_UINT32, &gflags,
                                           DBUS_TYPE_INVALID);
            break;
        }
        default:
        {
            ret = dbus_message_append_args(msg, DBUS_TYPE_STRING, &app, DBUS_TYPE_STRING, &reason,
                                           DBUS_TYPE_INVALID);
            break;
        }
    }

    if (!ret || !dbus_connection_send_with_reply(this->m_dbusConn, msg, &this->m_dbusPending, -1))
        this->m_dbusPending = nullptr;

    this->freeMessage(msg);
}

bool LinuxPreventer::debusInit()
{
    DBusError err;

    this->m_dbusPending = nullptr;
    this->m_dbusCookie = 0;

    dbus_error_init(&err);

    this->m_dbusConn = dbus_bus_get_private(DBUS_BUS_SESSION, &err);

    if (this->m_dbusConn == nullptr)
    {
        dbus_error_free(&err);
        return false;
    }

    for (int i = 0; i < SSAPI_COUNT; i++)
    {
        if (dbus_bus_name_has_owner(this->m_dbusConn, DBUS_SERVICE[i], nullptr))
        {
            this->m_screenSaverAPI = (ScreenSaverAPI)i;
            return true;
        }
    }

    return false;
}

void LinuxPreventer::debusDeinit()
{
    if (this->m_dbusPending != nullptr)
    {
        dbus_pending_call_cancel(this->m_dbusPending);
        dbus_pending_call_unref(this->m_dbusPending);

        this->m_dbusPending = nullptr;
    }

    if (this->m_dbusConn)
    {
        dbus_connection_close(this->m_dbusConn);
        dbus_connection_unref(this->m_dbusConn);

        this->m_dbusConn = nullptr;
    }
}

DBusMessage* LinuxPreventer::createMessage(bool enable)
{
    if (this->m_dbusConn == nullptr)
        return nullptr;

    if (this->m_dbusPending != nullptr)
    {
        DBusMessage *reply;

        dbus_pending_call_block(this->m_dbusPending);
        reply = dbus_pending_call_steal_reply(this->m_dbusPending);
        dbus_pending_call_unref(this->m_dbusPending);
        this->m_dbusPending = nullptr;

        if (reply != nullptr)
        {
            if (!dbus_message_get_args(reply, nullptr, DBUS_TYPE_UINT32, &this->m_dbusCookie, DBUS_TYPE_INVALID))
                this->m_dbusCookie = 0;

            dbus_message_unref(reply);
        }
    }

    const char *method = enable ? "UnInhibit" : "Inhibit";
    ScreenSaverAPI api = this->m_screenSaverAPI;

    return dbus_message_new_method_call(DBUS_SERVICE[api], DBUS_PATH[api], DBUS_SERVICE[api], method);
}

void LinuxPreventer::freeMessage(DBusMessage *msg)
{
    dbus_connection_flush(this->m_dbusConn);
    dbus_message_unref(msg);
}
