﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include <QGlobalStatic>

#include "ScreenSaverPreventer.h"
#include "ScreenSaverPreventerInterface.h"

#ifdef Q_OS_WIN
# include "WinPreventer.h"
#elif defined Q_OS_MAC && !defined Q_OS_MOBILE
# include "MacPreventer.h"
#elif defined Q_OS_LINUX && !defined Q_OS_MOBILE
# include "LinuxPreventer.h"
#elif defined Q_OS_RASPBERRY_PI
# include "../../AnyVODMobileClient/src/screensaver/RaspberryPiPreventer.h"
#elif defined Q_OS_ANDROID
# include "../../AnyVODMobileClient/src/screensaver/AndroidPreventer.h"
#elif defined Q_OS_IOS
# include "../../AnyVODMobileClient/src/screensaver/IOSPreventer.h"
#endif

ScreenSaverPreventer::ScreenSaverPreventer()
{
#ifdef Q_OS_WIN
    this->m_preventer = new WinPreventer;
#elif defined Q_OS_MAC && !defined Q_OS_MOBILE
    this->m_preventer = new MacPreventer;
#elif defined Q_OS_LINUX && !defined Q_OS_MOBILE
    this->m_preventer = new LinuxPreventer;
#elif defined Q_OS_RASPBERRY_PI
    this->m_preventer = new RaspberryPiPreventer;
#elif defined Q_OS_ANDROID
    this->m_preventer = new AndroidPreventer;
#elif defined Q_OS_IOS
    this->m_preventer = new IOSPreventer;
#else
    this->m_preventer = nullptr;
#endif
}

ScreenSaverPreventer::~ScreenSaverPreventer()
{
    delete this->m_preventer;
}

void ScreenSaverPreventer::enable()
{
    if (this->m_preventer)
        this->m_preventer->enable();
}

void ScreenSaverPreventer::disable()
{
    if (this->m_preventer)
        this->m_preventer->disable();
}
