﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QVector>
#include <QStringList>

class QFile;

class PlayListParserInterface
{
public:
    struct PlayListItem
    {
        QString path;
        QString title;
    };

public:
    PlayListParserInterface();
    virtual ~PlayListParserInterface();

    bool open(const QString &filePath);
    void close();

    void setRoot(const QString &root);

    void getFileList(QVector<PlayListItem> *ret) const;

    bool isExist() const;
    bool isSuccess() const;

protected:
    virtual bool parse(QFile &file) = 0;

protected:
    bool openFromRemote(const QString &url);
    void clear();

    bool loadPlayList(const QString &filePath);
    void appendPlayList(const PlayListItem &item);

    void getRoot(QString *ret) const;

private:
    bool loadPlayListFromFile(const QString &filePath);
    void getLoadedPlayList(QStringList *ret) const;
    bool isValidPlayListExt(const QString &ext);

private:
    QVector<PlayListItem> m_list;
    QStringList m_loadedPlayList;
    bool m_success;
    QString m_root;
};
