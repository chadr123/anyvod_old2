﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ASXParser.h"
#include "utils/RemoteFileUtils.h"

#include <QFile>
#include <QFileInfo>
#include <QDomDocument>

ASXParser::ASXParser()
{

}

bool ASXParser::parse(QFile &file)
{
    QDomDocument doc;

    if (!doc.setContent(&file))
        return false;

    QDomElement root = doc.documentElement();

    for (QDomNode node = root.firstChild(); !node.isNull(); node = node.nextSibling())
    {
        QString name = node.nodeName().toLower();
        QDomElement elem = node.toElement();

        if (name == "entry")
        {
            PlayListParserInterface::PlayListItem item;

            for (QDomNode entryNode = elem.firstChild(); !entryNode.isNull(); entryNode = entryNode.nextSibling())
            {
                QString entryNodeName = entryNode.nodeName().toLower();
                QDomElement nodeElem = entryNode.toElement();

                if (entryNodeName == "ref")
                {
                    QDomNamedNodeMap attrs = nodeElem.attributes();

                    for (int i = 0; i < attrs.count(); i++)
                    {
                        QDomNode attrsNode = attrs.item(i);
                        QString attrsNodeName = attrsNode.nodeName().toLower();

                        if (attrsNodeName == "href")
                            item.path = attrsNode.nodeValue();
                    }
                }
                else if (entryNodeName == "title")
                {
                    item.title = nodeElem.text();
                }
            }

            this->appendPlayList(item);
        }
        else if (name == "entryref")
        {
            QDomNamedNodeMap attrs = node.toElement().attributes();

            for (int i = 0; i < attrs.count(); i++)
            {
                QDomNode attrsNode = attrs.item(i);
                QString attrsNodeName = attrsNode.nodeName().toLower();

                if (attrsNodeName == "href")
                {
                    QString rootPath;
                    QString value = attrsNode.nodeValue();
                    QFileInfo info(value);

                    if (!RemoteFileUtils::determinRemoteProtocol(value) && !info.isAbsolute())
                        this->getRoot(&rootPath);

                    this->loadPlayList(rootPath + value);
                }
            }
        }
    }

    return true;
}
