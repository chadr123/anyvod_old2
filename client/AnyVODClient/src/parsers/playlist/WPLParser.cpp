﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "WPLParser.h"

#include <QFile>
#include <QDomDocument>

WPLParser::WPLParser()
{

}

bool WPLParser::parse(QFile &file)
{
    QDomDocument doc;

    if (!doc.setContent(&file))
        return false;

    QDomElement root = doc.documentElement();
    QDomElement body = root.firstChildElement("body");
    QDomElement seq = body.firstChildElement("seq");
    QDomElement media = seq.firstChildElement("media");

    while (!media.isNull())
    {
        PlayListParserInterface::PlayListItem item;

        item.path = media.attribute("src");

        this->appendPlayList(item);

        media = media.nextSiblingElement("media");
    }

    return true;
}
