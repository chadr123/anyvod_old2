﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "SRTParser.h"

#include <QString>
#include <QMutex>
#include <QMap>

class QBuffer;

class YouTubeParser
{
public:
    YouTubeParser();
    ~YouTubeParser();

    bool open(const QString &path);
    void close();

    bool save(const QString &path, double sync);

    bool get(int32_t millisecond, QVector<SRTParser::Item> *ret);
    bool isExist();

    void getDefaultLanguage(QString *ret);
    void setDefaultLanguage(const QString &lang);
    void getLanguages(QStringList *ret);

private:
    bool getManualSubtitles(const QString &id);

    bool getAutoSubtitles(const QString &id);
    bool getTTSSubtitlesFromTTSURL(const QString &content);
    bool getTTSSubtitlesFromJson(const QString &content, const QString &id);

    bool getTTSUrl(const QString &content, QString *url, QString *listUrl, QString *timestamp) const;

protected:
    SRTParser* getSRTParser(QBuffer &subtitle) const;

private:
    struct Item
    {
        QString url;
        QString code;
        SRTParser* parser;
    };

private:
    static const QString SUBTITLE_INFO_URL;
    static const QString SUBTITLE_URL;

private:
    QString m_curLanguage;
    QMap<QString, Item> m_subtitles;
    QMutex m_lock;
};
