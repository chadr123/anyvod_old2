﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <ass/ass.h>

#include <QString>
#include <QMutex>
#include <QHash>

class ASSParser
{
public:
    ASSParser();

    void init();
    void deInit();

    bool open(const QString &path);
    void close();

    void setHeader(const QString &header);
    void setFrameSize(const int width, const int height);

    void clearFonts();

    void setDefaultFont(const QString &family);
    void setFontPath(const QString &path);
    void getFontPath(QString *ret);
    void addFont(const QString &name, uint8_t *data, int size);

    bool get(const int32_t millisecond, ASS_Image **ret, bool *changed);
    bool getSingle(const int32_t millisecond, ASS_Image **ret, bool *changed);

    void parseSingle(const char *ass);

    bool isExist();

private:
    bool getImage(ASS_Track *track, const int32_t millisecond, ASS_Image **ret, bool *changed);
    void freeStyle(ASS_Track *track);

private:
    static void messageCallback(int, const char *, va_list, void *);

private:
    QMutex m_lock;
    QHash<QString, QString> m_singleLines;
    QString m_fontPath;
    bool m_init;
    ASS_Library *m_lib;
    ASS_Renderer *m_renderer;
    ASS_Track *m_singleTrack;
    ASS_Track *m_fileTrack;
};
