﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "NaverTVParser.h"
#include "net/HttpDownloader.h"
#include "pickers/NaverTVURLPicker.h"
#include "utils/PlayItemUtils.h"

#include <QBuffer>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>

NaverTVParser::NaverTVParser()
{

}

NaverTVParser::~NaverTVParser()
{
    this->close();
}

bool NaverTVParser::open(const QString &path)
{
    bool ok = false;
    NaverTVURLPicker picker;
    QStringList list = path.split(PlayItemUtils::URL_PICKER_SEPARATOR, Qt::SkipEmptyParts);

    if (list.length() != 2)
        return false;

    this->close();

    ok = picker.isSupported(list.last());

    if (!ok)
        return false;

    QString id = list.first();

    ok = this->getSubtitles(id);

    if (ok)
        this->setDefaultLanguage(this->m_curLanguage);

    return this->isExist();
}

void NaverTVParser::close()
{
    QMutexLocker locker(&this->m_lock);

    for (const Item &item : qAsConst(this->m_subtitles))
    {
        if (item.parser)
        {
            item.parser->close();
            delete item.parser;
        }
    }

    this->m_subtitles.clear();
    this->m_curLanguage.clear();
}

bool NaverTVParser::save(const QString &path, double sync)
{
    QMutexLocker locker(&this->m_lock);

    if (this->m_curLanguage.isEmpty())
        return false;

    if (this->m_subtitles.contains(this->m_curLanguage))
    {
        Item item = this->m_subtitles[this->m_curLanguage];

        if (item.parser)
            return item.parser->save(path, sync);
    }

    return false;
}

bool NaverTVParser::get(int32_t millisecond, QVector<SRTParser::Item> *ret)
{
    QMutexLocker locker(&this->m_lock);

    if (this->m_curLanguage.isEmpty())
        return false;

    if (this->m_subtitles.contains(this->m_curLanguage))
    {
        Item item = this->m_subtitles[this->m_curLanguage];

        if (item.parser)
            return item.parser->get(millisecond, ret);
    }

    return false;
}

bool NaverTVParser::isExist()
{
    QMutexLocker locker(&this->m_lock);

    return !this->m_subtitles.isEmpty();
}

void NaverTVParser::getDefaultLanguage(QString *ret)
{
    QMutexLocker locker(&this->m_lock);

    *ret = this->m_curLanguage;
}

void NaverTVParser::setDefaultLanguage(const QString &lang)
{
    QMutexLocker locker(&this->m_lock);
    QBuffer subtitle;
    HttpDownloader d;

    if (lang.isEmpty())
        return;

    Item item = this->m_subtitles[lang];

    this->m_curLanguage = lang;

    if (!item.parser)
    {
        if (!d.download(item.url, subtitle))
            return;

        item.parser = this->getSRTParser(subtitle);

        if (item.parser)
            this->m_subtitles[lang] = item;
    }
}

void NaverTVParser::getLanguages(QStringList *ret)
{
    QMutexLocker locker(&this->m_lock);

    *ret = this->m_subtitles.keys();
}

bool NaverTVParser::getSubtitles(const QString &id)
{
    NaverTVURLPicker picker;
    QJsonObject root = picker.getRootObject("https://tv.naver.com/v/" + id, NaverTVURLPicker::VT_NONE);

    if (root.isEmpty())
        return false;

    QJsonObject captions = root["captions"].toObject();
    QString defaultLang = captions["captionLang"].toString();
    const QJsonArray list = captions["list"].toArray();

    for (const QJsonValue &elem : list)
    {
        QJsonObject elemObj = elem.toObject();
        QString label = elemObj["label"].toString();
        QString locale = elemObj["locale"].toString();
        QString url = elemObj["source"].toString();
        Item item;

        item.url = url;
        item.code = locale;
        item.parser = nullptr;

        this->m_subtitles[label] = item;

        if (defaultLang == locale)
            this->m_curLanguage = label;
    }

    return true;
}
