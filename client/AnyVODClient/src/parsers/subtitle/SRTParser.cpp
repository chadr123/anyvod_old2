﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SRTParser.h"
#include "core/Common.h"
#include "core/TextEncodingDetector.h"
#include "utils/TimeUtils.h"
#include "utils/ConvertingUtils.h"
#include "../../../../common/types.h"

#include <QFile>
#include <QBuffer>
#include <QMutexLocker>
#include <QRegularExpression>
#include <QTextStream>
#include <QTime>
#include <QDebug>

SRTParser::SRTParser() :
    m_vttMode(false)
{

}

bool SRTParser::open(const QString &path)
{
    this->close();

    QFile file(path);
    QString codecName;
    TextEncodingDetector detector;

    if (!detector.getCodecName(path, &codecName))
        return false;

    if (!file.open(QIODevice::ReadOnly))
        return false;

    this->parse(file, codecName);
    stable_sort(this->m_items.begin(), this->m_items.end());

    return this->isExist();
}

bool SRTParser::save(const QString &path, double sync)
{
    QMutexLocker locker(&this->m_lock);
    QFile file(path);

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;

    QTextStream stream(&file);

    stream.setCodec("UTF-8");
    stream.setGenerateByteOrderMark(true);

    for (int i = 0; i < this->m_items.count(); i++)
    {
        const Item &item = this->m_items[i];

        stream << i + 1 << Qt::endl;

        QTime startTime = QTime::fromMSecsSinceStartOfDay(item.start + int(sync * 1000));
        QTime endTime = QTime::fromMSecsSinceStartOfDay(item.end + int(sync * 1000));

        stream << startTime.toString(ConvertingUtils::TIME_HH_MM_SS_ZZZ_COMMA);
        stream << " --> ";
        stream << endTime.toString(ConvertingUtils::TIME_HH_MM_SS_ZZZ_COMMA) << Qt::endl;

        stream << item.content << Qt::endl;

        if (stream.status() != QTextStream::Ok)
            return false;
    }

    stream.flush();

    if (stream.status() != QTextStream::Ok)
        return false;

    return true;
}

void SRTParser::close()
{
    QMutexLocker locker(&this->m_lock);

    this->m_items.clear();
}

bool SRTParser::get(int32_t millisecond, QVector<Item> *ret)
{
    QMutexLocker locker(&this->m_lock);
    int count = this->m_items.count();

    if (count > 0)
    {
        for (int i = 0; i < count; i++)
        {
            Item &item = this->m_items[i];

            if (item.start <= millisecond && millisecond < item.end)
                ret->append(item);
        }

        if (this->m_vttMode && ret->count() > 1)
        {
            Item merged;
            Item &first = ret->first();
            Item &last = ret->last();

            merged.start = first.start;
            merged.end = last.end;
            merged.halign = first.halign;
            merged.valign = first.valign;
            merged.rect = first.rect;
            merged.paragraph.className = first.paragraph.className;

            for (int i = 0; i < ret->count(); i++)
            {
                const Item &v = ret->at(i);

                merged.content += v.content;

                if (i < ret->count() - 1)
                    merged.content += NEW_LINE;

                merged.paragraph.lines.append(v.paragraph.lines);
            }

            ret->clear();
            ret->append(merged);
        }
    }

    return !ret->isEmpty();
}

bool SRTParser::isExist()
{
    QMutexLocker locker(&this->m_lock);

    return this->m_items.count() > 0;
}

bool SRTParser::parseTime(const QString &time, int *ret) const
{
    QTime tmp = QTime::fromString(time, ConvertingUtils::TIME_HH_MM_SS_ZZZ_COMMA);

    if (!tmp.isValid())
    {
        tmp = QTime::fromString(time, ConvertingUtils::TIME_HH_MM_SS_ZZZ);

        if (!tmp.isValid())
            return false;
    }

    *ret = -tmp.msecsTo(TimeUtils::ZERO_TIME);

    return true;
}

bool SRTParser::parseRect(const QString rect[4], QRect *ret) const
{
    for (int i = 0; i < 4; i++)
    {
        QStringList elements = rect[i].split(":", Qt::SkipEmptyParts);

        if (elements.count() < 2)
            return false;

        QString elem = elements[0].toLower();
        bool ok = false;
        int integer = elements[1].toInt(&ok);

        if (!ok)
            return false;

        if (elem == "x1")
            ret->setLeft(integer);
        else if (elem == "x2")
            ret->setRight(integer);
        else if (elem == "y1")
            ret->setTop(integer);
        else if (elem == "y2")
            ret->setBottom(integer);
        else
            return false;
    }

    return true;
}

bool SRTParser::parseTimeline(const QString &line, Item *ret) const
{
    QString time;
    QString tmp = line;
    QTextStream parser(&tmp);

    parser >> time;

    if (!this->parseTime(time, &ret->start))
        return false;

    parser >> time;
    parser >> time;

    if (!this->parseTime(time, &ret->end))
        return false;

    QString rect[4];

    for (int i = 0; i < 4; i++)
        parser >> rect[i];

    if (rect[0].isEmpty())
        return true;

    if (!this->parseRect(rect, &ret->rect))
        return false;

    return true;
}

QString SRTParser::parseAlign(const QString &para, SRTParser::Item *ret) const
{
    QRegularExpression exp(R"({\\an(\d)})");
    QRegularExpressionMatch match = exp.match(para);

    if (match.hasMatch())
    {
        int number = match.captured(1).toInt();

        switch (number)
        {
            case 1:
            {
                ret->halign = AnyVODEnums::HAM_LEFT;
                ret->valign = AnyVODEnums::VAM_BOTTOM;
                break;
            }
            case 2:
            {
                ret->halign = AnyVODEnums::HAM_MIDDLE;
                ret->valign = AnyVODEnums::VAM_BOTTOM;
                break;
            }
            case 3:
            {
                ret->halign = AnyVODEnums::HAM_RIGHT;
                ret->valign = AnyVODEnums::VAM_BOTTOM;
                break;
            }
            case 4:
            {
                ret->halign = AnyVODEnums::HAM_LEFT;
                ret->valign = AnyVODEnums::VAM_MIDDLE;
                break;
            }
            case 5:
            {
                ret->halign = AnyVODEnums::HAM_MIDDLE;
                ret->valign = AnyVODEnums::VAM_MIDDLE;
                break;
            }
            case 6:
            {
                ret->halign = AnyVODEnums::HAM_RIGHT;
                ret->valign = AnyVODEnums::VAM_MIDDLE;
                break;
            }
            case 7:
            {
                ret->halign = AnyVODEnums::HAM_LEFT;
                ret->valign = AnyVODEnums::VAM_TOP;
                break;
            }
            case 8:
            {
                ret->halign = AnyVODEnums::HAM_MIDDLE;
                ret->valign = AnyVODEnums::VAM_TOP;
                break;
            }
            case 9:
            {
                ret->halign = AnyVODEnums::HAM_RIGHT;
                ret->valign = AnyVODEnums::VAM_TOP;
                break;
            }
        }

        return QString(para).remove(exp);
    }

    ret->halign = AnyVODEnums::HAM_NONE;
    ret->valign = AnyVODEnums::VAM_NONE;

    return para;
}

QString SRTParser::parsePosition(const QString &para, QRect *ret) const
{
    QRegularExpression exp(R"({\\pos.*\((\d*),(\d*)\)})");
    QRegularExpressionMatch match = exp.match(para);

    if (match.hasMatch())
    {
        ret->setTopLeft(QPoint(match.captured(1).toInt(), match.captured(2).toInt()));
        ret->setBottomRight(QPoint(0, 0));

        return QString(para).remove(exp);
    }

    return para;
}

void SRTParser::parseText(const QString &para, SAMIParser::Paragraph *ret) const
{
    SAMIParser sami;
    QBuffer buf;
    QByteArray data = (QString("<Sync Start=0>") + para).toUtf8();

    buf.setBuffer(&data);
    sami.setProcessNewLine(true);

    if (sami.open(buf, "UTF-8"))
    {
        QString className;

        sami.getDefaultClassName(&className);
        sami.get(className, 0, ret);
    }
}

void SRTParser::parse(QFile &file, const QString &codecName)
{
    QMutexLocker locker(&this->m_lock);
    QTextStream stream(&file);

    stream.setCodec(codecName.toLatin1());

    while (true)
    {
        QString line = stream.readLine().trimmed();

        if (line.isNull())
            break;

        if (line.isEmpty())
            continue;

        if (line == "WEBVTT")
        {
            this->m_vttMode = true;
            continue;
        }

        Item item;

        if (!this->m_vttMode)
            line = stream.readLine().trimmed();

        if (!this->parseTimeline(line, &item))
            continue;

        while (true)
        {
            line = stream.readLine().trimmed();

            if (line.isEmpty())
                break;

            item.content += line + NEW_LINE;
        }

        QString para = item.content.left(item.content.length() - 1);

        para = this->parseAlign(para, &item);
        para = this->parsePosition(para, &item.rect);
        para.remove(QRegularExpression(R"({\\.*})"));

        this->parseText(para, &item.paragraph);

        this->m_items.append(item);
    }
}
