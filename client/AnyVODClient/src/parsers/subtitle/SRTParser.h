﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "core/AnyVODEnums.h"
#include "SAMIParser.h"

#include <stdint.h>
#include <QVector>
#include <QMutex>
#include <QRect>

class QFile;

class SRTParser
{
public:
    struct Item
    {
        AnyVODEnums::HAlignMethod halign;
        AnyVODEnums::VAlignMethod valign;
        QRect rect;
        int start;
        int end;
        SAMIParser::Paragraph paragraph;
        QString content;

        bool operator < (const Item &rhs) const
        {
            return start < rhs.start;
        }
    };

    SRTParser();

    bool open(const QString &path);
    void close();

    bool save(const QString &path, double sync);

    bool get(int32_t millisecond, QVector<Item> *ret);
    bool isExist();

private:
    void parse(QFile &file, const QString &codecName);
    bool parseRect(const QString rect[4], QRect *ret) const;
    bool parseTime(const QString &time, int *ret) const;
    bool parseTimeline(const QString &line, Item *ret) const;
    QString parseAlign(const QString &para, Item *ret) const;
    QString parsePosition(const QString &para, QRect *ret) const;
    void parseText(const QString &para, SAMIParser::Paragraph *ret) const;

private:
    QVector<Item> m_items;
    QMutex m_lock;
    bool m_vttMode;
};
