﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "DXVA2Decoder.h"

#include <QDebug>

extern "C"
{
# include <libavcodec/avcodec.h>
# include <libavutil/imgutils.h>
}

DEFINE_GUID(DXVA_NoEncrypt,                              0x1b81bed0, 0xa0c7, 0x11d3, 0xb9, 0x84, 0x00, 0xc0, 0x4f, 0x2e, 0x73, 0xc5);
DEFINE_GUID(DXVA2_ModeMPEG2and1_VLD,                     0x86695f12, 0x340e, 0x4f04, 0x9f, 0xd3, 0x92, 0x53, 0xdd, 0x32, 0x74, 0x60);
DEFINE_GUID(DXVA2_ModeH264_VLD_Multiview,                0x9901CCD3, 0xca12, 0x4b7e, 0x86, 0x7a, 0xe2, 0x22, 0x3d, 0x92, 0x55, 0xc3); // MVC
DEFINE_GUID(DXVA2_ModeH264_VLD_WithFMOASO_NoFGT,         0xd5f04ff9, 0x3418, 0x45d8, 0x95, 0x61, 0x32, 0xa7, 0x6a, 0xae, 0x2d, 0xdd);
DEFINE_GUID(DXVA2_ModeH264_VLD_Stereo_Progressive_NoFGT, 0xd79be8da, 0x0cf1, 0x4c81, 0xb8, 0x2a, 0x69, 0xa4, 0xe2, 0x36, 0xf4, 0x3d);
DEFINE_GUID(DXVA2_ModeH264_VLD_Stereo_NoFGT,             0xf9aaccbb, 0xc2b6, 0x4cfc, 0x87, 0x79, 0x57, 0x07, 0xb1, 0x76, 0x05, 0x52);
DEFINE_GUID(DXVA2_ModeH264_VLD_Multiview_NoFGT,          0x705b9d82, 0x76cf, 0x49d6, 0xb7, 0xe6, 0xac, 0x88, 0x72, 0xdb, 0x01, 0x3c);
DEFINE_GUID(DXVADDI_Intel_ModeH264_E,                    0x604F8E68, 0x4951, 0x4c54, 0x88, 0xFE, 0xAB, 0xD2, 0x5C, 0x15, 0xB3, 0xD6); // DXVA_Intel_H264_NoFGT_ClearVideo
DEFINE_GUID(DXVA_ModeH264_VLD_NoFGT_Flash,               0x4245F676, 0x2BBC, 0x4166, 0xa0, 0xBB, 0x54, 0xE7, 0xB8, 0x49, 0xC3, 0x80);
DEFINE_GUID(DXVA2_ModeVC1_D2010,                         0x1b81beA4, 0xa0c7, 0x11d3, 0xb9, 0x84, 0x00, 0xc0, 0x4f, 0x2e, 0x73, 0xc5); // August 2010 update
DEFINE_GUID(DXVA_Intel_VC1_ClearVideo,                   0xBCC5DB6D, 0xA2B6, 0x4AF0, 0xAC, 0xE4, 0xAD, 0xB1, 0xF7, 0x87, 0xBC, 0x89);
DEFINE_GUID(DXVA_Intel_VC1_ClearVideo_2,                 0xE07EC519, 0xE651, 0x4CD6, 0xAC, 0x84, 0x13, 0x70, 0xCC, 0xEE, 0xC8, 0x51);
DEFINE_GUID(DXVA2_ModeMPEG4pt2_VLD_Simple,               0xefd64d74, 0xc9e8, 0x41d7, 0xa5, 0xe9, 0xe9, 0xb0, 0xe3, 0x9f, 0xa3, 0x19);
DEFINE_GUID(DXVA2_ModeMPEG4pt2_VLD_AdvSimple_NoGMC,      0xed418a9f, 0x010d, 0x4eda, 0x9a, 0xe3, 0x9a, 0x65, 0x35, 0x8d, 0x8d, 0x2e);
DEFINE_GUID(DXVA2_ModeMPEG4pt2_VLD_AdvSimple_GMC,        0xab998b5b, 0x4258, 0x44a9, 0x9f, 0xeb, 0x94, 0xe5, 0x97, 0xa6, 0xba, 0xae);
DEFINE_GUID(DXVA_ModeMPEG4pt2_VLD_AdvSimple_Avivo,       0x7C74ADC6, 0xe2ba, 0x4ade, 0x86, 0xde, 0x30, 0xbe, 0xab, 0xb4, 0x0c, 0xc1);
DEFINE_GUID(DXVA2_ModeHEVC_VLD_Main,                     0x5b11d51b, 0x2f4c, 0x4452, 0xbc, 0xc3, 0x09, 0xf2, 0xa1, 0x16, 0x0c, 0xc0);
DEFINE_GUID(DXVA2_ModeHEVC_VLD_Main10,                   0x107af0e0, 0xef1a, 0x4d19, 0xab, 0xa8, 0x67, 0xa1, 0x63, 0x07, 0x3d, 0x13);
DEFINE_GUID(DXVA2_ModeVP9_VLD_Profile0,                  0x463707f8, 0xa1d0, 0x4585, 0x87, 0x6d, 0x83, 0xaa, 0x6d, 0x60, 0xb8, 0x9e);
DEFINE_GUID(DXVA2_ModeVP9_VLD_10bit_Profile2,            0xa4c749ef, 0x6ecf, 0x48aa, 0x84, 0x48, 0x50, 0xa7, 0xa1, 0x16, 0x5f, 0xf7);
DEFINE_GUID(DXVA2_ModeAV1_VLD_Profile_Main,              0xb8be4ccb, 0xcf53, 0x46ba, 0x8d, 0x59, 0xd6, 0xb8, 0xa6, 0xda, 0x5d, 0x2a);

const int DXVA2Decoder::PROFILE_MPEG2_SIMPLE[] =
{
    FF_PROFILE_MPEG2_SIMPLE,
    -1
};

const int DXVA2Decoder::PROFILE_MPEG2_MAIN[] =
{
    FF_PROFILE_MPEG2_SIMPLE,
    FF_PROFILE_MPEG2_MAIN,
    -1
};

const int DXVA2Decoder::PROFILE_H264_HIGH[] =
{
    FF_PROFILE_H264_BASELINE,
    FF_PROFILE_H264_CONSTRAINED_BASELINE,
    FF_PROFILE_H264_MAIN,
    FF_PROFILE_H264_HIGH,
    FF_PROFILE_H264_HIGH_10,
    FF_PROFILE_H264_HIGH_10_INTRA,
    -1
};

const int DXVA2Decoder::PROFILE_HEVC_MAIN[] =
{
    FF_PROFILE_HEVC_MAIN,
    -1
};

const int DXVA2Decoder::PROFILE_HEVC_MAIN10[] =
{
    FF_PROFILE_HEVC_MAIN,
    FF_PROFILE_HEVC_MAIN_10,
    -1
};

const int DXVA2Decoder::PROFILE_VP9_0[] =
{
    FF_PROFILE_VP9_0,
    -1
};

const int DXVA2Decoder::PROFILE_VP9_1[] =
{
    FF_PROFILE_VP9_1,
    -1
};

const int DXVA2Decoder::PROFILE_VP9_2[] =
{
    FF_PROFILE_VP9_2,
    -1
};

const int DXVA2Decoder::PROFILE_VP9_3[] =
{
    FF_PROFILE_VP9_3,
    -1
};

const int DXVA2Decoder::PROFILE_AV1_MAIN[] =
{
    FF_PROFILE_AV1_MAIN,
    -1
};

const DXVA2Decoder::DXVA2Mode DXVA2Decoder::MODES[] =
{
    /* MPEG2 */
    {"MPEG-2 & MPEG-1 variable-length decoder",                                      &DXVA2_ModeMPEG2and1_VLD,                       AV_CODEC_ID_MPEG2VIDEO, DXVA2Decoder::PROFILE_MPEG2_SIMPLE},
    {"MPEG-2 variable-length decoder",                                               &DXVA2_ModeMPEG2_VLD,                           AV_CODEC_ID_MPEG2VIDEO, DXVA2Decoder::PROFILE_MPEG2_MAIN},

    /* H.264 */
    {"H.264 variable-length decoder, film grain technology",                         &DXVA2_ModeH264_F,                              AV_CODEC_ID_H264,       DXVA2Decoder::PROFILE_H264_HIGH},
    {"H.264 variable-length decoder, no film grain technology (Intel ClearVideo)",   &DXVADDI_Intel_ModeH264_E,                      AV_CODEC_ID_H264,       DXVA2Decoder::PROFILE_H264_HIGH},
    {"H.264 variable-length decoder, no film grain technology",                      &DXVA2_ModeH264_E,                              AV_CODEC_ID_H264,       DXVA2Decoder::PROFILE_H264_HIGH},
    {"H.264 variable-length decoder, no film grain technology, FMO/ASO",             &DXVA2_ModeH264_VLD_WithFMOASO_NoFGT,           AV_CODEC_ID_H264,       DXVA2Decoder::PROFILE_H264_HIGH},
    {"H.264 variable-length decoder, no film grain technology, Flash",               &DXVA_ModeH264_VLD_NoFGT_Flash,                 AV_CODEC_ID_H264,       DXVA2Decoder::PROFILE_H264_HIGH},

    {"H.264 variable-length decoder, stereo progressive, no film grain technology",  &DXVA2_ModeH264_VLD_Stereo_Progressive_NoFGT,   AV_CODEC_ID_H264,       nullptr},
    {"H.264 variable-length decoder, stereo, no film grain technology",              &DXVA2_ModeH264_VLD_Stereo_NoFGT,               AV_CODEC_ID_H264,       nullptr},
    {"H.264 variable-length decoder, multiview, no film grain technology",           &DXVA2_ModeH264_VLD_Multiview_NoFGT,            AV_CODEC_ID_H264,       nullptr},

    /* VC-1 */
    {"VC-1 variable-length decoder",                                                 &DXVA2_ModeVC1_D,                               AV_CODEC_ID_VC1,        nullptr},
    {"VC-1 variable-length decoder",                                                 &DXVA2_ModeVC1_D,                               AV_CODEC_ID_WMV3,       nullptr},
    {"VC-1 variable-length decoder",                                                 &DXVA2_ModeVC1_D2010,                           AV_CODEC_ID_VC1,        nullptr},
    {"VC-1 variable-length decoder",                                                 &DXVA2_ModeVC1_D2010,                           AV_CODEC_ID_WMV3,       nullptr},
    {"VC-1 variable-length decoder (Intel ClearVideo 2)",                            &DXVA_Intel_VC1_ClearVideo_2,                   AV_CODEC_ID_VC1,        nullptr},
    {"VC-1 variable-length decoder (Intel ClearVideo 2)",                            &DXVA_Intel_VC1_ClearVideo_2,                   AV_CODEC_ID_WMV3,       nullptr},
    {"VC-1 variable-length decoder (Intel ClearVideo)",                              &DXVA_Intel_VC1_ClearVideo,                     AV_CODEC_ID_VC1,        nullptr},
    {"VC-1 variable-length decoder (Intel ClearVideo)",                              &DXVA_Intel_VC1_ClearVideo,                     AV_CODEC_ID_WMV3,       nullptr},

    /* Xvid/Divx */
    {"MPEG-4 Part 2 variable-length decoder, Simple Profile",                        &DXVA2_ModeMPEG4pt2_VLD_Simple,                 AV_CODEC_ID_MPEG4,      nullptr},
    {"MPEG-4 Part 2 variable-length decoder, Simple&Advanced Profile, no GMC",       &DXVA2_ModeMPEG4pt2_VLD_AdvSimple_NoGMC,        AV_CODEC_ID_MPEG4,      nullptr},
    {"MPEG-4 Part 2 variable-length decoder, Simple&Advanced Profile, GMC",          &DXVA2_ModeMPEG4pt2_VLD_AdvSimple_GMC,          AV_CODEC_ID_MPEG4,      nullptr},
    {"MPEG-4 Part 2 variable-length decoder, Simple&Advanced Profile, Avivo",        &DXVA_ModeMPEG4pt2_VLD_AdvSimple_Avivo,         AV_CODEC_ID_MPEG4,      nullptr},

    /* HEVC/H.265 */
    {"HEVC/H.265 variable-length decoder, Main Profile",                             &DXVA2_ModeHEVC_VLD_Main,                       AV_CODEC_ID_HEVC,       DXVA2Decoder::PROFILE_HEVC_MAIN},
    {"HEVC/H.265 variable-length decoder, Main10 Profile",                           &DXVA2_ModeHEVC_VLD_Main10,                     AV_CODEC_ID_HEVC,       DXVA2Decoder::PROFILE_HEVC_MAIN10},

    /* VP9 */
    {"VP9 variable-length decoder, Profile0",                                        &DXVA2_ModeVP9_VLD_Profile0,                    AV_CODEC_ID_VP9,        DXVA2Decoder::PROFILE_VP9_0},
    {"VP9 variable-length decoder, Profile2",                                        &DXVA2_ModeVP9_VLD_10bit_Profile2,              AV_CODEC_ID_VP9,        DXVA2Decoder::PROFILE_VP9_2},

    /* AV1 */
    {"AV1 variable-length decoder, Main Profile",                                    &DXVA2_ModeAV1_VLD_Profile_Main,                AV_CODEC_ID_AV1,        DXVA2Decoder::PROFILE_AV1_MAIN},

    {nullptr, nullptr, AV_CODEC_ID_NONE, nullptr}
};

const DXVA2Decoder::D3DFormat DXVA2Decoder::D3D_FORMATS[] =
{
    {"YV12", (D3DFORMAT)MAKEFOURCC('Y','V','1','2'), AV_PIX_FMT_YUV420P},
    {"NV12", (D3DFORMAT)MAKEFOURCC('N','V','1','2'), AV_PIX_FMT_NV12},
    {"IMC3", (D3DFORMAT)MAKEFOURCC('I','M','C','3'), AV_PIX_FMT_YUV420P},
    {"P010", (D3DFORMAT)MAKEFOURCC('P','0','1','0'), AV_PIX_FMT_P010},

    {nullptr, D3DFMT_UNKNOWN, AV_PIX_FMT_NONE}
};

DXVA2Decoder::DXVA2Decoder() :
    m_initialRefCount(0)
{

}

DXVA2Decoder::~DXVA2Decoder()
{
    this->closeInternal();
}

bool DXVA2Decoder::open(AVCodecContext *codec)
{
    Context &context = this->m_context;

    if (!this->loadDLLs())
    {
        this->close();
        return false;
    }

    if (!this->createDeviceEx())
    {
        if (!this->createDevice())
        {
            this->close();
            return false;
        }
    }

    if (!this->createDeviceManager())
    {
        this->close();
        return false;
    }

    if (!this->createVideoService())
    {
        this->close();
        return false;
    }

    if (!this->findVideoServiceConversion(codec->codec_id, codec->profile))
    {
        this->close();
        return false;
    }

    if (!this->createDecoder(codec->codec_id, codec->width, codec->height))
    {
        this->close();
        return false;
    }

    context.hw.decoder = context.decoder;
    context.hw.cfg = &context.cfg;
    context.hw.surface_count = context.surfaceCount;
    context.hw.surface = context.hwSurface;

    for (int i = 0; i < context.surfaceCount; i++)
        context.hw.surface[i] = context.surface[i].d3dSurface;

    codec->hwaccel_context = &context.hw;

    return true;
}

void DXVA2Decoder::close()
{
    this->closeInternal();
}

bool DXVA2Decoder::prepare(AVCodecContext *codec)
{
    (void)codec;

    return true;
}

bool DXVA2Decoder::getBuffer(AVFrame *ret)
{
    Context &context = this->m_context;

    if (FAILED(IDirect3DDeviceManager9_TestDevice(context.devManager, context.device)))
        return false;

    int i = 0;
    int old = 0;
    int noUsing = -1;

    for (; i < context.surfaceCount; i++)
    {
        Surface &surface = context.surface[i];

        if (!surface.ref && (noUsing == -1 || surface.order < context.surface[noUsing].order))
            noUsing = i;

        if (surface.order < context.surface[old].order)
            old = i;
    }

    i = (noUsing == -1) ? old : noUsing;

    Surface &surface = context.surface[i];

    surface.ref = this->m_initialRefCount;
    surface.order = context.surfaceOrder++;

    ret->data[0] = (uint8_t*)surface.d3dSurface;
    ret->data[3] = (uint8_t*)surface.d3dSurface;

    return true;
}

void DXVA2Decoder::releaseBuffer(uint8_t *data[AV_NUM_DATA_POINTERS])
{
    Surface *surface = this->findSurface((LPDIRECT3DSURFACE9)data[3]);

    if (surface)
        surface->ref--;
}

AVPixelFormat DXVA2Decoder::getFormat() const
{
    const Context &context = this->m_context;

    for (int i = 0; D3D_FORMATS[i].name; i++)
    {
        D3DFormat d3dFormat = D3D_FORMATS[i];

        if (context.render == d3dFormat.format)
            return d3dFormat.pixFormat;
    }

    return AV_PIX_FMT_NONE;
}

QString DXVA2Decoder::getName() const
{
    return "DXVA2";
}

bool DXVA2Decoder::decodePicture(const AVPacket &packet, AVFrame *ret)
{
    (void)packet;
    (void)ret;

    return false;
}

bool DXVA2Decoder::copyPicture(const AVFrame &src, AVFrame *ret)
{
    Context &context = this->m_context;
    LPDIRECT3DSURFACE9 srcD3DSurface = (LPDIRECT3DSURFACE9)src.data[3];
    LPDIRECT3DSURFACE9 destD3DSurface = context.surfaceQueue[context.surfaceQueueIndex];
    Surface *srcSurface = this->findSurface(srcD3DSurface);
    AVPixelFormat format = this->getFormat();

    context.surfaceQueue[context.surfaceQueueIndex] = srcD3DSurface;
    context.surfaceQueueIndex = (context.surfaceQueueIndex + 1) % context.surfaceQueueCount;

    srcSurface->ref++;

    if (!destD3DSurface)
    {
        if (context.surfaceQueueInit < DXVA2_MAX_SURFACE_QUEUE)
        {
            const int initialColor = 128;

            memset(ret->data[0], 0, ret->linesize[0] * context.height);
            memset(ret->data[1], initialColor, ret->linesize[1] * context.height / 2);

            if (format == AV_PIX_FMT_YUV420P)
                memset(ret->data[2], initialColor, ret->linesize[2] * context.height / 2);

            context.surfaceQueueInit++;
        }

        return true;
    }

    D3DLOCKED_RECT lock;

    if (FAILED(IDirect3DSurface9_LockRect(destD3DSurface, &lock, nullptr, D3DLOCK_READONLY)))
        return false;

    AVFrame pic;
    Surface *destSurface = this->findSurface(destD3DSurface);
    int planes = 0;
    bool result = true;

    destSurface->ref--;

    if (format == AV_PIX_FMT_YUV420P)
    {
        int pitch = lock.Pitch / 2;

        pic.linesize[0] = lock.Pitch;
        pic.linesize[1] = pitch;
        pic.linesize[2] = pitch;

        pic.data[0] = (uint8_t*)lock.pBits;
        pic.data[1] = pic.data[0] + pic.linesize[0] * context.surfaceHeight;
        pic.data[2] = pic.data[1] + pic.linesize[1] * context.surfaceHeight / 2;

        uint8_t *tmp;

        tmp = pic.data[1];
        pic.data[1] = pic.data[2];
        pic.data[2] = tmp;

        planes = 3;
    }
    else if (format == AV_PIX_FMT_NV12 || format == AV_PIX_FMT_P010)
    {
        pic.linesize[0] = lock.Pitch;
        pic.linesize[1] = lock.Pitch;

        pic.data[0] = (uint8_t*)lock.pBits;
        pic.data[1] = pic.data[0] + lock.Pitch * context.surfaceHeight;

        planes = 2;
    }
    else
    {
        result = false;
    }

    if (result)
    {
        if (!this->getMemCopy().copyYUV(pic, ret, context.height, planes))
            av_image_copy(ret->data, ret->linesize, (const uint8_t**)pic.data, pic.linesize, format, context.width, context.height);
    }

    IDirect3DSurface9_UnlockRect(destD3DSurface);

    return result;
}

bool DXVA2Decoder::isDecodable(AVPixelFormat format) const
{
    return format == AV_PIX_FMT_DXVA2_VLD;
}

void DXVA2Decoder::getDecoderDesc(QString *ret) const
{
    for (UINT i = 0; MODES[i].name != nullptr; i++)
    {
        const DXVA2Mode &mode = MODES[i];

        if (IsEqualGUID(this->m_context.input, *mode.guid))
        {
            *ret = mode.name;
            return;
        }
    }
}

void DXVA2Decoder::flushSurfaceQueue()
{
    Context &context = this->m_context;

    for (int i = 0; i < context.surfaceQueueCount; i++)
    {
        if (context.surfaceQueue[context.surfaceQueueIndex])
            context.surfaceQueue[context.surfaceQueueIndex] = nullptr;

        context.surfaceQueueIndex = (context.surfaceQueueIndex + 1) % context.surfaceQueueCount;
    }
}

int DXVA2Decoder::getSurfaceQueueCount() const
{
    return this->m_context.surfaceQueueCount;
}

bool DXVA2Decoder::isUseDefaultGetBuffer() const
{
    return false;
}

bool DXVA2Decoder::loadDLLs()
{
    Context &context = this->m_context;

    this->unloadDLLs();

    context.d3d9 = LoadLibraryW(L"D3D9.DLL");

    if (context.d3d9 == nullptr)
        return false;

    context.dxva2 = LoadLibraryW(L"DXVA2.DLL");

    if (context.dxva2 == nullptr)
        return false;

    return true;
}

void DXVA2Decoder::unloadDLLs()
{
    Context &context = this->m_context;

    if (context.dxva2)
    {
        FreeLibrary(context.dxva2);
        context.dxva2 = nullptr;
    }

    if (context.d3d9)
    {
        FreeLibrary(context.d3d9);
        context.d3d9 = nullptr;
    }
}

void DXVA2Decoder::closeInternal()
{
    Context &context = this->m_context;

    memset(&context.hw, 0, sizeof(context.hw));

    this->deleteDecoder();
    this->deleteVideoService();
    this->deleteDeviceManager();
    this->deleteDevice();

    this->unloadDLLs();
}

bool DXVA2Decoder::createDevice()
{
    Context &context = this->m_context;
    CreateD3D9 createD3D9 = this->getProcAddress<CreateD3D9>(context.d3d9, "Direct3DCreate9");

    if (createD3D9 == nullptr)
        return false;

    context.d3d = createD3D9(D3D_SDK_VERSION);

    if (context.d3d == nullptr)
        return false;

    D3DDISPLAYMODE d3ddm;

    if (FAILED(IDirect3D9_GetAdapterDisplayMode(context.d3d, D3DADAPTER_DEFAULT, &d3ddm)))
        return false;

    memset(&context.d3dParam, 0, sizeof(context.d3dParam));

    context.d3dParam.Flags                  = D3DPRESENTFLAG_VIDEO;
    context.d3dParam.Windowed               = TRUE;
    context.d3dParam.hDeviceWindow          = nullptr;
    context.d3dParam.SwapEffect             = D3DSWAPEFFECT_DISCARD;
    context.d3dParam.MultiSampleType        = D3DMULTISAMPLE_NONE;
    context.d3dParam.PresentationInterval   = D3DPRESENT_INTERVAL_DEFAULT;
    context.d3dParam.BackBufferCount        = 0;
    context.d3dParam.BackBufferFormat       = d3ddm.Format;
    context.d3dParam.BackBufferWidth        = 0;
    context.d3dParam.BackBufferHeight       = 0;
    context.d3dParam.EnableAutoDepthStencil = FALSE;

    if (FAILED(IDirect3D9_CreateDevice(context.d3d, D3DADAPTER_DEFAULT,
                                       D3DDEVTYPE_HAL, GetDesktopWindow(),
                                       D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED | D3DCREATE_FPU_PRESERVE,
                                       &context.d3dParam, &context.d3dDev)))
    {
        return false;
    }

    return true;
}

bool DXVA2Decoder::createDeviceEx()
{
    Context &context = this->m_context;
    IDirect3D9Ex *d3d9ex = nullptr;
    CreateD3D9Ex createD3D9Ex = this->getProcAddress<CreateD3D9Ex>(context.d3d9, "Direct3DCreate9Ex");

    if (createD3D9Ex == nullptr)
        return false;

    if (FAILED(createD3D9Ex(D3D_SDK_VERSION, &d3d9ex)))
        return false;

    context.d3d = (LPDIRECT3D9)d3d9ex;

    D3DDISPLAYMODEEX d3ddm;

    d3ddm.Size = sizeof(D3DDISPLAYMODEEX);

    if (FAILED(IDirect3D9Ex_GetAdapterDisplayModeEx(d3d9ex, D3DADAPTER_DEFAULT, &d3ddm, nullptr)))
        return false;

    memset(&context.d3dParam, 0, sizeof(context.d3dParam));

    context.d3dParam.Flags                  = D3DPRESENTFLAG_VIDEO;
    context.d3dParam.Windowed               = TRUE;
    context.d3dParam.hDeviceWindow          = nullptr;
    context.d3dParam.SwapEffect             = D3DSWAPEFFECT_DISCARD;
    context.d3dParam.MultiSampleType        = D3DMULTISAMPLE_NONE;
    context.d3dParam.PresentationInterval   = D3DPRESENT_INTERVAL_DEFAULT;
    context.d3dParam.BackBufferCount        = 0;
    context.d3dParam.BackBufferFormat       = d3ddm.Format;
    context.d3dParam.BackBufferWidth        = 0;
    context.d3dParam.BackBufferHeight       = 0;
    context.d3dParam.EnableAutoDepthStencil = FALSE;

    IDirect3DDevice9Ex *exdev = nullptr;

    if (FAILED(IDirect3D9Ex_CreateDeviceEx(d3d9ex, D3DADAPTER_DEFAULT,
                                           D3DDEVTYPE_HAL, GetDesktopWindow(),
                                           D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED | D3DCREATE_FPU_PRESERVE,
                                           &context.d3dParam, nullptr, &exdev)))
    {
        return false;
    }

    context.d3dDev = (LPDIRECT3DDEVICE9)exdev;

    return true;
}

void DXVA2Decoder::deleteDevice()
{
    Context &context = this->m_context;

    if (context.d3dDev)
    {
        IDirect3DDevice9_Release(context.d3dDev);
        context.d3dDev = nullptr;
    }

    if (context.d3d)
    {
        IDirect3D9_Release(context.d3d);
        context.d3d = nullptr;
    }
}

bool DXVA2Decoder::createDeviceManager()
{
    Context &context = this->m_context;
    CreateDeviceManager9 createDeviceManager9 = this->getProcAddress<CreateDeviceManager9>(context.dxva2, "DXVA2CreateDirect3DDeviceManager9");

    if (createDeviceManager9 == nullptr)
        return false;

    UINT token;

    if (FAILED(createDeviceManager9(&token, &context.devManager)))
        return false;

    if (FAILED(IDirect3DDeviceManager9_ResetDevice(context.devManager, context.d3dDev, token)))
        return false;

    return true;
}

void DXVA2Decoder::deleteDeviceManager()
{
    Context &context = this->m_context;

    if (context.devManager)
    {
        IDirect3DDeviceManager9_Release(context.devManager);
        context.devManager = nullptr;
    }
}

bool DXVA2Decoder::createVideoService()
{
    Context &context = this->m_context;
    CreateVideoService createVideoService = this->getProcAddress<CreateVideoService>(context.dxva2, "DXVA2CreateVideoService");

    if (createVideoService == nullptr)
        return false;

    if (FAILED(IDirect3DDeviceManager9_OpenDeviceHandle(context.devManager, &context.device)))
        return false;

    if (FAILED(IDirect3DDeviceManager9_GetVideoService(context.devManager, context.device,
                                                       IID_IDirectXVideoDecoderService, (void**)&context.videoService)))
    {
        return false;
    }

    return true;
}

void DXVA2Decoder::deleteVideoService()
{
    Context &context = this->m_context;

    if (context.videoService)
    {
        IDirectXVideoDecoderService_Release(context.videoService);
        context.videoService = nullptr;
    }

    if (context.device)
    {
        IDirect3DDeviceManager9_CloseDeviceHandle(context.devManager, context.device);
        context.device = nullptr;
    }
}

bool DXVA2Decoder::findVideoServiceConversion(AVCodecID codecID, int profile)
{
    UINT inputCount = 0;
    GUID *inputList = nullptr;
    Context &context = this->m_context;

    if (FAILED(IDirectXVideoDecoderService_GetDecoderDeviceGuids(context.videoService, &inputCount, &inputList)))
        return false;

    for (UINT i = 0; MODES[i].name != nullptr; i++)
    {
        const DXVA2Mode &mode = MODES[i];

        if (mode.codec == AV_CODEC_ID_NONE || mode.codec != codecID)
            continue;

        bool isSupported = false;

        for (UINT i = 0; i < inputCount; i++)
        {
            isSupported = IsEqualGUID(*mode.guid, inputList[i]);

            if (isSupported)
                break;
        }

        if (isSupported)
            isSupported = this->isProfileSupported(mode, profile);

        if (!isSupported)
            continue;

        UINT outputCount = 0;
        D3DFORMAT *outputList = nullptr;

        if (FAILED(IDirectXVideoDecoderService_GetDecoderRenderTargets(context.videoService, *mode.guid, &outputCount, &outputList)))
            continue;

        for (UINT j = 0; D3D_FORMATS[j].name; j++)
        {
            const D3DFormat &format = D3D_FORMATS[j];
            bool isSupported = false;

            for (UINT k = 0; k < outputCount; k++)
            {
                isSupported = format.format == outputList[k];

                if (isSupported)
                    break;
            }

            if (!isSupported)
                continue;

            context.input = *mode.guid;
            context.render = format.format;

            CoTaskMemFree(outputList);
            CoTaskMemFree(inputList);

            return true;
        }

        CoTaskMemFree(outputList);
    }

    CoTaskMemFree(inputList);

    return false;
}

bool DXVA2Decoder::createDecoder(AVCodecID codecID, int width, int height)
{
    Context &context = this->m_context;
    int alignment;

    if (codecID == AV_CODEC_ID_MPEG2VIDEO)
        alignment = 32;
    else if (codecID == AV_CODEC_ID_HEVC)
        alignment = 128;
    else
        alignment = 16;

    context.surfaceWidth = FFALIGN(width, alignment);
    context.surfaceHeight = FFALIGN(height, alignment);

    context.width = width;
    context.height = height;

    switch (codecID)
    {
        case AV_CODEC_ID_H264:
        case AV_CODEC_ID_HEVC:
        {
            context.surfaceCount = 16 + 1;
            this->m_initialRefCount = 2;

            break;
        }
        case AV_CODEC_ID_VP9:
        case AV_CODEC_ID_AV1:
        {
            context.surfaceCount = 16 + 1;
            this->m_initialRefCount = 2;

            break;
        }
        default:
        {
            context.surfaceCount = 16 + 1;
            this->m_initialRefCount = 2;

            break;
        }
    }

    context.surfaceCount += context.surfaceQueueCount;

    LPDIRECT3DSURFACE9 surfaceList[DXVA2_MAX_SURFACE_COUNT];

    if (FAILED(IDirectXVideoDecoderService_CreateSurface(context.videoService, context.surfaceWidth, context.surfaceHeight,
                                                         context.surfaceCount - 1, context.render, D3DPOOL_DEFAULT,
                                                         0, DXVA2_VideoDecoderRenderTarget, surfaceList, nullptr)))
    {
        return false;
    }

    for (int i = 0; i < context.surfaceCount; i++)
    {
        Surface &surface = context.surface[i];

        surface.d3dSurface = surfaceList[i];
        surface.ref = 0;
        surface.order = 0;
    }

    DXVA2_VideoDesc dsc;

    memset(&dsc, 0, sizeof(dsc));

    dsc.SampleWidth = context.surfaceWidth;
    dsc.SampleHeight = context.surfaceHeight;
    dsc.Format = context.render;
    dsc.UABProtectionLevel = FALSE;
    dsc.Reserved = 0;

    UINT cfgCount = 0;
    DXVA2_ConfigPictureDecode *cfgList = nullptr;

    if (FAILED(IDirectXVideoDecoderService_GetDecoderConfigurations(context.videoService, context.input,
                                                                    &dsc, nullptr, &cfgCount, &cfgList)))
    {
        return false;
    }

    int cfgScore = 0;

    for (UINT i = 0; i < cfgCount; i++)
    {
        const DXVA2_ConfigPictureDecode &cfg = cfgList[i];
        int score;

        if (cfg.ConfigBitstreamRaw == 1)
            score = 1;
        else if (codecID == AV_CODEC_ID_H264 && cfg.ConfigBitstreamRaw == 2)
            score = 2;
        else
            continue;

        if (IsEqualGUID(cfg.guidConfigBitstreamEncryption, DXVA_NoEncrypt))
            score += 16;

        if (cfgScore < score)
        {
            context.cfg = cfg;
            cfgScore = score;
        }
    }

    CoTaskMemFree(cfgList);

    if (cfgScore <= 0)
        return false;

    if (FAILED(IDirectXVideoDecoderService_CreateVideoDecoder(context.videoService, context.input, &dsc,
                                                              &context.cfg, surfaceList, context.surfaceCount,
                                                              &context.decoder)))
    {
        return false;
    }

    return true;
}

void DXVA2Decoder::deleteDecoder()
{
    Context &context = this->m_context;

    if (context.decoder)
    {
        IDirectXVideoDecoder_Release(context.decoder);
        context.decoder = nullptr;
    }

    for (int i = 0; i < context.surfaceCount; i++)
    {
        Surface &surface = context.surface[i];

        if (surface.d3dSurface)
        {
            IDirect3DSurface9_Release(surface.d3dSurface);
            surface.d3dSurface = nullptr;
        }

        surface.ref = 0;
        surface.order = 0;
    }

    context.surfaceQueueIndex = 0;
    context.surfaceQueueCount = DXVA2_MAX_SURFACE_QUEUE;
    context.surfaceQueueInit = 0;
    memset(context.surfaceQueue, 0, sizeof(context.surfaceQueue));

    context.surfaceOrder = 0;
    context.surfaceCount = 0;
    context.width = 0;
    context.height = 0;
}

DXVA2Decoder::Surface* DXVA2Decoder::findSurface(LPDIRECT3DSURFACE9 d3dSurface)
{
    Context &context = this->m_context;

    if (d3dSurface == nullptr)
        return nullptr;

    for (int i = 0; i < context.surfaceCount; i++)
    {
        Surface &surface = context.surface[i];

        if (surface.d3dSurface == d3dSurface)
            return &surface;
    }

    return nullptr;
}

bool DXVA2Decoder::isProfileSupported(const DXVA2Decoder::DXVA2Mode &mode, int profile) const
{
    bool isSupported = mode.profiles == nullptr || mode.profiles[0] < 0;

    if (!isSupported)
    {
        if (profile < 0)
            return true;

        for (const int *profiles = mode.profiles; *profiles >= 0; ++profiles)
        {
            if (*profiles == profile)
                return true;
        }

        return false;
    }

    return true;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-function-type"
template <typename T>
T DXVA2Decoder::getProcAddress(HMODULE hModule, LPCSTR lpProcName) const
{
    return (T)GetProcAddress(hModule, lpProcName);
}
#pragma GCC diagnostic pop
