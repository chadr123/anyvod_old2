﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "HWDecoderInterface.h"

#include <X11/Xlib.h>

extern "C"
{
# include <libavcodec/vdpau.h>
}

const int VDPAU_MAX_SURFACE_COUNT = 64;
const int VDPAU_MAX_SURFACE_QUEUE = 2;

class VDPAUDecoder : public HWDecoderInterface
{
public:
    VDPAUDecoder();
    virtual ~VDPAUDecoder();

    virtual bool open(AVCodecContext *codec);
    virtual void close();

    virtual bool prepare(AVCodecContext *codec);

    virtual bool getBuffer(AVFrame *ret);
    virtual void releaseBuffer(uint8_t *data[AV_NUM_DATA_POINTERS]);

    virtual AVPixelFormat getFormat() const;
    virtual QString getName() const;

    virtual bool decodePicture(const AVPacket &packet, AVFrame *ret);
    virtual bool copyPicture(const AVFrame &src, AVFrame *ret);

    virtual bool isDecodable(AVPixelFormat format) const;
    virtual void getDecoderDesc(QString *ret) const;

    virtual void flushSurfaceQueue();
    virtual int getSurfaceQueueCount() const;

    virtual bool isUseDefaultGetBuffer() const;

private:
    struct Surface
    {
        Surface()
        {
            surface = 0;
            ref = 0;
            order = 0;
        }

        VdpVideoSurface surface;
        int ref;
        unsigned int order;
    };

    struct Context
    {
        Context()
        {
            x11 = nullptr;
            device = 0;

            getProcAddress = nullptr;
            deviceDestroy = nullptr;
            getTransferCaps = nullptr;
            getData = nullptr;
            surfaceCreate = nullptr;
            surfaceDestroy = nullptr;
            getDecoderCaps = nullptr;

            surfaceWidth = 0;
            surfaceHeight = 0;
            surfaceCount = 0;
            surfaceOrder = 0;

            surfaceQueueIndex = 0;
            surfaceQueueCount = VDPAU_MAX_SURFACE_QUEUE;
            surfaceQueueInit = 0;
            memset(surfaceQueue, 0, sizeof(surfaceQueue));

            chromaType = std::numeric_limits<VdpChromaType>::max();
            vdpauformat = std::numeric_limits<VdpYCbCrFormat>::max();
            pixelFormat = AV_PIX_FMT_NONE;
            profile = std::numeric_limits<VdpDecoderProfile>::max();
        }

        Display *x11;
        VdpDevice device;

        VdpGetProcAddress *getProcAddress;
        VdpDeviceDestroy *deviceDestroy;
        VdpVideoSurfaceQueryGetPutBitsYCbCrCapabilities *getTransferCaps;
        VdpVideoSurfaceGetBitsYCbCr *getData;
        VdpVideoSurfaceCreate *surfaceCreate;
        VdpVideoSurfaceDestroy *surfaceDestroy;
        VdpDecoderQueryCapabilities *getDecoderCaps;

        int surfaceWidth;
        int surfaceHeight;
        int surfaceCount;
        unsigned int surfaceOrder;

        int surfaceQueueIndex;
        int surfaceQueueCount;
        int surfaceQueueInit;
        VdpVideoSurface surfaceQueue[VDPAU_MAX_SURFACE_QUEUE];

        Surface surface[VDPAU_MAX_SURFACE_COUNT];

        VdpChromaType chromaType;
        VdpYCbCrFormat vdpauformat;
        AVPixelFormat pixelFormat;
        VdpDecoderProfile profile;
    };

    struct VDPAUPixFormatMap
    {
        VdpYCbCrFormat vdpauFormat;
        AVPixelFormat pixelFormat;
    };

    struct VDPAUPixFormat
    {
        VdpChromaType chromaType;
        const VDPAUPixFormatMap *map;
    };

private:
    bool openDisplay(AVCodecID codecID, int ffProfile, AVPixelFormat pixFormat);
    void closeDisplay();
    void* getProcAddress(VdpFuncId id) const;
    bool initPixelFormats(VdpChromaType chromaType);
    VdpDecoderProfile getRextProfile() const;

    Surface* findSurface(VdpVideoSurface surface);

    bool createSurfaces(int width, int height);
    void deleteSurfaces();

    void closeInternal();

private:
    static const VDPAUPixFormatMap PIX_FORMATS_420[];
    static const VDPAUPixFormatMap PIX_FORMATS_422[];
    static const VDPAUPixFormatMap PIX_FORMATS_444[];
    static const VDPAUPixFormat PIX_FORMATS[];

private:
    Context m_context;
    int m_initialRefCount;
};
