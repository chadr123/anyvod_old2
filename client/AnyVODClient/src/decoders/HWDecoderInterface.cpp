﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "HWDecoderInterface.h"

HWDecoderInterface::HWDecoderInterface() :
    m_shouldStop(false)
{

}

HWDecoderInterface::~HWDecoderInterface()
{

}

bool HWDecoderInterface::isSuitable(AVCodecID codecID, int profile) const
{
    (void)codecID;
    (void)profile;

    return true;
}

void HWDecoderInterface::resetShouldStop()
{
    this->m_shouldStop = false;
}

bool HWDecoderInterface::getShouldStop() const
{
    return this->m_shouldStop;
}

#if !defined Q_OS_MOBILE
USWCMemCopy& HWDecoderInterface::getMemCopy()
{
    return this->m_memCopy;
}
#endif
