﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include <QGlobalStatic>

#include "HWDecoder.h"
#include "HWDecoderInterface.h"

#ifdef Q_OS_WIN
# include "DXVA2Decoder.h"
# include "NVDecoder.h"
#elif defined Q_OS_MAC
# include "VideoToolBoxDecoder.h"
#elif defined Q_OS_LINUX && !defined Q_OS_ANDROID && !defined Q_OS_RASPBERRY_PI
# include "VAAPIDecoder.h"
# include "VDPAUDecoder.h"
# include "NVDecoder.h"
#elif defined Q_OS_ANDROID
# include "../../AnyVODMobileClient/src/decoders/MediaCodec.h"
#endif

HWDecoder::HWDecoder() :
    m_isOpened(false),
    m_currentIndex(-1),
    m_selectedIndex(0)
{
#ifdef Q_OS_WIN
    this->m_decoders.append(new NVDecoder);
    this->m_decoders.append(new DXVA2Decoder);
#elif defined Q_OS_MAC
    this->m_decoders.append(new VideoToolBoxDecoder);
#elif defined Q_OS_LINUX && !defined Q_OS_ANDROID && !defined Q_OS_RASPBERRY_PI
    this->m_decoders.append(new NVDecoder);
    this->m_decoders.append(new VAAPIDecoder);
    this->m_decoders.append(new VDPAUDecoder);
#elif defined Q_OS_ANDROID
    this->m_decoders.append(new MediaCodec);
#endif
}

HWDecoder::~HWDecoder()
{
    for (const HWDecoderInterface *decoder : qAsConst(this->m_decoders))
        delete decoder;

    this->m_decoders.clear();
}

bool HWDecoder::isSuitable(AVCodecID codecID, AVPixelFormat format, int profile) const
{
    if (!this->getDecoder()->isSuitable(codecID, profile))
        return false;

    if (codecID == AV_CODEC_ID_H264 || codecID == AV_CODEC_ID_MPEG2VIDEO)
    {
        if (codecID == AV_CODEC_ID_H264)
        {
            if (profile == FF_PROFILE_UNKNOWN)
                return false;
        }

        if (format == AV_PIX_FMT_YUV420P || format == AV_PIX_FMT_YUVJ420P || this->getDecoder()->isDecodable(format))
            return true;
    }
    else
    {
        return true;
    }

    return false;
}

bool HWDecoder::isValid() const
{
    return this->m_currentIndex >= 0;
}

HWDecoderInterface* HWDecoder::getDecoder() const
{
    return this->m_decoders[this->m_currentIndex];
}

bool HWDecoder::open(AVCodecContext *codec)
{
    for (int i = this->m_selectedIndex; i< this->m_decoders.length(); i++)
    {
        this->m_currentIndex = i;

        if (this->isSuitable(codec->codec_id, codec->pix_fmt, codec->profile) && this->getDecoder()->open(codec))
        {
            this->getDecoder()->resetShouldStop();

            this->m_isOpened = true;
            this->m_selectedIndex = i;

            return true;
        }
    }

    this->m_isOpened = false;
    this->m_currentIndex = -1;

    return false;
}

void HWDecoder::close()
{
    if (this->isValid())
        this->getDecoder()->close();

    this->m_isOpened = false;
    this->m_currentIndex = -1;
}

bool HWDecoder::getBuffer(AVFrame *ret)
{
    if (this->isValid())
    {
        if (this->getDecoder()->isUseDefaultGetBuffer())
        {
            return this->getDecoder()->getBuffer(ret);
        }
        else
        {
            memset(ret->data, 0, sizeof(ret->data));
            memset(ret->linesize, 0, sizeof(ret->linesize));
            memset(ret->buf, 0, sizeof(ret->buf));

            ret->extended_data = ret->data;

            bool success = this->getDecoder()->getBuffer(ret);

            if (success)
            {
                Buffer *buf = new Buffer;

                buf->data = ret->data;
                buf->self = this;

                ret->buf[0] = av_buffer_create(nullptr, 0, HWDecoder::freeBuffer, buf, 0);
            }

            return success;
        }
    }

    return false;
}

void HWDecoder::releaseBuffer(uint8_t *data[AV_NUM_DATA_POINTERS])
{
    if (this->isValid() && this->isOpened())
        this->getDecoder()->releaseBuffer(data);
}

AVPixelFormat HWDecoder::getFormat() const
{
    if (this->isValid())
        return this->getDecoder()->getFormat();

    return AV_PIX_FMT_NONE;
}

bool HWDecoder::decodePicture(const AVPacket &packet, AVFrame *ret)
{
    if (this->isValid())
        return this->getDecoder()->decodePicture(packet, ret);

    return false;
}

bool HWDecoder::copyPicture(const AVFrame &src, AVFrame *ret)
{
    if (this->isValid())
        return this->getDecoder()->copyPicture(src, ret);

    return false;
}

bool HWDecoder::isDecodable(AVPixelFormat format) const
{
    if (this->isValid())
        return this->getDecoder()->isDecodable(format);

    return false;
}

void HWDecoder::getDecoderDesc(QString *ret) const
{
    if (this->isValid())
        this->getDecoder()->getDecoderDesc(ret);
}

void HWDecoder::flushSurfaceQueue()
{
    if (this->isValid())
        this->getDecoder()->flushSurfaceQueue();
}

int HWDecoder::getSurfaceQueueCount() const
{
    if (this->isValid())
        return this->getDecoder()->getSurfaceQueueCount();

    return 0;
}

bool HWDecoder::isUseDefaultGetBuffer() const
{
    if (this->isValid())
        return this->getDecoder()->isUseDefaultGetBuffer();

    return false;
}

bool HWDecoder::getShouldStop() const
{
    if (this->isValid())
        return this->getDecoder()->getShouldStop();

    return true;
}

int HWDecoder::getDecoderCount() const
{
    return this->m_decoders.count();
}

bool HWDecoder::setDecoderIndex(int index)
{
    if (index >= 0 && index < this->getDecoderCount())
    {
        this->m_selectedIndex = index;
        return true;
    }

    return false;
}

int HWDecoder::getDecoderIndex() const
{
    return this->m_selectedIndex;
}

QString HWDecoder::getDecoderName(int index) const
{
    if (index >= 0 && index < this->getDecoderCount())
        return this->m_decoders[index]->getName();

    return QString();
}

bool HWDecoder::isOpened() const
{
    return this->m_isOpened;
}

bool HWDecoder::prepare(AVCodecContext *codec)
{
    if (this->isValid())
        return this->getDecoder()->prepare(codec);

    return false;
}

void HWDecoder::freeBuffer(void *opaque, uint8_t *)
{
    Buffer *buf = (Buffer*)opaque;

    buf->self->releaseBuffer(buf->data);

    delete buf;
}
