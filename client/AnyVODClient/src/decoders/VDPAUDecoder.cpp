﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include <QDebug>

#include "VDPAUDecoder.h"

#include <vdpau/vdpau_x11.h>

extern "C"
{
# include <libavcodec/avcodec.h>
# include <libavutil/imgutils.h>
}

const VDPAUDecoder::VDPAUPixFormatMap VDPAUDecoder::PIX_FORMATS_420[] =
{
    { VDP_YCBCR_FORMAT_NV12, AV_PIX_FMT_NV12    },
    { VDP_YCBCR_FORMAT_YV12, AV_PIX_FMT_YUV420P },
#ifdef VDP_YCBCR_FORMAT_P016
    { VDP_YCBCR_FORMAT_P016, AV_PIX_FMT_P016    },
    { VDP_YCBCR_FORMAT_P010, AV_PIX_FMT_P010    },
#endif
    { 0,                     AV_PIX_FMT_NONE,   },
};

const VDPAUDecoder::VDPAUPixFormatMap VDPAUDecoder::PIX_FORMATS_422[] =
{
    { VDP_YCBCR_FORMAT_NV12, AV_PIX_FMT_NV16    },
    { VDP_YCBCR_FORMAT_YV12, AV_PIX_FMT_YUV422P },
    { VDP_YCBCR_FORMAT_UYVY, AV_PIX_FMT_UYVY422 },
    { VDP_YCBCR_FORMAT_YUYV, AV_PIX_FMT_YUYV422 },
    { 0,                     AV_PIX_FMT_NONE,   },
};

const VDPAUDecoder::VDPAUPixFormatMap VDPAUDecoder::PIX_FORMATS_444[] =
{
#ifdef VDP_YCBCR_FORMAT_Y_U_V_444
    { VDP_YCBCR_FORMAT_Y_U_V_444,       AV_PIX_FMT_YUV444P   },
#endif
#ifdef VDP_YCBCR_FORMAT_P016
    { VDP_YCBCR_FORMAT_Y_U_V_444_16,    AV_PIX_FMT_YUV444P16 },
#endif
    { 0,                                AV_PIX_FMT_NONE,     },
};

const VDPAUDecoder::VDPAUPixFormat VDPAUDecoder::PIX_FORMATS[] =
{
    { VDP_CHROMA_TYPE_420,    VDPAUDecoder::PIX_FORMATS_420 },
    { VDP_CHROMA_TYPE_422,    VDPAUDecoder::PIX_FORMATS_422 },
    { VDP_CHROMA_TYPE_444,    VDPAUDecoder::PIX_FORMATS_444 },
#ifdef VDP_YCBCR_FORMAT_P016
    { VDP_CHROMA_TYPE_420_16, VDPAUDecoder::PIX_FORMATS_420 },
    { VDP_CHROMA_TYPE_420_16, VDPAUDecoder::PIX_FORMATS_420 },
    { VDP_CHROMA_TYPE_422_16, VDPAUDecoder::PIX_FORMATS_422 },
    { VDP_CHROMA_TYPE_444_16, VDPAUDecoder::PIX_FORMATS_444 },
    { VDP_CHROMA_TYPE_444_16, VDPAUDecoder::PIX_FORMATS_444 },
#endif
};

VDPAUDecoder::VDPAUDecoder() :
    m_initialRefCount(0)
{

}

VDPAUDecoder::~VDPAUDecoder()
{
    this->closeInternal();
}

bool VDPAUDecoder::open(AVCodecContext *codec)
{
    Context &context = this->m_context;

    if (!this->openDisplay(codec->codec_id, codec->profile, codec->pix_fmt))
    {
        this->close();
        return false;
    }

    if (!this->createSurfaces(codec->width, codec->height))
    {
        this->close();
        return false;
    }

    if (av_vdpau_bind_context(codec, context.device, context.getProcAddress, 0))
    {
        this->close();
        return false;
    }

    codec->hwaccel_flags = AV_HWACCEL_FLAG_ALLOW_PROFILE_MISMATCH;

    return true;
}

void VDPAUDecoder::close()
{
    this->closeInternal();
}

bool VDPAUDecoder::prepare(AVCodecContext *codec)
{
    (void)codec;

    return true;
}

bool VDPAUDecoder::getBuffer(AVFrame *ret)
{
    Context &context = this->m_context;
    int i = 0;
    int old = 0;
    int noUsing = -1;

    for (; i < context.surfaceCount; i++)
    {
        Surface &surface = context.surface[i];

        if (!surface.ref && (noUsing == -1 || surface.order < context.surface[noUsing].order))
            noUsing = i;

        if (surface.order < context.surface[old].order)
            old = i;
    }

    i = (noUsing == -1) ? old : noUsing;

    Surface &surface = context.surface[i];

    surface.ref = this->m_initialRefCount;
    surface.order = context.surfaceOrder++;

    ret->data[3] = (uint8_t*)(uintptr_t)surface.surface;
    ret->format = AV_PIX_FMT_VDPAU;
    ret->width = context.surfaceWidth;
    ret->height = context.surfaceHeight;

    return true;
}

void VDPAUDecoder::releaseBuffer(uint8_t *data[AV_NUM_DATA_POINTERS])
{
    Surface *surface = this->findSurface((VdpVideoSurface)((uintptr_t)data[3]));

    if (surface)
        surface->ref--;
}

AVPixelFormat VDPAUDecoder::getFormat() const
{
    const Context &context = this->m_context;

    return context.pixelFormat;
}

QString VDPAUDecoder::getName() const
{
    return "VDPAU";
}

bool VDPAUDecoder::decodePicture(const AVPacket &packet, AVFrame *ret)
{
    (void)packet;
    (void)ret;

    return false;
}

bool VDPAUDecoder::copyPicture(const AVFrame &src, AVFrame *ret)
{
    Context &context = this->m_context;
    VdpVideoSurface srcID = (VdpVideoSurface)((uintptr_t)src.data[3]);
    VdpVideoSurface destID = context.surfaceQueue[context.surfaceQueueIndex];
    Surface *srcSurface = this->findSurface(srcID);
    AVPixelFormat format = this->getFormat();

    context.surfaceQueue[context.surfaceQueueIndex] = srcID;
    context.surfaceQueueIndex = (context.surfaceQueueIndex + 1) % context.surfaceQueueCount;

    srcSurface->ref++;

    if (!destID)
    {
        if (context.surfaceQueueInit < VDPAU_MAX_SURFACE_QUEUE)
        {
            const int initialColor = 128;

            memset(ret->data[0], 0, ret->linesize[0] * context.surfaceHeight);
            memset(ret->data[1], initialColor, ret->linesize[1] * context.surfaceHeight / 2);

            if (format == AV_PIX_FMT_YUV420P)
                memset(ret->data[2], initialColor, ret->linesize[2] * context.surfaceHeight / 2);

            context.surfaceQueueInit++;
        }

        return true;
    }

    void *data[3];
    uint32_t linesize[3];
    VdpStatus err;

    for (size_t i = 0; i < FF_ARRAY_ELEMS(data) && ret->data[i]; i++)
    {
        data[i] = ret->data[i];
        linesize[i] = ret->linesize[i];
    }

    if (context.vdpauformat == VDP_YCBCR_FORMAT_YV12
#ifdef VDP_YCBCR_FORMAT_Y_U_V_444
        || context.vdpauformat == VDP_YCBCR_FORMAT_Y_U_V_444
#endif
#ifdef VDP_YCBCR_FORMAT_P016
        || context.vdpauformat == VDP_YCBCR_FORMAT_Y_U_V_444_16
#endif
        )
    {
        FFSWAP(void*, data[1], data[2]);
    }

    err = context.getData(destID, context.vdpauformat, data, linesize);

    if (err != VDP_STATUS_OK)
        return false;

    Surface *destSurface = this->findSurface(destID);

    destSurface->ref--;

    return true;
}

bool VDPAUDecoder::isDecodable(AVPixelFormat format) const
{
    return format == AV_PIX_FMT_VDPAU;
}

void VDPAUDecoder::getDecoderDesc(QString *ret) const
{
    switch (this->m_context.profile)
    {
        case VDP_DECODER_PROFILE_MPEG1:
            *ret = "MPEG1";
            break;
        case VDP_DECODER_PROFILE_MPEG2_SIMPLE:
            *ret = "MPEG2 Simple";
            break;
        case VDP_DECODER_PROFILE_MPEG2_MAIN:
            *ret = "MPEG2 Main";
            break;
        case VDP_DECODER_PROFILE_MPEG4_PART2_SP:
            *ret = "MPEG4 Simple";
            break;
        case VDP_DECODER_PROFILE_MPEG4_PART2_ASP:
            *ret = "MPEG4 Advanced Simple";
            break;
        case VDP_DECODER_PROFILE_VC1_SIMPLE:
            *ret = "VC1 Simple";
            break;
        case VDP_DECODER_PROFILE_VC1_MAIN:
            *ret = "VC1 Main";
            break;
        case VDP_DECODER_PROFILE_VC1_ADVANCED:
            *ret = "VC1 Advanced";
            break;
        case VDP_DECODER_PROFILE_H264_BASELINE:
            *ret = "H264 Baseline";
            break;
        case VDP_DECODER_PROFILE_H264_CONSTRAINED_BASELINE:
            *ret = "H264 Constrained Baseline";
            break;
        case VDP_DECODER_PROFILE_H264_MAIN:
            *ret = "H264 Main";
            break;
        case VDP_DECODER_PROFILE_H264_HIGH:
            *ret = "H264 High";
            break;
        case VDP_DECODER_PROFILE_HEVC_MAIN:
            *ret = "HEVC Main";
            break;
        case VDP_DECODER_PROFILE_HEVC_MAIN_10:
            *ret = "HEVC Main10";
            break;
        case VDP_DECODER_PROFILE_HEVC_MAIN_444:
            *ret = "HEVC Main 444";
            break;
#ifdef VDP_DECODER_PROFILE_HEVC_MAIN_444_10
        case VDP_DECODER_PROFILE_HEVC_MAIN_444_10:
            *ret = "HEVC Main10 444";
            break;
        case VDP_DECODER_PROFILE_HEVC_MAIN_444_12:
            *ret = "HEVC Main12 444";
            break;
#endif
        default:
            break;
    }
}

void VDPAUDecoder::flushSurfaceQueue()
{
    Context &context = this->m_context;

    for (int i = 0; i < context.surfaceQueueCount; i++)
    {
        if (context.surfaceQueue[context.surfaceQueueIndex])
            context.surfaceQueue[context.surfaceQueueIndex] = 0;

        context.surfaceQueueIndex = (context.surfaceQueueIndex + 1) % context.surfaceQueueCount;
    }
}

int VDPAUDecoder::getSurfaceQueueCount() const
{
    return this->m_context.surfaceQueueCount;
}

bool VDPAUDecoder::isUseDefaultGetBuffer() const
{
    return false;
}

bool VDPAUDecoder::openDisplay(AVCodecID codecID, int ffProfile, AVPixelFormat pixFormat)
{
    VdpStatus err;
    int surfaceCount;
    VdpDecoderProfile profile;
    Context &context = this->m_context;

    switch (codecID)
    {
        case AV_CODEC_ID_MPEG1VIDEO:
        {
            profile = VDP_DECODER_PROFILE_MPEG1;
            surfaceCount = 16 + 1;
            this->m_initialRefCount = 2;

            break;
        }
        case AV_CODEC_ID_MPEG2VIDEO:
        {
            profile = VDP_DECODER_PROFILE_MPEG2_MAIN;
            surfaceCount = 16 + 1;
            this->m_initialRefCount = 2;

            break;
        }
        case AV_CODEC_ID_MPEG4:
        {
            profile = VDP_DECODER_PROFILE_MPEG4_PART2_ASP;
            surfaceCount = 16 + 1;
            this->m_initialRefCount = 2;

            break;
        }
        case AV_CODEC_ID_WMV3:
        {
            profile = VDP_DECODER_PROFILE_VC1_MAIN;
            surfaceCount = 16 + 1;
            this->m_initialRefCount = 2;

            break;
        }
        case AV_CODEC_ID_VC1:
        {
            profile = VDP_DECODER_PROFILE_VC1_ADVANCED;
            surfaceCount = 16 + 1;
            this->m_initialRefCount = 2;

            break;
        }
        case AV_CODEC_ID_H264:
        {
            if (ffProfile == FF_PROFILE_H264_BASELINE)
                profile = VDP_DECODER_PROFILE_H264_BASELINE;
            else if (ffProfile == FF_PROFILE_H264_CONSTRAINED_BASELINE)
                profile = VDP_DECODER_PROFILE_H264_CONSTRAINED_BASELINE;
            else if (ffProfile == FF_PROFILE_H264_MAIN)
                profile = VDP_DECODER_PROFILE_H264_MAIN;
            else
                profile = VDP_DECODER_PROFILE_H264_HIGH;

            surfaceCount = 16 + 1;
            this->m_initialRefCount = 2;

            break;
        }
        case AV_CODEC_ID_HEVC:
        {
            if (ffProfile == FF_PROFILE_HEVC_MAIN_10)
                profile = VDP_DECODER_PROFILE_HEVC_MAIN_10;
#ifdef VDP_DECODER_PROFILE_HEVC_MAIN_444_10
            else if (ffProfile == FF_PROFILE_HEVC_REXT)
                profile = this->getRextProfile();
#endif
            else
                profile = VDP_DECODER_PROFILE_HEVC_MAIN;

            surfaceCount = 16 + 1;
            this->m_initialRefCount = 2;

            break;
        }
        case AV_CODEC_ID_VP9:
        {
            if (ffProfile == FF_PROFILE_VP9_0)
                profile = VDP_DECODER_PROFILE_VP9_PROFILE_0;
            else if (ffProfile == FF_PROFILE_VP9_1)
                profile = VDP_DECODER_PROFILE_VP9_PROFILE_1;
            else if (ffProfile == FF_PROFILE_VP9_2)
                profile = VDP_DECODER_PROFILE_VP9_PROFILE_2;
            else if (ffProfile == FF_PROFILE_VP9_3)
                profile = VDP_DECODER_PROFILE_VP9_PROFILE_3;
            else
                profile = VDP_DECODER_PROFILE_VP9_PROFILE_0;

            surfaceCount = 16 + 1;
            this->m_initialRefCount = 2;

            break;
        }
        default:
        {
            return false;
        }
    }

    surfaceCount += context.surfaceQueueCount;

    context.x11 = XOpenDisplay(nullptr);

    if (!context.x11)
        return false;

    err = vdp_device_create_x11(context.x11, XDefaultScreen(context.x11), &context.device, &context.getProcAddress);

    if (err != VDP_STATUS_OK)
        return false;

    context.deviceDestroy = (VdpDeviceDestroy*)this->getProcAddress(VDP_FUNC_ID_DEVICE_DESTROY);
    context.getTransferCaps = (VdpVideoSurfaceQueryGetPutBitsYCbCrCapabilities*)this->getProcAddress(VDP_FUNC_ID_VIDEO_SURFACE_QUERY_GET_PUT_BITS_Y_CB_CR_CAPABILITIES);
    context.getData = (VdpVideoSurfaceGetBitsYCbCr*)this->getProcAddress(VDP_FUNC_ID_VIDEO_SURFACE_GET_BITS_Y_CB_CR);
    context.surfaceCreate = (VdpVideoSurfaceCreate*)this->getProcAddress(VDP_FUNC_ID_VIDEO_SURFACE_CREATE);
    context.surfaceDestroy = (VdpVideoSurfaceDestroy*)this->getProcAddress(VDP_FUNC_ID_VIDEO_SURFACE_DESTROY);
    context.getDecoderCaps = (VdpDecoderQueryCapabilities*)this->getProcAddress(VDP_FUNC_ID_DECODER_QUERY_CAPABILITIES);

    VdpBool isSupported;
    uint32_t maxLevel;
    uint32_t maxMacroblocks;
    uint32_t maxWidth;
    uint32_t maxHeight;

    err = context.getDecoderCaps(context.device, profile, &isSupported, &maxLevel, &maxMacroblocks, &maxWidth, &maxHeight);

    if ((err != VDP_STATUS_OK || isSupported != VDP_TRUE) && profile == VDP_DECODER_PROFILE_H264_CONSTRAINED_BASELINE)
    {
        profile = VDP_DECODER_PROFILE_H264_MAIN;
        err = context.getDecoderCaps(context.device, profile, &isSupported, &maxLevel, &maxMacroblocks, &maxWidth, &maxHeight);
    }

    if (err != VDP_STATUS_OK || isSupported != VDP_TRUE)
        return false;

    VdpChromaType chromaType;

    switch (pixFormat)
    {
        case AV_PIX_FMT_YUV420P:
            chromaType = VDP_CHROMA_TYPE_420;
            break;
        case AV_PIX_FMT_YUV422P:
            chromaType = VDP_CHROMA_TYPE_422;
            break;
        case AV_PIX_FMT_YUV444P:
            chromaType = VDP_CHROMA_TYPE_444;
            break;
        default:
            return false;
    }

    if (!this->initPixelFormats(chromaType))
        return false;

    context.surfaceCount = surfaceCount;
    context.profile = profile;

    return true;
}

void VDPAUDecoder::closeDisplay()
{
    Context &context = this->m_context;

    if (context.deviceDestroy)
        context.deviceDestroy(context.device);

    if (context.x11)
    {
        XCloseDisplay(context.x11);
        context.x11 = nullptr;
    }

    context.surfaceCount = 0;

    context.chromaType = std::numeric_limits<VdpChromaType>::max();
    context.vdpauformat = std::numeric_limits<VdpYCbCrFormat>::max();
    context.pixelFormat = AV_PIX_FMT_NONE;
    context.profile = std::numeric_limits<VdpDecoderProfile>::max();
}

void* VDPAUDecoder::getProcAddress(VdpFuncId id) const
{
    VdpStatus err;
    const Context &context = this->m_context;
    void *address = nullptr;

    err = context.getProcAddress(context.device, id, &address);

    if (err != VDP_STATUS_OK)
        return nullptr;

    return address;
}

bool VDPAUDecoder::initPixelFormats(VdpChromaType chromaType)
{
    Context &context = this->m_context;

    for (size_t i = 0; i < FF_ARRAY_ELEMS(PIX_FORMATS); i++)
    {
        const VDPAUPixFormat &format = PIX_FORMATS[i];

        if (format.chromaType != chromaType)
            continue;

        const VDPAUPixFormatMap *map = format.map;

        while (map->pixelFormat != AV_PIX_FMT_NONE)
        {
            VdpBool supported;
            VdpStatus err = context.getTransferCaps(context.device, format.chromaType, map->vdpauFormat, &supported);

            if (err == VDP_STATUS_OK && supported == VDP_TRUE)
            {
                context.chromaType = format.chromaType;
                context.vdpauformat = map->vdpauFormat;
                context.pixelFormat = map->pixelFormat;

                return true;
            }

            map++;
        }
    }

    return false;
}

VdpDecoderProfile VDPAUDecoder::getRextProfile() const
{
// TODO : Support rext profiles
    return VDP_DECODER_PROFILE_HEVC_MAIN_444;
#ifdef VDP_DECODER_PROFILE_HEVC_MAIN_444_10
    return VDP_DECODER_PROFILE_HEVC_MAIN_444_10;
    return VDP_DECODER_PROFILE_HEVC_MAIN_444_12;
#endif
}

VDPAUDecoder::Surface* VDPAUDecoder::findSurface(VdpVideoSurface surface)
{
    Context &context = this->m_context;

    if (surface == 0)
        return nullptr;

    for (int i = 0; i < context.surfaceCount; i++)
    {
        Surface &s = context.surface[i];

        if (s.surface == surface)
            return &s;
    }

    return nullptr;
}

bool VDPAUDecoder::createSurfaces(int width, int height)
{
    Context &context = this->m_context;
    VdpStatus err;

    for (int i = 0; i < context.surfaceCount; i++)
    {
        Surface &surface = context.surface[i];

        err = context.surfaceCreate(context.device, context.chromaType, width, height, &surface.surface);

        if (err != VDP_STATUS_OK)
            return false;

        surface.ref = 0;
        surface.order = 0;
    }

    context.surfaceWidth = width;
    context.surfaceHeight = height;

    return true;
}

void VDPAUDecoder::deleteSurfaces()
{
    Context &context = this->m_context;

    for (int i = 0; i < context.surfaceCount; i++)
    {
        Surface &surface = context.surface[i];

        if (surface.surface != 0)
        {
            context.surfaceDestroy(surface.surface);
            surface.surface = 0;
        }
    }

    context.surfaceQueueIndex = 0;
    context.surfaceQueueCount = VDPAU_MAX_SURFACE_QUEUE;
    context.surfaceQueueInit = 0;
    memset(context.surfaceQueue, 0, sizeof(context.surfaceQueue));

    context.surfaceOrder = 0;
    context.surfaceWidth = 0;
    context.surfaceHeight = 0;
}

void VDPAUDecoder::closeInternal()
{
    this->deleteSurfaces();
    this->closeDisplay();
}
