﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

extern "C"
{
# include <libavcodec/avcodec.h>
# include <libavutil/pixfmt.h>
}

#include <QVector>

class HWDecoderInterface;
class QString;

class HWDecoder
{
public:
    HWDecoder();
    ~HWDecoder();

    bool open(AVCodecContext *codec);
    void close();
    bool isOpened() const;

    bool prepare(AVCodecContext *codec);

    bool getBuffer(AVFrame *buf);
    AVPixelFormat getFormat() const;

    bool decodePicture(const AVPacket &packet, AVFrame *ret);
    bool copyPicture(const AVFrame &src, AVFrame *ret);

    bool isDecodable(AVPixelFormat format) const;
    void getDecoderDesc(QString *ret) const;

    void flushSurfaceQueue();
    int getSurfaceQueueCount() const;

    bool isUseDefaultGetBuffer() const;
    bool getShouldStop() const;

    int getDecoderCount() const;
    bool setDecoderIndex(int index);
    int getDecoderIndex() const;
    QString getDecoderName(int index) const;

private:
    void releaseBuffer(uint8_t *data[AV_NUM_DATA_POINTERS]);
    bool isSuitable(AVCodecID codecID, AVPixelFormat format, int profile) const;
    bool isValid() const;
    HWDecoderInterface* getDecoder() const;

private:
    static void freeBuffer(void *opaque, uint8_t *);

private:
    struct Buffer
    {
        HWDecoder *self;
        uint8_t **data;
    };

private:
    QVector<HWDecoderInterface*> m_decoders;
    bool m_isOpened;
    int m_currentIndex;
    int m_selectedIndex;
};
