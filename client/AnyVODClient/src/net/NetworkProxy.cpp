﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "NetworkProxy.h"
#include "core/Common.h"

#include <QNetworkProxyFactory>

NetworkProxy::NetworkProxy()
{

}

void NetworkProxy::setupProxy()
{
    QNetworkProxyFactory::setUseSystemConfiguration(true);

    QNetworkProxyQuery proxyQuery(QUrl("http://www.google.com"));
    QList<QNetworkProxy> proxyList = QNetworkProxyFactory::systemProxyForQuery(proxyQuery);

    for (const QNetworkProxy &proxy : qAsConst(proxyList))
    {
        if (proxy.type() == QNetworkProxy::HttpProxy)
        {
            NetworkProxy::setupProxyEnvironmentVariable(proxy);
            break;
        }
    }
}

void NetworkProxy::setupProxyManual(const QString &proxyInfo)
{
    QString proxy = proxyInfo.trimmed();

    if (proxy.isEmpty())
        return;

    if (!proxy.contains(PROTOCOL_POSTFIX))
        proxy.prepend("http://");

    QUrl url(proxy);

    if (!url.isValid())
        return;

    QNetworkProxy p(QNetworkProxy::HttpProxy, url.host(), url.port(), url.userName(), url.password());

    QNetworkProxy::setApplicationProxy(p);
    NetworkProxy::setupProxyEnvironmentVariable(p);
}

void NetworkProxy::setupProxyEnvironmentVariable(const QNetworkProxy &proxy)
{
    QString auth;
    QString user = proxy.user();
    QString password = proxy.password();
    QString host = proxy.hostName();
    quint16 port = proxy.port();

    if (!user.isEmpty())
    {
        auth = user;

        if (!password.isEmpty())
            auth += ":" + password;

        auth += "@";
    }

    if (port)
        host += ":" + QString::number(port);

    QString httpProxy("http://" + auth + host);

    qputenv("http_proxy", httpProxy.toUtf8());
}
