﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "../../../../common/packets.h"

#include <QString>
#include <QMutex>
#include <QCoreApplication>

class QByteArray;

class Socket
{
    Q_DECLARE_TR_FUNCTIONS(Socket)
private:
    Socket();
    ~Socket();

public:
    static Socket& getInstance();
    static Socket& getStreamInstance();

    bool connect(const QString &address, uint16_t port);
    void disconnect();
    bool isConnected() const;

    bool login(const QString &id, const QString &password, QString *error);
    bool logout(QString *error);
    bool join(const QString &ticket, QString *error);

    bool requestFilelist(const QString &path, QString *error, QList<ANYVOD_FILE_ITEM> *ret);

    bool startMovie(const QString &path, QString *error, ANYVOD_MOVIE_INFO *ret);
    bool stopMovie(QString *error);

    bool streamRequest(uint64_t start, uint32_t size, QString *error);

    bool subtitle(const QString &path, bool searchSubtitle, bool searchLyrics, QString *error, QString *subtitleFileName, QByteArray *ret);
    bool existSubtitleURL(const QString &path, bool searchSubtitle, QString *error, QString *url);

    bool recvRequest(ANYVOD_PACKET **ret, QString *error);

    bool isLogined() const;
    void getTicket(QString *ret) const;

    void setReadBufferSize(int size) const;
    void restoreReadBufferSize() const;

private:
    void fillTicket(ANYVOD_PACKET *packet) const;
    ERROR_TYPE checkError(ANYVOD_PACKET &packet, QString *error);

    bool getResponse(ANYVOD_PACKET *ret);
    bool waitResponse(ANYVOD_PACKET &packet, QString *error, ANYVOD_PACKET **recvPacket = nullptr);

    bool send(ANYVOD_PACKET &packet);

    bool recv(ANYVOD_PACKET **packet);
    bool recvSub(char *data, qint64 size);

private:
    QString m_ticket;
    QMutex m_lock;
    uint8_t *m_buffer;
    int m_bufferSize;
    int m_socket;

private:
    static const int DEFAULT_READBUFFER_SIZE;
};
