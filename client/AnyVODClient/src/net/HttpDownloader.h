﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QThread>
#include <QNetworkReply>
#include <QMutex>

class HttpDownloader : public QThread
{
    Q_OBJECT
public:
    HttpDownloader();
    ~HttpDownloader();

    bool download(const QString &path, QIODevice &to);
    bool download(const QString &path, QIODevice &to, const QVector<QPair<QByteArray, QByteArray>> &headers);
    bool download(const QString &path, QIODevice &to, const QByteArray &payload);
    bool download(const QString &path, QIODevice &to, const QVector<QPair<QByteArray, QByteArray>> &headers, const QByteArray &payload);

    QNetworkReply::NetworkError error() const;
    QString getRedirectURL() const;

private slots:
    void received();

protected:
    virtual void run();

private:
    void request(QIODevice *to);
    bool isRedirect() const;
    bool downloadInternal(const QString &path, QIODevice &to, const QVector<QPair<QByteArray, QByteArray>> &headers, const QByteArray &payload);

private:
    QNetworkRequest m_header;
    QByteArray m_payload;
    QIODevice *m_to;
    QNetworkReply::NetworkError m_error;
    QString m_redirectURL;
    QString m_prevRedirectURL;
    QMutex m_lock;
};
