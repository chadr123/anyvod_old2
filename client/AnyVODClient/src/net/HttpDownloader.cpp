﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "HttpDownloader.h"

#include <QMutexLocker>
#include <QNetworkAccessManager>
#include <QDebug>

HttpDownloader::HttpDownloader() :
    m_to(nullptr),
    m_error(QNetworkReply::NoError)
{

}

HttpDownloader::~HttpDownloader()
{

}

QNetworkReply::NetworkError HttpDownloader::error() const
{
    return this->m_error;
}

QString HttpDownloader::getRedirectURL() const
{
    return this->m_prevRedirectURL;
}

bool HttpDownloader::isRedirect() const
{
    return !this->m_redirectURL.isEmpty();
}

bool HttpDownloader::downloadInternal(const QString &path, QIODevice &to,
                                      const QVector<QPair<QByteArray, QByteArray>> &headers, const QByteArray &payload)
{
    QSslConfiguration sslConfig;

    this->m_header = QNetworkRequest(path);

    sslConfig = this->m_header.sslConfiguration();
    sslConfig.setPeerVerifyMode(QSslSocket::VerifyNone);

    this->m_header.setSslConfiguration(sslConfig);
    this->m_header.setHeader(QNetworkRequest::UserAgentHeader, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");

    if (!payload.isEmpty())
        this->m_header.setHeader(QNetworkRequest::ContentTypeHeader, "text/plain;charset=UTF-8");

    for (const QPair<QByteArray, QByteArray> &header : headers)
        this->m_header.setRawHeader(header.first, header.second);

    this->m_payload = payload;

    this->request(&to);

    if (this->isRedirect())
        return this->downloadInternal(this->m_redirectURL, to, headers, payload);

    return this->error() == QNetworkReply::NoError;
}

void HttpDownloader::request(QIODevice *to)
{
    this->m_to = to;

    this->start();
    this->wait();

    this->m_to = nullptr;
}

bool HttpDownloader::download(const QString &path, QIODevice &to)
{
    return this->download(path, to, QVector<QPair<QByteArray, QByteArray>>(), QByteArray());
}

bool HttpDownloader::download(const QString &path, QIODevice &to, const QVector<QPair<QByteArray, QByteArray> > &headers)
{
    return this->download(path, to, headers, QByteArray());
}

bool HttpDownloader::download(const QString &path, QIODevice &to, const QByteArray &payload)
{
    return this->download(path, to, QVector<QPair<QByteArray, QByteArray>>(), payload);
}

bool HttpDownloader::download(const QString &path, QIODevice &to,
                              const QVector<QPair<QByteArray, QByteArray>> &headers, const QByteArray &payload)
{
    QMutexLocker locker(&this->m_lock);

    return this->downloadInternal(path, to, headers, payload);
}

void HttpDownloader::received()
{
    this->exit();
}

void HttpDownloader::run()
{
    QNetworkAccessManager http;
    QNetworkReply *reply;

    if (this->m_payload.isEmpty())
        reply = http.get(this->m_header);
    else
        reply = http.post(this->m_header, this->m_payload);

    connect(reply, &QNetworkReply::finished, this, &HttpDownloader::received, Qt::DirectConnection);

    this->exec();

    bool opened = this->m_to->isOpen();

    if (!opened)
        this->m_to->open(QIODevice::WriteOnly);

    this->m_to->write(reply->readAll());

    if (!opened)
        this->m_to->close();

    this->m_redirectURL = reply->header(QNetworkRequest::LocationHeader).toString();

    if (this->m_redirectURL.isEmpty())
        this->m_redirectURL = reply->rawHeader("Location");

    if (!this->m_redirectURL.isEmpty())
    {
        QUrl redirectURL(this->m_redirectURL);

        if (redirectURL.host().isEmpty())
        {
            QUrl headerURL = this->m_header.url();

            redirectURL.setScheme(headerURL.scheme());
            redirectURL.setHost(headerURL.host());

            this->m_redirectURL = redirectURL.toString();
        }
    }

    if (!this->m_redirectURL.isEmpty())
        this->m_prevRedirectURL = this->m_redirectURL;

    this->m_error = reply->error();

    delete reply;
}
