﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "core/AnyVODEnums.h"

#include <QThread>

class QSettings;

class Updater : public QThread
{
    Q_OBJECT
private:
    Updater();

public:
    static Updater& getInstance();

public:
    void checkUpdate();
    QString getUpdateURL();
    void updateCurrentDate() const;

    AnyVODEnums::CheckUpdateGap getUpdateGap() const;
    void setUpdateGap(AnyVODEnums::CheckUpdateGap gap);

    void fixUpdateGap();

signals:
    void findResult(const QString &fileName);

private slots:
    void updateResult(const QString &fileName);

private:
    void findUpdate();
    bool isUpdatable(const QString &fileName) const;

    QString getFileName() const;
    QString getUpdateURL(const QString &fileName) const;

    int getCurrnetVersion() const;
    int getVersionFromFileName(const QString &fileName) const;
    QStringList getArchs() const;

protected:
    virtual void run();

private:
    static const QString PREFIX;
    static const QString DOWNLOAD_PAGE_URL;

private:
    QSettings &m_settings;
};
