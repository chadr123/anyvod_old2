﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QChar>

class SeparatingUtils
{
private:
    SeparatingUtils();

public:
    static void appendDirSeparator(QString *path);
    static QString adjustNetworkPath(const QString &path);
    static QString removeFFMpegSeparator(const QString &org);

private:
    static const QString NETWORK_PATH_PREFIX;

public:
    static const QChar WINDOWS_DIR_SEPARATOR;
    static const QChar DIR_SEPARATOR;
    static const QChar FFMPEG_SEPARATOR;
};
