﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QString>

class ConvertingUtils
{
private:
    ConvertingUtils();

public:
    static QString sizeToString(unsigned long long size);
    static QString valueToString(unsigned long long value);
    static QString* getTimeString(double seconds, const QString &format, QString *ret);

public:
    static const QString TIME_MM_SS;
    static const QString TIME_MM_SS_Z;
    static const QString TIME_HH_MM_SS;
    static const QString TIME_HH_MM_SS_ZZZ;
    static const QString TIME_HH_MM_SS_ZZZ_COMMA;
    static const QString TIME_HH_MM_SS_ZZZ_DIR;
};
