﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ConvertingUtils.h"
#include "core/Common.h"

#include <QDateTime>

#include <cmath>

const QString ConvertingUtils::TIME_MM_SS = "mm:ss";
const QString ConvertingUtils::TIME_MM_SS_Z = "mm:ss.z";
const QString ConvertingUtils::TIME_HH_MM_SS = "hh:mm:ss";
const QString ConvertingUtils::TIME_HH_MM_SS_ZZZ = "hh:mm:ss.zzz";
const QString ConvertingUtils::TIME_HH_MM_SS_ZZZ_COMMA = "hh:mm:ss,zzz";
const QString ConvertingUtils::TIME_HH_MM_SS_ZZZ_DIR = "hh_mm_ss_zzz";

ConvertingUtils::ConvertingUtils()
{

}

QString ConvertingUtils::sizeToString(unsigned long long size)
{
    QString ret;

    if (size == 0)
        return "0";

    if (size < MEGA)
        ret = QString("%1K").arg((int)floor(size / (double)KILO + 0.5));
    else if (size < GIGA)
        ret = QString("%1M").arg(size / (double)MEGA, 0, 'f', 1);
    else
        ret = QString("%1G").arg(size / (double)GIGA, 0, 'f', 1);

    return ret;
}

QString ConvertingUtils::valueToString(unsigned long long value)
{
    QString ret;

    if (value == 0)
        return "0";

    if (value < MEGA_10E3)
        ret = QString("%1K").arg((int)floor(value / (double)KILO_10E3 + 0.5));
    else if (value < GIGA_10E3)
        ret = QString("%1M").arg(value / (double)MEGA_10E3, 0, 'f', 1);
    else
        ret = QString("%1G").arg(value / (double)GIGA_10E3, 0, 'f', 1);

    return ret;
}

QString* ConvertingUtils::getTimeString(double seconds, const QString &format, QString *ret)
{
    if (seconds < 0.0)
        seconds = 0.0;

    QDateTime time = QDateTime::fromTime_t((uint)seconds);

    time = time.addMSecs((seconds - (uint)seconds) * 1000);

    time = time.toTimeSpec(Qt::UTC);
    *ret = time.toString(format);

    return ret;
}
