﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ChapterInfoUtils.h"

#include <QVector>

ChapterInfoUtils::ChapterInfoUtils()
{

}

int ChapterInfoUtils::findChapter(double pos, const QVector<ChapterInfo> &list)
{
    int index = -1;

    for (int i = 0; i < list.count(); i++)
    {
        const ChapterInfo &info = list[i];

        if (info.start <= pos && pos <= info.end)
        {
            index = i;
            break;
        }
    }

    return index;
}
