﻿#include "TimeUtils.h"

const QTime TimeUtils::ZERO_TIME = QTime(0, 0);

TimeUtils::TimeUtils()
{

}

QDateTime TimeUtils::getTime(double seconds)
{
    QDateTime time = QDateTime::fromTime_t((uint)seconds);

    time = time.addMSecs((seconds - (uint)seconds) * 1000);

    return time.toTimeSpec(Qt::UTC);
}
