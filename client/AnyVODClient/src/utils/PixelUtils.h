﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

extern "C"
{
# include <libavformat/avformat.h>
}

#ifndef GET_RED_VALUE
# define GET_RED_VALUE(rgb) ((uint8_t)(((rgb)>>24)&0xff))
#endif

#ifndef GET_GREEN_VALUE
# define GET_GREEN_VALUE(rgb) ((uint8_t)(((rgb)>>16)&0xff))
#endif

#ifndef GET_BLUE_VALUE
# define GET_BLUE_VALUE(rgb) ((uint8_t)(((rgb)>>8)&0xff))
#endif

#ifndef GET_ALPHA_VALUE
# define GET_ALPHA_VALUE(rgb) ((uint8_t)((rgb)&0xff))
#endif

#ifndef GET_PIXEL_SIZE
# define GET_PIXEL_SIZE(alpha) ((alpha) ? 4 : 3)
#endif

class PixelUtils
{
private:
    PixelUtils();

public:
    static bool is8bitFormat(AVPixelFormat format);
};
