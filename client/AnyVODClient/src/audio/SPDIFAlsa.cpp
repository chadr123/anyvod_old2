﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SPDIFAlsa.h"

#include <QStringList>
#include <QDebug>

#include <stdint.h>

SPDIFAlsa::SPDIFAlsa() :
    m_handle(nullptr)
{

}

SPDIFAlsa::~SPDIFAlsa()
{
    this->closeInternal();
}

bool SPDIFAlsa::open()
{
    this->m_bytesPerSample = 2 * this->m_channelCount;

    if (this->m_device == -1)
        this->m_deviceName = "hw:0,0";
    else
        this->m_deviceName = this->m_deviceList[this->m_device].name;

    if (snd_pcm_open(&this->m_handle, this->m_deviceName.toLatin1().constData(), SND_PCM_STREAM_PLAYBACK, 0))
    {
        this->close();
        return false;
    }

    if (!this->setDeviceParams(this->m_bufferLength * 1000, 4))
    {
        this->close();
        return false;
    }

    return true;
}

void SPDIFAlsa::close()
{
    this->closeInternal();
}

bool SPDIFAlsa::checkSupport()
{
    snd_pcm_t *handle = nullptr;

    if (snd_pcm_open(&handle, "hw:0,0", SND_PCM_STREAM_PLAYBACK, 0))
        return false;

    snd_pcm_close(handle);

    return true;
}

bool SPDIFAlsa::play()
{
    return true;
}

bool SPDIFAlsa::pause()
{
    return true;
}

bool SPDIFAlsa::resume()
{
    return this->play();
}

bool SPDIFAlsa::stop()
{
    return this->pause();
}

bool SPDIFAlsa::fillBuffer(void *buffer, int size)
{
    uint8_t *buf = (uint8_t*)buffer;
    snd_pcm_uframes_t frames = size / this->m_bytesPerSample;
    snd_pcm_sframes_t wrote = 0;

    while (frames > 0 && !this->m_quit)
    {
        wrote = snd_pcm_writei(this->m_handle, buf, frames);

        if (wrote >= 0)
        {
            frames -= wrote;
            buf += wrote * this->m_bytesPerSample;

            continue;
        }

        switch (wrote)
        {
            case -EPIPE:
            {
                if (snd_pcm_state(this->m_handle) == SND_PCM_STATE_XRUN)
                {
                    if (snd_pcm_prepare(this->m_handle))
                        return false;
                }

                break;
            }
            case -ESTRPIPE:
            {
                while ((wrote = snd_pcm_resume(this->m_handle)) == -EAGAIN)
                    usleep(200);

                if (wrote < 0)
                {
                    if (snd_pcm_prepare(this->m_handle))
                        return false;
                }

                break;
            }
            default:
            {
                return false;
            }
        }
    }

    return true;
}

unsigned int SPDIFAlsa::getNeedBufferSize() const
{
    return 0;
}

void SPDIFAlsa::getDeviceList(QStringList *ret)
{
    QStringList list;

    this->updateDeviceList();

    for (const DeviceItem &item : qAsConst(this->m_deviceList))
        list.append(item.desc);

    *ret = list;
}

int SPDIFAlsa::getDeviceCount() const
{
    return this->m_deviceList.count();
}

double SPDIFAlsa::getLatency()
{
    snd_pcm_sframes_t delay = 0;

    if (this->m_handle && snd_pcm_delay(this->m_handle, &delay))
        return 0.0;

    return delay / (double)this->m_sampleRate;
}

bool SPDIFAlsa::isSupportPull() const
{
    return false;
}

int SPDIFAlsa::getAlignSize() const
{
    return this->m_bytesPerSample;
}

bool SPDIFAlsa::canFillBufferBeforeStart() const
{
    return false;
}

bool SPDIFAlsa::updateDeviceList()
{
    void **hints = nullptr;
    void **hint = nullptr;
    char *name = nullptr;
    char *desc = nullptr;

    if (snd_device_name_hint(-1, "pcm", &hints))
        return false;

    hint = hints;

    this->m_deviceList.clear();

    QStringList filters;

    filters << "digital";
    filters << "iec958";
    filters << "spdif";
    filters << "hdmi";
#if defined Q_OS_RASPBERRY_PI
    filters << "hifiberry_digi";
#endif

    while (*hint != nullptr)
    {
        name = snd_device_name_get_hint(*hint, "NAME");
        desc = snd_device_name_get_hint(*hint, "DESC");

        if (name && desc && strcmp(name, "null"))
        {
            bool ok = false;
            DeviceItem item;

            item.name = name;
            item.desc = desc;

            for (const QString &filter : qAsConst(filters))
            {
                if (item.name.contains(filter, Qt::CaseInsensitive) || item.desc.contains(filter, Qt::CaseInsensitive))
                {
                    ok = true;
                    break;
                }
            }

            if (ok)
                this->m_deviceList.append(item);
        }

        if (name)
            free(name);

        if (desc)
            free(desc);

        hint++;
    }

    snd_device_name_free_hint(hints);
    snd_config_update_free_global();

    return true;
}

bool SPDIFAlsa::setDeviceParams(uint bufferTime, uint periodTime)
{
    snd_pcm_hw_params_t *hwParams;
    snd_pcm_sw_params_t *swParams;
    snd_pcm_uframes_t periodSize;
    snd_pcm_uframes_t bufferSize;

    snd_pcm_hw_params_alloca(&hwParams);
    snd_pcm_sw_params_alloca(&swParams);

    if (snd_pcm_hw_params_any(this->m_handle, hwParams))
        return false;

    if (snd_pcm_hw_params_set_access(this->m_handle, hwParams, SND_PCM_ACCESS_RW_INTERLEAVED))
        return false;

    if (snd_pcm_hw_params_set_format(this->m_handle, hwParams, SND_PCM_FORMAT_S16))
        return false;

    if (snd_pcm_hw_params_set_channels(this->m_handle, hwParams, this->m_channelCount))
        return false;

    if (snd_pcm_hw_params_set_rate(this->m_handle, hwParams, this->m_sampleRate, 0))
        return false;

    if (snd_pcm_hw_params_set_buffer_time_near(this->m_handle, hwParams, &bufferTime, nullptr))
    {
        int dir = -1;
        uint orgTime = bufferTime;
        int attempt = 0;
        int err;

        do
        {
            err = snd_pcm_hw_params_set_buffer_time_near(this->m_handle, hwParams, &bufferTime, &dir);

            if (err)
            {
                if ((bufferTime <= 100000) || (attempt > 0 && bufferTime == orgTime))
                    return false;

                bufferTime -= 100000;
                attempt++;
            }
        } while (err);
    }

    if (snd_pcm_hw_params_set_periods_near(this->m_handle, hwParams, &periodTime, nullptr))
        return false;

    if (snd_pcm_hw_params(this->m_handle, hwParams))
        return false;

    if (snd_pcm_get_params(this->m_handle, &bufferSize, &periodSize))
        return false;

    this->m_bufferLength = bufferTime / 1000;

    if (snd_pcm_sw_params_current(this->m_handle, swParams))
        return false;

    if (snd_pcm_sw_params_set_start_threshold(this->m_handle, swParams, periodSize))
        return false;

    if (snd_pcm_sw_params_set_avail_min(this->m_handle, swParams, periodSize))
        return false;

    if (snd_pcm_sw_params(this->m_handle, swParams))
        return false;

    if (snd_pcm_prepare(this->m_handle))
        return false;

    return true;
}

void SPDIFAlsa::closeInternal()
{
    if (this->m_handle)
    {
        snd_pcm_close(this->m_handle);
        this->m_handle = nullptr;
    }
}
