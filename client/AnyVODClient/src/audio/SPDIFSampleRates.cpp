﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "SPDIFSampleRates.h"

SPDIFSampleRates::SPDIFSampleRates() :
    m_curIndex(0)
{
    this->m_list.append(0);
    this->m_list.append(44100);
    this->m_list.append(48000);
    this->m_list.append(88200);
    this->m_list.append(96000);
    this->m_list.append(176400);
    this->m_list.append(192000);
}

SPDIFSampleRates& SPDIFSampleRates::getInstance()
{
    static SPDIFSampleRates instance;

    return instance;
}

int SPDIFSampleRates::getCount() const
{
    return this->m_list.count();
}

int SPDIFSampleRates::getSampleRate(int index) const
{
    if (index < 0 || index >= this->getCount())
        return this->m_list.first();
    else
        return this->m_list[index];
}

bool SPDIFSampleRates::isDefaultSampleRate(int sampleRate) const
{
    return sampleRate == this->m_list.first();
}

void SPDIFSampleRates::selectNextSampleRate()
{
    int index = this->m_curIndex + 1;

    if (index >= this->getCount())
        index = 0;

    this->m_curIndex = index;
}

void SPDIFSampleRates::selectSampleRate(int sampleRate)
{
    for (int i = 0; i < this->getCount(); i++)
    {
        if (sampleRate == this->getSampleRate(i))
        {
            this->m_curIndex = i;
            break;
        }
    }
}

int SPDIFSampleRates::getSelectedSampleRate() const
{
    return this->m_list[this->m_curIndex];
}

int SPDIFSampleRates::getSelectedSampleRateIndex() const
{
    return this->m_curIndex;
}
