﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#define INITGUID

#include <audioclient.h>

#ifndef _WAVEFORMATEXTENSIBLE_IEC61937_
#define _WAVEFORMATEXTENSIBLE_IEC61937_
typedef struct {
    WAVEFORMATEXTENSIBLE  FormatExt;    /* Format of encoded data as it is */
                                        /* intended to be streamed over the link */
    DWORD   dwEncodedSamplesPerSec;     /* Sampling rate of the post-decode audio. */
    DWORD   dwEncodedChannelCount;      /* Channel count of the post-decode audio. */
    DWORD   dwAverageBytesPerSec;       /* Byte rate of the content, can be 0. */
} WAVEFORMATEXTENSIBLE_IEC61937, *PWAVEFORMATEXTENSIBLE_IEC61937;
#endif // !_WAVEFORMATEXTENSIBLE_IEC61937_
