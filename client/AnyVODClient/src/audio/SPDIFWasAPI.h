﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "SPDIFInterface.h"
#include "SPDIFWasAPIDefs.h"

#include <QVector>

class IMMDevice;

class SPDIFWasAPI : public SPDIFInterface
{
public:
    SPDIFWasAPI();
    virtual ~SPDIFWasAPI();

    virtual bool open();
    virtual void close();
    virtual bool checkSupport();

    virtual bool play();
    virtual bool pause();
    virtual bool resume();
    virtual bool stop();

    virtual bool fillBuffer(void *buffer, int size);
    virtual unsigned int getNeedBufferSize() const;

    virtual void getDeviceList(QStringList *ret);
    virtual int getDeviceCount() const;

    virtual double getLatency();
    virtual bool isSupportPull() const;
    virtual int getAlignSize() const;

    virtual bool canFillBufferBeforeStart() const;

private:
    struct DeviceItem
    {
        QString guid;
        QString name;
    };

private:
    IMMDevice* getDefaultDevice() const;
    IMMDevice* findDevice(const QString &guid) const;
    void makeWaveFormat(WAVEFORMATEXTENSIBLE_IEC61937 *ret) const;
    bool isUSBDevice() const;

    bool updateDeviceList();
    bool createDevice();
    void deleteDevice();

    bool createAudioClient();
    void deleteAudioClient();

    void closeInternal();

    void errorToString(const HRESULT error);

private:
     QVector<DeviceItem> m_deviceList;
     IMMDevice *m_wasDevice;
     IAudioClient *m_audioClient;
     IAudioRenderClient *m_renderClient;
     HANDLE m_needDataEvent;
     double m_latency;
     UINT32 m_bufferSizeInSamples;
};
