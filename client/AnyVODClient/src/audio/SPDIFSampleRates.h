﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QVector>

class SPDIFSampleRates
{
private:
    SPDIFSampleRates();

public:
    static SPDIFSampleRates& getInstance();

public:
    int getCount() const;
    int getSampleRate(int index) const;

    bool isDefaultSampleRate(int sampleRate) const;

    void selectNextSampleRate();
    void selectSampleRate(int sampleRate);
    int getSelectedSampleRate() const;
    int getSelectedSampleRateIndex() const;

private:
    int m_curIndex;
    QVector<int> m_list;
};
