﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "DelegateBase.h"

class ShaderCompositer;

class ShaderCompositerDelegate : public DelegateBase
{
    Q_OBJECT
private:
    ShaderCompositerDelegate();

public:
    static ShaderCompositerDelegate& getInstance();

public slots:
    void resetVideoAttribute();
    void brightnessDown();
    void brightnessUp();
    void saturationDown();
    void saturationUp();
    void hueDown();
    void hueUp();
    void contrastDown();
    void contrastUp();
    void brightness(double step);
    void saturation(double step);
    void hue(double step);
    void contrast(double step);
    void sharply();
    void sharpen();
    void soften();
    void leftRightInvert();
    void topBottomInvert();
    void use360Degree();
    void anaglyphAlgorithmOrder();
    void selectAnaglyphAlgorithm(int algorithm);
    void projectionTypeOrder();
    void selectProjectionType(int type);

private:
    ShaderCompositer &m_shader;
};
