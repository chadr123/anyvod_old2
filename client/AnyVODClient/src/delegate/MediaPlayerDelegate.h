﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "DelegateBase.h"

class MainWindowInterface;
class SPDIFSampleRates;

class MediaPlayerDelegate : public DelegateBase
{
    Q_OBJECT
private:
    MediaPlayerDelegate();

public:
    static MediaPlayerDelegate& getInstance();

private:
    bool moveChapter(int index, QString *desc);
    int getChapterIndex(double pos);

    void rewind(double distance);
    void forward(double distance);

public slots:
    void selectSubtitleClass(const QString &className);
    void selectVideoStream(int index);
    void selectAudioStream(int index);
    void audioNormalize();
    void audioEqualizer();
    void resetSubtitlePosition();
    void upSubtitlePosition();
    void downSubtitlePosition();
    void leftSubtitlePosition();
    void rightSubtitlePosition();
    void rewind5();
    void forward5();
    void rewind30();
    void forward30();
    void rewind60();
    void forward60();
    void gotoBegin();
    void subtitleToggle();
    void prevSubtitleSync();
    void nextSubtitleSync();
    void resetSubtitleSync();
    void subtitleLanguageOrder();
    void videoOrder();
    void audioOrder();
    void detail();
    void deinterlaceMethodOrder();
    void deinterlaceAlgorithmOrder();
    void selectDeinterlacerMethod(int method);
    void selectDeinterlacerAlgorithem(int algorithm);
    void prevAudioSync();
    void nextAudioSync();
    void resetAudioSync();
    void subtitleHAlignOrder();
    void selectSubtitleHAlignMethod(int method);
    void repeatRangeStart();
    void repeatRangeEnd();
    void repeatRangeEnable();
    void repeatRangeStartBack100MS();
    void repeatRangeStartForw100MS();
    void repeatRangeEndBack100MS();
    void repeatRangeEndForw100MS();
    void repeatRangeBack100MS();
    void repeatRangeForw100MS();
    void seekKeyFrame();
    void skipOpening();
    void skipEnding();
    void useSkipRange();
    void prevFrame();
    void nextFrame();
    void slowerPlayback();
    void fasterPlayback();
    void normalPlayback();
    void lowerMusic();
    void lowerVoice();
    void higherVoice();
    void pincushionUpK1();
    void pincushionDownK1();
    void pincushionUpK2();
    void pincushionDownK2();
    void incSubtitleOpaque();
    void decSubtitleOpaque();
    void resetSubtitleOpaque();
    void selectAudioDevice(int device);
    void audioDeviceOrder();
    void enableSearchSubtitle();
    void enableSearchLyrics();
    void subtitleVAlignOrder();
    void selectSubtitleVAlignMethod(int method);
    void incSubtitleSize();
    void decSubtitleSize();
    void resetSubtitleSize();
    void useHWDecoder();
    void useSPDIF();
    void spdifSampleRateOrder();
    void selectSPDIFSampleRate(int sampleRate);
    void usePBO();
    void method3DOrder();
    void select3DMethod(int method);
    void prevChapter();
    void nextChapter();
    void closeExternalSubtitle();
    void showAlbumJacket();
    void lastPlay();
    void saveSubtitle();
    void saveAsSubtitle();
    void selectSPDIFAudioDevice(int device);
    void audioSPDIFDeviceOrder();
    void useFrameDrop();
    void useSPDIFEncodingOrder();
    void selectSPDIFEncodingMethod(int method);
    void method3DSubtitleOrder();
    void select3DSubtitleMethod(int method);
    void use3DFull();
    void reset3DSubtitleOffset();
    void up3DSubtitleOffset();
    void down3DSubtitleOffset();
    void left3DSubtitleOffset();
    void right3DSubtitleOffset();
    void screenRotationDegreeOrder();
    void selectScreenRotationDegree(int degree);
    void histEQ();
    void hq3DDenoise();
    void ataDenoise();
    void owDenoise();
    void deband();
    void deblock();
    void useSearchSubtitleComplex();
    void useBufferingMode();
    void useGPUConvert();
    void autoSaveSearchLyrics();
    void vrInputSourceOrder();
    void selectVRInputSource(int source);
    void upLensCenterX();
    void downLensCenterX();
    void upLensCenterY();
    void downLensCenterY();
    void selectLensCenter(const QVector2D &lensCenter);
    void upVirtual3DDepth();
    void downVirtual3DDepth();
    void barrelUpK1();
    void barrelDownK1();
    void barrelUpK2();
    void barrelDownK2();
    void distortionAdjustModeOrder();
    void selectDistortionAdjustMode(int mode);
    void useDistortion();
    void videoNormalize();
    void nlMeansDenoise();
    void vagueDenoise();
    void hwDecoderOrder();
    void selectHWDecoder(int index);
    void useAutoColorConversion();
    void useIVTC();
    void playOrder();
    void selectPlayingMethod(int method);

private:
    MainWindowInterface *m_mainWindow;
    SPDIFSampleRates &m_spdifSampleRates;
};
