﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ShaderCompositerDelegate.h"
#include "video/ShaderCompositer.h"
#include "media/MediaPlayer.h"
#include "core/EnumsTranslator.h"
#include "utils/FloatPointUtils.h"

ShaderCompositerDelegate::ShaderCompositerDelegate() :
    m_shader(ShaderCompositer::getInstance())
{

}

ShaderCompositerDelegate& ShaderCompositerDelegate::getInstance()
{
    static ShaderCompositerDelegate instance;

    return instance;
}

void ShaderCompositerDelegate::anaglyphAlgorithmOrder()
{
    if (this->m_shader.canUseAnaglyphAlgorithm())
    {
        AnyVODEnums::AnaglyphAlgorithm algorithm = this->m_shader.getAnaglyphAlgorithm();

        algorithm = (AnyVODEnums::AnaglyphAlgorithm)(algorithm + 1);

        if (algorithm >= AnyVODEnums::AGA_COUNT)
            algorithm = AnyVODEnums::AGA_DEFAULT;

        this->selectAnaglyphAlgorithm(algorithm);
    }
}

void ShaderCompositerDelegate::selectAnaglyphAlgorithm(int algorithm)
{
    AnyVODEnums::AnaglyphAlgorithm algo = (AnyVODEnums::AnaglyphAlgorithm)algorithm;

    this->m_player.showOptionDesc(tr("애너글리프 알고리즘 (%1)").arg(this->m_enums.getAnaglyphAlgoritmDesc(algo)));
    this->m_shader.setAnaglyphAlgorithm(algo);
}

void ShaderCompositerDelegate::projectionTypeOrder()
{
    AnyVODEnums::Video360Type type = this->m_shader.getVideo360Type();

    type = (AnyVODEnums::Video360Type)(type + 1);

    if (type >= AnyVODEnums::V3T_COUNT)
        type = AnyVODEnums::V3T_EQR;

    this->selectProjectionType(type);
}

void ShaderCompositerDelegate::selectProjectionType(int type)
{
    this->m_shader.setVideo360Type((AnyVODEnums::Video360Type)type);

    QString desc = tr("360도 영상 프로젝션 종류 : %1").arg(this->m_enums.getProjectionTypeDesc((AnyVODEnums::Video360Type)type));

    this->m_player.showOptionDesc(desc);
}

void ShaderCompositerDelegate::resetVideoAttribute()
{
    if (this->m_player.isEnabledVideo())
    {
        this->m_shader.setBrightness(1.0);
        this->m_shader.setContrast(1.0);
        this->m_shader.setHue(0.0);
        this->m_shader.setSaturation(1.0);

        this->m_player.showOptionDesc(tr("영상 속성 초기화"));
    }
}

void ShaderCompositerDelegate::brightnessDown()
{
    this->brightness(-0.01);
}

void ShaderCompositerDelegate::brightnessUp()
{
    this->brightness(0.01);
}

void ShaderCompositerDelegate::brightness(double step)
{
    if (this->m_player.isEnabledVideo())
    {
        double value = this->m_shader.getBrightness();

        value += step;
        value = FloatPointUtils::zeroDouble(value);

        if (value < 0.0)
            value = 0.0;

        this->m_shader.setBrightness(value);
        this->m_player.showOptionDesc(tr("영상 밝기 %1배").arg(value));
    }
}

void ShaderCompositerDelegate::saturationDown()
{
   this->saturation(-0.01);
}

void ShaderCompositerDelegate::saturationUp()
{
   this->saturation(0.01);
}

void ShaderCompositerDelegate::saturation(double step)
{
    if (this->m_player.isEnabledVideo())
    {
        double value = this->m_shader.getSaturation();

        value += step;
        value = FloatPointUtils::zeroDouble(value);

        if (value < 0.0)
            value = 0.0;

        this->m_shader.setSaturation(value);
        this->m_player.showOptionDesc(tr("영상 채도 %1배").arg(value));
    }
}

void ShaderCompositerDelegate::hueDown()
{
    this->hue(-1.0 / 360.0);
}

void ShaderCompositerDelegate::hueUp()
{
    this->hue(1.0 / 360.0);
}

void ShaderCompositerDelegate::hue(double step)
{
    if (this->m_player.isEnabledVideo())
    {
        double value = this->m_shader.getHue();

        value += step;
        value = FloatPointUtils::zeroDouble(value);

        if (value < 0.0)
            value = 0.0;

        if (value > 1.0)
            value = 1.0;

        this->m_shader.setHue(value);
        this->m_player.showOptionDesc(tr("영상 색상 각도 : %1°").arg((int)(value * 360.0)));
    }
}

void ShaderCompositerDelegate::contrastDown()
{
    this->contrast(-0.01);
}

void ShaderCompositerDelegate::contrastUp()
{
   this->contrast(0.01);
}

void ShaderCompositerDelegate::contrast(double step)
{
    if (this->m_player.isEnabledVideo())
    {
        double value = this->m_shader.getContrast();

        value += step;
        value = FloatPointUtils::zeroDouble(value);

        if (value < 0.0)
            value = 0.0;

        this->m_shader.setContrast(value);
        this->m_player.showOptionDesc(tr("영상 대비 %1배").arg(value));
    }
}

void ShaderCompositerDelegate::sharply()
{
    if (this->m_player.isEnabledVideo())
    {
        bool use = !this->m_shader.isUsingSharply();

        this->m_shader.useSharply(use);

        QString desc;

        if (use)
            desc = tr("영상 날카롭게 켜짐");
        else
            desc = tr("영상 날카롭게 꺼짐");

        this->m_player.showOptionDesc(desc);
    }
}

void ShaderCompositerDelegate::sharpen()
{
    if (this->m_player.isEnabledVideo())
    {
        bool use = !this->m_shader.isUsingSharpen();

        this->m_shader.useSharpen(use);

        QString desc;

        if (use)
            desc = tr("영상 선명하게 켜짐");
        else
            desc = tr("영상 선명하게 꺼짐");

        this->m_player.showOptionDesc(desc);
    }
}

void ShaderCompositerDelegate::soften()
{
    if (this->m_player.isEnabledVideo())
    {
        bool use = !this->m_shader.isUsingSoften();

        this->m_shader.useSoften(use);

        QString desc;

        if (use)
            desc = tr("영상 부드럽게 켜짐");
        else
            desc = tr("영상 부드럽게 꺼짐");

        this->m_player.showOptionDesc(desc);
    }
}

void ShaderCompositerDelegate::leftRightInvert()
{
    if (this->m_player.isEnabledVideo())
    {
        bool use = !this->m_shader.isUsingLeftRightInvert();

        this->m_shader.useLeftRightInvert(use);

        QString desc;

        if (use)
            desc = tr("영상 좌우 반전 켜짐");
        else
            desc = tr("영상 좌우 반전 꺼짐");

        this->m_player.showOptionDesc(desc);
    }
}

void ShaderCompositerDelegate::topBottomInvert()
{
    if (this->m_player.isEnabledVideo())
    {
        bool use = !this->m_shader.isUsingTopBottomInvert();

        this->m_shader.useTopBottomInvert(use);

        QString desc;

        if (use)
            desc = tr("영상 상하 반전 켜짐");
        else
            desc = tr("영상 상하 반전 꺼짐");

        this->m_player.showOptionDesc(desc);
    }
}

void ShaderCompositerDelegate::use360Degree()
{
    bool use = !this->m_shader.is360Degree();

    this->m_shader.use360Degree(use);

    QString desc;

    if (use)
        desc = tr("360도 영상 모드 사용 함");
    else
        desc = tr("360도 영상 모드 사용 안 함");

    this->m_player.showOptionDesc(desc);
}
