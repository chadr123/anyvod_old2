﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "core/AnyVODEnums.h"

#include <QMenu>

class MainWindowInterface;
class Screen;
class MediaPlayer;
class ShortcutKey;
class EnumsTranslator;
class ShaderCompositer;
class ScreenDelegate;
class UpdaterDelegate;
class MediaPlayerDelegate;
class ShaderCompositerDelegate;

class ScreenContextMenu : public QObject
{
    Q_OBJECT
public:
    ScreenContextMenu(Screen *screen);

    void show();

private:
    template <typename T>
    struct ActionAttribute
    {
        ActionAttribute(QPair<AnyVODEnums::ShortcutKey, T> _slot, bool _checked, bool _enabled)
        {
            slot = _slot;
            checked = _checked;
            enabled = _enabled;
        }

        QPair<AnyVODEnums::ShortcutKey, T> slot;
        bool checked;
        bool enabled;
    };

    template <typename T>
    struct ActionAttributeWithKeys
    {
        ActionAttributeWithKeys(QPair<QList<QKeySequence>, T> _slot, const QString &_desc, bool _checked)
        {
            slot = _slot;
            desc = _desc;
            checked = _checked;
        }

        QPair<QList<QKeySequence>, T> slot;
        QString desc;
        bool checked;
    };

private:
    void createDefaultLanguageMenu();
    void createUpdateGapMenu();

    void createScreenMenu();
    void createScreenRotationDegreeMenu(QMenu *menu);
    void createVideoMenu(QMenu *menu);
    void create3DMenu(QMenu *menu);
    void create3DCheckerBoardMenu(QMenu *menu);
    void create3DPageFlipMenu(QMenu *menu);
    void create3DInterlaceMenu(QMenu *menu);
    void create3DAnaglyphMenu(QMenu *menu);
    void createHWDecoderMenu(QMenu *menu);

    void createScreenRatioMenu(QMenu *menu);
    void createUserAspectRatioMenu(QMenu *menu);
    void createDeinterlaceMenu(QMenu *menu);
    void createCaptureMenu(QMenu *menu);
    void createVideoAttributeMenu(QMenu *menu);
    void createVideoEffectMenu(QMenu *menu);

    void createAdjustDistortionModeMenu(QMenu *menu);
    void createVRMenu(QMenu *menu);

    void create360DegreeMenu(QMenu *menu);
    void createProjectionTypeMenu(QMenu *menu);

    void createPlaybackMenu();
    void createPlayOrderMenu(QMenu *menu);
    void createSkipRange(QMenu *menu);
    void createRepeatRange(QMenu *menu);

    void createSubtitleMenu();
    void create3DSubtitleMenu(QMenu *menu);
    void createAlignMenu(QMenu *menu);
    void createLanguageMenu(QMenu *menu);
    void createPositionMenu(QMenu *menu);

    void createSoundMenu();
    void createAudioDeviceMenu(QMenu *menu);
    void createAudioMenu(QMenu *menu);
    void createAudioEffectMenu(QMenu *menu);
    void createSPDIFMenu(QMenu *menu);
    void createSPDIFAudioDeviceMenu(QMenu *menu);
    void createSPDIFEncodingMenu(QMenu *menu);

    void createDTVMenu();
    void createRadioMenu();

    void createContextMenu();

    void addActionsFor3DVideo(QMenu *subMenu, const QVector<AnyVODEnums::Video3DMethod> &methodList) const;

    template <typename T>
    ActionAttribute<T> makeActionAttribute(AnyVODEnums::ShortcutKey key, T slot, bool checked, bool enabled) const;

    template <typename T>
    ActionAttribute<T> makeActionAttribute(AnyVODEnums::ShortcutKey key, T slot, bool checked) const;

    template <typename T>
    ActionAttributeWithKeys<T> makeActionAttributeWithKeys(QList<AnyVODEnums::ShortcutKey> keys, T slot, bool checked) const;

    template <typename T>
    ActionAttributeWithKeys<T> makeActionAttributeWithKeys(QList<AnyVODEnums::ShortcutKey> keys, T slot) const;

private:
    QMenu m_menu;
    MainWindowInterface *m_mainWindow;
    Screen *m_screen;
    MediaPlayer &m_player;
    ShortcutKey &m_shortcutKey;
    EnumsTranslator &m_enums;
    ShaderCompositer &m_shader;
    ScreenDelegate &m_screenDelegate;
    UpdaterDelegate &m_updaterDelegate;
    MediaPlayerDelegate &m_playerDelegate;
    ShaderCompositerDelegate &m_shaderDelegate;
};
