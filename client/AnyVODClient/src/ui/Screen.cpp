﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include <QGlobalStatic>

#define GL_NV_geometry_program4

#ifndef Q_OS_WIN
# define GL_GLEXT_PROTOTYPES
#endif

#include "Screen.h"
#include "ScreenContextMenu.h"
#include "net/Socket.h"
#include "core/Common.h"
#include "core/EnumsTranslator.h"
#include "utils/PathUtils.h"
#include "utils/SeparatingUtils.h"
#include "utils/MessageBoxUtils.h"
#include "utils/ConvertingUtils.h"
#include "utils/StringUtils.h"
#include "video/ShaderCompositer.h"
#include "setting/ScreenSettingLoader.h"
#include "setting/ScreenSettingSaver.h"

#include <qmath.h>
#include <QEvent>
#include <QMouseEvent>
#include <QApplication>
#include <QVector>
#include <QPainter>
#include <QOpenGLFramebufferObject>
#include <QFileInfo>
#include <QDir>
#include <QStandardPaths>
#include <QLocale>
#include <QDebug>

#ifdef Q_OS_WIN
PFNGLGENBUFFERSARBPROC glGenBuffers = nullptr;
PFNGLBINDBUFFERARBPROC glBindBuffer = nullptr;
PFNGLBUFFERDATAARBPROC glBufferData = nullptr;
PFNGLDELETEBUFFERSARBPROC glDeleteBuffers = nullptr;
PFNGLMAPBUFFERARBPROC glMapBuffer = nullptr;
PFNGLUNMAPBUFFERARBPROC glUnmapBuffer = nullptr;
PFNGLACTIVETEXTUREPROC glActiveTexture = nullptr;
#endif

const QEvent::Type Screen::PLAYING_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type Screen::EMPTY_BUFFER_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type Screen::AUDIO_OPTION_DESC_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type Screen::AUDIO_SUBTITLE_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type Screen::ABORT_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type Screen::NONE_PLAYING_DESC_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type Screen::STATUS_CHANGE_EVENT = (QEvent::Type)QEvent::registerEventType();

const QSize Screen::DEFAULT_SCREEN_SIZE = QSize(640, 480);

#ifdef Q_OS_WIN
const int Screen::SCREEN_TOP_OFFSET = 1;
#else
const int Screen::SCREEN_TOP_OFFSET = 0;
#endif

const int Screen::CURSOR_TIME = 2000;
QString Screen::OBJECT_NAME;

Screen::Screen(QWidget *parent, Qt::WindowFlags flags) :
    QGLWidget(parent, nullptr, flags),
    m_player(MediaPlayer::getInstance()),
    m_cursorTimer(0),
    m_waitCounter(0),
    m_visibleWait(false),
    m_disableHideCursor(false),
    m_useVSync(false),
    m_render(false),
    m_initGLUnits(true),
#ifdef Q_OS_WIN
    m_initOpenGLFunctions(false),
#endif
    m_isReady(false),
    m_isFullScreening(false),
    m_lastVisible(true),
    m_offScreen(nullptr),
    m_shader(ShaderCompositer::getInstance())
{
    Screen::OBJECT_NAME = this->objectName();

    QPalette p(this->palette());

    p.setColor(QPalette::Background, Qt::black);
    this->setPalette(p);

    this->setWaitVisible(false);
    this->setPlayerCallbacks();
    this->setMouseTracking(true);

    this->setAttribute(Qt::WA_OpaquePaintEvent);
    this->setAttribute(Qt::WA_NoSystemBackground);
    this->setAttribute(Qt::WA_TouchPadAcceptSingleTouchEvents);
    this->setAttribute(Qt::WA_AcceptTouchEvents);

    this->setAutoFillBackground(false);
    this->setAutoBufferSwap(false);

    this->m_shader.initShaders();

    connect(this, &Screen::screenUpdated, this, (void (Screen::*)())&Screen::update);

    this->m_player.resetScreen(this->width(), this->height(), this->m_texInfo, false);
    this->m_captureHelper.setSavePath(ScreenCaptureHelper::getDefaultSavePath());

    PathUtils::createDirectory(this->m_captureHelper.getSavePath());

    ScreenSettingLoader(this).load();
}

Screen::~Screen()
{
    ScreenSettingSaver(this).save();
}

void Screen::setWaitVisible(bool visible)
{
    this->m_visibleWait = visible;
    this->m_waitCounter = 0;
    this->m_waitTimer.restart();
}

void Screen::renderWait()
{
    QPainter painter(this);

    painter.setRenderHint(QPainter::Antialiasing);

    painter.setPen(QPen(Qt::darkGray, 1));
    painter.setBrush(Qt::SolidPattern);

    qreal maxY = 0.0;

    for (int i = 0; i < 6; i++)
    {
        QColor color(Qt::gray);

        if ((this->m_waitCounter / 5) % 6 == i)
        {
            int red = qMin(color.red() + (this->m_waitCounter % 5) * 32, 255);

            color.setRed(red);
        }

        painter.setBrush(color);

        qreal y = this->height() / 2 + 30 * qFastSin(2 * M_PI * i / 6.0) - 10;

        maxY = max(maxY, y);
        painter.drawEllipse(this->width() / 2 + 30 * qFastCos(2 * M_PI * i / 6.0) - 10, y, 20, 20);
    }

    if (this->m_waitTimer.elapsed() >= 50)
    {
        this->m_waitCounter++;
        this->m_waitTimer.restart();
    }

    this->swapBuffers();
}

void Screen::updateMovie(bool visible)
{
    bool rendered = false;

    if (this->m_initGLUnits)
    {
        this->initTextures();
        this->m_player.applyColorConversion();

        this->m_initGLUnits = false;
    }

    if (this->m_player.isPlayOrPause())
    {
        bool captureBound = false;
        bool capture = this->m_captureHelper.isStarted();

        if (capture)
        {
            this->m_player.setCaptureMode(true);

            this->capturePrologue();
            captureBound = this->m_offScreen->bind();
        }

        rendered = this->m_player.render();

        if (capture)
        {
            this->m_offScreen->release();
            this->captureEpilogue(captureBound);

            this->m_player.setCaptureMode(false);
        }

        if (capture && !captureBound)
            this->m_player.showOptionDesc(tr("캡쳐를 시작하지 못했습니다"));
    }
    else
    {
        QPainter painter(this);
        QSize size(this->width(), this->height());
        QImage icon(ICON_PATH);
        QSize iconSize(256, 256);
        QPoint pos((size.width() - iconSize.width()) / 2, (size.height() - iconSize.height()) / 2);
        QRect rect(pos, iconSize);
        QLinearGradient gradient(QPoint(0, 0), QPoint(size.width() / 2, size.height() / 2));

        gradient.setColorAt(0, QColor(244, 244, 244));
        gradient.setColorAt(1, QColor(214, 214, 214));

        painter.save();

        painter.setRenderHint(QPainter::Antialiasing);
        painter.setRenderHint(QPainter::SmoothPixmapTransform);

        painter.fillRect(QRect(QPoint(0, 0), QPoint(size.width(), size.height())), QBrush(gradient));
        painter.drawImage(rect, icon);

        painter.restore();

        rendered = true;
    }

    if (rendered && visible)
        this->swapBuffers();
}

void Screen::capturePrologue()
{
    int width;
    int height;

    if (this->m_captureHelper.getCapturedCount() <= 0)
    {
        if (this->m_player.getStatus() == MediaPlayer::Paused)
            this->m_captureHelper.markPaused();
        else
            this->m_captureHelper.unmarkPaused();

        if (this->m_captureHelper.isPaused())
            this->m_player.resume(true);
    }

    if (this->m_captureHelper.isCaptureByOriginalSize())
    {
        if (!this->m_player.getFrameSize(&width, &height))
        {
            QSize size = this->size();

            width = size.width();
            height = size.height();
        }
    }
    else
    {
        QSize size = this->m_captureHelper.getSize();

        width = size.width();
        height = size.height();
    }

    this->resizeGL(width, height);
}

void Screen::captureEpilogue(bool captureBound)
{
    QSize orgSize = this->size();
    QSize offSize = this->m_offScreen->size();
    QRect rect;
    QSize targetSize;
    QRect captureRect;
    int targetWidth;
    int targetHeight;

    this->m_player.pause(true);

    if (this->m_captureHelper.isCaptureByOriginalSize())
    {
        targetWidth = orgSize.width();
        targetHeight = orgSize.height();

        this->m_player.getFrameSize(&targetWidth, &targetHeight);
    }
    else
    {
        QSize size = this->m_captureHelper.getSize();

        targetWidth = size.width();
        targetHeight = size.height();
    }

    targetSize = QSize(targetWidth, targetHeight);
    captureRect = QRect(0, offSize.height() - targetHeight, targetWidth, targetHeight);

    this->resizeGL(orgSize.width(), orgSize.height());
    this->m_player.getPictureRect(&rect);

    GLfloat width = targetSize.width() / (GLfloat)this->m_offScreen->width();
    GLfloat height = targetSize.height() / (GLfloat)this->m_offScreen->height();

    glClear(GL_COLOR_BUFFER_BIT);

    glBindTexture(GL_TEXTURE_2D, this->m_offScreen->texture());

    this->renderCapturePicture(rect, QSizeF(width, height));

    glBindTexture(GL_TEXTURE_2D, 0);

    QImage captured = this->m_offScreen->toImage();
    QString path("%1%2_%3_%4.%5");
    QString time;
    QString savePath = this->m_captureHelper.getSavePath();

    PathUtils::createDirectory(savePath);
    SeparatingUtils::appendDirSeparator(&savePath);
    ConvertingUtils::getTimeString(this->m_player.getCurrentPosition(), ConvertingUtils::TIME_HH_MM_SS_ZZZ_DIR, &time);

    QString fileName;

    if (this->m_player.isPlayUserDataEmpty())
        fileName = QFileInfo(this->m_player.getFileName()).baseName();
    else
        fileName = this->m_player.getTitle();

    path = path.arg(savePath, fileName, time)
            .arg(this->m_captureHelper.getCapturedCount())
            .arg(this->m_captureHelper.getExtention());

    path = QDir::toNativeSeparators(path);
    captured = captured.copy(captureRect);

    QString desc;

    if (captureBound && captured.save(path, nullptr, this->m_captureHelper.getQuality()))
        desc = tr("캡쳐 성공");
    else
        desc = tr("캡쳐 실패");

    desc += " : " + StringUtils::normalizedString(QFileInfo(path).fileName());

    this->m_player.resume(true);
    this->m_player.showOptionDesc(desc);

    this->m_captureHelper.reduceRemainedCount();

    if (!this->m_captureHelper.isRemained())
    {
        this->m_captureHelper.stop();

        if (this->m_captureHelper.isPaused())
            this->m_player.pause(true);
    }

    if (!this->m_player.isPlayOrPause())
        this->m_captureHelper.stop();
}

void Screen::renderCapturePicture(const QRect &picArea, const QSizeF &texSize)
{
    QVector4D vColor(1.0, 1.0, 1.0, 1.0);
    QVector3D vertices[] =
    {
        QVector3D(picArea.left(), picArea.bottom(), 0.0f),
        QVector3D(picArea.right(), picArea.bottom(), 0.0f),
        QVector3D(picArea.left(), picArea.top(), 0.0f),
        QVector3D(picArea.right(), picArea.top(), 0.0f)
    };
    QVector2D texCoords[] =
    {
        QVector2D(0.0, 0.0),
        QVector2D(texSize.width(), 0.0),
        QVector2D(0.0, texSize.height()),
        QVector2D(texSize.width(), texSize.height())
    };

    bool enabledBlend = glIsEnabled(GL_BLEND);
    QMatrix4x4 ortho;

    this->m_shader.startSimple();

    glDisable(GL_BLEND);

    ortho.ortho(0.0f, this->width(), this->height(), 0.0f, -1.0f, 1.0f);
    this->m_shader.setRenderData(ShaderCompositer::ST_SIMPLE, ortho, QMatrix4x4(), vertices, texCoords, QRectF(), vColor, AV_PIX_FMT_NONE);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    this->m_shader.endSimple();

    if (enabledBlend)
        glEnable(GL_BLEND);
}

void Screen::toggleUseVSync()
{
    this->setUseVSync(!this->isUseVSync());
    MessageBoxUtils::informationMessageBox(this, tr("수직 동기화 상태를 변경 후 적용 하려면 프로그램을 재 시작 해야 합니다."));
}

void Screen::setUseVSync(bool use)
{
    this->m_useVSync = use;
}

bool Screen::isUseVSync() const
{
    return this->m_useVSync;
}

void Screen::setDisableHideCusor(bool disable)
{
    this->m_disableHideCursor = disable;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Waddress"

void Screen::deleteTexture(TextureID type)
{
    TextureInfo &info = this->m_texInfo[type];

    for (unsigned int i = 0; i < info.textureCount; i++)
    {
        if (info.id[i])
        {
            glDeleteTextures(1, &info.id[i]);
            info.id[i] = 0;
        }
    }

    if (glDeleteBuffers != nullptr)
    {
        glDeleteBuffers(MAX_PBO_COUNT, info.idPBO);

        memset(info.idPBO, 0, sizeof(info.idPBO));
        info.indexPBO = 0;
    }
}

void Screen::genOffScreen(int size)
{
    if (this->m_offScreen)
        delete this->m_offScreen;

    QOpenGLFramebufferObjectFormat format;

    if (this->m_player.isUseHDR())
        format.setInternalTextureFormat(GL_RGBA16);

    this->m_offScreen = new QOpenGLFramebufferObject(size, size, format);

    glBindTexture(GL_TEXTURE_2D, this->m_offScreen->texture());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBindTexture(GL_TEXTURE_2D, 0);

    this->m_offScreen->release();
}

void Screen::genTexture(TextureID type)
{
    TextureInfo &info = this->m_texInfo[type];

    info.textureCount = 1;

    for (unsigned int i = 0; i < info.textureCount; i++)
    {
        glGenTextures(1, &info.id[i]);

        glBindTexture(GL_TEXTURE_2D, info.id[i]);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }

    if (glGenBuffers != nullptr)
        glGenBuffers(MAX_PBO_COUNT, info.idPBO);

    glBindTexture(GL_TEXTURE_2D, 0);
}

#pragma GCC diagnostic pop

void Screen::initTextures()
{
    GLint maxSize;

#ifdef Q_OS_WIN
    if (!this->m_initOpenGLFunctions)
        this->initOpenGLFunctions();
#endif

    int width;
    int height;

    if (this->m_player.getFrameSize(&width, &height))
    {
        maxSize = max(width, height);

        int powValue;

        frexp(maxSize, &powValue);
        maxSize = ::pow(2, powValue);
    }
    else
    {
        maxSize = DEFAULT_MIN_TEXTURE_SIZE;
    }

    glEnable(GL_TEXTURE_2D);

    for (int i = 0; i < TEX_COUNT; i++)
        this->deleteTexture((TextureID)i);

    this->genTexture(TEX_MOVIE_FRAME);
    this->genTexture(TEX_FFMPEG_SUBTITLE);
    this->genTexture(TEX_ASS_SUBTITLE);

    this->genTexture(TEX_YUV_0);
    this->genTexture(TEX_YUV_1);
    this->genTexture(TEX_YUV_2);

    if (this->isHDRTextureSupported())
        this->genTexture(TEX_HDR_MOVIE_FRAME);

    this->genOffScreen(maxSize);

    this->m_player.setMaxTextureSize(maxSize);
}

void Screen::tearDown()
{
    this->unSetPlayerCallbacks();
    this->makeCurrent();

    for (int i = 0; i < TEX_COUNT; i++)
        this->deleteTexture((TextureID)i);

    if (this->m_offScreen)
        delete this->m_offScreen;

    this->m_player.clearFrameBuffers();
    this->m_shader.deInitShaders();

    this->doneCurrent();

    this->m_player.resetScreen(0, 0, nullptr, false);
}

void Screen::initializeGL()
{
#ifdef Q_OS_WIN
    this->initOpenGLFunctions();
#endif

    this->m_player.setHDRTextureSupported(this->isHDRTextureSupported());

    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    this->m_shader.build();
}

void Screen::resizeGL(int width, int height)
{
    if (this->m_isFullScreening)
        height += SCREEN_TOP_OFFSET;

    glViewport(0, 0, width, height);

    QSize size(width, height);

    if (size.width() <= 0)
        size.setWidth(DEFAULT_SCREEN_SIZE.width());

    if (size.height() <= 0)
        size.setHeight(DEFAULT_SCREEN_SIZE.height());

    this->m_player.resetScreen(size.width(), size.height(), this->m_texInfo, true);
}

void Screen::paintGL()
{
    bool render;

    if (!this->m_player.isPlayOrPause())
    {
        render = true;
    }
    else if (this->m_render)
    {
        render = true;
        this->m_render = false;
    }
    else
    {
        render = false;
    }

    if (render)
    {
        this->updateMovie(!this->m_visibleWait);

        if (this->m_visibleWait)
            this->renderWait();
    }
}

void Screen::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == this->m_cursorTimer && !this->m_disableHideCursor)
    {
        if (!this->m_player.isPlayOrPause() || !this->m_player.isEnabledVideo())
            return;

        QPoint localPos = this->mapFromGlobal(QCursor::pos());

        if (localPos.x() < 0 || localPos.y() < 0)
            return;

        QSize size = this->size();

        if (localPos.y() < size.height() && localPos.x() < size.width())
            this->window()->setCursor(Qt::BlankCursor);
    }
}

void Screen::mouseMoveEvent(QMouseEvent *event)
{
    this->window()->unsetCursor();
    QApplication::postEvent(this->window(), new QMouseEvent(event->type(), event->localPos(),
                                                                event->windowPos(), event->screenPos(),
                                                                event->button(), event->buttons(),
                                                                event->modifiers(), event->source()));

    event->accept();
}

void Screen::mousePressEvent(QMouseEvent *event)
{
    this->window()->unsetCursor();
    event->ignore();
}

void Screen::mouseReleaseEvent(QMouseEvent *event)
{
    this->window()->unsetCursor();
    event->ignore();
}

void Screen::wheelEvent(QWheelEvent *event)
{
    this->window()->unsetCursor();
    QApplication::postEvent(this->window(), new QWheelEvent(event->position(), event->globalPosition(),
                                                                event->pixelDelta(), event->angleDelta(),
                                                                event->buttons(), event->modifiers(),
                                                                event->phase(), event->inverted(),
                                                                event->source()));

    event->accept();
}

bool Screen::event(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::TouchBegin:
        {
            event->accept();
            break;
        }
        case QEvent::TouchEnd:
        case QEvent::TouchCancel:
        {
            this->window()->unsetCursor();
            event->accept();

            break;
        }
        default:
        {
            return QGLWidget::event(event);
        }
    }

    return false;
}

void Screen::contextMenuEvent(QContextMenuEvent *)
{
    this->showContextMenu();
}

void Screen::customEvent(QEvent *event)
{
    if (event->type() == STATUS_CHANGE_EVENT)
    {
        StatusChangeEvent *e = (StatusChangeEvent*)event;
        MediaPlayer::Status status = e->getStatus();

        if (status == MediaPlayer::Started ||
                status == MediaPlayer::Playing ||
                status == MediaPlayer::Paused ||
                status == MediaPlayer::Stopped ||
                status == MediaPlayer::Ended)
        {
            if (status == MediaPlayer::Stopped)
            {
                this->update();
                this->setWaitVisible(false);
                this->window()->unsetCursor();

                if (this->m_cursorTimer != 0)
                {
                    this->killTimer(this->m_cursorTimer);
                    this->m_cursorTimer = 0;
                }

                this->m_initGLUnits = true;
            }
            else if (status == MediaPlayer::Started)
            {
                this->m_cursorTimer = this->startTimer(CURSOR_TIME);
            }

            if (!this->m_player.isEnabledVideo())
                this->setWaitVisible(false);
        }
    }
    else if (event->type() == EMPTY_BUFFER_EVENT)
    {
        EmptyBufferEvent *e = (EmptyBufferEvent*)event;

        if (this->m_player.isAudio())
            QApplication::postEvent(this->window(), new EmptyBufferEvent(e->isEmpty()));
        else
            this->setWaitVisible(e->isEmpty());
    }
    else
    {
        QWidget::customEvent(event);
    }
}

void Screen::playingCallback(void *userData)
{
    if (!userData)
        return;

    Screen *parent = (Screen*)userData;

    QApplication::postEvent(parent->window(), new PlayingEvent());
}

void Screen::statusChangedCallback(void *userData)
{
    if (!userData)
        return;

    Screen *parent = (Screen*)userData;
    MediaPlayer::Status status = MediaPlayer::getInstance().getStatus();

    QApplication::postEvent(parent, new StatusChangeEvent(status));
    QApplication::postEvent(parent->window(), new StatusChangeEvent(status));
}

void Screen::emptyBufferCallback(void *userData, bool empty)
{
    if (!userData)
        return;

    QApplication::postEvent((Screen*)userData, new EmptyBufferEvent(empty));
}

void Screen::showAudioOptionDescCallback(void *userData, const QString &desc, bool show)
{
    if (!userData)
        return;

    Screen *parent = (Screen*)userData;

    QApplication::postEvent(parent->window(), new ShowAudioOptionDescEvent(desc, show));
}

void Screen::audioSubtitleCallback(void *userData, const QVector<Lyrics> &lines)
{
    if (!userData)
        return;

    Screen *parent = (Screen*)userData;

    QApplication::postEvent(parent->window(), new AudioSubtitleEvent(lines));
}

void Screen::paintCallback(void *userData)
{
    if (!userData)
        return;

    Screen *parent = (Screen*)userData;

    parent->m_render = true;
    emit parent->screenUpdated();
}

void Screen::abortCallback(void *userData, int reason)
{
    if (!userData)
        return;

    Screen *parent = (Screen*)userData;

    QApplication::postEvent(parent->window(), new AbortEvent(reason));
}

void Screen::nonePlayingDescCallback(void *userData, const QString &desc)
{
    if (!userData)
        return;

    Screen *parent = (Screen*)userData;

    if (parent->m_isReady)
        QApplication::postEvent(parent->window(), new NonePlayingDescEvent(desc));
}

void Screen::setPlayerCallbacks()
{
    this->setPlayerCallbacksWith(this);
}

void Screen::unSetPlayerCallbacks()
{
    this->setPlayerCallbacksWith(nullptr);
}

void Screen::setPlayerCallbacksWith(void *listener)
{
    EventCallback cb;
    cb.callback = Screen::statusChangedCallback;
    cb.userData = listener;
    this->m_player.setStatusChangedCallback(cb);

    cb.callback = Screen::playingCallback;
    cb.userData = listener;
    this->m_player.setPlayingCallback(cb);

    EmptyBufferCallback ecb;

    ecb.callback = Screen::emptyBufferCallback;
    ecb.userData = listener;
    this->m_player.setEmptyBufferCallback(ecb);

    ShowAudioOptionDescCallback acb;

    acb.callback = Screen::showAudioOptionDescCallback;
    acb.userData = listener;
    this->m_player.setShowAudioOptionDescCallback(acb);

    AudioSubtitleCallback asc;

    asc.callback = Screen::audioSubtitleCallback;
    asc.userData = listener;
    this->m_player.setAudioSubtitleCallback(asc);

    PaintCallback pcb;

    pcb.callback = Screen::paintCallback;
    pcb.userData = listener;
    this->m_player.setPaintCallback(pcb);

    AbortCallback atcb;

    atcb.callback = Screen::abortCallback;
    atcb.userData = listener;
    this->m_player.setAbortCallback(atcb);

    NonePlayingDescCallback npcb;

    npcb.callback = Screen::nonePlayingDescCallback;
    npcb.userData = listener;
    this->m_player.setNonePlayingDescCallback(npcb);
}

#ifdef Q_OS_WIN
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-function-type"
void Screen::initOpenGLFunctions()
{
    if (this->m_initOpenGLFunctions)
        return;

    glGenBuffers = (PFNGLGENBUFFERSARBPROC)wglGetProcAddress("glGenBuffersARB");
    glBindBuffer = (PFNGLBINDBUFFERARBPROC)wglGetProcAddress("glBindBufferARB");
    glBufferData = (PFNGLBUFFERDATAARBPROC)wglGetProcAddress("glBufferDataARB");
    glDeleteBuffers = (PFNGLDELETEBUFFERSARBPROC)wglGetProcAddress("glDeleteBuffersARB");
    glBindBuffer = (PFNGLBINDBUFFERARBPROC)wglGetProcAddress("glBindBufferARB");
    glBufferData = (PFNGLBUFFERDATAARBPROC)wglGetProcAddress("glBufferDataARB");
    glMapBuffer = (PFNGLMAPBUFFERARBPROC)wglGetProcAddress("glMapBufferARB");
    glUnmapBuffer = (PFNGLUNMAPBUFFERARBPROC)wglGetProcAddress("glUnmapBufferARB");
    glActiveTexture = (PFNGLACTIVETEXTUREPROC)wglGetProcAddress("glActiveTextureARB");

    this->m_initOpenGLFunctions = true;
}
#pragma GCC diagnostic pop
#endif

bool Screen::isHDRSupported() const
{
    int majorVersion = this->format().majorVersion();
    int minorVersion = this->format().minorVersion();

    return majorVersion >= 3 && minorVersion >= 3;
}

bool Screen::isHDRTextureSupported() const
{
    QString exts = QString::fromLatin1((char *)glGetString(GL_EXTENSIONS));
    QStringList extList = exts.split(" ", Qt::SkipEmptyParts);
    int majorVersion = this->format().majorVersion();
    int minorVersion = this->format().minorVersion();

    return extList.contains("GL_ARB_texture_rg", Qt::CaseInsensitive) || (majorVersion >= 3 && minorVersion >= 0);
}

void Screen::setLastVisible(bool visible)
{
    this->m_lastVisible = visible;
}

bool Screen::getLastVisible() const
{
    return this->m_lastVisible;
}

void Screen::setLastSize(const QSize &size)
{
    this->m_lastSize = size;
}

QSize Screen::getLastSize() const
{
    return this->m_lastSize;
}

void Screen::setFullScreenMode(bool enable)
{
    this->m_isFullScreening = enable;
}

void Screen::markAsReady()
{
    this->m_isReady = true;
}

Screen* Screen::getInstance()
{
    static Screen *instance = nullptr;

    if (instance != nullptr)
        return instance;

    QWidgetList widgets = qApp->topLevelWidgets();

    for (QWidget *widget : qAsConst(widgets))
    {
        if (widget->objectName() == Screen::OBJECT_NAME)
        {
            instance = (Screen*)widget;
            break;
        }
        else
        {
            instance = widget->findChild<Screen*>(Screen::OBJECT_NAME);

            if (instance)
                break;
        }
    }

    return instance;
}

void Screen::showContextMenu()
{
    this->setDisableHideCusor(true);
    ScreenContextMenu(this).show();
    this->setDisableHideCusor(false);
}

ScreenCaptureHelper& Screen::getCaptureHelper()
{
    return this->m_captureHelper;
}
