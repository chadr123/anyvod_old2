﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "MainWindow.h"
#include "ui_mainwindow.h"
#include "PlayList.h"
#include "Login.h"
#include "Equalizer.h"
#include "OpenExternal.h"
#include "SkipRange.h"
#include "Shader.h"
#include "CustomShortcut.h"
#include "FileAssociation.h"
#include "UserAspectRatio.h"
#include "TextEncodeSetting.h"
#include "ServerSetting.h"
#include "ScreenExplorer.h"
#include "SubtitleDirectory.h"
#include "OpenDevice.h"
#include "OpenDTVScanChannel.h"
#include "OpenRadioScanChannel.h"
#include "ViewEPG.h"
#include "ShortcutKey.h"
#include "setting/MainWindowSettingLoader.h"
#include "setting/MainWindowSettingSaver.h"
#include "setting/SettingBackupAndRestore.h"
#include "core/Common.h"
#include "core/PlayListInjector.h"
#include "core/Environment.h"
#include "core/Version.h"
#include "core/VersionDescription.h"
#include "core/FileExtensions.h"
#include "core/EnumsTranslator.h"
#include "utils/PlayItemUtils.h"
#include "utils/MessageBoxUtils.h"
#include "utils/SeparatingUtils.h"
#include "utils/ConvertingUtils.h"
#include "utils/FileListUtils.h"
#include "utils/RemoteFileUtils.h"
#include "utils/DeviceUtils.h"
#include "device/DTVReader.h"
#include "device/RadioReader.h"
#include "language/GlobalLanguage.h"
#include "language/LanguageInstaller.h"
#include "parsers/subtitle/GlobalSubtitleCodecName.h"
#include "media/MediaPlayer.h"
#include "net/Socket.h"
#include "net/Updater.h"
#include "net/VirtualFile.h"
#include "pickers/URLPicker.h"
#include "delegate/MediaPlayerDelegate.h"
#include "video/Camera.h"

#include <qmath.h>
#include <QUrl>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QList>
#include <QMouseEvent>
#include <QFileDialog>
#include <QApplication>
#include <QDesktopServices>
#include <QLocale>
#include <QMimeData>
#include <QRegularExpression>

#ifdef Q_OS_WIN
# include <QWinTaskbarProgress>
#endif

#include <QDebug>

using namespace std;

#ifdef Q_OS_MAC
const int MainWindow::SCREEN_MINIMUM_SIZE = 16;
#else
const int MainWindow::SCREEN_MINIMUM_SIZE = 10;
#endif
const int MainWindow::LOGIN_TIME = 500;
const int MainWindow::SHARE_MEM_TIME = 500;
const int MainWindow::INIT_COMPLETE_TIME = 10;
const int MainWindow::RADIO_METER_TIME = 100;

QString MainWindow::OBJECT_NAME;

MainWindow::MainWindow(const QStringList starting, QWidget *parent) :
    MainWindowInterface(parent),
    ui(new Ui::MainWindow),
    m_buttonPressed(false),
    m_prevMaximized(false),
    m_prevRemoteFileListVisible(false),
    m_prevPlayListVisible(false),
    m_isReady(false),
    m_isFullScreening(false),
    m_spectrumTimerID(0),
    m_nonePlayingDescTimerID(0),
    m_loginTimerID(0),
    m_shareMemTimerID(0),
    m_initCompleteTimerID(0),
    m_radioMeterTimerID(0),
    m_player(MediaPlayer::getInstance()),
    m_remoteFileList(this),
    m_playList(nullptr),
    m_popup(this),
    m_shareMem(PlayListInjector::SHARE_MEM_KEY),
    m_enums(EnumsTranslator::getInstance()),
    m_startingPaths(starting)
{
    this->ui->setupUi(this);

    MainWindow::OBJECT_NAME = this->objectName();

    this->setAttribute(Qt::WA_TouchPadAcceptSingleTouchEvents);
    this->setAttribute(Qt::WA_AcceptTouchEvents);

#ifdef Q_OS_MAC
    const auto btns = this->findChildren<QPushButton*>();

    for (QPushButton *btn : btns)
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);

    const auto labels = this->findChildren<QLabel*>();

    for (QLabel *label : labels)
    {
        QFont font = label->font();

        font.setPointSizeF(12);
        label->setFont(font);
    }
#endif

    this->m_playList = new PlayList(this);
    this->m_playList->adjustPlayIndex();

    this->m_shareMem.create(PlayListInjector::SHARE_MEM_SIZE);
    this->m_shareMemTimerID = this->startTimer(SHARE_MEM_TIME);

    this->updateWindowTitleInternal();

    MainWindowSettingLoader(this).load();

    this->resetWindowTitle();
    this->setAcceptDrops(true);

    this->loadSkin();
    this->initUiComponents();
    this->initPlayer();

    this->setMouseTracking(true);
    this->setFocus();
    this->setEnglishIMEInternal();

    this->m_initCompleteTimerID = this->startTimer(INIT_COMPLETE_TIME);

#ifdef Q_OS_MAC
    this->m_appleMediaKey.init();
#endif
}

MainWindow::~MainWindow()
{
#ifdef Q_OS_MAC
    this->m_appleMediaKey.deInit();
#endif

    delete this->m_playList;
    delete ui;
}

#ifdef Q_OS_WIN
void MainWindow::initThumbnailBar()
{
    this->m_thumbnailToolBar.setParent(this);
    this->m_thumbnailToolBar.setWindow(this->windowHandle());

    this->m_thumbnailToolResumePause.setParent(&this->m_thumbnailToolBar);

    this->m_thumbnailToolStop.setParent(&this->m_thumbnailToolBar);
    this->m_thumbnailToolStop.setEnabled(false);
    this->m_thumbnailToolStop.setIcon(QIcon(":/icons/18_64x64.png"));

    this->m_thumbnailToolBackward.setParent(&this->m_thumbnailToolBar);
    this->m_thumbnailToolBackward.setIcon(QIcon(":/icons/26_64x64.png"));

    this->m_thumbnailToolForward.setParent(&this->m_thumbnailToolBar);
    this->m_thumbnailToolForward.setIcon(QIcon(":/icons/27_64x64.png"));

    connect(&this->m_thumbnailToolResumePause, &QWinThumbnailToolButton::clicked, this, &MainWindow::on_resume_clicked);
    connect(&this->m_thumbnailToolStop, &QWinThumbnailToolButton::clicked, this, &MainWindow::on_stop_clicked);
    connect(&this->m_thumbnailToolBackward, &QWinThumbnailToolButton::clicked, this, &MainWindow::prev);
    connect(&this->m_thumbnailToolForward, &QWinThumbnailToolButton::clicked, this, &MainWindow::next);

    this->m_thumbnailToolBar.addButton(&this->m_thumbnailToolResumePause);
    this->m_thumbnailToolBar.addButton(&this->m_thumbnailToolStop);
    this->m_thumbnailToolBar.addButton(&this->m_thumbnailToolBackward);
    this->m_thumbnailToolBar.addButton(&this->m_thumbnailToolForward);

    this->translateThumbnailText();
}

void MainWindow::setThumbnailButtonType(bool resume)
{
    if (resume)
    {
        this->m_thumbnailToolResumePause.setToolTip(tr("재생"));
        this->m_thumbnailToolResumePause.setIcon(QIcon(":/icons/31_64x64.png"));

        connect(&this->m_thumbnailToolResumePause, &QWinThumbnailToolButton::clicked, this, &MainWindow::on_resume_clicked);
    }
    else
    {
        this->m_thumbnailToolResumePause.setToolTip(tr("일시정지"));
        this->m_thumbnailToolResumePause.setIcon(QIcon(":/icons/33_64x64.png"));

        connect(&this->m_thumbnailToolResumePause, &QWinThumbnailToolButton::clicked, this, &MainWindow::on_pause_clicked);
    }
}

void MainWindow::translateThumbnailText()
{
    this->m_thumbnailToolStop.setToolTip(tr("정지"));
    this->m_thumbnailToolBackward.setToolTip(tr("이전 파일"));
    this->m_thumbnailToolForward.setToolTip(tr("다음 파일"));

    this->setThumbnailButtonType(this->ui->resume->isEnabled());
}
#endif

QUuid MainWindow::getUniqueInPlayer() const
{
    return this->m_player.getUnique();
}

void MainWindow::resetWindowTitle()
{
    this->setWindowTitle(APP_NAME);
    this->setFileNameIndicator(VersionDescription::getAppNameWithVersion());
}

void MainWindow::setFileNameIndicator(const QString &fileName)
{
    this->ui->currentFileName->setText(fileName);
    this->ui->currentFileName->setToolTip(fileName);
}

bool MainWindow::isPrevEnabled() const
{
    return this->ui->prev->isEnabled();
}

bool MainWindow::isNextEnabled() const
{
    return this->ui->next->isEnabled();
}

bool MainWindow::isMostTop() const
{
    return IS_BIT_SET(this->windowFlags(), Qt::WindowStaysOnTopHint);
}

bool MainWindow::isMute() const
{
    return this->ui->mute->isChecked();
}

bool MainWindow::isReady() const
{
    return this->m_isReady;
}

void MainWindow::setSeekingText()
{
    if (this->m_player.isAudio() && this->m_player.isRemoteFile())
        this->ui->audioBuffering->setText(tr("탐색 중입니다"));
}

QWidget& MainWindow::getControlBar()
{
    return *this->ui->controlBar;
}

void MainWindow::updateWindowTitle()
{
    this->updateWindowTitleInternal();
}

void MainWindow::moveChildWindows()
{
    if (this->m_playList)
        this->m_playList->move(QPoint(this->x() + this->frameGeometry().width(), this->y()));

    this->m_remoteFileList.move(QPoint(this->x(), this->y() + this->frameGeometry().height()));
    this->movePopup();
}

void MainWindow::movePopup()
{
    if (!this->m_isReady)
        return;

    int xFrame = (this->frameGeometry().width() - this->width()) / 2;
    int x = this->frameGeometry().width() - this->m_popup.frameGeometry().width() - xFrame;

    int yFrame = (this->frameGeometry().height() - this->height());
    int y = this->ui->screen->height() - this->m_popup.frameGeometry().height() + yFrame - xFrame;

    this->m_popup.move(QPoint(this->x() + x, this->y() + y));
}

bool MainWindow::isFullScreening() const
{
    return this->m_isFullScreening;
}

UserAspectRatios& MainWindow::getUserAspectRatios()
{
    return this->m_userAspectRatios;
}

bool MainWindow::canShowControlBar() const
{
    return this->m_player.isEnabledVideo() && !this->isFullScreen();
}

MainWindow* MainWindow::getInstance()
{
    static MainWindow *instance = nullptr;

    if (instance != nullptr)
        return instance;

    QWidgetList widgets = qApp->topLevelWidgets();

    for (QWidget *widget : qAsConst(widgets))
    {
        if (widget->objectName() == MainWindow::OBJECT_NAME)
        {
            instance = (MainWindow*)widget;
            break;
        }
    }

    return instance;
}

bool MainWindow::isLogined() const
{
    return Socket::getInstance().isLogined() && Socket::getStreamInstance().isLogined();
}

void MainWindow::moveEvent(QMoveEvent *)
{
    this->moveChildWindows();
}

void MainWindow::resizeEvent(QResizeEvent *)
{
    this->moveChildWindows();
}

void MainWindow::closeEvent(QCloseEvent *)
{
    this->m_player.close();

    if (this->isFullScreen())
        this->fullScreen();

    this->ui->screen->tearDown();
    this->m_playList->saveSettings();

    MainWindowSettingSaver(this).save();
}

void MainWindow::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        case QEvent::LocaleChange:
        {
            QString lang = QLocale::system().name().split("_").first();

            this->setLanguage(lang);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

bool MainWindow::event(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::TouchBegin:
            break;
        case QEvent::TouchEnd:
        case QEvent::TouchCancel:
            this->processMouseUp();
            break;
        default:
            return QMainWindow::event(event);
    }

    event->accept();

    return true;
}

void MainWindow::contextMenuEvent(QContextMenuEvent *)
{
    this->ui->screen->showContextMenu();
}

void MainWindow::mouseDoubleClickEvent(QMouseEvent *)
{
    this->m_player.resetFOV();
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
        this->processMouseDown();
}

void MainWindow::mouseReleaseEvent(QMouseEvent *)
{
    this->processMouseUp();
}

void MainWindow::mouseMoveEvent(QMouseEvent *)
{
    this->processMouseMoving();
}

void MainWindow::wheelEvent(QWheelEvent *event)
{
    static QPoint accumDegree;
    QPoint degree = event->angleDelta();

    if (event->phase() == Qt::ScrollEnd)
    {
        accumDegree = QPoint();
        return;
    }

    if (degree.isNull())
        return;

    if (!this->is360DegreeInteraction())
        return;

    accumDegree += degree;

    if (degree.y() > 0 && accumDegree.y() < 120)
        return;

    if (degree.y() < 0 && accumDegree.y() > -120)
        return;

    accumDegree = accumDegree / 8 / 15;

    this->m_player.addFOV(accumDegree.y());

    accumDegree = QPoint();
}

void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    this->processDragEnterEvent(event);
}

void MainWindow::setEnglishIME() const
{
    this->setEnglishIMEInternal();
}

void MainWindow::openScreenExplorer()
{
    if (this->m_player.hasDuration() && !this->m_player.isAudio() && !this->m_player.isRemoteFile())
    {
        ScreenExplorer dlg(this);

        dlg.exec();
    }
}

void MainWindow::openSubtitleDirectory()
{
    QStringList paths;
    bool prior;

    this->m_player.getSubtitleDirectory(&paths, &prior);

    SubtitleDirectory dlg(paths, prior, this);

    if (dlg.exec())
    {
        dlg.getResult(&paths, &prior);
        this->m_player.setSubtitleDirectory(paths, prior);
    }
}

void MainWindow::openImportFonts()
{
    QString fontDir(QFileInfo(Environment::getInstance().getFontFilePath()).absolutePath());
    QString msg;

    msg = tr("관리자 권한으로 실행되지 않을 경우 정상적으로 가져오기가 안될 수 있습니다.\n만약에 관리자 권한이 아닐 경우 아래 경로에 폰트를 수동으로 복사 하세요.\n계속 하시겠습니까?");
    msg += "\n\n";
    msg += fontDir;

    if (MessageBoxUtils::questionMessageBox(this, msg))
    {
        QString filter = tr("폰트 (*.%1);;모든 파일 (*.*)").arg(FileExtensions::FONT_EXTS_LIST.join(" *."));
        QStringList files = QFileDialog::getOpenFileNames(this, tr("자막 폰트 가져오기"), this->m_lastSelectedDir, filter, nullptr, QFileDialog::HideNameFilterDetails);

        if (!files.isEmpty())
        {
            bool copied = true;

            SeparatingUtils::appendDirSeparator(&fontDir);

            for (const QString &filePath : qAsConst(files))
            {
                QFile file(filePath);
                QString dest = fontDir + QFileInfo(file).fileName();

                copied &= file.copy(dest);
            }

            if (copied)
                this->m_player.showOptionDesc(tr("복사가 완료 되었습니다."));
            else
                this->m_player.showOptionDesc(tr("일부 파일이 복사되지 않았습니다."));

            this->m_lastSelectedDir = fontDir;
        }
    }
}

void MainWindow::openDevice()
{
    OpenDevice dlg(this);

    if (dlg.exec() != QDialog::Accepted)
        return;

    QString path;
    QString desc;
    QVector<PlayItem> itemVector;
    QStringList list;

    dlg.getDevicePath(&path);
    dlg.getDesc(&desc);

    list.append(path);
    itemVector = PlayItemUtils::getPlayItemFromFileNames(list);

    if (!itemVector.isEmpty())
    {
        itemVector[0].title = desc;
        itemVector[0].extraData.userData = path.toUtf8();
    }

    this->addToPlayList(itemVector, true, true);
}

void MainWindow::addDTVChannelToPlaylist()
{
    const DTVReader &reader = DTVReader::getInstance();
    QVector<DTVReader::ChannelInfo> scanned;

    if (!reader.isSupport())
        return;

    reader.getScannedChannels(&scanned);

    if (scanned.isEmpty())
    {
        this->openScanDTVChannel();
    }
    else
    {
        QVector<PlayItem> itemVector;

        for (const DTVReader::ChannelInfo &channelInfo : qAsConst(scanned))
        {
            QStringList urlList;
            QVector<PlayItem> single;
            QString url = DTVReader::makeDTVPath(channelInfo);

            urlList.append(url);

            single = PlayItemUtils::getPlayItemFromFileNames(urlList);

            if (single.isEmpty())
                continue;

            DTVReaderInterface::AdapterInfo info;

            if (reader.getAdapterInfo(channelInfo.adapter, &info))
            {
                if (channelInfo.name.isEmpty())
                {
                    single[0].title = QString("%1 (%2)")
                            .arg(channelInfo.channel)
                            .arg(info.name);
                }
                else
                {
                    single[0].title = QString("%1 (%2, %3)")
                            .arg(channelInfo.name)
                            .arg(channelInfo.channel)
                            .arg(info.name);
                }

                single[0].extraData.userData = url.toUtf8();
            }

            itemVector += single;
        }

        this->addToPlayList(itemVector, true, true);
    }
}

void MainWindow::addRadioChannelToPlaylist()
{
    const RadioReader &reader = RadioReader::getInstance();
    QVector<RadioReader::ChannelInfo> scanned;

    if (!reader.isSupport())
        return;

    reader.getScannedChannels(&scanned);

    if (scanned.isEmpty())
    {
        this->openScanRadioChannel();
    }
    else
    {
        QVector<PlayItem> itemVector;
        QMap<int, RadioReaderInterface::AdapterInfo> adapterInfos;

        for (const RadioReader::ChannelInfo &channelInfo : qAsConst(scanned))
        {
            QStringList urlList;
            QVector<PlayItem> single;
            QString url = RadioReader::makeRadioPath(channelInfo);

            urlList.append(url);

            single = PlayItemUtils::getPlayItemFromFileNames(urlList);

            if (single.isEmpty())
                continue;

            RadioReaderInterface::AdapterInfo info;
            auto iter = adapterInfos.find(channelInfo.adapter);

            if (iter == adapterInfos.end())
            {
                if (reader.getAdapterInfo(channelInfo.adapter, &info))
                {
                    adapterInfos.insert(channelInfo.adapter, info);
                    iter = adapterInfos.find(channelInfo.adapter);
                }
            }

            if (iter != adapterInfos.end())
            {
                info = *iter;

                single[0].title = QString("%1 %2Hz (%3)")
                        .arg(reader.demodulatorTypeToString(channelInfo.type),
                             ConvertingUtils::valueToString(channelInfo.freq),
                             info.name);
                single[0].extraData.userData = url.toUtf8();
            }

            itemVector += single;
        }

        this->addToPlayList(itemVector, true, true);
    }
}

void MainWindow::openScanRadioChannel()
{
    RadioReader &reader = RadioReader::getInstance();
    QVector<RadioReaderInterface::AdapterInfo> adapters;

    reader.getAdapterList(&adapters);

    if (!reader.isSupport())
        return;

    if (adapters.isEmpty())
    {
        MessageBoxUtils::criticalMessageBox(this, tr("라디오 수신 장치가 없습니다."));
        return;
    }

    OpenRadioScanChannel dlg(reader, this);

    if (dlg.exec() != QDialog::Accepted)
        return;

    QVector<RadioReader::ChannelInfo> scanned;

    reader.getScannedChannels(&scanned);

    if (!scanned.isEmpty())
        this->addRadioChannelToPlaylist();
}

void MainWindow::openScanDTVChannel()
{
    DTVReader &reader = DTVReader::getInstance();
    QVector<DTVReaderInterface::AdapterInfo> adapters;

    reader.getAdapterList(&adapters);

    if (!reader.isSupport())
        return;

    if (adapters.isEmpty())
    {
        MessageBoxUtils::criticalMessageBox(this, tr("DTV 수신 장치가 없습니다."));
        return;
    }

    OpenDTVScanChannel dlg(reader, this);

    if (dlg.exec() != QDialog::Accepted)
        return;

    QVector<DTVReader::ChannelInfo> scanned;

    reader.getScannedChannels(&scanned);

    if (!scanned.isEmpty())
        this->addDTVChannelToPlaylist();
}

void MainWindow::viewEPG()
{
    DTVReader &reader = DTVReader::getInstance();

    if (!reader.isSupport() || !reader.isOpened())
        return;

    QString channelName = this->m_playList->getCurrentPlayItem().title;
    ViewEPG dlg(reader, channelName, this);

    dlg.exec();
}

void MainWindow::saveSettingsToFile()
{
    QString path = QFileDialog::getSaveFileName(this, QString(), QString(), tr("설정 (*.%1);;모든 파일 (*.*)").arg("settings"));

    if (path.isEmpty())
        return;

    if (!SettingBackupAndRestore::saveSettings(path))
        MessageBoxUtils::criticalMessageBox(this, tr("설정을 저장하지 못했습니다."));
}

void MainWindow::loadSettingsToFile()
{
    QString path = QFileDialog::getOpenFileName(this, QString(), QString(), tr("설정 (*.%1);;모든 파일 (*.*)").arg("settings"), nullptr, QFileDialog::HideNameFilterDetails);

    if (path.isEmpty())
        return;

    if (SettingBackupAndRestore::loadSettings(path))
        MessageBoxUtils::informationMessageBox(this, tr("설정을 불러왔습니다. 적용하려면 AnyVOD를 재 시작 해주세요."));
    else
        MessageBoxUtils::criticalMessageBox(this, tr("설정을 불러오지 못했습니다. 관리자 권한으로 실행 후 재 시도 해주세요."));
}

void MainWindow::openAudioCD()
{
    QString audioCDPath;

#ifdef Q_OS_WIN
    QString dir = QFileDialog::getExistingDirectory(this);

    if (!dir.isEmpty())
    {
        const QFileInfoList drives = QDir::drives();

        for (const QFileInfo &drive : drives)
        {
            QString driveLetter = drive.absolutePath();

            if (dir.startsWith(driveLetter, Qt::CaseInsensitive))
            {
                audioCDPath = driveLetter.remove(SeparatingUtils::WINDOWS_DIR_SEPARATOR).remove(SeparatingUtils::DIR_SEPARATOR).trimmed();
                break;
            }
        }
    }
#elif defined Q_OS_LINUX
    QString filter = tr("시디롬 (sr*);;모든 파일 (*.*)");
    QString path = QFileDialog::getOpenFileName(this, QString(), "/dev", filter, nullptr, QFileDialog::HideNameFilterDetails);

    audioCDPath = path;
#elif defined Q_OS_MAC
    audioCDPath = tr("기본 장치");
#endif

    if (audioCDPath.isEmpty())
        return;

    QVector<PlayItem> itemVector;
    QStringList list;
    QString cdioPath = DeviceUtils::CDIO_PROTOCOL + audioCDPath;

    list.append(cdioPath);
    itemVector = PlayItemUtils::getPlayItemFromFileNames(list);

    if (!itemVector.isEmpty())
    {
        itemVector[0].title = tr("오디오 시디(%1)").arg(audioCDPath);
        itemVector[0].extraData.userData = cdioPath.toUtf8();
    }

    this->addToPlayList(itemVector, true, true);
}

void MainWindow::openCustomShortcut()
{
    CustomShortcut dlg(this);

    dlg.exec();
}

void MainWindow::openFileAssociation()
{
    FileAssociation dlg(this);

    dlg.exec();
}

void MainWindow::userAspectRatioOrder()
{
    this->m_userAspectRatios.selectNextRatio();
    this->selectUserAspectRatio(this->m_userAspectRatios.getSelectedRatioIndex());
}

void MainWindow::selectUserAspectRatio(int index)
{
    UserAspectRatioInfo ratio;
    UserAspectRatios::Item item = this->m_userAspectRatios.getRatio(index);

    if (item.isInvalid())
    {
        ratio.use = false;
    }
    else if (item.isFullScreen())
    {
        ratio.use = true;
        ratio.fullscreen = true;
    }
    else
    {
        ratio.use = true;

        ratio.width = item.width;
        ratio.height = item.height;
    }

    this->m_userAspectRatios.selectRatio(index);
    this->m_player.setUserAspectRatio(ratio);
}

void MainWindow::openUserAspectRatio()
{
    UserAspectRatios::Item item = this->m_userAspectRatios.getUserRatio();
    UserAspectRatio dlg(item.width, item.height, this);

    if (dlg.exec() != QDialog::Accepted)
        return;

    QSizeF size;

    dlg.getSize(&size);

    item.width = size.width();
    item.height = size.height();

    this->m_userAspectRatios.setUserRatio(item);
    this->selectUserAspectRatio(this->m_userAspectRatios.getUserRatioIndex());
}

void MainWindow::screenSizeHalf()
{
    this->setScreenSizeRatio(0.5);
}

void MainWindow::screenSizeNormal()
{
    this->setScreenSizeRatio(1.0);
}

void MainWindow::screenSizeNormalHalf()
{
    this->setScreenSizeRatio(1.5);
}

void MainWindow::screenSizeDouble()
{
    this->setScreenSizeRatio(2.0);
}

void MainWindow::adjustSPDIFAudioUI()
{
    Ui::MainWindow *ctrl = this->ui;
    bool toggle = this->m_player.isUseSPDIF();

    ctrl->volume->setEnabled(!toggle);
    ctrl->mute->setEnabled(!toggle);
}

bool MainWindow::is360DegreeInteraction() const
{
    return ShaderCompositer::getInstance().is360Degree() && this->m_player.getStatus() != MediaPlayer::Stopped;
}

void MainWindow::selectLanguage(const QString &lang, const QString &desc)
{
    this->setLanguage(lang);
    this->m_player.showOptionDesc(desc);
}

void MainWindow::openTextEncoding()
{
    TextEncodeSetting dlg(this);
    int ret = dlg.exec();

    if (ret != QDialog::Accepted)
        return;

    QString encoding;

    dlg.getEncoding(&encoding);

    if (encoding == TextEncodeSetting::DEFAULT_NAME)
        encoding = "System";

    GlobalSubtitleCodecName::getInstance().setCodecName(encoding);
}

void MainWindow::openServerSetting()
{
    ServerSetting dlg(this->m_remoteServerInfo, this);
    int ret = dlg.exec();

    if (ret != QDialog::Accepted)
        return;

    this->m_remoteServerInfo = dlg.getInformation();
}

RemoteFileList& MainWindow::getRemoteFileList()
{
    return this->m_remoteFileList;
}

PlayList& MainWindow::getPlayList()
{
    return *this->m_playList;
}

void MainWindow::handleSpectrumTimerEvent()
{
    this->ui->spectrum->changeHandle(this->m_player.getAudioHandle());
    this->ui->spectrum->updateSpectrum();
}

void MainWindow::handleNonePlayingDescTimerEvent(QTimerEvent *event)
{
    this->ui->audioOptionDesc->setVisible(false);
    this->ui->audioOptionDesc->setText(QString());
    this->killTimer(event->timerId());
    this->m_nonePlayingDescTimerID = 0;
}

void MainWindow::handleLoginTimerEvent(QTimerEvent *event)
{
    if (!this->isLogined())
    {
        this->m_remoteFileList.hide();
        this->m_remoteFileList.clearFileList();

        if (this->m_player.isRemoteFile())
            this->m_player.close();

        this->killTimer(event->timerId());
    }
}

void MainWindow::startByFirstAppInstance()
{
    QVector<PlayItem> items;
    QVector<PlayItem> filterdItems;
    QString subtitle;

    items = PlayItemUtils::getPlayItemFromFileNames(this->m_startingPaths);

    for (const PlayItem &item : qAsConst(items))
    {
        QFileInfo info(item.path);

        if (FileExtensions::SUBTITLE_EXTS_LIST.contains(info.suffix().toLower()))
            subtitle = info.filePath();
        else
            filterdItems.append(item);
    }

    this->addToPlayList(filterdItems, true, false);

    if (!subtitle.isEmpty())
        this->m_player.openSubtitle(SeparatingUtils::adjustNetworkPath(subtitle), true);

    this->m_startingPaths.clear();
}

void MainWindow::startByAnotherAppInstance()
{
    char *data = (char*)this->m_shareMem.data();
    QString path = QString::fromUtf8(data).trimmed();

    if (!path.isEmpty())
    {
        QStringList list = path.split(NEW_LINE, Qt::SkipEmptyParts);
        QVector<PlayItem> playList;
        QVector<PlayItem> filterdItems;
        QString subtitle;

        playList = PlayItemUtils::getPlayItemFromFileNames(list);

        for (const PlayItem &item : qAsConst(playList))
        {
            QFileInfo info(item.path);

            if (FileExtensions::SUBTITLE_EXTS_LIST.contains(info.suffix().toLower()))
                subtitle = info.filePath();
            else
                filterdItems.append(item);
        }

        this->addToPlayList(filterdItems, true, true);

        if (!subtitle.isEmpty())
            this->m_player.openSubtitle(SeparatingUtils::adjustNetworkPath(subtitle), true);

        memset(data, 0, PlayListInjector::SHARE_MEM_SIZE);

        QApplication::alert(this);
    }

    this->m_shareMem.unlock();
}

void MainWindow::handleShareMemTimerEvent()
{
    if (!this->m_startingPaths.isEmpty())
        this->startByFirstAppInstance();
    else if (this->m_shareMem.lock())
        this->startByAnotherAppInstance();
}

void MainWindow::checkSoftwareUpdate()
{
    QString updateURL = Updater::getInstance().getUpdateURL();

    if (updateURL.length() > 0)
    {
        bool download = MessageBoxUtils::questionMessageBox(this, tr("AnyVOD 업데이트가 존재합니다.\n업데이트를 다운로드 하시겠습니까?"));

        if (download)
            QDesktopServices::openUrl(QUrl(updateURL));
    }

    Updater::getInstance().checkUpdate();
}

void MainWindow::handleInitCompleteTimerEvent()
{
    this->m_isReady = true;
    this->ui->screen->markAsReady();

#ifdef Q_OS_WIN
    this->m_taskbarButton.setParent(this);
    this->m_taskbarButton.setWindow(this->windowHandle());

    this->initThumbnailBar();
#endif

    this->killTimer(this->m_initCompleteTimerID);
    this->m_initCompleteTimerID = 0;

    this->setFocus();
    this->checkSoftwareUpdate();
}

void MainWindow::handleRadioMeterTimerEvent()
{
    this->updateRadioMeter();
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    if (this->m_spectrumTimerID == event->timerId())
        this->handleSpectrumTimerEvent();
    else if (this->m_nonePlayingDescTimerID == event->timerId())
        this->handleNonePlayingDescTimerEvent(event);
    else if (this->m_loginTimerID == event->timerId())
        this->handleLoginTimerEvent(event);
    else if (this->m_shareMemTimerID == event->timerId())
        this->handleShareMemTimerEvent();
    else if (this->m_initCompleteTimerID == event->timerId())
        this->handleInitCompleteTimerEvent();
    else if (this->m_radioMeterTimerID == event->timerId())
        this->handleRadioMeterTimerEvent();
}

void MainWindow::dropEvent(QDropEvent *event)
{
    this->processDropEvent(event);
}

void MainWindow::resetAudioSubtitle()
{
    Ui::MainWindow *ctrl = this->ui;
    QPalette pal;

    pal = ctrl->audioSubtitle1->palette();
    pal.setColor(QPalette::WindowText, QColor(0, 0, 0));

    this->setPaletteAndText(ctrl->audioSubtitle1, pal, QString());
    this->setPaletteAndText(ctrl->audioSubtitle2, pal, QString());
    this->setPaletteAndText(ctrl->audioSubtitle3, pal, QString());
}

void MainWindow::adjustScreenSizeForVideo()
{
    Ui::MainWindow *ctrl = this->ui;
    Screen *screen = ctrl->screen;
    QSize mainSize = screen->size();
    QSize screenLastSize = screen->getLastSize();
    int controlBarHeight = ctrl->controlBar->height();

    if (mainSize.height() <= SCREEN_MINIMUM_SIZE)
        mainSize.setHeight(screenLastSize.height() - mainSize.height());

    if (this->m_player.isAudio())
    {
        mainSize.setWidth(this->width());
    }
    else
    {
        if (mainSize.width() <= 0)
            mainSize.setWidth(screenLastSize.width());
    }

    this->setMinimumHeight(controlBarHeight);
    this->setMaximumHeight(QWIDGETSIZE_MAX);

    if (!this->isFullScreen())
    {
        int centerHeight = ctrl->centralWidget->height();

        if (ctrl->controlBar->isVisible())
            mainSize.rheight() += controlBarHeight;

        if (centerHeight > mainSize.height())
            mainSize.rheight() += centerHeight - mainSize.height();

        this->resize(mainSize);
    }

    screen->setLastVisible(true);
}

void MainWindow::adjustScreenSizeForAudio()
{
    Ui::MainWindow *ctrl = this->ui;
    Screen *screen = ctrl->screen;

    if (this->isFullScreen())
    {
        this->m_remoteFileList.setVisible(this->m_prevRemoteFileListVisible);
        this->m_playList->setVisible(this->m_prevPlayListVisible);

        this->setFullScreenMode(false);
        this->showNormal();
    }

    if (screen->getLastVisible())
    {
        if (ctrl->screen->size().height() > 0)
            ctrl->screen->setLastSize(QSize(ctrl->screen->getLastSize().width(), ctrl->screen->size().height()));

        if (ctrl->screen->size().width() > 0)
            ctrl->screen->setLastSize(QSize(ctrl->screen->size().width(), ctrl->screen->getLastSize().height()));

        if (!ctrl->controlBar->isVisible())
            ctrl->screen->setLastSize(QSize(ctrl->screen->getLastSize().width(), ctrl->screen->getLastSize().height() - CONTROL_BAR_HEIGHT - SCREEN_MINIMUM_SIZE));
    }

    screen->setLastVisible(false);
    ctrl->controlBar->show();

    QSize controlBarSize = ctrl->controlBar->size();

    controlBarSize.setHeight(CONTROL_BAR_HEIGHT);
    this->resize(controlBarSize);
    this->setMinimumHeight(CONTROL_BAR_HEIGHT);
    this->setMaximumHeight(CONTROL_BAR_HEIGHT);
}

void MainWindow::handlePlayingStatus()
{
    Ui::MainWindow *ctrl = this->ui;

    ctrl->stop->setEnabled(true);
    ctrl->resume->setEnabled(false);

    if (this->m_player.hasDuration())
    {
        ctrl->back->setEnabled(true);
        ctrl->forw->setEnabled(true);
    }
    else
    {
        ctrl->back->setEnabled(false);
        ctrl->forw->setEnabled(false);
    }

    if (DeviceUtils::determinDevice(this->m_player.getFilePath()))
        ctrl->pause->setEnabled(false);
    else
        ctrl->pause->setEnabled(true);

    ctrl->volume->setEnabled(!this->m_player.isUseSPDIF());
    ctrl->mute->setEnabled(!this->m_player.isUseSPDIF());

    ctrl->pause->setVisible(true);
    ctrl->resume->setVisible(false);

    ctrl->trackBar->setMinimum(0);
    ctrl->trackBar->setMaximum(this->m_player.getDuration());

#ifdef Q_OS_WIN
    if (this->m_player.hasDuration())
    {
        QWinTaskbarProgress *progress = this->m_taskbarButton.progress();

        progress->resume();
    }

    this->m_thumbnailToolStop.setEnabled(true);
#endif

    if (this->m_player.isEnabledVideo())
        this->adjustScreenSizeForVideo();
    else
        this->adjustScreenSizeForAudio();

    ctrl->screen->setVisible(ctrl->screen->getLastVisible());

    if (ctrl->spectrum->isPaused())
        this->resumeSpectrum();
}

void MainWindow::handlePausedStatus()
{
    Ui::MainWindow *ctrl = this->ui;

    ctrl->stop->setEnabled(true);
    ctrl->back->setEnabled(true);
    ctrl->forw->setEnabled(true);
    ctrl->pause->setEnabled(false);
    ctrl->resume->setEnabled(true);

    ctrl->pause->setVisible(false);
    ctrl->resume->setVisible(true);

    this->pauseSpectrum();

#ifdef Q_OS_WIN
    if (this->m_player.hasDuration())
    {
        QWinTaskbarProgress *progress = this->m_taskbarButton.progress();

        progress->show();
        progress->pause();
    }

    this->m_thumbnailToolStop.setEnabled(true);
#endif
}

void MainWindow::handleStartedStatus()
{
    Ui::MainWindow *ctrl = this->ui;

    ctrl->trackBar->setChapters(this->m_player.getChapters());
    ctrl->trackBar->setEnabled(this->m_player.getDuration() > 0.0);

    this->startSpectrum();

    if (this->m_player.isMovieSubtitleVisiable())
    {
        ctrl->audioSubtitle->setVisible(false);
    }
    else
    {
        ctrl->audioSpecWidget->setVisible(true);
        ctrl->audioSubtitle->setVisible(true);

        if (!this->m_player.existAudioSubtitle())
            ctrl->audioSubtitle1->setText(tr("가사가 없습니다"));
    }

    this->setScreenSaverActivity(false);

    const RadioReader &radio = RadioReader::getInstance();

    if (radio.isOpened())
    {
        this->updateRadioMeter();

        ctrl->radioMeter->setVisible(true);
        this->m_radioMeterTimerID = this->startTimer(RADIO_METER_TIME);
    }
    else
    {
        ctrl->radioMeter->setVisible(false);
    }

#ifdef Q_OS_WIN
    if (this->m_player.hasDuration())
    {
        QWinTaskbarProgress *progress = this->m_taskbarButton.progress();

        progress->setMinimum(0);
        progress->setMaximum(this->m_player.getDuration());

        progress->show();
        progress->resume();
    }
#endif
}

void MainWindow::handleStoppedStatus()
{
    Ui::MainWindow *ctrl = this->ui;
    bool resume = this->m_player.isOpened() || this->m_playList->exist();

    ctrl->stop->setEnabled(false);
    ctrl->back->setEnabled(false);
    ctrl->forw->setEnabled(false);
    ctrl->pause->setEnabled(false);
    ctrl->resume->setEnabled(resume);
    ctrl->mute->setEnabled(resume);

    ctrl->pause->setVisible(false);
    ctrl->resume->setVisible(true);

    ctrl->trackBar->setMaximum(0);
    ctrl->trackBar->clearChapters();
    ctrl->trackBar->setEnabled(true);

    this->updateTrackBar();
    this->stopSpectrum();
    this->setScreenSaverActivity(true);

    ctrl->audioSpec->setText(QString());
    ctrl->audioSpecWidget->setVisible(false);
    ctrl->audioBuffering->setText(QString());
    ctrl->audioSubtitle->setVisible(!ctrl->screen->getLastVisible());
    ctrl->audioOptionDesc->setVisible(false);

    this->resetAudioSubtitle();

    ctrl->radioMeter->setVisible(false);

    if (this->m_radioMeterTimerID)
    {
        this->killTimer(this->m_radioMeterTimerID);
        this->m_radioMeterTimerID = 0;
    }

    //Workaround for Mac
    ctrl->defaultBar->repaint();

#ifdef Q_OS_WIN
    QWinTaskbarProgress *progress = this->m_taskbarButton.progress();

    progress->stop();
    progress->hide();

    this->m_thumbnailToolStop.setEnabled(false);
#endif
}

void MainWindow::handleEndedStatus()
{
    Ui::MainWindow *ctrl = this->ui;

    ctrl->stop->setEnabled(false);
    ctrl->back->setEnabled(true);
    ctrl->forw->setEnabled(true);
    ctrl->pause->setEnabled(false);
    ctrl->resume->setEnabled(true);

    ctrl->pause->setVisible(false);
    ctrl->resume->setVisible(true);

#ifdef Q_OS_WIN
    this->m_thumbnailToolStop.setEnabled(false);
#endif

    this->stopSpectrum();

    switch (this->m_player.getPlayingMethod())
    {
        case AnyVODEnums::PM_TOTAL:
        {
            if (this->m_playList->getCurrentPlayingIndex() + 1 < this->m_playList->getCount())
                this->playNext();
            else
                this->close();

            break;
        }
        case AnyVODEnums::PM_TOTAL_REPEAT:
        {
            if (this->m_playList->getCurrentPlayingIndex() + 1 >= this->m_playList->getCount())
                this->m_playList->resetCurrentPlayingIndex();

            this->playNext();

            break;
        }
        case AnyVODEnums::PM_SINGLE:
        {
            this->close();
            break;
        }
        case AnyVODEnums::PM_SINGLE_REPEAT:
        {
            this->playCurrent();
            break;
        }
        case AnyVODEnums::PM_RANDOM:
        {
            this->m_playList->setRandomCurrentPlayingIndex();
            this->playCurrent();

            break;
        }
        default:
        {
            break;
        }
    }
}

void MainWindow::handleStatusChangeEvent(QEvent *event)
{
    Screen::StatusChangeEvent *e = (Screen::StatusChangeEvent*)event;

    switch (e->getStatus())
    {
        case MediaPlayer::Playing:
            this->handlePlayingStatus();
            break;
        case MediaPlayer::Paused:
            this->handlePausedStatus();
            break;
        case MediaPlayer::Started:
            this->handleStartedStatus();
            break;
        case MediaPlayer::Stopped:
            this->handleStoppedStatus();
            break;
        case MediaPlayer::Ended:
            this->handleEndedStatus();
            break;
        default:
            break;
    }

#ifdef Q_OS_WIN
    this->setThumbnailButtonType(this->ui->resume->isEnabled());
#endif
}

void MainWindow::handlePlayingEvent()
{
    Ui::MainWindow *ctrl = this->ui;

    this->updateTrackBar();
    this->updateAudioSpec();

#ifdef Q_OS_WIN
    if (this->m_player.hasDuration())
    {
        QWinTaskbarProgress *progress = this->m_taskbarButton.progress();

        progress->setValue(this->m_player.getCurrentPosition());
    }
#endif
    ctrl->audioBuffering->setText(QString());
}

void MainWindow::handleAudioOptionDescEvent(QEvent *event)
{
    Ui::MainWindow *ctrl = this->ui;
    Screen::ShowAudioOptionDescEvent *e = (Screen::ShowAudioOptionDescEvent*)event;

    ctrl->audioOptionDesc->setVisible(e->getShow());
    ctrl->audioOptionDesc->setText(e->getDesc());
}

void MainWindow::handleEmptyBufferEvent(QEvent *event)
{
    Ui::MainWindow *ctrl = this->ui;
    Screen::EmptyBufferEvent *e = (Screen::EmptyBufferEvent*)event;

    if (e->isEmpty())
        ctrl->audioBuffering->setText(tr("버퍼링 중입니다"));
    else
        ctrl->audioBuffering->setText(QString());
}

void MainWindow::handleAudioSubtitleEvent(QEvent *event)
{
    if (!this->m_player.existExternalSubtitle())
        return;

    Ui::MainWindow *ctrl = this->ui;
    Screen::AudioSubtitleEvent *e = (Screen::AudioSubtitleEvent*)event;
    QVector<Lyrics> lines = e->getLines();
    QVector<Lyrics> mergedLines;
    QVector<Lyrics> *realLine = nullptr;
    QVector<QLabel*> outputs;
    QPalette pal;
    int i;

    pal = ctrl->audioSubtitle1->palette();
    outputs.append(ctrl->audioSubtitle1);
    outputs.append(ctrl->audioSubtitle2);
    outputs.append(ctrl->audioSubtitle3);

    if (lines.count() > 0 && lines[0].text.indexOf(NEW_LINE) >= 0)
    {
        QStringList texts = lines[0].text.split(NEW_LINE);
        Lyrics lyrics;

        for (i = 0; i < texts.count(); i++)
        {
            lyrics.color = lines[0].color;
            lyrics.text = texts[i];

            mergedLines.append(lyrics);
        }

        realLine = &mergedLines;
    }
    else
    {
        realLine = &lines;
    }

    for (i = 0; i < outputs.size() && i < realLine->size(); i++)
    {
        QColor color;

        if (this->m_player.existAudioSubtitleGender())
        {
            color = (*realLine)[i].color;
        }
        else
        {
            if (i == 0)
                color = QColor(0x40, 0x40, 0x40);
            else
                color = Qt::darkGray;
        }

        color.setAlphaF(this->m_player.getSubtitleOpaque());

        pal.setColor(QPalette::WindowText, color);
        this->setPaletteAndText(outputs[i], pal, (*realLine)[i].text.trimmed().remove(QRegularExpression("\\r|\\n")));
    }

    for (; i < outputs.size(); i++)
        outputs[i]->setText(QString());
}

void MainWindow::handleAbortEvent(QEvent *event)
{
    char buf[2048] = {0, };
    Screen::AbortEvent *e = (Screen::AbortEvent*)event;

    av_strerror(e->getReason(), buf, sizeof(buf));
    MessageBoxUtils::criticalMessageBox(this, tr("다음 에러로 중지 되었습니다 : %1").arg(buf));

    this->m_player.close();
}

void MainWindow::handleNonePlayingDescEvent(QEvent *event)
{
    Ui::MainWindow *ctrl = this->ui;
    Screen::NonePlayingDescEvent *e = (Screen::NonePlayingDescEvent*)event;

    ctrl->audioOptionDesc->setVisible(true);
    ctrl->audioOptionDesc->setText(e->getDesc());

    if (this->m_nonePlayingDescTimerID != 0)
        this->killTimer(this->m_nonePlayingDescTimerID);

    this->m_nonePlayingDescTimerID = this->startTimer(MediaPresenter::OPTION_DESC_TIME);
}

void MainWindow::customEvent(QEvent *event)
{
    if (event->type() == Screen::STATUS_CHANGE_EVENT)
        this->handleStatusChangeEvent(event);
    else if (event->type() == Screen::PLAYING_EVENT)
        this->handlePlayingEvent();
    else if (event->type() == Screen::AUDIO_OPTION_DESC_EVENT)
        this->handleAudioOptionDescEvent(event);
    else if (event->type() == Screen::EMPTY_BUFFER_EVENT)
        this->handleEmptyBufferEvent(event);
    else if (event->type() == Screen::AUDIO_SUBTITLE_EVENT)
        this->handleAudioSubtitleEvent(event);
    else if (event->type() == Screen::ABORT_EVENT)
        this->handleAbortEvent(event);
    else if (event->type() == Screen::NONE_PLAYING_DESC_EVENT)
        this->handleNonePlayingDescEvent(event);
    else
        QWidget::customEvent(event);
}

void MainWindow::updateTrackBar()
{
    Ui::MainWindow *ctrl = this->ui;
    double currentPosition = this->m_player.getCurrentPosition();

    if (currentPosition < 0.0)
        return;

    if (!ctrl->trackBar->isSliderDown())
        ctrl->trackBar->setValue(currentPosition);

    QString current;
    QString duration;
    QString format = ConvertingUtils::TIME_HH_MM_SS;

    ctrl->currentTime->setText(*ConvertingUtils::getTimeString(currentPosition, format, &current));
    ctrl->totalTime->setText(*ConvertingUtils::getTimeString(this->m_player.getDuration(), format, &duration));
}

void MainWindow::loadSkin()
{
    const Environment &env = Environment::getInstance();
    QFile skin(env.getSkinFilePath());

    if (skin.open(QFile::ReadOnly))
    {
        const QString scrollbarAreaStart = "#BEGIN_SCROLLBAR_AREA";
        const QString scrollbarAreaEnd = "#END_SCROLLBAR_AREA";
        QTextStream stream(&skin);
        QString contents = stream.readAll();

#if defined Q_OS_MAC
        int start = contents.indexOf(scrollbarAreaStart);

        if (start >= 0)
        {
            int end = contents.indexOf(scrollbarAreaEnd, start);

            if (end >= 0)
                contents.remove(start, end - start + scrollbarAreaEnd.length());
        }
#else
        contents.remove(scrollbarAreaStart);
        contents.remove(scrollbarAreaEnd);
#endif

        qApp->setStyleSheet(contents);
    }
    else
    {
        MessageBoxUtils::criticalMessageBox(this, tr("스킨을 읽을 수 없습니다 (%1)").arg(env.getSkinFilePath()));
    }
}

void MainWindow::initUiComponents()
{
    Ui::MainWindow *ctrl = this->ui;

    ctrl->stop->setEnabled(false);
    ctrl->back->setEnabled(false);
    ctrl->forw->setEnabled(false);
    ctrl->pause->setEnabled(false);
    ctrl->prev->setEnabled(false);
    ctrl->next->setEnabled(false);
    ctrl->mute->setEnabled(false);

    if (this->m_playList->exist())
    {
        this->updateNavigationButtonsInternal();
        ctrl->resume->setEnabled(true);
        ctrl->mute->setEnabled(true);
    }
    else
    {
        ctrl->resume->setEnabled(false);
        ctrl->mute->setEnabled(false);
    }

    ctrl->pause->setVisible(false);

    ctrl->volume->setRange(0, this->m_player.getMaxVolume());
    ctrl->volume->setValue(this->m_player.getVolume());

    ctrl->trackBar->setMinimum(0);
    ctrl->trackBar->setMaximum(0);

    ctrl->audioSpec->setText(QString());
    ctrl->audioSpecWidget->setVisible(false);

    ctrl->audioOptionDesc->setVisible(false);
    ctrl->audioBuffering->setText(QString());

    ctrl->audioSubtitle->setVisible(!ctrl->screen->getLastVisible());
    ctrl->audioSubtitle1->setText(VersionDescription::getAppNameWithVersion());
    ctrl->audioSubtitle2->setText(QString());
    ctrl->audioSubtitle3->setText(QString());

    ctrl->radioMeter->setVisible(false);
    ctrl->radioMeter->setMinimum(RadioReader::MIN_SIGNAL_STRENGTH_LINEAR);
    ctrl->radioMeter->setMaximum(RadioReader::MAX_SIGNAL_STRENGTH_LINEAR);

    connect(ctrl->back, &QPushButton::clicked, &MediaPlayerDelegate::getInstance(), &MediaPlayerDelegate::rewind5);
    connect(ctrl->forw, &QPushButton::clicked, &MediaPlayerDelegate::getInstance(), &MediaPlayerDelegate::forward5);
}

void MainWindow::initPlayer()
{
    this->m_prev360Angle = Camera::getInstance().getAngles();

    this->m_player.setDevicePixelRatio(this->devicePixelRatioF());
    this->m_player.setASSFontFamily(Environment::getInstance().getFontFamily());
    this->m_player.setASSFontPath(Environment::getInstance().getFontFilePath());
}

void MainWindow::updateAudioSpec()
{
    QString title;

    if (this->m_player.isAudio() || !this->m_player.isEnabledVideo())
    {
        QString temp("%L1kbps %2bits %3kHz (%4 %5)");
        const Detail &detail = this->m_player.getDetail();
        QString channel;

        if (detail.audioInputChannels == 1)
            channel = tr("모노");
        else if (detail.audioInputChannels <= 3)
            channel = tr("스테레오");
        else
            channel = tr("서라운드");

        title = temp
                .arg(detail.audioInputByteRate / 1000 * 8)
                .arg(detail.audioInputBits)
                .arg(detail.audioInputSampleRate / 1000.0f, 0, 'f', 1)
                .arg(channel, detail.audioCodecSimple);
    }

    this->ui->audioSpec->setText(title);

    if (title.isEmpty())
        this->ui->audioSpecWidget->setVisible(false);
}

void MainWindow::updatePlayUI()
{
    this->m_player.mute(this->ui->mute->isChecked(), false);
}

void MainWindow::updateWindowTitleInternal()
{
    QString current = QString().setNum(this->m_playList->getCurrentPlayingIndex() + 1);
    QString totalCount = QString().setNum(this->m_playList->getCount());

    if (this->m_playList->exist())
    {
        QString fileName = this->m_playList->getCurrentFileName();
        QString desc = QString("%1 - [%2 / %3] %4").arg(APP_NAME, current, totalCount, fileName);

        this->setWindowTitle(desc);
        this->setFileNameIndicator(fileName);
    }
    else if (this->m_player.isPlayOrPause())
    {
        QString fileName = this->m_player.getFileName();
        QString desc = QString("%1 - %2").arg(APP_NAME, fileName);

        this->setWindowTitle(desc);
        this->setFileNameIndicator(fileName);
    }
    else
    {
        this->resetWindowTitle();
    }
}

void MainWindow::updateNavigationButtonsInternal()
{
    Ui::MainWindow *ctrl = this->ui;

    if (this->m_playList->getCount() <= 1)
    {
        ctrl->prev->setEnabled(false);
        ctrl->next->setEnabled(false);

#ifdef Q_OS_WIN
        this->m_thumbnailToolBackward.setEnabled(false);
        this->m_thumbnailToolForward.setEnabled(false);
#endif
    }
    else if (this->m_playList->getCurrentPlayingIndex() - 1 < 0)
    {
        ctrl->prev->setEnabled(false);
        ctrl->next->setEnabled(true);

#ifdef Q_OS_WIN
        this->m_thumbnailToolBackward.setEnabled(false);
        this->m_thumbnailToolForward.setEnabled(true);
#endif
    }
    else if (this->m_playList->getCurrentPlayingIndex() + 1 >= this->m_playList->getCount())
    {
        ctrl->prev->setEnabled(true);
        ctrl->next->setEnabled(false);

#ifdef Q_OS_WIN
        this->m_thumbnailToolBackward.setEnabled(true);
        this->m_thumbnailToolForward.setEnabled(false);
#endif
    }
    else
    {
        ctrl->prev->setEnabled(true);
        ctrl->next->setEnabled(true);

#ifdef Q_OS_WIN
        this->m_thumbnailToolBackward.setEnabled(true);
        this->m_thumbnailToolForward.setEnabled(true);
#endif
    }
}

void MainWindow::startSpectrum()
{
    if (this->m_spectrumTimerID == 0)
    {
        this->ui->spectrum->init(this->m_player.getAudioHandle());
        this->m_spectrumTimerID = this->startTimer(Spectrum::SPECTRUM_ELAPSE);
    }
}

void MainWindow::stopSpectrum()
{
    if (this->m_spectrumTimerID != 0)
    {
        this->killTimer(this->m_spectrumTimerID);
        this->m_spectrumTimerID = 0;

        this->ui->spectrum->update();
        this->ui->spectrum->deInit();
    }
}

void MainWindow::pauseSpectrum()
{
    this->ui->spectrum->pause();
}

void MainWindow::resumeSpectrum()
{
    this->ui->spectrum->resume();
}

void MainWindow::checkGOMSubtitle()
{
    QString url;

    this->m_player.getGOMSubtitleURL(&url);

    if (url.count() > 0)
        this->m_popup.showText(tr("외부 서버에 자막이 존재합니다.\n이동하시려면 여기를 클릭하세요."), url);
}

bool MainWindow::open(const QString &filePath, ExtraPlayData &data, const QUuid &unique)
{
    this->m_player.close();
    this->m_popup.hide();

    PlayItem item = this->m_playList->getPlayItem(this->m_playList->findIndex(unique));
    QString title = item.title;
    QString audioPath;

    if (!item.urlPickerData.isEmpty())
    {
        for (const URLPickerInterface::Item &yItem : qAsConst(item.urlPickerData))
        {
            if (yItem.url != filePath)
                continue;

            URLPicker picker;

            if (!yItem.dashmpd.isEmpty())
                picker.getAudioStreamByMimeType(yItem.orgUrl, yItem.dashmpd, yItem.mimeType, &audioPath);
            else if (yItem.noAudio)
                picker.getProperAudioStreamByMimeType(yItem.orgUrl, item.urlPickerData, yItem.mimeType, &audioPath);

            break;
        }
    }

    const Environment &env = Environment::getInstance();

    if (this->m_player.open(filePath, title, data, unique, env.getFontFamily(), env.getFontSize(),
                             env.getSubtitleOutlineSize(), audioPath))
    {
        if (this->m_player.play())
        {
            this->updatePlayUI();
            this->checkGOMSubtitle();

            return true;
        }
    }

    return false;
}

void MainWindow::close()
{
    this->resetWindowTitle();
    this->m_playList->resetCurrentPlayingIndex();
    this->m_player.close();
}

void MainWindow::restart()
{
    if (this->m_player.isOpened())
    {
        this->m_player.stop();

        if (!(this->m_player.reOpen() && this->m_player.play()))
        {
            MessageBoxUtils::criticalMessageBox(this, tr("파일을 열 수 없습니다"));
        }
        else
        {
            this->updatePlayUI();
            this->checkGOMSubtitle();
        }
    }
    else if (this->m_playList->exist())
    {
        this->playCurrent();
    }
    else
    {
        this->open();
    }
}

bool MainWindow::isAutoStart() const
{
    return !this->m_playList->exist() || !this->m_player.isPlayOrPause();
}

Spectrum* MainWindow::getSpectrum() const
{
    return this->ui->spectrum;
}

Slider* MainWindow::getOpaqueSlider() const
{
    return this->ui->opaque;
}

bool MainWindow::playAt(int index)
{
    this->m_playList->setCurrentPlayingIndex(index);
    return this->playCurrent();
}

void MainWindow::updateNavigationButtons()
{
    this->updateNavigationButtonsInternal();
}

void MainWindow::resetFirstAudioSubtitle()
{
    this->ui->audioSubtitle1->setText(QString());
}

bool MainWindow::playCurrent()
{
    bool success = false;

    this->m_playList->adjustPlayIndex();
    this->updateNavigationButtons();

    if (this->m_playList->exist())
    {
        PlayItem item = this->m_playList->getCurrentPlayItem();

        if (item.path.startsWith(RemoteFileUtils::ANYVOD_PROTOCOL))
        {
            item.path += SeparatingUtils::FFMPEG_SEPARATOR;
            item.path += QString().setNum((qulonglong)&VirtualFile::getInstance());

            if (!this->isLogined())
            {
                this->logout();
                this->login();

                if (!this->isLogined())
                {
                    this->m_player.close();
                    return false;
                }
            }
        }
        else if (item.path.startsWith(DTVReader::DTV_PROTOCOL))
        {
            item.path += SeparatingUtils::FFMPEG_SEPARATOR;
            item.path += QString().setNum((qulonglong)&DTVReader::getInstance());
        }
        else if (item.path.startsWith(RadioReader::RADIO_PROTOCOL))
        {
            item.path += SeparatingUtils::FFMPEG_SEPARATOR;
            item.path += QString().setNum((qulonglong)&RadioReader::getInstance());
        }

        QVector<URLPickerInterface::Item> urls;
        URLPicker picker;
        QString path;
        int urlPikcerIndex = 0;

        urls = picker.pickURL(item.path);

        if (urls.isEmpty())
        {
            path = item.path;
        }
        else
        {
            urlPikcerIndex = this->m_playList->updateURLPickerData(item.unique, urls);

            path = urls[urlPikcerIndex].url;
            item.extraData.query = urls[urlPikcerIndex].query;
        }

        success = this->open(path, item.extraData, item.unique);

        if (!success && !urls.isEmpty() && ++urlPikcerIndex < urls.length())
        {
            if (this->m_playList->selectOtherQualityByCurrentItem(urlPikcerIndex))
                success = this->playCurrent();
        }

        if (success)
        {
            this->m_playList->setCurrentPlayingIndexByUnique(this->m_player.getUnique());
            this->updateNavigationButtons();
        }
        else
        {
            MessageBoxUtils::criticalMessageBox(this, tr("파일을 열 수 없습니다"));

            if (MessageBoxUtils::questionMessageBox(this, tr("재생 목록에서 삭제 하시겠습니까?")))
            {
                int oldIndex = this->m_playList->getCurrentPlayingIndex();

                this->m_playList->deleteItem(oldIndex);
                this->close();
                this->m_playList->adjustPlayIndex();
                this->setFocus();
            }
        }

        this->updateWindowTitle();
        this->m_playList->selectItem(this->m_playList->getCurrentPlayingIndex());
    }
    else
    {
        this->resetWindowTitle();
    }

    return success;
}

bool MainWindow::playPrev()
{
    this->m_playList->decreaseCurrentPlayingIndex();
    return this->playCurrent();
}


bool MainWindow::playNext()
{
    this->m_playList->increaseCurrentPlayingIndex();
    return this->playCurrent();
}

bool MainWindow::addToPlayList(const QVector<PlayItem> &list, bool enableAuto, bool forceStart)
{
    if (list.count() <= 0)
        return false;

    bool autoStart = (this->isAutoStart() && enableAuto) || forceStart;
    int curIndex = this->m_playList->getCount();
    bool added = this->m_playList->addPlayList(list);

    if (autoStart && added)
    {
        this->m_playList->setCurrentPlayingIndex(curIndex);
        this->playCurrent();
    }
    else
    {
        if (this->m_player.isOpened())
            this->updateWindowTitle();
    }

    return autoStart;
}

void MainWindow::setPlayList(const QVector<PlayItem> &list)
{
    Ui::MainWindow *ctrl = this->ui;

    this->m_playList->setPlayList(list);
    this->playNext();

    ctrl->prev->setEnabled(false);

    if (list.count() == 1)
        ctrl->next->setEnabled(false);
}

void MainWindow::processMouseUp()
{
    this->m_buttonPressed = false;
}

void MainWindow::processMouseDown()
{
    this->m_buttonPressed = true;
    this->m_prevPos = QCursor::pos();
}

void MainWindow::processMouseMoving()
{
    QPoint mousePos = QCursor::pos();

    if (this->m_buttonPressed)
    {
        QPoint currentPos = this->pos();
        QPoint delta = mousePos - this->m_prevPos;

        if (delta.manhattanLength() >= QApplication::startDragDistance())
        {
            if (this->is360DegreeInteraction())
            {
                QPointF angles = this->m_prev360Angle;

                delta /= 5.0f;

                angles.rx() += delta.x();
                angles.ry() -= delta.y();

                Camera::getInstance().setAngles(angles);

                this->m_prevPos = mousePos;
                this->m_prev360Angle = angles;
            }
            else if (!this->isFullScreen())
            {
                this->move(currentPos + delta);
                this->m_prevPos = mousePos;
            }
        }
    }

    if (this->isFullScreen())
    {
        if (!(ShaderCompositer::getInstance().is360Degree() && this->m_buttonPressed))
        {
            QWidget *ctlBar = this->ui->controlBar;
            int ctlHeight = ctlBar->height();
            int screenHeight = this->height();
            int mouseYPosInScreen = this->mapFromGlobal(mousePos).y();
            int mousePosHeight = screenHeight - mouseYPosInScreen;

            if (mousePosHeight <= ctlHeight && !ctlBar->isVisible())
                ctlBar->setVisible(true);
            else if (mousePosHeight > ctlHeight && ctlBar->isVisible())
                ctlBar->setVisible(false);
        }
    }
}

void MainWindow::volume(int maxVolume, int dir)
{
    if (this->m_player.isUseSPDIF())
        return;

    Ui::MainWindow *ctrl = this->ui;
    int volume = maxVolume * dir;

    volume = qCeil(volume * 0.05);
    volume += ctrl->volume->value();

    ctrl->volume->setValue(volume);

    if (volume < 0)
        this->m_player.volume(0);
    else if (volume > maxVolume)
        this->m_player.volume(maxVolume);

    if (ctrl->mute->isChecked())
        ctrl->mute->setChecked(false);
}

void MainWindow::opaque(int dir)
{
    QSlider *ctrl = this->ui->opaque;
    int maxValue = ctrl->maximum();
    int opaque = maxValue * dir;

    opaque = qCeil(opaque * 0.05);
    opaque += ctrl->value();

    this->changeOpaque(opaque);
}

void MainWindow::changeOpaque(int value)
{
    this->ui->opaque->setValue(value);
}

void MainWindow::setFullScreenMode(bool enable)
{
    this->ui->centralWidget->layout()->setContentsMargins(0, enable ? Screen::SCREEN_TOP_OFFSET : 0, 0, 0);
    this->ui->screen->setFullScreenMode(enable);

    this->m_isFullScreening = enable;
}

void MainWindow::startMedia(const QString &path)
{
    QFileInfo info(path);

    if (FileExtensions::SUBTITLE_EXTS_LIST.contains(info.suffix().toLower()))
    {
        this->m_player.openSubtitle(SeparatingUtils::adjustNetworkPath(info.filePath()), true);
    }
    else
    {
        QVector<PlayItem> items = PlayItemUtils::getPlayItemFromFileNames(QStringList(path));

        this->addToPlayList(items, true, true);
    }
}

void MainWindow::setScreenSizeRatio(double ratio)
{
    int width;
    int height;

    if (this->m_player.getFrameSize(&width, &height))
    {
        QSize size(width, height);

        size *= ratio;

        if (this->isFullScreen())
            this->fullScreen();

        if (this->ui->controlBar->isVisible())
            size.rheight() += this->ui->controlBar->height();

        this->resize(size);
        this->m_player.showOptionDesc(tr("화면 크기 %1배").arg(ratio));
    }
}

void MainWindow::setPaletteAndText(QLabel *item, const QPalette &palette, const QString &text)
{
    item->setPalette(palette);
    item->setText(text);
}

void MainWindow::setLanguage(const QString &lang)
{
    LanguageInstaller::getInstance().install(lang);

    ShortcutKey &shortcutKey = ShortcutKey::getInstance();

    shortcutKey.saveToSetting();
    shortcutKey.reloadKey();
    shortcutKey.loadFromSetting();

    this->m_enums.reload();

#ifdef Q_OS_WIN
    this->translateThumbnailText();
#endif

    GlobalLanguage::getInstance().setLanguage(lang);
}

void MainWindow::setLastSelectedDirectory(const QString &path)
{
    this->m_lastSelectedDir = path;
}

QString MainWindow::getLastSelectedDirectory() const
{
    return this->m_lastSelectedDir;
}

void MainWindow::setRemoteServerInformation(const RemoteServerInformation &info)
{
    this->m_remoteServerInfo = info;
}

RemoteServerInformation MainWindow::getRemoteServerInformation() const
{
    return this->m_remoteServerInfo;
}

void MainWindow::processDragEnterEvent(QDragEnterEvent *event)
{
    const QMimeData *mime = event->mimeData();

    if (mime->hasUrls())
    {
        const QList<QUrl> urls = mime->urls();
        QStringList filePathList;
        bool ignoreExts = IS_BIT_SET(event->keyboardModifiers(), Qt::AltModifier | Qt::ControlModifier);

        if (urls.count() == 1)
        {
            const QUrl &url = urls[0];

            if (RemoteFileUtils::determinRemoteProtocol(url.toString()))
            {
                event->accept();
                return;
            }
            else
            {
                QFileInfo info(url.toLocalFile());

                if (FileExtensions::SUBTITLE_EXTS_LIST.contains(info.suffix().toLower()))
                {
                    event->accept();
                    return;
                }
            }
        }

        for (int i = 0; i < urls.count(); i++)
            filePathList.append(FileListUtils::getOnlyMediaLocalFileList(urls[i].toLocalFile(), FileExtensions::MEDIA_EXTS_LIST, ignoreExts));

        if (!filePathList.isEmpty())
            event->accept();
    }
}

void MainWindow::processDropEvent(QDropEvent *event)
{
    const QMimeData *mime = event->mimeData();

    if (!mime->hasUrls())
        return;

    const QList<QUrl> urls = mime->urls();

    if (urls.count() == 1)
    {
        const QUrl &url = urls[0];

        if (RemoteFileUtils::determinRemoteProtocol(url.toString()))
        {
            QVector<PlayItem> itemVector;
            QString address = url.toString();

            this->getURLPickerPlayItems(address, &itemVector);

            if (IS_BIT_SET(event->keyboardModifiers(), Qt::ControlModifier))
                this->addToPlayList(itemVector, true, false);
            else
                this->setPlayList(itemVector);

            this->activateWindow();
        }
        else
        {
            QFileInfo info(url.toLocalFile());

            if (FileExtensions::SUBTITLE_EXTS_LIST.contains(info.suffix().toLower()))
            {
                this->m_player.openSubtitle(SeparatingUtils::adjustNetworkPath(info.filePath()), true);
                return;
            }
        }
    }

    QStringList filePathList;
    bool ignoreExts = IS_BIT_SET(event->keyboardModifiers(), Qt::AltModifier | Qt::ControlModifier);

    for (int i = 0; i < urls.count(); i++)
        filePathList.append(FileListUtils::getOnlyMediaLocalFileList(urls[i].toLocalFile(), FileExtensions::MEDIA_EXTS_LIST, ignoreExts));

    if (!filePathList.empty())
    {
        QVector<PlayItem> itemVector = PlayItemUtils::getPlayItemFromFileNames(filePathList);

        if (IS_BIT_SET(event->keyboardModifiers(), Qt::ControlModifier))
            this->addToPlayList(itemVector, true, false);
        else
            this->setPlayList(itemVector);

        this->activateWindow();
    }
}

void MainWindow::setScreenSaverActivity(bool enable)
{
    if (enable)
        this->m_screenSaverPreventer.enable();
    else
        this->m_screenSaverPreventer.disable();
}

void MainWindow::setEnglishIMEInternal() const
{
#ifdef Q_OS_WIN
    ImmAssociateContext((HWND)this->winId(), nullptr);
#endif
}

void MainWindow::on_pause_clicked()
{
    this->m_player.pause();
}

void MainWindow::on_stop_clicked()
{
    this->stop();
}

void MainWindow::on_resume_clicked()
{
    if (!this->m_player.isPlayOrPause())
        this->restart();
    else
        this->m_player.resume();
}

void MainWindow::on_volume_valueChanged(int value)
{
    this->m_player.volume(value);
}

void MainWindow::on_trackBar_sliderMoved(int position)
{
    if (!this->m_player.isRemoteFile())
        this->m_player.seek(position, !this->ui->trackBar->isDragging());
}

void MainWindow::on_trackBar_sliderReleased()
{
    if (this->m_player.isRemoteFile())
    {
        this->setSeekingText();
        this->m_player.seek(this->ui->trackBar->value(), true);
    }
}

void MainWindow::on_opaque_valueChanged(int value)
{
    qreal opaque = value / (qreal)this->ui->opaque->maximum();

    this->setWindowOpacity(opaque);
    this->m_remoteFileList.setWindowOpacity(opaque);
    this->m_playList->setWindowOpacity(opaque);

    QString desc = tr("투명도 (%1%)");

    this->m_player.showOptionDesc(desc.arg((int)(ceil(opaque * 100))));
}

void MainWindow::on_mute_toggled(bool checked)
{
    this->m_player.mute(checked, true);
}

void MainWindow::login()
{
    if (!this->isLogined())
    {
        Login login(this->m_remoteServerInfo, this);
        int ret = login.exec();

        if (ret != QDialog::Accepted)
            return;

        QString error;
        QList<ANYVOD_FILE_ITEM> fileList;

        this->m_loginTimerID = this->startTimer(LOGIN_TIME);

        if (Socket::getInstance().requestFilelist(RemoteFileList::ROOT_DIR, &error, &fileList))
        {
            this->m_remoteFileList.setFileList(fileList, RemoteFileList::ROOT_DIR);
            this->m_remoteFileList.show();
        }
        else
        {
            MessageBoxUtils::criticalMessageBox(this, error);
        }
    }
}

void MainWindow::logout()
{
    QString error;

    Socket::getInstance().logout(&error);
    this->m_remoteFileList.clearFileList();

    if (this->m_player.isRemoteFile())
        this->close();

    Socket::getStreamInstance().logout(&error);
}

void MainWindow::mostTop()
{
    bool fullscreen = this->isFullScreen();
    unsigned int flags = this->windowFlags();
    unsigned int topFlag = Qt::WindowStaysOnTopHint;

    if (IS_BIT_SET(flags, topFlag))
        flags &= ~topFlag;
    else
        flags |= topFlag;

    if (fullscreen)
        this->fullScreen();

#ifdef Q_OS_WIN
    HWND topMost;

    topMost = IS_BIT_SET(flags, Qt::WindowStaysOnTopHint) ? HWND_TOPMOST : HWND_NOTOPMOST;

    SetWindowPos((HWND)this->winId(), topMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
#endif

    this->setWindowFlags((Qt::WindowFlags)flags);

    if (this->m_isReady)
    {
        this->show();
        this->activateWindow();
    }

    if (fullscreen)
        this->fullScreen();

    QString desc;

    if (this->isMostTop())
        desc = tr("항상 위 켜짐");
    else
        desc = tr("항상 위 꺼짐");

    this->m_player.showOptionDesc(desc);
}

void MainWindow::open()
{
    QString filter = tr("지원하는 모든 파일 (*.%1);;동영상 (*.%2);;음악 (*.%3);;자막 / 가사 (*.%4);;재생 목록 (*.%5);;모든 파일 (*.*)").arg(
                FileExtensions::ALL_EXTS_LIST.join(" *."),
                FileExtensions::MOVIE_EXTS_LIST.join(" *."),
                FileExtensions::AUDIO_EXTS_LIST.join(" *."),
                FileExtensions::SUBTITLE_EXTS_LIST.join(" *."),
                FileExtensions::PLAYLIST_EXTS_LIST.join(" *."));
    QString file = QFileDialog::getOpenFileName(this, QString(), this->m_lastSelectedDir, filter, nullptr, QFileDialog::HideNameFilterDetails);

    if (!file.isEmpty())
    {
        this->startMedia(file);
        this->m_lastSelectedDir = QFileInfo(file).absolutePath();
    }
}

void MainWindow::getURLPickerPlayItems(const QString &address, QVector<PlayItem> *itemVector)
{
    QVector<URLPickerInterface::Item> urls;
    URLPicker picker;

    urls = picker.pickURL(address);

    if (urls.isEmpty())
    {
        QStringList list;

        list.append(address);
        *itemVector = PlayItemUtils::getPlayItemFromFileNames(list);
    }
    else
    {
        bool isPlayList = picker.isPlayList();

        if (isPlayList)
        {
            QVector<URLPickerInterface::Item> playlist;

            if (urls.length() > 0)
                playlist = picker.pickURL(urls.first().url);

            urls = playlist.mid(0, 1) + urls.mid(1);
        }

        *itemVector = PlayItemUtils::getPlayItemFromURLPicker(urls, isPlayList);
    }
}

void MainWindow::updateRadioMeter()
{
    Ui::MainWindow *ctrl = this->ui;
    const RadioReader &reader = RadioReader::getInstance();
    float signal = reader.getSignalStrength();

    ctrl->radioMeter->setValue(reader.getSignalStrengthInLinear(signal));
    ctrl->radioMeter->setToolTip(QString("%1dBFS").arg(signal));
}

void MainWindow::openExternal()
{
    OpenExternal dlg(this);
    int ret = dlg.exec();

    if (ret != QDialog::Accepted)
        return;

    QVector<PlayItem> itemVector;
    QString address;

    dlg.getAddress(&address);

    this->getURLPickerPlayItems(address, &itemVector);
    this->addToPlayList(itemVector, true, true);
}

void MainWindow::openRemoteFileList()
{
    if (this->isLogined())
        this->m_remoteFileList.setVisible(!this->m_remoteFileList.isVisible());
}

void MainWindow::openPlayList()
{
    this->m_playList->setVisible(!this->m_playList->isVisible());
}

void MainWindow::openEqualizerSetting()
{
    Equalizer dlg(this);

    dlg.exec();
}

void MainWindow::toggle()
{
    if (!this->m_player.isPlayOrPause())
    {
        this->restart();
    }
    else
    {
        if (!DeviceUtils::determinDevice(this->m_player.getFilePath()))
            this->m_player.toggle();
    }
}

void MainWindow::stop()
{
    this->m_player.stop();
}

void MainWindow::fullScreen()
{
    Ui::MainWindow *ctrl = this->ui;

    if ((this->m_player.isEnabledVideo() || !this->m_player.isPlayOrPause()) &&
            ctrl->screen->size().height() > 0 && ctrl->screen->isVisible())
    {
        if (this->isFullScreen())
        {
            this->m_remoteFileList.setVisible(this->m_prevRemoteFileListVisible);
            this->m_playList->setVisible(this->m_prevPlayListVisible);
            ctrl->controlBar->show();

            this->setFullScreenMode(false);

            if (this->m_prevMaximized)
                this->showMaximized();
            else
                this->showNormal();

            this->m_player.showOptionDesc(tr("일반 화면"));
        }
        else
        {
            ctrl->controlBar->hide();
            this->m_prevMaximized = this->isMaximized();
            this->m_prevRemoteFileListVisible = this->m_remoteFileList.isVisible();
            this->m_prevPlayListVisible = this->m_playList->isVisible();
            this->m_remoteFileList.hide();
            this->m_playList->hide();

            this->setFullScreenMode(true);
            this->showFullScreen();

            this->m_player.showOptionDesc(tr("전체 화면"));
        }

        this->activateWindow();
    }
}

void MainWindow::prev()
{
    if (this->isPrevEnabled())
        this->playPrev();
}

void MainWindow::next()
{
    if (this->isNextEnabled())
        this->playNext();
}

void MainWindow::volumeUp()
{
    this->volume(this->m_player.getMaxVolume(), 1);
}

void MainWindow::volumeDown()
{
    this->volume(this->m_player.getMaxVolume(), -1);
}

void MainWindow::mute()
{
    if (!this->m_player.isUseSPDIF())
        this->ui->mute->setChecked(!this->ui->mute->isChecked());
}

void MainWindow::info()
{
    MessageBoxUtils::informationMessageBox(this, VersionDescription::get());
}

void MainWindow::showControlBar()
{
    Ui::MainWindow *ctrl = this->ui;

    if (this->canShowControlBar())
    {
        ctrl->controlBar->setVisible(!ctrl->controlBar->isVisible());

        QString desc;

        if (ctrl->controlBar->isVisible())
            desc = tr("컨트롤바 보임");
        else
            desc = tr("컨트롤바 숨김");

        this->m_player.showOptionDesc(desc);
    }
}

void MainWindow::incOpaque()
{
    this->opaque(1);
}

void MainWindow::decOpaque()
{
    this->opaque(-1);
}

void MainWindow::maxOpaque()
{
    this->changeOpaque(this->ui->opaque->maximum());
}

void MainWindow::minOpaque()
{
    this->changeOpaque(this->ui->opaque->minimum());
}

void MainWindow::openSkipRange()
{
    QVector<MediaPresenter::Range> ranges;
    SkipRange dlg(&ranges, this);

    if (dlg.exec() != QDialog::Accepted)
        return;

    this->m_player.setSkipRanges(ranges);
}

void MainWindow::openShaderCompositer()
{
    if (this->m_player.isEnabledVideo())
    {
        Shader dlg(this);

        dlg.exec();
    }
}

void MainWindow::addToPlayList()
{
    QString filter = tr("지원하는 모든 파일 (*.%1);;동영상 (*.%2);;음악 (*.%3);;재생 목록 (*.%4);;모든 파일 (*.*)").arg(
                FileExtensions::ALL_EXTS_LIST.join(" *."),
                FileExtensions::MOVIE_EXTS_LIST.join(" *."),
                FileExtensions::AUDIO_EXTS_LIST.join(" *."),
                FileExtensions::PLAYLIST_EXTS_LIST.join(" *."));
    QStringList files = QFileDialog::getOpenFileNames(this, tr("재생 목록에 추가"), this->m_lastSelectedDir, filter, nullptr, QFileDialog::HideNameFilterDetails);

    if (!files.isEmpty())
    {
        QVector<PlayItem> itemVector = PlayItemUtils::getPlayItemFromFileNames(files);

        this->addToPlayList(itemVector, false, false);
        this->m_lastSelectedDir = QFileInfo(files[0]).absolutePath();
    }
}

void MainWindow::exit()
{
    QWidget::close();
}
