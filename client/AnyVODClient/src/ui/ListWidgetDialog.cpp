﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ListWidgetDialog.h"

#include <QListWidget>

ListWidgetDialog::ListWidgetDialog(QWidget *parent, Qt::WindowFlags flags) :
    QDialog(parent, flags)
{

}

QList<QListWidgetItem*> ListWidgetDialog::sortByRow(const QList<QListWidgetItem*> &list, const QListWidget &widget)
{
    QMap<int, QListWidgetItem*> sorting;

    for (int i = 0; i < list.count(); i++)
    {
        QListWidgetItem *item = list[i];

        sorting.insert(widget.row(item), item);
    }

    return sorting.values();
}
