﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "MultipleCapture.h"
#include "ui_multiplecapture.h"
#include "Screen.h"
#include "core/FileExtensions.h"
#include "media/MediaPlayer.h"
#include "utils/MessageBoxUtils.h"
#include "utils/TimeUtils.h"
#include "video/ScreenCaptureHelper.h"

#include <QRect>
#include <QSize>
#include <QFileDialog>
#include <QDir>

const int MultipleCapture::CAPTURE_CHECK_TIME = 40;

MultipleCapture::MultipleCapture(Screen *screen, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MultipleCapture),
    m_screen(screen),
    m_player(MediaPlayer::getInstance()),
    m_captureWidthPrio(true)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    const auto btns = this->findChildren<QPushButton*>();

    for (QPushButton *btn : btns)
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);

    const auto groups = this->findChildren<QGroupBox*>();

    for (QGroupBox *group : groups)
    {
        QFont font = group->font();

        font.setPointSizeF(13);
        group->setFont(font);
    }
#endif

    Ui::MultipleCapture *ctrl = this->ui;
    ScreenCaptureHelper &capHelper = this->m_screen->getCaptureHelper();
    QStringList exts = FileExtensions::CAPTURE_FORMAT_LIST;
    int index = 0;

    for (int i = 0; i < exts.count(); i++)
    {
        exts[i] = exts.at(i).toUpper();

        if (exts[i].toUpper() == capHelper.getExtention().toUpper())
            index = i;
    }

    ctrl->savePath->setText(QDir::toNativeSeparators(capHelper.getSavePath()));
    ctrl->exts->addItems(exts);

    ctrl->exts->setCurrentIndex(index);

    if (capHelper.isDefaultQuality())
        ctrl->defaultQuality->setChecked(true);
    else
        ctrl->quality->setValue(capHelper.getQuality());

    QRect picRect;
    QSize picSize;

    this->m_player.getPictureRect(&picRect);

    picSize = picRect.size();
    picSize.setHeight(picSize.height() - 1);
    picSize.setWidth(picSize.width() - 1);

    if (picSize.width() < 1)
        picSize.setWidth(1);

    if (picSize.height() < 1)
        picSize.setHeight(1);

    ctrl->captureWidth->setValue(picSize.width());
    ctrl->captureHeight->setValue(picSize.height());

    double curTime = this->m_player.getCurrentPosition();

    curTime += 60.0;

    ctrl->targetTime->setCurrentSection(QTimeEdit::SecondSection);
    ctrl->targetTime->setTime(TimeUtils::getTime(curTime).time());

    ctrl->captureSubtitle->setChecked(this->m_player.isShowSubtitle());
    ctrl->captureDetail->setChecked(this->m_player.isShowDetail());
}

MultipleCapture::~MultipleCapture()
{
    delete ui;
}

void MultipleCapture::on_changePath_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, QString(), this->ui->savePath->text());

    if (!dir.isEmpty())
        this->ui->savePath->setText(dir);
}

void MultipleCapture::on_adjustResolution_clicked()
{
    Ui::MultipleCapture *ctrl = this->ui;
    double ratio = this->m_player.getAspectRatio(this->m_captureWidthPrio);
    int height;
    int width;

    if (this->m_captureWidthPrio)
    {
        width = ctrl->captureWidth->value();
        height = (int)rint(width * ratio);
    }
    else
    {
        height = ctrl->captureHeight->value();
        width = (int)rint(height * ratio);
    }

    ctrl->captureWidth->setValue(width);
    ctrl->captureHeight->setValue(height);
}

void MultipleCapture::on_startCapture_clicked()
{
    ScreenCaptureHelper &capHelper = this->m_screen->getCaptureHelper();
    Ui::MultipleCapture *ctrl = this->ui;
    int totalCount = this->getTotalCount();

    if (totalCount <= 0)
    {
        MessageBoxUtils::criticalMessageBox(this, tr("캡쳐를 할 수 없습니다. 지정 시간 또는 지정 개수를 확인 해 주세요."));

        ctrl->startCapture->setEnabled(true);
        ctrl->stopCapture->setEnabled(false);

        return;
    }

    if (ctrl->sizeGroup->checkedButton() == ctrl->captureOrg)
    {
        capHelper.setCaptureByOriginalSize(true);
    }
    else if (ctrl->sizeGroup->checkedButton() == ctrl->captureCurrent)
    {
        QRect rect;
        QSize size;

        this->m_player.getPictureRect(&rect);

        size = rect.size();

        size.setWidth(size.width() - 1);
        size.setHeight(size.height() - 1);

        capHelper.setSize(size);
        capHelper.setCaptureByOriginalSize(false);
    }
    else if (ctrl->sizeGroup->checkedButton() == ctrl->captureUser)
    {
        QSize size(ctrl->captureWidth->value(), ctrl->captureHeight->value());

        capHelper.setSize(size);
        capHelper.setCaptureByOriginalSize(false);
    }
    else
    {
        capHelper.setCaptureByOriginalSize(true);
    }

    capHelper.setTotalCount(totalCount);
    capHelper.setExtension(ctrl->exts->currentText().toLower());

    if (ctrl->defaultQuality->isChecked())
        capHelper.resetToDefaultQuality();
    else
        capHelper.setQuality(ctrl->quality->value());

    capHelper.setSavePath(ctrl->savePath->text());

    this->m_player.showSubtitle(ctrl->captureSubtitle->isChecked());
    this->m_player.showDetail(ctrl->captureDetail->isChecked());

    capHelper.start();
    this->startTimer(CAPTURE_CHECK_TIME);
}

void MultipleCapture::on_stopCapture_clicked()
{
    ScreenCaptureHelper &capHelper = this->m_screen->getCaptureHelper();

    capHelper.stop();

    if (capHelper.isPaused())
        this->m_player.pause(true);
}

void MultipleCapture::on_captureWidth_editingFinished()
{
    this->m_captureWidthPrio = true;
}

void MultipleCapture::on_captureHeight_editingFinished()
{
    this->m_captureWidthPrio = false;
}

void MultipleCapture::timerEvent(QTimerEvent *event)
{
    ScreenCaptureHelper &capHelper = this->m_screen->getCaptureHelper();

    this->ui->currentStat->setText(QString::number(capHelper.getCapturedCount()));
    this->ui->totalStat->setText(QString::number(capHelper.getTotalCount()));

    if (!capHelper.isRemained())
    {
        this->killTimer(event->timerId());
        this->ui->stopCapture->click();
    }
}

void MultipleCapture::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

int MultipleCapture::getTotalCount() const
{
    Ui::MultipleCapture *ctrl = this->ui;

    if (ctrl->frameGroup->checkedButton() == ctrl->captureTime)
    {
        double targetTime = -ctrl->targetTime->time().msecsTo(TimeUtils::ZERO_TIME) / 1000.0;
        double time = targetTime - this->m_player.getCurrentPosition();

        if (time < 0.0)
            return 0;

        const Detail &detail = this->m_player.getDetail();
        double frameRatio = detail.videoTotalFrame / detail.totalTime;

        return (int)time * frameRatio;
    }
    else
    {
        return ctrl->targetCount->value();
    }
}
