﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "device/RadioReader.h"

#include <QDialog>

namespace Ui
{
    class OpenRadioScanChannel;
}

class OpenRadioScanChannel : public QDialog
{
    Q_OBJECT

public:
    explicit OpenRadioScanChannel(RadioReader &reader, QWidget *parent = nullptr);
    ~OpenRadioScanChannel();

private:
    void initHeader();
    void addChannel(const RadioReader::ChannelInfo &info);
    bool existScannedChannel(int channel);

private:
    virtual void changeEvent(QEvent *event);

private slots:
    void updateChannelRange();

private slots:
    void on_save_clicked();
    void on_scan_clicked();
    void on_stopScan_clicked();
    void on_OpenRadioScanChannel_finished(int result);

private:
    Ui::OpenRadioScanChannel *ui;
    RadioReader &m_reader;
    QVector<RadioReader::ChannelInfo> m_channels;

private:
    volatile bool m_stop;
};
