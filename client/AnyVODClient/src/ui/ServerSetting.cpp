﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ServerSetting.h"
#include "ui_serversetting.h"

ServerSetting::ServerSetting(const RemoteServerInformation &info, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ServerSetting),
    m_info(info)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    const auto btns = this->findChildren<QPushButton*>();

    for (QPushButton *btn : btns)
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    ui->address->setText(this->m_info.address);
    ui->commandPort->setValue(this->m_info.commandPort);
    ui->streamPort->setValue(this->m_info.streamPort);
}

ServerSetting::~ServerSetting()
{
    delete ui;
}

RemoteServerInformation ServerSetting::getInformation() const
{
    return this->m_info;
}

void ServerSetting::on_ok_clicked()
{
    this->m_info = RemoteServerInformation(ui->address->text(), ui->commandPort->value(), ui->streamPort->value());
    this->accept();
}

void ServerSetting::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}
