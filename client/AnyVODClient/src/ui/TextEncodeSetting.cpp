﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "TextEncodeSetting.h"
#include "ui_textencodesetting.h"
#include "parsers/subtitle/GlobalSubtitleCodecName.h"

#include <algorithm>
#include <QTextCodec>
#include <QList>

using namespace std;

const QString TextEncodeSetting::DEFAULT_NAME = "Default";

TextEncodeSetting::TextEncodeSetting(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TextEncodeSetting)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    const auto btns = this->findChildren<QPushButton*>();

    for (QPushButton *btn : btns)
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);

    const auto groups = this->findChildren<QGroupBox*>();

    for (QGroupBox *group : groups)
    {
        QFont font = group->font();

        font.setPointSizeF(13);
        group->setFont(font);
    }
#endif

    QList<int> mibList = QTextCodec::availableMibs();
    QStringList codecList;
    QListWidget *list = this->ui->encoding;

    for (const int mib : qAsConst(mibList))
        codecList.append(QTextCodec::codecForMib(mib)->name());

    stable_sort(codecList.begin(), codecList.end());
    codecList.removeDuplicates();

    list->addItem(DEFAULT_NAME);

    for (const QString &codec : codecList)
        list->addItem(codec);

    QList<QListWidgetItem*> currentItem = list->findItems(GlobalSubtitleCodecName::getInstance().getCodecName(), Qt::MatchExactly);

    if (currentItem.size() > 0)
        list->setCurrentItem(currentItem.first());
    else
        list->setCurrentRow(0);
}

TextEncodeSetting::~TextEncodeSetting()
{
    delete ui;
}

void TextEncodeSetting::getEncoding(QString *ret) const
{
    QListWidgetItem *item = this->ui->encoding->currentItem();

    *ret = item->text();
}

void TextEncodeSetting::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}
