﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "core/AnyVODEnums.h"

#include <QCoreApplication>
#include <QString>

class MainWindowInterface;
class PlayList;
class ScreenDelegate;
class UpdaterDelegate;
class MediaPlayerDelegate;
class ShaderCompositerDelegate;
class QShortcut;

class ShortcutKey : public QObject
{
    Q_OBJECT
private:
    ShortcutKey();
    ~ShortcutKey();

public:
    static ShortcutKey& getInstance();

public:
    void reloadKey();
    QShortcut* getKey(AnyVODEnums::ShortcutKey key) const;

    QString getShortcutKeyDesc(AnyVODEnums::ShortcutKey key) const;
    QKeySequence getShortcutKey(AnyVODEnums::ShortcutKey key) const;
    bool isShortcutKeyEnabled(AnyVODEnums::ShortcutKey key) const;
    void setShortcutKey(AnyVODEnums::ShortcutKey key, QKeySequence seq);

public slots:
    void loadFromSetting();
    void saveToSetting();

private:
    void setupKey();
    void setupMainWindowKey();
    void setupScreenKey();
    void setupUpdaterKey();
    void setupMediaPlayerKey();
    void setupShaderKey();
    void setupPlayListKey();

    void deleteKey();

    template <typename T1, typename T2>
    void createShortcutItem(AnyVODEnums::ShortcutKey keyName, const QString &desc, int keySequence, T1 receiver, T2 slot);

    void createMainWindowShortcutItem(AnyVODEnums::ShortcutKey keyName, const QString &desc, int keySequence, void (MainWindowInterface::*slot)());
    void createScreenShortcutItem(AnyVODEnums::ShortcutKey keyName, const QString &desc, int keySequence, void (ScreenDelegate::*slot)());
    void createUpdaterShortcutItem(AnyVODEnums::ShortcutKey keyName, const QString &desc, int keySequence, void (UpdaterDelegate::*slot)());
    void createMediaPlayerShortcutItem(AnyVODEnums::ShortcutKey keyName, const QString &desc, int keySequence, void (MediaPlayerDelegate::*slot)());
    void createShaderShortcutItem(AnyVODEnums::ShortcutKey keyName, const QString &desc, int keySequence, void (ShaderCompositerDelegate::*slot)());
    void createPlayListShortcutItem(AnyVODEnums::ShortcutKey keyName, const QString &desc, int keySequence, void (PlayList::*slot)());

private:
    struct ShortcutItem
    {
        ShortcutItem()
        {
            shortcut = nullptr;
        }

        ShortcutItem(const QString &d, QShortcut *s)
        {
            desc = d;
            shortcut = s;
        }

        QString desc;
        QShortcut *shortcut;
    };

private:
    MainWindowInterface *m_mainWindow;
    ScreenDelegate &m_screenDelegate;
    UpdaterDelegate &m_updaterDelegate;
    MediaPlayerDelegate &m_playerDelegate;
    ShaderCompositerDelegate &m_shaderDelegate;
    ShortcutItem m_shortcut[AnyVODEnums::SK_COUNT];
};
