﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "EPGItem.h"
#include "ui_epgitem.h"

#include <QPushButton>

EPGItem::EPGItem(const DTVReaderInterface::EPG &epg, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EPGItem),
    m_epg(epg)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    const auto btns = this->findChildren<QPushButton*>();

    for (QPushButton *btn : btns)
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    QDateTime curDate = QDateTime::currentDateTime();
    QDateTime startDate = epg.start.toLocalTime();
    QDateTime endDate = startDate.addSecs(QTime(0, 0).secsTo(epg.duration));

    this->ui->date->setText(startDate.toString("[yyyy-MM-dd]"));
    this->ui->title->setText(epg.title);
    this->ui->start->setText(startDate.toString("hh:mm:ss"));
    this->ui->end->setText(endDate.toString("hh:mm:ss"));

    if (startDate <= curDate && endDate >= curDate)
    {
        this->ui->progressBar->setMinimum(0);
        this->ui->progressBar->setMaximum(QTime(0, 0).secsTo(epg.duration));
        this->ui->progressBar->setValue(startDate.secsTo(curDate));
    }
    else if (curDate < startDate)
    {
        this->ui->progressBar->setMinimum(0);
        this->ui->progressBar->setMaximum(1);
        this->ui->progressBar->setValue(0);
    }
    else
    {
        this->ui->progressBar->setMinimum(0);
        this->ui->progressBar->setMaximum(1);
        this->ui->progressBar->setValue(1);
    }
}

EPGItem::~EPGItem()
{
    delete ui;
}

void EPGItem::getEPG(DTVReaderInterface::EPG *ret) const
{
    *ret = this->m_epg;
}

void EPGItem::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}
