﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ShortcutEdit.h"

#include <QKeyEvent>
#include <QCoreApplication>
#include <QDebug>

const QEvent::Type ShortcutEdit::COMPLETE_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type ShortcutEdit::CLEAR_EVENT = (QEvent::Type)QEvent::registerEventType();

ShortcutEdit::ShortcutEdit(QWidget *parent) :
    QLineEdit(parent),
    m_validKey(false)
{

}

void ShortcutEdit::keyPressEvent(QKeyEvent *event)
{
    QString seq;

    if (event->modifiers() & Qt::ShiftModifier)
        seq += "Shift+";

    if (event->modifiers() & Qt::ControlModifier)
        seq += "Ctrl+";

    if (event->modifiers() & Qt::AltModifier)
        seq += "Alt+";

    if (event->modifiers() & Qt::MetaModifier)
        seq += "Meta+";

    if (this->isValidKey(event->key()))
    {
        seq += QKeySequence(event->key()).toString().toUpper();
        this->m_validKey = true;
    }
    else
    {
        this->m_validKey = false;
    }

    this->setText(seq);
    event->accept();
}

void ShortcutEdit::keyReleaseEvent(QKeyEvent *event)
{
    if (!this->m_validKey)
    {
        this->setText(QString());
        QCoreApplication::postEvent(this->window(), new ClearEvent(this));
    }
    else if (this->isValidKey(event->key()) && !event->isAutoRepeat())
    {
        QCoreApplication::postEvent(this->window(), new CompleteEvent(QKeySequence::fromString(this->text()), this));
    }

    event->accept();
}

bool ShortcutEdit::event(QEvent *e)
{
    if (e->type() == QEvent::KeyPress)
    {
        this->keyPressEvent((QKeyEvent*)e);

        return true;
    }
    else if (e->type() == QEvent::KeyRelease)
    {
        this->keyReleaseEvent((QKeyEvent*)e);

        return true;
    }

    return QLineEdit::event(e);
}

bool ShortcutEdit::isValidKey(int key) const
{
    switch (key)
    {
        case Qt::Key_Shift:
        case Qt::Key_Control:
        case Qt::Key_Alt:
        case Qt::Key_Meta:
        case Qt::Key_unknown:
        {
            return false;
        }
    }

    return true;
}
