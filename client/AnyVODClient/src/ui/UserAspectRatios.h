﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QVector>

class UserAspectRatios
{
public:
    struct Item
    {
        Item()
        {
            width = 1.0;
            height = 1.0;
        }

        Item(double _width, double _height)
        {
            width = _width;
            height = _height;
        }

        bool isInvalid() const
        {
            return width == 0.0 || height == 0.0;
        }

        bool isFullScreen() const
        {
            return width == -1.0 || height == -1.0;
        }

        double width;
        double height;
    };

public:
    UserAspectRatios();

    int getCount() const;
    Item getRatio(int index) const;

    void setUserRatio(const Item &item);
    Item getUserRatio() const;
    int getUserRatioIndex() const;

    bool isUserRatio(int index) const;

    void selectNextRatio();
    bool isSelectedRatio(int index) const;
    void selectRatio(int index);
    int getSelectedRatioIndex() const;

private:
    int m_curIndex;
    QVector<Item> m_list;
};
