﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "UserAspectRatios.h"

UserAspectRatios::UserAspectRatios() :
    m_curIndex(0)
{
    this->m_list.append(Item(0.0, 0.0));
    this->m_list.append(Item(-1.0, -1.0));
    this->m_list.append(Item(4.0, 3.0));
    this->m_list.append(Item(16.0, 9.0));
    this->m_list.append(Item(16.0, 10.0));
    this->m_list.append(Item(1.85, 1.0));
    this->m_list.append(Item(2.35, 1.0));
    this->m_list.append(Item(4.0, 3.0));
}

int UserAspectRatios::getCount() const
{
    return this->m_list.count();
}

UserAspectRatios::Item UserAspectRatios::getRatio(int index) const
{
    if (index < 0 || index >= this->getCount())
        return this->m_list.first();
    else
        return this->m_list[index];
}

void UserAspectRatios::setUserRatio(const UserAspectRatios::Item &item)
{
    this->m_list[this->getUserRatioIndex()] = item;
}

UserAspectRatios::Item UserAspectRatios::getUserRatio() const
{
    return this->m_list[this->getUserRatioIndex()];
}

bool UserAspectRatios::isUserRatio(int index) const
{
    return index == this->getUserRatioIndex();
}

void UserAspectRatios::selectNextRatio()
{
    int index = this->m_curIndex + 1;

    if (index >= this->getCount())
        index = 0;

    this->m_curIndex = index;
}

bool UserAspectRatios::isSelectedRatio(int index) const
{
    return this->m_curIndex == index;
}

void UserAspectRatios::selectRatio(int index)
{
    if (index < 0 || index >= this->getCount())
        return;

    this->m_curIndex = index;
}

int UserAspectRatios::getSelectedRatioIndex() const
{
    return this->m_curIndex;
}

int UserAspectRatios::getUserRatioIndex() const
{
    return this->m_list.size() - 1;
}
