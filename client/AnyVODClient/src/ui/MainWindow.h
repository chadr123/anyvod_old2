﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "MainWindowInterface.h"
#include "RemoteFileList.h"
#include "UserAspectRatios.h"
#include "Popup.h"
#include "core/RemoteServerInformation.h"
#include "screensaver/ScreenSaverPreventer.h"
#include "../../../../common/network.h"

#ifdef Q_OS_WIN
# include <QWinTaskbarButton>
# include <QWinThumbnailToolBar>
# include <QWinThumbnailToolButton>
#endif

#ifdef Q_OS_MAC
# include "../device/AppleMediaKeyWrapper.h"
#endif

#include <QSharedMemory>

struct ExtraPlayData;

class MediaPlayer;
class ShaderCompositer;
class EnumsTranslator;
class QLabel;

namespace Ui
{
    class MainWindow;
}

class MainWindow : public MainWindowInterface
{
    Q_OBJECT
public:
    friend class MainWindowFactory;

public:
    explicit MainWindow(const QStringList starting, QWidget *parent = nullptr);
    ~MainWindow();

    void resetWindowTitle();
    void setPlayList(const QVector<PlayItem> &list);

    bool open(const QString &filePath, ExtraPlayData &data, const QUuid &unique);

    void setFileNameIndicator(const QString &fileName);

    bool isReady() const;
    bool isFullScreening() const;

public:
    virtual bool playAt(int index);

    virtual void adjustSPDIFAudioUI();
    virtual void checkGOMSubtitle();

    virtual void resetAudioSubtitle();
    virtual void resetFirstAudioSubtitle();

    virtual void updateTrackBar();
    virtual void setSeekingText();

    virtual bool addToPlayList(const QVector<PlayItem> &list, bool enableAuto, bool forceStart);
    virtual void movePopup();

    virtual void updateWindowTitle();
    virtual void updateNavigationButtons();

    virtual UserAspectRatios& getUserAspectRatios();
    virtual QUuid getUniqueInPlayer() const;

    virtual PlayList& getPlayList();
    virtual QWidget& getControlBar();
    virtual RemoteFileList& getRemoteFileList();
    virtual Spectrum* getSpectrum() const;
    virtual Slider* getOpaqueSlider() const;

    virtual bool isPrevEnabled() const;
    virtual bool isNextEnabled() const;
    virtual bool isMostTop() const;
    virtual bool isMute() const;
    virtual bool isLogined() const;
    virtual bool canShowControlBar() const;

    virtual void setLanguage(const QString &lang);

    virtual void setLastSelectedDirectory(const QString &path);
    virtual QString getLastSelectedDirectory() const;

    virtual void setRemoteServerInformation(const RemoteServerInformation &info);
    virtual RemoteServerInformation getRemoteServerInformation() const;

    virtual void processDragEnterEvent(QDragEnterEvent *event);
    virtual void processDropEvent(QDropEvent *event);

protected:
    virtual void moveEvent(QMoveEvent *);
    virtual void customEvent(QEvent *event);
    virtual void contextMenuEvent(QContextMenuEvent *);
    virtual void mouseDoubleClickEvent(QMouseEvent *);
    virtual void mouseReleaseEvent(QMouseEvent *);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *);
    virtual void wheelEvent(QWheelEvent *event);
    virtual void dragEnterEvent(QDragEnterEvent *event);
    virtual void dropEvent(QDropEvent *event);
    virtual void timerEvent(QTimerEvent *event);
    virtual void resizeEvent(QResizeEvent *);
    virtual void closeEvent(QCloseEvent *);
    virtual void changeEvent(QEvent *event);
    virtual bool event(QEvent *event);

private:
    static MainWindow* getInstance();

private:
    static QString OBJECT_NAME;

private:
    static const int SCREEN_MINIMUM_SIZE;
    static const int LOGIN_TIME;
    static const int SHARE_MEM_TIME;
    static const int INIT_COMPLETE_TIME;
    static const int RADIO_METER_TIME;

private:
    void loadSkin();
    void initUiComponents();
    void initPlayer();

    void restart();
    bool playNext();
    bool playPrev();
    bool playCurrent();

    bool isAutoStart() const;

    void processMouseUp();
    void processMouseDown();
    void processMouseMoving();

    void moveChildWindows();

    void updateAudioSpec();
    void updatePlayUI();
    void updateWindowTitleInternal();
    void updateNavigationButtonsInternal();

    void startSpectrum();
    void stopSpectrum();
    void pauseSpectrum();
    void resumeSpectrum();

    void volume(int maxVolume, int dir);
    void opaque(int dir);
    void changeOpaque(int value);

    void startMedia(const QString &path);

    void setFullScreenMode(bool enable);
    void setScreenSizeRatio(double ratio);
    void setScreenSaverActivity(bool enable);
    void setEnglishIMEInternal() const;

    bool is360DegreeInteraction() const;

    void handleSpectrumTimerEvent();
    void handleNonePlayingDescTimerEvent(QTimerEvent *event);
    void handleLoginTimerEvent(QTimerEvent *event);
    void handleShareMemTimerEvent();
    void handleInitCompleteTimerEvent();
    void handleRadioMeterTimerEvent();

    void checkSoftwareUpdate();

    void startByFirstAppInstance();
    void startByAnotherAppInstance();

    void handleStatusChangeEvent(QEvent *event);
    void handlePlayingEvent();
    void handleAudioOptionDescEvent(QEvent *event);
    void handleEmptyBufferEvent(QEvent *event);
    void handleAudioSubtitleEvent(QEvent *event);
    void handleAbortEvent(QEvent *event);
    void handleNonePlayingDescEvent(QEvent *event);

    void handlePlayingStatus();
    void handlePausedStatus();
    void handleStartedStatus();
    void handleStoppedStatus();
    void handleEndedStatus();

    void adjustScreenSizeForVideo();
    void adjustScreenSizeForAudio();

    void setPaletteAndText(QLabel *item, const QPalette &palette, const QString &text);
    void getURLPickerPlayItems(const QString &address, QVector<PlayItem> *itemVector);

    void updateRadioMeter();

#ifdef Q_OS_WIN
    void initThumbnailBar();
    void setThumbnailButtonType(bool resume);
    void translateThumbnailText();
#endif

private:
    static void audioOptionDescCallback(void *userdata, QKeyEvent *event);

private slots:
    void on_pause_clicked();
    void on_stop_clicked();
    void on_resume_clicked();
    void on_volume_valueChanged(int value);
    void on_trackBar_sliderMoved(int position);
    void on_trackBar_sliderReleased();
    void on_mute_toggled(bool checked);
    void on_opaque_valueChanged(int value);

public slots:
    virtual void login();
    virtual void logout();
    virtual void mostTop();
    virtual void openRemoteFileList();
    virtual void openExternal();
    virtual void openEqualizerSetting();
    virtual void openPlayList();
    virtual void openSkipRange();
    virtual void openShaderCompositer();
    virtual void openFileAssociation();
    virtual void openCustomShortcut();
    virtual void openUserAspectRatio();
    virtual void openServerSetting();
    virtual void openScreenExplorer();
    virtual void openTextEncoding();
    virtual void openSubtitleDirectory();
    virtual void openImportFonts();
    virtual void openDevice();
    virtual void openScanDTVChannel();
    virtual void openScanRadioChannel();
    virtual void openAudioCD();
    virtual void viewEPG();
    virtual void open();
    virtual void close();
    virtual void toggle();
    virtual void stop();
    virtual void exit();
    virtual void fullScreen();
    virtual void prev();
    virtual void next();
    virtual void volumeUp();
    virtual void volumeDown();
    virtual void mute();
    virtual void info();
    virtual void showControlBar();
    virtual void incOpaque();
    virtual void decOpaque();
    virtual void maxOpaque();
    virtual void minOpaque();
    virtual void addToPlayList();
    virtual void addDTVChannelToPlaylist();
    virtual void addRadioChannelToPlaylist();
    virtual void setEnglishIME() const;
    virtual void userAspectRatioOrder();
    virtual void selectUserAspectRatio(int index);
    virtual void screenSizeHalf();
    virtual void screenSizeNormal();
    virtual void screenSizeNormalHalf();
    virtual void screenSizeDouble();
    virtual void selectLanguage(const QString &lang, const QString &desc);
    virtual void saveSettingsToFile();
    virtual void loadSettingsToFile();

private:
    Ui::MainWindow *ui;
    bool m_buttonPressed;
    bool m_prevMaximized;
    bool m_prevRemoteFileListVisible;
    bool m_prevPlayListVisible;
    bool m_isReady;
    bool m_isFullScreening;
    int m_spectrumTimerID;
    int m_nonePlayingDescTimerID;
    int m_loginTimerID;
    int m_shareMemTimerID;
    int m_initCompleteTimerID;
    int m_radioMeterTimerID;
    MediaPlayer &m_player;
    RemoteFileList m_remoteFileList;
    PlayList *m_playList;
    Popup m_popup;
    QSharedMemory m_shareMem;
    RemoteServerInformation m_remoteServerInfo;
    ScreenSaverPreventer m_screenSaverPreventer;
    UserAspectRatios m_userAspectRatios;
    EnumsTranslator &m_enums;
    QStringList m_startingPaths;
    QString m_lastSelectedDir;
    QPointF m_prev360Angle;
    QPoint m_prevPos;

#ifdef Q_OS_WIN
    QWinTaskbarButton m_taskbarButton;
    QWinThumbnailToolBar m_thumbnailToolBar;
    QWinThumbnailToolButton m_thumbnailToolResumePause;
    QWinThumbnailToolButton m_thumbnailToolStop;
    QWinThumbnailToolButton m_thumbnailToolBackward;
    QWinThumbnailToolButton m_thumbnailToolForward;
#endif

#ifdef Q_OS_MAC
    AppleMediaKeyWrapper m_appleMediaKey;
#endif
};
