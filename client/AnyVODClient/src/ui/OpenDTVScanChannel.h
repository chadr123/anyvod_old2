﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "device/DTVReader.h"

#include <QDialog>

namespace Ui
{
    class OpenDTVScanChannel;
}

class OpenDTVScanChannel : public QDialog
{
    Q_OBJECT

public:
    explicit OpenDTVScanChannel(DTVReader &reader, QWidget *parent = nullptr);
    ~OpenDTVScanChannel();

private:
    void initHeader();
    void addChannel(const DTVReader::ChannelInfo &info);
    bool existScannedChannel(int channel);

private:
    virtual void changeEvent(QEvent *event);

private slots:
    void updateChannelRange();

private slots:
    void on_save_clicked();
    void on_scan_clicked();
    void on_stopScan_clicked();
    void on_OpenDTVScanChannel_finished(int result);

private:
    Ui::OpenDTVScanChannel *ui;
    DTVReader &m_reader;
    QVector<DTVReader::ChannelInfo> m_channels;

private:
    volatile bool m_stop;
};
