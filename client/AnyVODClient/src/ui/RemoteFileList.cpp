﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "RemoteFileList.h"
#include "ui_remotefilelist.h"
#include "MainWindowInterface.h"
#include "Screen.h"
#include "core/PlayItem.h"
#include "core/FileIcons.h"
#include "utils/SeparatingUtils.h"
#include "utils/MessageBoxUtils.h"
#include "utils/ConvertingUtils.h"
#include "utils/RemoteFileUtils.h"
#include "net/Socket.h"
#include "media/MediaPlayer.h"
#include "factories/MainWindowFactory.h"

#include <QPoint>
#include <QFileInfo>
#include <QMenu>
#include <QScrollBar>

const QSize RemoteFileList::DEFAULT_REMOTEFILELIST_SIZE = QSize(400, 300);
const QString RemoteFileList::GOTO_PARENT_DIR = "..";
const QString RemoteFileList::ROOT_DIR = "/";

RemoteFileList::RemoteFileList(QWidget *parent) :
    QDialog(parent, Qt::Tool),
    ui(new Ui::RemoteFileList),
    m_isInit(false),
    m_addShort(QKeySequence(Qt::Key_A | Qt::AltModifier), this, SLOT(addPlayList()), nullptr)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    const auto btns = this->findChildren<QPushButton*>();

    for (QPushButton *btn : btns)
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    this->initHeader();
}

RemoteFileList::~RemoteFileList()
{
    delete ui;
}

void RemoteFileList::initHeader()
{
    QTreeWidget *filelist = this->ui->filelist;
    QStringList headers;

    headers.append(tr("이름"));
    headers.append(tr("확장자"));
    headers.append(tr("재생 시간"));
    headers.append(tr("전송 속도"));

    filelist->setColumnCount(headers.count());
    filelist->setHeaderLabels(headers);
    filelist->header()->setSectionsMovable(false);
}

void RemoteFileList::adjustFileListHeaderSize()
{
    QTreeWidget *filelist = this->ui->filelist;

    filelist->setColumnWidth(0, this->width() - 250);
    filelist->setColumnWidth(1, 70);
    filelist->setColumnWidth(2, 70);
    filelist->setColumnWidth(3, 70);
}

void RemoteFileList::showEvent(QShowEvent *)
{
   this->adjustFileListHeaderSize();
}

void RemoteFileList::resizeEvent(QResizeEvent *)
{
    if(!this->m_isInit)
    {
        QWidget *parent = this->parentWidget();
        QPoint pos(parent->x(), parent->y() + parent->frameGeometry().height());
        int width = parent->size().width();

        if (width < DEFAULT_REMOTEFILELIST_SIZE.width())
            width = DEFAULT_REMOTEFILELIST_SIZE.width();

        this->move(pos);
        this->resize(QSize(width, this->size().height()));

        this->m_isInit = true;
    }

    this->adjustFileListHeaderSize();
}

int RemoteFileList::setFileListSub(QList<ANYVOD_FILE_ITEM> &list, FILE_ITEM_TYPE type)
{
    QList<QTreeWidgetItem*> items;
    int count = 0;

    for (int i = 0; i < list.count(); i++)
    {
        ANYVOD_FILE_ITEM &item = list[i];

        if (item.type == type && item.permission)
        {
            QStringList row;
            QString fileName = QString::fromStdWString(item.fileName);
            QIcon icon;

            if (item.type == FT_FILE)
            {
                QString time;
                QFileInfo info = fileName;
                QString ext = info.suffix();

                row.append(info.completeBaseName());
                row.append(ext);
                row.append(*ConvertingUtils::getTimeString(item.totalTime, ConvertingUtils::TIME_HH_MM_SS, &time));
                row.append(QString().setNum(item.bitRate / 1024 / 8) + " KB");

                icon = FileIcons::getInstance().getExtentionIcon(ext);

                count++;
            }
            else
            {
                row.append(fileName);
                row.append(tr("<DIR>"));
                row.append(QString());
                row.append(QString());

                icon = FileIcons::getInstance().getDirectoryIcon();

                count++;
            }

            QTreeWidgetItem *witem = new QTreeWidgetItem((QTreeWidgetItem*)nullptr, row);

            witem->setIcon(0, icon);

            if (fileName != GOTO_PARENT_DIR)
                witem->setToolTip(0, fileName);

            witem->setTextAlignment(3, Qt::AlignRight | Qt::AlignVCenter);
            witem->setData(0, Qt::UserRole, item.type);
            witem->setData(1, Qt::UserRole, QString::fromStdWString(item.title));
            witem->setData(2, Qt::UserRole, item.totalTime);
            witem->setData(3, Qt::UserRole, item.totalFrame);

            items.append(witem);
        }
    }

    this->ui->filelist->insertTopLevelItems(0, items);

    return count;
}

void RemoteFileList::setFileList(QList<ANYVOD_FILE_ITEM> &list, const QString &currentPath)
{
    this->disbleUpdates();
    this->clearFileList();

    int fileCount = this->setFileListSub(list, FT_FILE);
    int dirCount = this->setFileListSub(list, FT_DIR);

    if (currentPath.trimmed() != ROOT_DIR)
    {
        QList<ANYVOD_FILE_ITEM> tmpList;
        ANYVOD_FILE_ITEM item;

        item.type = FT_DIR;
        item.fileName = GOTO_PARENT_DIR.toStdWString();
        item.permission = true;

        tmpList.append(item);

        this->setFileListSub(tmpList, FT_DIR);
    }

    this->enableUpdates();

    this->ui->folders->setText(QString::number(dirCount));
    this->ui->files->setText(QString::number(fileCount));

    this->ui->filelist->scrollToTop();
    this->m_currentPath = currentPath;
}

void RemoteFileList::clearFileList()
{
    this->ui->filelist->clear();
}

bool RemoteFileList::isRemotePlaying()
{
    MediaPlayer &player = MediaPlayer::getInstance();

    return player.isPlayOrPause() && player.isRemoteFile();
}

bool RemoteFileList::containDirectory(const QList<QTreeWidgetItem*> &list)
{
    for (int i = 0; i < list.count(); i++)
    {
        QTreeWidgetItem *item = list[i];
        QString itemText = item->text(0);

        if (item->data(0, Qt::UserRole) == FT_DIR && itemText != GOTO_PARENT_DIR)
            return true;
    }

    return false;
}

void RemoteFileList::disbleUpdates()
{
    QTreeWidget *filelist = this->ui->filelist;

    filelist->setUpdatesEnabled(false);
}

void RemoteFileList::enableUpdates()
{
    QTreeWidget *filelist = this->ui->filelist;

    filelist->setUpdatesEnabled(true);
}

void RemoteFileList::loadFileList(const QString &path)
{
    QString error;
    QList<ANYVOD_FILE_ITEM> list;

    if (Socket::getInstance().requestFilelist(path, &error, &list))
        this->setFileList(list, path);
    else
        MessageBoxUtils::criticalMessageBox(this, error);
}

void RemoteFileList::contextMenuEvent(QContextMenuEvent *)
{
    QMenu menu;
    QAction *action = nullptr;

    action = menu.addAction(tr("재생 목록에 추가"));
    action->setShortcut(this->m_addShort.key());
    action->setShortcutVisibleInContextMenu(true);
    action->setEnabled(this->ui->filelist->selectedItems().count() > 0);
    connect(action, &QAction::triggered, this, &RemoteFileList::addPlayList);

    Screen *screen = Screen::getInstance();

    screen->setDisableHideCusor(true);
    menu.exec(QCursor::pos());
    screen->setDisableHideCusor(false);
}

void RemoteFileList::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            this->initHeader();

            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void RemoteFileList::on_filelist_itemActivated(QTreeWidgetItem *item, int)
{
    QString fileName = item->text(0);

    if (item->data(0, Qt::UserRole) == FT_DIR)
    {
        QString path;
        Scroll vScroll;
        QScrollBar *scroll = this->ui->filelist->verticalScrollBar();

        if (fileName == GOTO_PARENT_DIR)
        {
            int index = this->m_currentPath.lastIndexOf(ROOT_DIR);

            path = this->m_currentPath.mid(0, index);

            if (path.isEmpty())
                path = ROOT_DIR;

            if (this->m_dirScroll.isEmpty())
            {
                vScroll.max = 0;
                vScroll.value = 0;
            }
            else
            {
                vScroll = this->m_dirScroll.pop();
            }
        }
        else
        {
            path = this->m_currentPath;
            SeparatingUtils::appendDirSeparator(&path);
            path += fileName;

            vScroll.max = scroll->maximum();
            vScroll.value = scroll->value();
            vScroll.name = item->text(0);

            this->m_dirScroll.push(vScroll);

            vScroll.max = 0;
            vScroll.value = 0;
            vScroll.name.clear();
        }

        this->loadFileList(path);

        if (vScroll.max != 0 && vScroll.value != 0)
        {
            scroll->setMaximum(vScroll.max);
            scroll->setValue(vScroll.value);
        }

        if (!vScroll.name.isEmpty())
        {
            QList<QTreeWidgetItem *> foundItem = this->ui->filelist->findItems(vScroll.name, Qt::MatchExactly);

            if (foundItem.count() > 0)
                this->ui->filelist->setCurrentItem(foundItem[0]);
        }
    }
    else
    {
        MainWindowInterface *window = MainWindowFactory::getInstance();
        QVector<PlayItem> list;
        PlayItem playItem;
        QString title = item->data(1, Qt::UserRole).toString();
        uint32_t totalTime = item->data(2, Qt::UserRole).toUInt();
        uint32_t totalFrame = item->data(3, Qt::UserRole).toUInt();

        this->getAnyVODUrl(this->m_currentPath, fileName, item->text(1), &playItem.path);

        playItem.extraData.duration = totalTime;
        playItem.extraData.totalFrame = totalFrame;
        playItem.extraData.valid = true;
        playItem.title = title;
        playItem.totalTime = totalTime;
        playItem.itemUpdated = true;

        list.append(playItem);

        window->addToPlayList(list, false, true);
        window->activateWindow();
    }
}

void RemoteFileList::getAnyVODUrl(const QString &parent, const QString &fileName, const QString &ext, QString *ret)
{
    *ret = RemoteFileUtils::ANYVOD_PROTOCOL;
    *ret += parent;
    SeparatingUtils::appendDirSeparator(ret);
    *ret += fileName + "." + ext;
}

void RemoteFileList::getFilePaths(const QString &parent, QVector<PlayItem> *ret)
{
    QList<ANYVOD_FILE_ITEM> list;

    if (Socket::getInstance().requestFilelist(parent, nullptr, &list))
    {
        for (int i = 0; i < list.count(); i++)
        {
            ANYVOD_FILE_ITEM &item = list[i];

            if (item.type == FT_DIR)
            {
                QString dir = parent;

                SeparatingUtils::appendDirSeparator(&dir);
                dir += QString::fromStdWString(item.fileName);

                this->getFilePaths(dir, ret);
            }
            else
            {
                PlayItem playItem;
                QFileInfo info(QString::fromStdWString(item.fileName));

                this->getAnyVODUrl(parent, info.completeBaseName(), info.suffix(), &playItem.path);

                playItem.extraData.duration = item.totalTime;
                playItem.extraData.totalFrame = item.totalFrame;
                playItem.extraData.valid = true;
                playItem.title = QString::fromStdWString(item.title);
                playItem.totalTime = item.totalTime;
                playItem.itemUpdated = true;

                ret->append(playItem);
            }
        }
    }
}

void RemoteFileList::addPlayList()
{
    MainWindowInterface *window = MainWindowFactory::getInstance();
    QList<QTreeWidgetItem*> selectedItems = this->ui->filelist->selectedItems();
    QVector<PlayItem> list;

    for (int i = 0; i < selectedItems.count(); i++)
    {
        QTreeWidgetItem *item = selectedItems[i];
        QString itemText = item->text(0);

        if (item->data(0, Qt::UserRole) == FT_DIR)
        {
            if (itemText != GOTO_PARENT_DIR)
            {
                QString path = this->m_currentPath;

                SeparatingUtils::appendDirSeparator(&path);
                path += itemText;

                this->getFilePaths(path, &list);
            }
        }
        else
        {
            PlayItem playItem;
            QString title = item->data(1, Qt::UserRole).toString();
            uint32_t totalTime = item->data(2, Qt::UserRole).toUInt();
            uint32_t totalFrame = item->data(3, Qt::UserRole).toUInt();

            this->getAnyVODUrl(this->m_currentPath, itemText, item->text(1), &playItem.path);

            playItem.extraData.duration = totalTime;
            playItem.extraData.totalFrame = totalFrame;
            playItem.extraData.valid = true;
            playItem.title = title;
            playItem.totalTime = totalTime;
            playItem.itemUpdated = true;

            list.append(playItem);
        }
    }

    if (window->addToPlayList(list, true, false))
        window->activateWindow();
}

void RemoteFileList::on_refresh_clicked()
{
    QScrollBar *scrollbar = this->ui->filelist->verticalScrollBar();
    int scroll = scrollbar->value();

    this->loadFileList(this->m_currentPath);
    scrollbar->setValue(scroll);
}
