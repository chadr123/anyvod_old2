﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QMainWindow>

struct RemoteServerInformation;
struct PlayItem;

class UserAspectRatios;
class PlayList;
class RemoteFileList;
class Spectrum;
class Slider;

class MainWindowInterface : public QMainWindow
{
	Q_OBJECT
public:
    MainWindowInterface(QWidget *parent);

    virtual bool playAt(int index) = 0;

    virtual void adjustSPDIFAudioUI() = 0;
    virtual void checkGOMSubtitle() = 0;

    virtual void resetAudioSubtitle() = 0;
    virtual void resetFirstAudioSubtitle() = 0;

    virtual void updateTrackBar() = 0;
    virtual void setSeekingText() = 0;

    virtual bool addToPlayList(const QVector<PlayItem> &list, bool enableAuto, bool forceStart) = 0;
    virtual void movePopup() = 0;

    virtual void updateWindowTitle() = 0;
    virtual void updateNavigationButtons() = 0;

    virtual UserAspectRatios& getUserAspectRatios() = 0;
    virtual QUuid getUniqueInPlayer() const = 0;

    virtual PlayList& getPlayList() = 0;
    virtual QWidget& getControlBar() = 0;
    virtual RemoteFileList& getRemoteFileList() = 0;
    virtual Spectrum* getSpectrum() const = 0;
    virtual Slider* getOpaqueSlider() const = 0;

    virtual bool isPrevEnabled() const = 0;
    virtual bool isNextEnabled() const = 0;
    virtual bool isMostTop() const = 0;
    virtual bool isMute() const = 0;
    virtual bool isLogined() const = 0;
    virtual bool canShowControlBar() const = 0;

    virtual void setLanguage(const QString &lang) = 0;

    virtual void setLastSelectedDirectory(const QString &path) = 0;
    virtual QString getLastSelectedDirectory() const = 0;

    virtual void setRemoteServerInformation(const RemoteServerInformation &info) = 0;
    virtual RemoteServerInformation getRemoteServerInformation() const = 0;

    virtual void processDragEnterEvent(QDragEnterEvent *event) = 0;
    virtual void processDropEvent(QDropEvent *event) = 0;

public slots:
    virtual void login() = 0;
    virtual void logout() = 0;
    virtual void mostTop() = 0;
    virtual void openRemoteFileList() = 0;
    virtual void openExternal() = 0;
    virtual void openEqualizerSetting() = 0;
    virtual void openPlayList() = 0;
    virtual void openSkipRange() = 0;
    virtual void openShaderCompositer() = 0;
    virtual void openFileAssociation() = 0;
    virtual void openCustomShortcut() = 0;
    virtual void openUserAspectRatio() = 0;
    virtual void openServerSetting() = 0;
    virtual void openScreenExplorer() = 0;
    virtual void openTextEncoding() = 0;
    virtual void openSubtitleDirectory() = 0;
    virtual void openImportFonts() = 0;
    virtual void openDevice() = 0;
    virtual void openScanDTVChannel() = 0;
    virtual void openScanRadioChannel() = 0;
    virtual void openAudioCD() = 0;
    virtual void viewEPG() = 0;
    virtual void open() = 0;
    virtual void close() = 0;
    virtual void toggle() = 0;
    virtual void stop() = 0;
    virtual void exit() = 0;
    virtual void fullScreen() = 0;
    virtual void prev() = 0;
    virtual void next() = 0;
    virtual void volumeUp() = 0;
    virtual void volumeDown() = 0;
    virtual void mute() = 0;
    virtual void info() = 0;
    virtual void showControlBar() = 0;
    virtual void incOpaque() = 0;
    virtual void decOpaque() = 0;
    virtual void maxOpaque() = 0;
    virtual void minOpaque() = 0;
    virtual void addToPlayList() = 0;
    virtual void addDTVChannelToPlaylist() = 0;
    virtual void addRadioChannelToPlaylist() = 0;
    virtual void setEnglishIME() const = 0;
    virtual void userAspectRatioOrder() = 0;
    virtual void selectUserAspectRatio(int index) = 0;
    virtual void screenSizeHalf() = 0;
    virtual void screenSizeNormal() = 0;
    virtual void screenSizeNormalHalf() = 0;
    virtual void screenSizeDouble() = 0;
    virtual void selectLanguage(const QString &lang, const QString &desc) = 0;
    virtual void saveSettingsToFile() = 0;
    virtual void loadSettingsToFile() = 0;

public:
    static const QPoint DEFAULT_POSITION;
    static const QSize DEFAULT_SIZE;

    static const int CONTROL_BAR_HEIGHT;
};
