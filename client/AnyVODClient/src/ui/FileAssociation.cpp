﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "FileAssociation.h"
#include "ui_fileassociation.h"
#include "core/FileExtensions.h"
#include "core/FileAssociationInstaller.h"
#include "setting/Settings.h"
#include "setting/SettingsFactory.h"
#include "utils/MessageBoxUtils.h"

#include <QSettings>

FileAssociation::FileAssociation(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FileAssociation),
    m_settings(SettingsFactory::getInstance(SettingsFactory::ST_GLOBAL))
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    for (QPushButton *btn : this->findChildren<QPushButton*>())
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    QString video = this->m_settings.value(SETTING_FILE_ASSOCIATION_VIDEO, FileExtensions::MOVIE_EXTS).toString();
    QString audio = this->m_settings.value(SETTING_FILE_ASSOCIATION_AUDIO, FileExtensions::AUDIO_EXTS).toString();
    QString subtitle = this->m_settings.value(SETTING_FILE_ASSOCIATION_SUBTITLE, FileExtensions::SUBTITLE_EXTS).toString();
    QString playlist = this->m_settings.value(SETTING_FILE_ASSOCIATION_PLAYLIST, FileExtensions::PLAYLIST_EXTS).toString();

    this->ui->video->setText(video);
    this->ui->audio->setText(audio);
    this->ui->subtitle->setText(subtitle);
    this->ui->playlist->setText(playlist);

    this->m_orgExts = (video + " " + audio + " " + subtitle + " " + playlist).split(" ", Qt::SkipEmptyParts);
}

FileAssociation::~FileAssociation()
{
    delete ui;
}

void FileAssociation::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void FileAssociation::on_videoDefault_clicked()
{
    this->ui->video->setText(FileExtensions::MOVIE_EXTS);
}

void FileAssociation::on_audioDefault_clicked()
{
    this->ui->audio->setText(FileExtensions::AUDIO_EXTS);
}

void FileAssociation::on_subtitleDefault_clicked()
{
    this->ui->subtitle->setText(FileExtensions::SUBTITLE_EXTS);
}

void FileAssociation::on_playlistDefault_clicked()
{
    this->ui->playlist->setText(FileExtensions::PLAYLIST_EXTS);
}

void FileAssociation::on_apply_clicked()
{
    QString video = this->ui->video->text().trimmed();
    QString audio = this->ui->audio->text().trimmed();
    QString subtitle = this->ui->subtitle->text().trimmed();
    QString playlist = this->ui->playlist->text().trimmed();
    QStringList exts = (video + " " + audio + " " + subtitle + " " + playlist).split(" ", Qt::SkipEmptyParts);

    FileAssociationInstaller::uninstallFileAssociation(this->m_orgExts);
    FileAssociationInstaller::installFileAssociation(exts);

    this->m_orgExts = exts;

    this->m_settings.setValue(SETTING_FILE_ASSOCIATION_VIDEO, video);
    this->m_settings.setValue(SETTING_FILE_ASSOCIATION_AUDIO, audio);
    this->m_settings.setValue(SETTING_FILE_ASSOCIATION_SUBTITLE, subtitle);
    this->m_settings.setValue(SETTING_FILE_ASSOCIATION_PLAYLIST, playlist);
    this->m_settings.sync();

#ifdef Q_OS_WIN
    MessageBoxUtils::informationMessageBox(this, tr("관리자 권한으로 실행하지 않았을 경우 정상적으로 적용 되지 않을 수 있습니다"));
#endif

    this->accept();
}
