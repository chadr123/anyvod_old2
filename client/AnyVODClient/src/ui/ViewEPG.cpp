﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ViewEPG.h"
#include "ui_viewepg.h"
#include "EPGItem.h"
#include "device/DTVReader.h"

#include <QDebug>

ViewEPG::ViewEPG(DTVReader &reader, const QString channelName, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ViewEPG),
    m_reader(reader),
    m_updateTimer(0),
    m_timeTimer(0)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    const auto btns = this->findChildren<QPushButton*>();

    for (QPushButton *btn : btns)
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    this->ui->channel->setText(channelName);
    this->ui->title->setText(tr("업데이트 중..."));
    this->ui->period->setText(tr("업데이트 중..."));
    this->ui->curDateTime->setText(tr("업데이트 중..."));

    this->m_updateTimer = this->startTimer(500);
}

ViewEPG::~ViewEPG()
{
    delete ui;
}

void ViewEPG::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void ViewEPG::updateCurrentDateTime()
{
    QDateTime time = QDateTime::currentDateTime();
    qint64 diff = this->m_startDateTime.secsTo(time);
    QDateTime cur = this->m_curDateTime.addSecs(diff);

    this->ui->curDateTime->setText(cur.toString("yyyy-MM-dd hh:mm:ss"));
}

void ViewEPG::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == this->m_updateTimer)
    {
        QVector<DTVReaderInterface::EPG> epgs;
        QListWidget *list = this->ui->items;
        QListWidgetItem *curItem = nullptr;

        this->killTimer(event->timerId());
        this->m_reader.getEPG(&epgs);

        for (const DTVReaderInterface::EPG &epg : qAsConst(epgs))
        {
            QListWidgetItem *widgetItem = new QListWidgetItem;
            EPGItem *epgItem = new EPGItem(epg, list);

            widgetItem->setSizeHint(epgItem->sizeHint());
            widgetItem->setData(Qt::UserRole, (qulonglong)epgItem);

            list->addItem(widgetItem);
            list->setItemWidget(widgetItem, epgItem);

            QDateTime curDate = QDateTime::currentDateTime();
            QDateTime startDate = epg.start.toLocalTime();
            QDateTime endDate = startDate.addSecs(QTime(0, 0).secsTo(epg.duration));

            if (startDate <= curDate && endDate >= curDate)
                curItem = widgetItem;
        }

        if (curItem)
        {
            this->on_items_itemClicked(curItem);
            list->scrollToItem(curItem);
            curItem->setSelected(true);
        }
        else
        {
            this->ui->title->clear();
            this->ui->period->clear();
        }

        QDateTime curDate = this->m_reader.getCurrentDateTime();

        if (curDate.isValid())
            this->m_curDateTime = curDate.toLocalTime();
        else
            this->m_curDateTime = QDateTime::currentDateTime();

        this->m_startDateTime = QDateTime::currentDateTime();
        this->m_timeTimer = this->startTimer(1000);
    }
    else if (event->timerId() == this->m_timeTimer)
    {
        this->updateCurrentDateTime();
    }
}

void ViewEPG::on_items_itemClicked(QListWidgetItem *item)
{
   EPGItem *epgItem = (EPGItem*)item->data(Qt::UserRole).toULongLong();
   DTVReaderInterface::EPG epg;
   QString period;
   QDateTime date;

   epgItem->getEPG(&epg);

   date = epg.start.toLocalTime();

   period = date.toString("yyyy-MM-dd hh:mm:ss");
   period += " ~ ";
   period += date.addSecs(QTime(0, 0).secsTo(epg.duration)).toString("hh:mm:ss");

   this->ui->title->setText(epg.title);
   this->ui->period->setText(period);
}
