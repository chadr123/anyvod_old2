﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "URLPickerInterface.h"

class URLPicker
{
public:
    URLPicker();
    ~URLPicker();

    QVector<URLPickerInterface::Item> pickURL(const QString &url);

    bool getAudioStreamByMimeType(const QString &pickerURL, const QString &url, const QString &mimeType, QString *ret);
    bool getProperAudioStreamByMimeType(const QString &pickerURL, const QVector<URLPickerInterface::Item> &items, const QString &mimeType, QString *ret);

    bool isPlayList() const;
    QString getPlayListTitle() const;
    QString getPlayListURL() const;

private:
    bool isVaild() const;
    bool initPicker(const QString &pickerURL);
    URLPickerInterface* getPicker(const QString &url) const;

private:
    URLPickerInterface *m_picker;
};
