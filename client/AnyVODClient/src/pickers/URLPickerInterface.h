﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QString>
#include <QUrlQuery>

class URLPickerInterface
{
public:
    struct Item
    {
        QString title;
        QString url;
        QString id;
        QString orgUrl;
        QString desc;
        QString shortDesc;
        QString mimeType;
        QString quality;
        QString dashmpd;
        QUrlQuery query;
        bool noAudio;

        bool operator < (const Item &rhs) const
        {
            return desc < rhs.desc;
        }
    };

public:
    URLPickerInterface();
    virtual ~URLPickerInterface();

    virtual bool isSupported(const QString &url) const = 0;

    virtual QVector<URLPickerInterface::Item> pickURL(const QString &url) = 0;

    virtual bool getAudioStreamByMimeType(const QString &url, const QString &mimeType, QString *ret) const = 0;
    virtual bool getProperAudioStreamByMimeType(const QVector<URLPickerInterface::Item> &items, const QString &mimeType, QString *ret) const = 0;

    bool isPlayList() const;
    QString getPlayListTitle() const;
    QString getPlayListURL() const;

protected:
    QString escapeHtml(const QString &html) const;
    QString markLive(const QString &url) const;

    void getFormatListFromM3U8(const QString &url, bool isLive, URLPickerInterface::Item &refItem, QVector<URLPickerInterface::Item> *ret);

protected:
    bool m_isPlayList;
    QString m_playListTitle;
    QString m_playListURL;
};
