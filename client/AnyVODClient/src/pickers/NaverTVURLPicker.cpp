﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "NaverTVURLPicker.h"
#include "core/Common.h"
#include "net/HttpDownloader.h"
#include "utils/ConvertingUtils.h"

#include <QRegularExpression>
#include <QUrlQuery>
#include <QBuffer>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QDebug>

//http://naver.me/5gKav9Io
//https://tv.naver.com/s/vingo/907
//https://tv.naver.com/v/1001212
//https://tv.naver.com/v/875247
//http://tv.naver.com/v/81652
//http://tvcast.naver.com/v/81652
//http://m.tv.naver.com/v/81652
//http://m.tvcast.naver.com/v/81652
//http://tv.naver.com/v/395837
//https://tv.naver.com/v/3919421
//https://tv.naver.com/l/51549
//https://tv.naver.com/v/3506511/list/229965
//http://tvcast.naver.com/live/news/74f
//https://sports.news.naver.com/video.nhn?id=591829
//https://sports.news.naver.com/tv/index.nhn?uCategory=others&category=etc&gameId=20210211IBSF1&type=onair
//https://sports.news.naver.com/tv/index.nhn?category=etc&gameId=20131228SPOTVGAME
//https://m.sports.naver.com/esports/gamecenter/lol/index.nhn?gameId=2018100613TBDTBDlol

const QString NaverTVURLPicker::VOD_API = "https://apis.naver.com/rmcnmv/rmcnmv/vod/play/v2.0/";
const QString NaverTVURLPicker::VOD_NEWS_LIVE_INFO_API = "https://apis.naver.com/pcLive/livePlatform/liveInfo?seriesId=";
const QString NaverTVURLPicker::VOD_NEWS_LIVE_INFO2_API = "https://apis.naver.com/pcLive/livePlatform/liveInfo2?";
const QString NaverTVURLPicker::VOD_NEWS_URL_API = "https://apis.naver.com/pcLive/livePlatform/sUrl?p=hls";

const auto ITEM_SORT_FUNC = [] (const URLPickerInterface::Item &first, const URLPickerInterface::Item &second) -> bool
{
    QRegularExpression numberExp(R"((\d+))");
    int firstQuality = numberExp.match(first.quality).captured(1).toInt();
    int secondQuality = numberExp.match(second.quality).captured(1).toInt();

    return firstQuality > secondQuality;
};

NaverTVURLPicker::NaverTVURLPicker()
{

}

NaverTVURLPicker::~NaverTVURLPicker()
{

}

bool NaverTVURLPicker::isSupported(const QString &url) const
{
    return url.contains("tv.naver.com") ||
           url.contains("tvcast.naver.com") ||
           url.contains("sports.naver.com") ||
           url.contains("sports.news.naver.com") ||
           url.contains("naver.me");
}

QVector<URLPickerInterface::Item> NaverTVURLPicker::pickURL(const QString &url)
{
    QVector<URLPickerInterface::Item> items;

    if (url.contains("/list/", Qt::CaseInsensitive))
    {
        items = this->getPlayList(url);
    }
    else if (url.contains("/v/", Qt::CaseInsensitive))
    {
        QRegularExpression exp(R"(naver\.com/v/(\d+))");
        QRegularExpressionMatch match = exp.match(url);

        if (match.hasMatch())
        {
            QString id = match.captured(1);

            items = this->getVideoList(url, id, VT_NONE);
        }
    }
    else if (url.contains("/l/", Qt::CaseInsensitive))
    {
        QRegularExpression exp(R"(naver\.com/l/(\d+))");
        QRegularExpressionMatch match = exp.match(url);

        if (match.hasMatch())
        {
            QString id = match.captured(1);

            items = this->getLiveVideoList(url, id);
        }
    }
    else if (url.contains("/live/news/", Qt::CaseInsensitive))
    {
        QRegularExpression exp(R"(naver\.com/live/news/(.*))");
        QRegularExpressionMatch match = exp.match(url);

        if (match.hasMatch())
        {
            QString id = match.captured(1);

            items = this->getLiveNewsVideoList(url, id);
        }
    }
    else if (url.contains("/s/vingo/", Qt::CaseInsensitive))
    {
        QRegularExpression exp(R"(naver\.com/s/vingo/(\d+))");
        QRegularExpressionMatch match = exp.match(url);

        if (match.hasMatch())
        {
            QString id = match.captured(1);

            items = this->getVideoList(url, id, VT_VINGO);
        }
    }
    else if (url.contains("sports.news.naver.com/tv/", Qt::CaseInsensitive))
    {
        items = this->getLiveSportsVideoList(url);
    }
    else if (url.contains("sports.naver.com", Qt::CaseInsensitive))
    {
        QRegularExpression exp(R"(sports\.naver\.com/.*/(.*)/.*gameId=(.*))");
        QRegularExpressionMatch match = exp.match(url);

        if (match.hasMatch())
        {
            QString cat = match.captured(1);
            QString gameID = match.captured(2);
            QString newUrl = QString("https://sports.news.naver.com/tv/index.nhn?category=%1&gameId=%2")
                             .arg(cat, gameID);

            items = this->getLiveSportsVideoList(newUrl);
        }
    }
    else if (url.contains("sports.news.naver.com/video.nhn", Qt::CaseInsensitive) ||
             url.contains(QRegularExpression("sports.news.naver.com/.*/vod/index.nhn")))
    {
        QUrl u(url);
        QString id = QUrlQuery(u.query()).queryItemValue("id");

        items = this->getVideoList(url, id, VT_SPORT);
    }
    else if (url.contains("naver.me", Qt::CaseInsensitive))
    {
        HttpDownloader d;
        QBuffer b;

        if (d.download(url, b))
            return this->pickURL(d.getRedirectURL());
    }

    return items;
}

bool NaverTVURLPicker::getAudioStreamByMimeType(const QString &url, const QString &mimeType, QString *ret) const
{
    (void)url;
    (void)mimeType;
    (void)ret;

    return false;
}

bool NaverTVURLPicker::getProperAudioStreamByMimeType(const QVector<URLPickerInterface::Item> &items, const QString &mimeType, QString *ret) const
{
    (void)items;
    (void)mimeType;
    (void)ret;

    return false;
}

QJsonObject NaverTVURLPicker::getRootObject(const QString &url, VIDEO_TYPE videoType) const
{
    HttpDownloader d;
    QBuffer webPageBuffer;
    QJsonObject root;

    if (!d.download(url, webPageBuffer))
        return root;

    QRegularExpression videoIDExp;
    QRegularExpression inKeyExp;
    QRegularExpressionMatch match;
    QString webPage = webPageBuffer.buffer();
    QString videoID;
    QString inKey;

    switch (videoType)
    {
        case VT_SPORT:
        {
            videoIDExp.setPattern(R"(["']?masterVideoId["']?\s*:\s*["']?(.*)["'])");
            inKeyExp.setPattern(R"(["']?videoKey["']?\s*:\s*["']?(.*)["'])");

            break;
        }
        case VT_VINGO:
        {
            videoIDExp.setPattern(R"(["']?playerVideoId["']?\s*value=\s*["']?(.*)["'])");
            inKeyExp.setPattern(R"(["']?playerInKey["']?\s*value=\s*["']?(.*)["'])");

            break;
        }
        default:
        {
            videoIDExp.setPattern(R"(["']?videoId["']?\s*:\s*["']?(.*)["'])");
            inKeyExp.setPattern(R"(["']?inKey["']?\s*:\s*["']?(.*)["'])");

            break;
        }
    }

    match = videoIDExp.match(webPage);

    if (!match.hasMatch())
        return root;

    videoID = match.captured(1);

    match = inKeyExp.match(webPage);

    if (!match.hasMatch())
        return root;

    inKey = match.captured(1);

    QUrlQuery query;

    query.addQueryItem("key", inKey);
    query.addQueryItem("ver", "2.0");
    query.addQueryItem("devt", "html5_pc");
    query.addQueryItem("doct", "json");
    query.addQueryItem("ptc", "https");
    query.addQueryItem("sptc", "https");
    query.addQueryItem("sm", "tvcast");
    query.addQueryItem("cv", "NAVER");

    QBuffer apiBuffer;

    if (!d.download(VOD_API + videoID + "?" + query.toString(QUrl::FullyEncoded), apiBuffer))
        return root;

    QJsonDocument doc = QJsonDocument::fromJson(apiBuffer.buffer());

    if (!doc.isObject())
        return root;

    root = doc.object();

    return root;
}

QString NaverTVURLPicker::getLiveNewsURL(const QString &chID, const QString &quality) const
{
    HttpDownloader d;
    QBuffer buffer;
    QString url;
    QUrlQuery query;

    query.addQueryItem("ch", chID);
    query.addQueryItem("q", quality);

    if (!d.download(VOD_NEWS_URL_API + "&" + query.toString(QUrl::FullyEncoded), buffer))
        return url;

    QJsonDocument doc = QJsonDocument::fromJson(buffer.buffer());

    if (!doc.isObject())
        return url;

    QJsonObject root = doc.object();

    if (!root["success"].toBool(false))
        return url;

    return root["secUrl"].toString();
}

QString NaverTVURLPicker::getTitle(const QString &url, NaverTVURLPicker::VIDEO_TYPE videoType) const
{
    HttpDownloader d;
    QBuffer webPageBuffer;

    if (!d.download(url, webPageBuffer))
        return QString();

    QString webPage = webPageBuffer.buffer();
    QRegularExpression titleExp;
    QRegularExpressionMatch match;

    switch (videoType)
    {
        case VT_VINGO:
        {
            titleExp.setPattern(R"(["']?sChannelName["']?\s*:\s*["']?(.*)["'])");
            break;
        }
        default:
        {
            break;
        }
    }

    match = titleExp.match(webPage);

    if (!match.hasMatch())
        return QString();

    return match.captured(1);
}

QVector<URLPickerInterface::Item> NaverTVURLPicker::getPlayList(const QString &url)
{
    QVector<URLPickerInterface::Item> items;
    HttpDownloader d;
    QBuffer webPageBuffer;

    this->m_playListTitle.clear();
    this->m_playListURL.clear();

    if (!d.download(url, webPageBuffer))
        return items;

    QString title;
    QString webPage = webPageBuffer.buffer();
    QRegularExpression exp(R"(id\s*=\s*["']?playlistInfo["']?\s*data-title\s*=\s*(['"][^'"]+['"]))");
    QRegularExpressionMatch match = exp.match(webPage);

    if (match.hasMatch())
    {
        title = match.captured(1);

        if (title.length() > 1)
        {
            if (title[0] == '"' || title[0] == '\'')
                title = title.mid(1);

            if (title[title.length() - 1] == '"' || title[title.length() - 1] == '\'')
                title = title.left(title.length() - 1);
        }
    }

    exp.setPattern(R"(<a\s*class\s*=\s*["']?_click["']?\s*data-moveclipno\s*=\s*["']?(\d+)["']?)");

    QRegularExpressionMatchIterator i = exp.globalMatch(webPage);

    while (i.hasNext())
    {
        QRegularExpressionMatch match = i.next();
        URLPickerInterface::Item item;
        QString vid = match.captured(1);

        item.url = QString("https://tv.naver.com/v/%1").arg(vid);
        item.orgUrl = item.url;
        item.id = vid;

        items.append(item);
    }

    this->m_playListTitle = this->escapeHtml(title);
    this->m_playListURL = url;

    this->m_isPlayList = true;

    return items;
}

QVector<URLPickerInterface::Item> NaverTVURLPicker::getVideoList(const QString &url, const QString &id, VIDEO_TYPE videoType)
{
    QVector<URLPickerInterface::Item> items;
    QJsonObject root = this->getRootObject(url, videoType);

    if (root.isEmpty())
        return items;

    QJsonObject meta = root["meta"].toObject();
    QJsonObject videos = root["videos"].toObject();
    QString title = meta["subject"].toString().trimmed();

    if (title.isEmpty())
        title = this->getTitle(url, videoType);

    items += this->getVideoListFromStream(videos["list"].toArray(), QUrlQuery(), url, id, title, false);

    const QJsonArray streams = root["streams"].toArray();

    for (const QJsonValue &stream : streams)
    {
        QJsonObject streamObj = stream.toObject();
        QUrlQuery query;

        const QJsonArray keys = streamObj["keys"].toArray();

        for (const QJsonValue &param : keys)
        {
            QJsonObject paramObj = param.toObject();

            query.addQueryItem(paramObj["name"].toString(), paramObj["value"].toString());
        }

        QString streamType = streamObj["type"].toString();
        QJsonArray videos = streamObj["videos"].toArray();
        bool isLive = streamType.toUpper() == "HLS";

        if (!videos.isEmpty())
        {
            items += this->getVideoListFromStream(videos, query, url, id, title, isLive);
        }
        else if (isLive)
        {
            QString streamURL = streamObj["source"].toString();

            if (streamURL.isEmpty())
                continue;

            URLPickerInterface::Item item;

            if (!query.isEmpty())
            {
                streamURL += "?" + query.toString(QUrl::FullyEncoded);
                item.query = query;
            }

            item.title = title;
            item.orgUrl = url;
            item.id = id;

            this->getFormatListFromM3U8(url, true, item, &items);
        }
    }

    std::sort(items.begin(), items.end(), ITEM_SORT_FUNC);
    this->m_isPlayList = false;

    return items;
}

QVector<URLPickerInterface::Item> NaverTVURLPicker::getLiveVideoList(const QString &url, const QString &id)
{
    QVector<URLPickerInterface::Item> items;
    HttpDownloader d;
    QBuffer webPageBuffer;

    if (!d.download(url, webPageBuffer))
        return items;

    QString webPage = webPageBuffer.buffer();
    QRegularExpression exp(R"(["']?sApiF["']?\s*:\s*["']?(.*)["'])");
    QRegularExpressionMatch match = exp.match(webPage);

    if (!match.hasMatch())
        return items;

    QString apiURL = match.captured(1);
    QBuffer apiBuffer;

    if (!d.download(apiURL, apiBuffer))
        return items;

    QJsonDocument doc = QJsonDocument::fromJson(apiBuffer.buffer());

    if (!doc.isObject())
        return items;

    QJsonObject root = doc.object();
    QJsonObject meta = root["meta"].toObject();
    QString title = meta["title"].toString();
    const QJsonArray streams = root["streams"].toArray();

    for (const QJsonValue &stream : streams)
    {
        URLPickerInterface::Item item;
        QJsonObject streamObj = stream.toObject();
        QString quality = streamObj["qualityId"].toString();

        item.url = streamObj["url"].toString();

        if (item.url.isEmpty())
            continue;

        QUrlQuery query(QUrl(url).query(QUrl::FullyEncoded));

        if (!query.isEmpty())
            item.query = query;

        QJsonObject property = streamObj["property"].toObject();
        int width = property["width"].toInt();
        int height = property["height"].toInt();

        item.url = this->markLive(item.url);
        item.id = id;
        item.noAudio = false;
        item.orgUrl = url;
        item.title = title;
        item.quality = quality;
        item.desc = QString("%1 %2x%3")
                    .arg(quality)
                    .arg(width)
                    .arg(height);
        item.shortDesc = item.desc;

        items.append(item);
    }

    std::sort(items.begin(), items.end(), ITEM_SORT_FUNC);
    this->m_isPlayList = false;

    return items;
}

QVector<URLPickerInterface::Item> NaverTVURLPicker::getLiveNewsVideoList(const QString &url, const QString &id)
{
    QVector<URLPickerInterface::Item> items;
    HttpDownloader d;
    QBuffer buffer;

    if (!d.download(VOD_NEWS_LIVE_INFO_API + id, buffer))
        return items;

    QJsonDocument doc = QJsonDocument::fromJson(buffer.buffer());

    if (!doc.isObject())
        return items;

    QJsonObject root = doc.object();
    bool success = root["success"].toBool(false);

    if (!success)
        return items;

    QJsonObject program = root["program"].toObject();
    QString title = program["title"].toString();
    QString chID = program["chId"].toString();
    QJsonObject channel = root["channel"].toObject();
    QJsonArray mobile = channel["mo"].toArray();
    const QJsonArray pc = channel["pc"].toArray();
    QJsonArray merged = mobile;

    for (const QJsonValue &stream : pc)
        merged.append(stream);

    for (const QJsonValue &stream : qAsConst(merged))
    {
        URLPickerInterface::Item item;
        QJsonObject streamObj = stream.toObject();
        bool contains = false;

        item.quality = streamObj["id"].toString();

        for (const URLPickerInterface::Item &i : qAsConst(items))
        {
            if (i.quality == item.quality)
            {
                contains = true;
                break;
            }
        }

        if (contains)
            continue;

        QString liveURL = this->getLiveNewsURL(chID, item.quality);

        if (liveURL.isEmpty())
            continue;

        item.id = id;
        item.noAudio = false;
        item.orgUrl = url;
        item.title = title;
        item.desc = QString("Video %1").arg(streamObj["name"].toString());
        item.shortDesc = item.desc;
        item.url = this->markLive(liveURL);

        items.append(item);
    }

    std::sort(items.begin(), items.end(), ITEM_SORT_FUNC);

    const QJsonArray audio = channel["audio"].toArray();

    for (const QJsonValue &stream : audio)
    {
        URLPickerInterface::Item item;
        QJsonObject streamObj = stream.toObject();

        item.quality = streamObj["id"].toString();

        QString liveURL = this->getLiveNewsURL(chID, item.quality);

        if (liveURL.isEmpty())
            continue;

        item.id = id;
        item.noAudio = false;
        item.orgUrl = url;
        item.title = title;
        item.desc = QString("Audio %1").arg(streamObj["name"].toString());
        item.shortDesc = item.desc;
        item.url = this->markLive(liveURL);

        items.append(item);
    }

    this->m_isPlayList = false;

    return items;
}

QVector<URLPickerInterface::Item> NaverTVURLPicker::getLiveSportsVideoList(const QString &url)
{
    QVector<URLPickerInterface::Item> items;
    HttpDownloader d;
    QBuffer webPageBuffer;

    if (!d.download(url, webPageBuffer))
        return items;

    QString webPage = webPageBuffer.buffer();
    QString gameID;
    QString svcID;
    QRegularExpression exp(R"(["']?gameId["']?\s*:\s*["']?(.*)["'])");
    QRegularExpressionMatch match = exp.match(webPage);

    if (!match.hasMatch())
        return items;

    gameID = match.captured(1);

    exp.setPattern(R"(["']?svcId ["']?\s*:\s*["']?(.*)["'])");
    match = exp.match(webPage);

    if (!match.hasMatch())
        return items;

    svcID = match.captured(1);

    QBuffer apiBuffer;
    QUrlQuery query;

    query.addQueryItem("svcId", svcID);
    query.addQueryItem("svcLiveId", gameID);

    if (!d.download(VOD_NEWS_LIVE_INFO2_API + query.toString(QUrl::FullyEncoded), apiBuffer))
        return items;

    QJsonDocument doc = QJsonDocument::fromJson(apiBuffer.buffer());

    if (!doc.isObject())
        return items;

    QJsonObject root = doc.object();
    bool success = root["success"].toBool(false);

    if (!success)
        return items;

    QJsonObject program = root["program"].toObject();
    QString title = program["title"].toString();
    const QJsonArray chList = root["chList"].toArray();
    QJsonObject chConf = root["chConf"].toObject();

    for (const QJsonValue &ch : chList)
    {
        QJsonObject chObj = ch.toObject();
        QString chID = chObj["chId"].toString();
        const QJsonArray channels = chConf[chID].toArray();

        for (const QJsonValue &channel : channels)
        {
            URLPickerInterface::Item item;
            QJsonObject channelObj = channel.toObject();

            item.quality = channelObj["id"].toString();

            QString liveURL = this->getLiveNewsURL(chID, item.quality);

            if (liveURL.isEmpty())
                continue;

            item.id = gameID;
            item.noAudio = false;
            item.orgUrl = url;
            item.title = title;
            item.desc = QString("Live %1").arg(channelObj["name"].toString());
            item.shortDesc = item.desc;
            item.url = this->markLive(liveURL);

            items.append(item);
        }
    }

    std::sort(items.begin(), items.end(), ITEM_SORT_FUNC);
    this->m_isPlayList = false;

    return items;
}

QVector<URLPickerInterface::Item> NaverTVURLPicker::getVideoListFromStream(const QJsonArray &streams, const QUrlQuery &query,
                                                                           const QString &orgURL, const QString &id, const QString &title,
                                                                           bool isLive)
{
    QVector<URLPickerInterface::Item> items;

    for (const QJsonValue &stream : streams)
    {
        URLPickerInterface::Item item;
        QJsonObject streamObj = stream.toObject();

        item.url = streamObj["source"].toString();

        if (item.url.isEmpty())
            continue;

        if (!query.isEmpty())
        {
            item.url += "?" + query.toString(QUrl::FullyEncoded);
            item.query = query;
        }

        if (isLive)
            item.url = this->markLive(item.url);

        QString codec = streamObj["type"].toString(isLive ? "Live" : "Unknown");
        QJsonObject option = streamObj["encodingOption"].toObject();
        int width = option["width"].toInt();
        int height = option["height"].toInt();
        QString name = option["name"].toString();
        QString quality = name + codec;
        QJsonObject bitrateObj = streamObj["bitrate"].toObject();
        unsigned long long bitrate = (bitrateObj["video"].toDouble() + bitrateObj["audio"].toDouble()) * KILO;

        item.id = id;
        item.noAudio = false;
        item.orgUrl = orgURL;
        item.title = title;
        item.quality = quality;
        item.desc = QString("%1 %2 %3x%4 (%5)")
                    .arg(name, ConvertingUtils::sizeToString(bitrate))
                    .arg(width)
                    .arg(height)
                    .arg(codec);
        item.shortDesc = item.desc;

        items.append(item);
    }

    return items;
}
