﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "URLPickerInterface.h"

class QJsonArray;
class QJsonObject;
class QUrlQuery;

class NaverTVURLPicker : public URLPickerInterface
{
public:
    NaverTVURLPicker();
    virtual ~NaverTVURLPicker();

    virtual bool isSupported(const QString &url) const;

    virtual QVector<URLPickerInterface::Item> pickURL(const QString &url);

    virtual bool getAudioStreamByMimeType(const QString &url, const QString &mimeType, QString *ret) const;
    virtual bool getProperAudioStreamByMimeType(const QVector<URLPickerInterface::Item> &items, const QString &mimeType, QString *ret) const;

public:
    enum VIDEO_TYPE
    {
        VT_NONE,
        VT_SPORT,
        VT_VINGO,
        VT_COUNT
    };

public:
    QJsonObject getRootObject(const QString &url, VIDEO_TYPE videoType) const;

private:
    QString getLiveNewsURL(const QString &chID, const QString &quality) const;
    QString getTitle(const QString &url, VIDEO_TYPE videoType) const;

    QVector<URLPickerInterface::Item> getPlayList(const QString &url);
    QVector<URLPickerInterface::Item> getVideoList(const QString &url, const QString &id, VIDEO_TYPE videoType);
    QVector<URLPickerInterface::Item> getLiveVideoList(const QString &url, const QString &id);
    QVector<URLPickerInterface::Item> getLiveNewsVideoList(const QString &url, const QString &id);
    QVector<URLPickerInterface::Item> getLiveSportsVideoList(const QString &url);
    QVector<URLPickerInterface::Item> getVideoListFromStream(const QJsonArray &streams, const QUrlQuery &query,
                                                             const QString &orgURL, const QString &id, const QString &title,
                                                             bool isLive);

private:
    static const QString VOD_API;
    static const QString VOD_NEWS_LIVE_INFO_API;
    static const QString VOD_NEWS_LIVE_INFO2_API;
    static const QString VOD_NEWS_URL_API;
};
