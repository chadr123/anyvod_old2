﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "URLPickerInterface.h"
#include "core/Common.h"
#include "net/HttpDownloader.h"
#include "utils/ConvertingUtils.h"

#include <QTextDocument>
#include <QBuffer>
#include <QRegularExpression>

URLPickerInterface::URLPickerInterface() :
    m_isPlayList(false)
{

}

URLPickerInterface::~URLPickerInterface()
{

}

bool URLPickerInterface::isPlayList() const
{
    return this->m_isPlayList;
}

QString URLPickerInterface::getPlayListTitle() const
{
    return this->m_playListTitle;
}

QString URLPickerInterface::getPlayListURL() const
{
    return this->m_playListURL;
}

QString URLPickerInterface::escapeHtml(const QString &html) const
{
    QTextDocument escape;
    QString escaped;

    escape.setHtml(html);
    escaped = escape.toPlainText();
    escape.setHtml(escaped);
    escaped = escape.toPlainText();

    return escaped;
}

QString URLPickerInterface::markLive(const QString &url) const
{
    QString marked = url;

    marked.prepend(LIVE);

    return marked;
}

void URLPickerInterface::getFormatListFromM3U8(const QString &url, bool isLive, URLPickerInterface::Item &refItem, QVector<URLPickerInterface::Item> *ret)
{
    HttpDownloader d;
    QBuffer m3u8;
    QString m3u8Data;

    if (!d.download(url, m3u8))
        return;

    m3u8Data = m3u8.buffer();

    QTextStream stream(&m3u8Data);

    while (true)
    {
        QString line = stream.readLine().trimmed();

        if (line.isNull())
            break;

        if (line.isEmpty())
            continue;

        if (!line.startsWith("#EXT-X-STREAM-INF"))
            continue;

        int index = line.indexOf(":");
        int end;

        if (index < 0)
            continue;

        index++;

        QString bitrate;
        QString size;
        QString codec;
        QString video;
        QString fps;
        QString videoUrl;
        QString data = line.mid(index);

        index = 0;

        while (true)
        {
            QString key;
            QString value;
            bool quota = false;

            end = data.indexOf("=", index);

            if (end < 0)
                break;

            key = data.mid(index, end - index).toUpper();

            end++;

            if (data[end] == '"' || data[end] == '\'')
            {
                QChar c = data[end];

                end++;

                index = end;
                end = data.indexOf(c, index);

                if (end < 0)
                {
                    end = data.indexOf(',', index);

                    if (end < 0)
                        end = data.length();
                }

                quota = true;
            }
            else
            {
                index = end;
                end = data.indexOf(',', index);

                if (end < 0)
                    end = data.length();
            }

            value = data.mid(index, end - index);
            index = end + 1;

            if (quota)
                index++;

            if (key == "BANDWIDTH")
                bitrate = value;
            else if (key == "CODECS")
                codec = value;
            else if (key == "RESOLUTION")
                size = value;
            else if (key == "VIDEO")
                video = value;
            else if (key == "FRAME-RATE")
                fps = value;
        }

        videoUrl = stream.readLine().trimmed();

        QString extra;

        if (!fps.isEmpty())
            extra += fps + "fps ";

        if (!video.isEmpty())
            extra += video + " ";

        refItem.desc = QString("%1 %2 %3(%4)")
                .arg(ConvertingUtils::sizeToString(bitrate.toInt()), size, extra, codec);
        refItem.shortDesc = refItem.desc;

        refItem.quality = bitrate + extra;
        refItem.url = isLive ? this->markLive(videoUrl) : videoUrl;

        ret->append(refItem);
    }
}
