﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "EnumsTranslator.h"

EnumsTranslator::EnumsTranslator()
{
    this->setupConstStrings();
}

EnumsTranslator::~EnumsTranslator()
{

}

EnumsTranslator& EnumsTranslator::getInstance()
{
    static EnumsTranslator instance;

    return instance;
}

void EnumsTranslator::reload()
{
    this->setupConstStrings();
}

void EnumsTranslator::setupConstStrings()
{
    this->setup3DMethod();
    this->setup3DMethodCategory();
    this->setupSubtitle3DMethod();
    this->setupAnaglyphAlgorithm();
    this->setupDeinterlaceMethod();
    this->setupUpdateGap();
    this->setupDistortionAdjustMode();
    this->setupVRInputSource();
    this->setupProjectionType();
}

void EnumsTranslator::setup3DMethod()
{
    this->m_3DMethodDesc[AnyVODEnums::V3M_NONE] = tr("사용 안 함");
    this->m_3DMethodDesc[AnyVODEnums::V3M_HALF_LEFT] = tr("왼쪽 영상 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_HALF_RIGHT] = tr("오른쪽 영상 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_HALF_TOP] = tr("상단 영상 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_HALF_BOTTOM] = tr("하단 영상 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_FULL_LEFT_RIGHT] = tr("좌우 영상 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_FULL_TOP_BOTTOM] = tr("상하 영상 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_LEFT_PRIOR] = tr("왼쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_RIGHT_PRIOR] = tr("오른쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_TOP_PRIOR] = tr("상단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_BOTTOM_PRIOR] = tr("하단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_ROW_LEFT_RIGHT_LEFT_PRIOR] = tr("왼쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_ROW_LEFT_RIGHT_RIGHT_PRIOR] = tr("오른쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_ROW_TOP_BOTTOM_TOP_PRIOR] = tr("상단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_ROW_TOP_BOTTOM_BOTTOM_PRIOR] = tr("하단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_COL_LEFT_RIGHT_LEFT_PRIOR] = tr("왼쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_COL_LEFT_RIGHT_RIGHT_PRIOR] = tr("오른쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_COL_TOP_BOTTOM_TOP_PRIOR] = tr("상단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_COL_TOP_BOTTOM_BOTTOM_PRIOR] = tr("하단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_LEFT_PRIOR] = tr("왼쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_RIGHT_PRIOR] = tr("오른쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_TOP_PRIOR] = tr("상단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_BOTTOM_PRIOR] = tr("하단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_LEFT_PRIOR] = tr("왼쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_RIGHT_PRIOR] = tr("오른쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_TOP_PRIOR] = tr("상단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_BOTTOM_PRIOR] = tr("하단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_LEFT_PRIOR] = tr("왼쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_RIGHT_PRIOR] = tr("오른쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_TOP_PRIOR] = tr("상단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_BOTTOM_PRIOR] = tr("하단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_LEFT_PRIOR] = tr("왼쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_RIGHT_PRIOR] = tr("오른쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_TOP_PRIOR] = tr("상단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_BOTTOM_PRIOR] = tr("하단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_LEFT_PRIOR] = tr("왼쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_RIGHT_PRIOR] = tr("오른쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_TOP_PRIOR] = tr("상단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_BOTTOM_PRIOR] = tr("하단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_LEFT_PRIOR] = tr("왼쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_RIGHT_PRIOR] = tr("오른쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_TOP_PRIOR] = tr("상단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_BOTTOM_PRIOR] = tr("하단 영상 우선 사용");
}

void EnumsTranslator::setup3DMethodCategory()
{
    this->m_3DMethodCategory[AnyVODEnums::V3M_NONE] = tr("일반");
    this->m_3DMethodCategory[AnyVODEnums::V3M_HALF_LEFT] = tr("일반");
    this->m_3DMethodCategory[AnyVODEnums::V3M_HALF_RIGHT] = tr("일반");
    this->m_3DMethodCategory[AnyVODEnums::V3M_HALF_TOP] = tr("일반");
    this->m_3DMethodCategory[AnyVODEnums::V3M_HALF_BOTTOM] = tr("일반");
    this->m_3DMethodCategory[AnyVODEnums::V3M_FULL_LEFT_RIGHT] = tr("일반");
    this->m_3DMethodCategory[AnyVODEnums::V3M_FULL_TOP_BOTTOM] = tr("일반");
    this->m_3DMethodCategory[AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_LEFT_PRIOR] = tr("Page Flipping");
    this->m_3DMethodCategory[AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_RIGHT_PRIOR] = tr("Page Flipping");
    this->m_3DMethodCategory[AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_TOP_PRIOR] = tr("Page Flipping");
    this->m_3DMethodCategory[AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_BOTTOM_PRIOR] = tr("Page Flipping");
    this->m_3DMethodCategory[AnyVODEnums::V3M_ROW_LEFT_RIGHT_LEFT_PRIOR] = tr("Row Interlaced");
    this->m_3DMethodCategory[AnyVODEnums::V3M_ROW_LEFT_RIGHT_RIGHT_PRIOR] = tr("Row Interlaced");
    this->m_3DMethodCategory[AnyVODEnums::V3M_ROW_TOP_BOTTOM_TOP_PRIOR] = tr("Row Interlaced");
    this->m_3DMethodCategory[AnyVODEnums::V3M_ROW_TOP_BOTTOM_BOTTOM_PRIOR] = tr("Row Interlaced");
    this->m_3DMethodCategory[AnyVODEnums::V3M_COL_LEFT_RIGHT_LEFT_PRIOR] = tr("Column Interlaced");
    this->m_3DMethodCategory[AnyVODEnums::V3M_COL_LEFT_RIGHT_RIGHT_PRIOR] = tr("Column Interlaced");
    this->m_3DMethodCategory[AnyVODEnums::V3M_COL_TOP_BOTTOM_TOP_PRIOR] = tr("Column Interlaced");
    this->m_3DMethodCategory[AnyVODEnums::V3M_COL_TOP_BOTTOM_BOTTOM_PRIOR] = tr("Column Interlaced");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_LEFT_PRIOR] = tr("Red-Cyan Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_RIGHT_PRIOR] = tr("Red-Cyan Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_TOP_PRIOR] = tr("Red-Cyan Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_BOTTOM_PRIOR] = tr("Red-Cyan Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_LEFT_PRIOR] = tr("Green-Magenta Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_RIGHT_PRIOR] = tr("Green-Magenta Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_TOP_PRIOR] = tr("Green-Magenta Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_BOTTOM_PRIOR] = tr("Green-Magenta Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_LEFT_PRIOR] = tr("Yellow-Blue Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_RIGHT_PRIOR] = tr("Yellow-Blue Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_TOP_PRIOR] = tr("Yellow-Blue Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_BOTTOM_PRIOR] = tr("Yellow-Blue Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_LEFT_PRIOR] = tr("Red-Blue Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_RIGHT_PRIOR] = tr("Red-Blue Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_TOP_PRIOR] = tr("Red-Blue Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_BOTTOM_PRIOR] = tr("Red-Blue Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_LEFT_PRIOR] = tr("Red-Green Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_RIGHT_PRIOR] = tr("Red-Green Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_TOP_PRIOR] = tr("Red-Green Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_BOTTOM_PRIOR] = tr("Red-Green Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_LEFT_PRIOR] = tr("Checker Board");
    this->m_3DMethodCategory[AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_RIGHT_PRIOR] = tr("Checker Board");
    this->m_3DMethodCategory[AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_TOP_PRIOR] = tr("Checker Board");
    this->m_3DMethodCategory[AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_BOTTOM_PRIOR] = tr("Checker Board");
}

void EnumsTranslator::setupSubtitle3DMethod()
{
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_NONE] = tr("사용 안 함");
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_TOP_BOTTOM] = tr("상/하");
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_LEFT_RIGHT] = tr("좌/우");
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_PAGE_FLIP] = tr("페이지 플리핑");
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_INTERLACED] = tr("인터레이스");
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_CHECKER_BOARD] = tr("체커 보드");
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_ANAGLYPH] = tr("애너글리프");
}

void EnumsTranslator::setupAnaglyphAlgorithm()
{
    this->m_anaglyphAlgorithmDesc[AnyVODEnums::AGA_GRAY] = "Gray";
    this->m_anaglyphAlgorithmDesc[AnyVODEnums::AGA_COLORED] = "Colored";
    this->m_anaglyphAlgorithmDesc[AnyVODEnums::AGA_HALF_COLORED] = "Half Colored";
    this->m_anaglyphAlgorithmDesc[AnyVODEnums::AGA_DUBOIS] = "Dubois";
}

void EnumsTranslator::setupDeinterlaceMethod()
{
    this->m_deinterlaceDesc[AnyVODEnums::DA_BLEND] = "Blend";
    this->m_deinterlaceDesc[AnyVODEnums::DA_BOB] = "BOB";
    this->m_deinterlaceDesc[AnyVODEnums::DA_YADIF] = "YADIF";
    this->m_deinterlaceDesc[AnyVODEnums::DA_YADIF_BOB] = "YADIF(BOB)";
    this->m_deinterlaceDesc[AnyVODEnums::DA_W3FDIF] = "Weston 3 Field";
    this->m_deinterlaceDesc[AnyVODEnums::DA_KERNDEINT] = "Adaptive Kernel";
    this->m_deinterlaceDesc[AnyVODEnums::DA_MCDEINT] = "Motion Compensation";
    this->m_deinterlaceDesc[AnyVODEnums::DA_BWDIF] = "Bob Weaver";
    this->m_deinterlaceDesc[AnyVODEnums::DA_BWDIF_BOB] = "Bob Weaver(BOB)";
}

void EnumsTranslator::setupUpdateGap()
{
    this->m_updateGapDesc[AnyVODEnums::CUG_NONE] = tr("확인하지 않음");
    this->m_updateGapDesc[AnyVODEnums::CUG_ALWAYS] = tr("매 실행 시");
    this->m_updateGapDesc[AnyVODEnums::CUG_EVERY_DAY] = tr("1일");
    this->m_updateGapDesc[AnyVODEnums::CUG_WEEK] = tr("1주일");
    this->m_updateGapDesc[AnyVODEnums::CUG_MONTH] = tr("한 달");
    this->m_updateGapDesc[AnyVODEnums::CUG_HALF_YEAR] = tr("반 년");
    this->m_updateGapDesc[AnyVODEnums::CUG_YEAR] = tr("1년");
}

void EnumsTranslator::setupDistortionAdjustMode()
{
    this->m_distortionAdjustModeDesc[AnyVODEnums::DAM_NONE] = tr("사용 안 함");
    this->m_distortionAdjustModeDesc[AnyVODEnums::DAM_BARREL] = tr("VR 평면 영상");
    this->m_distortionAdjustModeDesc[AnyVODEnums::DAM_PINCUSION] = tr("360도 영상");
}

void EnumsTranslator::setupVRInputSource()
{
    this->m_vrInputSourceDesc[AnyVODEnums::VRI_NONE] = tr("사용 안 함");
    this->m_vrInputSourceDesc[AnyVODEnums::VRI_LEFT_RIGHT_LEFT_PRIOR] = tr("좌우 영상(좌측 영상 우선)");
    this->m_vrInputSourceDesc[AnyVODEnums::VRI_LEFT_RIGHT_RIGHT_PRIOR] = tr("좌우 영상(우측 영상 우선)");
    this->m_vrInputSourceDesc[AnyVODEnums::VRI_TOP_BOTTOM_TOP_PRIOR] = tr("상하 영상(상단 영상 우선)");
    this->m_vrInputSourceDesc[AnyVODEnums::VRI_TOP_BOTTOM_BOTTOM_PRIOR] = tr("상하 영상(하단 영상 우선)");
    this->m_vrInputSourceDesc[AnyVODEnums::VRI_COPY] = tr("복제");
    this->m_vrInputSourceDesc[AnyVODEnums::VRI_VIRTUAL_3D] = tr("가상 3D");
}

void EnumsTranslator::setupProjectionType()
{
    this->m_projectionTypeDesc[AnyVODEnums::V3T_EQR] = "Equirectangular";
    this->m_projectionTypeDesc[AnyVODEnums::V3T_CUBEMAP_2X3] = "Cubemap (2x3)";
    this->m_projectionTypeDesc[AnyVODEnums::V3T_EAC_2X3] = "Equi-Angular Cubemap (2x3)";
    this->m_projectionTypeDesc[AnyVODEnums::V3T_EAC_3X2] = "Equi-Angular Cubemap (3x2)";
}

QString EnumsTranslator::get3DMethodDesc(AnyVODEnums::Video3DMethod method) const
{
    return this->m_3DMethodDesc[method];
}

QString EnumsTranslator::get3DMethodCategoryDesc(AnyVODEnums::Video3DMethod method) const
{
    return this->m_3DMethodCategory[method];
}

QString EnumsTranslator::getSubtitle3DMethodDesc(AnyVODEnums::Subtitle3DMethod method) const
{
    return this->m_subtitle3DMethodDesc[method];
}

QString EnumsTranslator::getAnaglyphAlgoritmDesc(AnyVODEnums::AnaglyphAlgorithm algorithm) const
{
    return this->m_anaglyphAlgorithmDesc[algorithm];
}

QString EnumsTranslator::getDeinterlaceDesc(AnyVODEnums::DeinterlaceAlgorithm algorithm) const
{
    return this->m_deinterlaceDesc[algorithm];
}

QString EnumsTranslator::getUpdateGapDesc(AnyVODEnums::CheckUpdateGap gap) const
{
    return this->m_updateGapDesc[gap];
}

QString EnumsTranslator::getVRInputSourceDesc(AnyVODEnums::VRInputSource source) const
{
    return this->m_vrInputSourceDesc[source];
}

QString EnumsTranslator::getDistortionAdjustModeDesc(AnyVODEnums::DistortionAdjustMode mode) const
{
    return this->m_distortionAdjustModeDesc[mode];
}

QString EnumsTranslator::getProjectionTypeDesc(AnyVODEnums::Video360Type type) const
{
    return this->m_projectionTypeDesc[type];
}
