﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "TextEncodingDetector.h"
#include "parsers/subtitle/GlobalSubtitleCodecName.h"

#include <QFile>
#include <QDebug>

TextEncodingDetector::TextEncodingDetector()
{

}

bool TextEncodingDetector::getCodecName(const QString &filePath, QString *ret) const
{
    QFile file(filePath);
    QString codecName;
    QString ascii = GlobalSubtitleCodecName::getInstance().getCodecName();

    if (!this->getCodecNameFromBOM(filePath, &codecName))
        return false;

    if (codecName.isEmpty())
    {
        if (file.open(QIODevice::ReadOnly))
        {
            if (this->isUTF8(file))
                *ret = "UTF-8";
            else
                *ret = ascii;

            file.close();

            return true;
        }
    }
    else
    {
        *ret = codecName;
        return true;
    }

    return false;
}

bool TextEncodingDetector::getCodecNameFromBOM(const QString &filePath, QString *ret) const
{
    QFile file(filePath);

    if (file.open(QIODevice::ReadOnly))
    {
        uchar buf[4];
        qint64 readSize = file.read((char*)buf, sizeof(buf));

        file.close();

        if (readSize == sizeof(buf))
        {
            if (buf[0] == 0xef && buf[1] == 0xbb && buf[2] == 0xbf)
                *ret = "UTF-8";
            else if (buf[0] == 0xff && buf[1] == 0xfe && buf[2] == 0x00 && buf[3] == 0x00)
                *ret = "UTF-32LE";
            else if (buf[0] == 0x00 && buf[1] == 0x00 && buf[2] == 0xfe && buf[3] == 0xff)
                *ret = "UTF-32BE";
            else if (buf[0] == 0xff && buf[1] == 0xfe)
                *ret = "UTF-16LE";
            else if (buf[0] == 0xfe && buf[1] == 0xff)
                *ret = "UTF-16BE";

            return true;
        }
    }

    return false;
}

bool TextEncodingDetector::isUTF8(QFile &file) const
{
    bool utf8 = false;
    const int maxValidCount = 200;
    int validCount = 0;

    while (!file.atEnd() && validCount < maxValidCount)
    {
        char byte;

        if (file.read(&byte, 1) != 1)
            return false;

        if (this->inRange(byte, 0x00, 0x7f))
            continue;

        if (this->inRange(byte, 0x80, 0xbf))
            return false;

        int count = this->getByteCount(byte) - 1;

        if (count <= 0)
            return false;

        QByteArray seq = file.read(count);

        for (int i = 0; i < seq.count(); i++)
        {
            if (!this->inRange(seq[i], 0x80, 0xbf))
                return false;
        }

        utf8 = true;
        validCount++;
    }

    return utf8;
}

bool TextEncodingDetector::inRange(char byte, char first, char second) const
{
    return byte >= first && byte <= second;
}

int TextEncodingDetector::getByteCount(char byte) const
{
    if (this->isMatchMask(byte, 0xf0))
        return 4;

    if (this->isMatchMask(byte, 0xe0))
        return 3;

    if (this->isMatchMask(byte, 0xc0))
        return 2;

    return 0;
}

bool TextEncodingDetector::isMatchMask(char byte, char mask) const
{
    return (byte & mask) == mask;
}
