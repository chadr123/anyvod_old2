﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QStringList>

class FileExtensions
{
private:
    FileExtensions();

public:
    static const QString MOVIE_EXTS;
    static const QString AUDIO_EXTS;
    static const QString SUBTITLE_EXTS;
    static const QString PLAYLIST_EXTS;
    static const QString FONT_EXTS;
    static const QString CAPTURE_FORMAT_EXTS;

    static const QStringList MOVIE_EXTS_LIST;
    static const QStringList AUDIO_EXTS_LIST;
    static const QStringList SUBTITLE_EXTS_LIST;
    static const QStringList PLAYLIST_EXTS_LIST;
    static const QStringList MEDIA_EXTS_LIST;
    static const QStringList ALL_EXTS_LIST;
    static const QStringList FONT_EXTS_LIST;
    static const QStringList CAPTURE_FORMAT_LIST;
};
