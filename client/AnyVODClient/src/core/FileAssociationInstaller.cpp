﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "FileAssociationInstaller.h"
#include "TextEncodingDetector.h"
#include "Common.h"

#include <QMimeDatabase>
#include <QSettings>
#include <QDir>
#include <QApplication>
#include <QTextStream>
#include <QTextCodec>
#include <QFile>
#include <QStringList>

#ifdef Q_OS_WIN
# include <Shlobj.h>
#endif

FileAssociationInstaller::FileAssociationInstaller()
{

}

void FileAssociationInstaller::installFileAssociation(const QStringList &list)
{
#ifdef Q_OS_WIN
    QSettings hkcr("HKEY_CLASSES_ROOT", QSettings::NativeFormat);
    QSettings hkcu("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts", QSettings::NativeFormat);
    QString appPath = QDir::toNativeSeparators(QApplication::applicationFilePath());

    for (int i = 0; i < list.count(); i++)
    {
        QString ext = list[i].toLower();
        QString fileClass = QString(APP_NAME) + ".Media." + ext;
        QString old = hkcr.value("." + ext + "/.").toString();

        hkcr.setValue("." + ext + "/" + fileClass + "_backup", old);
        hkcr.setValue("." + ext + "/.", fileClass);

        hkcr.setValue(fileClass + "/.", "");
        hkcr.setValue(fileClass + "/DefaultIcon/.", appPath + ",0");
        hkcr.setValue(fileClass + "/shell/.", "open");
        hkcr.setValue(fileClass + "/shell/open/.", QString(tr("%1로 열기")).arg(APP_NAME));
        hkcr.setValue(fileClass + "/shell/open/command/.", appPath + " \"%1\"");

        old = hkcu.value("." + ext + "/UserChoice/Progid").toString();
        hkcu.setValue("." + ext + "/UserChoice_backup/Progid", old);

        FileAssociationInstaller::deleteRegKey(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts\\." + ext, "UserChoice");
        hkcu.setValue("." + ext + "/UserChoice/Progid", fileClass);
    }

    SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_FLUSH, nullptr, nullptr);
#elif defined Q_OS_MACOS
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFStringRef bundleID = CFBundleGetIdentifier(mainBundle);

    FileAssociationInstaller::setDefaultExtension(list, bundleID, kLSRolesViewer);

    CFRelease(bundleID);
#elif defined Q_OS_LINUX
    FileAssociationInstaller::setFileAssociation(list, false);
#else
    list.count();
#endif
}

void FileAssociationInstaller::uninstallFileAssociation(const QStringList &list)
{
#ifdef Q_OS_WIN
    QSettings hkcr("HKEY_CLASSES_ROOT", QSettings::NativeFormat);
    QSettings hkcu("HKEY_CURRENT_USER", QSettings::NativeFormat);

    for (int i = 0; i < list.count(); i++)
    {
        QString ext = list[i].toLower();
        QString fileClass = QString(APP_NAME) + ".Media." + ext;
        QString old = hkcr.value("." + ext + "/" + fileClass + "_backup").toString();

        hkcr.setValue("." + ext + "/.", old);
        hkcr.remove(fileClass);
        hkcr.remove("." + ext + "/" + fileClass + "_backup");

        old = hkcu.value("Software/Microsoft/Windows/CurrentVersion/Explorer/FileExts/." + ext + "/UserChoice_backup/Progid").toString();

        FileAssociationInstaller::deleteRegKey(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts\\." + ext, "UserChoice");
        hkcu.setValue("Software/Microsoft/Windows/CurrentVersion/Explorer/FileExts/." + ext + "/UserChoice/Progid", old);

        hkcu.remove("Software/Microsoft/Windows/CurrentVersion/Explorer/FileExts/." + ext + "/UserChoice_backup");
    }

    SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_FLUSH, nullptr, nullptr);
#elif defined Q_OS_MACOS
    CFStringRef bundleID = CFStringCreateWithCString(kCFAllocatorDefault, QString("com.apple.QuickTimePlayerX").toUtf8(), kCFStringEncodingUTF8);

    FileAssociationInstaller::setDefaultExtension(list, bundleID, kLSRolesAll);

    CFRelease(bundleID);
#elif defined Q_OS_LINUX
    FileAssociationInstaller::setFileAssociation(list, true);
#else
    list.count();
#endif
}

#ifdef Q_OS_WIN
void FileAssociationInstaller::deleteRegKey(HKEY root, const QString &subKey, const QString &toDeleteKey)
{
    HKEY key = NULL;

    if (RegOpenKeyExA(root, subKey.toLocal8Bit(), 0, KEY_SET_VALUE, &key) == ERROR_SUCCESS)
    {
        RegDeleteKeyA(key, toDeleteKey.toLocal8Bit());
        RegCloseKey(key);
    }
}
#endif

#if defined Q_OS_MACOS
void FileAssociationInstaller::setDefaultExtension(const QStringList &list, CFStringRef bundleID, UInt32 role)
{
    for (int i = 0; i < list.count(); i++)
    {
        CFStringRef ext = CFStringCreateWithCString(kCFAllocatorDefault, list[i].toUtf8(), kCFStringEncodingUTF8);
        CFStringRef tag = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, ext, nullptr);

        LSSetDefaultRoleHandlerForContentType(tag, role, bundleID);

        CFRelease(tag);
        CFRelease(ext);
    }
}
#endif

#ifdef Q_OS_LINUX
void FileAssociationInstaller::getExtentionToMime(const QString &ext, QString *ret)
{
    QMimeDatabase db;

    *ret = db.mimeTypeForFile("a." + ext, QMimeDatabase::MatchExtension).name();

    if (ret->startsWith("application"))
        ret->clear();
}

void FileAssociationInstaller::setFileAssociationInternal(const QString &filePath, const QString &header, const QStringList &extList, bool erase)
{
    QFile file(filePath);
    QString appName = "dcple-com-AnyVOD.desktop";
    QString codecName;
    TextEncodingDetector detector;
    bool exist = file.exists();

    if (!detector.getCodecName(filePath, &codecName))
        codecName = QTextCodec::codecForLocale()->name();

    if (file.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        QTextStream stream(&file);
        QStringList lines;
        QString line;
        QStringList toWrite;

        stream.setCodec(codecName.toLatin1());

        while (!(line = stream.readLine()).isNull())
        {
            line = line.trimmed();

            if (!line.isEmpty())
                lines.append(line);
        }

        if (erase)
        {
            for (const QString &test : lines)
            {
                if (!test.contains(appName))
                    toWrite.append(test);
            }
        }
        else
        {
            int index = -1;

            toWrite = lines;

            if (!exist || toWrite.isEmpty())
                toWrite.append(header);

            for (int i = 0; i < toWrite.count(); i++)
            {
                if (toWrite[i].contains(header, Qt::CaseInsensitive))
                {
                    index = i + 1;
                    break;
                }
            }

            if (index < 0)
            {
                toWrite.append(header);
                index = toWrite.count();
            }

            for (const QString &ext : extList)
            {
                QString mime;

                FileAssociationInstaller::getExtentionToMime(ext, &mime);

                if (!mime.isEmpty())
                    toWrite.insert(index, mime + "=" + appName + ";");
            }
        }

        file.close();

        if (file.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate))
        {
            stream.setDevice(&file);

            toWrite.removeDuplicates();

            for (const QString &test : qAsConst(toWrite))
                stream << test << Qt::endl;

            file.close();
        }
    }
}

void FileAssociationInstaller::setFileAssociation(const QStringList &extList, bool erase)
{
    QString base = QDir::homePath() + "/.local/share/applications/";

    QDir::root().mkpath(base);

    FileAssociationInstaller::setFileAssociationInternal(base + "defaults.list", "[Default Applications]", extList, erase);
    FileAssociationInstaller::setFileAssociationInternal(base + "mimeapps.list", "[Added Associations]", extList, erase);
}
#endif
