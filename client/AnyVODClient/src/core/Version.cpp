﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "Version.h"
#include "BuildNumber.h"
#include "Common.h"

const QString Version::VERSION_TEMPLATE = "%1.%2.%3.%4";
const QString Version::SPLITTER = " ";

Version::Version()
{
    this->m_version = this->getVersion();
}

Version& Version::withArchName()
{
    this->appendSplitter();
    this->m_version.append(ARCH_NAME);

    return *this;
}

Version& Version::withBuildType()
{
    this->appendSplitter();
    this->m_version.append(STRFILEVER);

    return *this;
}

QString Version::build()
{
    QString ret = this->m_version;

    this->m_version.clear();
    return ret;
}

QString Version::makeVersion(const VersionNumber &versionNumbers)
{
    return VERSION_TEMPLATE
            .arg(versionNumbers.major)
            .arg(versionNumbers.minor)
            .arg(versionNumbers.patch)
            .arg(versionNumbers.build);
}

QString Version::getVersion()
{
    return this->makeVersion(VersionNumber(MAJOR, MINOR, PATCH, BUILDNUM));
}

void Version::appendSplitter()
{
    this->m_version.append(SPLITTER);
}
