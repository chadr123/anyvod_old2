﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <stdint.h>

class QString;

extern const char ORG_NAME[];
extern const char APP_NAME[];

extern const char PROTOCOL_POSTFIX[];
extern const char COMMENT_PREFIX[];

extern const int TAB_SIZE;
extern const QString TABS;

extern const int DEFAULT_MIN_TEXTURE_SIZE;

extern const double MICRO_SECOND;

extern const int MAX_AUDIO_QUEUE_SIZE;
extern const int MAX_VIDEO_QUEUE_SIZE;
extern const int MAX_SUBTITLE_QUEUE_SIZE;

extern const char ICON_PATH[];

extern const unsigned long long KILO;
extern const unsigned long long MEGA;
extern const unsigned long long GIGA;

extern const unsigned long long KILO_10E3;
extern const unsigned long long MEGA_10E3;
extern const unsigned long long GIGA_10E3;

extern const QString ARCH_NAME;
extern const QString LIVE;
extern const QString NEW_LINE;

#ifndef IS_BIT_SET
# define IS_BIT_SET(src, flag) (((src)&(flag))==(flag))
#endif

#if defined Q_OS_MOBILE
# define GL_PREFIX this->m_gl->
#else
# define GL_PREFIX
#endif

#if defined Q_OS_ANDROID
# define ASSETS_PREFIX "assets:"
#else
# define ASSETS_PREFIX "."
#endif
