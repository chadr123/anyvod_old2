﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <qopengl.h>

#include <cstring>

#if defined Q_OS_RASPBERRY_PI
# define MAX_TEXTURE_COUNT 3
#else
# define MAX_TEXTURE_COUNT 1
#endif

#define MAX_PBO_COUNT 3

enum TextureID
{
    TEX_HEAD = -1,
    TEX_MOVIE_FRAME,
    TEX_FFMPEG_SUBTITLE,
    TEX_ASS_SUBTITLE,
    TEX_YUV_0,
    TEX_YUV_1,
    TEX_YUV_2,
    TEX_HDR_MOVIE_FRAME,
    TEX_COUNT
};

struct TextureInfo
{
    TextureInfo()
    {
        memset(id, 0, sizeof(id));
        memset(idPBO, 0, sizeof(idPBO));
        index = 0;
        indexPBO = 0;
        textureCount = 0;

        for (int i = 0; i < MAX_TEXTURE_COUNT; i++)
            init[i] = false;
    }

    GLuint id[MAX_TEXTURE_COUNT];
    GLuint idPBO[MAX_PBO_COUNT];
    unsigned int index;
    unsigned int indexPBO;
    unsigned int textureCount;
    bool init[MAX_TEXTURE_COUNT];
};
