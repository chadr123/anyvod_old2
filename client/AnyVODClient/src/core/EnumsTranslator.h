﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "AnyVODEnums.h"

#include <QCoreApplication>

class EnumsTranslator
{
    Q_DECLARE_TR_FUNCTIONS(EnumsTranslator)
private:
    EnumsTranslator();
    ~EnumsTranslator();

public:
    static EnumsTranslator& getInstance();

public:
    void reload();

    QString get3DMethodDesc(AnyVODEnums::Video3DMethod method) const;
    QString get3DMethodCategoryDesc(AnyVODEnums::Video3DMethod method) const;
    QString getSubtitle3DMethodDesc(AnyVODEnums::Subtitle3DMethod method) const;
    QString getAnaglyphAlgoritmDesc(AnyVODEnums::AnaglyphAlgorithm algorithm) const;
    QString getDeinterlaceDesc(AnyVODEnums::DeinterlaceAlgorithm algorithm) const;
    QString getUpdateGapDesc(AnyVODEnums::CheckUpdateGap gap) const;
    QString getVRInputSourceDesc(AnyVODEnums::VRInputSource source) const;
    QString getDistortionAdjustModeDesc(AnyVODEnums::DistortionAdjustMode mode) const;
    QString getProjectionTypeDesc(AnyVODEnums::Video360Type type) const;

private:
    void setupConstStrings();
    void setup3DMethod();
    void setup3DMethodCategory();
    void setupSubtitle3DMethod();
    void setupAnaglyphAlgorithm();
    void setupDeinterlaceMethod();
    void setupUpdateGap();
    void setupDistortionAdjustMode();
    void setupVRInputSource();
    void setupProjectionType();

private:
    QString m_3DMethodDesc[AnyVODEnums::V3M_COUNT];
    QString m_3DMethodCategory[AnyVODEnums::V3M_COUNT];
    QString m_subtitle3DMethodDesc[AnyVODEnums::S3M_COUNT];
    QString m_anaglyphAlgorithmDesc[AnyVODEnums::AGA_COUNT];
    QString m_deinterlaceDesc[AnyVODEnums::DA_COUNT];
    QString m_updateGapDesc[AnyVODEnums::CUG_COUNT];
    QString m_distortionAdjustModeDesc[AnyVODEnums::DAM_COUNT];
    QString m_vrInputSourceDesc[AnyVODEnums::VRI_COUNT];
    QString m_projectionTypeDesc[AnyVODEnums::V3T_COUNT];
};
