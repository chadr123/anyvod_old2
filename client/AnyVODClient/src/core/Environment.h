﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QString>

class Environment
{
private:
    Environment();

public:
    static const Environment& getInstance();

public:
    QString getFontFilePath() const;
    QString getFontFamily() const;
    QString getSkinFilePath() const;
    QString getShaderDirectoryPath() const;
    uint8_t getFontSize() const;
    uint8_t getSubtitleOutlineSize() const;

private:
    void load();
    bool parsePair(const QString &filePath, QMap<QString, QString> *ret);

private:
    QString m_fontFilePath;
    QString m_fontFamily;
    QString m_skinFilePath;
    QString m_shaderDirectoryPath;
    uint8_t m_fontSize;
    uint8_t m_subtitleOutlineSize;
};
