﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

extern "C"
{
# include <libavutil/mem.h>
}

struct AVFrame;

class USWCMemCopy
{
public:
    USWCMemCopy();
    ~USWCMemCopy();

    bool copyYUV(const AVFrame &src, AVFrame *ret, int height, int planes) const;

private:
    void copyPlane(const uint8_t *src, int srcLineSize, uint8_t *dest, int destLineSize, int height) const;
    inline void copyFromUSWC(const uint8_t *src, int srcLineSize, uint8_t *dest, int destLineSize, int height) const;
    inline void copyToMem(const uint8_t *src, int srcLineSize, uint8_t *dest, int destLineSize, int height) const;

private:
    static const int USWC_MAX_BUFFER_SIZE;

private:
    int m_cpu;
    uint8_t *m_buf;
};
