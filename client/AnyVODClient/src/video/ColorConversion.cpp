﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "ColorConversion.h"
#include "PrimaryMatrix.h"

const ColorConversion::Primaries ColorConversion::PRIMARIES_BT709 =
{
    {{0.640f, 0.330f}, {0.300f, 0.600f}, {0.150f, 0.060f}},
    {0.3127f, 0.3290f}
};

const ColorConversion::Primaries ColorConversion::PRIMARIES_BT2020 =
{
    {{0.708f, 0.292f}, {0.170f, 0.797f}, {0.131f, 0.046f}},
    {0.3127f, 0.3290f}
};

const ColorConversion::ConvYCbCr ColorConversion::CONV_BT709YCBCR = {0.2126f, 0.0722f};
const ColorConversion::ConvYCbCr ColorConversion::CONV_BT2020YCBCR = {0.2627f, 0.0593f};

ColorConversion::ColorConversion()
{

}

ColorConversion::Info ColorConversion::getColorConversionInfo(AVColorPrimaries src, AVColorPrimaries dst)
{
    Primaries primToRGB;
    Primaries primToXYZ;
    Info info;

    switch (src)
    {
        case AVCOL_PRI_BT709:
        {
            primToXYZ = PRIMARIES_BT709;
            info.gammaSrc = 2.2f;
            break;
        }
        case AVCOL_PRI_BT2020:
        {
            primToXYZ = PRIMARIES_BT2020;
            info.gammaSrc = 2.8f;
            break;
        }
        default:
        {
            primToXYZ = PRIMARIES_BT709;
            info.gammaSrc = 2.2f;
            break;
        }
    }

    switch (dst)
    {
        case AVCOL_PRI_BT709:
        {
            primToRGB = PRIMARIES_BT709;
            info.gammaDst = 2.2f;
            break;
        }
        case AVCOL_PRI_BT2020:
        {
            primToRGB = PRIMARIES_BT2020;
            info.gammaDst = 2.4f;
            break;
        }
        default:
        {
            primToRGB = PRIMARIES_BT709;
            info.gammaDst = 2.2f;
            break;
        }
    }

    PrimaryMatrix toXYZ(primToXYZ.primaries, primToXYZ.whitePoint);
    PrimaryMatrix toRGB(primToRGB.primaries, primToRGB.whitePoint);

    info.matPrimary = toRGB.invert() * toXYZ;

    return info;
}

QVector3D ColorConversion::getRGBYuvCoefs(AVColorSpace colspace)
{
    switch (colspace)
    {
        case AVCOL_SPC_BT2020_NCL:
        case AVCOL_SPC_BT2020_CL:
        {
            return QVector3D(CONV_BT2020YCBCR.kr, 1 - CONV_BT2020YCBCR.kr - CONV_BT2020YCBCR.kb, CONV_BT2020YCBCR.kb);
        }
        case AVCOL_SPC_BT709:
        default:
        {
            return QVector3D(CONV_BT709YCBCR.kr, 1 - CONV_BT709YCBCR.kr - CONV_BT709YCBCR.kb, CONV_BT709YCBCR.kb);
        }
    }
}

float ColorConversion::getTone(const FrameMetaData &frameMeta)
{
    float tone;

    if (frameMeta.hasCLM)
        tone = log10(100) / log10(frameMeta.clm.MaxCLL);
    else if (frameMeta.hasMDM && frameMeta.mdm.has_luminance)
        tone = log10(100) / log10(frameMeta.mdm.max_luminance.num / frameMeta.mdm.max_luminance.den);
    else
        tone = 0.7f;

    if (tone < 0.1f || tone > 5.0f)
        tone = 0.7f;

    return tone;
}
