﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QSize>
#include <QString>

class ScreenCaptureHelper
{
public:
    explicit ScreenCaptureHelper();

    void start();
    void stop();
    bool isStarted() const;

    bool isRemained() const;
    int getCapturedCount() const;
    void reduceRemainedCount();

    void setTotalCount(int count);
    int getTotalCount() const;

    void markPaused();
    void unmarkPaused();
    bool isPaused() const;

    void setCaptureByOriginalSize(bool on);
    bool isCaptureByOriginalSize() const;

    void setSavePath(const QString &path);
    QString getSavePath() const;

    void setExtension(const QString &ext);
    QString getExtention() const;

    void setSize(const QSize &size);
    QSize getSize() const;

    void setQuality(int quality);
    void resetToDefaultQuality();
    int getQuality() const;
    bool isDefaultQuality() const;

public:
    static QString getDefaultSavePath();

private:
    static const int DEFAULT_QUALITY_VALUE;

private:
    QString m_savePath;
    QString m_ext;
    bool m_capture;
    bool m_captureOrg;
    int m_captureCount;
    int m_totalCount;
    int m_quality;
    QSize m_captureSize;
    bool m_paused;
};
