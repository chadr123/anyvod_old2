﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "Camera.h"

#include <qmath.h>
#include <QMutexLocker>

Camera::Camera() :
    m_pos(0.0f, 0.0f, 0.0f),
    m_angles(M_PI_2, 0.0),
    m_adjustAngles(0.0, 0.0),
    m_lock(QMutex::Recursive)
{
    this->calMatrix();
}

Camera& Camera::getInstance()
{
    static Camera instance;

    return instance;
}

void Camera::lock()
{
    this->m_lock.lock();
}

void Camera::unlock()
{
    this->m_lock.unlock();
}

QPointF Camera::getAngles()
{
    QMutexLocker locker(&this->m_lock);

    return QPointF(qRadiansToDegrees(this->m_angles.x()), qRadiansToDegrees(this->m_angles.y()));
}

void Camera::setAngles(const QPointF &angles)
{
    QMutexLocker locker(&this->m_lock);

    this->m_angles.setX(qDegreesToRadians(angles.x()));
    this->m_angles.setY(qDegreesToRadians(angles.y()));

    this->calMatrix();
}

QMatrix4x4 Camera::getMatrix()
{
    QMutexLocker locker(&this->m_lock);

    return this->m_mat;
}

void Camera::moveLeftRight(qreal value)
{
    QMutexLocker locker(&this->m_lock);

    float angle = qAtan(value / this->m_direction.length());

    this->m_adjustAngles.setX(-angle);

    this->calMove(this->m_right, value);
}

void Camera::moveForwardBackward(qreal value)
{
    QMutexLocker locker(&this->m_lock);

    this->calMove(this->m_direction, value);
}

void Camera::resetAngles()
{
    QMutexLocker locker(&this->m_lock);

    this->m_angles = QPointF(M_PI_2, 0.0);
    this->calMatrix();
}

void Camera::resetPosition()
{
    QMutexLocker locker(&this->m_lock);

    this->m_pos = QVector3D(0.0f, 0.0f, 0.0f);
    this->m_adjustAngles = QPointF(0.0, 0.0);

    this->calMatrix();
}

QVector3D Camera::getPosition()
{
    QMutexLocker locker(&this->m_lock);

    return this->m_pos;
}

void Camera::calMatrix()
{
    QPointF angles = this->m_angles + this->m_adjustAngles;

    this->m_direction.setX(qCos(angles.y()) * qSin(angles.x()));
    this->m_direction.setY(qSin(angles.y()));
    this->m_direction.setZ(qCos(angles.y()) * qCos(angles.x()));

    this->m_right.setX(qSin(this->m_angles.x() - M_PI_2));
    this->m_right.setY(0.0f);
    this->m_right.setZ(qCos(this->m_angles.x() - M_PI_2));

    this->m_up = QVector3D::crossProduct(this->m_right, this->m_direction);

    this->m_mat.setToIdentity();
    this->m_mat.lookAt(this->m_direction, this->m_pos, this->m_up);
}

void Camera::calMove(const QVector3D &base, qreal value)
{
    QVector3D normal = base.normalized();

    normal *= value;
    this->m_pos += normal;

    this->calMatrix();
}
