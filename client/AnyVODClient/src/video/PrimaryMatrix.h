﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QMatrix3x3>

class PrimaryMatrix : public QMatrix3x3
{
public:
    PrimaryMatrix(const float primaries[3][2], const float whitePoint[2]);

    QMatrix3x3 invert() const;

private:
    float calcBy(const float p[3][2], const float w[2]) const;
    float calcGy(const float p[3][2], const float w[2], const float By) const;
    float calcRy(const float By, const float Gy) const;

    template <int order>
    float calculateDeterminant(float src[order][order]) const;

    template <int>
    float calculateDeterminant(float src[2][2]) const;

    template <int order>
    float calculateMinor(float src[order][order], int row, int col) const;

    template <int order>
    void getSubMatrix(float dest[order - 1][order - 1], float src[order][order], int row, int col) const;
};
