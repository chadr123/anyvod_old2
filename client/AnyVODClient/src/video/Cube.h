﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QVector>
#include <QHash>

#include "core/AnyVODEnums.h"

class QVector3D;
class QVector2D;

class Cube
{
private:
    Cube();

private:
    struct Transform
    {
        Transform()
        {
            rotate = -1;
        }

        Transform(int _rotate)
        {
            rotate = _rotate;
        }

        int rotate;
    };

    struct Profile
    {
        int rows;
        int cols;
        QString inputOrder;
        QString outputOrder;
        QHash<QString, Transform> transforms;
    };

public:
    static void init(AnyVODEnums::Video360Type type);
    static void deInit();

    static const QVector3D* getVertices();
    static const QVector2D* getTexCoords();
    static const QVector2D* getSBSTexCoords(bool left);
    static const QVector2D* getTABTexCoords(bool top);

    static const unsigned short* getIndices();
    static int getIndexCount();

private:
    static Profile getProfile(AnyVODEnums::Video360Type type);
    static int findFaceIndex(const QString &face, const QStringList &list);
    static QVector<QVector2D> rotate(const QVector<QVector2D> &coord, int angle);

private:
    static const QVector3D VERTICES[];

private:
    static QVector<unsigned short> m_indices;
    static QVector<QVector2D> m_texCoord;
    static QVector<QVector2D> m_texSBSCoordsLeft;
    static QVector<QVector2D> m_texSBSCoordsRight;
    static QVector<QVector2D> m_texTABCoordsTop;
    static QVector<QVector2D> m_texTABCoordsBottom;
};
