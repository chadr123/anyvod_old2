﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <QMutex>
#include <QVector>

extern "C"
{
# include <libavformat/avformat.h>
# include <libavfilter/avfilter.h>
}

class FilterGraph
{
public:
    FilterGraph();
    ~FilterGraph();

    bool setCodec(AVCodecContext *codec, AVPixelFormat pixFormat, AVPixelFormat screenFormat, const AVRational timeBase);
    bool getFrame(int width, int height, AVPixelFormat informat, const AVFrame &in,
                  AVFrame *out, AVPixelFormat *retFormat);
    bool hasFilters();
    int getDelayCount() const;

    void setEnableHistEQ(bool enable);
    bool getEnableHistEQ() const;

    void setEnableHighQuality3DDenoise(bool enable);
    bool getEnableHighQuality3DDenoise() const;

    void setEnableDeBand(bool enable);
    bool getEnableDeBand() const;

    void setEnableDeBlock(bool enable);
    bool getEnableDeBlock() const;

    void setEnableNormalize(bool enable);
    bool getEnableNormalize() const;

    void setEnableATADenoise(bool enable);
    bool getEnableATADenoise() const;

    void setEnableOWDenoise(bool enable);
    bool getEnableOWDenoise() const;

    void setEnableNLMeansDenoise(bool enable);
    bool getEnableNLMeansDenoise() const;

    void setEnableVagueDenoise(bool enable);
    bool getEnableVagueDenoise() const;

private:
    struct Filter
    {
        Filter() :
            filter(nullptr),
            enable(false)
        {

        }

        AVFilterContext *filter;
        bool enable;
    };

private:
    void unInitFilter();
    bool link();

private:
    AVFilterContext *m_in;
    Filter m_histeq;
    Filter m_hqdn3d;
    Filter m_atadenoise;
    Filter m_owdenoise;
    Filter m_nlmeansdenoise;
    Filter m_vaguedenoise;
    Filter m_deband;
    Filter m_deblock;
    Filter m_normalize;
    AVFilterContext *m_out;
    AVFilterGraph *m_graph;
    int m_delayCount;
    QMutex m_lock;
    QVector<AVFilterContext*> m_filters;
};
