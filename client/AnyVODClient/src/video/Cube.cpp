﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "Cube.h"

#include <QVector2D>
#include <QVector3D>
#include <QDebug>

const QVector3D Cube::VERTICES[] =
{
    QVector3D(1.0, -1.0, 1.0),
    QVector3D(-1.0, -1.0, 1.0),
    QVector3D(-1.0, 1.0, 1.0),
    QVector3D(1.0, 1.0, 1.0),

    QVector3D(-1.0, -1.0, -1.0),
    QVector3D(1.0, -1.0, -1.0),
    QVector3D(1.0, 1.0, -1.0),
    QVector3D(-1.0, 1.0, -1.0),

    QVector3D(1.0, 1.0, 1.0),
    QVector3D(-1.0, 1.0, 1.0),
    QVector3D(-1.0, 1.0, -1.0),
    QVector3D(1.0, 1.0, -1.0),

    QVector3D(1.0, -1.0, -1.0),
    QVector3D(-1.0, -1.0, -1.0),
    QVector3D(-1.0, -1.0, 1.0),
    QVector3D(1.0, -1.0, 1.0),

    QVector3D(1.0, -1.0, -1.0),
    QVector3D(1.0, -1.0, 1.0),
    QVector3D(1.0, 1.0, 1.0),
    QVector3D(1.0, 1.0, -1.0),

    QVector3D(-1.0, -1.0, 1.0),
    QVector3D(-1.0, -1.0, -1.0),
    QVector3D(-1.0, 1.0, -1.0),
    QVector3D(-1.0, 1.0, 1.0)
};

QVector<unsigned short> Cube::m_indices;
QVector<QVector2D> Cube::m_texCoord;
QVector<QVector2D> Cube::m_texSBSCoordsLeft;
QVector<QVector2D> Cube::m_texSBSCoordsRight;
QVector<QVector2D> Cube::m_texTABCoordsTop;
QVector<QVector2D> Cube::m_texTABCoordsBottom;

Cube::Cube()
{

}

void Cube::init(AnyVODEnums::Video360Type type)
{
    for (size_t i = 0; i < sizeof(VERTICES) / sizeof(QVector3D); i += 4)
    {
        Cube::m_indices.append(i);
        Cube::m_indices.append(i + 1);
        Cube::m_indices.append(i + 2);

        Cube::m_indices.append(i);
        Cube::m_indices.append(i + 2);
        Cube::m_indices.append(i + 3);
    }

    Profile profile = Cube::getProfile(type);
    int rows = profile.rows;
    int cols = profile.cols;
    QVector<QVector2D> texCoord;
    QStringList order = profile.inputOrder.split("", Qt::SkipEmptyParts);

    for (int r = rows - 1; r >= 0; r--)
    {
        for (int c = 0; c < cols; c++)
        {
            texCoord.append(QVector2D(c / (float)cols, r / (float)rows));
            texCoord.append(QVector2D((c + 1) / (float)cols, r / (float)rows));
            texCoord.append(QVector2D((c + 1) / (float)cols, (r + 1) / (float)rows));
            texCoord.append(QVector2D(c / (float)cols, (r + 1) / (float)rows));
        }
    }

    QStringList newOrder = profile.outputOrder.split("", Qt::SkipEmptyParts);

    for (const QString &newFace : qAsConst(newOrder))
    {
        int index = Cube::findFaceIndex(newFace, order);
        QVector<QVector2D> coord = texCoord.mid(index * 4, 4);

        if (profile.transforms.find(newFace) != profile.transforms.end())
        {
            Transform transform = profile.transforms.value(newFace);

            coord = Cube::rotate(coord, transform.rotate);
        }

        Cube::m_texCoord.append(coord);
    }

    for (const QVector2D &orgTex : qAsConst(Cube::m_texCoord))
    {
        Cube::m_texSBSCoordsLeft.append(QVector2D(orgTex.x() / 2.0f, orgTex.y()));
        Cube::m_texSBSCoordsRight.append(QVector2D((orgTex.x() + 1.0f) / 2.0f, orgTex.y()));

        Cube::m_texTABCoordsTop.append(QVector2D(orgTex.x(), orgTex.y() / 2.0f));
        Cube::m_texTABCoordsBottom.append(QVector2D(orgTex.x(), (orgTex.y() + 1.0f) / 2.0f));
    }
}

void Cube::deInit()
{
    Cube::m_indices.clear();
    Cube::m_texCoord.clear();
    Cube::m_texSBSCoordsLeft.clear();
    Cube::m_texSBSCoordsRight.clear();
    Cube::m_texTABCoordsTop.clear();
    Cube::m_texTABCoordsBottom.clear();
}

const QVector3D *Cube::getVertices()
{
    return VERTICES;
}

const QVector2D *Cube::getTexCoords()
{
    return Cube::m_texCoord.data();
}

const QVector2D *Cube::getSBSTexCoords(bool left)
{
    return left ? Cube::m_texSBSCoordsLeft.data() : Cube::m_texSBSCoordsRight.data();
}

const QVector2D *Cube::getTABTexCoords(bool top)
{
    return top ? Cube::m_texTABCoordsTop.data() : Cube::m_texTABCoordsBottom.data();
}

const unsigned short *Cube::getIndices()
{
    return Cube::m_indices.data();
}

int Cube::getIndexCount()
{
    return Cube::m_indices.count();
}

Cube::Profile Cube::getProfile(AnyVODEnums::Video360Type type)
{
    Profile profile;

    switch (type)
    {
        case AnyVODEnums::V3T_CUBEMAP_2X3:
        {
            profile.rows = 2;
            profile.cols = 3;
            profile.inputOrder = "RLUDFB";
            profile.outputOrder = "LRUDBF";

            profile.transforms.insert("U", Transform(270));
            profile.transforms.insert("D", Transform(90));

            break;
        }
        case AnyVODEnums::V3T_EAC_2X3:
        {
            profile.rows = 2;
            profile.cols = 3;
            profile.inputOrder = "DBULFR";
            profile.outputOrder = "DURLFB";

            profile.transforms.insert("L", Transform(180));
            profile.transforms.insert("F", Transform(270));

            break;
        }
        case AnyVODEnums::V3T_EAC_3X2:
        {
            profile.rows = 3;
            profile.cols = 2;
            profile.inputOrder = "LDFBRU";
            profile.outputOrder = "RLDUBF";

            profile.transforms.insert("R", Transform(90));
            profile.transforms.insert("L", Transform(90));
            profile.transforms.insert("U", Transform(270));
            profile.transforms.insert("D", Transform(90));
            profile.transforms.insert("F", Transform(90));

            break;
        }
        default:
        {
            break;
        }
    }

    return profile;
}

int Cube::findFaceIndex(const QString &face, const QStringList &list)
{
    for (int i = 0; i < list.count(); i++)
    {
        if (list[i] == face)
            return i;
    }

    return -1;
}

QVector<QVector2D> Cube::rotate(const QVector<QVector2D> &coord, int angle)
{
    int count = (angle / 90) % 4;

    if (count == 0)
        return coord;

    QVector<QVector2D> rotated = coord;

    for (int i = 0; i < count; i++)
    {
        if (count > 0)
        {
            rotated.append(rotated.first());
            rotated.removeFirst();
        }
        else
        {
            rotated.prepend(rotated.last());
            rotated.removeLast();
        }
    }

    return rotated;
}
