﻿// entry emboss

void emboss(inout vec4 texel, inout vec2 coord)
{
  float dx = 1.0 / AnyVOD_Width;
  float dy = 1.0 / AnyVOD_Height;

  vec4 t1 = AnyVOD_getTexel(coord + vec2(-dx, -dy));
  vec4 t2 = AnyVOD_getTexel(coord + vec2(0.0, -dy));
  vec4 t3 = AnyVOD_getTexel(coord + vec2(-dx, 0.0));
  vec4 t4 = AnyVOD_getTexel(coord + vec2(dx, 0.0));
  vec4 t5 = AnyVOD_getTexel(coord + vec2(0.0, dy));
  vec4 t6 = AnyVOD_getTexel(coord + vec2(dx, dy));

  texel = (-t1 - t2 - t3 + t4 + t5 + t6);

  float value = (texel.r + texel.g + texel.b) / 3.0 + 0.5;

  texel = vec4(value, value, value, 1.0);
}
