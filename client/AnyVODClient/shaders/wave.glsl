﻿// entry wave

void wave(inout vec4 texel, inout vec2 coord)
{
  vec4 color = vec4(0.0, 0.0, 0.0, 1.0);

  coord.s += sin(coord.s + AnyVOD_Clock / 0.3) / 20.0;
  coord.t += sin(coord.s + AnyVOD_Clock / 0.3) / 20.0;

  if (coord.s >= 0.0 && coord.s <= 1.0 && coord.t >= 0.0 && coord.t <= 1.0)
  {
    color = AnyVOD_getTexel(coord);
  }

  texel =  color;
}
