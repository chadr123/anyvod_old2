#!/bin/bash

. ./installer/common/functions.sh

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 TEAM ID(signing)"
  exit 1
fi

cd ./installer/ios
check

./make.sh $1
check

exit 0
