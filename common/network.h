﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include <stdint.h>

#if defined __WIN32__ || defined _WIN32 || defined Q_OS_WIN
# include <winsock.h>
#else
# include <netinet/in.h>
#endif

const unsigned short DEFAULT_NETWORK_PORT = 21238;
const unsigned short DEFAULT_NETWORK_STREAM_PORT = 21239;
const wchar_t DEFAULT_SERVER_ADDRESS[] = L"127.0.0.1";
const wchar_t DEFAULT_SERVER_ADDRESS6[] = L"::1";

#ifndef htonll
inline static uint64_t htonll(uint64_t x)
{
  return (((uint64_t)htonl((u_long)x)) << 32) + htonl(x >> 32);

}
#endif

#ifndef ntohll
inline static uint64_t ntohll(uint64_t x)
{
  return (((uint64_t)ntohl((u_long)x)) << 32) + ntohl(x >> 32);

}
#endif

