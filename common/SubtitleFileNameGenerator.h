﻿/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

class SubtitleFileNameGenerator
{
public:
    SubtitleFileNameGenerator();

    void getFileNames(const wstring &src, vector_wstring *ret);
    void getFileBaseName(const wstring &fullPath, bool withExt, wstring *retFileName);
    void getFileNamesWithExt(const wstring &src, vector_wstring *ret);
    void getFileNamesWithExtNoBaseName(const wstring &src, vector_wstring *ret);

private:
    void getFileNamesSub(const wstring &fileBaseName, vector_wstring *ret);
    void getFileNameByExt(const wstring &src, const wstring &ext, vector_wstring *ret);
    void getFileNameBySimilar(const wstring &fileBaseName, const wstring &ext, const wchar_t sep, vector_wstring *ret);
    void splitString(const wstring &src, const wchar_t delem, vector_wstring *ret);

private:
  vector_wchar m_sepList;
  vector_wstring m_appended;
  vector_wstring m_extList;
};
