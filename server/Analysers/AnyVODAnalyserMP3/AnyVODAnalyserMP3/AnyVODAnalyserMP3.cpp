/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// AnyVODAnalyserMP3.cpp : DLL 응용 프로그램을 위해 내보낸 함수를 정의합니다.
//

#include "stdafx.h"
#include "AnyVODAnalyserMP3.h"
#include "..\..\..\..\common\types.h"
#include "..\..\..\..\common\network.h"

const uint8_t AnyVODAnalyserMP3::MP3_SIG[3] = {'I', 'D', '3'};

AnyVODAnalyserMP3::AnyVODAnalyserMP3(HMODULE handle)
:Analyser(handle)
{

}

AnyVODAnalyserMP3::~AnyVODAnalyserMP3()
{

}

void
AnyVODAnalyserMP3::GetName(wchar_t ret[MAX_ANALYSER_NAME_SIZE])
{
  wcsncpy_s(ret, MAX_ANALYSER_NAME_SIZE, L"mpeg audio", MAX_ANALYSER_NAME_CHAR_SIZE);

}

void
AnyVODAnalyserMP3::GetDescription(wchar_t ret[MAX_ANALYSER_DESC_SIZE])
{
  wcsncpy_s(ret, MAX_ANALYSER_DESC_SIZE, L"MP3 Analyser", MAX_ANALYSER_DESC_CHAR_SIZE);

}

int64_t AnyVODAnalyserMP3::FindPattern(HANDLE file, const uint8_t *pattern, size_t size)
{
  if (size == 0)
    return -1;

  while (true)
  {
    uint8_t bytes[4096 * 2];
    DWORD read;

    if (!ReadFile(file, bytes, sizeof(bytes), &read, nullptr) || read == 0)
      break;

    for (size_t i = 0; i < read - size; i++)
    {
      uint8_t *cur = &bytes[i];

      if (memcmp(pattern, cur, size) != 0)
        continue;

      size_t offset = read - (i + size);

      return SetFilePointer(file, (LONG)(-1 * offset), nullptr, FILE_CURRENT);

    }

  }

  return -1;

}

bool AnyVODAnalyserMP3::ReadAudioHeaderMP3(HANDLE file, uint32_t *header, int32_t *read, bool *sig)
{
  uint16_t sync;
  DWORD readed;

  *header = 0;
  *read = 0;
  *sig = false;

  if (!ReadFile(file, &sync, 2, &readed, nullptr) || readed != 2)
    return false;

  *read += 2;
  sync = ntohs(sync);

  if ((sync & 0xffe0) == 0xffe0)
  {
    if (!ReadFile(file, header, 2, &readed, nullptr) || readed != 2)
      return false;

    *read += 2;

    *header = ntohs(*header);
    *header = ((uint32_t)sync << 16) | *header;

    *sig = true;

  }
  else
  {
    *sig = false;

  }

  return true;

}

bool AnyVODAnalyserMP3::FindDataMP3(HANDLE file)
{
  do
  {
    DWORD cur = SetFilePointer(file, 0, nullptr, FILE_CURRENT);
    DWORD next = cur + 1;
    uint32_t header = 0;
    int32_t read = 0;
    bool sig = false;

    if (!this->ReadAudioHeaderMP3(file, &header, &read, &sig))
      return false;

    if (sig)
    {
      int32_t frameLen = this->CalcFramePosMP3(header);

      if (frameLen < 0)
      {
        SetFilePointer(file, next, nullptr, FILE_BEGIN);

        continue;

      }

      SetFilePointer(file, frameLen - read, nullptr, FILE_CURRENT);

      if (!this->ReadAudioHeaderMP3(file, &header, &read, &sig))
        return false;

      if (sig)
      {
        if (this->CalcFramePosMP3(header) >= 0)
        {
          SetFilePointer(file, cur, nullptr, FILE_BEGIN);

          return true;

        }

      }

      SetFilePointer(file, next, nullptr, FILE_BEGIN);

    }
    else
    {
      SetFilePointer(file, -1, nullptr, FILE_CURRENT);

    }

  }while (true);

  return false;

}

int32_t AnyVODAnalyserMP3::CalcFramePosMP3(uint32_t header)
{
  int version = (header & 0x00180000) >> 19;
  int layer = (header & 0x00060000) >> 17;
  int bitRate = (header & 0xf000) >> 12;
  int sampleRate = (header & 0x0c00) >> 10;
  bool padding = (header & 0x0200) >> 9 ? true : false;
  const int sampleRateTable[][4] =
  {
    {22050, 24000, 16000, 0},
    {44100, 48000, 32000, 0}
  };

  const int bitRateTable[][16] =
  {
    {0, 32000, 40000, 48000, 56000, 64000, 80000, 96000, 112000, 128000, 160000, 192000, 224000, 256000, 320000, 0},
    {0,  8000, 16000, 24000, 32000, 40000, 48000, 56000,  64000,  80000,  96000, 112000, 128000, 144000, 160000, 0}
  };

  if (version != 2 && version != 3) // MPEG vesion 1 && 2
    return -1;

  if (layer != 1) // Layer 3
    return -1;

  if (bitRate == 0 || bitRate == 15)
    return -1;

  if (sampleRate == 3)
    return -1;

  int paddingSize = padding ? 1 : 0;
  int bitRateSelector = version == 2 ? 1 : 0;

  return 144 * bitRateTable[bitRateSelector][bitRate] / sampleRateTable[version - 2][sampleRate] + paddingSize;
}

bool AnyVODAnalyserMP3::ParseMP3(HANDLE file, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK *ret)
{
  uint16_t version;
  uint32_t id3Size;
  uint8_t flag;
  DWORD read;

  do
  {
    if (!ReadFile(file, &version, 2, &read, nullptr) || read != 2)
      return false;

    if (!ReadFile(file, &flag, 1, &read, nullptr) || read != 1)
      return false;

    if (!ReadFile(file, &id3Size, 4, &read, nullptr) || read != 4)
      return false;

    id3Size = ntohl(id3Size);
    id3Size = this->UnSynchSafe(id3Size);

    SetFilePointer(file, id3Size, nullptr, FILE_CURRENT);

    uint8_t header[sizeof(MP3_SIG)] = {0, };

    if (!ReadFile(file, header, sizeof(header), &read, nullptr) || read != sizeof(header))
      return false;

    if (memcmp(MP3_SIG, header, sizeof(header)) != 0)
      break;

  }while(true);

  SetFilePointer(file, -1 * (long)sizeof(MP3_SIG), nullptr, FILE_CURRENT);

  ANYVOD_ANALYSED_DATA_BLOCK block;

  if (this->FindDataMP3(file))
  {
    block.blockOffset = 0;
    block.size = SetFilePointer(file, 0, nullptr, FILE_CURRENT);

    ret->push_back(block);

  }

  this->FindFooter(file, ret);

  return true;

}

void AnyVODAnalyserMP3::FindFooter(HANDLE file, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK *ret)
{
  ANYVOD_ANALYSED_DATA_BLOCK block;
  uint8_t lyricsHeader[] = {'L', 'Y', 'R', 'I', 'C', 'S', 'B', 'E', 'G', 'I', 'N'};

  block.blockOffset = this->FindPattern(file, lyricsHeader, sizeof(lyricsHeader)) - sizeof(lyricsHeader);

  if (block.blockOffset > 0)
  {
    uint8_t lyricsFooter[] = {'L', 'Y', 'R', 'I', 'C', 'S', '2', '0', '0'};

    block.size = this->FindPattern(file, lyricsFooter, sizeof(lyricsFooter)) - block.blockOffset;

    if (block.size > 0)
      ret->push_back(block);

  }

}

uint32_t AnyVODAnalyserMP3::UnSynchSafe(uint32_t synchsafe)
{
  uint32_t ret = 0;
  uint32_t mask = 0x7f000000;

  while (mask)
  {
    ret >>= 1;
    ret |= synchsafe & mask;
    mask >>= 8;
  }

  return ret;

}

bool
AnyVODAnalyserMP3::Analyse(const wstring &path, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK *ret)
{
  HANDLE file = CreateFileW(path.c_str(), GENERIC_READ,
    FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
  FILE_CLOSER closer(file);

  if (file != INVALID_HANDLE_VALUE)
  {
    uint8_t tmp[sizeof(MP3_SIG)];
    DWORD read;

    if (!ReadFile(file, &tmp, sizeof(tmp), &read, nullptr) || read != sizeof(tmp))
      return false;

    if (memcmp(tmp, MP3_SIG, read) != 0)
    {
      SetFilePointer(file, 0, nullptr, FILE_BEGIN);
      this->FindFooter(file, ret);

      return true;

    }
    else
    {
      return this->ParseMP3(file, ret);

    }

  }
  else
  {
    return false;

  }

  return true;

}
