/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "..\..\..\..\common\Analyser.h"
#include <stdint.h>

class AnyVODAnalyserMP3 : public Analyser
{
public:
  AnyVODAnalyserMP3(HMODULE handle);
  virtual ~AnyVODAnalyserMP3();

  virtual void GetName(wchar_t ret[MAX_ANALYSER_NAME_SIZE]);
  virtual void GetDescription(wchar_t ret[MAX_ANALYSER_DESC_SIZE]);
  virtual bool Analyse(const wstring &path, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK *ret);

private:
  void FindFooter(HANDLE file, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK *ret);
  int64_t FindPattern(HANDLE file, const uint8_t *pattern, size_t size);
  uint32_t UnSynchSafe(uint32_t synchsafe);
  bool FindDataMP3(HANDLE file);
  int32_t CalcFramePosMP3(uint32_t header);
  bool ReadAudioHeaderMP3(HANDLE file, uint32_t *header, int32_t *read, bool *sig);
  bool ParseMP3(HANDLE file, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK *ret);

private:
  static const uint8_t MP3_SIG[3];

};
