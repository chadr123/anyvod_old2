/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// AnyVODAnalyserMP4.cpp : DLL 응용 프로그램을 위해 내보낸 함수를 정의합니다.
//

#include "stdafx.h"
#include "AnyVODAnalyserMP4.h"
#include "..\..\..\..\common\types.h"
#include "..\..\..\..\common\network.h"

AnyVODAnalyserMP4::AnyVODAnalyserMP4(HMODULE handle)
:Analyser(handle)
{

}

AnyVODAnalyserMP4::~AnyVODAnalyserMP4()
{

}

void
AnyVODAnalyserMP4::GetName(wchar_t ret[MAX_ANALYSER_NAME_SIZE])
{
  wcsncpy_s(ret, MAX_ANALYSER_NAME_SIZE, L"mpeg-4", MAX_ANALYSER_NAME_CHAR_SIZE);

}

void
AnyVODAnalyserMP4::GetDescription(wchar_t ret[MAX_ANALYSER_DESC_SIZE])
{
  wcsncpy_s(ret, MAX_ANALYSER_DESC_SIZE, L"MP4 Analyser", MAX_ANALYSER_DESC_CHAR_SIZE);

}

bool
AnyVODAnalyserMP4::GetAtomHeader(HANDLE file, LARGE_INTEGER start, UINT64 *size, char type[5], bool *isExtended)
{
  if (!SetFilePointerEx(file, start, nullptr, FILE_BEGIN))
    return false;

  UINT32 atomSize;
  DWORD read;

  if (!ReadFile(file, &atomSize, sizeof(atomSize), &read, nullptr))
    return false;

  if (read != sizeof(atomSize))
    return false;

  atomSize = ntohl(atomSize);

  if (!ReadFile(file, type, 4, &read, nullptr))
    return false;

  if (read != 4)
    return false;

  *isExtended = false;

  if (atomSize == 0)//파일 끝을 알리는 맨 마지막 atom
  {
    LARGE_INTEGER totalSize;

    if (!GetFileSizeEx(file, &totalSize))
      return false;

    *size = totalSize.QuadPart - start.QuadPart;

  }
  else if (atomSize == 1)
  {
    UINT64 extendedSize;

    if (!ReadFile(file, &extendedSize, sizeof(extendedSize), &read, nullptr))
      return false;

    if (read != sizeof(extendedSize))
      return false;

    extendedSize = ntohll(extendedSize);
    *size = extendedSize;

    *isExtended = true;
  }
  else
  {
    *size = atomSize;

  }

  return true;

}

bool
AnyVODAnalyserMP4::FindAtom(HANDLE file, LARGE_INTEGER start, INT64 end, const vector_string &types, LARGE_INTEGER *nextStart, UINT64 *atomStart, UINT64 *atomSize)
{
  char byte4[5] = {0, };
  bool isExtended = false;

  while (true)
  {
    if (end != 0ll && start.QuadPart >= end)
    {
      nextStart->QuadPart = start.QuadPart;

      break;

    }

    if (this->GetAtomHeader(file, start, atomSize, byte4, &isExtended))
    {
      for (size_t i = 0; i < types.size(); i++)
      {
        if (strcmp(byte4, types[i].c_str()) == 0)
        {
          nextStart->QuadPart = start.QuadPart;
          nextStart->QuadPart += sizeof(UINT32) * 2;

          if (isExtended)
            nextStart->QuadPart += sizeof(UINT64);

          *atomStart = start.QuadPart;

          return true;

        }

      }

    }
    else
    {
      return false;

    }

    start.QuadPart += *atomSize;

  }

  return false;

}

void
AnyVODAnalyserMP4::AddMetaData(HANDLE file, LARGE_INTEGER start, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK *ret)
{
  LARGE_INTEGER nextStart;
  UINT64 atomStart;
  UINT64 atomSize;
  vector_string types;

  types.push_back("moov");
  types.push_back("moof");
  types.push_back("mfra");
  types.push_back("free");
  types.push_back("skip");
  types.push_back("pnot");

  while (this->FindAtom(file, start, 0, types, &nextStart, &atomStart, &atomSize))
  {
    ANYVOD_ANALYSED_DATA_BLOCK block;

    block.blockOffset = atomStart;
    block.size = atomSize;

    ret->push_back(block);

    start.QuadPart = atomStart + atomSize;

  }

}

bool
AnyVODAnalyserMP4::Analyse(const wstring &path, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK *ret)
{
  HANDLE file = CreateFileW(path.c_str(), GENERIC_READ,
    FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
  FILE_CLOSER closer(file);
  ANYVOD_ANALYSED_DATA_BLOCK block;

  if (file != INVALID_HANDLE_VALUE)
  {
    LARGE_INTEGER start;
    LARGE_INTEGER nextStart;
    UINT64 atomStart;
    UINT64 atomSize;
    vector_string types;

    start.QuadPart = 0ll;
    types.push_back("ftyp");

    if (this->FindAtom(file, start, 0, types, &nextStart, &atomStart, &atomSize))
      start.QuadPart += atomSize;
    else
      return false;

    this->AddMetaData(file, start, ret);

  }
  else
  {
    return false;

  }

  return true;

}
