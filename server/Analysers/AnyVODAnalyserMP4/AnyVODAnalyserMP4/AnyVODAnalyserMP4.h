/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "..\..\..\..\common\Analyser.h"

class AnyVODAnalyserMP4 : public Analyser
{
public:
  AnyVODAnalyserMP4(HMODULE handle);
  virtual ~AnyVODAnalyserMP4();

  virtual void GetName(wchar_t ret[MAX_ANALYSER_NAME_SIZE]);
  virtual void GetDescription(wchar_t ret[MAX_ANALYSER_DESC_SIZE]);
  virtual bool Analyse(const wstring &path, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK *ret);

private:
  bool GetAtomHeader(HANDLE file, LARGE_INTEGER start, UINT64 *size, char type[5], bool *isExtended);
  bool FindAtom(HANDLE file, LARGE_INTEGER start, INT64 end, const vector_string &types, LARGE_INTEGER *nextStart, UINT64 *atomStart, UINT64 *atomSize);
  void AddMetaData(HANDLE file, LARGE_INTEGER start, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK *ret);

};
