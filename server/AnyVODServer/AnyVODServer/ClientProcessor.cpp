/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "stdafx.h"
#include "ClientProcessor.h"
#include "PacketHandler.h"
#include "UserPacketHandler.h"
#include "Log.h"
#include "Util.h"

ClientProcessor::ClientProcessor()
{
  InitLock(&this->m_clientLocker, MAX_SPIN_COUNT);

}

ClientProcessor::~ClientProcessor()
{
  DeleteLock(&this->m_clientLocker);

}

void
ClientProcessor::AddClient(SOCKET socket, SOCKADDR_STORAGE *clientAddr, Server *server, HANDLE completionPort, bool isStream)
{
  Client *client = new Client(socket, clientAddr, server, isStream);

  if (CreateIoCompletionPort((HANDLE)socket, completionPort, (ULONG_PTR)client, 0) == NULL)
  {
    Log::GetInstance().Write(L"Cannot associate with completion port : \"%s\"",
      Util::SystemErrorToString(GetLastError()).c_str());

    delete client;

  }

  WriteLock(&this->m_clientLocker);

  this->m_clients.push_back(client);

  if (!client->Init())
  {
    Log::GetInstance().Write(L"Cannot enter to recv mode : \"%s\"",
      Util::SystemErrorToString(WSAGetLastError()).c_str());

    this->m_clients.pop_back();

    delete client;

  }

  WriteUnlock(&this->m_clientLocker);

}

void
ClientProcessor::Clear()
{
  WriteLock(&this->m_clientLocker);

  LIST_CLIENT_ITERATOR i = this->m_clients.begin();

  while (i != this->m_clients.end())
  {
    Client *client = *i;

    if (client == nullptr)
      continue;

    if (TryEnterCriticalSection(client->GetLocker()))
    {
      client->SetForceClose();

      if (client->GetIORefCount() == 0)
      {
        LeaveCriticalSection(client->GetLocker());

        delete client;

        i = this->m_clients.erase(i);

      }
      else
      {
        wcout << L">>  " << Log::GetInstance().Write(L"Exist pending I/O of client %s:%hu", Util::AddressToString(client->GetSockAddr()).c_str(), Util::PortFromAddress(client->GetSockAddr())) << endl;
        wcout << Log::GetInstance().Write(L"Send buffer count : %d, Recv buffer count : %d", (int)client->GetSendBufferCount(), (int)client->GetRecvBufferCount()) << endl;

        LeaveCriticalSection(client->GetLocker());

        WriteUnlock(&this->m_clientLocker);

        SleepEx(100, FALSE);

        WriteLock(&this->m_clientLocker);
        i = this->m_clients.begin();

      }

    }

  }

  this->m_clients.clear();

  WriteUnlock(&this->m_clientLocker);

}

void
ClientProcessor::LogoutUser(const wstring &id)
{
  ReadLock(&this->m_clientLocker);

  LIST_CLIENT_ITERATOR i = this->m_clients.begin();

  while (i != this->m_clients.end())
  {
    Client *client = *i;

    EnterCriticalSection(client->GetLocker());

    if (client->GetUserInfo().id == id)
    {
      PacketHandler parent(client);
      UserPacketHandler handler(client, &parent);

      handler.SendLogout(true);

      client->SetToBeClosed();
      client->Shutdown();

    }

    i++;

    LeaveCriticalSection(client->GetLocker());

  }

  ReadUnlock(&this->m_clientLocker);

}

bool
ClientProcessor::LogoutClient(const wstring &ip, unsigned short port)
{
  bool found = false;

  ReadLock(&this->m_clientLocker);

  LIST_CLIENT_ITERATOR i = this->m_clients.begin();

  while (i != this->m_clients.end())
  {
    Client *client = *i;
    wstring socketIP;

    EnterCriticalSection(client->GetLocker());

    socketIP = Util::AddressToString(client->GetSockAddr());

    if (socketIP == ip && Util::PortFromAddress(client->GetSockAddr()) == port)
    {
      PacketHandler parent(client);
      UserPacketHandler handler(client, &parent);

      handler.SendLogout(true);

      client->SetToBeClosed();
      client->Shutdown();

      found = true;

      LeaveCriticalSection(client->GetLocker());

      break;

    }

    i++;

    LeaveCriticalSection(client->GetLocker());

  }

  ReadUnlock(&this->m_clientLocker);

  return found;

}

bool
ClientProcessor::ProcessClientTasks()
{
  bool remain = false;

  ReadLock(&this->m_clientLocker);

  LIST_CLIENT_ITERATOR i = this->m_clients.begin();

  for (;i != this->m_clients.end(); i++)
  {
    Client *client = *i;

    if (TryEnterCriticalSection(client->GetLocker()))
    {
      if (!(client->IsToBeClose() || client->IsForceClose()))
      {
        remain |= client->ProcessOwnTasks();

      }

      LeaveCriticalSection(client->GetLocker());

    }

  }

  ReadUnlock(&this->m_clientLocker);

  return remain;

}

void
ClientProcessor::CollectDeadClient()
{
  WriteLock(&this->m_clientLocker);

  LIST_CLIENT_ITERATOR i = this->m_clients.begin();

  while (i != this->m_clients.end())
  {
    Client *client = *i;

    if (client == nullptr)
      continue;

    if (TryEnterCriticalSection(client->GetLocker()))
    {
      if (client->GetIORefCount() == 0 && client->IsToBeClose())
      {
        client->Shutdown();

        LeaveCriticalSection(client->GetLocker());

        delete client;

        i = this->m_clients.erase(i);

      }
      else
      {
        i++;

        LeaveCriticalSection(client->GetLocker());

      }

    }
    else
    {
      i++;

    }

  }

  WriteUnlock(&this->m_clientLocker);

}

bool
ClientProcessor::FindUserByTicket(const string &ticket, Client::USER_INFO *userInfo)
{
  bool found = false;

  ReadLock(&this->m_clientLocker);

  LIST_CLIENT_ITERATOR i = this->m_clients.begin();

  for (; i != this->m_clients.end(); i++)
  {
    Client *client = *i;
    const Client::USER_INFO &info = client->GetUserInfo();

    if (info.ticket == ticket)
    {
      *userInfo = info;
      found = true;

      break;
    }
  }

  ReadUnlock(&this->m_clientLocker);

  return found;
}

void
ClientProcessor::GetClientStatus(VECTOR_ANYVOD_CLIENT_STATUS *ret)
{
  ReadLock(&this->m_clientLocker);

  LIST_CLIENT_ITERATOR i = this->m_clients.begin();

  for (; i != this->m_clients.end(); i++)
  {
    ANYVOD_CLIENT_STATUS status;
    Client *client = *i;

    status.ip = Util::AddressToString(client->GetSockAddr());
    status.port = Util::PortFromAddress(client->GetSockAddr());
    status.totalRecvBytes = client->GetTotalRecvBytes();
    status.totalSentBytes = client->GetTotalSentBytes();
    status.recvBufCount = client->GetRecvBufferCount();
    status.sendBufCount = client->GetSendBufferCount();
    status.recvBytesPerSec = client->GetRecvBytesPerSec();
    status.sentBytesPerSec = client->GetSentBytesPerSec();
    status.id = client->GetUserInfo().id;
    status.lastPacketType = client->GetLastPacketType();
    status.clientState = client->GetState();
    status.isStream = client->IsStream();
    status.ticket = client->GetUserInfo().ticket;

    ret->push_back(status);

  }

  ReadUnlock(&this->m_clientLocker);

}
