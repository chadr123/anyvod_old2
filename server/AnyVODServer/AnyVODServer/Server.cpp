/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "stdafx.h"
#include "Server.h"
#include "Worker.h"
#include "Util.h"
#include "Log.h"
#include "BuildNumber.h"

Server::Server()
:m_completionPort(NULL), m_listenSocket(INVALID_SOCKET), m_listenStreamSocket(INVALID_SOCKET)
{
  Util::SplitString(AUDIO_EXTENSION, EXTENSION_DELIMITER, &this->m_audioExts);

}

Server::~Server()
{

}

bool
Server::Start()
{
  wcout << L">> Initializing server" << endl;

  if (!this->ReadEnv())
  {
    wcout << L">> Cannot read env file : " << INIT_FILE_NAME << L" Stopping server" << endl;

    this->Stop();

    return false;

  }

  wstring logPath = this->m_env.log_dir;
  wstring time;

  Log::GetNewLogDate(&time);

  Util::AppendDirSeparator(&logPath);

  logPath.append(LOG_FILENAME_PREFIX);
  logPath.append(time);
  logPath.append(LOG_FILENAME_EXTENTION);

  if (!Log::GetInstance().StartLog(logPath))
  {
    wcout << L">> Cannot open log file : " << logPath << endl;

    this->Stop();

    return false;

  }

  wcout << L">> " << Log::GetInstance().Write(L"Starting server") << endl;

  if (!this->CheckMovieDir())
  {
    Log::GetInstance().Write(L"Invalid movie dir : \"%s\" Path : %s",
      Util::SystemErrorToString(GetLastError()).c_str(), this->m_env.movie_dir.c_str());

    this->Stop();

    return false;

  }

  if (!this->m_analyserLoader.LoadAnalysers(this->m_env.analyser_dir))
  {
    Log::GetInstance().Write(L"Cannot load analysers : %s Stopping server", this->m_env.analyser_dir.c_str());

    this->Stop();

    return false;

  }

  wstring dbPath = this->m_env.db_dir;

  Util::AppendDirSeparator(&dbPath);

  dbPath.append(SQLITE3_FILENAME);

  if (!this->m_db.Open(dbPath, this))
  {
    Log::GetInstance().Write(L"Cannot open DB : %s Stopping server", dbPath.c_str());

    this->Stop();

    return false;

  }

  if (WSAStartup(MAKEWORD(2,2), &this->m_wsd) != 0)
  {
    Log::GetInstance().Write(L"Unable to load winsock : \"%s\" Stopping server",
      Util::SystemErrorToString(WSAGetLastError()).c_str());

    return false;

  }

  int af;

  if (this->m_env.use_ipv6)
    af = AF_INET6;
  else
    af = AF_INET;

  this->m_listenSocket = WSASocketW(af, SOCK_STREAM, IPPROTO_TCP, nullptr, 0, WSA_FLAG_OVERLAPPED);
  this->m_listenStreamSocket = WSASocketW(af, SOCK_STREAM, IPPROTO_TCP, nullptr, 0, WSA_FLAG_OVERLAPPED);

  if (this->m_listenSocket == INVALID_SOCKET || this->m_listenStreamSocket == INVALID_SOCKET)
  {
    Log::GetInstance().Write(L"Socket failed : \"%s\" Stopping server",
      Util::SystemErrorToString(WSAGetLastError()).c_str());

    this->Stop();

    return false;

  }

  BOOL on = TRUE;

  if (setsockopt(this->m_listenSocket, SOL_SOCKET, SO_REUSEADDR, (char*)&on, sizeof(on)) == SOCKET_ERROR ||
    setsockopt(this->m_listenStreamSocket, SOL_SOCKET, SO_REUSEADDR, (char*)&on, sizeof(on)) == SOCKET_ERROR)
  {
    Log::GetInstance().Write(L"ReuseAddr failed : \"%s\" Stopping server",
      Util::SystemErrorToString(WSAGetLastError()).c_str());

    this->Stop();

    return false;

  }

  SOCKADDR_STORAGE addr_in;
  SOCKADDR_STORAGE addr_in_stream;
  int addrSize = sizeof(SOCKADDR_STORAGE);

  addr_in.ss_family = af;
  addr_in_stream.ss_family = af;

  wstring tmp;

  if (this->m_env.use_ipv6)
    tmp = this->m_env.nic6;
  else
    tmp = this->m_env.nic;

  if (WSAStringToAddressW((LPWSTR)tmp.c_str(), af, nullptr, (SOCKADDR*)&addr_in, &addrSize) == SOCKET_ERROR)
  {
    Log::GetInstance().Write(L"Normal address parsing failed : \"%s\" Stopping server",
      Util::SystemErrorToString(WSAGetLastError()).c_str());

    this->Stop();

    return false;

  }

  if (this->m_env.use_ipv6)
    tmp = this->m_env.nic6_stream;
  else
    tmp = this->m_env.nic_stream;

  if (WSAStringToAddressW((LPWSTR)tmp.c_str(), af, nullptr, (SOCKADDR*)&addr_in_stream, &addrSize) == SOCKET_ERROR)
  {
    Log::GetInstance().Write(L"Stream address parsing failed : \"%s\" Stopping server",
      Util::SystemErrorToString(WSAGetLastError()).c_str());

    this->Stop();

    return false;

  }

  if (this->m_env.use_ipv6)
  {
    SOCKADDR_IN6 *ipv6 = (SOCKADDR_IN6*)&addr_in;
    SOCKADDR_IN6 *ipv6_stream = (SOCKADDR_IN6*)&addr_in_stream;

    ipv6->sin6_port = htons(this->m_env.listen_port);
    ipv6->sin6_flowinfo = 0;
    ipv6->sin6_scope_id = this->m_env.nic6_scope_id;

    ipv6_stream->sin6_port = htons(this->m_env.listen_stream_port);
    ipv6_stream->sin6_flowinfo = 0;
    ipv6_stream->sin6_scope_id = this->m_env.nic6_stream_scope_id;

  }
  else
  {
    SOCKADDR_IN *ipv4 = (SOCKADDR_IN*)&addr_in;
    SOCKADDR_IN *ipv4_stream = (SOCKADDR_IN*)&addr_in_stream;

    ipv4->sin_port = htons(this->m_env.listen_port);
    ipv4_stream->sin_port = htons(this->m_env.listen_stream_port);

  }

  if (bind(this->m_listenSocket, (LPSOCKADDR)&addr_in, sizeof(addr_in)) == SOCKET_ERROR ||
    bind(this->m_listenStreamSocket, (LPSOCKADDR)&addr_in_stream, sizeof(addr_in_stream)) == SOCKET_ERROR)
  {
    Log::GetInstance().Write(L"Bind failed : \"%s\" Stopping server",
      Util::SystemErrorToString(WSAGetLastError()).c_str());

    this->Stop();

    return false;

  }

  if (listen(this->m_listenSocket, MAX_BACK_LOG) == SOCKET_ERROR ||
    listen(this->m_listenStreamSocket, MAX_BACK_LOG) == SOCKET_ERROR)
  {
    Log::GetInstance().Write(L"Listen failed : \"%s\" Stopping server",
      Util::SystemErrorToString(WSAGetLastError()).c_str());

    this->Stop();

    return false;

  }

  SYSTEM_INFO sysInfo;

  GetSystemInfo(&sysInfo);

  sysInfo.dwNumberOfProcessors = sysInfo.dwNumberOfProcessors * 2 + 1;

  if (sysInfo.dwNumberOfProcessors > MAX_COMPLETION_THREAD_COUNT)
  {
    sysInfo.dwNumberOfProcessors = MAX_COMPLETION_THREAD_COUNT;

  }

  this->m_completionPort = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, sysInfo.dwNumberOfProcessors);

  if (this->m_completionPort == NULL)
  {
    Log::GetInstance().Write(L"CreateIoCompletionPort failed : \"%s\" Stopping server",
      Util::SystemErrorToString(GetLastError()).c_str());

    this->Stop();

    return false;

  }

  wcout << L">> " << Log::GetInstance().Write(L"Starting scheduler thread") << endl;

  this->m_scheduler.SetServer(this);

  if (!this->m_scheduler.Start())
  {
    Log::GetInstance().Write(L"Scheduler starting failed. Stopping server");

    this->Stop();

    return false;

  }

  wcout << L">> " << Log::GetInstance().Write(L"Scheduler thread started (Handle : %p)", this->m_scheduler.GetThreadHandle()) << endl;

  for (DWORD i = 0; i < sysInfo.dwNumberOfProcessors; i++)
  {
    Worker *worker = new Worker(this->m_completionPort, this);

    wcout << L">> " << Log::GetInstance().Write(L"Starting worker thread") << endl;

    if (!worker->Start())
    {
      delete worker;

      Log::GetInstance().Write(L"Worker starting failed. Stopping server.");

      this->Stop();

      return false;

    }

    wcout << L">> " << Log::GetInstance().Write(L"Worker thread started (Handle : %p)", worker->GetThreadHandle()) << endl;

    this->m_workers.push_back(worker);

  }

  if (!Thread::Start())
  {
    Log::GetInstance().Write(L"Cannot create server thread. Stopping server");

    this->Stop();

    return false;

  }

  this->m_stream.SetServer(this);
  this->m_stream.SetSocket(this->m_listenStreamSocket);

  wcout << L">> " << Log::GetInstance().Write(L"Starting stream thread") << endl;

  if (!this->m_stream.Start())
  {
    Log::GetInstance().Write(L"Cannot create stream thread. Stopping server");

    this->Stop();

    return false;

  }

  wcout << L">> " << Log::GetInstance().Write(L"Stream thread started (Handle : %p)", this->m_stream.GetThreadHandle()) << endl;

  Log::GetInstance().Write(L"AnyVOD server version : %d.%d.%d.%d %S (%s)", MAJOR, MINOR, PATCH, BUILDNUM, STRFILEVER, ARCH_BITS);
  Log::GetInstance().Write(L"SQLite3 version : %S", CppSQLite3DB::SQLiteVersion());
  Log::GetInstance().Write(L"MediaInfo version : %s", MediaInfo_Option(nullptr, L"Info_Version", L""));
  Log::GetInstance().Write(L"zlib version : %S", zlibVersion());

#ifdef NDEBUG
  Log::GetInstance().Write(L"TBBmalloc version : %S", TBB_VERSION_STRING);
  Log::GetInstance().Write(L"TBBmalloc_proxy version : %S", TBB_VERSION_STRING);
#endif // NDEBUG

  wcout << L">> " << Log::GetInstance().Write(L"Server started") << endl;

  return true;

}

bool
Server::SplitFileLine(const wstring &fileName, VECTOR_KEY_VALUE *ret)
{
  wifstream ifs(fileName.c_str());

  if (ifs.is_open())
  {
    ifs.imbue(locale(ifs.getloc(), new codecvt_utf8<wchar_t, 0x10ffff, consume_header>));

    while (ifs.good())
    {
      wstring line;

      getline(ifs, line);

      Util::TrimString(line, &line);

      if (line.size() == 0)
        continue;

      if (line.find_first_of(COMMENT_PREFIX) == 0)
        continue;

      size_t index = line.find_first_of(ITEM_DELIMITER);

      if (index == wstring::npos)
      {
        continue;

      }
      else
      {
        KEY_VALUE data;

        data.key = line.substr(0, index);
        data.value = line.substr(index + 1);

        ret->push_back(data);

      }

    }

    return true;

  }

  return false;

}

bool
Server::CheckMovieDir()
{
  DWORD attr = GetFileAttributesW(this->m_env.movie_dir.c_str());

  if (IS_BIT_SET(attr, FILE_ATTRIBUTE_DIRECTORY))
    return true;
  else
    return false;

}

bool
Server::ReadEnv()
{
  VECTOR_KEY_VALUE ret;

  if (this->SplitFileLine(INIT_FILE_NAME, &ret))
  {
    if (ret.size() == 0)
    {
      return false;

    }
    else
    {
      for (size_t i = 0; i < ret.size(); i++)
      {
        KEY_VALUE &data = ret[i];

        transform(data.key.begin(), data.key.end(), data.key.begin(), towlower);
        transform(data.value.begin(), data.value.end(), data.value.begin(), towlower);

        if (data.key == MOVIE_DIR)
        {
          this->m_env.movie_dir = data.value;

          replace(this->m_env.movie_dir.begin(), this->m_env.movie_dir.end(),
            UNIX_DIRECTORY_DELIMITER, DIRECTORY_DELIMITER);

        }
        else if (data.key == MOVIE_EXTS)
        {
          Util::SplitString(data.value, MOVIE_EXTS_DELIMITER, &this->m_env.movie_exts);

        }
        else if (data.key == LOG_DIR)
        {
          this->m_env.log_dir = data.value;

          replace(this->m_env.log_dir.begin(), this->m_env.log_dir.end(),
            UNIX_DIRECTORY_DELIMITER, DIRECTORY_DELIMITER);

        }
        else if (data.key == DB_DIR)
        {
          this->m_env.db_dir = data.value;

          replace(this->m_env.db_dir.begin(), this->m_env.db_dir.end(),
            UNIX_DIRECTORY_DELIMITER, DIRECTORY_DELIMITER);

        }
        else if (data.key == ANALYSER_DIR)
        {
          this->m_env.analyser_dir = data.value;

          replace(this->m_env.analyser_dir.begin(), this->m_env.analyser_dir.end(),
            UNIX_DIRECTORY_DELIMITER, DIRECTORY_DELIMITER);

        }
        else if (data.key == USE_IPV6)
        {
          if (data.value == L"yes")
            this->m_env.use_ipv6 = true;
          else
            this->m_env.use_ipv6 = false;

        }
        else if (data.key == NIC)
        {
          if (data.value.empty())
          {
            this->m_env.nic = DEFAULT_SERVER_ADDRESS;

          }
          else
          {
            this->m_env.nic = data.value;

          }

        }
        else if (data.key == NIC_STREAM)
        {
          if (data.value.empty())
          {
            this->m_env.nic_stream = DEFAULT_SERVER_ADDRESS;

          }
          else
          {
            this->m_env.nic_stream = data.value;

          }

        }
        else if (data.key == NIC6)
        {
          if (data.value.empty())
          {
            this->m_env.nic6 = DEFAULT_SERVER_ADDRESS6;

          }
          else
          {
            this->m_env.nic6 = data.value;

          }

        }
        else if (data.key == NIC6_STREAM)
        {
          if (data.value.empty())
          {
            this->m_env.nic6_stream = DEFAULT_SERVER_ADDRESS6;

          }
          else
          {
            this->m_env.nic6_stream = data.value;

          }

        }
        else if (data.key == NIC6_SCOPE_ID)
        {
          if (swscanf(data.value.c_str(), L"%ul", &this->m_env.nic6_scope_id) <= 0)
          {
            this->m_env.nic6_scope_id = 0;

          }

        }
        else if (data.key == NIC6_STREAM_SCOPE_ID)
        {
          if (swscanf(data.value.c_str(), L"%ul", &this->m_env.nic6_stream_scope_id) <= 0)
          {
            this->m_env.nic6_stream_scope_id = 0;

          }

        }
        else if (data.key == LISTEN_PORT)
        {
          if (swscanf(data.value.c_str(), L"%hu", &this->m_env.listen_port) <= 0)
          {
            this->m_env.listen_port = DEFAULT_NETWORK_PORT;

          }

        }
        else if (data.key == BUFFERING_MULTIPLIER)
        {
          if (swscanf(data.value.c_str(), L"%d", &this->m_env.buffering_multiplier) <= 0)
          {
            this->m_env.buffering_multiplier = DEFAULT_BUFFERING_MULTIPLIER;

          }

        }
        else if (data.key == LISTEN_STREAM_PORT)
        {
          if (swscanf(data.value.c_str(), L"%hu", &this->m_env.listen_stream_port) <= 0)
          {
            this->m_env.listen_port = DEFAULT_NETWORK_STREAM_PORT;

          }

        }

      }

      return true;

    }

  }
  else
  {
    return false;

  }

}

DataBase&
Server::GetDataBase()
{
  return this->m_db;

}

const ENV_INFO&
Server::GetEnv() const
{
  return this->m_env;

}

ClientProcessor&
Server::GetClientProcessor()
{
  return this->m_client;

}

Scheduler&
Server::GetScheduler()
{
  return this->m_scheduler;

}

AnalyserLoader&
Server::GetAnalyserLoader()
{
  return this->m_analyserLoader;

}

const
vector_wstring& Server::GetAudioExts() const
{
  return this->m_audioExts;

}

void
Server::Clear()
{
  this->m_client.Clear();

  for (size_t i = 0; i < this->m_workers.size(); i++)
  {
    Worker *worker = this->m_workers[i];

    if (worker == nullptr)
      continue;

    if (worker->IsRun())
    {
      wcout << L">> " << Log::GetInstance().Write(L"Stopping worker thread (Handle : %p)", worker->GetThreadHandle()) << endl;

      worker->Stop();

      wcout << L">> " << Log::GetInstance().Write(L"Worker thread stopped") << endl;

    }

    delete worker;

  }

  this->m_workers.clear();

  if (this->m_scheduler.IsRun())
  {
    wcout << L">> " << Log::GetInstance().Write(L"Stopping scheduler thread (Handle : %p)", this->m_scheduler.GetThreadHandle()) << endl;

    this->m_scheduler.Stop();

    wcout << L">> " << Log::GetInstance().Write(L"Scheduler thread stopped") << endl;

  }

}

bool
Server::Accept(SOCKET socket, bool isStream)
{
  SOCKADDR_STORAGE clientSockAddr;
  int sockAddrSize = sizeof(clientSockAddr);
  SOCKET clientSock = WSAAccept(socket, (LPSOCKADDR)&clientSockAddr, &sockAddrSize, nullptr, NULL);

  if (clientSock == INVALID_SOCKET)
  {
    int error = WSAGetLastError();

    if (error == WSAEINTR)
    {
      Log::GetInstance().Write(L"Listen %s socket closed", isStream ? L"stream" : L"command");

      return false;

    }
    else
    {
      Log::GetInstance().Write(L"Accept %s failed : %d", isStream ? L"stream" : L"command", error);

      return true;

    }

  }

  Log::GetInstance().Write(L"Client %s connected (IP : %s, Port : %hu, Socket : %u)",
    isStream ? L"stream" : L"command",
    Util::AddressToString(clientSockAddr).c_str(),
    Util::PortFromAddress(clientSockAddr),
    clientSock);

  BOOL on = TRUE;

  if (setsockopt(clientSock, IPPROTO_TCP, TCP_NODELAY, (char*)&on, sizeof(on)) == SOCKET_ERROR)
  {
    Log::GetInstance().Write(L"NoDelay %s failed : \"%s\" (IP : %s, Port : %hu, Socket : %u)",
      isStream ? L"stream" : L"command",
      Util::SystemErrorToString(WSAGetLastError()).c_str(),
      Util::AddressToString(clientSockAddr).c_str(),
      Util::PortFromAddress(clientSockAddr),
      clientSock);

  }

  if (setsockopt(clientSock, SOL_SOCKET, SO_KEEPALIVE, (char*)&on, sizeof(on)) == SOCKET_ERROR)
  {
    Log::GetInstance().Write(L"KeepAlive %s failed : \"%s\" (IP : %s, Port : %hu, Socket : %u)",
      isStream ? L"stream" : L"command",
      Util::SystemErrorToString(WSAGetLastError()).c_str(),
      Util::AddressToString(clientSockAddr).c_str(),
      Util::PortFromAddress(clientSockAddr),
      clientSock);

  }

  this->m_client.AddClient(clientSock, &clientSockAddr, this, this->m_completionPort, isStream);

  return true;
}

bool
Server::Run()
{
  return this->Accept(this->m_listenSocket, false);

}

bool
Server::RecvPost(SOCKET sock, WSABUF *buf, LPWSAOVERLAPPED overlapped)
{
  DWORD flags = 0;
  int ret = WSARecv(sock, buf, 1, nullptr, &flags, overlapped, nullptr);

  if (ret == SOCKET_ERROR)
  {
    if (WSAGetLastError() != WSA_IO_PENDING)
    {
      return false;

    }

  }

  return true;

}

bool
Server::SendPost(SOCKET sock, WSABUF *buf, LPWSAOVERLAPPED overlapped)
{
  int ret = WSASend(sock, buf, 1, nullptr, 0, overlapped, nullptr);

  if (ret == SOCKET_ERROR)
  {
    if (WSAGetLastError() != WSA_IO_PENDING)
    {
      return false;

    }

  }

  return true;

}

void
Server::Stop()
{
  wcout << L">> " << Log::GetInstance().Write(L"Stopping server") << endl;

  closesocket(this->m_listenSocket);
  this->m_listenSocket = INVALID_SOCKET;

  if (this->m_stream.IsRun())
  {
    wcout << L">> " << Log::GetInstance().Write(L"Stopping stream thread (Handle : %p)", this->m_stream.GetThreadHandle()) << endl;

    closesocket(this->m_listenStreamSocket);
    this->m_listenStreamSocket = INVALID_SOCKET;

    this->m_stream.Stop();
    this->m_stream.SetSocket(INVALID_SOCKET);

    wcout << L">> " << Log::GetInstance().Write(L"Stream thread stopped") << endl;

  }

  Thread::Stop();

  this->Clear();

  CloseHandle(this->m_completionPort);
  this->m_completionPort = NULL;

  wcout << L">> " << Log::GetInstance().Write(L"Starting DB vacuuming") << endl;

  this->m_db.Vacuum();

  wcout << L">> " << Log::GetInstance().Write(L"DB Vacuuming completed") << endl;

  this->m_db.Close();

  WSACleanup();

  this->m_env = ENV_INFO();
  this->m_analyserLoader.CleanUp();

  wcout << L">> " << Log::GetInstance().Write(L"Server stopped") << endl;

  Log::GetInstance().EndLog();

}
