/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "MovieTransmitter.h"
#include "..\..\..\common\MemBuffer.h"

class Server;
union ANYVOD_PACKET;
struct IO_CONTEXT;
enum IO_OPERATION_TYPE;

class Client
{
public:

  friend class PacketHandler;
  friend class UserPacketHandler;

  struct USER_INFO
  {
    wstring id;
    wstring pass;
    wstring name;
    string ticket;
    bool admin;

  };

  Client(SOCKET socket, SOCKADDR_STORAGE *clientAddr, Server *server, bool isStream);
  ~Client();

  bool Init();
  void Clear();

  void Shutdown();

  bool SendPostPacket(ANYVOD_PACKET &packet);
  bool SendPost(const char *data, ULONG size);
  bool RecvHeaderPost(ULONG size);
  bool RecvBodyPost(ULONG size);

  bool ProcessContext(IO_OPERATION_TYPE op, const IO_CONTEXT *context,
    DWORD bytesTransferred, bool removeContextOnly);

  CRITICAL_SECTION* GetLocker();

  const SOCKADDR_STORAGE& GetSockAddr() const;

  unsigned long long GetTotalRecvBytes() const;
  unsigned long long GetTotalSentBytes() const;

  unsigned int GetRecvBytesPerSec() const;
  unsigned int GetSentBytesPerSec() const;

  ANYVOD_PACKET_TYPE GetLastPacketType() const;
  CLIENT_STATE GetState() const;
  Server* GetServer();
  const USER_INFO& GetUserInfo() const;
  void SetUserInfo(const USER_INFO &info);
  void SetTicket(const string &ticket);
  void ResetAdmin();

  void SetSearchSubtitle(bool use);
  void SetSearchLyrics(bool use);

  bool IsForceClose() const;
  bool IsMatchTicket(const string &ticket) const;
  bool IsLogined() const;
  bool IsToBeClose() const;
  bool IsSendable() const;
  bool IsAdmin();
  bool IsStream() const;
  bool IsSearchSubtitle() const;
  bool isSearchLyrics() const;

  void SetForceClose();
  void SetToBeClosed();

  LONG GetIORefCount() const;
  void IncreaseIORef();
  void DecreaseIORef();

  SOCKET GetSocket() const;

  size_t GetRecvBufferCount();
  size_t GetSendBufferCount();

  MovieTransmitter& GetMovieTransmitter();

  bool ProcessOwnTasks();

private:
  bool RecvPostInteral(ULONG size, IO_OPERATION_TYPE op);
  void RemoveContext(IO_OPERATION_TYPE op, const IO_CONTEXT *context);
  void SetState(CLIENT_STATE state);
  void SetLastPacketType(ANYVOD_PACKET_TYPE packetType);

private:
  typedef list<IO_CONTEXT*> LIST_PACKET;
  typedef LIST_PACKET::iterator LIST_PACKET_ITERATOR;

private:
  SOCKET m_socket;
  SOCKADDR_STORAGE m_clientAddr;
  Server *m_server;
  LIST_PACKET m_recvQueue;
  LIST_PACKET m_sendQueue;
  CRITICAL_SECTION m_recvLocker;
  CRITICAL_SECTION m_sendLocker;
  CRITICAL_SECTION m_thisLocker;
  MemBuffer m_recvBuffer;
  DWORD m_lastTime;
  unsigned long long m_totalRecvBytes;
  unsigned long long m_totalSentBytes;
  unsigned int m_recvBytesPerSec;
  unsigned int m_sentBytesPerSec;
  unsigned int m_accumRecvBytesPerSec;
  unsigned int m_accumSentBytesPerSec;
  CLIENT_STATE m_state;
  ANYVOD_PACKET_TYPE m_lastPacketType;
  USER_INFO m_userInfo;
  bool m_forceClose;
  bool m_toBeClose;
  volatile LONG m_refIOCount;
  MovieTransmitter m_movieTrans;
  bool m_isStream;
  bool m_searchSubtitle;
  bool m_searchLyrics;

};
