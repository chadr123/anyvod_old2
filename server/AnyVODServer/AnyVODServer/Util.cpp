/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "stdafx.h"
#include "Util.h"
#include "Common.h"
#include "..\..\..\common\version.h"

bool
Util::IsMatchExt(const wstring &fileName, const vector_wstring &exts)
{
  bool extFound = false;

  for (int i = (int)wcsnlen(fileName.c_str(), MAX_PATH) - 1; i >= 0 ; i--)
  {
    if (fileName[i] == EXTENSION_SEPERATOR)
    {
      wstring ext;

      ext.assign(&fileName.c_str()[i + 1]);

      transform(ext.begin(), ext.end(), ext.begin(), towlower);

      vector_wstring::const_iterator j = std::find(exts.begin(), exts.end(), ext);

      if (j != exts.end())
        extFound = true;

      break;

    }

  }

  return extFound;

}

void
Util::SplitFilePath(const wstring &fullPath, wstring *retPath, wstring *retFileName)
{
  size_t index = fullPath.find_last_of(DIRECTORY_DELIMITER);

  if (index != wstring::npos)
  {
    retPath->assign(fullPath, 0, index + 1);
    retFileName->assign(fullPath, index + 1, fullPath.size() - index);

  }

}

bool
Util::IsRoot(const wstring &path)
{
  if (path.size() == 3)
  {
    if (path[1] == L':' && path[2] == DIRECTORY_DELIMITER)
    {
      return true;

    }
    else
    {
      return false;

    }

  }
  else
  {
    return false;

  }

}

bool
Util::GetMediaInfo(const wstring &filePath, DataBase::MOVIE_INFO *info)
{
  void *handle = MediaInfo_New();

  if (MediaInfo_Open(handle, filePath.c_str()) != 1)
  {
    MediaInfo_Delete(handle);

    return false;

  }

  swscanf(MediaInfo_Get(handle, MediaInfo_Stream_General, 0, L"FileSize", MediaInfo_Info_Text, MediaInfo_Info_Name),
    L"%lld", &info->totalSize);
  swscanf(MediaInfo_Get(handle, MediaInfo_Stream_General, 0, L"Duration", MediaInfo_Info_Text, MediaInfo_Info_Name),
    L"%d", &info->totalTime);

  if (info->totalTime < 0)
    info->totalTime = 0;

  swscanf(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, L"FrameCount", MediaInfo_Info_Text, MediaInfo_Info_Name),
    L"%d", &info->totalFrame);

  if (info->totalFrame <= 0)
  {
    float frameRate = 0.0;

    swscanf(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, L"FrameRate", MediaInfo_Info_Text, MediaInfo_Info_Name),
      L"%f", &frameRate);

    if (frameRate <= 0)
    {
      swscanf(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, L"FrameRate_Original", MediaInfo_Info_Text, MediaInfo_Info_Name),
        L"%f", &frameRate);

    }

    info->totalFrame = int(frameRate * (info->totalTime / 1000.0));

  }

  if (info->totalFrame < 0)
    info->totalFrame = 0;

  int videoBitRate = 0;

  swscanf(MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, L"BitRate", MediaInfo_Info_Text, MediaInfo_Info_Name),
    L"%d", &videoBitRate);

  int maxAudioBitRate = 0;

  if (videoBitRate > 0)
  {
    int streamCount = 0;

    swscanf(MediaInfo_Get(handle, MediaInfo_Stream_Audio, 0, L"StreamCount", MediaInfo_Info_Text, MediaInfo_Info_Name),
      L"%d", &streamCount);

    for (int i = 0; i < streamCount; i++)
    {
      int audioBitRate = 0;

      swscanf(MediaInfo_Get(handle, MediaInfo_Stream_Audio, i, L"BitRate", MediaInfo_Info_Text, MediaInfo_Info_Name),
        L"%d", &audioBitRate);

      maxAudioBitRate = max(maxAudioBitRate, audioBitRate);

    }

  }

  info->bitRate = videoBitRate + maxAudioBitRate;

  if (info->bitRate == 0)
  {
    swscanf(MediaInfo_Get(handle, MediaInfo_Stream_General, 0, L"OverallBitRate", MediaInfo_Info_Text, MediaInfo_Info_Name),
      L"%d", &info->bitRate);

  }

  info->totalTime /= 1000;

  info->movieType = MediaInfo_Get(handle, MediaInfo_Stream_General, 0, L"Format", MediaInfo_Info_Text, MediaInfo_Info_Name);
  info->codecVideo = MediaInfo_Get(handle, MediaInfo_Stream_Video, 0, L"CodecID", MediaInfo_Info_Text, MediaInfo_Info_Name);
  info->title = MediaInfo_Get(handle, MediaInfo_Stream_General, 0, L"Title", MediaInfo_Info_Text, MediaInfo_Info_Name);

  transform(info->movieType.begin(), info->movieType.end(), info->movieType.begin(), towupper);
  transform(info->codecVideo.begin(), info->codecVideo.end(), info->codecVideo.begin(), towupper);

  MediaInfo_Close(handle);
  MediaInfo_Delete(handle);

  return true;

}

void
Util::TrimString(const wstring &src, wstring *ret)
{
  *ret = src;

  ret->erase(0, ret->find_first_not_of(L" \t\v\n\r"));
  ret->erase(ret->find_last_not_of(L" \t\v\n\r") + 1);

}

void
Util::GetLocalTimeToLargeInteger(const SYSTEMTIME *time, ULARGE_INTEGER *integer)
{
  SYSTEMTIME tmpTime;
  FILETIME tmp;
  BOOL success = FALSE;

  if (time == nullptr)
  {
    GetLocalTime(&tmpTime);
    success = SystemTimeToFileTime(&tmpTime, &tmp);

  }
  else
  {
    success = SystemTimeToFileTime(time, &tmp);

  }

  if (success)
  {
    integer->LowPart = tmp.dwLowDateTime;
    integer->HighPart = tmp.dwHighDateTime;

  }
  else
  {
    integer->LowPart = 0;
    integer->HighPart = 0;

  }

}

void
Util::AppendDirSeparator(wstring *path)
{
  if (path->size() == 0 || (*path)[path->size()-1] != DIRECTORY_DELIMITER)
    path->append(1, DIRECTORY_DELIMITER);

}

void
Util::BindPath(const wstring &first, const wstring &second, wstring *ret)
{
  wstring s = second;

  if (s[0] == CURRENT_PATH)
  {
    s.erase(0, 1);

  }

  *ret = first + s;

  wstring::iterator i = ret->begin();

  while (i != ret->end())
  {
    if (*i == DIRECTORY_DELIMITER && (i+1 != ret->end()) && (*(i+1) == DIRECTORY_DELIMITER))
    {
      i = ret->erase(i);

    }
    else
    {
      i++;

    }

  }

  wstring networkStarter;

  networkStarter.push_back(DIRECTORY_DELIMITER);
  networkStarter.push_back(DIRECTORY_DELIMITER);

  if (first.find_first_of(networkStarter) == 0)
  {
    ret->insert(0, 1, DIRECTORY_DELIMITER);

  }

}

void
Util::ConvertAsciiToUnicode(const string &ascii, wstring *ret)
{
  int nReqire = ::MultiByteToWideChar(CP_ACP, 0, ascii.c_str(), (int)ascii.size(), nullptr, 0);
  wchar_t *wszTmp = new wchar_t[nReqire+1];

  MultiByteToWideChar(CP_ACP, 0, ascii.c_str(), (int)ascii.size(), wszTmp, nReqire+1);

  wszTmp[nReqire] = L'\0';

  *ret = wszTmp;

  delete[] wszTmp;

}

void
Util::ConvertUnicodeToAscii(const wstring &unicode, string *ret)
{
  int nReqire = ::WideCharToMultiByte(CP_ACP, 0, unicode.c_str(), (int)unicode.size(), nullptr, 0, nullptr, nullptr);
  char *szTmp = new char[nReqire+1];

  WideCharToMultiByte(CP_ACP, 0, unicode.c_str(), (int)unicode.size(), szTmp, nReqire+1, nullptr, nullptr);

  szTmp[nReqire] = '\0';

  *ret = szTmp;

  delete[] szTmp;

}

void
Util::ConvertUnicodeToUTF8(const wstring &unicode, char *ret, int size)
{
  memset(ret, 0x00, size);

  WideCharToMultiByte(CP_UTF8, 0, unicode.c_str(), (int)unicode.size(), ret, size-MAX_UTF8_SIZE, nullptr, nullptr);

}

void
Util::ConvertUTF8ToUnicode(const char *utf8, int size, wstring *ret)
{
  int nReqire = ::MultiByteToWideChar(CP_UTF8, 0, utf8, size, nullptr, 0);
  wchar_t *wszTmp = new wchar_t[nReqire+1];

  MultiByteToWideChar(CP_UTF8, 0, utf8, size, wszTmp, nReqire+1);

  wszTmp[nReqire] = '\0';

  *ret = wszTmp;

  delete[] wszTmp;

}

wstring
Util::SystemErrorToString(DWORD error)
{
  wchar_t *msg = nullptr;
  wstring ret;
  wchar_t errorToString[10];

  swprintf(errorToString, 9, L"%lu", error);

  if (FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_ALLOCATE_BUFFER,
    nullptr, error, MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), (LPWSTR)&msg, 0, nullptr) != 0)
  {

    ret = msg;

    LocalFree(msg);

  }
  else
  {
    ret = L"Unknown";

  }

  ret.erase(ret.find_first_of(L'\r'));

  ret.append(L"(");
  ret.append(errorToString);
  ret.append(L")");

  return ret;

}

wstring
Util::GetModuleVersion(HMODULE module, bool getStringVersion)
{
  DWORD handle;
  DWORD size;
  wstringstream ss;
  wchar_t name[MAX_PATH];

  if (GetModuleFileNameW(module, name, MAX_PATH) > 0)
  {
    size = GetFileVersionInfoSizeW(name, &handle);

    if (size > 0)
    {
      char *bytes = new char[size];

      if (GetFileVersionInfoW(name, handle, size, bytes))
      {
        UINT *version;
        UINT len;

        if (VerQueryValueW(bytes, L"\\", (LPVOID*)&version, &len))
        {
          VS_FIXEDFILEINFO *vs = (VS_FIXEDFILEINFO *)version;

          ss << HIWORD(vs->dwFileVersionMS) << L"." << LOWORD(vs->dwFileVersionMS) << L"." <<
            HIWORD(vs->dwFileVersionLS) << L"." << LOWORD(vs->dwFileVersionLS);

        }

        if (getStringVersion)
        {
          wstring key;

          Util::ConvertAsciiToUnicode(TRANSLATION, &key);

          key = L"\\StringFileInfo\\" + key + L"\\FileVersion";

          if (VerQueryValueW(bytes, key.c_str(), (LPVOID*)&version, &len))
          {
            wchar_t *versionString = (wchar_t *)version;

            ss << L" " <<  versionString;

          }

        }

      }

      delete[] bytes;

    }

  }

  return ss.str();

}

bool
Util::DeterminMovieMetaDataRangeSub(ANYVOD_ANALYSED_DATA_BLOCK &prevBlock, ANYVOD_ANALYSED_DATA_BLOCK &block,
                                    long long startOffset, int size, long long *accumOffset)
{
  *accumOffset = prevBlock.blockOffset + prevBlock.size;

  if (*accumOffset == block.blockOffset)
  {
    if (startOffset + size <= block.blockOffset + block.size)
      return true;

  }
  else
  {
    if (startOffset >= block.blockOffset && startOffset + size <= block.blockOffset + block.size)
      return true;

  }

  return false;

}

bool
Util::DeterminMovieMetaDataRange(long long startOffset, int size, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK &metaData)
{
  if (metaData.size() > 0)
  {
    long long accumOffset = 0;
    ANYVOD_ANALYSED_DATA_BLOCK prev = metaData[0];

    if (startOffset >= prev.blockOffset &&
      startOffset + size <= prev.blockOffset + prev.size)
      return true;

    for (size_t i = 1; i < metaData.size(); i++)
    {
      if (Util::DeterminMovieMetaDataRangeSub(prev, metaData[i], startOffset, size, &accumOffset))
        return true;

      prev = metaData[i];

    }

    return false;

  }

  return false;

}

void
Util::SplitString(const wstring &src, const wchar_t delem, vector_wstring *ret)
{
  size_t start = 0;
  size_t end = src.find_first_of(delem);

  if (end == wstring::npos)
  {
    ret->push_back(src);

  }
  else
  {
    wstring substr;

    do
    {
      substr = src.substr(start, end - start);

      if (substr.size() > 0)
        ret->push_back(substr);

      start = end + 1;

      end = src.find_first_of(delem, start);

    } while (end != wstring::npos);

    substr = src.substr(start, src.size() - start);

    if (substr.size() > 0)
      ret->push_back(substr);

  }

}

bool
Util::CreateDirectories(const wstring &path)
{
  wstring fullPath;

  if (PathIsRelativeW(path.c_str()))
  {
    wchar_t pwd[MAX_PATH];

    if (!GetCurrentDirectoryW(MAX_PATH, pwd))
      return false;

    Util::BindPath(pwd, path, &fullPath);

  }
  else
  {
    fullPath = path;

  }

  int ret = SHCreateDirectoryExW(NULL, fullPath.c_str(), nullptr);

  if (ret == ERROR_SUCCESS || ret == ERROR_ALREADY_EXISTS || ret == ERROR_FILE_EXISTS)
    return true;
  else
    return false;

}

const wstring
Util::AddressToString(const SOCKADDR_STORAGE &addr)
{
  wstring out;
  DWORD bufSize = 0;
  wchar_t *buf = nullptr;

  WSAAddressToStringW((sockaddr*)&addr, sizeof(addr), nullptr, nullptr, &bufSize);

  buf = new wchar_t[bufSize];
  memset(buf, 0, bufSize);

  WSAAddressToStringW((sockaddr*)&addr, sizeof(addr), nullptr, buf, &bufSize);

  out = buf;
  delete[] buf;

  wstring::size_type portPos = out.find_last_of(':');

  if (portPos != wstring::npos)
    out = out.substr(0, portPos);

  return out;

}

unsigned short
Util::PortFromAddress(const SOCKADDR_STORAGE &addr)
{
  unsigned short port = 0;

  switch (addr.ss_family)
  {
  case AF_INET:
    {
      sockaddr_in *sa = (sockaddr_in*)&addr;

      port = sa->sin_port;

      break;

    }
  case AF_INET6:
    {
      sockaddr_in6 *sa = (sockaddr_in6*)&addr;

      port = sa->sin6_port;

      break;

    }
  default:
    {
      break;

    }

  }

  return ntohs(port);

}

void
Util::GetHashValue(ALG_ID alg, uint8_t *buf, size_t size, string *ret)
{
  BYTE *hashValue = nullptr;
  HCRYPTPROV cryptProv = NULL;
  HCRYPTHASH hash = NULL;
  DWORD hashLen = 0;
  DWORD hashLenSize = sizeof(DWORD);
  DWORD bufSize = (DWORD)size;
  BYTE *content = (BYTE*)buf;

  ret->clear();

  if (CryptAcquireContext(&cryptProv, nullptr, nullptr, PROV_RSA_AES, CRYPT_VERIFYCONTEXT | CRYPT_MACHINE_KEYSET))
  {
    if (CryptCreateHash(cryptProv, alg, 0, 0, &hash))
    {
      if (CryptHashData(hash, content, bufSize, 0))
      {
        if (CryptGetHashParam(hash, HP_HASHSIZE, (BYTE *)&hashLen, &hashLenSize, 0))
        {
          hashValue = new BYTE[hashLen];

          if (CryptGetHashParam(hash, HP_HASHVAL, hashValue, &hashLen, 0))
          {
            char tmp[3] = {0, };

            for (DWORD i = 0; i < hashLen; i++)
            {
              sprintf(tmp, "%02x", hashValue[i]);
              ret->append(tmp);

            }

          }

          delete[] hashValue;

        }

      }

    }

  }

  if (hash != NULL)
    CryptDestroyHash(hash);

  if (cryptProv != NULL)
    CryptReleaseContext(cryptProv, 0);
}

void
Util::GetMD5(uint8_t *buf, size_t size, string *ret)
{
  Util::GetHashValue(CALG_MD5, buf, size, ret);

}

void
Util::GetSHA256(uint8_t *buf, size_t size, string *ret)
{
  Util::GetHashValue(CALG_SHA_256, buf, size, ret);

}

