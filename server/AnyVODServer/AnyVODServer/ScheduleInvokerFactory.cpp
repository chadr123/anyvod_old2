/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "stdafx.h"
#include "ScheduleInvokerFactory.h"
#include "ScheduleInvoker.h"
#include "UpdateMovieInfoInvoker.h"
#include "RebuildMovieMetaInvoker.h"

const wchar_t ScheduleInvokerFactory::INVOKER_NAMES[IT_COUNT][MAX_SCHEDULE_NAME_SIZE] = {
  L"updatemovieinfo",
  L"rebuildmoviemeta"
};

ScheduleInvoker*
ScheduleInvokerFactory::GetInvoker(Server *server, const wstring &type)
{
  ScheduleInvoker *invoker = nullptr;

  if (type == ScheduleInvokerFactory::INVOKER_NAMES[IT_UPDATEMOVIEINFO])
  {
    invoker = new UpdateMovieInfoInvoker;

  }
  else if (type == ScheduleInvokerFactory::INVOKER_NAMES[IT_REBUILDMOVIEMETA])
  {
    invoker = new RebuildMovieMetaInvoker;

  }

  if (invoker != nullptr)
  {
    invoker->SetServer(server);
    invoker->SetName(type);

  }

  return invoker;

}
