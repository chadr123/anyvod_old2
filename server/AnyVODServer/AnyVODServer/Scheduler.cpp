/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "stdafx.h"
#include "Scheduler.h"
#include "ScheduleInvoker.h"
#include "ScheduleInvokerFactory.h"
#include "Util.h"
#include "Log.h"

Scheduler::Scheduler()
:m_server(nullptr)
{
  this->m_timeLimits.push_back(TIME(0, 59));//minute
  this->m_timeLimits.push_back(TIME(0, 23));//hour
  this->m_timeLimits.push_back(TIME(1, 31));//day
  this->m_timeLimits.push_back(TIME(1, 12));//month
  this->m_timeLimits.push_back(TIME(0, 6));//weekday

  InitLock(&this->m_jobLocker, MAX_SPIN_COUNT);

}

Scheduler::~Scheduler()
{
  this->Stop();

  DeleteLock(&this->m_jobLocker);

}

void
Scheduler::SetServer(Server *server)
{
  this->m_server = server;

}

bool
Scheduler::Start()
{
  if (this->ParseCommand(SCHEDULE_FILE_NAME))
  {
    if (Thread::Start())
    {
      return true;

    }
    else
    {
      this->Stop();

      Log::GetInstance().Write(L"Cannot parse schedule file : %s", SCHEDULE_FILE_NAME);

      return false;

    }

  }
  else
  {
    return false;

  }

}

bool
Scheduler::RunningSchedules(unsigned int *index)
{
  bool running = false;

  ReadLock(&this->m_jobLocker);

  for (size_t i = 0; i < this->m_jobs.size(); i++)
  {
    JOB &job = this->m_jobs[i];

    if (job.invoker != nullptr)
    {
      if (job.invoker->IsRun())
      {
        running = true;

        *index = (unsigned int)i;

        break;

      }

    }

  }

  ReadUnlock(&this->m_jobLocker);

  return running;

}

void
Scheduler::Stop()
{
  Thread::Stop();

  WriteLock(&this->m_jobLocker);

  VECTOR_JOB_ITERATOR i = this->m_jobs.begin();

  while (i != this->m_jobs.end())
  {
    JOB &job = *i;

    if (job.invoker != nullptr)
    {
      while (job.invoker->IsRun())
      {
        Log::GetInstance().Write(L"Waiting for invoker \"%s\" (Handle : %p)",
          job.invokerName.c_str(), job.invoker->GetThreadHandle());

        WaitForSingleObject(job.invoker->GetThreadHandle(), INFINITE);

      }

      delete job.invoker;

    }

    i++;

  }

  this->m_jobs.clear();

  WriteUnlock(&this->m_jobLocker);

}

bool
Scheduler::ParseSub(const wstring &token, TIME_INDEX index, VECTOR_TIME *times)
{
  TIME time;
  size_t rangeEnd = token.find_first_of(L'-');
  TIME limit = this->m_timeLimits[index];

  if (rangeEnd == wstring::npos)
  {
    if (swscanf(token.c_str(), L"%d", &time.min) < 1)
      return false;

    time.max = time.min;

  }
  else
  {
    wstring rangeToken = token.substr(0, rangeEnd);

    if (swscanf(rangeToken.c_str(), L"%d", &time.min) < 1)
      return false;

    rangeToken = token.substr(rangeEnd + 1);

    if (swscanf(rangeToken.c_str(), L"%d", &time.max) < 1)
      return false;

    if (time.min > time.max)
      return false;

  }

  if (time.min >= limit.min && time.max <= limit.max)
  {
    times->push_back(time);

    return true;

  }
  else
  {
    Log::GetInstance().Write(L"Exceed range : Type(%d), MinMax(%d, %d)", index, time.min, time.max);

    return false;

  }

}

bool
Scheduler::ParseLine(const wstring &line, JOB *ret)
{
  size_t elementStart = 0;
  size_t elementEnd = 0;

  for (int i = TI_MINUTE;
    ((elementEnd = line.find_first_of(L" \t", elementStart)) != wstring::npos) || i < TI_COUNT;
    i++)
  {
    wstring elementToken = line.substr(elementStart, elementEnd - elementStart);
    size_t timeStart = 0;
    size_t timeEnd = 0;

    if (elementToken == L"*")
    {
      ret->elements[i].all = true;

    }
    else
    {
      ret->elements[i].all = false;

      timeEnd = elementToken.find_first_of(L',', timeStart);

      if (timeEnd == wstring::npos)
      {
        if (!this->ParseSub(elementToken, (TIME_INDEX)i, &ret->elements[i].times))
          return false;

      }
      else
      {
        while ((timeEnd = elementToken.find_first_of(L',', timeStart)) != wstring::npos)
        {
          wstring timeToken = elementToken.substr(timeStart, timeEnd - timeStart);

          if (!this->ParseSub(timeToken, (TIME_INDEX)i, &ret->elements[i].times))
            return false;

          timeStart = timeEnd + 1;

        }

        wstring timeToken = elementToken.substr(timeStart);

        if (!this->ParseSub(timeToken, (TIME_INDEX)i, &ret->elements[i].times))
          return false;

      }

    }

    elementStart = line.find_first_not_of(L" \t", elementEnd);

  }

  //invoker생성
  wstring command = line.substr(elementStart);

  ret->invoker = ScheduleInvokerFactory::GetInvoker(this->m_server, command);

  if (ret->invoker == nullptr)
  {
    Log::GetInstance().Write(L"Invalid schedule command : %s", command.c_str());

    return false;

  }
  else
  {
    Util::GetLocalTimeToLargeInteger(nullptr, &ret->lastTime);

    ret->invokerName = command;
    ret->script = line;

    return true;

  }

}

bool
Scheduler::ParseCommand(const wstring &filePath)
{
  wifstream ifs(filePath.c_str());

  if (ifs.is_open())
  {
    while (ifs.good())
    {
      wstring line;

      getline(ifs, line);

      Util::TrimString(line, &line);

      if (line.size() == 0)
        continue;

      if (line.find_first_of(COMMENT_PREFIX) == 0)
        continue;

      JOB job;

      if (!this->ParseLine(line, &job))
      {
        return false;

      }

      WriteLock(&this->m_jobLocker);

      this->m_jobs.push_back(job);

      WriteUnlock(&this->m_jobLocker);

    }

    return true;

  }

  return false;

}

void
Scheduler::GetJobStatus(Scheduler::VECTOR_JOB_STATUS *ret)
{
  ReadLock(&this->m_jobLocker);

  VECTOR_JOB_ITERATOR i = this->m_jobs.begin();

  for (; i != this->m_jobs.end(); i++)
  {
    JOB &job = *i;
    JOB_STATUS status;

    status.name = job.invokerName;
    status.run = job.invoker->IsRun();
    status.script = job.script;

    job.invoker->GetStartedTime(&status.startedTime);
    job.invoker->GetEndedTime(&status.endedTime);

    ret->push_back(status);

  }

  ReadUnlock(&this->m_jobLocker);

}

bool
Scheduler::IsMatchTime(const TIME_TRAIT &times, WORD value)
{
  if (times.all)
  {
    return true;

  }
  else
  {
    VECTOR_TIME_CONST_ITERATOR i = times.times.begin();

    for (; i != times.times.end(); i++)
    {
      const TIME &time = *i;

      if (time.min <= value && time.max >= value)
      {
        return true;

      }

    }

  }

  return false;

}

bool
Scheduler::StartNow(int index, SCHEDULE_FAIL_REASON *reason)
{
  bool success = false;

  WriteLock(&this->m_jobLocker);

  if (index < 0 || index > ((int)this->m_jobs.size()-1))
  {
    *reason = SFR_NOT_EXIST;
    success = false;

  }
  else
  {
    JOB &job = this->m_jobs[index];

    if (!job.invoker->IsRun())
    {
      job.startNow = true;
      success = true;

    }
    else
    {
      *reason = SFR_RUNNING;
      success = false;

    }

  }

  WriteUnlock(&this->m_jobLocker);

  return success;

}

unsigned int
Scheduler::Add(const wstring &script, bool *success, SCHEDULE_FAIL_REASON *reason)
{
  *success = false;
  unsigned int index = 0;

  WriteLock(&this->m_jobLocker);

  if (script.size() == 0)
  {
    *success = false;
    *reason = SFR_INVALID_SCRIPT;

  }
  else
  {
    JOB job;

    if (this->ParseLine(script, &job))
    {
      this->m_jobs.push_back(job);

      if (this->SaveJobsToFile(false))
      {
        index = (unsigned int)this->m_jobs.size()-1;
        *success = true;

      }
      else
      {
        this->m_jobs.pop_back();
        delete job.invoker;

        *success = false;
        *reason = SFR_FILE_WRITE_FAILD;

      }

    }
    else
    {
      *success = false;
      *reason = SFR_INVALID_SCRIPT;

    }

  }

  WriteUnlock(&this->m_jobLocker);

  return index;

}

bool
Scheduler::Delete(int index, SCHEDULE_FAIL_REASON *reason)
{
  bool success = false;

  WriteLock(&this->m_jobLocker);

  if (index < 0 || index > ((int)this->m_jobs.size()-1))
  {
    *reason = SFR_NOT_EXIST;
    success = false;

  }
  else
  {
    //파일에 저장을 못했을 경우를 대비해 백업 해놓는다.
    JOB job = this->m_jobs[index];

    if (!job.invoker->IsRun())
    {
      VECTOR_JOB_ITERATOR i = this->m_jobs.begin() + index;

      this->m_jobs.erase(i);

      if (this->SaveJobsToFile(false))
      {
        delete job.invoker;

        success = true;

      }
      else
      {
        i = this->m_jobs.begin() + index;

        this->m_jobs.insert(i, job);

        *reason = SFR_FILE_WRITE_FAILD;
        success = false;

      }

    }
    else
    {
      *reason = SFR_RUNNING;
      success = false;

    }

  }

  WriteUnlock(&this->m_jobLocker);

  return success;

}

bool
Scheduler::Modify(int index, const wstring &script, SCHEDULE_FAIL_REASON *reason)
{
  bool success = false;

  WriteLock(&this->m_jobLocker);

  if (index < 0 || index > ((int)this->m_jobs.size()-1))
  {
    *reason = SFR_NOT_EXIST;
    success = false;

  }
  else
  {
    //파일에 저장을 못했을 경우를 대비해 백업 해놓는다.
    JOB job = this->m_jobs[index];

    if (!job.invoker->IsRun())
    {
      JOB newJob;

      //삽입
      if (this->ParseLine(script, &newJob))
      {
        SYSTEMTIME startTime;
        SYSTEMTIME endTime;

        job.invoker->GetStartedTime(&startTime);
        job.invoker->GetEndedTime(&endTime);

        newJob.invoker->SetStartedTime(startTime);
        newJob.invoker->SetEndedTime(endTime);
        newJob.startNow = job.startNow;

        //기존거 삭제
        VECTOR_JOB_ITERATOR i = this->m_jobs.begin() + index;

        this->m_jobs.erase(i);

        //새로운거 삽입
        i = this->m_jobs.begin() + index;
        this->m_jobs.insert(i, newJob);

        if (this->SaveJobsToFile(false))
        {
          delete job.invoker;

          success = true;

        }
        else
        {
          //삽입된거 삭제
          i = this->m_jobs.begin() + index;
          this->m_jobs.erase(i);

          //기존거 삽입
          i = this->m_jobs.begin() + index;
          this->m_jobs.insert(i, job);

          delete newJob.invoker;

          *reason = SFR_FILE_WRITE_FAILD;
          success = false;

        }

      }
      else
      {
        *reason = SFR_INVALID_SCRIPT;
        success = false;

      }

    }
    else
    {
      *reason = SFR_RUNNING;
      success = false;

    }

  }

  WriteUnlock(&this->m_jobLocker);

  return success;
}

bool
Scheduler::SaveJobsToFile(bool useLock)
{
  if (useLock)
  {
    ReadLock(&this->m_jobLocker);

  }

  bool success = false;
  VECTOR_JOB_ITERATOR i = this->m_jobs.begin();
  wstringstream ss;

  for (; i != this->m_jobs.end(); i++)
  {
    JOB &job = *i;

    ss << job.script << endl;

  }

  wofstream ofs;

  ofs.open(SCHEDULE_FILE_NAME, ios::trunc);

  if (ofs.is_open())
  {
    ofs << ss.str();

    if (ofs.good())
    {
      success = true;

    }
    else
    {
      success = false;

    }

  }
  else
  {
    success = false;

  }

  if (useLock)
  {
    ReadUnlock(&this->m_jobLocker);

  }

  return success;

}

bool
Scheduler::Run()
{
  SYSTEMTIME time;
  ULARGE_INTEGER serialTime;

  GetLocalTime(&time);
  Util::GetLocalTimeToLargeInteger(&time, &serialTime);

  ReadLock(&this->m_jobLocker);

  VECTOR_JOB_ITERATOR i = this->m_jobs.begin();

  for (; i != this->m_jobs.end(); i++)
  {
    JOB &job = *i;
    bool run = false;

    if (serialTime.QuadPart - job.lastTime.QuadPart > 60ull * 10000000ull)
    {
      if (this->IsMatchTime(job.elements[TI_MINUTE], time.wMinute) &&
        this->IsMatchTime(job.elements[TI_HOUR], time.wHour) &&
        this->IsMatchTime(job.elements[TI_DAY], time.wDay) &&
        this->IsMatchTime(job.elements[TI_MONTH], time.wMonth) &&
        this->IsMatchTime(job.elements[TI_WEEKDAY], time.wDayOfWeek))

      {
        run = true;

      }

      job.lastTime = serialTime;

    }
    else if (job.startNow)
    {
      run = true;

    }

    if (run)
    {
      if (!job.invoker->IsRun())
      {
        if (!job.invoker->Start())
        {
          Log::GetInstance().Write(L"Cannot run invoker : %s", job.invokerName.c_str());

        }

      }
      else
      {
        Log::GetInstance().Write(L"Invoker is already running : %s", job.invokerName.c_str());

      }

      job.startNow = false;

    }

  }

  ReadUnlock(&this->m_jobLocker);

  SleepEx(1, FALSE);

  return true;

}
