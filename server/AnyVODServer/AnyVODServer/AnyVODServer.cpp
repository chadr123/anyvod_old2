/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// AnyVODServer.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "Server.h"
#include "ClientProcessor.h"
#include "DataBase.h"
#include "Common.h"
#include "Scheduler.h"
#include "ScheduleInvoker.h"
#include "Log.h"
#include "Util.h"
#include "BuildNumber.h"
#include "..\..\..\common\MiniDump.h"

static volatile HANDLE g_mainThread = INVALID_HANDLE_VALUE;
static volatile bool g_exit = false;

void PrintHelp()
{
  wcout << L">> start             : Start server "<< endl;
  wcout << L"   stop              : Stop server"<< endl;
  wcout << L"   restart           : Restart server"<< endl;
  wcout << L"   status            : View server status"<< endl;
  wcout << L"   env               : View server environments"<< endl;
  wcout << L"   userinfo          : View user accounts"<< endl;
  wcout << L"   schedule          : View schedules"<< endl;
  wcout << L"   statement         : View statements"<< endl;
  wcout << L"   updatemovieinfo   : Update movie informations"<< endl;
  wcout << L"   rebuildmovieinfo  : Rebuild movie informations"<< endl;
  wcout << L"   rebuildmeta       : Rebuild movie metadata"<< endl;
  wcout << L"   restartscheduler  : Restart scheduler"<< endl;
  wcout << L"   version           : Display version"<< endl;
  wcout << L"   exit              : Exit server"<< endl;
  wcout << L"   help              : View commands"<< endl;

}

BOOL WINAPI SignalHandler(DWORD signal)
{
  wcout << endl;

  switch (signal)
  {
  case CTRL_C_EVENT:
    {
      wcout << L">> " << Log::GetInstance().Write(L"Received CTRL-C signal") << endl;
      break;

    }
  case CTRL_CLOSE_EVENT:
    {
      wcout << L">> " << Log::GetInstance().Write(L"Received close signal") << endl;
      break;

    }
  case CTRL_BREAK_EVENT:
    {
      wcout << L">> " << Log::GetInstance().Write(L"Received CTRL-Break signal") << endl;
      break;

    }
  /* 콘솔 프로그램에서는 CTRL_LOGOFF_EVENT, CTRL_SHUTDOWN_EVENT 이벤트는 발생하지 않음 */
  case CTRL_LOGOFF_EVENT:
    {
      wcout << L">> " << Log::GetInstance().Write(L"Received log-off signal") << endl;
      break;

    }
  case CTRL_SHUTDOWN_EVENT:
    {
      wcout << L">> " << Log::GetInstance().Write(L"Received shutdown signal") << endl;
      break;

    }
  default:
    {
      wcout << L">> " << Log::GetInstance().Write(L"Unknown signal : %d", signal) << endl;

      return FALSE;

    }

  }

  g_exit = true;

  CloseHandle(GetStdHandle(STD_INPUT_HANDLE));

  WaitForSingleObject(g_mainThread, INFINITE);

  return TRUE;

}

bool StartServer(Server &server)
{
  bool started = server.Start();

  if (!started)
  {
    wstring fileName;

    Log::GetInstance().GetFileName(&fileName);

    wcout << L">> Starting server failed. Read the log file : " << fileName << endl;

  }

  return started;

}

bool StopServer(Server &server, bool started)
{
  if (started)
  {
    server.Stop();

    started = false;

  }
  else
  {
    wcout << L">> Server is not started" << endl;

  }

  return started;

}

int wmain(int argc, wchar_t* argv[])
{
  cMiniDump::Install(cMiniDump::DUMP_LEVEL_LIGHT);

  g_mainThread = GetCurrentThread();

  if (!SetConsoleCtrlHandler(SignalHandler, TRUE))
  {
    wcout << L">> Cannot install signal handler : \"" << Util::SystemErrorToString(GetLastError()) << L"\"" << endl;

    return 0;

  }

  setlocale(LC_ALL, LOCALE);
  _setmode(_fileno(stdout), _O_U8TEXT);

  wstring command;
  bool started = false;
  Server server;
  TIMECAPS tcs;

  tcs.wPeriodMin = 1;

  if (timeGetDevCaps(&tcs, sizeof(tcs)) == MMSYSERR_NOERROR)
    timeBeginPeriod(tcs.wPeriodMin);

  wcout << L" Server Control Console v" << MAJOR << L"." << MINOR << L"." << PATCH  << L"." << BUILDNUM << L" " << STRFILEVER << L" (" << ARCH_BITS << L")" << endl;
  wcout << L" Built on " << __DATE__ << L" at " << __TIME__ << endl;
  wcout << L" Author : DongRyeol Cha (chadr@dcple.com)" << endl;
  wcout << L"------------------------------------------" << endl;
  wcout << L"Enter \"help\" for commands" << endl;

  if (argc > 1 && wcscmp(argv[1], L"start") == 0)
  {
    wcout << L">> Auto starting..." << endl;

    started = StartServer(server);

  }

  while (true)
  {
    wcout << L"> ";

    getline(wcin, command);

    if (g_exit)
    {
      command = L"exit";

    }
    else
    {
      if (!wcin.good() || command == L"")
      {
        wcin.clear();

        continue;

      }

    }

    if (command == L"start")
    {
      if (started)
      {
        wcout << L">> Server already started" << endl;

      }
      else
      {
        started = StartServer(server);

      }

    }
    else if (command == L"stop")
    {
      started = StopServer(server, started);

    }
    else if (command == L"restart")
    {
      StopServer(server, started);
      started = StartServer(server);

    }
    else if (command == L"exit")
    {
      if (started)
      {
        server.Stop();

      }

      break;

    }
    else if (command == L"help")
    {
      PrintHelp();

    }
    else
    {
      if (started)
      {
        if (command == L"status")
        {
          VECTOR_ANYVOD_CLIENT_STATUS statuses;

          server.GetClientProcessor().GetClientStatus(&statuses);

          wcout << L"--- Status -------------------------" << endl;
          wcout << L"Total Client : " << statuses.size() << endl;


          long long totalRecv = 0;
          long long totalSent = 0;
          long long recvPerSec = 0;
          long long sentPerSec = 0;

          for (size_t i = 0; i < statuses.size(); i++)
          {
            ANYVOD_CLIENT_STATUS status = statuses[i];

            wcout << endl << status.ip << L":" << status.port << L", total " << status.totalRecvBytes << L" byte(s) received and " <<
              L"total " << status.totalSentBytes << L" byte(s) sent" << endl;
            wcout << L"Send buffer count : " << status.sendBufCount << L", Recv buffer count : " << status.recvBufCount << endl;
            wcout << L"Is Stream : " << (status.isStream ? L"true" : L"false");

            totalSent += status.totalSentBytes;
            totalRecv += status.totalRecvBytes;

            recvPerSec += status.recvBytesPerSec;
            sentPerSec += status.sentBytesPerSec;

          }

          recvPerSec /= 1024;
          sentPerSec /= 1024;

          wcout << endl << endl;
          wcout << L"Total received : " << totalRecv << L" byte(s) - " << recvPerSec << L"KB/s" << endl;
          wcout << L"Total sent     : " << totalSent << L" byte(s) - " << sentPerSec << L"KB/s" << endl;
          wcout << L"------------------------------------" << endl;

        }
        else if (command == L"env")
        {
          const ENV_INFO &env = server.GetEnv();

          wcout << L"--- Env ----------------------------" << endl;
          wcout << MOVIE_DIR << L"            : " << env.movie_dir << endl;
          wcout << LOG_DIR << L"              : " << env.log_dir << endl;
          wcout << DB_DIR << L"               : " << env.db_dir << endl;
          wcout << ANALYSER_DIR << L"         : " << env.analyser_dir << endl;
          wcout << MOVIE_EXTS << L"           : ";

          for (size_t i = 0; i < env.movie_exts.size(); i++)
          {
            wcout << env.movie_exts[i] << MOVIE_EXTS_DELIMITER;

          }

          wcout << endl;

          wcout << USE_IPV6 << L"             : " << (env.use_ipv6 ? L"yes" : L"no") << endl;
          wcout << NIC << L"                  : " << env.nic << endl;
          wcout << NIC_STREAM << L"           : " << env.nic_stream << endl;
          wcout << NIC6 << L"                 : " << env.nic6 << endl;
          wcout << NIC6_STREAM << L"          : " << env.nic6_stream << endl;
          wcout << NIC6_SCOPE_ID << L"        : " << env.nic6_scope_id << endl;
          wcout << NIC6_STREAM_SCOPE_ID << L" : " << env.nic6_stream_scope_id << endl;
          wcout << LISTEN_PORT << L"          : " << env.listen_port << endl;
          wcout << LISTEN_STREAM_PORT << L"   : " << env.listen_stream_port << endl;
          wcout << BUFFERING_MULTIPLIER << L" : " << env.buffering_multiplier << endl;
          wcout << L"------------------------------------" << endl;

        }
        else if (command == L"userinfo")
        {
          VECTOR_ANYVOD_USER_INFO userInfos;
          DataBase::DATABASE_FAIL_REASON reason;

          server.GetDataBase().GetUserInfos(&userInfos, &reason);

          wcout << L"--- Users --------------------------" << endl;

          VECTOR_ANYVOD_USER_INFO_ITERATOR i = userInfos.begin();

          for (; i != userInfos.end(); i++)
          {
            ANYVOD_USER_INFO &info = *i;

            wcout << L"ID : " << info.id << L", Name : " << info.name << L", Pass : "
              << info.pass << L", Admin : " << (info.admin ? L"YES" : L"NO") << endl;

          }

          wcout << L"------------------------------------" << endl;

        }
        else if (command == L"statement")
        {
          VECTOR_ANYVOD_STATEMENT_STATUS status;
          StatementCache &cache = server.GetDataBase().GetStatementCache();

          cache.GetStatementStatus(&status);

          wcout << L"--- Statements ---------------------" << endl;

          VECTOR_ANYVOD_STATEMENT_STATUS_ITERATOR i = status.begin();

          for (; i != status.end(); i++)
          {
            ANYVOD_STATEMENT_STATUS &info = *i;
            wstring sql;

            Util::ConvertAsciiToUnicode(info.sql, &sql);

            wcout << L"SQL : " << sql << endl;
            wcout << L"Total count : " << info.totalCount << endl;
            wcout << L"Using count : " << info.usingCount << endl << endl;

          }

          wcout << L"Max statement count : " << cache.GetMaxItemCount() << endl;
          wcout << L"Total statement count : " << status.size() << endl;
          wcout << L"------------------------------------" << endl;

        }
        else if (command == L"schedule")
        {
          Scheduler::VECTOR_JOB_STATUS statuses;

          server.GetScheduler().GetJobStatus(&statuses);

          Scheduler::VECTOR_JOB_STATUS_ITERATOR i = statuses.begin();
          int runCount = 0;
          int notRunCount = 0;

          wcout << L"--- Schedules ----------------------" << endl;

          for (; i != statuses.end(); i++)
          {
            Scheduler::JOB_STATUS &status = *i;

            wcout << L"Name    : " << status.name << endl;
            wcout << L"Status  : ";

            if (status.run)
            {
              runCount++;

              wcout << L"Running" << endl;

            }
            else
            {
              notRunCount++;

              wcout << L"Sleeping" << endl;

            }

            wcout << L"Started : " << status.startedTime.wYear << L"/" << setw(2) << setfill(L'0') <<
              status.startedTime.wMonth << L"/" << setw(2) << setfill(L'0') <<
              status.startedTime.wDay << L" " << setw(2) << setfill(L'0') <<
              status.startedTime.wHour << L":" << setw(2) << setfill(L'0') <<
              status.startedTime.wMinute << L":" << setw(2) << setfill(L'0') <<
              status.startedTime.wSecond << L"." << setw(4) << setfill(L'0') <<
              status.startedTime.wMilliseconds << endl;

            if (!status.run)
            {
              wcout << L"Ended   : " << status.endedTime.wYear << L"/" << setw(2) << setfill(L'0') <<
                status.endedTime.wMonth << L"/" << setw(2) << setfill(L'0') <<
                status.endedTime.wDay << L" " << setw(2) << setfill(L'0') <<
                status.endedTime.wHour << L":" << setw(2) << setfill(L'0') <<
                status.endedTime.wMinute << L":" << setw(2) << setfill(L'0') <<
                status.endedTime.wSecond << L"." << setw(4) << setfill(L'0') <<
                status.endedTime.wMilliseconds << endl;
            }

            wcout << L"Script  : " << status.script << endl << endl;

          }

          wcout << L"Total " << runCount + notRunCount << L", Running " << runCount << L", Sleeping " << notRunCount << endl;
          wcout << L"------------------------------------" << endl;

        }
        else if (command == L"updatemovieinfo")
        {
          wcout << L">> Updating movie informations" << endl;

          if (server.GetDataBase().UpdateMovieInfo(L""))
            wcout << L">> Updating movie informations completed" << endl;
          else
            wcout << L">> Updating movie informations failed" << endl;

        }
        else if (command == L"rebuildmovieinfo")
        {
          wcout << L">> Rebuilding movie informations" << endl;

          if (server.GetDataBase().RebuildMovieData())
            wcout << L">> Rebuilding movie informations completed" << endl;
          else
            wcout << L">> Rebuilding movie informations failed" << endl;

        }
        else if (command == L"rebuildmeta")
        {
          wcout << L">> Rebuilding movie metadata" << endl;

          if (server.GetDataBase().RebuildMovieMetaData())
            wcout << L">> Rebuilding movie metadata completed" << endl;
          else
            wcout << L">> Rebuilding movie metadata failed" << endl;

        }
        else if (command == L"restartscheduler")
        {
          unsigned int index = 0;
          Scheduler &sche = (Scheduler&)server.GetScheduler();

          if (sche.RunningSchedules(&index))
          {
            wcout << L">> Number " << index << L" schedule is running" << endl;

          }

          wcout << L">> Stopping scheduler" << endl;

          sche.Stop();

          wcout << L">> Scheduler stopped" << endl;

          wcout << L">> Starting scheduler" << endl;

          if (sche.Start())
          {
            wcout << L">> Scheduler started" << endl;

          }
          else
          {
            wcout << L">> Restarting scheduler failed" << endl;

          }

        }
        else if (command == L"version")
        {
          wcout << MAJOR << L"." << MINOR << L"." << PATCH  << L"." << BUILDNUM << endl;

        }
        else
        {
          wcout << L">> Invalid Operation : " << command << endl;

        }

      }
      else
      {
         wcout << L">> Server is not started" << endl;

      }

    }

  }

  timeEndPeriod(tcs.wPeriodMin);

  return 0;

}

