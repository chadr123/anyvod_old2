/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "stdafx.h"
#include "MovieTransmitter.h"
#include "Common.h"
#include "PacketHandler.h"
#include "UserPacketHandler.h"
#include "Server.h"
#include "Util.h"
#include "Log.h"

MovieTransmitter::MovieTransmitter()
{

}

MovieTransmitter::~MovieTransmitter()
{

}

void
MovieTransmitter::Init(Client *client)
{
  this->m_client = client;

}

bool
MovieTransmitter::InitializeTransmission(const wstring &filePath, ERROR_TYPE *error)
{
  if (this->m_context.status == STAT_NONE)
  {
    Server *server = this->m_client->GetServer();
    DataBase &db = server->GetDataBase();
    DataBase::DATABASE_FAIL_REASON reason = DataBase::DFR_NONE;

    if (db.HasPermission(filePath, this->m_client->GetUserInfo().id, &reason))
    {
      wstring movieDir = server->GetEnv().movie_dir;
      DataBase::MOVIE_INFO info;
      wstring physicalPath;

      Util::AppendDirSeparator(&movieDir);

      if (db.GetMovieInfo(filePath, &info, &reason))
      {
        Util::BindPath(movieDir, filePath, &physicalPath);

        this->m_context.lastTime = 0;
        this->m_context.lastSentBytes = 0;
        this->m_context.metaDataBlocks = info.metaDataBlocks;
        this->m_context.bitRate = info.bitRate;
        this->m_context.status = STAT_STARTED;
        this->m_context.fileSize = info.totalSize;
        this->m_context.file = CreateFileW(physicalPath.c_str(), GENERIC_READ,
          FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

        if (this->m_context.file != INVALID_HANDLE_VALUE)
        {
          return true;

        }
        else
        {
          *error = ET_CANNOT_OPEN_FILE;

          return false;

        }

      }
      else
      {
        if (reason != DataBase::DFR_NONE)
        {
          *error = DataBase::FailResonToErrorType(reason);

        }
        else
        {
          *error = ET_NOT_SUPPORTED_FORMAT;

        }

        return false;

      }

    }
    else
    {
      if (reason != DataBase::DFR_NONE)
      {
        *error = DataBase::FailResonToErrorType(reason);

      }
      else
      {
        *error = ET_NO_PERMISSION;

      }

      return false;

    }

  }
  else
  {
    *error = ET_TRANSMIT_ALREADY_STARTED;

    return false;

  }

}

bool
MovieTransmitter::StartTransmission(long long startOffset, int size, ERROR_CODE *error)
{
  if (this->m_context.status == STAT_NONE)
  {
    //파일이름을 세팅 한 적이 없다.
    error->type = ET_TRANSMIT_NOT_STARTED;

    return false;

  }
  else
  {
    //만약에 이미 요청한 컨텍스트라면 무시한다.
    if (this->m_context.startOffset == startOffset && this->m_context.totalSize == size)
    {
      error->type = ET_TRANSMIT_ALREADY_TRANSMITING;

      return false;

    }
    else
    {
      if (startOffset <= this->m_context.fileSize)
      {
        LARGE_INTEGER offset;

        offset.QuadPart = startOffset;

        if (SetFilePointerEx(this->m_context.file, offset, nullptr, FILE_BEGIN))
        {
          this->m_context.startOffset = startOffset;
          this->m_context.totalSentBytes = 0;
          this->m_context.totalSize = size;
          this->m_context.status = STAT_TRANSMITTING;

          if (this->m_client->GetServer()->GetEnv().buffering_multiplier <= 0 ||
            Util::DeterminMovieMetaDataRange(startOffset, size, this->m_context.metaDataBlocks))
            this->m_context.fast = true;
          else
            this->m_context.fast = false;

#ifdef _DEBUG
          wcout << L"Start offset is updated " << startOffset << L" size " << size << endl;

          this->m_context.seq = 0;
          this->m_context.prevSent = 0;
          this->m_context.accumTime = 0.0;
#endif // _DEBUG

          return true;

        }
        else
        {
          Log::GetInstance().Write(L"SetFilePointerEx failed : \"%s\"",
            Util::SystemErrorToString(GetLastError()).c_str());

          error->type = ET_CANNOT_START_TRANSMISSION;
          error->errorCode = GetLastError();

          this->ResetContext(STAT_STARTED);

          return false;

        }

      }
      else
      {
        error->type = ET_EXCEED_OFFSET;

        this->ResetContext(STAT_STARTED);

        return false;

      }

    }

  }

}

bool
MovieTransmitter::StopTransmission()
{
  if (this->m_context.status == STAT_TRANSMITTING || this->m_context.status == STAT_STARTED)
  {
    this->ResetContext(STAT_NONE);

    CloseHandle(this->m_context.file);

    this->m_context.file = INVALID_HANDLE_VALUE;
    this->m_context.fileSize = 0ll;
    this->m_context.bitRate = 0;

    this->m_context.lastTime = 0;
    this->m_context.lastSentBytes = 0;

    return true;

  }
  else
  {
    return false;

  }

}

void
MovieTransmitter::ResetContext(STATUS status)
{
  this->m_context.status = status;
  this->m_context.startOffset = -1ll;
  this->m_context.totalSize = -1;
  this->m_context.totalSentBytes = 0;

#ifdef _DEBUG
  this->m_context.seq = 0;
  this->m_context.prevSent = 0;
  this->m_context.accumTime = 0.0;
#endif // _DEBUG

}

void
MovieTransmitter::TransmitStream(DWORD curTime)
{
  char buf[MAX_STREAM_SIZE];
  DWORD read = 0;
  PacketHandler parent(this->m_client);
  UserPacketHandler handler(this->m_client, &parent);

  if (ReadFile(this->m_context.file, buf, MAX_STREAM_SIZE, &read, nullptr))
  {
    long long blockOffset = this->m_context.totalSentBytes + this->m_context.startOffset;
    bool ended = false;

    if (read < MAX_STREAM_SIZE)
    {
      //파일의 끝까지 도달 한 것이다.
      //end 처리 한다.
      DWORD actualRead = read;

      actualRead = min(MAX_STREAM_SIZE, this->m_context.totalSize - this->m_context.totalSentBytes);
      actualRead = min(read, actualRead);

      read = actualRead;

      ended = true;

    }
    else
    {
      if (this->m_context.totalSentBytes + (int)read >= this->m_context.totalSize)
      {
        //다보냄. end 처리 한다.
        ended = true;

        read = this->m_context.totalSize - this->m_context.totalSentBytes;

      }

    }

    bool success = false;

    if (this->m_context.fast)
    {
      success = handler.SendMetaStream(blockOffset, read, buf);

    }
    else
    {
      success = handler.SendStream(blockOffset, read, buf);

    }

    if (success)
    {
      this->m_context.lastTime = curTime;
      this->m_context.lastSentBytes = read;

      this->m_context.totalSentBytes += read;

      if (ended)
      {
        handler.SendStreamEnd();

        this->ResetContext(STAT_STARTED);

#ifdef _DEBUG
        wcout << L"Stream end" << endl;
#endif // _DEBUG

      }

    }
    else
    {
      //연결이 끊겼다. 전송을 중단 한다.
      this->StopTransmission();
      this->ResetContext(STAT_NONE);

    }

  }
  else
  {
    //파일에서 읽지 못한다. 에러처리
    parent.SendError(ET_CANNOT_READ_FILE, GetLastError());

    this->ResetContext(STAT_STARTED);

  }

}

bool
MovieTransmitter::Transmit()
{
  //블럭 만큼 읽어 전송한다.
  if (this->m_context.status == STAT_TRANSMITTING)
  {
    if (this->m_client->IsSendable())
    {
      if (this->m_context.bitRate == 0 || this->m_context.fast)
      {
        this->TransmitStream(0);

#ifdef _DEBUG
        if (this->m_context.fast)
          wcout << L"fast : ";

        wcout <<  this->m_context.seq << L". Next block offset " <<
          this->m_context.totalSentBytes + this->m_context.startOffset <<
          L" Send bytes " << this->m_context.totalSentBytes << endl;

        this->m_context.seq++;
#endif // _DEBUG

      }
      else
      {
        //비트레이트가 존재한다면 전송 속도를 계산하여
        //전송 속도를 넘어서면 스킵한다.
        int multiplier = this->m_client->GetServer()->GetEnv().buffering_multiplier;
        double bitsToByte = (double)this->m_context.bitRate / 8.0 * multiplier;
        DWORD curTime = timeGetTime();
        double deltaTime = curTime - this->m_context.lastTime;

        if (deltaTime < 0)
        {
          this->m_context.lastTime = 0;
          deltaTime = curTime - this->m_context.lastTime;

        }

        double deltaToSend = bitsToByte * deltaTime / 1000.0;

        if (this->m_context.lastSentBytes <= deltaToSend)
        {
          this->TransmitStream(curTime);

#ifdef _DEBUG
          if (this->m_context.accumTime >= 1000.0)
          {
            wcout <<  this->m_context.seq << L". Next block offset " <<
              this->m_context.totalSentBytes + this->m_context.startOffset <<
              L" Send bytes " << this->m_context.totalSentBytes - this->m_context.prevSent <<
              L", Delta time " << this->m_context.accumTime << endl;

            this->m_context.seq++;

            this->m_context.prevSent = this->m_context.totalSentBytes;
            this->m_context.accumTime = deltaTime;

          }
          else
          {
            this->m_context.accumTime += deltaTime;

          }
#endif // _DEBUG

        }
        else
        {
#ifdef _DEBUG
          wcout << L"Speed exceeded" << endl;
#endif // _DEBUG
          return false;

        }

      }

    }
    else
    {
#ifdef _DEBUG
      wcout << L"Transmission pending" << endl;
#endif // _DEBUG

      return false;

    }

  }

  return this->m_context.status == STAT_TRANSMITTING;

}
