/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "stdafx.h"
#include "AnalyserLoader.h"
#include "Common.h"
#include "Util.h"
#include "Log.h"
#include "FindHandleCloser.h"
#include "..\..\..\common\Analyser.h"

AnalyserLoader::AnalyserLoader()
{

}

AnalyserLoader::~AnalyserLoader()
{
  this->CleanUp();

}

void
AnalyserLoader::CleanUp()
{
  MAP_ANYVOD_ANALYSER_ITERATOR i = this->m_analysers.begin();

  for (; i != this->m_analysers.end(); i++)
  {
    Analyser* analyser = i->second;

    FreeLibrary(analyser->GetHandle());

  }

  this->m_analysers.clear();

}

bool
AnalyserLoader::LoadAnalysers(const wstring &path)
{
  this->CleanUp();

  wstring searchPath = path;

  Util::AppendDirSeparator(&searchPath);
  searchPath += L"*.dll";

  WIN32_FIND_DATAW findData;
  HANDLE find = FindFirstFileW(searchPath.c_str(), &findData);
  FindHandleCloser closer(find);

  if (find == INVALID_HANDLE_VALUE)
  {
    Log::GetInstance().Write(L"FindFirstFileW failed : \"%s\" Path : %s",
      Util::SystemErrorToString(GetLastError()).c_str(), searchPath.c_str());

  }
  else
  {
    do
    {
      wstring filePath = path;

      Util::AppendDirSeparator(&filePath);
      Util::BindPath(filePath, findData.cFileName, &filePath);

      HMODULE handle = LoadLibraryW(filePath.c_str());

      if (handle == NULL)
      {
        Log::GetInstance().Write(L"LoadLibraryW failed : \"%s\" Path : %s",
          Util::SystemErrorToString(GetLastError()).c_str(), filePath.c_str());

        continue;

      }
      else
      {
        Analyser* (*GetAnalyserInstance) ();
        GetAnalyserInstance = (Analyser* (*) ())GetProcAddress(handle, ANALYSER_INSTANCE_GETTER_FUNC_NAME);

        if (GetAnalyserInstance == nullptr)
        {
          Log::GetInstance().Write(L"Can not get %S function : %s",
            ANALYSER_INSTANCE_GETTER_FUNC_NAME, findData.cFileName);

          FreeLibrary(handle);

        }
        else
        {
          Analyser *analyer = GetAnalyserInstance();

          if (analyer == nullptr)
          {
            Log::GetInstance().Write(L"Can not get analyser instance : %s", findData.cFileName);

            FreeLibrary(handle);

          }
          else
          {
            wchar_t name[MAX_ANALYSER_NAME_SIZE];
            wchar_t desc[MAX_ANALYSER_DESC_SIZE];

            analyer->GetName(name);
            analyer->GetDescription(desc);

            this->m_analysers.insert(make_pair(name, analyer));

            Log::GetInstance().Write(L"Analyser loaded : %s %s (%s)",
              findData.cFileName, desc, Util::GetModuleVersion(handle, true).c_str());

          }

        }

      }

    } while (FindNextFileW(find, &findData));

    if (GetLastError() != ERROR_NO_MORE_FILES)
    {
      Log::GetInstance().Write(L"FindNextFileW failed : \"%s\" Path : %s",
        Util::SystemErrorToString(GetLastError()).c_str(), path.c_str());

    }
    else
    {
      return true;

    }

  }

  return false;

}

Analyser*
AnalyserLoader::GetAnalyser(const wstring &name)
{
  wstring tname = name;

  transform(tname.begin(), tname.end(), tname.begin(), towlower);

  MAP_ANYVOD_ANALYSER_ITERATOR i = this->m_analysers.find(tname);

  if (i == this->m_analysers.end())
    return nullptr;
  else
    return i->second;

}
