/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "..\..\..\common\types.h"

class Client;
struct ERROR_CODE;

class MovieTransmitter
{
public:
  enum STATUS
  {
    STAT_NONE,
    STAT_STARTED,
    STAT_TRANSMITTING,

  };

  struct MOVIE_TRANSMIT_CONTEXT
  {
    MOVIE_TRANSMIT_CONTEXT()
    {
      status = STAT_NONE;
      file = INVALID_HANDLE_VALUE;
      startOffset = -1ll;
      totalSize = -1;
      totalSentBytes = 0;
      bitRate = 0;
      lastTime = 0;
      lastSentBytes = 0;
      fileSize = 0ll;
      fast = false;

#ifdef _DEBUG
      seq = 0;
      prevSent = 0;
      accumTime = 0.0;
#endif // _DEBUG

    }

    ~MOVIE_TRANSMIT_CONTEXT()
    {
      CloseHandle(file);

    }

    STATUS status;
    HANDLE file;
    long long startOffset;
    int totalSize;
    int totalSentBytes;
    int bitRate;
    int lastSentBytes;
    DWORD lastTime;
    long long fileSize;
    bool fast;
    VECTOR_ANYVOD_ANALYSED_DATA_BLOCK metaDataBlocks;

#ifdef _DEBUG
    int seq;
    int prevSent;
    double accumTime;
#endif // _DEBUG

  };

  MovieTransmitter();
  ~MovieTransmitter();

  void Init(Client *client);

  bool InitializeTransmission(const wstring &filePath, ERROR_TYPE *error);

  bool StartTransmission(long long startOffset, int size, ERROR_CODE *error);
  bool StopTransmission();

  bool Transmit();

private:
  void ResetContext(STATUS status);
  void TransmitStream(DWORD curTime);

private:
  MOVIE_TRANSMIT_CONTEXT m_context;
  Client *m_client;

};
