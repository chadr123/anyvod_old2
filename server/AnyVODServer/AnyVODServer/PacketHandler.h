/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "Common.h"

class Client;
union ANYVOD_PACKET;

class PacketHandler
{
public:

  friend class UserPacketHandler;
  friend class AdminPacketHandler;

  PacketHandler(Client *client);
  ~PacketHandler();

  bool HandlePacket(const ANYVOD_PACKET &packet);
  bool SendError(ERROR_TYPE type, DWORD errorCode = 0);

private:
  void ConvertToNativePath(const wstring &path, wstring *ret);
  bool CheckTicket(const string &ticket);

private:
  Client *m_client;

};
