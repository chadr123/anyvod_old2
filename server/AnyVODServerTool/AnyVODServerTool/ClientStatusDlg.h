/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "ParentDlg.h"

struct ANYVOD_CLIENT_STATUS;
union ANYVOD_PACKET;

// CClientStatusDlg 대화 상자입니다.

class CClientStatusDlg : public CParentDlg
{
	DECLARE_DYNAMIC(CClientStatusDlg)

public:
	CClientStatusDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CClientStatusDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CLIENT_STATUS_DIALOG };

private:
  struct ITEM_KEY
  {
    string ticket;
    bool isStream;

    bool operator < (const ITEM_KEY& rhs) const
    {
      return (ticket < rhs.ticket || !(rhs.ticket < ticket) && isStream < rhs.isStream);

    }

  };

private:
  unsigned int m_itemCount;
  unsigned int m_itemCurCount;
  map<ITEM_KEY, ANYVOD_CLIENT_STATUS*> m_items;

private:
  void DeleteClientStatusItems();
  void ToggleClientStatusItemControls();
  bool ProcessPacket();
  void UpdateTotal();
  void ViewDetail();
  void InsertItems();
  virtual bool HandlePacket(ANYVOD_PACKET *packet);
  virtual void InitControls();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
  CListCtrl m_statusList;
  CStatic m_count;
  CButton m_logout;
  CStatic m_id;
  CStatic m_ip;
  CStatic m_state;
  CStatic m_port;
  CStatic m_totalRecv;
  CStatic m_totalSent;
  CStatic m_recv;
  CStatic m_sent;
  CStatic m_recvBufferCount;
  CStatic m_sentBufferCount;
  CStatic m_lastPacket;
  CStatic m_stream;

  afx_msg void OnBnClickedButtonClientStatusRefresh();
  afx_msg void OnClose();
  afx_msg void OnBnClickedButtonClientStatusClose();
  afx_msg void OnNMClickListClientStatusStatuslist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnBnClickedButtonClientStatusLogout();
  afx_msg void OnNMReturnListClientStatusStatuslist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMSetfocusListClientStatusStatuslist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnLvnItemchangedListClientStatusStatuslist(NMHDR *pNMHDR, LRESULT *pResult);
};
