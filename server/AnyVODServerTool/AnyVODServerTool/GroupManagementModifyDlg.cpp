/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// GroupManagementModifyDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AnyVODServerTool.h"
#include "GroupManagementModifyDlg.h"
#include "GroupManagementDlg.h"
#include "AnyVODServerToolDlg.h"
#include "Socket.h"
#include "Util.h"

// CGroupManagementModifyDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CGroupManagementModifyDlg, CDialog)

CGroupManagementModifyDlg::CGroupManagementModifyDlg(CWnd* pParent /*=nullptr*/)
	: CParentDlg(CGroupManagementModifyDlg::IDD, pParent)
{

}

CGroupManagementModifyDlg::~CGroupManagementModifyDlg()
{
}

void CGroupManagementModifyDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_EDIT_GROUP_MANAGEMENT_MODIFY_NAME, m_name);
  DDX_Control(pDX, IDC_BUTTON_GROUP_MANAGEMENT_STOP_MULTIPLE_MODIFY, m_stopMultiModify);
}


BEGIN_MESSAGE_MAP(CGroupManagementModifyDlg, CDialog)
  ON_BN_CLICKED(IDC_BUTTON_GROUP_MANAGEMENT_MODIFY_CONFIRM_EXIST, &CGroupManagementModifyDlg::OnBnClickedButtonGroupManagementModifyConfirmExist)
  ON_BN_CLICKED(IDC_BUTTON_GROUP_MANAGEMENT_MODIFY_MODIFY, &CGroupManagementModifyDlg::OnBnClickedButtonGroupManagementModifyModify)
  ON_BN_CLICKED(IDC_BUTTON_GROUP_MANAGEMENT_MODIFY_CANCEL, &CGroupManagementModifyDlg::OnBnClickedButtonGroupManagementModifyCancel)
  ON_BN_CLICKED(IDC_BUTTON_GROUP_MANAGEMENT_STOP_MULTIPLE_MODIFY, &CGroupManagementModifyDlg::OnBnClickedButtonGroupManagementStopMultipleModify)
END_MESSAGE_MAP()

void CGroupManagementModifyDlg::InitControls()
{
  CGroupManagementDlg *dlg = (CGroupManagementDlg*)this->m_pParentWnd;
  CListCtrl &list = dlg->GetGroupList();
  POSITION pos = list.GetFirstSelectedItemPosition();

  if (list.GetSelectedCount() > 1)
  {
    this->m_stopMultiModify.ShowWindow(SW_SHOW);

  }
  else
  {
    this->m_stopMultiModify.ShowWindow(SW_HIDE);

  }

  if (pos != NULL)
  {
    int item = list.GetNextSelectedItem(pos);
    this->m_name.SetLimitText(MAX_GROUP_NAME_CHAR_SIZE);
    this->m_name.SetWindowText(list.GetItemText(item, 0));

    ANYVOD_GROUP_INFO *info =  (ANYVOD_GROUP_INFO*)list.GetItemData(item);

    this->m_pid = info->pid;

  }
  else
  {
    AfxMessageBox(L"선택된 그룹이 없습니다.");

    this->EndDialog(IDCANCEL);

  }


}

bool CGroupManagementModifyDlg::HandlePacket(ANYVOD_PACKET *packet)
{
  ANYVOD_PACKET_TYPE type = (ANYVOD_PACKET_TYPE)ntohl(packet->s2c_header.header.type);

  if (type == PT_ADMIN_S2C_GROUP_MODIFY)
  {
    if (packet->s2c_header.success)
    {
      CGroupManagementDlg *dlg = (CGroupManagementDlg*)this->m_pParentWnd;
      CString name;
      CListCtrl &list = dlg->GetGroupList();
      POSITION pos = list.GetFirstSelectedItemPosition();
      int item = list.GetNextSelectedItem(pos);

      this->m_name.GetWindowText(name);

      list.SetItemText(item, 0, name);

      ANYVOD_GROUP_INFO *info = (ANYVOD_GROUP_INFO*)list.GetItemData(item);

      info->name = name.GetString();

      this->EndDialog(IDOK);

    }

  }
  else if (type == PT_ADMIN_S2C_GROUP_EXIST)
  {
    if (packet->s2c_header.success)
    {
      if (packet->admin_s2c_group_exist.exist)
      {
        AfxMessageBox(L"사용 불가능한 그룹 이름입니다.");

      }
      else
      {
        AfxMessageBox(L"사용 가능한 그룹 이름입니다.");

      }

    }

  }

  return false;

}

void CGroupManagementModifyDlg::ProcessPacket()
{
  bool nextProcess = false;
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd->GetParent();
  Socket &socket = dlg->GetSocket();

  do
  {
    if (!Util::HandlePacket(socket, this, &nextProcess, nullptr))
    {
      this->EndDialog(IDCLOSE);

      break;

    }

  } while (nextProcess);

}

// CGroupManagementModifyDlg 메시지 처리기입니다.

BOOL CGroupManagementModifyDlg::PreTranslateMessage(MSG* pMsg)
{
  return CDialog::PreTranslateMessage(pMsg);
}

void CGroupManagementModifyDlg::OnOK()
{
  this->OnBnClickedButtonGroupManagementModifyModify();

  //CParentDlg::OnOK();
}

void CGroupManagementModifyDlg::OnCancel()
{
  this->OnBnClickedButtonGroupManagementModifyCancel();

  CParentDlg::OnCancel();
}

void CGroupManagementModifyDlg::OnBnClickedButtonGroupManagementModifyConfirmExist()
{
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd->GetParent();
  CString name;

  this->m_name.GetWindowText(name);

  name.Trim();

  if (name.GetLength() > 0)
  {
    dlg->GetSocket().SendGroupExist(name);
    this->ProcessPacket();

  }
  else
  {
    AfxMessageBox(L"이름을 입력해 주세요.");

    this->m_name.SetFocus();

  }

}

void CGroupManagementModifyDlg::OnBnClickedButtonGroupManagementModifyModify()
{
  CString name;

  this->m_name.GetWindowText(name);

  name.Trim();

  if (name.GetLength() == 0)
  {
    AfxMessageBox(L"이름을 입력해 주세요.");

    this->m_name.SetFocus();

    return;


  }

  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd->GetParent();
  ANYVOD_GROUP_INFO info;

  info.name = name.GetString();

  dlg->GetSocket().SendGroupModify(this->m_pid, info);
  this->ProcessPacket();

}

void CGroupManagementModifyDlg::OnBnClickedButtonGroupManagementModifyCancel()
{
  this->EndDialog(IDCANCEL);

}

void CGroupManagementModifyDlg::OnBnClickedButtonGroupManagementStopMultipleModify()
{
  this->EndDialog(IDABORT);

}
