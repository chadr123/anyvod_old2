/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// UserGroupAuthDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AnyVODServerTool.h"
#include "UserGroupAuthDlg.h"
#include "AnyVODServerToolDlg.h"
#include "Socket.h"
#include "Util.h"

// CUserGroupAuthDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CUserGroupAuthDlg, CDialog)

CUserGroupAuthDlg::CUserGroupAuthDlg(CWnd* pParent /*=nullptr*/)
	: CParentDlg(CUserGroupAuthDlg::IDD, pParent)
{

}

CUserGroupAuthDlg::~CUserGroupAuthDlg()
{
}

void CUserGroupAuthDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_LIST_USER_GROUP_AUTH_USERLIST, m_userList);
  DDX_Control(pDX, IDC_LIST_USER_GROUP_AUTH_GROUPLIST, m_groupList);
  DDX_Control(pDX, IDC_LIST_USER_GROUP_AUTH_AUTHLIST, m_authList);
  DDX_Control(pDX, IDC_BUTTON_USER_GROUP_AUTH_ADDAUTH, m_addAuth);
  DDX_Control(pDX, IDC_BUTTON_USER_GROUP_AUTH_DELETEAUTH, m_deleteAuth);
}


BEGIN_MESSAGE_MAP(CUserGroupAuthDlg, CDialog)
  ON_BN_CLICKED(IDC_BUTTON_USER_GROUP_AUTH_CLOSE, &CUserGroupAuthDlg::OnBnClickedButtonUserGroupAuthClose)
  ON_WM_CLOSE()
  ON_NOTIFY(NM_CLICK, IDC_LIST_USER_GROUP_AUTH_USERLIST, &CUserGroupAuthDlg::OnNMClickListUserGroupAuthUserlist)
  ON_NOTIFY(NM_CLICK, IDC_LIST_USER_GROUP_AUTH_GROUPLIST, &CUserGroupAuthDlg::OnNMClickListUserGroupAuthGrouplist)
  ON_NOTIFY(NM_CLICK, IDC_LIST_USER_GROUP_AUTH_AUTHLIST, &CUserGroupAuthDlg::OnNMClickListUserGroupAuthAuthlist)
  ON_NOTIFY(LVN_GETDISPINFO, IDC_LIST_USER_GROUP_AUTH_USERLIST, &CUserGroupAuthDlg::OnLvnGetdispinfoListUserGroupAuthUserlist)
  ON_BN_CLICKED(IDC_BUTTON_USER_GROUP_AUTH_ADDAUTH, &CUserGroupAuthDlg::OnBnClickedButtonUserGroupAuthAddauth)
  ON_BN_CLICKED(IDC_BUTTON_USER_GROUP_AUTH_DELETEAUTH, &CUserGroupAuthDlg::OnBnClickedButtonUserGroupAuthDeleteauth)
  ON_BN_CLICKED(IDC_BUTTON_USER_GROUP_AUTH_REFRESH, &CUserGroupAuthDlg::OnBnClickedButtonUserGroupAuthRefresh)
  ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_USER_GROUP_AUTH_USERLIST, &CUserGroupAuthDlg::OnLvnItemchangedListUserGroupAuthUserlist)
  ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_USER_GROUP_AUTH_GROUPLIST, &CUserGroupAuthDlg::OnLvnItemchangedListUserGroupAuthGrouplist)
  ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_USER_GROUP_AUTH_AUTHLIST, &CUserGroupAuthDlg::OnLvnItemchangedListUserGroupAuthAuthlist)
  ON_NOTIFY(NM_RETURN, IDC_LIST_USER_GROUP_AUTH_USERLIST, &CUserGroupAuthDlg::OnNMReturnListUserGroupAuthUserlist)
  ON_NOTIFY(NM_RETURN, IDC_LIST_USER_GROUP_AUTH_GROUPLIST, &CUserGroupAuthDlg::OnNMReturnListUserGroupAuthGrouplist)
  ON_NOTIFY(NM_RETURN, IDC_LIST_USER_GROUP_AUTH_AUTHLIST, &CUserGroupAuthDlg::OnNMReturnListUserGroupAuthAuthlist)
END_MESSAGE_MAP()

void CUserGroupAuthDlg::InitControls()
{
  this->m_groupList.InsertColumn(0, L"이름", LVCFMT_LEFT, 230);

  this->m_groupList.SetExtendedStyle(LVS_EX_GRIDLINES| LVS_EX_FULLROWSELECT);
  this->m_groupList.SetImageList(&Util::GetIconImageList(), LVSIL_SMALL);

  this->m_userList.InsertColumn(0, L"아이디", LVCFMT_LEFT, 105);
  this->m_userList.InsertColumn(1, L"이름", LVCFMT_LEFT, 75);
  this->m_userList.InsertColumn(2, L"관리자", LVCFMT_LEFT, 50);

  this->m_userList.SetExtendedStyle(LVS_EX_GRIDLINES| LVS_EX_FULLROWSELECT);
  this->m_userList.SetImageList(&Util::GetIconImageList(), LVSIL_SMALL);

  this->m_authList.InsertColumn(0, L"사용자 아이디", LVCFMT_LEFT, 196);
  this->m_authList.InsertColumn(1, L"그룹 이름", LVCFMT_LEFT, 196);

  this->m_authList.SetExtendedStyle(LVS_EX_GRIDLINES| LVS_EX_FULLROWSELECT);
  this->m_authList.SetImageList(&Util::GetIconImageList(), LVSIL_SMALL);

  this->OnBnClickedButtonUserGroupAuthRefresh();

}

void CUserGroupAuthDlg::ToggleAddControls()
{
  bool select = this->m_userList.GetSelectedCount() != 0 && this->m_groupList.GetSelectedCount() != 0;

  this->m_addAuth.EnableWindow(select);

  if (this->GetFocus() == nullptr)
  {
    this->SetFocus();

  }

}

void CUserGroupAuthDlg::ToggleDeleteControls()
{
  bool select = this->m_authList.GetSelectedCount() != 0;

  this->m_deleteAuth.EnableWindow(select);

  if (this->GetFocus() == nullptr)
  {
    this->SetFocus();

  }

}

void CUserGroupAuthDlg::DeleteUserItems()
{
  for (int i = 0; i < this->m_userList.GetItemCount(); i++)
  {
    delete (ANYVOD_USER_INFO*)this->m_userList.GetItemData(i);

  }

  this->m_userList.DeleteAllItems();

}

void CUserGroupAuthDlg::DeleteUserGroupAuthItems()
{
  for (int i = 0; i < this->m_authList.GetItemCount(); i++)
  {
    delete (ANYVOD_USER_GROUP_INFO*)this->m_authList.GetItemData(i);

  }

  this->m_authList.DeleteAllItems();

}

void CUserGroupAuthDlg::DeleteGroupItems()
{
  for (int i = 0; i < this->m_groupList.GetItemCount(); i++)
  {
    delete (ANYVOD_GROUP_INFO*)this->m_groupList.GetItemData(i);

  }

  this->m_groupList.DeleteAllItems();

}

void
CUserGroupAuthDlg::AddAuthItemToList(ANYVOD_USER_GROUP_INFO *info)
{
  LVITEM lvi;

  lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
  lvi.iItem = this->m_authItemCurCount;
  lvi.iSubItem = 0;
  lvi.pszText = (LPWSTR)info->userID.c_str();
  lvi.lParam = (LPARAM)info;
  lvi.iImage = ICON_INDEX_USER_GROUP;

  this->m_authList.InsertItem(&lvi);

  lvi.mask = LVIF_TEXT;
  lvi.iSubItem = 1;
  lvi.pszText = (LPWSTR)info->groupName.c_str();

  this->m_authList.SetItem(&lvi);

  this->m_authItemCurCount++;

}

bool CUserGroupAuthDlg::HandlePacket(ANYVOD_PACKET *packet)
{
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  switch (ntohl(packet->s2c_header.header.type))
  {
  case PT_ADMIN_S2C_GROUPLIST:
    {
      if (packet->s2c_header.success)
      {
        this->m_groupItemCount = ntohl(packet->admin_s2c_grouplist.count);
        this->m_groupItemCurCount = 0;
        this->DeleteGroupItems();

        return this->m_groupItemCount > 0;

      }

      break;

    }
  case PT_ADMIN_S2C_GROUP_ITEM:
    {
      if (packet->s2c_header.success)
      {
        ANYVOD_GROUP_INFO *info = new ANYVOD_GROUP_INFO;
        ANYVOD_PACKET_GROUP_INFO &item = packet->admin_s2c_group_item.detail;

        Util::ConvertUTF8ToUnicode((char*)item.name, MAX_GROUP_NAME_SIZE, &info->name);
        info->pid = ntohll(packet->admin_s2c_group_item.pid);

        LVITEM lvi;

        lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
        lvi.iItem = this->m_groupItemCount;
        lvi.iSubItem = 0;
        lvi.pszText = (LPWSTR)info->name.c_str();
        lvi.lParam = (LPARAM)info;
        lvi.iImage = ICON_INDEX_GROUP;

        this->m_groupList.InsertItem(&lvi);

        this->m_groupItemCurCount++;

        if (this->m_groupItemCurCount == this->m_groupItemCount)
        {
          return false;

        }
        else
        {
          return true;

        }

      }

      break;

    }
  case PT_ADMIN_S2C_USERLIST:
    {
      if (packet->s2c_header.success)
      {
        this->m_userItemCount = ntohl(packet->admin_s2c_userlist.count);
        this->m_userItemCurCount = 0;
        this->DeleteUserItems();

        return this->m_userItemCount > 0;

      }

      break;

    }
  case PT_ADMIN_S2C_USER_ITEM:
    {
      if (packet->s2c_header.success)
      {
        ANYVOD_USER_INFO *info = new ANYVOD_USER_INFO;
        ANYVOD_PACKET_USER_INFO &item = packet->admin_s2c_user_item.detail;

        info->admin = item.admin != 0;
        Util::ConvertUTF8ToUnicode((char*)item.id, MAX_ID_SIZE, &info->id);
        Util::ConvertUTF8ToUnicode((char*)item.pass, MAX_PASS_SIZE, &info->pass);
        Util::ConvertUTF8ToUnicode((char*)item.name, MAX_NAME_SIZE, &info->name);
        info->pid = ntohll(packet->admin_s2c_user_item.pid);

        LPTSTR adminText = nullptr;

        if (info->admin)
        {
          adminText = L"예";

        }
        else
        {
          adminText = L"아니오";

        }

        LVITEM lvi;

        lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
        lvi.iItem = this->m_userItemCurCount;
        lvi.iSubItem = 0;
        lvi.pszText = (LPWSTR)info->id.c_str();
        lvi.lParam = (LPARAM)info;
        lvi.iImage = I_IMAGECALLBACK;

        this->m_userList.InsertItem(&lvi);

        lvi.mask = LVIF_TEXT;
        lvi.iSubItem = 1;
        lvi.pszText = (LPWSTR)info->name.c_str();

        this->m_userList.SetItem(&lvi);

        lvi.mask = LVIF_TEXT;
        lvi.iSubItem = 2;
        lvi.pszText = adminText;

        this->m_userList.SetItem(&lvi);

        this->m_userItemCurCount++;

        if (this->m_userItemCurCount == this->m_userItemCount)
        {
          return false;

        }
        else
        {
          return true;

        }

      }

      break;

    }
  case PT_ADMIN_S2C_USER_GROUPLIST:
    {
      if (packet->s2c_header.success)
      {
        this->m_authItemCount = ntohl(packet->admin_s2c_user_grouplist.count);
        this->m_authItemCurCount = 0;
        this->DeleteUserGroupAuthItems();

        return this->m_authItemCount > 0;

      }

      break;

    }
  case PT_ADMIN_S2C_USER_GROUP_ITEM:
    {
      if (packet->s2c_header.success)
      {
        ANYVOD_USER_GROUP_INFO *info = new ANYVOD_USER_GROUP_INFO;
        ANYVOD_PACKET_USER_GROUP_INFO &item = packet->admin_s2c_user_group_item.detail;

        Util::ConvertUTF8ToUnicode((char*)item.userID, MAX_ID_SIZE, &info->userID);
        Util::ConvertUTF8ToUnicode((char*)item.groupName, MAX_GROUP_NAME_SIZE, &info->groupName);
        info->pid = ntohll(packet->admin_s2c_user_group_item.pid);

        this->AddAuthItemToList(info);

        if (this->m_authItemCurCount == this->m_authItemCount)
        {
          return false;

        }
        else
        {
          return true;

        }

      }

      break;

    }
  case PT_ADMIN_S2C_USER_GROUP_ADD:
    {
      if (packet->s2c_header.success)
      {
        ANYVOD_USER_GROUP_INFO *info = new ANYVOD_USER_GROUP_INFO;

        info->groupName = this->m_lastAddGroupName;
        info->userID = this->m_lastAddUserID;
        info->pid = ntohll(packet->admin_s2c_user_group_add.pid);

        this->AddAuthItemToList(info);

      }

      break;

    }
  case PT_ADMIN_S2C_USER_GROUP_DELETE:
    {
      break;

    }

  }

  return false;

}

bool CUserGroupAuthDlg::ProcessPacket()
{
  bool nextProcess = false;
  bool success = false;
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;
  Socket &socket = dlg->GetSocket();

  do
  {
    if (!Util::HandlePacket(socket, this, &nextProcess, &success))
    {
      this->DeleteGroupItems();
      this->DeleteUserGroupAuthItems();
      this->DeleteUserItems();

      this->EndDialog(IDCLOSE);

      break;

    }

  } while (nextProcess);

  return success;

}

void
CUserGroupAuthDlg::DeselectAddControls()
{
  this->m_userList.SetItemState(-1, 0, LVIS_SELECTED | LVIS_FOCUSED);
  this->m_groupList.SetItemState(-1, 0, LVIS_SELECTED | LVIS_FOCUSED);

  this->ToggleAddControls();
  this->ToggleDeleteControls();

}

void
CUserGroupAuthDlg::DeselectDeleteControls()
{
  this->m_authList.SetItemState(-1, 0, LVIS_SELECTED | LVIS_FOCUSED);

  this->ToggleAddControls();
  this->ToggleDeleteControls();

}

// CUserGroupAuthDlg 메시지 처리기입니다.

void CUserGroupAuthDlg::OnBnClickedButtonUserGroupAuthClose()
{
  this->DeleteGroupItems();
  this->DeleteUserGroupAuthItems();
  this->DeleteUserItems();

  this->EndDialog(IDCANCEL);

}

void CUserGroupAuthDlg::OnClose()
{
  this->DeleteGroupItems();
  this->DeleteUserGroupAuthItems();
  this->DeleteUserItems();

  CParentDlg::OnClose();
}

void CUserGroupAuthDlg::OnNMClickListUserGroupAuthUserlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  this->DeselectDeleteControls();

  *pResult = 0;
}

void CUserGroupAuthDlg::OnNMClickListUserGroupAuthGrouplist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  this->DeselectDeleteControls();

  *pResult = 0;
}

void CUserGroupAuthDlg::OnNMClickListUserGroupAuthAuthlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  this->DeselectAddControls();

  *pResult = 0;
}

void CUserGroupAuthDlg::OnLvnGetdispinfoListUserGroupAuthUserlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
  ANYVOD_USER_INFO *info = (ANYVOD_USER_INFO*)pDispInfo->item.lParam;

  pDispInfo->item.iImage = info->admin ? ICON_INDEX_ADMIN : ICON_INDEX_USER;

  *pResult = 0;
}

void CUserGroupAuthDlg::OnBnClickedButtonUserGroupAuthAddauth()
{
  POSITION userPos = this->m_userList.GetFirstSelectedItemPosition();
  bool selectSingle = this->m_userList.GetSelectedCount() == 1;

  if (userPos != NULL)
  {
    CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

    while (userPos)
    {
      int userSelect = this->m_userList.GetNextSelectedItem(userPos);
      ANYVOD_USER_INFO *userInfo = (ANYVOD_USER_INFO*)this->m_userList.GetItemData(userSelect);
      POSITION groupPos = this->m_groupList.GetFirstSelectedItemPosition();

      if (groupPos != NULL)
      {
        while (groupPos)
        {
          int groupSelect = this->m_groupList.GetNextSelectedItem(groupPos);
          ANYVOD_GROUP_INFO *groupInfo = (ANYVOD_GROUP_INFO*)this->m_groupList.GetItemData(groupSelect);

          dlg->GetSocket().SendUserGroupAdd(userInfo->pid, groupInfo->name);

          this->m_lastAddGroupName = groupInfo->name;
          this->m_lastAddUserID = userInfo->id;

          if (this->ProcessPacket())
          {
            this->m_authItemCount++;

            if (userPos == NULL)
            {
              this->m_groupList.SetItemState(groupSelect, 0, LVIS_SELECTED | LVIS_FOCUSED);
              groupPos = this->m_groupList.GetFirstSelectedItemPosition();

            }

          }
          else
          {
            Util::ListControlScrollToEnd(this->m_authList);

            this->ToggleAddControls();
            this->ToggleDeleteControls();

            if (userPos != NULL || groupPos != NULL)
            {
              if (AfxMessageBox(L"권한을 추가 하는 도중 오류가 발생 했습니다. 계속 하시겠습니까?", MB_YESNO) == IDNO)
              {
                return;

              }

            }

          }

        }

        if (!selectSingle)
        {
          this->m_userList.SetItemState(userSelect, 0, LVIS_SELECTED | LVIS_FOCUSED);
          userPos = this->m_userList.GetFirstSelectedItemPosition();

        }

      }
      else
      {
        AfxMessageBox(L"그룹을 선택 해 주세요.");

        break;

      }

    }

    Util::ListControlScrollToEnd(this->m_authList);

    this->ToggleAddControls();
    this->ToggleDeleteControls();

  }
  else
  {
    AfxMessageBox(L"사용자를 선택 해 주세요.");

  }

}

void CUserGroupAuthDlg::OnBnClickedButtonUserGroupAuthDeleteauth()
{
  POSITION pos = this->m_authList.GetFirstSelectedItemPosition();

  if (pos != NULL)
  {
    if (AfxMessageBox(L"권한을 삭제 하시겠습니까?", MB_YESNO) == IDYES)
    {
      CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

      while (pos)
      {
        int item = this->m_authList.GetNextSelectedItem(pos);
        ANYVOD_USER_GROUP_INFO *info = (ANYVOD_USER_GROUP_INFO*)this->m_authList.GetItemData(item);

        dlg->GetSocket().SendUserGroupDelete(info->pid);

        if (this->ProcessPacket())
        {
          delete info;

          this->m_authItemCount--;
          this->m_authItemCurCount--;

          this->m_authList.DeleteItem(item);

          pos = this->m_authList.GetFirstSelectedItemPosition();

        }
        else
        {
          if (pos != NULL)
          {
            if (AfxMessageBox(L"권한을 삭제 하는 도중 오류가 발생 했습니다. 계속 하시겠습니까?", MB_YESNO) == IDNO)
            {
              break;

            }

          }

        }

      }

    }

    this->ToggleAddControls();
    this->ToggleDeleteControls();

  }
  else
  {
    AfxMessageBox(L"선택된 권한이 없습니다.");

  }

}

void CUserGroupAuthDlg::OnBnClickedButtonUserGroupAuthRefresh()
{
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  dlg->GetSocket().SendGroupList();
  this->ProcessPacket();

  dlg->GetSocket().SendUserList();
  this->ProcessPacket();

  dlg->GetSocket().SendUserGroupList();
  this->ProcessPacket();

  this->ToggleAddControls();
  this->ToggleDeleteControls();
}

void CUserGroupAuthDlg::OnLvnItemchangedListUserGroupAuthUserlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

  if (pNMLV->uOldState == 0)
  {
    this->DeselectDeleteControls();

  }

  *pResult = 0;
}

void CUserGroupAuthDlg::OnLvnItemchangedListUserGroupAuthGrouplist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

  if (pNMLV->uOldState == 0)
  {
    this->DeselectDeleteControls();

  }

  *pResult = 0;
}

void CUserGroupAuthDlg::OnLvnItemchangedListUserGroupAuthAuthlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

  if (pNMLV->uOldState == 0)
  {
    this->DeselectAddControls();

  }

  *pResult = 0;
}

void CUserGroupAuthDlg::OnNMReturnListUserGroupAuthUserlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  this->OnBnClickedButtonUserGroupAuthAddauth();

  *pResult = 0;
}

void CUserGroupAuthDlg::OnNMReturnListUserGroupAuthGrouplist(NMHDR *pNMHDR, LRESULT *pResult)
{
  this->OnBnClickedButtonUserGroupAuthAddauth();

  *pResult = 0;
}

void CUserGroupAuthDlg::OnNMReturnListUserGroupAuthAuthlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  this->OnBnClickedButtonUserGroupAuthDeleteauth();

  *pResult = 0;
}
