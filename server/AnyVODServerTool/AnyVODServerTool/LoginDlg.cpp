/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// LoginDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AnyVODServerTool.h"
#include "AnyVODServerToolDlg.h"
#include "LoginDlg.h"
#include "ParentDlg.h"
#include "Socket.h"
#include "Util.h"

// CLoginDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CLoginDlg, CDialog)

CLoginDlg::CLoginDlg(CWnd* pParent /*=nullptr*/)
	: CParentDlg(CLoginDlg::IDD, pParent)
{

}

CLoginDlg::~CLoginDlg()
{
}

void CLoginDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_EDIT_LOGIN_ID, m_ID);
  DDX_Control(pDX, IDC_EDIT_LOGIN_PASS, m_Password);
}


BEGIN_MESSAGE_MAP(CLoginDlg, CDialog)
  ON_BN_CLICKED(IDC_BUTTON_LOGIN_LOGIN, &CLoginDlg::OnBnClickedButtonLoginLogin)
  ON_BN_CLICKED(IDC_BUTTON_LOGIN_CLOSE, &CLoginDlg::OnBnClickedButtonLoginClose)
END_MESSAGE_MAP()

bool CLoginDlg::HandlePacket(ANYVOD_PACKET *packet)
{
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  if (ntohl(packet->s2c_header.header.type) == PT_S2C_LOGIN)
  {
    if (packet->s2c_header.success)
    {
      dlg->GetSocket().SetTicket(string((char*)packet->s2c_login.ticket));

      this->EndDialog(IDOK);

    }

  }

  return false;

}

void CLoginDlg::InitControls()
{
  this->m_ID.SetLimitText(MAX_ID_CHAR_SIZE);
  this->m_Password.SetLimitText(MAX_PASS_CHAR_SIZE);

  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;
  Socket &socket = dlg->GetSocket();
  ENV_INFO &env = dlg->GetEnv();
  string tmp;

  Util::ConvertUnicodeToAscii(env.server_address, &tmp);

  if (!socket.Connect(env.server_port, tmp.c_str()))
  {
    AfxMessageBox(L"연결에 실패했습니다.");

    this->EndDialog(IDCLOSE);

  }

  this->m_ID.SetFocus();

}

// CLogin 메시지 처리기입니다.

void CLoginDlg::OnBnClickedButtonLoginClose()
{
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  dlg->GetSocket().Disconnect();

  this->EndDialog(IDCANCEL);

}

void CLoginDlg::OnBnClickedButtonLoginLogin()
{
  CString id;
  CString pass;

  this->m_ID.GetWindowText(id);
  this->m_Password.GetWindowText(pass);

  id.Trim();
  pass.Trim();

  if (id.GetLength() == 0)
  {
    AfxMessageBox(L"아이디를 입력 해 주세요.");

    return;

  }

  if (pass.GetLength() == 0)
  {
    AfxMessageBox(L"암호를 입력 해 주세요.");

    return;

  }

  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;
  Socket &socket = dlg->GetSocket();

  socket.SendLogin(id, pass);

  if (!Util::HandlePacket(socket, this, nullptr, nullptr))
  {
    this->EndDialog(IDCLOSE);

  }

}

BOOL CLoginDlg::PreTranslateMessage(MSG* pMsg)
{
  return CDialog::PreTranslateMessage(pMsg);

}

void CLoginDlg::OnOK()
{
  this->OnBnClickedButtonLoginLogin();

  //CParentDlg::OnOK();
}

void CLoginDlg::OnCancel()
{
  this->OnBnClickedButtonLoginClose();

  CParentDlg::OnCancel();
}
