/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// FileAuth.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AnyVODServerTool.h"
#include "FileAuthDlg.h"
#include "AnyVODServerToolDlg.h"
#include "Socket.h"
#include "Util.h"

// CFileAuthDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CFileAuthDlg, CDialog)

CFileAuthDlg::CFileAuthDlg(CWnd* pParent /*=nullptr*/, wstring &path/* = wstring(L"")*/, vector_wstring &fileNames/* = vector_wstring()*/)
	: CParentDlg(CFileAuthDlg::IDD, pParent), m_initFileNames(fileNames), m_selectSingle(false)
{
  if (path == L"")
  {
    this->m_curPath.append(1, DIRECTORY_SEPERATOR);

  }
  else
  {
    this->m_curPath = path;

  }

}

CFileAuthDlg::~CFileAuthDlg()
{
}

void CFileAuthDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_COMBO_FILE_AUTH_PATH, m_path);
  DDX_Control(pDX, IDC_LIST_FILE_AUTH_FILELIST, m_fileList);
  DDX_Control(pDX, IDC_LIST_FILE_AUTH_GROUPLIST, m_groupList);
  DDX_Control(pDX, IDC_BUTTON_FILE_AUTH_DELETE, m_deleteAuth);
  DDX_Control(pDX, IDC_BUTTON_FILE_AUTH_ADD, m_addAuth);
  DDX_Control(pDX, IDC_LIST_FILE_AUTH_AUTHLIST, m_authList);
}


BEGIN_MESSAGE_MAP(CFileAuthDlg, CDialog)
  ON_CBN_SELCHANGE(IDC_COMBO_FILE_AUTH_PATH, &CFileAuthDlg::OnCbnSelchangeComboFileAuthPath)
  ON_NOTIFY(NM_DBLCLK, IDC_LIST_FILE_AUTH_FILELIST, &CFileAuthDlg::OnNMDblclkListFileAuthFilelist)
  ON_NOTIFY(NM_RETURN, IDC_LIST_FILE_AUTH_FILELIST, &CFileAuthDlg::OnNMReturnListFileAuthFilelist)
  ON_NOTIFY(NM_CLICK, IDC_LIST_FILE_AUTH_FILELIST, &CFileAuthDlg::OnNMClickListFileAuthFilelist)
  ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_FILE_AUTH_FILELIST, &CFileAuthDlg::OnLvnItemchangedListFileAuthFilelist)
  ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_FILE_AUTH_GROUPLIST, &CFileAuthDlg::OnLvnItemchangedListFileAuthGrouplist)
  ON_NOTIFY(NM_CLICK, IDC_LIST_FILE_AUTH_GROUPLIST, &CFileAuthDlg::OnNMClickListFileAuthGrouplist)
  ON_BN_CLICKED(IDC_BUTTON_FILE_AUTH_ADD, &CFileAuthDlg::OnBnClickedButtonFileAuthAdd)
  ON_NOTIFY(NM_RETURN, IDC_LIST_FILE_AUTH_GROUPLIST, &CFileAuthDlg::OnNMReturnListFileAuthGrouplist)
  ON_NOTIFY(NM_DBLCLK, IDC_LIST_FILE_AUTH_GROUPLIST, &CFileAuthDlg::OnNMDblclkListFileAuthGrouplist)
  ON_BN_CLICKED(IDC_BUTTON_FILE_AUTH_CLOSE, &CFileAuthDlg::OnBnClickedButtonFileAuthClose)
  ON_WM_CLOSE()
  ON_BN_CLICKED(IDC_BUTTON_FILE_AUTH_REFRESH, &CFileAuthDlg::OnBnClickedButtonFileAuthRefresh)
  ON_BN_CLICKED(IDC_BUTTON_FILE_AUTH_DELETE, &CFileAuthDlg::OnBnClickedButtonFileAuthDelete)
  ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_FILE_AUTH_AUTHLIST, &CFileAuthDlg::OnLvnItemchangedListFileAuthAuthlist)
  ON_NOTIFY(NM_CLICK, IDC_LIST_FILE_AUTH_AUTHLIST, &CFileAuthDlg::OnNMClickListFileAuthAuthlist)
  ON_NOTIFY(NM_DBLCLK, IDC_LIST_FILE_AUTH_AUTHLIST, &CFileAuthDlg::OnNMDblclkListFileAuthAuthlist)
  ON_NOTIFY(NM_RETURN, IDC_LIST_FILE_AUTH_AUTHLIST, &CFileAuthDlg::OnNMReturnListFileAuthAuthlist)
END_MESSAGE_MAP()

void CFileAuthDlg::InitControls()
{
  this->m_fileList.DeleteAllItems();
  this->m_path.ResetContent();

  this->m_fileList.InsertColumn(0, L"파일명", LVCFMT_LEFT, 493);
  this->m_fileList.InsertColumn(1, L"크기", LVCFMT_CENTER, 60);
  this->m_fileList.SetExtendedStyle(LVS_EX_GRIDLINES| LVS_EX_FULLROWSELECT);
  this->m_fileList.SetImageList(&Util::GetShellImageList(), LVSIL_SMALL);

  this->m_groupList.InsertColumn(0, L"그룹 이름", LVCFMT_LEFT, 212);
  this->m_groupList.SetExtendedStyle(LVS_EX_GRIDLINES| LVS_EX_FULLROWSELECT);
  this->m_groupList.SetImageList(&Util::GetIconImageList(), LVSIL_SMALL);

  this->m_authList.InsertColumn(0, L"그룹 이름", LVCFMT_LEFT, 212);
  this->m_authList.SetExtendedStyle(LVS_EX_GRIDLINES| LVS_EX_FULLROWSELECT);
  this->m_authList.SetImageList(&Util::GetIconImageList(), LVSIL_SMALL);

  this->OnBnClickedButtonFileAuthRefresh();

  LVFINDINFO info;

  info.flags = LVFI_STRING;

  for (size_t i = 0; i < this->m_initFileNames.size(); i++)
  {
    wstring &fileName = this->m_initFileNames[i];
    int item;

    info.psz = fileName.c_str();
    item = this->m_fileList.FindItem(&info);

    if (item != -1)
    {
      this->m_fileList.SetSelectionMark(item);

      NMITEMACTIVATE active;
      LRESULT result;

      active.iItem = item;

      this->OnNMClickListFileAuthFilelist((NMHDR*)&active, &result);

      this->m_fileList.SetItemState(item, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);

      POSITION pos = this->m_fileList.GetFirstSelectedItemPosition();

      if (pos)
      {
        CSize scroll(0, 0);
        CRect rect;
        int index;

        index = this->m_fileList.GetNextSelectedItem(pos);

        for (int i = 0; i < index; i++)
        {
          this->m_fileList.GetItemRect(i, &rect, LVIR_BOUNDS);
          scroll.cy += rect.Height();

        }

        this->m_fileList.Scroll(scroll);

      }

    }

  }

  this->m_fileList.SetFocus();

}

void CFileAuthDlg::ToggleAddControls()
{
  bool select = this->m_fileList.GetSelectedCount() != 0 && this->m_groupList.GetSelectedCount() != 0;

  this->m_addAuth.EnableWindow(select);

  if (this->GetFocus() == nullptr)
  {
    this->SetFocus();

  }

}

void CFileAuthDlg::ToggleDeleteControls()
{
  int count = this->m_fileList.GetSelectedCount();
  bool select = this->m_authList.GetSelectedCount() != 0;
  POSITION pos = this->m_fileList.GetFirstSelectedItemPosition();

  if (pos != NULL)
  {
    int item = this->m_fileList.GetNextSelectedItem(pos);
    ANYVOD_FILE_ITEM *info = (ANYVOD_FILE_ITEM*)this->m_fileList.GetItemData(item);

    if (this->m_groupList.GetSelectedCount() != 0 && info->type == FT_DIR)
    {
      select = true;

    }
    else if (count > 1 && this->m_groupList.GetSelectedCount() != 0)
    {
      select = true;

    }

  }

  this->m_deleteAuth.EnableWindow(select);

  if (this->GetFocus() == nullptr)
  {
    this->SetFocus();

  }

}

void CFileAuthDlg::DeleteFileGroupItems()
{
  for (int i = 0; i < this->m_authList.GetItemCount(); i++)
  {
    delete (ANYVOD_FILE_GROUP_INFO*)this->m_authList.GetItemData(i);

  }

  this->m_authList.DeleteAllItems();

}

void CFileAuthDlg::DeleteFileItems()
{
  for (int i = 0; i < this->m_fileList.GetItemCount(); i++)
  {
    delete (ANYVOD_FILE_ITEM*)this->m_fileList.GetItemData(i);

  }

  this->m_fileList.DeleteAllItems();

}

void CFileAuthDlg::DeleteGroupItems()
{
  for (int i = 0; i < this->m_groupList.GetItemCount(); i++)
  {
    delete (ANYVOD_GROUP_INFO*)this->m_groupList.GetItemData(i);

  }

  this->m_groupList.DeleteAllItems();

}

void
CFileAuthDlg::DeselectAddControls()
{
  this->m_groupList.SetItemState(-1 , 0, LVIS_SELECTED | LVIS_FOCUSED);

  this->ToggleAddControls();
  this->ToggleDeleteControls();

}

void
CFileAuthDlg::DeselectDeleteControls()
{
  this->m_authList.SetItemState(-1 , 0, LVIS_SELECTED | LVIS_FOCUSED);

  this->ToggleAddControls();
  this->ToggleDeleteControls();

}

int CFileAuthDlg::AddToFileList(int startIndex, ANYVOD_FILE_ITEM *items, unsigned int count, FILE_ITEM_TYPE type)
{
  for (unsigned int i = 0; i < count; i++)
  {
    ANYVOD_FILE_ITEM &item = items[i];

    if (item.type == type)
    {
      startIndex = this->AddToFileListSub(item, startIndex, type);

    }

  }

  return startIndex;

}

int CFileAuthDlg::AddToFileListSub(ANYVOD_FILE_ITEM &item, int startIndex, FILE_ITEM_TYPE type)
{
  ANYVOD_FILE_ITEM *toStore = new ANYVOD_FILE_ITEM;
  LVITEM lvi;

  lvi.mask = LVIF_TEXT | LVIF_PARAM | LVIF_IMAGE;
  lvi.iItem = startIndex;
  lvi.iSubItem = 0;
  lvi.pszText = (LPWSTR)item.fileName.c_str();
  lvi.lParam = (LPARAM)toStore;

  *toStore = item;

  SHFILEINFO sfi;

  if (type == FT_FILE)
  {
    wstring ext = item.fileName.substr(item.fileName.find_last_of(EXTENTION_SEPERATOR));

    transform(ext.begin(), ext.end(), ext.begin(), towlower);
    SHGetFileInfo(ext.c_str(), 0, &sfi, sizeof(SHFILEINFO), SHGFI_USEFILEATTRIBUTES | SHGFI_ICON | SHGFI_SMALLICON);

  }
  else
  {
    SHGetFileInfo(L"folder", FILE_ATTRIBUTE_DIRECTORY, &sfi, sizeof(SHFILEINFO), SHGFI_USEFILEATTRIBUTES | SHGFI_ICON | SHGFI_SMALLICON);

  }

  lvi.iImage = sfi.iIcon;

  DestroyIcon(sfi.hIcon);

  this->m_fileList.InsertItem(&lvi);

  CString fileSize;

  lvi.mask = LVIF_TEXT;
  lvi.iSubItem = 1;

  if (type == FT_FILE)
  {
    Util::FileSizeToString(item.totalSize, &fileSize);

    lvi.pszText = (LPTSTR)fileSize.GetString();

  }
  else
  {
    lvi.pszText = L"<DIR>";

  }

  this->m_fileList.SetItem(&lvi);

  startIndex++;

  return startIndex;

}

bool CFileAuthDlg::HandlePacket(ANYVOD_PACKET *packet)
{
  switch (ntohl(packet->s2c_header.header.type))
  {
  case PT_ADMIN_S2C_FILE_GROUPLIST:
    {
      if (packet->s2c_header.success)
      {
        this->m_fileGroupItemCount = ntohl(packet->admin_s2c_file_grouplist.count);
        this->m_fileGroupItemCurCount = 0;
        this->DeleteFileGroupItems();

        return this->m_fileGroupItemCount > 0;

      }

      break;

    }
  case PT_ADMIN_S2C_FILE_GROUP_ITEM:
    {
      if (packet->s2c_header.success)
      {
        ANYVOD_FILE_GROUP_INFO *info = new ANYVOD_FILE_GROUP_INFO;
        ANYVOD_PACKET_FILE_GROUP_INFO &item = packet->admin_s2c_file_group_item.detail;

        Util::ConvertUTF8ToUnicode((char*)item.groupName, MAX_GROUP_NAME_SIZE, &info->groupName);
        info->pid = ntohll(packet->admin_s2c_file_group_item.pid);

        this->AddAuthItemToList(info);

        if (this->m_fileGroupItemCurCount == this->m_fileGroupItemCount)
        {
          return false;

        }
        else
        {
          return true;

        }

      }

      break;

    }
  case PT_ADMIN_S2C_GROUPLIST:
    {
      if (packet->s2c_header.success)
      {
        this->m_groupItemCount = ntohl(packet->admin_s2c_grouplist.count);
        this->m_groupItemCurCount = 0;
        this->DeleteGroupItems();

        return this->m_groupItemCount > 0;

      }

      break;

    }
  case PT_ADMIN_S2C_GROUP_ITEM:
    {
      if (packet->s2c_header.success)
      {
        ANYVOD_GROUP_INFO *info = new ANYVOD_GROUP_INFO;
        ANYVOD_PACKET_GROUP_INFO &item = packet->admin_s2c_group_item.detail;

        Util::ConvertUTF8ToUnicode((char*)item.name, MAX_GROUP_NAME_SIZE, &info->name);
        info->pid = ntohll(packet->admin_s2c_group_item.pid);

        LVITEM lvi;

        lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
        lvi.iItem = this->m_groupItemCount;
        lvi.iSubItem = 0;
        lvi.pszText = (LPWSTR)info->name.c_str();;
        lvi.lParam = (LPARAM)info;
        lvi.iImage = ICON_INDEX_GROUP;

        this->m_groupList.InsertItem(&lvi);

        this->m_groupItemCurCount++;

        if (this->m_groupItemCurCount == this->m_groupItemCount)
        {
          return false;

        }
        else
        {
          return true;

        }

      }

      break;

    }
  case PT_S2C_FILELIST:
    {
      if (packet->s2c_header.success)
      {
        wstring path;
        int strIndex;

        Util::ConvertUTF8ToUnicode((char*)packet->s2c_filelist.path, MAX_PATH_SIZE, &path);
        strIndex = this->m_path.FindStringExact(0, path.c_str());

        if (strIndex == CB_ERR)
        {
          strIndex = this->m_path.AddString(path.c_str());

        }

        this->m_path.SetCurSel(strIndex);

        this->DeleteFileItems();

        int index = 0;

        if (path.size() != 1)
        {
          ANYVOD_FILE_ITEM item;

          item.fileName = PARENT_DIRECTORY;
          item.type = FT_DIR;

          index = this->AddToFileListSub(item, index, FT_DIR);

        }

        unsigned int count = ntohl(packet->s2c_filelist.count);
        ANYVOD_FILE_ITEM *items = new ANYVOD_FILE_ITEM[count];

        for (unsigned int i = 0; i < count; i++)
        {
          ANYVOD_FILE_ITEM &item = items[i];
          ANYVOD_PACKET_FILE_ITEM &src = packet->s2c_filelist.Items[i];

          item.bitRate = ntohl(src.bitRate);
          item.permission = src.permission != 0;
          item.totalSize = ntohll(src.totalSize);
          item.totalTime = ntohl(src.totalTime);
          item.type = (FILE_ITEM_TYPE)ntohl(src.type);
          Util::ConvertUTF8ToUnicode((char*)src.fileName, MAX_FILENAME_SIZE, &item.fileName);

        }

        index = this->AddToFileList(index, items, count, FT_DIR);
        this->AddToFileList(index, items, count, FT_FILE);

        delete[] items;

        this->m_curPath = path;

        this->m_fileList.SetColumnWidth(0, 476);

      }

      break;

    }
  case PT_ADMIN_S2C_FILE_GROUP_ADD:
    {
      if (packet->s2c_header.success)
      {
        if (this->m_lastFileType == FT_FILE && this->m_selectSingle)
        {
          ANYVOD_FILE_GROUP_INFO *info = new ANYVOD_FILE_GROUP_INFO;

          info->groupName = this->m_lastGroupName;
          info->pid = ntohll(packet->admin_s2c_file_group_add.pid);

          this->AddAuthItemToList(info);

        }

      }

      break;

    }

  }

  return false;

}

void
CFileAuthDlg::AddAuthItemToList(ANYVOD_FILE_GROUP_INFO *info)
{
  LVITEM lvi;

  lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
  lvi.iItem = this->m_fileGroupItemCount;
  lvi.iSubItem = 0;
  lvi.pszText = (LPWSTR)info->groupName.c_str();;
  lvi.lParam = (LPARAM)info;
  lvi.iImage = ICON_INDEX_FILE_GROUP;

  this->m_authList.InsertItem(&lvi);

  this->m_fileGroupItemCurCount++;

}

bool CFileAuthDlg::ProcessPacket()
{
  bool nextProcess = false;
  bool success = false;
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;
  Socket &socket = dlg->GetSocket();

  do
  {
    if (!Util::HandlePacket(socket, this, &nextProcess, &success))
    {
      this->DeleteFileGroupItems();
      this->DeleteGroupItems();
      this->DeleteFileItems();

      this->EndDialog(IDCLOSE);

      break;

    }

  } while (nextProcess);

  return success;

}


// CFileAuthDlg 메시지 처리기입니다.

void CFileAuthDlg::OnCbnSelchangeComboFileAuthPath()
{
  CString text;

  this->m_path.GetLBText(this->m_path.GetCurSel(), text);

  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  dlg->GetSocket().SendFileList(text.GetString());
  this->ProcessPacket();
}

void CFileAuthDlg::OnNMDblclkListFileAuthFilelist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  if (pNMItemActivate->iItem == -1)
    return;

  ANYVOD_FILE_ITEM *item = (ANYVOD_FILE_ITEM*)this->m_fileList.GetItemData(pNMItemActivate->iItem);

  if (item->type == FT_DIR)
  {
    CString text = this->m_fileList.GetItemText(pNMItemActivate->iItem, 0);
    wstring path = this->m_curPath;

    if (wcscmp(text.GetString(), PARENT_DIRECTORY.c_str()) == 0)
    {
      path = path.substr(0, path.find_last_of(DIRECTORY_SEPERATOR));

      if (path.size() == 0)
      {
        path.append(1, DIRECTORY_SEPERATOR);

      }

    }
    else
    {
      if (path[path.size()-1] != DIRECTORY_SEPERATOR)
      {
        path.append(1, DIRECTORY_SEPERATOR);

      }

      path.append(text.GetString());

    }

    CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

    dlg->GetSocket().SendFileList(path);
    this->ProcessPacket();

  }
  else
  {
    this->OnNMClickListFileAuthFilelist(pNMHDR, pResult);
    this->OnBnClickedButtonFileAuthAdd();

  }

  *pResult = 0;
}

void CFileAuthDlg::OnNMReturnListFileAuthFilelist(NMHDR *pNMHDR, LRESULT *pResult)
{
  POSITION pos = this->m_fileList.GetFirstSelectedItemPosition();

  if (pos != NULL)
  {
    int item = this->m_fileList.GetNextSelectedItem(pos);
    NMITEMACTIVATE active;

    active.iItem = item;

    this->DeselectDeleteControls();

    this->OnNMDblclkListFileAuthFilelist((NMHDR*)&active, pResult);

  }

  *pResult = 0;
}

void CFileAuthDlg::OnNMClickListFileAuthFilelist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  if (this->m_fileList.GetSelectedCount() > 1)
  {
    this->DeleteFileGroupItems();

  }
  else
  {
    if (pNMItemActivate->iItem == -1)
      return;

    ANYVOD_FILE_ITEM *item = (ANYVOD_FILE_ITEM*)this->m_fileList.GetItemData(pNMItemActivate->iItem);

    if (item->type == FT_FILE)
    {
      wstring filePath = this->m_curPath;

      if (this->m_curPath[this->m_curPath.size()-1] != DIRECTORY_SEPERATOR)
      {
        filePath.append(1, DIRECTORY_SEPERATOR);

      }

      filePath += item->fileName;

      CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

      dlg->GetSocket().SendFileGroupList(filePath);
      this->ProcessPacket();

    }
    else
    {
      this->DeleteFileGroupItems();

    }

  }

  this->DeselectDeleteControls();

  *pResult = 0;
}

void CFileAuthDlg::OnLvnItemchangedListFileAuthFilelist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

  if (pNMLV->uOldState == 0)
  {
    this->DeselectDeleteControls();

  }

  *pResult = 0;
}

void CFileAuthDlg::OnLvnItemchangedListFileAuthGrouplist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

  if (pNMLV->uOldState == 0)
  {
    this->DeselectDeleteControls();

  }

  *pResult = 0;
}

void CFileAuthDlg::OnNMClickListFileAuthGrouplist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  this->DeselectDeleteControls();

  *pResult = 0;
}

void CFileAuthDlg::OnBnClickedButtonFileAuthAdd()
{
  POSITION filePos = this->m_fileList.GetFirstSelectedItemPosition();
  bool recursive = false;

  this->m_selectSingle = this->m_fileList.GetSelectedCount() == 1;

  if (filePos != NULL)
  {
    CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

    while (filePos)
    {
      int fileSelect = this->m_fileList.GetNextSelectedItem(filePos);
      ANYVOD_FILE_ITEM *fileInfo = (ANYVOD_FILE_ITEM*)this->m_fileList.GetItemData(fileSelect);
      POSITION groupPos = this->m_groupList.GetFirstSelectedItemPosition();

      if (groupPos != NULL)
      {
        while (groupPos)
        {
          int groupSelect = this->m_groupList.GetNextSelectedItem(groupPos);
          ANYVOD_GROUP_INFO *groupInfo = (ANYVOD_GROUP_INFO*)this->m_groupList.GetItemData(groupSelect);
          wstring filePath = this->m_curPath;

          if (fileInfo->type == FT_DIR && !recursive)
          {
            if (AfxMessageBox(L"선택한 경로 중 디렉토리가 있습니다.\n선택하신 디렉토리 하위의 모든 파일에 권한을 등록 하시겠습니까?", MB_YESNO) == IDNO)
            {
              return;

            }

            recursive = true;

          }

          if (this->m_curPath[this->m_curPath.size()-1] != DIRECTORY_SEPERATOR)
          {
            filePath.append(1, DIRECTORY_SEPERATOR);

          }

          filePath += fileInfo->fileName;

          this->m_lastFileType = fileInfo->type;
          this->m_lastGroupName = groupInfo->name;

          dlg->GetSocket().SendFileGroupAdd(filePath, groupInfo->name);

          if (this->ProcessPacket())
          {
            this->m_fileGroupItemCount++;

            if (filePos == NULL)
            {
              this->m_groupList.SetItemState(groupSelect, 0, LVIS_SELECTED | LVIS_FOCUSED);
              groupPos = this->m_groupList.GetFirstSelectedItemPosition();

            }

          }
          else
          {
            Util::ListControlScrollToEnd(this->m_authList);

            this->ToggleAddControls();
            this->ToggleDeleteControls();

            if (filePos != NULL || groupPos != NULL)
            {
              if (AfxMessageBox(L"권한을 추가 하는 도중 오류가 발생 했습니다. 계속 하시겠습니까?", MB_YESNO) == IDNO)
              {
                return;

              }

            }
            else
            {
              AfxMessageBox(L"권한을 추가 하는 도중 오류가 발생 했습니다.", MB_OK);

              return;

            }

          }

        }

        if (!this->m_selectSingle)
        {
          this->m_fileList.SetItemState(fileSelect, 0, LVIS_SELECTED | LVIS_FOCUSED);
          filePos = this->m_fileList.GetFirstSelectedItemPosition();

        }

      }
      else
      {
        AfxMessageBox(L"그룹을 선택 해 주세요.");

        break;

      }

    }

    Util::ListControlScrollToEnd(this->m_authList);

    this->ToggleAddControls();
    this->ToggleDeleteControls();

    if (recursive)
    {
      AfxMessageBox(L"선택하신 디렉토리의 하위에 존재하는 모든 파일에 권한 설정이 완료 되었습니다.");

    }
    else if (!this->m_selectSingle)
    {
      AfxMessageBox(L"선택하신 파일의 권한 설정이 완료 되었습니다.");

    }

  }
  else
  {
    AfxMessageBox(L"경로를 선택 해 주세요.");

  }

}

void CFileAuthDlg::OnNMReturnListFileAuthGrouplist(NMHDR *pNMHDR, LRESULT *pResult)
{
  this->OnBnClickedButtonFileAuthAdd();

  *pResult = 0;
}

void CFileAuthDlg::OnNMDblclkListFileAuthGrouplist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  this->OnBnClickedButtonFileAuthAdd();

  *pResult = 0;
}

void CFileAuthDlg::OnBnClickedButtonFileAuthClose()
{
  this->OnClose();

}

void CFileAuthDlg::OnClose()
{
  this->DeleteFileGroupItems();
  this->DeleteFileItems();
  this->DeleteGroupItems();

  this->EndDialog(IDCANCEL);

  CParentDlg::OnClose();
}

void CFileAuthDlg::OnBnClickedButtonFileAuthRefresh()
{
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  dlg->GetSocket().SendFileList(this->m_curPath);
  this->ProcessPacket();

  dlg->GetSocket().SendGroupList();
  this->ProcessPacket();

  this->DeleteFileGroupItems();
  this->ToggleAddControls();
  this->ToggleDeleteControls();
}

void CFileAuthDlg::OnBnClickedButtonFileAuthDelete()
{
  POSITION filePos = this->m_fileList.GetFirstSelectedItemPosition();
  bool selectSingle = this->m_fileList.GetSelectedCount() == 1;
  bool recursive = false;

  if (filePos != NULL)
  {
    CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

    while (filePos)
    {
      int fileSelect = this->m_fileList.GetNextSelectedItem(filePos);
      ANYVOD_FILE_ITEM *fileInfo = (ANYVOD_FILE_ITEM*)this->m_fileList.GetItemData(fileSelect);
      CListCtrl *list = nullptr;
      bool selectDirSingle = false;

      if (fileInfo->type == FT_DIR && selectSingle)
      {
        list = &this->m_groupList;
        selectDirSingle = true;

      }
      else
      {
        list = selectSingle ? &this->m_authList : &this->m_groupList;
        selectDirSingle = false;

      }

      POSITION groupPos = list->GetFirstSelectedItemPosition();

      if (groupPos != NULL)
      {
        while (groupPos)
        {
          int groupSelect = list->GetNextSelectedItem(groupPos);
          wstring groupName;

          if (fileInfo->type == FT_DIR && !recursive)
          {
            if (AfxMessageBox(L"선택한 경로 중 디렉토리가 있습니다.\n선택하신 디렉토리 하위의 모든 파일의 권한을 삭제 하시겠습니까?", MB_YESNO) == IDNO)
            {
              return;

            }

            recursive = true;

          }

          if (selectSingle && !selectDirSingle)
          {
            ANYVOD_FILE_GROUP_INFO *groupInfo = (ANYVOD_FILE_GROUP_INFO*)list->GetItemData(groupSelect);

            groupName = groupInfo->groupName;

          }
          else
          {
            ANYVOD_GROUP_INFO *groupInfo = (ANYVOD_GROUP_INFO*)list->GetItemData(groupSelect);

            groupName = groupInfo->name;

          }

          wstring filePath = this->m_curPath;

          if (this->m_curPath[this->m_curPath.size()-1] != DIRECTORY_SEPERATOR)
          {
            filePath.append(1, DIRECTORY_SEPERATOR);

          }

          filePath += fileInfo->fileName;

          dlg->GetSocket().SendFileGroupDelete(filePath, groupName);

          if (this->ProcessPacket())
          {
            this->m_fileGroupItemCount++;

            if (selectSingle && !selectDirSingle)
            {
              ANYVOD_FILE_GROUP_INFO *groupInfo = (ANYVOD_FILE_GROUP_INFO*)list->GetItemData(groupSelect);

              delete groupInfo;

              this->m_fileGroupItemCount--;
              this->m_fileGroupItemCurCount--;

              list->DeleteItem(groupSelect);
              groupPos = list->GetFirstSelectedItemPosition();

            }
            else
            {
              if (filePos == NULL)
              {
                list->SetItemState(groupSelect, 0, LVIS_SELECTED | LVIS_FOCUSED);
                groupPos = list->GetFirstSelectedItemPosition();

              }

            }

          }
          else
          {
            this->ToggleAddControls();
            this->ToggleDeleteControls();

            if (filePos != NULL || groupPos != NULL)
            {
              if (AfxMessageBox(L"권한을 삭제 하는 도중 오류가 발생 했습니다. 계속 하시겠습니까?", MB_YESNO) == IDNO)
              {
                return;

              }

            }
            else
            {
              AfxMessageBox(L"권한을 삭제 하는 도중 오류가 발생 했습니다.", MB_OK);

              return;

            }

          }

        }

        if (!selectSingle && !selectDirSingle)
        {
          this->m_fileList.SetItemState(fileSelect, 0, LVIS_SELECTED | LVIS_FOCUSED);
          filePos = this->m_fileList.GetFirstSelectedItemPosition();

        }

      }
      else
      {
        AfxMessageBox(L"그룹을 선택 해 주세요.");

        break;

      }

    }

    this->ToggleAddControls();
    this->ToggleDeleteControls();

    if (recursive)
    {
      AfxMessageBox(L"선택하신 디렉토리의 하위에 존재하는 모든 파일의 권한이 삭제 되었습니다.");

    }
    else if (!selectSingle)
    {
      AfxMessageBox(L"선택하신 파일의 권한이 삭제 되었습니다.");

    }

  }
  else
  {
    AfxMessageBox(L"경로를 선택 해 주세요.");

  }

}

void CFileAuthDlg::OnLvnItemchangedListFileAuthAuthlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

  if (pNMLV->uOldState == 0)
  {
    this->DeselectAddControls();

  }

  *pResult = 0;
}

void CFileAuthDlg::OnNMClickListFileAuthAuthlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  this->DeselectAddControls();

  *pResult = 0;
}

void CFileAuthDlg::OnNMDblclkListFileAuthAuthlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  this->OnBnClickedButtonFileAuthDelete();

  *pResult = 0;
}

void CFileAuthDlg::OnNMReturnListFileAuthAuthlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  this->OnBnClickedButtonFileAuthDelete();

  *pResult = 0;
}
