/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "ParentDlg.h"
#include "..\..\..\common\types.h"

union ANYVOD_PACKET;

// CFileAuthDlg 대화 상자입니다.

class CFileAuthDlg : public CParentDlg
{
	DECLARE_DYNAMIC(CFileAuthDlg)

public:
	CFileAuthDlg(CWnd* pParent = nullptr, wstring &path = wstring(L""), vector_wstring &fileNames = vector_wstring());   // 표준 생성자입니다.
	virtual ~CFileAuthDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_FILE_AUTH_DIALOG };

private:
  wstring m_curPath;
  vector_wstring m_initFileNames;
  unsigned int m_groupItemCount;
  unsigned int m_groupItemCurCount;
  unsigned int m_fileGroupItemCount;
  unsigned int m_fileGroupItemCurCount;
  FILE_ITEM_TYPE m_lastFileType;
  wstring m_lastGroupName;
  bool m_selectSingle;

private:
  void AddAuthItemToList(ANYVOD_FILE_GROUP_INFO *info);
  void DeleteGroupItems();
  void DeleteFileItems();
  void DeleteFileGroupItems();
  void ToggleAddControls();
  void ToggleDeleteControls();
  void DeselectAddControls();
  void DeselectDeleteControls();
  int AddToFileList(int startIndex, ANYVOD_FILE_ITEM *items, unsigned int count, FILE_ITEM_TYPE type);
  int AddToFileListSub(ANYVOD_FILE_ITEM &item, int startIndex, FILE_ITEM_TYPE type);
  bool ProcessPacket();
  virtual bool HandlePacket(ANYVOD_PACKET *packet);
  virtual void InitControls();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
  CComboBox m_path;
  CListCtrl m_fileList;
  CListCtrl m_groupList;
  CButton m_deleteAuth;
  CButton m_addAuth;
  CListCtrl m_authList;

  afx_msg void OnCbnSelchangeComboFileAuthPath();
  afx_msg void OnNMDblclkListFileAuthFilelist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMReturnListFileAuthFilelist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMClickListFileAuthFilelist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnLvnItemchangedListFileAuthFilelist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnLvnItemchangedListFileAuthGrouplist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMClickListFileAuthGrouplist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnBnClickedButtonFileAuthAdd();
  afx_msg void OnNMReturnListFileAuthGrouplist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMDblclkListFileAuthGrouplist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnBnClickedButtonFileAuthClose();
  afx_msg void OnClose();
  afx_msg void OnBnClickedButtonFileAuthRefresh();
  afx_msg void OnBnClickedButtonFileAuthDelete();
  afx_msg void OnLvnItemchangedListFileAuthAuthlist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMClickListFileAuthAuthlist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMDblclkListFileAuthAuthlist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMReturnListFileAuthAuthlist(NMHDR *pNMHDR, LRESULT *pResult);
};
