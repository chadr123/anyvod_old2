/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#include "stdafx.h"
#include "Socket.h"
#include "Util.h"
#include "..\..\..\common\packets.h"
#include "..\..\..\common\network.h"

Socket::Socket()
:m_connectEvent(NULL), m_recvEvent(NULL), m_pickEvent(NULL), m_buf(nullptr)
{

}

Socket::~Socket()
{
  delete[] this->m_buf;

}

bool
Socket::Create()
{
  this->m_connectEvent = CreateEvent(nullptr, TRUE, FALSE, L"");

  if (this->m_connectEvent == NULL)
  {
    return false;

  }

  this->m_recvEvent = CreateEvent(nullptr, FALSE, FALSE, L"");

  if (this->m_recvEvent == NULL)
  {
    CloseHandle(this->m_connectEvent);

    return false;

  }

  this->m_pickEvent = CreateEvent(nullptr, FALSE, FALSE, L"");

  if (this->m_pickEvent == NULL)
  {
    CloseHandle(this->m_connectEvent);
    CloseHandle(this->m_recvEvent);

    return false;

  }

  if (AsyncWinSocket::Create())
  {
    return true;

  }
  else
  {
    CloseHandle(this->m_connectEvent);
    CloseHandle(this->m_recvEvent);
    CloseHandle(this->m_pickEvent);

    return false;

  }


}

bool
Socket::Connect(int nPort, const char *szIP/* = nullptr */)
{
  if (this->IsConnected())
  {
    return false;

  }

  if (AsyncWinSocket::Connect(nPort, szIP))
  {
    ResetEvent(this->m_connectEvent);
    ResetEvent(this->m_pickEvent);
    ResetEvent(this->m_recvEvent);
    WaitForSingleObject(this->m_connectEvent, INFINITE);

    return this->IsConnected();

  }
  else
  {
    return false;

  }

}

void
Socket::Disconnect()
{
  AsyncWinSocket::Disconnect();

  SetEvent(this->m_connectEvent);
  SetEvent(this->m_pickEvent);
  SetEvent(this->m_recvEvent);

  this->m_ticket.clear();

}

void
Socket::WaitForRecv()
{
  WaitForSingleObject(this->m_recvEvent, INFINITE);

}

bool
Socket::BeginResult(ANYVOD_PACKET **packet)
{
  if (!this->IsConnected())
    return false;

  WaitForSingleObject(this->m_recvEvent, INFINITE);

  if (this->m_buf == nullptr)
  {
    return false;

  }
  else
  {
    *packet = (ANYVOD_PACKET*)this->m_buf;

    return true;

  }

}

void
Socket::EndResult()
{
  delete[] this->m_buf;
  this->m_buf = nullptr;

  SetEvent(this->m_pickEvent);

}

void
Socket::SetTicket(string &ticket)
{
  this->m_ticket = ticket;

}

void
Socket::GetTicket(string *ticket)
{
  *ticket = this->m_ticket;

}

void
Socket::OnReceive(const ANYVOD_PACKET& packet)
{
  WaitForSingleObject(this->m_pickEvent, INFINITE);

  int size = ntohl(packet.s2c_header.header.size);

  delete[] this->m_buf;
  this->m_buf = new char[size];

  memcpy(this->m_buf, &packet, size);

  SetEvent(this->m_recvEvent);

}

void
Socket::OnDisconnect()
{
  ResetEvent(this->m_pickEvent);
  SetEvent(this->m_recvEvent);

  this->m_ticket.clear();

}

void
Socket::OnConnect(bool bRet)
{
  if (bRet)
  {
    SetEvent(this->m_pickEvent);
    ResetEvent(this->m_recvEvent);

  }
  else
  {
    ResetEvent(this->m_pickEvent);

  }

  SetEvent(this->m_connectEvent);

}

void
Socket::FillTicket(ANYVOD_PACKET *packet)
{
  strncpy_s((char*)packet->c2s_header.ticket, MAX_TICKET_SIZE, this->m_ticket.c_str(), MAX_TICKET_CHAR_SIZE);

}

void
Socket::SendLogin(CString &id, CString &pass)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_C2S_LOGIN);
  packet.c2s_header.header.type = PT_C2S_LOGIN;

  Util::ConvertUnicodeToUTF8(id.GetString(), (char*)packet.c2s_login.id, MAX_ID_SIZE);
  Util::ConvertUnicodeToUTF8(pass.GetString(), (char*)packet.c2s_login.pass, MAX_PASS_SIZE);

  this->SendPacket(packet);

}

void
Socket::SendLogout()
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_C2S_LOGOUT);
  packet.c2s_header.header.type = PT_C2S_LOGOUT;

  this->FillTicket(&packet);

  this->SendPacket(packet);

  this->m_ticket.clear();

}

void
Socket::SendFileList(const wstring &path)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_C2S_FILELIST);
  packet.c2s_header.header.type = PT_C2S_FILELIST;

  this->FillTicket(&packet);

  Util::ConvertUnicodeToUTF8(path.c_str(), (char*)packet.c2s_filelist.path, MAX_PATH_SIZE);

  this->SendPacket(packet);

}

void
Socket::SendNetworkStatistics()
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_NETWORK_STATISTICS);
  packet.c2s_header.header.type = PT_ADMIN_C2S_NETWORK_STATISTICS;

  this->FillTicket(&packet);

  this->SendPacket(packet);

}

void
Socket::SendLastLog()
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_LASTLOG);
  packet.c2s_header.header.type = PT_ADMIN_C2S_LASTLOG;

  this->FillTicket(&packet);

  this->SendPacket(packet);

}

void
Socket::SendEnv()
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_ENV);
  packet.c2s_header.header.type = PT_ADMIN_C2S_ENV;

  this->FillTicket(&packet);

  this->SendPacket(packet);

}

void
Socket::SendScheduleList()
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_SCHEDULELIST);
  packet.c2s_header.header.type = PT_ADMIN_C2S_SCHEDULELIST;

  this->FillTicket(&packet);

  this->SendPacket(packet);

}

void
Socket::SendRestartScheduler()
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_RESTART_SCHEDULER);
  packet.c2s_header.header.type = PT_ADMIN_C2S_RESTART_SCHEDULER;

  this->FillTicket(&packet);

  this->SendPacket(packet);

}

void
Socket::SendScheduleStartNow(unsigned int index)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_SCHEDULE_STARTNOW);
  packet.c2s_header.header.type = PT_ADMIN_C2S_SCHEDULE_STARTNOW;

  this->FillTicket(&packet);

  packet.admin_c2s_schedule_startnow.index = htonl(index);

  this->SendPacket(packet);

}

void
Socket::SendScheduleTypes()
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_SCHEDULE_TYPES);
  packet.c2s_header.header.type = PT_ADMIN_C2S_SCHEDULE_TYPES;

  this->FillTicket(&packet);

  this->SendPacket(packet);

}

void
Socket::SendScheduleDelete(unsigned int index)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_SCHEDULE_DELETE);
  packet.c2s_header.header.type = PT_ADMIN_C2S_SCHEDULE_DELETE;

  this->FillTicket(&packet);

  packet.admin_c2s_schedule_delete.index = htonl(index);

  this->SendPacket(packet);

}

void
Socket::SendScheduleAdd(string &script)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_SCHEDULE_ADD);
  packet.c2s_header.header.type = PT_ADMIN_C2S_SCHEDULE_ADD;

  this->FillTicket(&packet);

  strncpy_s((char*)packet.admin_c2s_schedule_add.script, MAX_SCHEDULE_SCRIPT_SIZE, script.c_str(), MAX_SCHEDULE_SCRIPT_CHAR_SIZE);

  this->SendPacket(packet);

}

void
Socket::SendScheduleModify(unsigned int number, string &script)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_SCHEDULE_MODIFY);
  packet.c2s_header.header.type = PT_ADMIN_C2S_SCHEDULE_MODIFY;

  this->FillTicket(&packet);

  packet.admin_c2s_schedule_modify.index = htonl(number);
  strncpy_s((char*)packet.admin_c2s_schedule_modify.script, MAX_SCHEDULE_SCRIPT_SIZE, script.c_str(), MAX_SCHEDULE_SCRIPT_CHAR_SIZE);

  this->SendPacket(packet);

}

void
Socket::SendLogList()
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_LOGLIST);
  packet.c2s_header.header.type = PT_ADMIN_C2S_LOGLIST;

  this->FillTicket(&packet);

  this->SendPacket(packet);

}

void
Socket::SendLogDelete(const wstring &fileName)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_LOG_DELETE);
  packet.c2s_header.header.type = PT_ADMIN_C2S_LOG_DELETE;

  this->FillTicket(&packet);

  Util::ConvertUnicodeToUTF8(fileName.c_str(), (char*)packet.admin_c2s_log_delete.fileName, MAX_PATH_SIZE);

  this->SendPacket(packet);

}

void
Socket::SendLogView(const wstring &fileName)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_LOG_VIEW);
  packet.c2s_header.header.type = PT_ADMIN_C2S_LOG_VIEW;

  this->FillTicket(&packet);

  Util::ConvertUnicodeToUTF8(fileName.c_str(), (char*)packet.admin_c2s_log_view.fileName, MAX_PATH_SIZE);

  this->SendPacket(packet);

}

void
Socket::SendUserList()
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_USERLIST);
  packet.c2s_header.header.type = PT_ADMIN_C2S_USERLIST;

  this->FillTicket(&packet);

  this->SendPacket(packet);

}

void
Socket::SendUserDelete(unsigned long long pid)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_USER_DELETE);
  packet.c2s_header.header.type = PT_ADMIN_C2S_USER_DELETE;

  this->FillTicket(&packet);

  packet.admin_c2s_user_delete.pid = htonll(pid);

  this->SendPacket(packet);

}

void
Socket::SendUserExist(CString &id)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_USER_EXIST);
  packet.c2s_header.header.type = PT_ADMIN_C2S_USER_EXIST;

  this->FillTicket(&packet);

  Util::ConvertUnicodeToUTF8(id.GetString(), (char*)packet.admin_c2s_user_exist.id, MAX_ID_SIZE);

  this->SendPacket(packet);

}

void
Socket::SendUserAdd(ANYVOD_USER_INFO &info)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_USER_ADD);
  packet.c2s_header.header.type = PT_ADMIN_C2S_USER_ADD;

  this->FillTicket(&packet);

  packet.admin_c2s_user_add.detail.admin = info.admin;

  Util::ConvertUnicodeToUTF8(info.id.c_str(), (char*)packet.admin_c2s_user_add.detail.id, MAX_ID_SIZE);
  Util::ConvertUnicodeToUTF8(info.pass.c_str(), (char*)packet.admin_c2s_user_add.detail.pass, MAX_PASS_SIZE);
  Util::ConvertUnicodeToUTF8(info.name.c_str(), (char*)packet.admin_c2s_user_add.detail.name, MAX_NAME_SIZE);

  this->SendPacket(packet);

}

void
Socket::SendUserModify(unsigned long long pid, ANYVOD_USER_INFO &info)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_USER_MODIFY);
  packet.c2s_header.header.type = PT_ADMIN_C2S_USER_MODIFY;

  this->FillTicket(&packet);

  packet.admin_c2s_user_modify.pid = htonll(pid);
  packet.admin_c2s_user_modify.detail.admin = info.admin;

  //아이디는 사용 안함
  //Util::ConvertUnicodeToUTF8(info.id.c_str(), (char*)packet.admin_c2s_user_modify.detail.id, MAX_ID_SIZE);
  Util::ConvertUnicodeToUTF8(info.pass.c_str(), (char*)packet.admin_c2s_user_modify.detail.pass, MAX_PASS_SIZE);
  Util::ConvertUnicodeToUTF8(info.name.c_str(), (char*)packet.admin_c2s_user_modify.detail.name, MAX_NAME_SIZE);

  this->SendPacket(packet);

}

void
Socket::SendClientStatusList()
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_CLIENT_STATUSLIST);
  packet.c2s_header.header.type = PT_ADMIN_C2S_CLIENT_STATUSLIST;

  this->FillTicket(&packet);

  this->SendPacket(packet);

}

void
Socket::SendClientLogout(const wstring &ip, unsigned short port)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_CLIENT_LOGOUT);
  packet.c2s_header.header.type = PT_ADMIN_C2S_CLIENT_LOGOUT;

  this->FillTicket(&packet);

  packet.admin_c2s_client_logout.port = htons(port);

  string tmp;

  Util::ConvertUnicodeToAscii(ip, &tmp);
  strncpy_s((char*)packet.admin_c2s_client_logout.ip, MAX_IP_SIZE, tmp.c_str(), MAX_IP_CHAR_SIZE);

  this->SendPacket(packet);

}

void
Socket::SendGroupList()
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_GROUPLIST);
  packet.c2s_header.header.type = PT_ADMIN_C2S_GROUPLIST;

  this->FillTicket(&packet);

  this->SendPacket(packet);

}

void
Socket::SendGroupDelete(unsigned long long pid)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_GROUP_DELETE);
  packet.c2s_header.header.type = PT_ADMIN_C2S_GROUP_DELETE;

  this->FillTicket(&packet);

  packet.admin_c2s_group_delete.pid = htonll(pid);

  this->SendPacket(packet);

}

void
Socket::SendGroupExist(CString &name)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_GROUP_EXIST);
  packet.c2s_header.header.type = PT_ADMIN_C2S_GROUP_EXIST;

  this->FillTicket(&packet);

  Util::ConvertUnicodeToUTF8(name.GetString(), (char*)packet.admin_c2s_group_exist.name, MAX_GROUP_NAME_SIZE);

  this->SendPacket(packet);

}

void
Socket::SendGroupAdd(ANYVOD_GROUP_INFO &info)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_GROUP_ADD);
  packet.c2s_header.header.type = PT_ADMIN_C2S_GROUP_ADD;

  this->FillTicket(&packet);

  Util::ConvertUnicodeToUTF8(info.name.c_str(), (char*)packet.admin_c2s_group_add.detail.name, MAX_GROUP_NAME_SIZE);

  this->SendPacket(packet);

}

void
Socket::SendGroupModify(unsigned long long pid, ANYVOD_GROUP_INFO &info)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_GROUP_MODIFY);
  packet.c2s_header.header.type = PT_ADMIN_C2S_GROUP_MODIFY;

  this->FillTicket(&packet);

  packet.admin_c2s_group_modify.pid = htonll(pid);

  Util::ConvertUnicodeToUTF8(info.name.c_str(), (char*)packet.admin_c2s_group_modify.detail.name, MAX_GROUP_NAME_SIZE);

  this->SendPacket(packet);

}

void
Socket::SendUserGroupList()
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_USER_GROUPLIST);
  packet.c2s_header.header.type = PT_ADMIN_C2S_USER_GROUPLIST;

  this->FillTicket(&packet);

  this->SendPacket(packet);

}

void
Socket::SendUserGroupAdd(unsigned long long userPID, wstring &groupName)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_USER_GROUP_ADD);
  packet.c2s_header.header.type = PT_ADMIN_C2S_USER_GROUP_ADD;

  this->FillTicket(&packet);

  packet.admin_c2s_user_group_add.userPID = htonll(userPID);

  Util::ConvertUnicodeToUTF8(groupName.c_str(), (char*)packet.admin_c2s_user_group_add.groupName, MAX_GROUP_NAME_SIZE);

  this->SendPacket(packet);

}

void
Socket::SendUserGroupDelete(unsigned long long pid)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_USER_GROUP_DELETE);
  packet.c2s_header.header.type = PT_ADMIN_C2S_USER_GROUP_DELETE;

  this->FillTicket(&packet);

  packet.admin_c2s_user_group_delete.pid = htonll(pid);

  this->SendPacket(packet);

}

void
Socket::SendFileGroupList(const wstring &filePath)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_FILE_GROUPLIST);
  packet.c2s_header.header.type = PT_ADMIN_C2S_FILE_GROUPLIST;

  this->FillTicket(&packet);

  Util::ConvertUnicodeToUTF8(filePath.c_str(), (char*)packet.admin_c2s_file_grouplist.filePath, MAX_FILEPATH_SIZE);

  this->SendPacket(packet);

}

void
Socket::SendFileGroupAdd(const wstring &filePath, const wstring &groupName)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_FILE_GROUP_ADD);
  packet.c2s_header.header.type = PT_ADMIN_C2S_FILE_GROUP_ADD;

  this->FillTicket(&packet);

  Util::ConvertUnicodeToUTF8(groupName.c_str(), (char*)packet.admin_c2s_file_group_add.detail.groupName, MAX_GROUP_NAME_SIZE);
  Util::ConvertUnicodeToUTF8(filePath.c_str(), (char*)packet.admin_c2s_file_group_add.filePath, MAX_FILEPATH_SIZE);

  this->SendPacket(packet);

}

void
Socket::SendFileGroupDelete(const wstring &filePath, const wstring &groupName)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_FILE_GROUP_DELETE);
  packet.c2s_header.header.type = PT_ADMIN_C2S_FILE_GROUP_DELETE;

  this->FillTicket(&packet);

  Util::ConvertUnicodeToUTF8(groupName.c_str(), (char*)packet.admin_c2s_file_group_delete.detail.groupName, MAX_GROUP_NAME_SIZE);
  Util::ConvertUnicodeToUTF8(filePath.c_str(), (char*)packet.admin_c2s_file_group_delete.filePath, MAX_FILEPATH_SIZE);

  this->SendPacket(packet);

}

void
Socket::SendStatementList()
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_STATEMENT_STATUSLIST);
  packet.c2s_header.header.type = PT_ADMIN_C2S_STATEMENT_STATUSLIST;

  this->FillTicket(&packet);

  this->SendPacket(packet);

}

void
Socket::SendSubtitleExist(const wstring &filePath)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_SUBTITLE_EXIST);
  packet.c2s_header.header.type = PT_ADMIN_C2S_SUBTITLE_EXIST;

  this->FillTicket(&packet);

  Util::ConvertUnicodeToUTF8(filePath.c_str(), (char*)packet.admin_c2s_subtitle_exist.filePath, MAX_FILEPATH_SIZE);

  this->SendPacket(packet);

}

void
Socket::SendSubtitleURL(const wstring &filePath)
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_C2S_SUBTITLE_URL);
  packet.c2s_header.header.type = PT_C2S_SUBTITLE_URL;

  this->FillTicket(&packet);

  Util::ConvertUnicodeToUTF8(filePath.c_str(), (char*)packet.c2s_subtitle_url.filePath, MAX_FILEPATH_SIZE);
  packet.c2s_subtitle_url.searchSubtitle = true;

  this->SendPacket(packet);

}

void
Socket::SendRebuildMetaData()
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_REBUILD_META_DATA);
  packet.c2s_header.header.type = PT_ADMIN_C2S_REBUILD_META_DATA;

  this->FillTicket(&packet);

  this->SendPacket(packet);

}

void
Socket::SendRebuildMediaData()
{
  ANYVOD_PACKET packet;

  packet.c2s_header.header.size = sizeof(ANYVOD_PACKET_ADMIN_C2S_REBUILD_MOVIE_DATA);
  packet.c2s_header.header.type = PT_ADMIN_C2S_REBUILD_MOVIE_DATA;

  this->FillTicket(&packet);

  this->SendPacket(packet);

}
