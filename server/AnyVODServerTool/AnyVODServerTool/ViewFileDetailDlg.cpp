/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

// ViewFileDetailDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AnyVODServerTool.h"
#include "ViewFileDetailDlg.h"
#include "AnyVODServerToolDlg.h"
#include "Util.h"

// CViewFileDetailDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CViewFileDetailDlg, CDialog)

CViewFileDetailDlg::CViewFileDetailDlg(ANYVOD_FILE_ITEM &info, wstring &subtitlePath, CWnd* pParent /*=nullptr*/)
	: CParentDlg(CViewFileDetailDlg::IDD, pParent), m_info(info), m_subtitlePath(subtitlePath), m_subtitleExist(false)
{

}

CViewFileDetailDlg::~CViewFileDetailDlg()
{
}

void CViewFileDetailDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_STATIC_VIEW_FILE_DETAIL_TIME, m_time);
  DDX_Control(pDX, IDC_STATIC_VIEW_FILE_DETAIL_BITRATE, m_bitRate);
  DDX_Control(pDX, IDC_STATIC__VIEW_FILE_DETAIL_SIZE, m_size);
  DDX_Control(pDX, IDC_STATIC_VIEW_FILE_DETAIL_FILENAME, m_fileName);
  DDX_Control(pDX, IDC_STATIC_VIEW_FILE_DETAIL_SUBTITLE, m_subtitle);
}


BEGIN_MESSAGE_MAP(CViewFileDetailDlg, CDialog)
  ON_WM_PAINT()
END_MESSAGE_MAP()


// CViewFileDetailDlg 메시지 처리기입니다.

BOOL CViewFileDetailDlg::OnInitDialog()
{
  CDialog::OnInitDialog();
  CString orgFileName;
  CString fileName;
  CString bitRate;
  CString byteRate;
  CString size;
  CString time;

  orgFileName = this->m_info.fileName.c_str();

  Util::GetFittedText(this->m_fileName, orgFileName, &fileName);

  bitRate.Format(L"%u", this->m_info.bitRate);
  Util::NumberFormatting(&bitRate);
  bitRate += L" bits/s";

  byteRate.Format(L"%u", this->m_info.bitRate / 8);
  Util::NumberFormatting(&byteRate);
  byteRate += L" bytes/s";

  bitRate += L" (" + byteRate + L")";

  size.Format(L"%llu", this->m_info.totalSize);
  Util::NumberFormatting(&size);
  size += L" bytes";

  Util::TimeToString(this->m_info.totalTime, &time);

  this->m_fileName.SetWindowText(fileName);
  this->m_bitRate.SetWindowText(bitRate);
  this->m_size.SetWindowText(size);
  this->m_time.SetWindowText(time);

  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  Util::AppendDirSeparator(&this->m_subtitlePath);
  this->m_subtitlePath.append(this->m_info.fileName);

  dlg->GetSocket().SendSubtitleExist(this->m_subtitlePath);
  this->ProcessPacket();

  if (this->m_subtitleExist)
  {
    CString subTitleFileName = this->m_subtitleFileName.c_str();
    CString subTitileFitted;

    Util::GetFittedText(this->m_subtitle, subTitleFileName, &subTitileFitted);
    this->m_subtitle.SetWindowText(subTitileFitted);

  }
  else
  {
    dlg->GetSocket().SendSubtitleURL(this->m_subtitlePath);
    this->ProcessPacket();

    if (this->m_subtitleExist)
    {
      this->m_subtitle.SetWindowText(L"외부서버에 존재");
    }
    else
    {
      this->m_subtitle.SetWindowText(L"없음");

    }

  }

  return TRUE;  // return TRUE unless you set the focus to a control
  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CViewFileDetailDlg::OnPaint()
{
  CPaintDC dc(this); // device context for painting
  CImageList &list = Util::GetShellImageList();
  CRect rc;
  CPoint pt;
  SHFILEINFO sfi;
  size_t index = this->m_info.fileName.find_last_of(EXTENTION_SEPERATOR);

  if (index != wstring::npos)
  {
    wstring ext = this->m_info.fileName.substr(index);

    SHGetFileInfo(ext.c_str(), 0, &sfi, sizeof(SHFILEINFO), SHGFI_USEFILEATTRIBUTES | SHGFI_ICON | SHGFI_SMALLICON);
    DestroyIcon(sfi.hIcon);

  }
  else
  {
    sfi.iIcon = 0;

  }

  this->m_fileName.GetWindowRect(&rc);
  this->ScreenToClient(&rc);

  IMAGEINFO imageInfo;

  list.GetImageInfo(sfi.iIcon, &imageInfo);

  pt.x = rc.left - 20;
  pt.y = rc.top - (((imageInfo.rcImage.bottom - imageInfo.rcImage.top) / 2) - ((rc.bottom - rc.top) / 2));

  list.Draw(&dc, sfi.iIcon, pt, ILD_TRANSPARENT);

  // 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.
}

void CViewFileDetailDlg::ProcessPacket()
{
  bool nextProcess = false;
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  do
  {
    if (!Util::HandlePacket(dlg->GetSocket(), this, &nextProcess, nullptr))
    {
      this->EndDialog(IDCLOSE);

      break;

    }

  } while (nextProcess);

}

bool CViewFileDetailDlg::HandlePacket(ANYVOD_PACKET *packet)
{
  switch (ntohl(packet->s2c_header.header.type))
  {
  case PT_ADMIN_S2C_SUBTITLE_EXIST:
    {
      if (packet->s2c_header.success)
      {
        this->m_subtitleExist = packet->admin_s2c_subtitle_exist.exist != 0;
        Util::ConvertUTF8ToUnicode((char*)packet->admin_s2c_subtitle_exist.fileName, MAX_PATH_SIZE, &this->m_subtitleFileName);

      }

      break;

    }
  case PT_S2C_SUBTITLE_URL:
    {
      if (packet->s2c_header.success)
      {
        this->m_subtitleExist = strlen((char*)packet->s2c_subtitle_url.url) > 0;

      }

    }

  }

  return false;

}
