/*************************************************************************
Copyright (c) 2011-2021, DongRyeol Cha (chadr@dcple.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*************************************************************************/

#pragma once

#include "ParentDlg.h"

union ANYVOD_PACKET;

// CScheduleAddDlg 대화 상자입니다.

class CScheduleAddDlg : public CParentDlg
{
	DECLARE_DYNAMIC(CScheduleAddDlg)

public:
	CScheduleAddDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CScheduleAddDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SCHEDULE_ADD_DIALOG };

private:
  void ProcessPacket();
  virtual bool HandlePacket(ANYVOD_PACKET *packet);
  virtual void InitControls();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
  CEdit m_minute;
  CEdit m_hour;
  CEdit m_day;
  CEdit m_month;
  CEdit m_weekday;
  CButton m_minuteAll;
  CButton m_hourAll;
  CButton m_dayAll;
  CButton m_monthAll;
  CButton m_weekdayAll;
  CComboBox m_types;

  afx_msg void OnBnClickedButtonScheduleAddClose();
  afx_msg void OnBnClickedButtonScheduleAddAdd();
  afx_msg void OnBnClickedCheckScheduleAddMinuteAll();
  afx_msg void OnBnClickedCheckScheduleAddHourAll();
  afx_msg void OnBnClickedCheckScheduleAddDayAll();
  afx_msg void OnBnClickedCheckScheduleAddMonthAll();
  afx_msg void OnBnClickedCheckScheduleAddWeekdayAll();
  virtual BOOL PreTranslateMessage(MSG* pMsg);
protected:
  virtual void OnOK();
  virtual void OnCancel();
};
