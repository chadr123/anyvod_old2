./configure --prefix=/home/pi/build/libass/build --disable-static --enable-shared --enable-fontconfig CFLAGS="-g -O2 -march=armv8-a -mtune=cortex-a53 -mfpu=crypto-neon-fp-armv8 -mabi=aapcs-linux" LDFLAGS="-march=armv8-a -mtune=cortex-a53 -mfpu=crypto-neon-fp-armv8 -mabi=aapcs-linux"

make install-strip
