#! /bin/sh

install_name_tool -id @executable_path/libavcodec.56.dylib libavcodec.56.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavutil.54.dylib @executable_path/libavutil.54.dylib libavcodec.56.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libswresample.1.dylib @executable_path/libswresample.1.dylib libavcodec.56.dylib

install_name_tool -id @executable_path/libavformat.56.dylib libavformat.56.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavcodec.56.dylib @executable_path/libavcodec.56.dylib libavformat.56.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavutil.54.dylib @executable_path/libavutil.54.dylib libavformat.56.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libswresample.1.dylib @executable_path/libswresample.1.dylib libavformat.56.dylib

install_name_tool -id @executable_path/libavutil.54.dylib libavutil.54.dylib

install_name_tool -id @executable_path/libswresample.1.dylib libswresample.1.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavutil.54.dylib @executable_path/libavutil.54.dylib libswresample.1.dylib

install_name_tool -id @executable_path/libswscale.3.dylib libswscale.3.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavutil.54.dylib @executable_path/libavutil.54.dylib libswscale.3.dylib
#######
install_name_tool -id @executable_path/libass.9.dylib libass.9.dylib

install_name_tool -id @executable_path/libbass.dylib libbass.dylib

install_name_tool -id @executable_path/libbassmix.dylib libbassmix.dylib
install_name_tool -change @loader_path/libbass.dylib @executable_path/libbass.dylib libbassmix.dylib

install_name_tool -id @executable_path/libbass_fx.dylib libbass_fx.dylib
install_name_tool -change @loader_path/libbass.dylib @executable_path/libbass.dylib libbass_fx.dylib

install_name_tool -id @executable_path/libmediainfo.0.dylib libmediainfo.0.dylib

exit 0
