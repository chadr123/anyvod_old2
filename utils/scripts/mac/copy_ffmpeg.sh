#!/bin/bash

cd build/lib

install_name_tool -id @executable_path/libavcodec.58.dylib libavcodec.58.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavutil.56.dylib @executable_path/libavutil.56.dylib libavcodec.58.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libswresample.3.dylib @executable_path/libswresample.3.dylib libavcodec.58.dylib
install_name_tool -change /opt/local/lib/libz.1.dylib /usr/lib/libz.1.dylib libavcodec.58.dylib
install_name_tool -change /Users/chadr/Downloads/tools/lib/libz.1.dylib /usr/lib/libz.1.dylib libavcodec.58.dylib

install_name_tool -id @executable_path/libavformat.58.dylib libavformat.58.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavcodec.58.dylib @executable_path/libavcodec.58.dylib libavformat.58.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavutil.56.dylib @executable_path/libavutil.56.dylib libavformat.58.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libswresample.3.dylib @executable_path/libswresample.3.dylib libavformat.58.dylib
install_name_tool -change /opt/local/lib/libz.1.dylib /usr/lib/libz.1.dylib libavformat.58.dylib
install_name_tool -change /Users/chadr/Downloads/tools/lib/libz.1.dylib /usr/lib/libz.1.dylib libavformat.58.dylib

install_name_tool -id @executable_path/libavutil.56.dylib libavutil.56.dylib
install_name_tool -change /opt/local/lib/libz.1.dylib /usr/lib/libz.1.dylib libavutil.56.dylib
install_name_tool -change /Users/chadr/Downloads/tools/lib/libz.1.dylib /usr/lib/libz.1.dylib libavutil.56.dylib

install_name_tool -id @executable_path/libavfilter.7.dylib libavfilter.7.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavformat.58.dylib @executable_path/libavformat.58.dylib libavfilter.7.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavcodec.58.dylib @executable_path/libavcodec.58.dylib libavfilter.7.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavutil.56.dylib @executable_path/libavutil.56.dylib libavfilter.7.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libswresample.3.dylib @executable_path/libswresample.3.dylib libavfilter.7.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libswscale.5.dylib @executable_path/libswscale.5.dylib libavfilter.7.dylib
install_name_tool -change /opt/local/lib/libz.1.dylib /usr/lib/libz.1.dylib libavfilter.7.dylib
install_name_tool -change /Users/chadr/Downloads/tools/lib/libz.1.dylib /usr/lib/libz.1.dylib libavfilter.7.dylib

install_name_tool -id @executable_path/libswresample.3.dylib libswresample.3.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavutil.56.dylib @executable_path/libavutil.56.dylib libswresample.3.dylib
install_name_tool -change /opt/local/lib/libz.1.dylib /usr/lib/libz.1.dylib libswresample.3.dylib
install_name_tool -change /Users/chadr/Downloads/tools/lib/libz.1.dylib /usr/lib/libz.1.dylib libswresample.3.dylib

install_name_tool -id @executable_path/libswscale.5.dylib libswscale.5.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavutil.56.dylib @executable_path/libavutil.56.dylib libswscale.5.dylib
install_name_tool -change /opt/local/lib/libz.1.dylib /usr/lib/libz.1.dylib libswscale.5.dylib
install_name_tool -change /Users/chadr/Downloads/tools/lib/libz.1.dylib /usr/lib/libz.1.dylib libswscale.5.dylib

install_name_tool -id @executable_path/libavdevice.58.dylib libavdevice.58.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavformat.58.dylib @executable_path/libavformat.58.dylib libavdevice.58.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavcodec.58.dylib @executable_path/libavcodec.58.dylib libavdevice.58.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libswresample.3.dylib @executable_path/libswresample.3.dylib libavdevice.58.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavutil.56.dylib @executable_path/libavutil.56.dylib libavdevice.58.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libswscale.5.dylib @executable_path/libswscale.5.dylib libavdevice.58.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavfilter.7.dylib @executable_path/libavfilter.7.dylib libavdevice.58.dylib
install_name_tool -change /opt/local/lib/libz.1.dylib /usr/lib/libz.1.dylib libavdevice.58.dylib
install_name_tool -change /Users/chadr/Downloads/tools/lib/libz.1.dylib /usr/lib/libz.1.dylib libavdevice.58.dylib

cp libavdevice.58.dylib libavcodec.58.dylib libavformat.58.dylib libavutil.56.dylib libavfilter.7.dylib libswresample.3.dylib libswscale.5.dylib /Users/chadr/project/anyvod/client/AnyVODClient/libs/mac

cd -


