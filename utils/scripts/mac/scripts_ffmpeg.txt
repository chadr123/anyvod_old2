common
./configure --prefix=/Users/chadr/Downloads/tools --disable-shared --enable-static

libcdio
./autogen.sh
./configure --enable-static --disable-shared --prefix=/Users/chadr/Downloads/tools --without-cd-drive --without-cd-info --without-cdda-player --without-cd-read --without-iso-info --without-iso-read

libcdio-paranoia
./autogen.sh
./configure --enable-static --disable-shared --prefix=/Users/chadr/Downloads/tools --enable-cpp-progs="no" LIBCDIO_CFLAGS="-I/Users/chadr/Downloads/tools/include" LIBCDIO_LIBS="-L/Users/chadr/Downloads/tools/lib -lcdio -framework CoreFoundation -framework DiskArbitration -framework IOKit"

bzip2
make install PREFIX=/Users/chadr/Downloads/tools

dav1d
meson build --default-library=static --prefix / --buildtype release
ninja -C build
DESTDIR=/Users/chadr/Downloads/tools ninja -C build install

ffmpeg
patch -p1 < ffmpeg_append_protocol.patch

./configure --prefix=/Users/chadr/Downloads/ffmpeg/build --pkg-config=/Users/chadr/Downloads/tools/bin/pkg-config --disable-doc --disable-ffmpeg --disable-ffplay --disable-ffprobe --disable-postproc --disable-encoders --enable-encoder="ac3,dca" --disable-muxers --enable-muxer="spdif" --disable-sdl2 --disable-static --enable-shared --enable-gpl --enable-runtime-cpudetect --enable-bzlib --enable-libspeex --enable-libilbc --enable-zlib --enable-libcdio --enable-libdav1d --extra-cflags="-I/Users/chadr/Downloads/tools/include -mmacosx-version-min=10.12" --extra-ldflags="-L/Users/chadr/Downloads/tools/lib -mmacosx-version-min=10.12" --extra-libs="-liconv -framework CoreFoundation -framework DiskArbitration -framework IOKit"
