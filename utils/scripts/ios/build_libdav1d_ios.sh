#!/bin/sh

#  Automatic build script for libdav1d 
#  for iPhoneOS and iPhoneSimulator
#
#  Created by Felix Schulze on 19.02.12.
#  Copyright 2012 Felix Schulze. All rights reserved.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#
###########################################################################
#  Change values here                                                     #
#                                                                         #
MIN_VERSION="8.0"                                                         #
#                                                                         #
###########################################################################
#                                                                         #
# Don't change anything under this line!                                  #
#								          #
###########################################################################


SDKVERSION=`xcrun -sdk iphoneos --show-sdk-version`
CURRENTPATH=`pwd`
ARCHS="x86_64 arm64"
DEVELOPER=`xcode-select -print-path`

set -e
mkdir -p "${CURRENTPATH}/bin"
mkdir -p "${CURRENTPATH}/lib_tmp"

for ARCH in ${ARCHS}
do
    CMAKE=
    if [ "${ARCH}" == "x86_64" ]; then
        CMAKE="x86_64-ios-simulator"
    else
        CMAKE="${ARCH}-ios"
    fi

    if [ "${ARCH}" == "x86_64" ];
    then
        PLATFORM="iPhoneSimulator"
        PROPERTY="needs_exe_wrapper = true"
    else
        PLATFORM="iPhoneOS"
        PROPERTY=""
    fi

    echo "Building libdav1d for ${PLATFORM} ${SDKVERSION} ${ARCH}"
    echo "Please stand by..."

    DEVROOT="${DEVELOPER}/Platforms/${PLATFORM}.platform/Developer"
    SDKROOT="${DEVROOT}/SDKs/${PLATFORM}${SDKVERSION}.sdk"

    LD=${DEVELOPER}/Toolchains/XcodeDefault.xctoolchain/usr/bin/ld
    CC=${DEVELOPER}/usr/bin/gcc
    CXX=${DEVELOPER}/usr/bin/g++

    AR=${DEVELOPER}/Toolchains/XcodeDefault.xctoolchain/usr/bin/ar

    HOST="${ARCH}"
    if [ "${ARCH}" == "arm64" ];
    then
        HOST="aarch64"
    fi

    cat << EOF > meson_ios_${ARCH}.txt
[binaries]
name = 'ios'
c = '${CC}'
cpp = '${CXX}'
ar = '${AR}'
ld = '${LD}'
    
[properties]
c_args = ['-arch', '${ARCH}', '-pipe', '-no-cpp-precomp', '-isysroot', '${SDKROOT}', '-I${CURRENTPATH}/include', '-miphoneos-version-min=${MIN_VERSION}', '-fheinous-gnu-extensions']
cpp_args = ['-arch', '${ARCH}', '-pipe', '-no-cpp-precomp', '-isysroot', '${SDKROOT}', '-I${CURRENTPATH}/include', '-miphoneos-version-min=${MIN_VERSION}', '-fheinous-gnu-extensions']
c_link_args = ['-arch', '${ARCH}', '-pipe', '-no-cpp-precomp', '-isysroot', '${SDKROOT}', '-I${CURRENTPATH}/include', '-miphoneos-version-min=${MIN_VERSION}', '-fheinous-gnu-extensions']
cpp_link_args = ['-arch', '${ARCH}', '-pipe', '-no-cpp-precomp', '-isysroot', '${SDKROOT}', '-I${CURRENTPATH}/include', '-miphoneos-version-min=${MIN_VERSION}', '-fheinous-gnu-extensions']
${PROPERTY}
 
[host_machine]
system = 'darwin'
cpu_family = '${HOST}'
cpu = '${HOST}'
endian = 'little'
EOF

    rm -rf "${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk"	
    mkdir -p "${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk"
    LOG="${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk/build-libdav1d.log"

    echo "Configure..."
    ~/bin/meson-0.53.2/meson.py build --default-library=static --prefix / --buildtype release --cross-file meson_ios_${ARCH}.txt > "${LOG}" 2>&1
    cd build >> "${LOG}" 2>&1
    ~/bin/meson-0.53.2/meson.py configure -Denable_tools=false -Denable_tests=false >> "${LOG}" 2>&1
    cd - >> "${LOG}" 2>&1
    echo "Make..."
    ninja -C build >> "${LOG}" 2>&1
    echo "Make install..."
    DESTDIR="${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk" ninja -C build install >> "${LOG}" 2>&1
    rm -rf build >> "${LOG}" 2>&1
    rm -f meson_ios_${ARCH}.txt >> "${LOG}" 2>&1
done

echo "Build library..."
lipo -create ${CURRENTPATH}/bin/iPhoneSimulator${SDKVERSION}-x86_64.sdk/lib/libdav1d.a \
             ${CURRENTPATH}/bin/iPhoneOS${SDKVERSION}-arm64.sdk/lib/libdav1d.a \
             -output ${CURRENTPATH}/lib_tmp/libdav1d.a

lipo -info ${CURRENTPATH}/lib_tmp/libdav1d.a
mkdir -p ${CURRENTPATH}/../lib_tmp
cp ${CURRENTPATH}/lib_tmp/libdav1d.a ${CURRENTPATH}/../lib_tmp
mkdir -p ${CURRENTPATH}/../include/dav1d
cp  ${CURRENTPATH}/bin/iPhoneSimulator${SDKVERSION}-x86_64.sdk/include/dav1d/* ${CURRENTPATH}/../include/dav1d
echo "Building done."
echo "Done."
