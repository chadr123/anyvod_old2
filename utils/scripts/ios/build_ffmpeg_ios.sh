#!/bin/sh

# directories
FAT="FFmpeg-iOS"

SCRATCH="scratch"
# must be an absolute path
THIN=`pwd`/"thin"

CONFIGURE_FLAGS="--enable-cross-compile \
		--disable-debug \
		--disable-programs \
		--disable-doc \
		--disable-avdevice \
		--disable-postproc \
		--disable-encoders \
		--enable-encoder="ac3,dca" \
		--disable-muxers \
		--pkg-config=/Users/chadr/Downloads/tools/bin/pkg-config \
		--enable-muxer="spdif" \
		--enable-gpl \
		--enable-runtime-cpudetect \
		--enable-bzlib \
		--enable-zlib \
		--enable-libspeex \
		--enable-libilbc \
		--enable-libdav1d \
		--enable-pic"

ARCHS="arm64 x86_64"

COMPILE="y"
LIPO="y"

DEPLOYMENT_TARGET="8.0"

if [ "$*" ]
then
	if [ "$*" = "lipo" ]
	then
		# skip compile
		COMPILE=
	else
		ARCHS="$*"
		if [ $# -eq 1 ]
		then
			# skip lipo
			LIPO=
		fi
	fi
fi

if [ "$COMPILE" ]
then
	if [ ! `which gas-preprocessor.pl` ]
	then
		echo 'gas-preprocessor.pl not found. Trying to install...'
		(sudo curl -L https://raw.githubusercontent.com/FFmpeg/gas-preprocessor/master/gas-preprocessor.pl \
			-o /usr/local/bin/gas-preprocessor.pl \
			&& sudo chmod +x /usr/local/bin/gas-preprocessor.pl) \
			|| exit 1
	fi
	
	CWD=`pwd`
	for ARCH in $ARCHS
	do
		echo "building $ARCH..."
		mkdir -p "$SCRATCH/$ARCH"
		cd "$SCRATCH/$ARCH"

		CFLAGS="-Os -arch $ARCH -DLIBICONV_PLUG -I/Users/chadr/Downloads/ffmpeg/complete/ios/include"
		ADD_CONFIGURE_FLAGS=""
		if [ "$ARCH" = "x86_64" ]
		then
		    PLATFORM="iPhoneSimulator"
		    CFLAGS="$CFLAGS -mios-simulator-version-min=$DEPLOYMENT_TARGET"
		    ADD_CONFIGURE_FLAGS="$ADD_CONFIGURE_FLAGS"
		else
		    PLATFORM="iPhoneOS"
		    CFLAGS="$CFLAGS -mios-version-min=$DEPLOYMENT_TARGET"
		    ADD_CONFIGURE_FLAGS="$ADD_CONFIGURE_FLAGS"
		    if [ "$ARCH" = "arm64" ]
		    then
		        EXPORT="GASPP_FIX_XCODE5=1"
		    fi
		fi

		GAS_ARCH="$ARCH"

		XCRUN_SDK=`echo $PLATFORM | tr '[:upper:]' '[:lower:]'`
		SDK_PATH=`xcrun --show-sdk-path -sdk $XCRUN_SDK`
		CFLAGS="$CFLAGS -I$SDK_PATH/usr/include"
		CC="xcrun -sdk $XCRUN_SDK clang"
		AS="gas-preprocessor.pl -arch $GAS_ARCH -as-type apple-clang -- xcrun -sdk $XCRUN_SDK clang"
		CXXFLAGS="$CFLAGS"
		LDFLAGS="$CFLAGS -L/Users/chadr/Downloads/ffmpeg/complete/ios/lib_tmp -L$SDK_PATH/usr/lib"
		TMPDIR=${TMPDIR/%\/} $CWD/configure \
		    --target-os=darwin \
		    --arch=$ARCH \
		    --cc="$CC" \
		    --as="$AS" \
		    $CONFIGURE_FLAGS \
		    $ADD_CONFIGURE_FLAGS \
		    --extra-cflags="$CFLAGS" \
		    --extra-ldflags="$LDFLAGS" \
		    --prefix="$THIN/$ARCH" \
		|| exit 1

		make clean
		make -j4 install $EXPORT || exit 1
		cd $CWD
	done
fi

if [ "$LIPO" ]
then
	echo "building fat binaries..."
	mkdir -p $FAT/lib
	set - $ARCHS
	CWD=`pwd`
	cd $THIN/$1/lib
	for LIB in *.a
	do
		cd $CWD
		echo lipo -create `find $THIN -name $LIB` -output $FAT/lib/$LIB 1>&2
		lipo -create `find $THIN -name $LIB` -output $FAT/lib/$LIB || exit 1
	done

	cd $CWD
	cp -rf $THIN/$1/include $FAT
fi

echo Done
