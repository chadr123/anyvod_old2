#!/bin/bash

. ./installer/common/functions.sh

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 PASSWORD(signing)"
  exit 1
fi

cd ./installer/android
check

./make.sh $1 07
check

exit 0
