#!/bin/bash

. ../../installer/common/functions.sh

version=`cat ../linux/version`
fileName="AnyVODMobile-${version}_raspi_armhf.deb"
installRoot="usr/bin"
installDirectory="AnyVODMobile"
installPath="${installRoot}/${installDirectory}"
escapedInstallPath=`echo "$installPath" | sed 's/\//\\\\\//g'`

cd ../../package
check

if [ -d AnyVODMobile ] ; then
  rm -rf AnyVODMobile
  check
fi

mkdir -p AnyVODMobile/${installPath}
check

mkdir -p AnyVODMobile/lib/systemd/system
check

cp ../installer/raspi/anyvodmobile.service AnyVODMobile/lib/systemd/system
check

sed -i "s/%BIN_PATH%/\/${escapedInstallPath}/g" client/AnyVODMobileClient.sh
check

cp -a client/* AnyVODMobile/${installPath}
check

cd AnyVODMobile/${installRoot}
check

strip "${installDirectory}/AnyVODMobileClient_bin"
check

ln -s "${installDirectory}/AnyVODMobileClient.sh" AnyVODMobileClient
check

cd -
check

cp -a ../installer/raspi/deb/* AnyVODMobile
check

sed -i "s/%VERSION%/${version}/g" AnyVODMobile/DEBIAN/control
check

escapedInstallPath=$(eval echo "/${installPath}" | sed 's/\//\\\//g')
sed -i "s/%INSTALL_PATH%/${escapedInstallPath}/g" AnyVODMobile/DEBIAN/postrm
check

dpkg-deb --build AnyVODMobile ${fileName}
check

exit 0
