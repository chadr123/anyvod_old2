#!/bin/bash

. ../../installer/common/functions.sh

if [ -d ../../package ] ; then
  rm -rf ../../package
  check
fi

cd ../../client/AnyVODMobileClient/scripts/raspi
check

./build.sh
check

cd ../../../../installer/raspi
check

./package.sh
check

exit 0
