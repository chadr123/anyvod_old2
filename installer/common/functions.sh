#!/bin/bash

function check()
{
  local ret=$?

  if [ $ret -ne 0 ]; then
    exit $ret
  fi
}
