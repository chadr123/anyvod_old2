#!/bin/bash

version_qt=`cat ../../client/AnyVODClient/scripts/linux/version`
qt_path=`../../client/AnyVODClient/scripts/linux/qt_path.sh`
BIN_FILE=../../package/client/AnyVODClient.app/Contents/MacOS/AnyVODClient
QTDIR=$HOME/$qt_path/$version_qt/clang_64

for P in `otool -L $BIN_FILE | awk '{print $1}'`
do
    if [[ "$P" == *//* ]]
    then
        PSLASH=$(echo $P | sed 's,//,/,g')
        install_name_tool -change $P $PSLASH $BIN_FILE
    fi
done

for F in `find $QTDIR/lib $QTDIR/plugins $QTDIR/qml  -perm 755 -type f`
do
    for P in `otool -L $F | awk '{print $1}'`
    do
        if [[ "$P" == *//* ]]
        then
            PSLASH=$(echo $P | sed 's,//,/,g')
            install_name_tool -change $P $PSLASH $F
        fi
     done
done

exit 0
