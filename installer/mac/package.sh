#!/bin/bash

. ../../installer/common/functions.sh

version=`cat ../linux/version`
version_qt=`cat ../../client/AnyVODClient/scripts/linux/version`
qt_path=`../../client/AnyVODClient/scripts/linux/qt_path.sh`

./change_bin_lib_path.sh
check

cd ../../package
check

$HOME/$qt_path/$version_qt/clang_64/bin/macdeployqt client/AnyVODClient.app -verbose=2
check

mkdir resources
check

cp ../licenses/anyvod.txt resources
check

mkdir scripts
check

cp ../installer/mac/postinstall ../installer/mac/preinstall scripts
check

pkgbuild --component client/AnyVODClient.app --scripts scripts --identifier com.dcple.anyvod --version ${version} --install-location /Applications AnyVOD.pkg
check

productbuild --distribution ../installer/mac/distribution.xml --resources resources --package-path AnyVOD.pkg AnyVOD-${version}_mac.pkg
check

rm -rf client resources scripts AnyVOD.pkg
check

exit 0
