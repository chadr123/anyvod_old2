#!/bin/bash

. ../../installer/common/functions.sh

if [ -d ../../package ] ; then
  rm -rf ../../package
  check
fi

cd ../../client/AnyVODMobileClient/scripts/android
check

./build.sh $1 $2
check

cd ../../../../installer/android
check

./package.sh
check

exit 0
