#!/bin/bash

. ../../installer/common/functions.sh

version=`cat ../linux/version`
version_qt=`cat ../../client/AnyVODClient/scripts/linux/version`
qt_path=`../../client/AnyVODClient/scripts/linux/qt_path.sh`

cd ../../package
check

cp ../client/AnyVODMobileClient-build-android/android-build/build/outputs/apk/release/android-build-release-signed.apk ./AnyVODMobile-${version}_android.apk
check

exit 0
