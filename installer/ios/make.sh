#!/bin/bash

. ../../installer/common/functions.sh

if [ -d ../../package ] ; then
  rm -rf ../../package
  check
fi

cd ../../client/AnyVODMobileClient/scripts/ios
check

./build.sh $1
check

cd ../../../../installer/ios
check

./package.sh
check

exit 0
